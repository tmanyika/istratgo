﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION imd_user_db.fxTrialPrdLeft 
(
	@daysUsed int,
	@trialMaxDays int
)
RETURNS int
AS
BEGIN
	declare @daysLeft int;
	set @daysLeft = @trialMaxDays - @daysUsed
	return @daysLeft
END