﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE function [imd_user_db].[RemoveDoubleSpace]
(
	@dataVal varchar(8000) = null
)
Returns VarChar(8000)
as
begin
	declare @data varchar(8000);
	set @data = ltrim(rtrim(@dataVal));
	
	if(@data is null)
		return null
		
    while CharIndex('  ', @data) > 0
        set @data = Replace(@data, '  ', ' ')

    return @data
end