﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [imd_user_db].[fxGetOrgStructHierachyTop]
(	
	@orgUnitId int
)
RETURNS TABLE 
AS
RETURN 
(
	 with myCTE (Id, ParentId, Depth)
	 as
	 (
		select ID, PARENT_ORG , 0 as Depth from imd_user_db.Company_Structure where ID = @orgUnitId
		union ALL
		select imd_user_db.Company_Structure.ID, imd_user_db.Company_Structure.PARENT_ORG, Depth + 1 
		from imd_user_db.Company_Structure 
		inner join myCte on imd_user_db.Company_Structure.Id = myCte.ParentId
	 )

	select Id, ParentId, Depth from myCTE
)