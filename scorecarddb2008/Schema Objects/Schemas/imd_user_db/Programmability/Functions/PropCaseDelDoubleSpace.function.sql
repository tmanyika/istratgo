﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE function [imd_user_db].[PropCaseDelDoubleSpace]
(
	@TextVal as varchar(8000) = null
)
	returns varchar(8000)
as
begin
   declare @Reset bit;
   declare @Ret varchar(8000);
   declare @RetVal varchar(8000);
   declare @i int;
   declare @c char(1);
   declare @Text as varchar(8000);
   
   set @Text = ltrim(rtrim(@TextVal))
   
   if (@Text is null) return null
   select @Reset = 1, @i=1, @Ret = '';
   
   while (@i <= len(@Text))
		begin
   			select @c= substring(@Text,@i,1),
				   @Ret = @Ret + case when @Reset=1 then UPPER(@c) else LOWER(@c) end,
				   @Reset = case when @c like '[a-zA-Z]' then 0 else 1 end,
				   @i = @i +1
		end        
   select @RetVal=imd_user_db.RemoveDoubleSpace(@Ret)
   return @RetVal
end