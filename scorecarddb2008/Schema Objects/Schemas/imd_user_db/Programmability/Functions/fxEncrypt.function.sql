﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [imd_user_db].[fxEncrypt]
(
	@strVal varchar(128)
)
RETURNS varbinary(128)
AS
BEGIN
	declare @encValue varbinary(128);
	set @encValue=EncryptByKey(Key_GUID('FieldSymmetricKey'),@strVal)
	return @encValue
END