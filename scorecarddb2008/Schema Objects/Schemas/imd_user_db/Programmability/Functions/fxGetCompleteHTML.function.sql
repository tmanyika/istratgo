﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE function [imd_user_db].[fxGetCompleteHTML] 
(
	@mailBody varchar(max)
)
returns varchar(max)
as
begin
	return '<html xmlns=''http://www.w3.org/1999/xhtml''><head><title>iStratgo Notification</title><link href=''http://scorecard.imdconsulting.co.za/ui/css/email.css'' rel=''stylesheet'' type=''text/css'' /></head><body>' + @mailBody + '</body></html>'
end