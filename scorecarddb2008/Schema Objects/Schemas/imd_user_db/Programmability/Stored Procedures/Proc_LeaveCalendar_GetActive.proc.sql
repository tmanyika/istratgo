﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_LeaveCalendar_GetActive]
	@countryId int,
	@calendarYear int,
	@active bit
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	select l.HolidayDateId, l.CountryId, l.HolidayDate, l.Detail, l.CalendarYear, l.Active, l.CreatedBy, l.DateCreated, l.DateUpdated, l.UpdatedBy,
	       u.[TYPE_NAME] as CountryName
	from imd_user_db.LeaveCalendar as l inner join imd_user_db.UTILITIES as u on u.ID = l.CountryId
	where l.CountryId = @countryId and l.CalendarYear = @calendarYear and l.Active = @active
	order by l.HolidayDate
END