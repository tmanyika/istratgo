﻿

CREATE PROCEDURE [imd_user_db].[Proc_DashBoard_TalentMatrix] 
	@CRITERIA_TYPE_ID int,
	@SELECTED_ITEM_VALUE int,
	@ISCOMPANY bit,
	@STARTDATE datetime,
	@ENDDATE datetime
as
begin
	set nocount on;
	set transaction isolation level read uncommitted;
	
	select d.EmployeeId, RatingValue, round(isnull(YearsOnSameRole,0),1) as YearsOnSameRole
	from (
			select EmployeeId, round((SUM(FINAL_SCORE_AGREED) / cast(count(FINAL_SCORE_AGREED) as decimal(18,1)) ),1) as RatingValue
			from (
					select isc.CRITERIA_TYPE_ID, isc.SELECTED_ITEM_VALUE AS EmployeeId, isc.PERIOD_DATE AS PERIOD_DATE_COUNT, 
					       SUM(isc.FINAL_SCORE_AGREED) AS FINAL_SCORE_AGREED, isc.PERIOD_DATE
					from  imd_user_db.INDIVIDUAL_SCORES AS isc INNER JOIN
						  imd_user_db.Employee AS e ON isc.SELECTED_ITEM_VALUE = e.EmployeeId INNER JOIN 
			              imd_user_db.SUBMISSIONS AS sb ON isc.SUBMISSION_ID = sb.ID
			       where (sb.[STATUS] = 40 AND isc.PARENT_ID IS NULL) AND (e.OrgUnitId = @SELECTED_ITEM_VALUE) AND (isc.CRITERIA_TYPE_ID = @CRITERIA_TYPE_ID) AND (isc.PERIOD_DATE BETWEEN @STARTDATE AND @ENDDATE)
					group by isc.CRITERIA_TYPE_ID, isc.SELECTED_ITEM_VALUE, isc.PERIOD_DATE
			    ) as data
			group by EmployeeId
		) as d INNER JOIN imd_user_db.vw_EmpTalentMatrix as e ON d.EmployeeId = e.EmployeeId
end