﻿


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_EmailTemplate_GetTemplates]
	@active bit,
	@ismaster bit
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	select   MailId, TemplateId, MailName, MailSubject, MailContent, IsMasterTemplate, Active, CreatedBy, DateCreated, DateUpdated, UpdatedBy
	from     imd_user_db.MailTemplate
	where     (Active = @active and @ismaster = IsMasterTemplate)
	order by MailName
END