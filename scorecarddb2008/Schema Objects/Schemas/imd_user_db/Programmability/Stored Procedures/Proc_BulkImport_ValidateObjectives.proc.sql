﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE imd_user_db.Proc_BulkImport_ValidateObjectives
	@CompanyId int
AS
BEGIN
	set nocount on
	set transaction isolation level read uncommitted;
	
	select     CompanyId, StrategicObjective, KeyFocusArea, TotalCount
	from         imd_user_db.vw_BulkValidateObjectives
	where     (CompanyId = @CompanyId)
END