﻿
CREATE PROCEDURE [imd_user_db].[PROC_GET_COMPANY_BY_CUSTOMER]
@ACCOUNT_NO INT
AS
SELECT [COMPANY_ID]
      ,[ACCOUNT_NO]
      ,[COMPANY_NAME]
      ,[REGISTRATION_NO]
      ,[INDUSTRY]
      ,[COUNTRY]
      ,[VAT_NO]
      ,[ADDRESS]
      ,[CONTACT_NO]
      ,[ACTIVE]
  FROM [COMPANY_DETAIL] WHERE ACCOUNT_NO= @ACCOUNT_NO