﻿

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_EmployeeStatus_Update]
	@statusId int,
	@statusName varchar(100), 
	@active bit, 
	@updatedBy varchar(20)
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	update  imd_user_db.EmployeeStatus
	set     StatusName = @statusName, 
			Active = @active, 
			UpdatedBy = @updatedBy, 
			DateUpdated = GETDATE()
	where   (StatusId = @statusId)
		
	select @@rowcount
END