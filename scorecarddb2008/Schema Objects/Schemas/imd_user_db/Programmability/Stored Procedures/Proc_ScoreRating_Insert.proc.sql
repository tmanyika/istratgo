﻿


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_ScoreRating_Insert]
	@CompanyId int,
	@RatingValue decimal(18,1),
	@MinimumValue decimal(18,2) = null, 
	@MaximumValue decimal(18,2) = null,
	@RatingDescription varchar(100),
	@Active bit,
	@CreatedBy varchar(50)
AS
BEGIN
	set nocount on;
	if not(exists(select RatingValue from imd_user_db.ScoreRating where Active = 1 and CompanyId = @CompanyId and RatingValue = @RatingValue)) 
		insert into imd_user_db.ScoreRating(CompanyId, RatingValue, MinimumValue, MaximumValue, RatingDescription, Active, CreatedBy)
		values(@CompanyId, @RatingValue, @MinimumValue, @MaximumValue, @RatingDescription, @Active, @CreatedBy)
	select @@rowcount
END