﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_EmailRelay_Get]
	@mailId int
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	declare @subject varchar(800), @mailContent nvarchar(max);
	select @subject=mailsubject,@mailContent=mailContent from imd_user_db.MailTemplate 
	where mailId in (select top 1 templateId from imd_user_db.MailTemplate where MailId = @mailId)
	
	select MailId, replace(@subject,'{0}', MailSubject) as MailSubject, replace(@mailContent,'{1}',MailContent) as MailContent
	from imd_user_db.MailTemplate
	where  (MailId = @mailId)	
END