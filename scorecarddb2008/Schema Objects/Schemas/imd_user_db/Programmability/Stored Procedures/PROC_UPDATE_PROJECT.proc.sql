﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[PROC_UPDATE_PROJECT]
	@PROJECT_ID int,
	@PROJECT_NAME varchar(200),
	@PROJECT_DESC varchar(400),
	@UPDATED_BY varchar(20),
	@RESPONSIBLE_PERSON_ID int = NULL	
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE  COMPANY_PROJECT
	SET		PROJECT_NAME = @PROJECT_NAME, 
			PROJECT_DESC = @PROJECT_DESC, 
			DATE_UPDATED = getdate(), 
			UPDATE_BY = @UPDATED_BY,
			RESPONSIBLE_PERSON_ID = @RESPONSIBLE_PERSON_ID
	where PROJECT_ID = @PROJECT_ID
	SELECT @@rowcount
END