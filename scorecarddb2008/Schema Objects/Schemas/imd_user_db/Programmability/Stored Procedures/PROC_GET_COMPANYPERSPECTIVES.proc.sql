﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[PROC_GET_COMPANYPERSPECTIVES]
	@COMAPNY_ID int,
	@ACTIVE bit
AS
BEGIN
	SET NOCOUNT ON;
	SELECT     PERSPECTIVE_ID, COMPANY_ID, PERSPECTIVE_NAME, PERSPECTIVE_DESC, ACTIVE, CREATED_BY, DATE_CREATED, DATE_UPDATED, UPDATE_BY
	FROM         COMPANY_PERSPECTIVE WITH (nolock)
	WHERE     (COMPANY_ID = @COMAPNY_ID and active = @ACTIVE)
END