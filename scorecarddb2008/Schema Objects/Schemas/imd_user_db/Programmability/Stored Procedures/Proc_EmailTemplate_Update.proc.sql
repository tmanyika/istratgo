﻿


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_EmailTemplate_Update]
	@templateId int = null,
	@mailId int,
	@mailName varchar(800), 
	@subject varchar(800),
	@content nvarchar(max),
	@active bit, 
	@isMaster bit,
	@updatedBy varchar(20)
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	update  imd_user_db.MailTemplate
	set     MailName = @mailName, 
	        MailSubject = @subject,
	        MailContent = @content,
			Active = @active, 
			IsMasterTemplate = @isMaster,
			TemplateId = @templateId,
			UpdatedBy = @updatedBy, 
			DateUpdated = GETDATE()
	where   (MailId = @mailId)
		
	select @@rowcount
END