﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[PROC_GET_COMPANYPROJECTS]
	@companyId int,
	@active bit
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	select     cp.PROJECT_ID, cp.COMPANY_ID, cp.PROJECT_NAME, cp.ACTIVE, cp.PROJECT_DESC, cp.CREATED_BY, cp.DATE_CREATED, cp.DATE_UPDATED, cp.UPDATE_BY, 
                      cp.RESPONSIBLE_PERSON_ID, e.EmailAddress AS Email_Address, e.FirstName + ' ' + ISNULL(e.MiddleName, '') + ' ' + e.LastName AS NAME
	from         imd_user_db.COMPANY_PROJECT as cp with (nolock) INNER JOIN
						  imd_user_db.Employee as e on cp.RESPONSIBLE_PERSON_ID = e.EmployeeId
	where     (cp.COMPANY_ID = @companyId) AND (cp.ACTIVE = @active)
	order by cp.PROJECT_NAME
END