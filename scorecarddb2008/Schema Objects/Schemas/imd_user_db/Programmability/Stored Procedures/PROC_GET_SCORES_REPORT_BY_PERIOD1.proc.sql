﻿CREATE PROCEDURE [imd_user_db].[PROC_GET_SCORES_REPORT_BY_PERIOD1]
	@CRITERIA_TYPE_ID INT,
	@SELECTED_ITEM_VALUE int,
	@START_DATE datetime,
	@END_DATE datetime
as
begin
	set nocount on;
	set transaction isolation level read uncommitted;
	
	select     isc.CRITERIA_TYPE_ID, isc.SELECTED_ITEM_VALUE, SUM(isc.FINAL_SCORE) / COUNT(isc.PERIOD_DATE) AS FINAL_SCORE, SUM(ig.WEIGHT) / COUNT(isc.PERIOD_DATE) 
                      AS WEIGHT, SUM(isc.FINAL_SCORE) as TOTAL_FINAL_SCORE, SUM(ig.WEIGHT) as TOTAL_WEIGHT, count(isc.PERIOD_DATE) as TotalPeriod, ig.GOAL_DESCRIPTION, 
                      cp.PERSPECTIVE_NAME, cs.OBJECTIVE, u.TYPE_NAME AS UNIT_MEASURE, cp.PERSPECTIVE_ID, 100 as TARGET
	from         imd_user_db.INDIVIDUAL_SCORES AS isc INNER JOIN
						  imd_user_db.INDIVIDUAL_GOALS AS ig ON isc.GOAL_ID = ig.ID INNER JOIN
						  imd_user_db.COMPANY_STRATEGIC AS cs ON ig.STRATEGIC_ID = cs.ID INNER JOIN
						  imd_user_db.COMPANY_PERSPECTIVE AS cp ON cs.PERSPECTIVE = cp.PERSPECTIVE_ID LEFT OUTER JOIN
						  imd_user_db.UTILITIES AS u ON ig.UNIT_MEASURE = u.ID
	where     (isc.SELECTED_ITEM_VALUE = @SELECTED_ITEM_VALUE) AND (isc.CRITERIA_TYPE_ID = @CRITERIA_TYPE_ID) 
		AND (isc.PERIOD_DATE BETWEEN @START_DATE AND @END_DATE)
	group by isc.CRITERIA_TYPE_ID, isc.SELECTED_ITEM_VALUE, ig.GOAL_DESCRIPTION, cp.PERSPECTIVE_NAME, cs.OBJECTIVE, u.TYPE_NAME, cp.PERSPECTIVE_ID
	order by cp.PERSPECTIVE_NAME, cs.OBJECTIVE
end