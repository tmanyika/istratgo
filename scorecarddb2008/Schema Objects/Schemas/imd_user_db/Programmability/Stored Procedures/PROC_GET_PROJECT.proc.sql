﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[PROC_GET_PROJECT]
	@PROJECT_ID int
AS
BEGIN
	SET NOCOUNT ON;
	SELECT     cp.PROJECT_ID, cp.COMPANY_ID, cp.PROJECT_NAME, cp.ACTIVE, cp.PROJECT_DESC, cp.CREATED_BY, cp.DATE_CREATED, cp.DATE_UPDATED, cp.UPDATE_BY, 
                      cp.RESPONSIBLE_PERSON_ID, ua.NAME, ua.EMAIL_ADDRESS
	FROM         COMPANY_PROJECT AS cp WITH (nolock) left outer JOIN
						  USER_ADMIN AS ua WITH (nolock) ON cp.RESPONSIBLE_PERSON_ID = ua.ID
	WHERE     (cp.PROJECT_ID = @PROJECT_ID)
END