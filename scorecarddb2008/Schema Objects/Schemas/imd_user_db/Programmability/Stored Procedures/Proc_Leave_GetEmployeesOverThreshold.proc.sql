﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_Leave_GetEmployeesOverThreshold]
	@orgUnitId int,
	@LeaveXml xml
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	select	nref.value('Id[1]', 'int') as leaveTypeId
	into #lvType
	from   @LeaveXml.nodes('//Data/Leave') AS R(nref)
	
	select e.FirstName + ' ' + isnull(e.MiddleName,'') + ' ' + e.LastName as FullName, e.EmailAddress, e.EmployeeNo, lt.LeaveName, isnull(lb.Accumulated,0) as Accumulated, isnull(lb.LimitValue,0) as LimitValue, (isnull(lb.Accumulated,0) - isnull(lb.LimitValue,0)) as OverLimit 
	from   imd_user_db.Employee AS e LEFT OUTER JOIN
		   imd_user_db.LeaveBalance AS lb ON e.EmployeeId = lb.EmployeeId INNER JOIN
		   imd_user_db.LeaveType AS lt ON lb.LeaveId = lt.LeaveId
	where  (isnull(lb.Accumulated,0) > isnull(lb.LimitValue,0)) AND (e.OrgUnitId = @orgUnitId) AND (lb.LeaveId IN (select leaveTypeId from #lvType))
	order by e.LastName, e.FirstName, lt.LeaveName
	
	drop table #lvType
END