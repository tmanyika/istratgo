﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_BulkSheet_Update]
	@SheetId int,
	@SheetName varchar(150), 
	@DbTableName varchar(50), 
	@SheetDescription varchar(1000), 
	@SheetNotes varchar(800), 
	@Active bit = 1, 
	@UpdatedBy varchar(50)
AS
BEGIN
	set nocount on;
	if not(exists(select SheetName from imd_user_db.BulkSheet where SheetId <> @SheetId and SheetName like @SheetName and Active = 1))
		update imd_user_db.BulkSheet
		set SheetName = @SheetName, 
			DbTableName = @DbTableName, 
			SheetDescription = @SheetDescription, 
			SheetNotes = @SheetNotes, 
			Active = @Active, 
			DateUpdated = GetDate(), 
			UpdatedBy = @UpdatedBy
		where SheetId = @SheetId
	select @@rowcount
END