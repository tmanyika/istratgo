﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_LeaveCalendar_GetCompany]
	@orgUnitId int,
	@calendarYear int
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	select c.LinkedId, c.HolidayDateId, l.HolidayDate, c.OrgUnitId, c.Applied, c.CreatedBy, c.DateCreated, c.UpdatedBy, c.DateUpdated, l.CalendarYear
	from imd_user_db.LeaveCalendarCompany AS c INNER JOIN
                      imd_user_db.LeaveCalendar AS l ON c.HolidayDateId = l.HolidayDateId
	where  (l.CalendarYear = @calendarYear) AND (c.OrgUnitId = @orgUnitId)
END