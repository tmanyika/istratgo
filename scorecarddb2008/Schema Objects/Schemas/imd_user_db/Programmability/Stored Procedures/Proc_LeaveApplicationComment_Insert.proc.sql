﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE imd_user_db.Proc_LeaveApplicationComment_Insert
	@ApplicationId int, 
	@DocumentTitle varchar(200), 
	@Comment varchar(1000), 
	@SupportingDoc varchar(200), 
	@Valid bit = 1, 
	@UploadedBy varchar(20)
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	insert into imd_user_db.LeaveApplicationComment(ApplicationId, DocumentTitle, Comment, SupportingDoc, Valid, UploadedBy, DateCreated, UpdatedBy, DateUpdated)
	values(@ApplicationId, @DocumentTitle, @Comment, @SupportingDoc, @Valid, @UploadedBy, getdate(), null, null)
	select @@rowcount
END