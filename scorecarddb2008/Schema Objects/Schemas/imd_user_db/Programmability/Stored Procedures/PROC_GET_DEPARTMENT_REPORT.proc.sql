﻿
create  PROCEDURE [imd_user_db].[PROC_GET_DEPARTMENT_REPORT]
 @START_DATE varchar (25) = '',-- = '2013-09-14 00:00:00.000',
 @END_DATE varchar (25)= '',-- = '2014-03-03 00:00:00.000',
 @COMPANY varchar (40) = ''
as
begin
set nocount on;

if ( @START_DATE = '')
begin
set @START_DATE = '01/01/1900'
end 


if ( @END_DATE =  '')
begin
set @END_DATE = '01/01/3000'
end 


select --e.OrgUnitId  , 
T.ORGUNIT_NAME, --SUm (s.FINAL_SCORE)as TotalScore, 
--count (distinct S.PERIOD_DATE) as DistinctPeriod,
SUm (s.FINAL_SCORE)/ count (distinct PERIOD_DATE ) as AvgPerfomance , 100 as TargetVal from 
imd_user_db.INDIVIDUAL_SCORES S inner join  imd_user_db.Employee E
on S.RESPONSIBLE_USER_ID = e.EmployeeId 
inner join  imd_user_db.COMPANY_STRUCTURE T
on e.OrgUnitId = T.ID 
inner join  imd_user_db.COMPANY_DETAIL  C
on T.COMPANY_ID = c.COMPANY_ID 
where S.PERIOD_DATE >= @START_DATE and 
S.PERIOD_DATE <= @END_DATE and c.COMPANY_NAME = @COMPANY
group by e.OrgUnitId , T.ORGUNIT_NAME 
END