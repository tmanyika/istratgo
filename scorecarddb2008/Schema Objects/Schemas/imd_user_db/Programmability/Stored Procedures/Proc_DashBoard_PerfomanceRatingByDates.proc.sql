﻿

CREATE PROCEDURE [imd_user_db].[Proc_DashBoard_PerfomanceRatingByDates] 
	@CRITERIA_TYPE_ID int,
	@SELECTED_ITEM_VALUE int,
	@ISCOMPANY bit,
	@STARTDATE datetime,
	@ENDDATE datetime
as
begin
	set nocount on;
	set transaction isolation level read uncommitted;
	
	select ItemName, cast(round(sum(ActualValue) / cast(count(ItemName) as decimal(18,2)),0) as int) as ActualValue, 
			cast(round(sum(DefinedActualValue) / cast(count(ItemName) as decimal(18,2)),0) as int) as DefinedActualValue
	from (
			select     isc.CRITERIA_TYPE_ID, isc.SELECTED_ITEM_VALUE, sum(isc.FINAL_SCORE) as ActualValue, isc.PERIOD_DATE, ISNULL(e.FirstName, '') 
							  + ' ' + ISNULL(e.MiddleName, '') + ' ' + e.LastName AS ItemName, 
							  sum(isc.FINAL_SCORE_AGREED) as DefinedActualValue
			from         imd_user_db.INDIVIDUAL_SCORES AS isc INNER JOIN
						  imd_user_db.Employee AS e ON isc.SELECTED_ITEM_VALUE = e.EmployeeId INNER JOIN 
			              imd_user_db.SUBMISSIONS AS sb ON isc.SUBMISSION_ID = sb.ID
			where (sb.[STATUS] = 40 AND isc.PARENT_ID IS NULL) AND (e.OrgUnitId = @SELECTED_ITEM_VALUE) AND (isc.CRITERIA_TYPE_ID = @CRITERIA_TYPE_ID) AND 
					  (isc.PERIOD_DATE BETWEEN @STARTDATE AND @ENDDATE)
			group by isc.CRITERIA_TYPE_ID, isc.SELECTED_ITEM_VALUE, isc.PERIOD_DATE, e.FirstName, e.MiddleName, e.LastName
		) as data
	group by ItemName
end