﻿
CREATE PROCEDURE [imd_user_db].[PROC_GET_SCORES_EXCEPTIONREPORT] 
	@OrgUnitId int,
	@ScoreDate datetime
as
begin
	set nocount on;
	set transaction isolation level read uncommitted;

	select e.EmployeeId, e.OrgUnitId, e.LastName + ' ' + ISNULL(e.MiddleName, '') + ' ' + e.FirstName AS FullName,
		   d.FinalScore, d.FinalScoreAgreed, isnull(d.StatusName,'No Score Captured') as StatusName
	from  imd_user_db.Employee AS e left outer join 
		(select FinalScore, PeriodDate, FinalScoreAgreed, EmployeeId, StatusName
		 from imd_user_db.vw_ExceptionEmpReport where (datediff(dd, @ScoreDate, PeriodDate) = 0)) as d
		 on e.EmployeeId = d.EmployeeId	
	where (e.OrgUnitId = @OrgUnitId)
	order by FullName
end