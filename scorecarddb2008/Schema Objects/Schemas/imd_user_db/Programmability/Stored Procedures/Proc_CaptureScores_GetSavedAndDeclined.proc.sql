﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_CaptureScores_GetSavedAndDeclined] 
	@CapturerId int
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	select     FirstName, MiddleName, LastName, EmailAddress, ID, SELECTED_ITEM_VALUE, CRITERIA_TYPE_ID, APPROVER_ID, [STATUS], JOB_TITLE_ID, OVERALL_COMMENT, DATE_SUBMITTED, 
                      DATE_UPDATED, CAPTURER_ID, CriteriaDescription, StatusDescription
	from       imd_user_db.vw_Submissions  
	where     ([Status] IN (41, 330)) AND (CAPTURER_ID = @CapturerId)
END