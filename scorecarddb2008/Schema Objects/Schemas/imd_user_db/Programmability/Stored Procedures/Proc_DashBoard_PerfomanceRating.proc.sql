﻿

CREATE PROCEDURE [imd_user_db].[Proc_DashBoard_PerfomanceRating] 
	@CRITERIA_TYPE_ID int,
	@SELECTED_ITEM_VALUE int,
	@ISCOMPANY bit,
	@YEAR int
as
begin
	set nocount on;
	set transaction isolation level read uncommitted;
	
	if(@ISCOMPANY = 0)
		begin
			select count(SELECTED_ITEM_VALUE) as NumOfEmployees, RatingValue
			from (
					select  isc.CRITERIA_TYPE_ID, isc.SELECTED_ITEM_VALUE, 
						  cast(round((SUM(isc.FINAL_SCORE_AGREED) / COUNT(datepart(yyyy,isc.PERIOD_DATE))),0) as int) AS RatingValue, ost.ID AS OrgUnitId, cs.COMPANY_ID
					from  imd_user_db.INDIVIDUAL_SCORES AS isc INNER JOIN
						  imd_user_db.INDIVIDUAL_GOALS AS ig ON isc.GOAL_ID = ig.ID INNER JOIN
						  imd_user_db.COMPANY_STRATEGIC AS cs ON ig.STRATEGIC_ID = cs.ID INNER JOIN
						  imd_user_db.COMPANY_STRUCTURE AS ost ON cs.COMPANY_ID = ost.COMPANY_ID INNER JOIN
						  imd_user_db.COMPANY_PERSPECTIVE AS cp ON cs.PERSPECTIVE = cp.PERSPECTIVE_ID INNER JOIN 
			              imd_user_db.SUBMISSIONS AS sb ON isc.SUBMISSION_ID = sb.ID
			        where (sb.[STATUS] = 40) AND (ost.ID = @SELECTED_ITEM_VALUE) AND (isc.CRITERIA_TYPE_ID = @CRITERIA_TYPE_ID) AND 
					(datepart(yyyy,isc.PERIOD_DATE) = @YEAR)
					group by isc.CRITERIA_TYPE_ID, isc.SELECTED_ITEM_VALUE, ost.ID, cs.COMPANY_ID, datepart(yyyy,isc.PERIOD_DATE)
				) as data
			group by RatingValue
			order by RatingValue
		end
	else
		begin
			select count(SELECTED_ITEM_VALUE) as NumOfEmployees, RatingValue
			from (
					select isc.CRITERIA_TYPE_ID, isc.SELECTED_ITEM_VALUE, 
						  cast(round((SUM(isc.FINAL_SCORE_AGREED) / COUNT(datepart(yyyy,isc.PERIOD_DATE))),0) as int) as RatingValue, cs.COMPANY_ID
					from         imd_user_db.INDIVIDUAL_SCORES AS isc INNER JOIN
								  imd_user_db.INDIVIDUAL_GOALS AS ig ON isc.GOAL_ID = ig.ID INNER JOIN
								  imd_user_db.COMPANY_STRATEGIC AS cs ON ig.STRATEGIC_ID = cs.ID INNER JOIN
								  imd_user_db.COMPANY_STRUCTURE AS ost ON cs.COMPANY_ID = ost.COMPANY_ID INNER JOIN
								  imd_user_db.COMPANY_PERSPECTIVE AS cp ON cs.PERSPECTIVE = cp.PERSPECTIVE_ID INNER JOIN 
			                      imd_user_db.SUBMISSIONS AS sb ON isc.SUBMISSION_ID = sb.ID
			        where (sb.[STATUS] = 40) AND (cs.COMPANY_ID = @SELECTED_ITEM_VALUE) AND (isc.CRITERIA_TYPE_ID = @CRITERIA_TYPE_ID) AND 
							(datepart(yyyy,isc.PERIOD_DATE) = @YEAR)
					group by isc.CRITERIA_TYPE_ID, isc.SELECTED_ITEM_VALUE, cs.COMPANY_ID, datepart(yyyy,isc.PERIOD_DATE)
				 ) as data
			group by RatingValue
			order by RatingValue
		end
end