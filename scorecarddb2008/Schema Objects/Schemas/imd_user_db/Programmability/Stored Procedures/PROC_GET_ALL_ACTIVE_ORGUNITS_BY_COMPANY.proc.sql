﻿

CREATE PROCEDURE [imd_user_db].[PROC_GET_ALL_ACTIVE_ORGUNITS_BY_COMPANY]
	@COMPANY_ID INT 
AS
begin
set nocount on;
set transaction isolation level read uncommitted;

select [ID]
      ,[COMPANY_ID]
      ,[ORGUNIT_NAME]
      ,[PARENT_ORG]
      ,[ORGUNT_TYPE]
      ,[DATE_CREATED],OWNER_ID
from [COMPANY_STRUCTURE]
where COMPANY_ID=@COMPANY_ID AND ACTIVE = 1

end