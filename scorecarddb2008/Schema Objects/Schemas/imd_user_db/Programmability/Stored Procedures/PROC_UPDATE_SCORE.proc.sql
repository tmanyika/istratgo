﻿

CREATE PROCEDURE [imd_user_db].[PROC_UPDATE_SCORE]
 @CRITERIA_TYPE_ID INT ,
 @SELECTED_ITEM_VALUE INT ,
 @GOAL_ID INT,
 @SCORES VARCHAR(250),
 @FINAL_SCORE DECIMAL(18,2),
 @SUBMISSION_ID INT,
 @PERIOD_DATE DATETIME,
 @JUSTIFICATION_COMMENT VARCHAR(4000),
 @RATINGVALUE DECIMAL(18,2),
 @FINAL_SCORE_AGREED DECIMAL(18,2),
 @EVIDENCE VARCHAR(4000),
 @ID int,
 @STATUS_ID int,
 @HAS_SUB_GOALS bit = 0,
 @PARENT_ID int = null 
AS 
begin
	set nocount on;
	
	declare @responsible_uid int;
			
	if(@CRITERIA_TYPE_ID = 33) --organisation unit
		select @responsible_uid=OWNER_ID from imd_user_db.COMPANY_STRUCTURE (nolock) where id = @SELECTED_ITEM_VALUE
	else if(@CRITERIA_TYPE_ID = 49) --Project
		select @responsible_uid=RESPONSIBLE_PERSON_ID from imd_user_db.COMPANY_PROJECT (nolock) where PROJECT_ID = @SELECTED_ITEM_VALUE
	else -- users
		set @responsible_uid = @SELECTED_ITEM_VALUE
	
	update imd_user_db.INDIVIDUAL_SCORES
	set SCORES = @SCORES, 
		FINAL_SCORE = @FINAL_SCORE, 
        RESPONSIBLE_USER_ID = @responsible_uid, 
        JUSTIFICATION_COMMENT = @JUSTIFICATION_COMMENT, 
        RATINGVALUE = @RATINGVALUE,
        --MANAGER_SCORE = @SCORES,
        --AGREED_SCORE = @SCORES,
        --FINAL_SCORE_AGREED = @FINAL_SCORE_AGREED, 
        EVIDENCE = @EVIDENCE,
        DATE_UPDATED = GetDate()
	where ID = @ID
	select @@ROWCOUNT
END