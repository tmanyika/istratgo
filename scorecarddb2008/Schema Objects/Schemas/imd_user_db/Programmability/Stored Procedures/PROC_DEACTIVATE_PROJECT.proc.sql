﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[PROC_DEACTIVATE_PROJECT]
	@PROJECT_ID int,
	@UPDATED_BY varchar(20),
	@ACTIVE bit
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE  COMPANY_PROJECT
	SET		active = @ACTIVE, UPDATE_BY = @UPDATED_BY, 
			DATE_UPDATED = getdate()
	WHERE PROJECT_ID = @PROJECT_ID
	SELECT @@rowcount
END