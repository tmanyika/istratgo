﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE imd_user_db.Proc_Welcome_GetById
	@welcomeId int
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	select WelcomeId, WelcomeSectionName, WelcomeDisplayCaption, VirtualPath, PopupPageName, Position, Active, DateCreated, CreatedBy, DateUpdate, UpdatedBy
	from imd_user_db.Welcome
	where WelcomeId = @welcomeId
END