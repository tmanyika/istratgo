﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE imd_user_db.PROC_ENVIRONMENT_CONTEXT_GET
	@COMPANY_ID INT,
	@AREA_ID INT,
	@AREA_TYPE_ID INT
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT ENV_CONTEXT_ID, COMPANY_ID, AREA_TYPE_ID, AREA_ID, ENV_CONTEXT
	FROM imd_user_db.ENVIRONMENT_CONTEXT
	WHERE COMPANY_ID = @COMPANY_ID AND AREA_TYPE_ID = @AREA_TYPE_ID AND AREA_ID = @AREA_ID
END