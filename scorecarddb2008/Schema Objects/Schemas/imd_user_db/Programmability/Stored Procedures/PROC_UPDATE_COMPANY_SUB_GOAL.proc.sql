﻿


CREATE PROCEDURE [imd_user_db].[PROC_UPDATE_COMPANY_SUB_GOAL]
@ID INT,
@STRATEGIC_ID INT,
@AREA_TYPE_ID INT,
@GOAL_DESCRIPTION VARCHAR(150),
@UNIT_MEASURE int,
@TARGET VARCHAR(150),
@WEIGHT INT,
@AREA_ID INT,
@RATIONAL VARCHAR(1000) = NULL
AS
BEGIN
	set nocount on;
	declare @rec int = 0;
	
	begin try
		update imd_user_db.INDIVIDUAL_GOALS 
		set --STRATEGIC_ID=@STRATEGIC_ID, 
			--AREA_TYPE_ID=@AREA_TYPE_ID,
			GOAL_DESCRIPTION=@GOAL_DESCRIPTION,
			UNIT_MEASURE=@UNIT_MEASURE,
			[TARGET]=@TARGET,
			[WEIGHT]=@WEIGHT, 
			--AREA_ID=@AREA_ID,
			RATIONAL = @RATIONAL,
			DATE_UPDATED = getdate()
		where ID = @ID			
		select @rec=@@rowcount
	end try
	begin catch
		select @rec = 0
	end catch
	select @rec
END