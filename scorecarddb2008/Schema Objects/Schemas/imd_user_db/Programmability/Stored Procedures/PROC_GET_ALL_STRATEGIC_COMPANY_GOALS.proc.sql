﻿
CREATE PROCEDURE [imd_user_db].[PROC_GET_ALL_STRATEGIC_COMPANY_GOALS]
@COMPANY_ID INT
AS
begin
set nocount on;
set transaction isolation level read uncommitted;

select [ID]
      ,[COMPANY_ID]    
      ,[PERSPECTIVE]
      ,[OBJECTIVE]
      ,[RATIONALE]
      ,[DATE_CREATED]
      ,[ACTIVE]
      ,[CREATEDBY]
      ,[UPDATED_BY]
      ,[DATE_UPDATED]
from [COMPANY_STRATEGIC]
where COMPANY_ID =@COMPANY_ID AND ACTIVE = 1
order by PERSPECTIVE, OBJECTIVE
end