﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_ScoreRating_GetMaximumRating] 
	@CompanyId int,
	@Active bit = 1
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	declare @ratingVal decimal(18,1);
	select @ratingVal=max(RatingValue) from imd_user_db.ScoreRating where CompanyId = @CompanyId and Active = @Active
	select isnull(@ratingVal,imd_user_db.fxStandardScoreRating())
END