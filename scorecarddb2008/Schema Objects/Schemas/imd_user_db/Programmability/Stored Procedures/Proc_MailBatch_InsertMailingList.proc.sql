﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_MailBatch_InsertMailingList]
	@batchId int,
	@Xml xml
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	declare @rAffected int, @noOfUsers int;	
	set @rAffected = 0;
	set @noOfUsers = 0;
	
	begin try
		begin transaction
			insert into imd_user_db.MailBatchUserList(BatchId, Email, EmployeeId)
			select distinct nref.value('BatchId[1]', 'int') as BatchId,
							nref.value('Email[1]', 'varchar(80)') as Email,
							nref.value('EmployeeId[1]', 'int') as EmployeeId
			from  @Xml.nodes('//List/Data') AS R(nref)
			select @rAffected=@@rowcount
			
			select @noOfUsers=count(BatchId) from imd_user_db.MailBatchUserList where BatchId = @batchId 
			update imd_user_db.MailBatch
			set IsSent = 1,
				NoOfUsers = @noOfUsers
			where  BatchId = @batchId
		commit transaction
	end try
	begin catch
		if(@@trancount > 0)
			rollback transaction
	end catch
	select @rAffected
END