﻿


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_LeaveAccrual_GetById]
	@leaveAccrualId int
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	select     l.AccrualTimeId, l.Active, l.CreatedBy, l.DateCreated, l.DateUpdated, l.UpdatedBy, l.LeaveAccrualId, l.LeaveId, l.WorkingWeekId, l.AccrualRate, l.MaximumThreshold, 
                      lat.TimeName, lat.TimeValue, w.CompanyId, w.Name, w.NoOfDays, lt.LeaveName, lt.LeaveDescription
	from         imd_user_db.LeaveAccrual AS l INNER JOIN
                      imd_user_db.LeaveAccrualTime AS lat ON l.AccrualTimeId = lat.AccrualTimeId INNER JOIN
                      imd_user_db.LeaveType AS lt ON l.LeaveId = lt.LeaveId INNER JOIN
                      imd_user_db.WorkingWeek AS w ON l.WorkingWeekId = w.WorkingWeekId
	where     (l.LeaveAccrualId = @leaveAccrualId)
END