﻿

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_EmployeeStatus_Insert]
	@statusName varchar(150), 
	@active bit, 
	@createdBy varchar(20)
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	insert into imd_user_db.EmployeeStatus( StatusName,  Active, CreatedBy, DateCreated)
	values(@statusName, @active, @createdBy, getdate())
		
	select @@rowcount
END