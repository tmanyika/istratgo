﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_LeaveCalendar_UpdateAccrual]
	@holidayDateId int,
	@orgUnitId int, 
    @apply bit, 
    @createdBy varchar(20)
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	declare @i int, @rec int, @hDate datetime, @dateCreated datetime;
	declare @limitValue decimal(18,4), @workingWeekId int, @rowC int;
	declare @empId int, @leaveId int, @accruedLeave int, @accrualDate datetime;
	 
	set @i = 1;
	set @rec = 0;
	set @rowC = 0;
	select @hDate=holidaydate, @dateCreated = dateCreated from imd_user_db.LeaveCalendar where HolidayDateId = @holidayDateId
		
	begin try	
		select row_number() over(order by e.EmployeeId asc) as rId, e.WorkingWeekId, e.EmployeeId, a.LeaveId, null as ApplicationId, @holidayDateId as HolidayDateId, 1 as LeaveDays, GETDATE() as TransactionDate, 'Holiday Date Reversal' as SourceOfTrans                      
		into #tranD
		from imd_user_db.Employee as e inner join imd_user_db.LeaveApplication as a ON e.EmployeeId = a.EmployeeId
		where (e.OrgUnitId = @orgUnitId) and (a.StatusId = 2) and (@hDate between a.StartDate and a.EndDate) 
				and (datediff(dd,a.DateCreated,@dateCreated) > 0)-- take only approved leave transactions
		select @rec=count(rId) from #tranD
		
		begin transaction
			insert into imd_user_db.LeaveCalendarCompany(HolidayDateId, OrgUnitId, Applied, CreatedBy, DateCreated)
			values(@holidayDateId, @orgUnitId, @apply, @createdBy, getdate())
			select @rowC=@@rowcount
			
			if(@rec > 0)
				begin
					--- insert leave days that are approved to reverse them
					insert into imd_user_db.LeaveTransaction(EmployeeId, LeaveId, ApplicationId, HolidayDateId, LeaveDays, TransactionDate, SourceOfTrans)
					select EmployeeId, LeaveId, ApplicationId, HolidayDateId, LeaveDays, TransactionDate, SourceOfTrans from #tranD
					
					while(@i <= @rec)
						begin
							select @empId = employeeId, @leaveId = leaveId, @accruedLeave = LeaveDays, @workingWeekId = WorkingWeekId, @accrualDate = TransactionDate from #tranD where rId = @i
							select @limitValue = MaximumThreshold from imd_user_db.LeaveAccrual with(nolock) where @leaveId = leaveId and WorkingWeekId = @workingWeekId
							if exists(select employeeId from imd_user_db.LeaveBalance where @empId = employeeId and @leaveId = leaveId)
								update imd_user_db.LeaveBalance
								set	Accumulated = Accumulated + @accruedLeave,
									Balance = Balance + @accruedLeave
								where @empId = employeeId and @leaveId = leaveId
							else
								insert into imd_user_db.LeaveBalance(EmployeeId, LeaveId, Accumulated, Balance, DaysTaken, LimitValue, DateLastUpdated)
								values(@empId, @leaveId, @accruedLeave, @accruedLeave, 0, @limitValue, @accrualDate)
							
							set @i = @i + 1;
						end
				end
		commit transaction
		
		drop table #tranD
	end try
	begin catch
		if (@@trancount > 0)
			begin
				set @rowC = 0;
				rollback transaction
			end
	end catch
	select @rowC
END