﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_WorkingWeek_Insert]
	@companyId int, 
	@name varchar(200), 
	@noofdays int,
	@active bit, 
	@createdBy varchar(20)
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	insert into imd_user_db.WorkingWeek(CompanyId, Name, NoOfDays, Active, CreatedBy, DateCreated)
	values(@companyId, @name, @noofdays, @active, @createdBy, getdate())
		
	select @@rowcount
END