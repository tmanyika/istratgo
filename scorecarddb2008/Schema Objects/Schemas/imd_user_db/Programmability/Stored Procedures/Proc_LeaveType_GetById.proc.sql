﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_LeaveType_GetById]
	@leaveId int
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	select   LeaveId, CompanyId, LeaveName, LeaveDescription, Active, CreatedBy, DateCreated, DateUpdated, UpdatedBy
	from     imd_user_db.LeaveType
	where     (@leaveId = LeaveId)
END