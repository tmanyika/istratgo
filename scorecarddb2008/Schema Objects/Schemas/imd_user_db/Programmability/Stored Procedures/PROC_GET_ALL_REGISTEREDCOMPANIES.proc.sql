﻿
CREATE PROCEDURE [imd_user_db].[PROC_GET_ALL_REGISTEREDCOMPANIES]
AS
begin
set nocount on;
set transaction isolation level read uncommitted;
select  cd.COMPANY_ID, cd.ACCOUNT_NO, cd.COMPANY_NAME, cd.REGISTRATION_NO, cd.INDUSTRY, cd.COUNTRY, cd.VAT_NO, cd.ADDRESS, cd.CONTACT_NO, 
        cd.ACTIVE, CONVERT(VARCHAR(11),cd.DATE_REGISTERED,106) as DATE_REGISTERED, cd.PAID, u.TYPE_NAME AS INDUSTRY_NAME, c.TYPE_NAME AS COUNTRY_NAME
from  imd_user_db.COMPANY_DETAIL AS cd WITH (nolock) LEFT OUTER JOIN
      imd_user_db.UTILITIES AS u WITH (nolock) ON cd.INDUSTRY = u.ID LEFT OUTER JOIN
      imd_user_db.UTILITIES AS c WITH (nolock) ON cd.COUNTRY = u.ID
order by cd.COMPANY_NAME
end