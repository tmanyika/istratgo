﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_BulkImport_CleanupCompanyData]
	@CompanyId int
AS
BEGIN
	set nocount on;	
	declare @rows int;
	
	begin try
		set @rows = 1;
		--Org Unit
		delete from imd_user_db.BulkOrganisationUnit where CompanyId = @CompanyId
		--Jo Titles
		delete from imd_user_db.BulkJobTitle where CompanyId = @CompanyId
		--Projects
		delete from  imd_user_db.BulkProject where CompanyId = @CompanyId
		--Employee Data
		delete from imd_user_db.BulkEmployee  where CompanyId = @CompanyId
		--Performance Measures
		delete from imd_user_db.BulkPerformanceMeasure where CompanyId = @CompanyId
	end try
	begin catch
		set @rows = 0;
		insert into imd_user_db.ProcError(ErrorNo, ErrorSeverity, ErrorState, ErrorProcedure, ErrorLine, ErrorMessage, DateCreated)
		select error_number() AS ErrorNumber, error_severity() AS ErrorSeverity, error_state() AS ErrorState, 
					error_procedure() AS ErrorProcedure, error_line() AS ErrorLine, error_message() AS ErrorMessage, 
					getdate() as DateCreated
	end catch
	
	select @rows
END