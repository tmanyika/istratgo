﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE imd_user_db.PROC_DELETE_SUBMITTED_SCORE
	@ID int
AS
BEGIN
	set nocount on;
	set xact_abort on;
	declare @rec int = 0;
	
	begin try
		begin tran
			delete from imd_user_db.INDIVIDUAL_SCORES where SUBMISSION_ID = @ID
			delete from imd_user_db.SUBMISSIONS where ID = @ID
			select @rec=@@rowcount
		commit tran
	end try
	begin catch
		set @rec = 0
		if(@@trancount > 0)
			rollback tran
	end catch
	select @rec
END