﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_BulkSheet_GetAll]
	
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	select row_number() over(order by SheetName asc) as RowIdx, SheetId, SheetName, DbTableName, SheetDescription, SheetNotes, Active, OrderNumber, CreatedBy, DateCreated, UpdatedBy, DateUpdated
	from imd_user_db.BulkSheet
	order by SheetName
END