﻿


CREATE PROCEDURE [imd_user_db].[PROC_GET_USERBYLOGIN]
	@loginId varchar(20)
AS
begin
	SET TRANSACTION isolation level READ uncommitted;
    SET nocount ON;
	
	SELECT     ua.ID, ua.LOGIN_ID, ua.[PASSWORD], ua.ORG_ID, ua.JOB_TITLE_ID, ua.NAME, ua.EMAIL_ADDRESS, ua.ACTIVE, ua.REPORTS_TO, ua.USER_TYPE, 
						  cs.ORGUNIT_NAME
	FROM         USER_ADMIN AS ua left OUTER JOIN
						  COMPANY_STRUCTURE AS cs ON ua.ORG_ID = cs.ID
	WHERE     (ua.LOGIN_ID = @loginId)
END