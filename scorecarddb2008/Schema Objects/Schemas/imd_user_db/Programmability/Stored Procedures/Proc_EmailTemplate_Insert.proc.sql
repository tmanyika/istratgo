﻿


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_EmailTemplate_Insert]
	@templateId int = null,
	@mailName varchar(800),
	@mailSubject varchar(800),
	@mailContent nvarchar(max), 
	@active bit, 
	@isMaster bit,
	@createdBy varchar(20)
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	insert into imd_user_db.MailTemplate(MailName, TemplateId, MailSubject, MailContent,  Active, IsMasterTemplate, CreatedBy, DateCreated)
	values(@mailName, @templateId, @mailSubject, @mailContent, @active, @isMaster, @createdBy, getdate())
		
	select @@rowcount
END