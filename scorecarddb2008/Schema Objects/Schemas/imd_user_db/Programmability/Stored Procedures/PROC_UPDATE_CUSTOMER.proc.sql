﻿

CREATE PROCEDURE [imd_user_db].[PROC_UPDATE_CUSTOMER]
 @ACCOUNT_NO INT
,@EMAIL_ADDRESS  VARCHAR(250)
,@FIRST_NAME  VARCHAR(250)
,@LAST_NAME  VARCHAR(250)
,@CELL_NUMBER NCHAR(20)
,@DATE_OF_BIRTH DATETIME
AS
UPDATE [CUSTOMER]
SET
          
           [EMAIL_ADDRESS]=@EMAIL_ADDRESS
           ,[FIRST_NAME]=@FIRST_NAME
           ,[LAST_NAME]=@LAST_NAME
           ,[CELL_NUMBER]=@CELL_NUMBER
           ,[DATE_OF_BIRTH]=@DATE_OF_BIRTH
  
WHERE ACCOUNT_NO=@ACCOUNT_NO