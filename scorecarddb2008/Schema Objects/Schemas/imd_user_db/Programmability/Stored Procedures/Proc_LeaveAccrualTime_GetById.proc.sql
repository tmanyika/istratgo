﻿

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_LeaveAccrualTime_GetById]
	@accrualTimeId int
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	select   AccrualTimeId,  TimeName, TimeValue,  Active, CreatedBy, DateCreated, DateUpdated, UpdatedBy
	from     imd_user_db.LeaveAccrualTime
	where     (@accrualTimeId = AccrualTimeId)
END