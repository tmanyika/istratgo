﻿CREATE PROCEDURE [imd_user_db].[PROC_GET_SCORES_USEDDATES_BYORGUNIT] 
	@compId INT,
	@isCompId BIT
as
begin
	set nocount on;
	set transaction isolation level read uncommitted;
	
	if(@isCompId = 1)
		select distinct PERIOD_DATE from imd_user_db.vw_ScoreReportDatesForCompany
		where CompanyId = @compId
	else
		select distinct PERIOD_DATE from imd_user_db.vw_ScoreReportDatesForCompany
		where OrgUnitId = @compId
end