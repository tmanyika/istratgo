﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_BulkImport_LinkInformation]
	@CompanyId int,
	@CreatedBy varchar(20)
AS
BEGIN
	set nocount on;
	set xact_abort on;
	
	declare @currDate datetime,
			@rows int, @rows1 int;
	
	begin try
		set @rows = 0;
		set @rows1 = 0;
		set @currDate = getdate();	
		--Get Empployee Ids for Projects
		select e.EmployeeId, e.EmailAddress 
		into #owner
		from imd_user_db.Employee as e with(nolock) inner join imd_user_db.bulkProject as d with(nolock)
		on e.EmailAddress = d.ProjectOwnerEmail
		where d.CompanyId = @CompanyId
		
		select e.EmployeeId, e.EmailAddress 
		into #orgowner
		from imd_user_db.Employee as e with(nolock) inner join imd_user_db.bulkOrganisationUnit as d with(nolock)
		on e.EmailAddress = d.OrganisationUnitOwnerEmail
		where d.CompanyId = @CompanyId
		
		begin tran	
			-- Update Project Owners
			update imd_user_db.COMPANY_PROJECT
			set COMPANY_PROJECT.RESPONSIBLE_PERSON_ID = e.EmployeeId				
			from imd_user_db.COMPANY_PROJECT cp inner join imd_user_db.bulkProject bp
				on cp.PROJECT_NAME = bp.ProjectName and cp.COMPANY_ID = bp.CompanyId inner join [#owner] e
				on bp.ProjectOwnerEmail = e.EmailAddress
			where cp.COMPANY_ID = @CompanyId
			select @rows1=@@rowcount 
			set @rows = @rows1 + @rows
		commit tran
		
		begin tran
			--Update Org Unit Owners
			update imd_user_db.COMPANY_STRUCTURE
			set COMPANY_STRUCTURE.OWNER_ID = e.EmployeeId
			from imd_user_db.COMPANY_STRUCTURE cp inner join imd_user_db.bulkOrganisationUnit bp
				on cp.ORGUNIT_NAME = bp.OrganisationUnitName and cp.COMPANY_ID = bp.CompanyId inner join [#orgowner] e
				on bp.OrganisationUnitOwnerEmail = e.EmailAddress
			where cp.COMPANY_ID = @CompanyId
		    select @rows1=@@rowcount 
			set @rows = @rows1 + @rows
		commit tran
		
		begin tran
		--Cleanup Org Units Linking
			update imd_user_db.COMPANY_STRUCTURE
			set	COMPANY_STRUCTURE.PARENT_ORG = 0 
			from imd_user_db.COMPANY_STRUCTURE
			where ID = PARENT_ORG and COMPANY_ID = @CompanyId
			select @rows1=@@rowcount 
			set @rows = @rows1 + @rows
		commit tran
		
		begin tran
			--update users in case there may be unlinked record
			update imd_user_db.Employee
			set Employee.JobTitleId =case when e.JobTitleId is null then j.ID 
										else e.JobTitleId end
			from imd_user_db.Employee e inner join imd_user_db.BulkEmployee be
			on e.EmailAddress = be.EmailAddress inner join imd_user_db.COMPANY_JOB_TITLES j
			on be.JobTitle = j.NAME and be.CompanyId = j.COMPANY_ID inner join imd_user_db.COMPANY_STRUCTURE cs
			on e.OrgUnitId = cs.Id
			where (j.COMPANY_ID = @CompanyId and cs.COMPANY_ID = @CompanyId and e.JobTitleId is null)
			select @rows1=@@rowcount 
			set @rows = @rows1 + @rows		
		commit tran
		
		drop table [#owner]
		drop table [#orgowner]
	end try
	begin catch
		set @rows = 0
		if(@@trancount > 0)
			rollback tran
		
		insert into imd_user_db.ProcError(ErrorNo, ErrorSeverity, ErrorState, ErrorProcedure, ErrorLine, ErrorMessage, DateCreated)
		select error_number() AS ErrorNumber, error_severity() AS ErrorSeverity, error_state() AS ErrorState, 
						error_procedure() AS ErrorProcedure, error_line() AS ErrorLine, error_message() AS ErrorMessage, 
						getdate() as DateCreated
	end catch
	select @rows
END