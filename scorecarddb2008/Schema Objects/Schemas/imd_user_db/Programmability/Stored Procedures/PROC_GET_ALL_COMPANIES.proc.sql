﻿
CREATE PROCEDURE [imd_user_db].[PROC_GET_ALL_COMPANIES]
AS
begin
set nocount on;
set transaction isolation level read uncommitted;
select     COMPANY_ID, ACCOUNT_NO, COMPANY_NAME, REGISTRATION_NO, INDUSTRY, COUNTRY, VAT_NO, [ADDRESS], CONTACT_NO, ACTIVE, DATE_REGISTERED, 
                      PAID
from         imd_user_db.COMPANY_DETAIL
order by COMPANY_NAME
end