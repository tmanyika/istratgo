﻿


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_BulkImport_Employees]
	@CompanyId int,
	@CreatedBy varchar(20)
AS
BEGIN
set nocount on;
set xact_abort on;
set ansi_warnings off;

--set dateformat mdy;

declare  @i int, 
		 @rec int,
		 @defRoleId int,
		 @currDate datetime,
		 @LineManagerEmployeeId int,
		 @OrgUnitId int, 
		 @FirstName varchar(25), 
		 @MiddleName varchar(25), 
		 @LastName varchar(25), 
		 @EmailAddress varchar(80),
		 @JobTitleId int, 
		 @StatusId int, 
		 @EmployeeNo varchar(15), 
		 @WorkingWeekId int, 
		 @Cellphone varchar(15),
		 @BusTelephone varchar(15), 
		 @Active bit, 
		 @UserName varchar(120), 
		 @Pass varchar(250),
		 @RoleId int,
		 @DateOfAppointment datetime,
		 @defOrgUnitId int,
		 @EmployeeId int;
		 
begin try
	set @i = 1;
	set @rec = 0;
	set @currDate = getdate();
	set @Active = 1;
	set @Pass = 'xEjyijxckpfHjIrhvdsa';	
	set @defRoleId = 46;
	set @WorkingWeekId = null;
	set @LineManagerEmployeeId = null;
	
	select top 1 @defOrgUnitId=Id 
	from imd_user_db.COMPANY_STRUCTURE with(nolock) 
	where COMPANY_ID = @CompanyId
	
	--Trim white spaces on the employee information
	update imd_user_db.bulkEmployee 
	set FirstName = imd_user_db.PropCaseDelDoubleSpace(FirstName),
		MiddleName = imd_user_db.PropCaseDelDoubleSpace(MiddleName),
		LastName = imd_user_db.PropCaseDelDoubleSpace(LastName),
		EmailAddress = lower(rtrim(ltrim(EmailAddress))),
		EmployeeNumber = upper(rtrim(ltrim(EmployeeNumber))),
		Cellphone = imd_user_db.RemoveDoubleSpace(Cellphone),
		LandlineTelephone = imd_user_db.RemoveDoubleSpace(LandlineTelephone),			
		JobTitle = imd_user_db.PropCaseDelDoubleSpace(JobTitle),
		OrganisationUnit = imd_user_db.PropCaseDelDoubleSpace(OrganisationUnit),
		LineManagerEmail = lower(rtrim(ltrim(LineManagerEmail))),
		UserName = lower(isnull(rtrim(ltrim(UserName)),rtrim(ltrim(EmailAddress)))),
		SystemRole = isnull(rtrim(ltrim(SystemRole)),'General User Access Level'),
		DateOfAppointment = case when isdate(rtrim(ltrim(DateOfAppointment))) = 1 then rtrim(ltrim(DateOfAppointment)) else null end
	where CompanyId = @CompanyId

    --Delete invalid data and Rebuild All Table Indexes
	delete from imd_user_db.bulkEmployee where (EmailAddress is null or EmailAddress = '')
	alter index all on imd_user_db.bulkEmployee rebuild with (fillfactor = 80)
	
	--Add new job titles if any
	begin tran
		insert into imd_user_db.COMPANY_JOB_TITLES(NAME,COMPANY_ID,ACTIVE,CREATEDBY,DATECREATED)
		select distinct JobTitle, CompanyId, 1 as Active, @CreatedBy as createdBy, @currDate as DateCreated
		from imd_user_db.bulkEmployee with(nolock)
		where CompanyId = @CompanyId and 
						  JobTitle not in (
												select rtrim(ltrim(NAME)) 
												from imd_user_db.COMPANY_JOB_TITLES (nolock)
												where COMPANY_ID = @CompanyId
										  )
	commit tran
	
	--Link with existing data
	select Row_Number() over(order by FirstName asc) as RowIdx, EmployeeId, FirstName, MiddleName, LastName, EmailAddress, 
			DateOfAppointment, EmployeeNumber, Cellphone, LandlineTelephone, JobTitle, OrganisationUnit, LineManagerEmail, 
                 case when UserName IS NULL OR UserName = '' then EmailAddress else UserName end as UserName, 
                 SystemRole, CompanyId, JobTitleId, OrgUnitId, RoleId
	into [#eData]
	from  imd_user_db.vw_bulkEmployee with(nolock)
	where (CompanyId = @CompanyId)
	
	--Get Record Count
	select @rec=count(RowIdx) from [#eData] with(nolock)		
	--Insert new employees
	while(@i <= @rec)
		begin
			select  @FirstName=FirstName, @MiddleName=MiddleName, @LastName=LastName, @EmailAddress=EmailAddress, 
					@DateOfAppointment=DateOfAppointment, @EmployeeNo=EmployeeNumber, @Cellphone=Cellphone, 
					@BusTelephone=LandlineTelephone, @UserName=UserName, 
					@EmployeeId=EmployeeId, @JobTitleId=JobTitleId, @OrgUnitId=isnull(OrgUnitId,@defOrgUnitId), @RoleId=isnull(RoleId,@defRoleId)
			from [#eData] with(nolock)
			where RowIdx = @i
			
			exec [imd_user_db].[Proc_BulkImport_EmployeeInsert]
								@LineManagerEmployeeId,
								@OrgUnitId,
								@FirstName,
								@MiddleName,
								@LastName,
								@EmailAddress,
								@JobTitleId,
								@StatusId,
								@EmployeeNo,
								@WorkingWeekId,
								@Cellphone,
								@BusTelephone,
								@CreatedBy,
								@Active,
								@UserName,
								@Pass,
								@RoleId,
								@DateOfAppointment,
								@EmployeeId
			set @i = @i + 1
		end		
		drop table [#eData]
		
		--Assign Line Manager
		select LineManagerEmployeeId, LineManagerEmail, EmailAddress, CompanyId
		into #linemng
		from imd_user_db.vw_BulkLineManagerList as v with(nolock)
		where CompanyId = @CompanyId
		
		begin tran
			update imd_user_db.Employee  
			set Employee.LineManagerEmployeeId = l.LineManagerEmployeeId
			from imd_user_db.Employee e inner join [#linemng] l on e.EmailAddress = l.EmailAddress  
			where (l.CompanyId = @CompanyId)
			select @i=@@rowcount
		commit tran
		drop table [#linemng]
		--End of assigning Line Manager 
		
		--Assign To Orgunits
		select OrgUnitId, EmailAddress, CompanyId
		into [#eorgunit]
		from vw_BulkEmployeeOrgUnit with(nolock)
		where CompanyId = @CompanyId
		
		begin tran
			update imd_user_db.Employee  
			set Employee.OrgUnitId = l.OrgUnitId
			from imd_user_db.Employee e inner join [#eorgunit] l on e.EmailAddress = l.EmailAddress  
			where (l.CompanyId = @CompanyId and l.OrgUnitId is not null)
		commit tran
		drop table [#eorgunit]
		--End of Assigning To Orgunits
end try
begin catch
	set @i = 0;
	if(@@trancount > 0)
			rollback tran
			
	insert into imd_user_db.ProcError(ErrorNo, ErrorSeverity, ErrorState, ErrorProcedure, ErrorLine, ErrorMessage, DateCreated)
	select error_number() AS ErrorNumber, error_severity() AS ErrorSeverity, error_state() AS ErrorState, 
					error_procedure() AS ErrorProcedure, error_line() AS ErrorLine, error_message() AS ErrorMessage, 
					getdate() as DateCreated	
end catch

select @i
END