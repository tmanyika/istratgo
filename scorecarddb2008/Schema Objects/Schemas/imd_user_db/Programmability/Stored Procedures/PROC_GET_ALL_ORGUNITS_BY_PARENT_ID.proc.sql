﻿

CREATE PROCEDURE [imd_user_db].[PROC_GET_ALL_ORGUNITS_BY_PARENT_ID] 
@PARENT_ORG INT 
AS
begin
set nocount on;
set transaction isolation level read uncommitted;
 
SELECT     cs.ID, cs.COMPANY_ID, cs.ORGUNIT_NAME, cs.PARENT_ORG, cs.ORGUNT_TYPE, cs.DATE_CREATED, cs.OWNER_ID, e.FirstName, e.MiddleName, 
                      e.LastName
FROM         imd_user_db.COMPANY_STRUCTURE AS cs LEFT OUTER JOIN
                      imd_user_db.Employee AS e ON cs.OWNER_ID = e.EmployeeId
WHERE     (cs.PARENT_ORG = @PARENT_ORG) and (cs.ACTIVE = 1)
order by cs.ORGUNIT_NAME
end