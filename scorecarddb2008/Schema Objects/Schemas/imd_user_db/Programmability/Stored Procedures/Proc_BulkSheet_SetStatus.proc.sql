﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_BulkSheet_SetStatus]
	@SheetId int,
	@Active bit,
	@UpdatedBy datetime
AS
BEGIN
	set nocount on;
	
	update imd_user_db.BulkSheet
	set Active = @Active,
		UpdatedBy = @UpdatedBy,
		DateUpdated = getdate()
	where SheetId = @SheetId
	select @@rowcount
END