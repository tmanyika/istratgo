﻿

CREATE PROCEDURE [imd_user_db].[PROC_GET_ALL_COMPANY_GOALS_BY_AREA_AND_TYPEID] 
@COMPANY_ID INT,
@AREA_TYPE_ID INT,
@AREA_ID INT
AS
begin 
set nocount on;
set transaction isolation level read uncommitted;

select     ig.ID, cs.COMPANY_ID, ig.STRATEGIC_ID, ig.AREA_TYPE_ID, ig.GOAL_DESCRIPTION, ig.UNIT_MEASURE, ig.TARGET, ig.WEIGHT, ig.DATE_CREATED, ig.AREA_ID, ig.PERIOD_DATE, 
                      ig.ENV_CONTEXT_ID, ig.RATIONAL, ec.ENV_CONTEXT, '' AS SCORES, '' AS JUSTIFICATION_COMMENT, '' AS EVIDENCE, ig.PARENT_ID, ig.HAS_SUB_GOALS
from         imd_user_db.INDIVIDUAL_GOALS AS ig INNER JOIN
                      imd_user_db.COMPANY_STRATEGIC AS cs ON ig.STRATEGIC_ID = cs.ID LEFT OUTER JOIN
                      imd_user_db.ENVIRONMENT_CONTEXT AS ec ON ig.ENV_CONTEXT_ID = ec.ENV_CONTEXT_ID
where     (cs.COMPANY_ID = @COMPANY_ID) AND (ig.ACTIVE = 1) AND (ig.AREA_TYPE_ID = @AREA_TYPE_ID) AND (ig.AREA_ID = @AREA_ID)
order by ig.GOAL_DESCRIPTION
end