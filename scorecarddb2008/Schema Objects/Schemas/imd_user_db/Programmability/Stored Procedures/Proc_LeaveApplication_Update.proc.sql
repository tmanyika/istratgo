﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_LeaveApplication_Update]
	@ApplicationId int,
	@EmployeeId int, 
	@LeaveId int,
    @LeaveNotes varchar(600), 
    @StatusId int, 
    @StartDate datetime, 
    @EndDate datetime, 
    @NoOfLeaveDays int,
    @SubmittedEmployeeId int,
    @RejectReason varchar(1000)    
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	update imd_user_db.LeaveApplication
	set EmployeeId = @EmployeeId, 
		LeaveId = @LeaveId, 
		LeaveNotes = @LeaveNotes, 
		StatusId = @StatusId, 
		StartDate = @StartDate, 
		EndDate = @EndDate, 
		NoOfLeaveDays = @NoOfLeaveDays, 
		SubmittedEmployeeId = @SubmittedEmployeeId, 
        DateUpdated = getdate(),
        RejectReason = case when @RejectReason = '' then RejectReason else @RejectReason end
	where ApplicationId = @ApplicationId
	select @@rowcount
END