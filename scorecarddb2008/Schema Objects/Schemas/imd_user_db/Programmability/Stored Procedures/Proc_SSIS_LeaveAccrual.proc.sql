﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_SSIS_LeaveAccrual] 

AS
BEGIN
	
set nocount on;
set transaction isolation level read uncommitted;

declare @accrualDate datetime, @i int, @rec int;
declare @empId int, @leaveId int, @accruedLeave decimal(18,4), @limitValue decimal(18,4);

set @i = 1;
set @accrualDate = getdate();

select  e.EmployeeId, la.LeaveId, la.AccrualRate as LeaveDays, @accrualDate as transactionDate, 'Leave Accrual' as sourceOfTrans, la.MaximumThreshold, row_number() over(order by employeeId asc) as rId
into #trans
from  imd_user_db.LeaveAccrual AS la INNER JOIN
                      imd_user_db.LeaveAccrualTime AS lt ON la.AccrualTimeId = lt.AccrualTimeId INNER JOIN
                      imd_user_db.Employee AS e ON la.WorkingWeekId = e.WorkingWeekId
where (la.Active = 1)

select @rec=count(rId) from #trans

begin try
	begin transaction
		insert into imd_user_db.LeaveTransaction(EmployeeId,LeaveId,LeaveDays,TransactionDate,SourceOfTrans)
		select EmployeeId,LeaveId,LeaveDays,TransactionDate,SourceOfTrans from #trans
		
		while(@i <= @rec)
			begin				
				select @empId = employeeId, @leaveId = leaveId, @accruedLeave = LeaveDays, @limitValue = MaximumThreshold from #trans where rId = @i
				if exists(select employeeId from imd_user_db.LeaveBalance where @empId = employeeId and @leaveId = leaveId)
					update imd_user_db.LeaveBalance
					set Accumulated = Accumulated + @accruedLeave,
						Balance = Balance + @accruedLeave
					where @empId = employeeId and @leaveId = leaveId
				else
					insert into imd_user_db.LeaveBalance(EmployeeId, LeaveId, Accumulated, Balance, DaysTaken, LimitValue, DateLastUpdated)
					values(@empId, @leaveId, @accruedLeave, @accruedLeave, 0, @limitValue, @accrualDate)
				set @i = @i + 1;
			end
	commit transaction
end try
begin catch
	if(@@trancount > 0)
		rollback transaction
	
	-- record errors into the ProcError table
	insert into imd_user_db.ProcError( ErrorNo, ErrorSeverity, ErrorState, ErrorProcedure, ErrorLine, ErrorMessage, DateCreated)	
	select
        ERROR_NUMBER() AS ErrorNumber
        ,ERROR_SEVERITY() AS ErrorSeverity
        ,ERROR_STATE() AS ErrorState
        ,ERROR_PROCEDURE() AS ErrorProcedure
        ,ERROR_LINE() AS ErrorLine
        ,ERROR_MESSAGE() AS ErrorMessage
        ,GETDATE() as DateCreated
end catch

drop table #trans
END