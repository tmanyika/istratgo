﻿
CREATE PROCEDURE [imd_user_db].[PROC_GET_SCORES_BY_OBJECT_TOTALS]
@SUBMISSION_ID INT
AS
SELECT     SUM(isc.FINAL_SCORE) AS SUM_FINAL_SCORE, SUM(ig.[WEIGHT]) AS SUM_WEIGHT, SUM(isc.FINAL_SCORE) / SUM(ig.[WEIGHT]) * 100 AS PERCENTAGE
FROM         imd_user_db.INDIVIDUAL_SCORES AS isc INNER JOIN
              imd_user_db.INDIVIDUAL_GOALS AS ig ON isc.GOAL_ID = ig.ID INNER JOIN
              imd_user_db.SUBMISSIONS AS s ON isc.SUBMISSION_ID = s.ID
WHERE     (isc.SUBMISSION_ID = @SUBMISSION_ID) AND (s.[STATUS] = 40)