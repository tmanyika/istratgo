﻿


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_BulkImport_PerformanceMeasures] 
	@CompanyId int,
	@CreatedBy varchar(20)
AS
BEGIN
	set nocount on;
	set xact_abort on;
	
	declare @projAreaId int,
			@jobAreaId int,
			@orgUnitAreaId int,
			@rowsAffected int,
			@result varchar(6000),
			@currDate datetime;
			
	begin try
	 set @jobAreaId = 32;
	 set @orgUnitAreaId = 33;
	 set @projAreaId = 49;
	 set @rowsAffected = 0;
	 set @currDate = getdate();
	 set @result = '';
	 
	 -- Trim white spaces KFA (Key Focus Areas --
	 update imd_user_db.bulkPerformanceMeasure
	 set KeyFocusArea = imd_user_db.RemoveDoubleSpace(KeyFocusArea),
		KeyFocusAreaDescription = imd_user_db.RemoveDoubleSpace(KeyFocusAreaDescription),
		StrategicObjective = imd_user_db.RemoveDoubleSpace(StrategicObjective),
		StrategicObjectiveRationale = imd_user_db.RemoveDoubleSpace(StrategicObjectiveRationale),
		PerformanceMeasure = imd_user_db.RemoveDoubleSpace(PerformanceMeasure),
		HasSubPerformanceMeasures = rtrim(ltrim(HasSubPerformanceMeasures)),
		ParentPerformanceMeasure = imd_user_db.RemoveDoubleSpace(ParentPerformanceMeasure),
		AreaOfMeasureType = imd_user_db.RemoveDoubleSpace(AreaOfMeasureType),
		AreaOfMeasure = imd_user_db.RemoveDoubleSpace(AreaOfMeasure),
		UnitOfMeasure =  imd_user_db.RemoveDoubleSpace(UnitOfMeasure),
		[TARGET] = rtrim(ltrim([TARGET])),
	    [WEIGHT] = isnull(rtrim(ltrim([WEIGHT])),0)
	 where CompanyId = @CompanyId
	 
	 --Delete invalid data and Rebuild All Table Indexes
	 delete from imd_user_db.bulkPerformanceMeasure 
	 where (KeyFocusArea is null and (StrategicObjective is null or PerformanceMeasure is null))
	 alter index all on imd_user_db.bulkPerformanceMeasure rebuild with (fillfactor = 80)
	 
	 --Add Missing Job Titles Just in Case
	 exec  [imd_user_db].[Proc_BulkImport_JobTitles] @CompanyId, @CreatedBy
	 
	 --Add KFA
	 select distinct CompanyId, KeyFocusArea, KeyFocusAreaDescription
	 into #kfa
	 from imd_user_db.bulkPerformanceMeasure with(nolock)
	 where CompanyId = @CompanyId and ( 
										KeyFocusArea not in (	select ltrim(rtrim(PERSPECTIVE_NAME))
																from imd_user_db.COMPANY_PERSPECTIVE with(nolock)
																where COMPANY_ID = @CompanyId
															)
									   )
	 begin tran
	   insert into imd_user_db.COMPANY_PERSPECTIVE(COMPANY_ID, PERSPECTIVE_NAME, PERSPECTIVE_DESC, ACTIVE, CREATED_BY, DATE_CREATED)
	   select CompanyId, KeyFocusArea, KeyFocusAreaDescription, 1 as Active, @CreatedBy as CreatedBy, @currDate as DateCreated
	   from [#kfa] with(nolock)
	   select @rowsAffected=@@rowcount
	   if(@rowsAffected > 0)
		set @result = @result + '- Key Focus Areas saved successfully.\r\n'
	   else
		set @result = @result + '- There are no new Key Focus Areas to be added.\r\n'
	 commit tran	 
	 drop table [#kfa]
	 --End of Adding KFA
	 
	 --Add Strategic Objectives
	 begin tran
	   insert into imd_user_db.COMPANY_STRATEGIC(COMPANY_ID, PERSPECTIVE, OBJECTIVE, RATIONALE, ACTIVE, DATE_CREATED, CREATEDBY)
	   select CompanyId, KeyFocusAreaId, StrategicObjective, StrategicObjectiveRationale, 1 as Active, @currDate as DateCreated, @CreatedBy as CreatedBy
	   from imd_user_db.vw_BulkObjectivesToInsert with(nolock)
	   where CompanyId = @CompanyId
	   select @rowsAffected=@@rowcount
	    if(@rowsAffected > 0)
			set @result = @result + '- Strategic Objectives saved successfully.\r\n'
	    else
			set @result = @result + '- There are no new Strategic Objectives to add.\r\n'
	 commit tran
	 --End of Adding Strategic Objectives
	 
	 --Start of Adding All Goals
	 begin tran --Add Job Titles
		insert into imd_user_db.INDIVIDUAL_GOALS(STRATEGIC_ID, AREA_TYPE_ID, AREA_ID, UNIT_MEASURE, HAS_SUB_GOALS, [TARGET], [WEIGHT], DATE_CREATED, GOAL_DESCRIPTION, ACTIVE, CREATEDBY)
		select StrategicObjectiveId, AreaOfMeasureTypeId, AreaId, UnitOfMeasureId, 0 as HAS_SUB_GOALS, [TARGET], [WEIGHT], @currDate as DateCreated, PerformanceMeasure, 1 as Active, @CreatedBy as CreatedBy
		from imd_user_db.vw_BulkRoleMeasuresToInsert with(nolock)
		where CompanyId = @CompanyId and AreaOfMeasureTypeId = @jobAreaId
		select @rowsAffected=@@rowcount
		if(@rowsAffected > 0)
			set @result = @result + '- Job Titles performance measures saved successfully.\r\n'
	    else
			set @result = @result + '- There are no new Job Titles performance measure to add.\r\n'
	 commit tran
	
	begin tran --Add Org Units
		insert into imd_user_db.INDIVIDUAL_GOALS(STRATEGIC_ID, AREA_TYPE_ID, AREA_ID, UNIT_MEASURE, HAS_SUB_GOALS, [TARGET], [WEIGHT], DATE_CREATED, GOAL_DESCRIPTION, ACTIVE, CREATEDBY)
		select StrategicObjectiveId, AreaOfMeasureTypeId, AreaId, UnitOfMeasureId, 0 as HAS_SUB_GOALS, [TARGET], [WEIGHT], @currDate as DateCreated, PerformanceMeasure, 1 as Active, @CreatedBy as CreatedBy
		from imd_user_db.vw_BulkOrgUnitMeasuresToInsert with(nolock)
		where CompanyId = @CompanyId and AreaOfMeasureTypeId = @orgUnitAreaId
		select @rowsAffected=@@rowcount
		if(@rowsAffected > 0)
			set @result = @result + '- Organisational Units performance measures saved successfully.\r\n'
	    else
			set @result = @result + '- There are no new Organisational Units performance measures to add.\r\n'
	commit tran
	
	begin tran --Add Projects
		insert into imd_user_db.INDIVIDUAL_GOALS(STRATEGIC_ID, AREA_TYPE_ID, AREA_ID, UNIT_MEASURE, HAS_SUB_GOALS, [TARGET], [WEIGHT], DATE_CREATED, GOAL_DESCRIPTION, ACTIVE, CREATEDBY)
		select StrategicObjectiveId, AreaOfMeasureTypeId, AreaId, UnitOfMeasureId, 0 as HAS_SUB_GOALS, [TARGET], [WEIGHT], @currDate as DateCreated, PerformanceMeasure, 1 as Active, @CreatedBy as CreatedBy
		from imd_user_db.vw_BulkProjectMeasuresToInsert with(nolock)
		where CompanyId = @CompanyId and AreaOfMeasureTypeId = @projAreaId
		select @rowsAffected=@@rowcount
		if(@rowsAffected > 0)
			set @result = @result + '- Projects performance measures saved successfully.\r\n'
	    else
			set @result = @result + '- There are no new Projects performance measures to add.\r\n'
	commit tran
	----End of Adding All Goals
	
	----Update Roles Parent Goals on Performance Measures	 
	select ParentId, SubGoalId, CompanyId, AreaOfMeasureTypeId
	into [#prnt]
	from imd_user_db.vw_BulkRoleLinking with(nolock)
	where CompanyId = @CompanyId and AreaOfMeasureTypeId = @jobAreaId
	
	begin tran
		update imd_user_db.INDIVIDUAL_GOALS
		set INDIVIDUAL_GOALS.PARENT_ID = pm.ParentId,
			INDIVIDUAL_GOALS.HAS_SUB_GOALS = 0
		from imd_user_db.INDIVIDUAL_GOALS ig inner join [#prnt] pm
		on ig.ID = pm.SubGoalId	
			
		select @rowsAffected=@@rowcount		
		if(@rowsAffected > 0)
			set @result = @result + '- Job Titles sub performance measures linked to parent successfully.\r\n'
	   	else
	   		set @result = @result + '- There are no Job Titles sub performance measures to be linked to parent.\r\n'
		
		update imd_user_db.INDIVIDUAL_GOALS
		set INDIVIDUAL_GOALS.HAS_SUB_GOALS = 1,
			INDIVIDUAL_GOALS.PARENT_ID = null
		from imd_user_db.INDIVIDUAL_GOALS ig inner join [#prnt] pm
		on ig.ID = pm.ParentId	
		select @rowsAffected=@@rowcount
	commit tran	
	drop table [#prnt]
	--End of Linking Role Sub Measures
	
	----Update Project Parent Goals on Performance Measures	 
	select ParentId, SubGoalId, CompanyId, AreaOfMeasureTypeId
	into [#prjnt]
	from imd_user_db.vw_BulkProjectLinking with(nolock)
	where CompanyId = @CompanyId and AreaOfMeasureTypeId = @projAreaId
	
	begin tran
		update imd_user_db.INDIVIDUAL_GOALS
		set INDIVIDUAL_GOALS.PARENT_ID = pm.ParentId,
			INDIVIDUAL_GOALS.HAS_SUB_GOALS = 0
		from imd_user_db.INDIVIDUAL_GOALS ig inner join [#prjnt] pm
		on ig.ID = pm.SubGoalId
		select @rowsAffected=@@rowcount
		if(@rowsAffected > 0)
			set @result = @result + '- Projects sub performance measures linked to parent successfully.\r\n'
		else
	   		set @result = @result + '- There are no Projects sub performance measures to be linked to parent.\r\n'
	   		
		update imd_user_db.INDIVIDUAL_GOALS
		set INDIVIDUAL_GOALS.HAS_SUB_GOALS = 1,
		INDIVIDUAL_GOALS.PARENT_ID = null
		from imd_user_db.INDIVIDUAL_GOALS ig inner join [#prjnt] pm
		on ig.ID = pm.ParentId	
		select @rowsAffected=@@rowcount
	commit tran	
	drop table [#prjnt]
	--End of Linking Project Sub Measures
	
	----Update Org Unit Parent Goals on Performance Measures	 
	select ParentId, SubGoalId, CompanyId, AreaOfMeasureTypeId
	into [#orgunt]
	from imd_user_db.vw_BulkOrgUnitLinking with(nolock)
	where CompanyId = @CompanyId and AreaOfMeasureTypeId = @orgUnitAreaId
	
	begin tran
		update imd_user_db.INDIVIDUAL_GOALS
		set INDIVIDUAL_GOALS.PARENT_ID = pm.ParentId,
			INDIVIDUAL_GOALS.HAS_SUB_GOALS = 0
		from imd_user_db.INDIVIDUAL_GOALS ig inner join [#orgunt] pm
		on ig.ID = pm.SubGoalId 
		select @rowsAffected=@@rowcount
		
		if(@rowsAffected > 0)
			set @result = @result + '- Organisational Units sub performance measures linked to parent successfully.\r\n'
		else
	   		set @result = @result + '- There are no Organisational Units sub performance measures to be linked to parent.\r\n'
	   			
		update imd_user_db.INDIVIDUAL_GOALS
		set INDIVIDUAL_GOALS.HAS_SUB_GOALS = 1,
		    INDIVIDUAL_GOALS.PARENT_ID = null
		from imd_user_db.INDIVIDUAL_GOALS ig inner join [#orgunt] pm
		on ig.ID = pm.ParentId	
		select @rowsAffected=@@rowcount
	commit tran	
	drop table [#orgunt]
	--End of Linking Org Unit Sub Measures
	--Cleanup of Parent ID & Sub Goals
	select ParentId
	into [#cleanup]
	from imd_user_db.vw_BulkPerformanceMeasuresCleanup 
	where CompanyId = @CompanyId
	
	begin tran
		update imd_user_db.INDIVIDUAL_GOALS
		set INDIVIDUAL_GOALS.HAS_SUB_GOALS = 1,
			INDIVIDUAL_GOALS.PARENT_ID = NULL
		from imd_user_db.INDIVIDUAL_GOALS ig inner join 
		     [#cleanup] c on ig.ID = c.ParentId
		select @rowsAffected=@@rowcount
		
		update imd_user_db.INDIVIDUAL_GOALS
		set INDIVIDUAL_GOALS.HAS_SUB_GOALS = 0
		from imd_user_db.INDIVIDUAL_GOALS ig inner join 
		     [#cleanup] c on ig.PARENT_ID = c.ParentId
		select @rowsAffected=@@rowcount
	commit tran
	drop table [#cleanup]
	--End of Cleanup of Parent ID & Sub Goals
	----End of Updating Parent Goals on Performance Measure
	end try
	begin catch
		set @rowsAffected = 0
		if(@@trancount > 0)
			rollback tran
		
		select @result = @result + '\r\n\r\n - System Technical Error: ' + error_message()
		insert into imd_user_db.ProcError(ErrorNo, ErrorSeverity, ErrorState, ErrorProcedure, ErrorLine, ErrorMessage, DateCreated)
		select error_number() AS ErrorNumber, error_severity() AS ErrorSeverity, error_state() AS ErrorState, 
						error_procedure() AS ErrorProcedure, error_line() AS ErrorLine, error_message() AS ErrorMessage, 
						getdate() as DateCreated
	end catch
	
	select @result
END