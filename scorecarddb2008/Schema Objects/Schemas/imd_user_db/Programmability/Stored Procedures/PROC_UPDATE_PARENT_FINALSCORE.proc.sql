﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[PROC_UPDATE_PARENT_FINALSCORE]
	@ParentId int
AS
BEGIN
	set nocount on;
	declare @finalscore decimal(18,2),
	        @finalagreedscore decimal(18,2),
	        @rows int;
	        
	set @rows = 0
	if exists(select ID from imd_user_db.INDIVIDUAL_SCORES (nolock) where PARENT_ID = @ParentId)
		begin       
			select @finalscore = avg(FINAL_SCORE), @finalagreedscore = avg(FINAL_SCORE_AGREED)
			from  imd_user_db.INDIVIDUAL_SCORES
			where PARENT_ID = @ParentId
			
			update imd_user_db.INDIVIDUAL_SCORES
			set FINAL_SCORE = @finalscore,
				FINAL_SCORE_AGREED = @finalagreedscore
			where ID = @ParentId
			select @rows=@@rowcount 
		end
	select @rows
END