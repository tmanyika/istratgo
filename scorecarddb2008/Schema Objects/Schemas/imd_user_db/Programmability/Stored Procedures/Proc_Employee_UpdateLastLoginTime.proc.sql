﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_Employee_UpdateLastLoginTime] 
	@employeeId int
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	update imd_user_db.EmployeeLogin
	set LastLoginTime = getdate()
	where employeeId = @employeeId
	select @@rowcount
END