﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_LeaveApplication_UpdateStatus] 
	@applicationId int, 
	@statusId int, 
    @approvedByEmployeeId int,
    @reason varchar(1000)
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	declare @rec int, @approvedId int, @employeeId int;
	declare @leaveId int, @leaveDays decimal(18,4);
	
	set @rec = 0;
	set @approvedId = 2;
	
	if(@statusId = @approvedId)
		select @employeeId=employeeId, @leaveId=leaveId, @leaveDays=NoOfLeaveDays
		from imd_user_db.LeaveApplication where ApplicationId = @applicationId
	
	begin try
		begin transaction
			update imd_user_db.LeaveApplication 
			set StatusId = @statusId,
				ApprovedByEmployeeId = @approvedByEmployeeId,
				DateUpdated = getdate(),
				RejectReason = @reason
			where (ApplicationId = @applicationId)
			select @rec=@@rowcount
			
			if(@statusId = @approvedId)-- add leave transaction 
				begin
					insert into imd_user_db.LeaveTransaction(EmployeeId, LeaveId, ApplicationId, LeaveDays, TransactionDate, SourceOfTrans)
					values(@employeeId, @leaveId, @applicationId, @leaveDays, getdate(), 'Leave Application')
					if exists (select employeeId from imd_user_db.LeaveBalance where EmployeeId = @employeeId and LeaveId = @leaveId)
						update imd_user_db.LeaveBalance 
						set Balance = Balance - @leaveDays,
							DateLastUpdated = getdate(),
							DaysTaken = DaysTaken + @leaveDays
						where EmployeeId = @employeeId and LeaveId = @leaveId
					else 
						insert into imd_user_db.LeaveBalance(EmployeeId,LeaveId,Accumulated,Balance,DaysTaken,LimitValue,DateLastUpdated)
						select @employeeId,@leaveId,0,(@leaveDays * -1) as balance,@leaveDays,la.MaximumThreshold,getdate()
						from imd_user_db.LeaveAccrual as la inner join 
							 imd_user_db.Employee as e on la.WorkingWeekId = e.WorkingWeekId
						where @employeeId = EmployeeId and la.LeaveId = @leaveId
				end
		commit transaction
	end try
	begin catch
		if(@@trancount > 0)
			begin
				set @rec = 0 
				rollback transaction
			end
	end catch
	select @rec
END