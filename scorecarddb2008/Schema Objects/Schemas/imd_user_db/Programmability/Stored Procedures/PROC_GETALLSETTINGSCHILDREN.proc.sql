﻿
CREATE PROCEDURE [imd_user_db].[PROC_GETALLSETTINGSCHILDREN]
@PARENTID INT
AS
SELECT ID, PARENT_ID, [TYPE_NAME] , DESCRIPTION, ACTIVE FROM UTILITIES
WHERE PARENT_ID=@PARENTID
SET NOCOUNT ON
RETURN