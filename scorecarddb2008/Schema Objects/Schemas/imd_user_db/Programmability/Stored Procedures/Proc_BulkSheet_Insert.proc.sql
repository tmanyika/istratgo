﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_BulkSheet_Insert]
	@SheetName varchar(150), 
	@DbTableName varchar(50), 
	@SheetDescription varchar(1000), 
	@SheetNotes varchar(800), 
	@Active bit = 1, 
	@CreatedBy varchar(50)
AS
BEGIN
	set nocount on;
	if not(exists(select SheetName from imd_user_db.BulkSheet where SheetName = @SheetName and Active = 1))
		insert into imd_user_db.BulkSheet(SheetName, DbTableName, SheetDescription, SheetNotes, Active, CreatedBy, DateCreated, UpdatedBy, DateUpdated)
		values(@SheetName, @DbTableName, @SheetDescription, @SheetNotes, @Active, @CreatedBy, GetDate(), null, null)
	select @@rowcount
END