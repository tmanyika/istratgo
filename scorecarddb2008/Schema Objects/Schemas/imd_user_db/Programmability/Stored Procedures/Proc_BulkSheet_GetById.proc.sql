﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_BulkSheet_GetById]
	@SheetId int
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	select 1 as RowIdx, SheetId, SheetName, DbTableName, SheetDescription, SheetNotes, Active, OrderNumber, CreatedBy, DateCreated, UpdatedBy, DateUpdated
	from imd_user_db.BulkSheet
	where SheetId = @SheetId
END