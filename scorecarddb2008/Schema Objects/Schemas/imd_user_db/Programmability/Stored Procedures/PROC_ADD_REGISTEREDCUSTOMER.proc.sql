﻿

CREATE PROCEDURE [imd_user_db].[PROC_ADD_REGISTEREDCUSTOMER] 
	 @USER_NAME varchar(20)
	,@SETUP_TYPE int
	,@PASSWORD varchar(250)
	,@EMAIL_ADDRESS  varchar(250)
	,@FIRST_NAME  varchar(250)
	,@LAST_NAME  varchar(250)
	,@CELL_NUMBER varchar(20) = NULL
	,@DATE_OF_BIRTH datetime = NULL
	,@COMPANY_NAME  varchar(150)
	,@JOB_TITLE_NAME varchar(50)
	,@USER_TYPE int
	,@CountryId int
as

begin
set nocount on;
set xact_abort on;

declare @ACCOUNT_NO int
declare @COMPANY_ID int
declare @ORG_ID int
declare @JOBTILE_ID int
declare @USER_ID int
declare @emailExist bit
declare @userNameExist bit

	begin try
		set @COMPANY_ID = 0
		set @emailExist = 0
		set @userNameExist = 0

		if exists(select EmailAddress from imd_user_db.Employee where EmailAddress = @EMAIL_ADDRESS)
			set @emailExist = 1
		if exists(select UserName from imd_user_db.EmployeeLogin where UserName = @USER_NAME)
			set @userNameExist = 1
		if(@userNameExist = 0 and @emailExist = 0)
			begin
				begin tran
					insert into imd_user_db.[CUSTOMER]
							   (
							   [EMAIL_ADDRESS]
							   ,[FIRST_NAME]
							   ,[LAST_NAME]
							   ,[CELL_NUMBER]          
							   ,[DATE_OF_BIRTH]
							   )
					values
							   (          
									  @EMAIL_ADDRESS,
									  @FIRST_NAME,
									  @LAST_NAME,
									  @CELL_NUMBER,                 
									  @DATE_OF_BIRTH
								)

					select @ACCOUNT_NO = scope_identity()
					insert into imd_user_db.COMPANY_DETAIL
							   (
								   [ACCOUNT_NO]
								   ,[COMPANY_NAME]
								   ,[COUNTRY]
								   ,[CONTACT_NO]
							   )
					values
							   (
									@ACCOUNT_NO
								   ,@COMPANY_NAME  
								   ,@CountryId
								   ,@CELL_NUMBER         
								)
					            
					select @COMPANY_ID = scope_identity()
					insert into imd_user_db.[COMPANY_STRUCTURE]
							   (
								[COMPANY_ID]
							   ,[ORGUNIT_NAME]
							   ,[PARENT_ORG]
							   ,[ORGUNT_TYPE]
							   )
					values
							   (
								@COMPANY_ID
							   ,@COMPANY_NAME
							   ,0
							   ,@SETUP_TYPE
							   )

					select @ORG_ID = scope_identity()
					insert into imd_user_db.COMPANY_JOB_TITLES
					(
					  COMPANY_ID,
					  [NAME]
					)
					values
					(
					  @COMPANY_ID,
					  @JOB_TITLE_NAME
					)
					select @JOBTILE_ID = scope_identity()			
					-- Create user profile			
					insert into imd_user_db.Employee(LineManagerEmployeeId, OrgUnitId, FirstName, MiddleName, LastName, EmailAddress, JobTitleId, StatusId, EmployeeNo, WorkingWeekId, Cellphone, BusTelephone, CreatedBy, DateCreated, DateUpdated, UpdatedBy)
					values (null, @ORG_ID, @FIRST_NAME, null, @LAST_NAME, @EMAIL_ADDRESS, @JOBTILE_ID, 2, null, null, null, null, @USER_NAME, getdate(), null, null)
					select @USER_ID = scope_identity()

					insert into imd_user_db.EmployeeLogin(EmployeeId, UserName, Pass, RoleId, Active, DateUpdated)
					values  (@USER_ID, @USER_NAME, @PASSWORD, @USER_TYPE, 1, getdate())
					
					--create permissions
					insert into imd_user_db.MENU_ACCESS(MENU_ID,USER_TYPE_ID,COMPANY_ID,DATE_CREATED)
					select MENU_ID,@USER_TYPE,@COMPANY_ID,getdate() 
					from imd_user_db.APP_MENU with(nolock) where (MENU_ID <> 6 or PARENT_MENU_ID <> 6)

					--update USER_ADMIN set REPORTS_TO = @USER_ID where ID = @USER_ID
					update imd_user_db.Employee set LineManagerEmployeeId =  @USER_ID where EmployeeId =  @USER_ID
					update imd_user_db.COMPANY_STRUCTURE set OWNER_ID = @USER_ID  where ID =@ORG_ID
					
					--inserting default job titles 
					insert into  imd_user_db.COMPANY_JOB_TITLES(COMPANY_ID, NAME, ACTIVE, ISADMIN, CREATEDBY, DATECREATED)
					select @COMPANY_ID as COMPANY_ID, t.NAME, t.ACTIVE, t.ISADMIN, 'SYSTEM' AS CREATEDBY, GETDATE() AS DATECREATED
					from  imd_user_db.COMPANY_JOB_TITLES as t with (nolock)
					where (t.COMPANY_ID = 146 and t.ID <> 206)
					
					--inserting default perspectives
					insert into imd_user_db.COMPANY_PERSPECTIVE(COMPANY_ID, PERSPECTIVE_NAME, PERSPECTIVE_DESC, ACTIVE, CREATED_BY, DATE_CREATED, DATE_UPDATED, UPDATE_BY)
					select @COMPANY_ID as company_id, p.PERSPECTIVE_NAME, p.PERSPECTIVE_DESC, p.ACTIVE, 'SYSTEM' as CREATED_BY, getdate() as DATE_CREATED, null as DATE_UPDATED, null as UPDATE_BY 
					from imd_user_db.COMPANY_PERSPECTIVE as p with (nolock)
					where p.COMPANY_ID = 146
					
					--inserting default leave types
					insert into imd_user_db.LeaveType(CompanyId, LeaveName, LeaveDescription, Active, CreatedBy, DateCreated, DateUpdated, UpdatedBy)
					select @COMPANY_ID as Company_Id, l.LeaveName, l.LeaveDescription, l.Active, 'SYSTEM' as CreatedBy, getdate() as DateCreated, null as DateUpdated, null as UpdatedBy
					from  imd_user_db.LeaveType as l (nolock)
					where l.CompanyId = 146
				
					--inserting default working weeks
					insert into imd_user_db.WorkingWeek(CompanyId, Name, NoOfDays, Active, CreatedBy, DateCreated, DateUpdated, UpdatedBy)
					select  @COMPANY_ID as Company_Id, w.Name, w.NoOfDays, w.Active, w.CreatedBy, getdate() as DateCreated, null as DateUpdated, null as UpdatedBy
					from imd_user_db.WorkingWeek as w (nolock) 
					where w.CompanyId = 146 
					
					---Welcome Page
					insert into imd_user_db.WelcomeState(WelcomeId, EmployeeId, ShowAgain, DateLastRequested)
					select WelcomeId, @USER_ID as EmployeeId, cast(1 as bit) as ShowAgain, getdate() as DateLastRequested
					from imd_user_db.Welcome with (nolock) 
					where Active = 1
				commit tran
			end
	end try
	begin catch
		if (@@trancount > 0)
			rollback tran
			
		insert into imd_user_db.ProcError(ErrorNo, ErrorSeverity, ErrorState, ErrorProcedure, ErrorLine, ErrorMessage, DateCreated)
		select error_number() AS ErrorNumber, error_severity() AS ErrorSeverity, error_state() AS ErrorState, 
						error_procedure() AS ErrorProcedure, error_line() AS ErrorLine, error_message() AS ErrorMessage, 
						getdate() as DateCreated	
	end catch

select @COMPANY_ID as companyId, @userNameExist as uname, @emailExist as email
end