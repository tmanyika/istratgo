﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_BulkImport_InsertMain]
	@CompanyId int,
	@CreatedBy varchar(20)
AS
BEGIN
	set nocount on;	
	--Org Unit
	exec imd_user_db.Proc_BulkImport_OrgUnits @CompanyId, @CreatedBy
	--Jo Titles
	exec imd_user_db.Proc_BulkImport_JobTitles @CompanyId, @CreatedBy
	--Projects
	exec Proc_BulkImport_Projects @CompanyId, @CreatedBy
	--Employee Data
	exec imd_user_db.Proc_BulkImport_Employees @CompanyId, @CreatedBy
	--Linking Up Project Owners & Org Units Owners & Users 
	exec imd_user_db.Proc_BulkImport_LinkInformation @CompanyId, @CreatedBy	
	--Performance Measures
	exec imd_user_db.Proc_BulkImport_PerformanceMeasures @CompanyId, @CreatedBy
END