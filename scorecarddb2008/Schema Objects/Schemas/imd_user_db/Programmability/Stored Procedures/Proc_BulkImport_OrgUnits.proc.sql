﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_BulkImport_OrgUnits]
	@CompanyId int,
	@CreatedBy varchar(20)
AS
BEGIN
	set nocount on;
	set xact_abort on;
	declare @pOrgUnitId int,
			@OrgAreaId int,
			@deforgUnitTypeId int;
	
	begin try	
	   set @OrgAreaId = 33;
	   set @deforgUnitTypeId = 10;
	   
	   --Trim White Spaces First before bulk insert
	   update imd_user_db.bulkOrganisationUnit
	   set OrganisationUnitName = imd_user_db.PropCaseDelDoubleSpace(OrganisationUnitName), 
	       ParentOrganisationUnitName = imd_user_db.PropCaseDelDoubleSpace(ParentOrganisationUnitName), 
	       OrganisationUnitType = imd_user_db.PropCaseDelDoubleSpace(OrganisationUnitType), 
	       OrganisationUnitOwnerEmail = lower(rtrim(ltrim(OrganisationUnitOwnerEmail))) 
	   where CompanyId = @CompanyId
	   
	   --Delete invalid data and Rebuild All Table Indexes
	   delete from imd_user_db.bulkOrganisationUnit where (OrganisationUnitName is null or OrganisationUnitName = '')
	   alter index all on imd_user_db.bulkOrganisationUnit rebuild with (fillfactor = 80)
		 
       --Get Default Org Unit Id
       select top 1 @pOrgUnitId=Id 
       from imd_user_db.COMPANY_STRUCTURE with(nolock)
       where (PARENT_ORG = 0 or PARENT_ORG = ID) and COMPANY_ID = @CompanyId
       order by ID
       
       --Get Only Org Units that are not in the system
	   select OrganisationUnitName, ParentOrganisationUnitName, OrganisationUnitType, OrganisationUnitOwnerEmail, CompanyId, OwnerId, OrgUnitId,
	          getdate() as DateCreated
       into #orgU
       from imd_user_db.vw_BulkOrgStructure
	   where CompanyId = @CompanyId and 
			 (
				OrganisationUnitName not in (
												select ORGUNIT_NAME 
												from imd_user_db.COMPANY_STRUCTURE with(nolock)
												where COMPANY_ID = @CompanyId
											 )
			 )
		--Insert Org Units Data
		begin tran
			insert into imd_user_db.COMPANY_STRUCTURE(COMPANY_ID, ORGUNIT_NAME, PARENT_ORG, ORGUNT_TYPE, DATE_CREATED, OWNER_ID, DATE_REGISTERED, ACTIVE, CREATED_BY)
			select @CompanyId as COMPANY_ID, OrganisationUnitName, null as ParentOrgUnitId, OrgUnitId, DateCreated, OwnerId, DateCreated, 1 as Active, @CreatedBy as CreatedBy  
			from #orgU with(nolock)
		commit tran
				
		--Get Additional Data
		select AreaOfMeasure as Orgunit_Name, @pOrgUnitId as PARENT_ORG, @deforgUnitTypeId as ORGUNT_TYPE, getdate() as DateRegistered, 1 as Active,
		       @CreatedBy as CreatedBy 
		into #aOrgU
		from imd_user_db.vw_BulkAdditionalData with(nolock)
		where (AreaId = @OrgAreaId and CompanyId = @CompanyId) and 
			   (
				 AreaOfMeasure not in (
										select rtrim(ltrim(ORGUNIT_NAME)) 
										from imd_user_db.COMPANY_STRUCTURE with(nolock)
										where COMPANY_ID = @CompanyId
									   )
			   )
		--Insert Additional Org Units Data
		begin tran
			insert into imd_user_db.COMPANY_STRUCTURE(COMPANY_ID, ORGUNIT_NAME, PARENT_ORG, ORGUNT_TYPE, DATE_CREATED, DATE_REGISTERED, ACTIVE, CREATED_BY)
			select @CompanyId as COMPANY_ID, Orgunit_Name, PARENT_ORG, ORGUNT_TYPE, DateRegistered, DateRegistered, Active, CreatedBy  
			from [#aOrgU] with(nolock)
		commit tran
		
		select ParentId, OrganisationUnitName, ParentOrganisationUnitName, OrganisationUnitOwnerEmail, COMPANY_ID, CompanyId
		into #bou
		from imd_user_db.vw_BulkOrgStructureParent with(nolock)
		where (COMPANY_ID = @CompanyId) AND (CompanyId = @CompanyId)
		
		--Update Parent Org Unit Id field
		begin tran
			update imd_user_db.COMPANY_STRUCTURE
			set COMPANY_STRUCTURE.PARENT_ORG = case when cs.ID = cs.PARENT_ORG then 0
											   else c.ParentId end,
			    COMPANY_STRUCTURE.UPDATED_BY = @CreatedBy,
			    COMPANY_STRUCTURE.DATE_UPDATED = getdate()
			from imd_user_db.COMPANY_STRUCTURE cs inner join #bou c
			on cs.ORGUNIT_NAME = c.OrganisationUnitName
			where (cs.COMPANY_ID = @CompanyId and c.CompanyId = @CompanyId
			      and c.ParentOrganisationUnitName is not null)
		commit tran
		
		drop table [#aOrgU]
		drop table [#orgU]
		drop table [#bou]
	end try
	begin catch
		set @OrgAreaId = 0
		if(@@trancount > 0)
			rollback tran
		
		insert into imd_user_db.ProcError(ErrorNo, ErrorSeverity, ErrorState, ErrorProcedure, ErrorLine, ErrorMessage, DateCreated)
		select error_number() AS ErrorNumber, error_severity() AS ErrorSeverity, error_state() AS ErrorState, 
						error_procedure() AS ErrorProcedure, error_line() AS ErrorLine, error_message() AS ErrorMessage, 
						getdate() as DateCreated
	end catch
	
	select @OrgAreaId
END