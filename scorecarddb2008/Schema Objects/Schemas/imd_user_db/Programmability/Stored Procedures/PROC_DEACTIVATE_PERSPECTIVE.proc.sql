﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[PROC_DEACTIVATE_PERSPECTIVE]
	@PERSPECTIVE_ID int,
	@UPDATED_BY varchar(20),
	@ACTIVE bit
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE  COMPANY_PERSPECTIVE
	SET		ACTIVE = @ACTIVE, UPDATE_BY = @UPDATED_BY, 
			DATE_UPDATED = getdate()
	WHERE PERSPECTIVE_ID = @PERSPECTIVE_ID
	SELECT @@rowcount
END