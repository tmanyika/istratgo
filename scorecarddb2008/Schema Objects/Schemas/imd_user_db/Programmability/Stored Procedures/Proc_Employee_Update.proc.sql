﻿

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_Employee_Update] 
	 @EmployeeId int,
	 @LineManagerEmployeeId int = null,
     @OrgUnitId int, 
     @FirstName varchar(25), 
     @MiddleName varchar(25), 
     @LastName varchar(25), 
     @EmailAddress varchar(80),
     @JobTitleId int = null, 
     @StatusId int = null, 
     @EmployeeNo varchar(15), 
     @WorkingWeekId int = null, 
     @Cellphone varchar(15),
     @BusTelephone varchar(15),    
     @UpdatedBy varchar(20),
     @Active bit = 1,
     @RoleId int,
     @DateOfAppointment datetime = null
AS
BEGIN
	set nocount on;
	set xact_abort on;
	
	declare @FullName varchar(100), 
			@updated int,
			@EmailExist bit,
			@EndDate datetime,
			@PrevPosId int;
			
	set @updated = 0;
	set @EmailExist = 0;
	set @EndDate = null;
	set @FullName = @FirstName + ' ' + @MiddleName + ' ' + @LastName;
	
	if (exists(select emailaddress from imd_user_db.Employee 
				where EmailAddress = @EmailAddress and EmployeeId <> @EmployeeId))
		set @EmailExist = 1
	
	if(@EmailExist = 0)	
		begin	
			begin try
				select @PrevPosId=JobTitleId 
				from imd_user_db.Employee with(nolock) 
				where EmployeeId = @EmployeeId
				begin tran
					update imd_user_db.Employee
					set LineManagerEmployeeId = @LineManagerEmployeeId, OrgUnitId = @OrgUnitId, FirstName = @FirstName, MiddleName = @MiddleName, LastName = @LastName, 
						EmailAddress = @EmailAddress, JobTitleId = @JobTitleId, StatusId = @StatusId, EmployeeNo = @EmployeeNo, WorkingWeekId = @WorkingWeekId, 
						Cellphone = @Cellphone, BusTelephone = @BusTelephone, UpdatedBy = @UpdatedBy, DateUpdated = getdate(),
						DateOfAppointment = @DateOfAppointment
					where EmployeeId = @EmployeeId
					select @updated=@@ROWCOUNT
			        
			        update imd_user_db.EmployeeLogin
			        set Active = @Active, RoleId = @RoleId
			        where EmployeeId = @EmployeeId
				commit tran	
				if(@DateOfAppointment is not null)
					exec imd_user_db.Proc_EmploymentHist_Insert	@EmployeeId, @PrevPosId, @JobTitleId, @DateOfAppointment, @EndDate, @UpdatedBy   
			end try
			begin catch
				if(@@trancount > 0)
					begin
						set @updated = 0
						rollback tran
					end
				insert into imd_user_db.ProcError(ErrorNo, ErrorSeverity, ErrorState, ErrorProcedure, ErrorLine, ErrorMessage, DateCreated)
				select error_number() AS ErrorNumber, error_severity() AS ErrorSeverity, error_state() AS ErrorState, error_procedure() AS ErrorProcedure, error_line() AS ErrorLine, error_message() AS ErrorMessage, getdate() as DateCreated	
			end catch
		end 
	select @updated
END