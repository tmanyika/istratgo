﻿




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_MailServer_Update]
	@accountId int,
	@accountName varchar(200),
	@sqlProfileName varchar(100),
	@user_Name varchar(100),
	@pin varchar(200),
	@smtpServer varchar(100),
	@portN int,
	@active bit, 
	@updatedBy varchar(20)
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	open master key decryption by password = '3891@Macd'
	open symmetric key FieldSymmetricKey
	decryption by certificate FieldCertificate;
	
	declare @recCount int, @encPin varbinary(128);
	
	set @recCount = 0;
	set @encPin = imd_user_db.fxEncrypt(@pin);
	
	begin try
		begin transaction
				--Delete profile and account if they exists
			   exec msdb.dbo.sysmail_delete_account_sp @account_name = @accountName
			   exec msdb.dbo.sysmail_delete_profile_sp @profile_name = @sqlProfileName, @force_delete = 1
			   
			   --Create database mail account.
			   exec msdb.dbo.sysmail_add_account_sp
				 @Account_name = @accountName
			   , @description = @accountName
			   , @email_address = @user_Name
			   , @replyto_address = @user_Name
			   , @display_name = @accountName
			   , @mailserver_name = @smtpServer
			   , @port = @portN
			   , @username = @user_Name
			   , @password = @pin
			   
			   --Create global mail profile.
			   exec msdb.dbo.sysmail_add_profile_sp
				 @profile_name = @sqlProfileName
			   , @description = @accountName
			   
			   --Add the account to the profile.
			   exec msdb.dbo.sysmail_add_profileaccount_sp
				 @profile_name = @sqlProfileName
			   , @Account_name = @accountName
			   , @sequence_number = 1
			   
			   --grant access to the profile to all users in the msdb database	   
			   exec msdb.dbo.sysmail_add_principalprofile_sp
				 @profile_name = @sqlProfileName
				,@principal_name = 'imd_user_db'
				,@is_default = 0
			
			update  imd_user_db.MailServer
			set     AccountName = @accountName, 
					SqlProfileName = @sqlProfileName,
					UserName = @user_Name,
					Port = @portN,
					PIN = @encPin,
					SmtpServer = @smtpServer,
					Active = @active, 
					UpdatedBy = @updatedBy, 
					DateUpdated = GETDATE()
			where   (AccountMailId = @accountId)
			select @recCount=@@rowcount	
		commit transaction
	end try
	begin catch
		if(@@TRANCOUNT > 0)
			begin
				rollback transaction
				set @recCount = -1
			end
	end catch
	select @recCount
END