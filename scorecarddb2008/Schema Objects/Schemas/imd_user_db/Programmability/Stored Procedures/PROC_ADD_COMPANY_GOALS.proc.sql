﻿




CREATE PROCEDURE [imd_user_db].[PROC_ADD_COMPANY_GOALS]
@STRATEGIC_ID INT,
@AREA_TYPE_ID INT,
@GOAL_DESCRIPTION VARCHAR(150),
@UNIT_MEASURE int,
@TARGET VARCHAR(150),
@WEIGHT int,
@AREA_ID INT,
@ENV_CONTEXT VARCHAR(1000) = NULL,
@COMPANY_ID INT = NULL,
@CONTEXTID INT = NULL,
@RATIONAL VARCHAR(1000) = NULL
AS
BEGIN
	set nocount on;
	set xact_abort on;
	
	begin try	
		
		if(@CONTEXTID is null)
			select @CONTEXTID=ENV_CONTEXT_ID 
			from imd_user_db.ENVIRONMENT_CONTEXT 
			where COMPANY_ID = @COMPANY_ID and AREA_TYPE_ID = @AREA_TYPE_ID and AREA_ID = @AREA_ID and lower(ENV_CONTEXT) = lower(@ENV_CONTEXT)
		
		begin tran
			if((@CONTEXTID is null) and (@ENV_CONTEXT is not null))
				begin
					insert into imd_user_db.ENVIRONMENT_CONTEXT(COMPANY_ID,AREA_TYPE_ID,AREA_ID,ENV_CONTEXT,DATE_CREATED)
					values(@COMPANY_ID,@AREA_TYPE_ID,@AREA_ID,@ENV_CONTEXT,getdate())
					select @contextId=scope_identity()
				end
			else
				begin				
					update imd_user_db.ENVIRONMENT_CONTEXT
					set ENV_CONTEXT = @ENV_CONTEXT,
						DATE_UPDATED = getdate()
					where ENV_CONTEXT_ID = @CONTEXTID
				end
				
			insert into imd_user_db.INDIVIDUAL_GOALS
			(
				 STRATEGIC_ID,
				 AREA_TYPE_ID,
				 GOAL_DESCRIPTION,
				 UNIT_MEASURE,
				 [TARGET],
				 [WEIGHT],
				 AREA_ID,
				 ENV_CONTEXT_ID,
				 RATIONAL
			)
			values
			( 
				@STRATEGIC_ID,
				@AREA_TYPE_ID,
				@GOAL_DESCRIPTION,
				@UNIT_MEASURE,
				@TARGET,
				@WEIGHT,
				@AREA_ID,
				@CONTEXTID,
				@RATIONAL
			)
		commit tran
	end try
	begin catch
		if(@@trancount > 0)
			rollback tran
	end catch
	select @contextId
END