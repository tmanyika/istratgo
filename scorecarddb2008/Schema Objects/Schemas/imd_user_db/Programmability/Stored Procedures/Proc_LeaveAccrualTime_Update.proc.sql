﻿

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_LeaveAccrualTime_Update]
	@accrualTimeId int,
	@timeName varchar(100), 
	@timeValue smallint,
	@active bit, 
	@updatedBy varchar(20)
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	update  imd_user_db.LeaveAccrualTime
	set     TimeName = @timeName,
			TimeValue = @timeValue,
			Active = @active, 
			UpdatedBy = @updatedBy, 
			DateUpdated = GETDATE()
	where   (AccrualTimeId = @accrualTimeId)
		
	select @@rowcount
END