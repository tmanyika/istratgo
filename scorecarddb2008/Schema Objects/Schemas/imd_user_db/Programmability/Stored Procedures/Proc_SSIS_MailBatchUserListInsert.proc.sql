﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE  imd_user_db.Proc_SSIS_MailBatchUserListInsert
	@mbatchId int,
	@mempId int,
	@memail varchar(80),
	@moutboxId int
AS
BEGIN

set nocount on;
set transaction isolation level read uncommitted;

declare @count int;

begin try
	if not (exists(select employeeid from imd_user_db.MailBatchUserList where batchId = @mbatchId and employeeId = @mempId))
		insert into imd_user_db.MailBatchUserList(BatchId,EmployeeId,Email,Exported)
		values(@mbatchId,@mempId,@memail,1)	

	update imd_user_db.MailOutbox set IsSent = 1
	where OutBoxId = @moutboxId
	
	select @count=@@rowcount
end try
begin catch
	set @count = 0
end catch

select @count

END