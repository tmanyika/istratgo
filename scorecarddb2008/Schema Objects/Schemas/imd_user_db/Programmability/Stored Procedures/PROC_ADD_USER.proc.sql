﻿




CREATE PROCEDURE [imd_user_db].[PROC_ADD_USER]
          
            @LOGIN_ID  VARCHAR(20)
           ,@PASSWORD VARCHAR(200)
           ,@ORG_ID INT
           ,@JOB_TITLE_ID INT
           ,@NAME VARCHAR(150)
           ,@EMAIL_ADDRESS VARCHAR(150)
           ,@REPORTS_TO INT
           ,@USER_TYPE INT         

AS
BEGIN
declare @USER_ID int
declare @emailExist bit
declare @userNameExist bit

set @emailExist = 0
set @userNameExist = 0
set @USER_ID = 0;

if exists(select email_address from imd_user_db.USER_ADMIN where EMAIL_ADDRESS = @EMAIL_ADDRESS)
 set @emailExist = 1
if exists(select email_address from imd_user_db.USER_ADMIN where EMAIL_ADDRESS = @EMAIL_ADDRESS)
 set @userNameExist = 1
if(@userNameExist = 0 and @emailExist = 0)
begin
	insert into [USER_ADMIN]
			   ([LOGIN_ID]
			   ,[PASSWORD]
			   ,[ORG_ID]
			   ,[JOB_TITLE_ID]
			   ,[NAME]
			   ,[EMAIL_ADDRESS]
			   ,[REPORTS_TO]
			   ,[USER_TYPE]
			 )
		 values
			   (
				@LOGIN_ID
			   ,@PASSWORD
			   ,@ORG_ID
			   ,@JOB_TITLE_ID
			   ,@NAME
			   ,@EMAIL_ADDRESS
			   ,@REPORTS_TO
			   ,@USER_TYPE
			   )

	select @USER_ID = scope_identity()
	IF @REPORTS_TO <= 0 
	   update USER_ADMIN set [REPORTS_TO] =@USER_ID where ID = @USER_ID
end
  select @USER_ID as userId,@userNameExist as uname,@emailExist as email
END