﻿

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_TalentMatrix_Update] 
	@MatrixId int,
	@CompanyId int, 
	@TalentMatrixName varchar(300), 
	@TalentMatrixDescription varchar(2000), 
	@LowerBoundSign varchar(4), 
	@LowerBound decimal(18,1), 
	@UpperBoundSign varchar(4), 
	@UpperBound decimal(18,1),
	@Active bit, 
	@UpdatedBy varchar(20),
	@PositionInMatrix int
AS
BEGIN
	set nocount on;
	
	update imd_user_db.TalentMatrix
	set CompanyId = @CompanyId, 
		TalentMatrixName = @TalentMatrixName, 
		TalentMatrixDescription = @TalentMatrixDescription, 
		LowerBoundSign = @LowerBoundSign, 
		LowerBound = @LowerBound, 
		UpperBoundSign = @UpperBoundSign, 
		UpperBound = @UpperBound, 
		UpdatedBy = @UpdatedBy, 
		Active = @Active,
		DateUpdated = GETDATE(),
		PositionInMatrix = @PositionInMatrix
	where MatrixId = @MatrixId
	select @@rowcount
END