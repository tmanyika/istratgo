﻿


CREATE PROCEDURE [imd_user_db].[PROC_UPDATE_COMPANY_GOAL]
@ID INT,
@STRATEGIC_ID INT,
@AREA_TYPE_ID INT,
@GOAL_DESCRIPTION VARCHAR(150),
@UNIT_MEASURE int = NULL,
@TARGET VARCHAR(150),
@WEIGHT INT,
@AREA_ID INT,
@RATIONAL VARCHAR(1000) = NULL,
@ENV_CONTEXT VARCHAR(1000) = NULL,
@CONTEXTID INT = NULL,
@COMPANY_ID INT = NULL,
@HAS_SUB_GOALS BIT = 0
AS
BEGIN
	set nocount on;
	declare @rec int = 0;
	
	begin try
		select     ig.ID into #IDs
		from         imd_user_db.INDIVIDUAL_GOALS AS ig INNER JOIN
							  imd_user_db.COMPANY_STRATEGIC AS cs ON ig.STRATEGIC_ID = cs.ID
		where     (cs.COMPANY_ID = @COMPANY_ID) AND (ig.AREA_ID = @AREA_ID) AND (ig.AREA_TYPE_ID = @AREA_TYPE_ID)
			
		begin tran
			if((@CONTEXTID is null) and (@ENV_CONTEXT is not null))
				begin
					insert into imd_user_db.ENVIRONMENT_CONTEXT(COMPANY_ID,AREA_TYPE_ID,AREA_ID,ENV_CONTEXT,DATE_CREATED)
					values(@COMPANY_ID,@AREA_TYPE_ID,@AREA_ID,@ENV_CONTEXT,getdate())
					select @CONTEXTID=scope_identity()
				end
			else if((@ENV_CONTEXT is not null) or @ENV_CONTEXT <> '')
				begin
					update imd_user_db.ENVIRONMENT_CONTEXT
					set ENV_CONTEXT =  @ENV_CONTEXT,
						DATE_UPDATED = getdate()
					where ENV_CONTEXT_ID = @CONTEXTID
				end
			
			if(@HAS_SUB_GOALS = 0)	
			begin
				update imd_user_db.INDIVIDUAL_GOALS 
				set STRATEGIC_ID=@STRATEGIC_ID, 
					AREA_TYPE_ID=@AREA_TYPE_ID,
					GOAL_DESCRIPTION=@GOAL_DESCRIPTION,
					UNIT_MEASURE=@UNIT_MEASURE,
					[TARGET]=@TARGET,
					[WEIGHT]=@WEIGHT, 
					AREA_ID=@AREA_ID,
					RATIONAL = @RATIONAL,
					ENV_CONTEXT_ID = @CONTEXTID,
					DATE_UPDATED = getdate(),
					HAS_SUB_GOALS = @HAS_SUB_GOALS
				where ID =@ID			
				select @rec=@@rowcount
			end
			else
				begin
					update imd_user_db.INDIVIDUAL_GOALS 
					set STRATEGIC_ID=@STRATEGIC_ID, 
						AREA_TYPE_ID=@AREA_TYPE_ID,
						GOAL_DESCRIPTION= case when @GOAL_DESCRIPTION = '' then GOAL_DESCRIPTION
										  else @GOAL_DESCRIPTION end,
						--UNIT_MEASURE=@UNIT_MEASURE,
						--[TARGET]=@TARGET,
						[WEIGHT]=@WEIGHT, 
						AREA_ID=@AREA_ID,
						--RATIONAL = @RATIONAL,
						ENV_CONTEXT_ID = @CONTEXTID,
						DATE_UPDATED = getdate(),
						HAS_SUB_GOALS = @HAS_SUB_GOALS
					where ID =@ID			
					select @rec=@@rowcount
				end
			update imd_user_db.INDIVIDUAL_GOALS 
			set ENV_CONTEXT_ID = @CONTEXTID
			where (ENV_CONTEXT_ID is null) and (ID in (select ID from #IDs))	
		commit tran
		
		drop table #IDs
	end try
	begin catch
		if(@@trancount > 0)
			rollback tran
	end catch
	select @rec
END