﻿

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_BulkSheetField_GetSheetActiveFields]
	@SheetId int,
	@Active bit
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	select      SheetName, FieldId, SheetId, SheetFieldName, DbFieldName, IsRequired, Active, FieldNotes, DataType, ExpectedValueList, CreatedBy, DateCreated, UpdatedBy, DateUpdated, DbTableName, SheetDescription, 
               SheetNotes
	from         imd_user_db.vw_BulkSheetFields
	where  (SheetId = @SheetId and Active = @Active)
	order by SheetFieldName
END