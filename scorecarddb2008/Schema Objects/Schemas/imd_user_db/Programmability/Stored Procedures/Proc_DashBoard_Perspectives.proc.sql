﻿

CREATE PROCEDURE [imd_user_db].[Proc_DashBoard_Perspectives] 
	@CRITERIA_TYPE_ID int,
	@SELECTED_ITEM_VALUE int,
	@ISCOMPANY bit,
	@START_DATE datetime,
	@END_DATE datetime
as
begin
	set nocount on;
	set transaction isolation level read uncommitted;

	select Perspective, round((sum(ActualValue)/count(Perspective)),0) as ActualValue, 
	round((sum(TargetValue)/count(Perspective)),0) as TargetValue
	from(
			select cp.PERSPECTIVE_NAME AS Perspective, isc.CRITERIA_TYPE_ID, SUM(isc.FINAL_SCORE) AS ActualValue, SUM(ig.WEIGHT) AS TargetValue, isc.PERIOD_DATE, 
                  cs.COMPANY_ID, isc.SELECTED_ITEM_VALUE
			from      imd_user_db.INDIVIDUAL_SCORES AS isc INNER JOIN
					  imd_user_db.INDIVIDUAL_GOALS AS ig ON isc.GOAL_ID = ig.ID INNER JOIN
					  imd_user_db.COMPANY_STRATEGIC AS cs ON ig.STRATEGIC_ID = cs.ID INNER JOIN
					  imd_user_db.COMPANY_PERSPECTIVE AS cp ON cs.PERSPECTIVE = cp.PERSPECTIVE_ID INNER JOIN 
			          imd_user_db.SUBMISSIONS AS sb ON isc.SUBMISSION_ID = sb.ID
			where (sb.[STATUS] = 40 AND isc.PARENT_ID IS NULL) AND (isc.CRITERIA_TYPE_ID = @CRITERIA_TYPE_ID) AND (isc.PERIOD_DATE BETWEEN @START_DATE AND @END_DATE) AND 
								  (isc.SELECTED_ITEM_VALUE = @SELECTED_ITEM_VALUE)
			group by isc.CRITERIA_TYPE_ID, cs.COMPANY_ID, cp.PERSPECTIVE_NAME, isc.PERIOD_DATE, isc.SELECTED_ITEM_VALUE
	) as d
	group by Perspective
	order by Perspective
end