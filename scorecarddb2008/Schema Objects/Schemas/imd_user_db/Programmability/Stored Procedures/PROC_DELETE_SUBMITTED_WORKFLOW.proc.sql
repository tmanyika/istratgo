﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE imd_user_db.PROC_DELETE_SUBMITTED_WORKFLOW
	@submitId int
AS
BEGIN
	set nocount on;
	set xact_abort on;
	
	declare @rows int = 0;
	begin try	
		begin tran		
			delete from imd_user_db.INDIVIDUAL_SCORES where SUBMISSION_ID = @submitId 
			delete from imd_user_db.SUBMISSIONS where ID = @submitId
			select @rows = @@rowcount
		commit tran
	end try
	begin catch
		set @rows = 0
		if(@@trancount > 0)
			rollback tran
	end catch	
	select @rows
END