﻿/*-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_BulkImport_PerformanceMeasuresBkup]
	@CompanyId int,
	@CreatedBy varchar(20)
AS
BEGIN
	set nocount on;
	set xact_abort on;

	declare @projAreaId int,
			@jobAreaId int,
			@orgUnitAreaId int;

	begin try
	 set @jobAreaId = 32;
	 set @orgUnitAreaId = 33;
	 set @projAreaId = 49;

	 -- Trim white spaces KFA (Key Focus Areas --
	 update imd_user_db.bulkPerformanceMeasure
	 set KeyFocusArea = rtrim(ltrim(KeyFocusArea)),
		KeyFocusAreaDescription = rtrim(ltrim(KeyFocusAreaDescription)),
		StrategicObjective = rtrim(ltrim(StrategicObjective)),
		StrategicObjectiveRationale = rtrim(ltrim(StrategicObjectiveRationale)),
		PerformanceMeasure = rtrim(ltrim(PerformanceMeasure)),
		HasSubPerformanceMeasures = rtrim(ltrim(HasSubPerformanceMeasures)),
		ParentPerformanceMeasure = rtrim(ltrim(ParentPerformanceMeasure)),
		AreaOfMeasureType = rtrim(ltrim(AreaOfMeasureType)),
		AreaOfMeasure = rtrim(ltrim(AreaOfMeasure)),
		UnitOfMeasure =  rtrim(ltrim(UnitOfMeasure)),
		[TARGET] = rtrim(ltrim([TARGET])),
	    [WEIGHT] = rtrim(ltrim([WEIGHT]))
	 where CompanyId = @CompanyId

	 --Delete invalid data and Rebuild All Table Indexes
	 delete from imd_user_db.bulkPerformanceMeasure 
	 where (KeyFocusArea is null and (StrategicObjective is null or PerformanceMeasure is null))
	 alter index all on imd_user_db.bulkPerformanceMeasure rebuild with (fillfactor = 80)


	 --Add KFA
	 select distinct CompanyId, KeyFocusArea, KeyFocusAreaDescription
	 into #kfa
	 from imd_user_db.bulkPerformanceMeasure with(nolock)
	 where CompanyId = @CompanyId and ( 
										KeyFocusArea not in (	select ltrim(rtrim(PERSPECTIVE_NAME))
																from imd_user_db.COMPANY_PERSPECTIVE with(nolock)
																where COMPANY_ID = @CompanyId
															)
									   )
	 begin tran
	   insert into imd_user_db.COMPANY_PERSPECTIVE(COMPANY_ID, PERSPECTIVE_NAME, PERSPECTIVE_DESC, ACTIVE, CREATED_BY, DATE_CREATED)
	   select CompanyId, KeyFocusArea, KeyFocusAreaDescription, 1 as Active, @CreatedBy as CreatedBy, getdate() as DateCreated
	   from [#kfa] with(nolock)
	 commit tran	 
	 drop table [#kfa]
	 --End of Adding KFA

	 --Add Strategic Objectives
	 select distinct s.CompanyId, s.StrategicObjective, s.StrategicObjectiveRationale, cp.PERSPECTIVE_ID 
	 into #stgo
	 from imd_user_db.bulkPerformanceMeasure as s with(nolock) left outer join 
	      imd_user_db.COMPANY_PERSPECTIVE as cp with(nolock) 
	 on s.KeyFocusArea = cp.PERSPECTIVE_NAME and s.CompanyId = cp.COMPANY_ID
	 where (s.CompanyId = @CompanyId and cp.COMPANY_ID = @CompanyId) and 
	       ( 
				s.StrategicObjective not in ( select OBJECTIVE 
										      from imd_user_db.COMPANY_STRATEGIC with(nolock)
										      where COMPANY_ID = @CompanyId
									        )
		   )

	 begin tran
	   insert into imd_user_db.COMPANY_STRATEGIC(COMPANY_ID, PERSPECTIVE, OBJECTIVE, RATIONALE, ACTIVE, DATE_CREATED, CREATEDBY)
	   select CompanyId, PERSPECTIVE_ID, StrategicObjective, StrategicObjectiveRationale, 1 as Active, getdate() as DateCreated, @CreatedBy as CreatedBy
	   from [#stgo] with(nolock)
	 commit tran
	 drop table [#stgo]

	 -- Update PerspectiveId field
	 select distinct s.CompanyId, s.KeyFocusArea, s.StrategicObjective, s.StrategicObjectiveRationale, cp.PERSPECTIVE_ID 
	 into #ustgo
	 from imd_user_db.bulkPerformanceMeasure as s with(nolock) inner join 
	      imd_user_db.COMPANY_PERSPECTIVE as cp with(nolock) on s.KeyFocusArea = cp.PERSPECTIVE_NAME
	 where (s.CompanyId = @CompanyId and cp.COMPANY_ID = @CompanyId) 

	 begin tran
		update imd_user_db.COMPANY_STRATEGIC
		set COMPANY_STRATEGIC.PERSPECTIVE = u.PERSPECTIVE_ID
		from imd_user_db.COMPANY_STRATEGIC cs inner join #ustgo u
		on cs.OBJECTIVE = u.StrategicObjective and cs.COMPANY_ID = u.CompanyId
		where cs.COMPANY_ID = @CompanyId and cs.PERSPECTIVE is null
	 commit tran
	 drop table [#ustgo]

	 --End Of Updating PerspectiveId field
	 --End of Adding Strategic Objectives

	 --Start Creating Base for Adding Goals
	 select g.CompanyId, cs.Company_ID, g.PerformanceMeasure, g.StrategicObjective, cs.ID as StrategicId, 0 as HasSubPerformanceMeasures, g.ParentPerformanceMeasure, g.AreaOfMeasureType, 
			 u.ID as AreaOfMeasureTypeId, g.AreaOfMeasure, ms.Id as UnitOfMeasureId, 1 as Active,
			 g.UnitOfMeasure, g.[TARGET], g.[WEIGHT], getdate() as DateCreated, @CreatedBy as CreatedBy
	 into #gol
	 from imd_user_db.bulkPerformanceMeasure as g with(nolock) inner join imd_user_db.COMPANY_STRATEGIC as cs with(nolock)
	 on g.StrategicObjective = cs.OBJECTIVE and g.CompanyId = cs.COMPANY_ID left outer join imd_user_db.UTILITIES as u with(nolock)
	 on g.AreaOfMeasureType = u.[TYPE_NAME] left outer join imd_user_db.UTILITIES as ms with(nolock)
	 on g.UnitOfMeasure = ms.[TYPE_NAME]
	 where (g.CompanyId = @CompanyId and cs.COMPANY_ID = @CompanyId)  and (
										 g.PerformanceMeasure not in (
																		select ltrim(rtrim(Goal))
																		from imd_user_db.vw_BulkGoal with(nolock)
																		where CompanyId = @CompanyId 
																	)
									   )
	--End Creating Base for Adding Goals

	--Start of Adding All Goals
	begin tran
		--Add Job Titles
		insert into imd_user_db.INDIVIDUAL_GOALS(STRATEGIC_ID, AREA_TYPE_ID, AREA_ID, UNIT_MEASURE, HAS_SUB_GOALS, [TARGET], [WEIGHT], DATE_CREATED, GOAL_DESCRIPTION, ACTIVE, CREATEDBY)
		select g.StrategicId, g.AreaOfMeasureTypeId, j.ID as AREA_ID, g.UnitOfMeasureId, g.HasSubPerformanceMeasures as HAS_SUB_GOALS, 
		       g.[TARGET], g.[WEIGHT], g.DateCreated, g.PerformanceMeasure, g.Active, g.CreatedBy
		from #gol as g with(nolock) inner join imd_user_db.COMPANY_JOB_TITLES as j with(nolock)
		on g.AreaOfMeasure = j.NAME and g.CompanyId = j.COMPANY_ID
		where (g.AreaOfMeasureTypeId = @jobAreaId and g.CompanyId = @CompanyId and j.COMPANY_ID = @CompanyId) 
	commit tran

	begin tran --Add Org Units
		insert into imd_user_db.INDIVIDUAL_GOALS(STRATEGIC_ID, AREA_TYPE_ID, AREA_ID, UNIT_MEASURE, HAS_SUB_GOALS, [TARGET], [WEIGHT], DATE_CREATED, GOAL_DESCRIPTION, ACTIVE, CREATEDBY)
		select g.StrategicId, g.AreaOfMeasureTypeId, j.ID as AREA_ID, g.UnitOfMeasureId, g.HasSubPerformanceMeasures as HAS_SUB_GOALS, 
		       g.[TARGET], g.[WEIGHT], g.DateCreated, g.PerformanceMeasure, g.Active, g.CreatedBy
		from #gol as g with(nolock) inner join imd_user_db.COMPANY_STRUCTURE as j with(nolock)
		on g.AreaOfMeasure = j.ORGUNIT_NAME and g.CompanyId = j.COMPANY_ID
		where (g.AreaOfMeasureTypeId = @orgUnitAreaId and g.CompanyId = @CompanyId and j.COMPANY_ID = @CompanyId)
	commit tran

	begin tran --Add Projects
		insert into imd_user_db.INDIVIDUAL_GOALS(STRATEGIC_ID, AREA_TYPE_ID, AREA_ID, UNIT_MEASURE, HAS_SUB_GOALS, [TARGET], [WEIGHT], DATE_CREATED, GOAL_DESCRIPTION, ACTIVE, CREATEDBY)
		select g.StrategicId, g.AreaOfMeasureTypeId, j.PROJECT_ID as AREA_ID, g.UnitOfMeasureId, g.HasSubPerformanceMeasures as HAS_SUB_GOALS, 
		       g.[TARGET], g.[WEIGHT], g.DateCreated, g.PerformanceMeasure, g.Active, g.CreatedBy
		from #gol as g with(nolock) inner join imd_user_db.COMPANY_PROJECT  as j with(nolock)
		on g.AreaOfMeasure = j.PROJECT_NAME and g.CompanyId = j.COMPANY_ID
		where (g.AreaOfMeasureTypeId = @projAreaId and g.CompanyId = @CompanyId and j.COMPANY_ID = @CompanyId)
	commit tran
	drop table [#gol]

	--End of Adding All Goals

	--Update Parent Goals on Performance Measure
	select GoalDescription, ParentGoal, ParentMeasureId, CompanyId, COMPANY_ID
	into #prnt
	from imd_user_db.vw_BulkMeasuresParent with (nolock)

	where (CompanyId = @CompanyId) AND (COMPANY_ID = @CompanyId)

	begin tran
		update imd_user_db.INDIVIDUAL_GOALS
		set INDIVIDUAL_GOALS.PARENT_ID = pm.ParentMeasureId
		from imd_user_db.INDIVIDUAL_GOALS ig inner join #prnt pm
		on ig.GOAL_DESCRIPTION = pm.GoalDescription
		where (pm.ParentGoal is not null and pm.CompanyId = @CompanyId)
	commit tran	
	begin tran
		update imd_user_db.INDIVIDUAL_GOALS
		set INDIVIDUAL_GOALS.HAS_SUB_GOALS = 1
		from imd_user_db.INDIVIDUAL_GOALS ig inner join [#prnt] pm
		on ig.ID = pm.ParentMeasureId
		where (pm.CompanyId = @CompanyId)
	commit tran
	drop table [#prnt]
	--End of Updating Parent Goals on Performance Measure
	end try
	begin catch
		set @jobAreaId = 0
		if(@@trancount > 0)
			rollback tran

		insert into imd_user_db.ProcError(ErrorNo, ErrorSeverity, ErrorState, ErrorProcedure, ErrorLine, ErrorMessage, DateCreated)
		select error_number() AS ErrorNumber, error_severity() AS ErrorSeverity, error_state() AS ErrorState, 
						error_procedure() AS ErrorProcedure, error_line() AS ErrorLine, error_message() AS ErrorMessage, 
						getdate() as DateCreated
	end catch

	select @jobAreaId
END*/