﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_BulkSheetField_Update]
	@FieldId int, 
	@SheetId int,
	@SheetFieldName varchar(120), 
	@DbFieldName varchar(40), 
	@IsRequired bit = 1, 
	@Active bit, 
	@FieldNotes varchar(1000), 
	@DataType int, 
	@ExpectedValueList varchar(2000),  
	@UpdatedBy varchar(50)
AS
BEGIN
	set nocount on;
	
	if not(exists(	select SheetFieldName 
					from imd_user_db.BulkSheetField 
					where FieldId <> @FieldId and SheetId = @SheetId and Active = 1 and SheetFieldName like @SheetFieldName
				)
		   )
		update imd_user_db.BulkSheetField
		set SheetFieldName = @SheetFieldName, 
			DbFieldName = @DbFieldName, 
			IsRequired = @IsRequired, 
			Active = @Active, 
			FieldNotes = @FieldNotes, 
			DataType = @DataType, 
			ExpectedValueList = @ExpectedValueList,
			UpdatedBy = @UpdatedBy, 
			DateUpdated = getdate()
		where (FieldId = @FieldId)
	select @@rowcount
END