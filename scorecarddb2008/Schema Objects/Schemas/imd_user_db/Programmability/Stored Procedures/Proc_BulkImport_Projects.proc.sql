﻿

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_BulkImport_Projects] 
	@CompanyId int,
	@CreatedBy varchar(20)
AS
BEGIN
	set nocount on;
	set xact_abort on;
	
	declare @currDate datetime,
			@projAreaId int;
	
	begin try
	    set @ProjAreaId = 49;
		set @currDate = getdate();
				
		--Trim white spaces on the projects
		update imd_user_db.bulkProject 
		set ProjectName = imd_user_db.PropCaseDelDoubleSpace(ProjectName),
			ProjectOwnerEmail = lower(rtrim(ltrim(ProjectOwnerEmail)))
		where CompanyId = @CompanyId
		
		 --Delete invalid data and Rebuild All Table Indexes
		delete from imd_user_db.bulkProject where (ProjectName is null)
		alter index all on imd_user_db.bulkProject rebuild with (fillfactor = 80)
	 
		--Get missing Projects only to avoid duplication		
		select distinct ProjectName, ProjectDescription, ProjectOwnerEmail, CompanyId
		into #proj
		from imd_user_db.bulkProject with(nolock)
		where CompanyId = @CompanyId and 
			  (
				ProjectName not in (
									select PROJECT_NAME 
									from imd_user_db.COMPANY_PROJECT (nolock)
									where COMPANY_ID = @CompanyId
								  )
			   )
			
		--Insert new Projects from primary source table
		begin tran
			insert into imd_user_db.COMPANY_PROJECT(COMPANY_ID, PROJECT_NAME, PROJECT_DESC, ACTIVE, CREATED_BY, DATE_CREATED)
			select CompanyId, ProjectName, ProjectDescription, 1 as Active, @CreatedBy as CreatedBy, @currDate as DateCreated
			from [#proj] with(nolock)
		commit tran
		
		--Add Additional Project Data In Performance Measure
		select CompanyId, imd_user_db.PropCaseDelDoubleSpace(AreaOfMeasure) as ProjectName, null as ProjectDescription, 1 as Active, @CreatedBy as CreatedBy, @currDate as DateCreated  
		into #aproj
		from imd_user_db.vw_BulkAdditionalData with(nolock)
		where (AreaId = @ProjAreaId and CompanyId = @CompanyId) and
		      AreaOfMeasure not in (
											select rtrim(ltrim(PROJECT_NAME)) 
											from imd_user_db.COMPANY_PROJECT (nolock)
											where COMPANY_ID = @CompanyId
								   )
		begin tran
			insert into imd_user_db.COMPANY_PROJECT(COMPANY_ID, PROJECT_NAME, PROJECT_DESC, ACTIVE, CREATED_BY, DATE_CREATED)
			select CompanyId, ProjectName, ProjectDescription, Active, CreatedBy, DateCreated
			from [#aproj] with(nolock)
		commit tran
		
		drop table [#proj]
		drop table [#aproj]
	end try
	begin catch
		set @projAreaId = 0
		if(@@trancount > 0)
			rollback tran
		
		insert into imd_user_db.ProcError(ErrorNo, ErrorSeverity, ErrorState, ErrorProcedure, ErrorLine, ErrorMessage, DateCreated)
		select error_number() AS ErrorNumber, error_severity() AS ErrorSeverity, error_state() AS ErrorState, 
						error_procedure() AS ErrorProcedure, error_line() AS ErrorLine, error_message() AS ErrorMessage, 
						getdate() as DateCreated
	end catch
	
	select @projAreaId
END