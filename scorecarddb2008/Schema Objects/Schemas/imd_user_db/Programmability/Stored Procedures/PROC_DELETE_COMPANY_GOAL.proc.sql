﻿

CREATE PROCEDURE [imd_user_db].[PROC_DELETE_COMPANY_GOAL]
@ID INT,
@UPDATEDBY VARCHAR(100)
AS
BEGIN
set nocount on;
declare @haschildren bit = 0;

if exists(select ID from imd_user_db.INDIVIDUAL_GOALS where PARENT_ID = @ID and ACTIVE = 1)
 set @haschildren = 1

if(@haschildren = 1)
	update INDIVIDUAL_GOALS 
	set UPDATEDBY = @UPDATEDBY,
		ACTIVE = 0,
		DATE_UPDATED = GETDATE()
	where ID = @ID OR PARENT_ID = @ID
else
	update INDIVIDUAL_GOALS 
	set UPDATEDBY = @UPDATEDBY,
		ACTIVE = 0,
		DATE_UPDATED = GETDATE()
	where ID = @ID
select @@ROWCOUNT
END