﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE imd_user_db.Proc_LeaveApplication_IsValid 
	@applicationId int = 0,  
	@employeeId int,
	@startDate datetime,
	@endDate datetime
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	declare @overlapping bit;
	set @overlapping = 0;
	
	if(@applicationId <= 0)
		begin
			if ((exists(select StartDate from imd_user_db.LeaveApplication 
					  where (EmployeeId = @employeeId and (StatusId in (1,2,5))) and (@startDate between startdate and enddate))) OR
				 (exists(select EndDate from imd_user_db.LeaveApplication 
					  where (EmployeeId = @employeeId and (StatusId in (1,2,5))) and (@endDate between startdate and enddate)))
				)
				set @overlapping = 1
		end
	else
		begin
			if ((exists(select StartDate from imd_user_db.LeaveApplication 
					  where (ApplicationId <> @applicationId) and (EmployeeId = @employeeId and (StatusId in (1,2,5))) and (@startDate between startdate and enddate))) OR
				 (exists(select EndDate from imd_user_db.LeaveApplication 
					  where (ApplicationId <> @applicationId) and (EmployeeId = @employeeId and (StatusId in (1,2,5))) and (@endDate between startdate and enddate)))
				)
				set @overlapping = 1
		end
		
	select @overlapping
END