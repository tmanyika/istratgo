﻿CREATE PROCEDURE [imd_user_db].[PROC_GET_MENUS]

AS
begin
	set transaction isolation level read uncommitted;
	set nocount on;
	select MENU_ID, MENU_NAME, IMAGE_ICON, MENU_PATH, ORDER_ID, ACTIVE, PARENT_MENU_ID
	from APP_MENU
end