﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_BulkSheetField_Insert]
	@SheetId int, 
	@SheetFieldName varchar(120), 
	@DbFieldName varchar(40), 
	@IsRequired bit = 1, 
	@Active bit, 
	@FieldNotes varchar(1000), 
	@DataType int, 
	@ExpectedValueList varchar(2000), 
	@CreatedBy varchar(50)
AS
BEGIN
	set nocount on;
	
	if not(exists(	select SheetFieldName 
					from imd_user_db.BulkSheetField 
					where SheetId = @SheetId and Active = 1 and SheetFieldName like @SheetFieldName
				)
		   )
		insert into imd_user_db.BulkSheetField(SheetId, SheetFieldName, DbFieldName, IsRequired, Active, FieldNotes, DataType, ExpectedValueList, CreatedBy, DateCreated)
		values(@SheetId, @SheetFieldName,@DbFieldName,@IsRequired,@Active,@FieldNotes, @DataType, @ExpectedValueList,@CreatedBy, GetDate())
	select @@rowcount
END