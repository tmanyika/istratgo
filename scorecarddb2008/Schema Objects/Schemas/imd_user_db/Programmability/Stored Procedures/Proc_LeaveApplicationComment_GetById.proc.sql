﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE imd_user_db.Proc_LeaveApplicationComment_GetById
	@commentId int
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	select CommentId, ApplicationId, DocumentTitle, Comment, SupportingDoc, Valid, UploadedBy, DateCreated, UpdatedBy, DateUpdated
	from imd_user_db.LeaveApplicationComment
	where (CommentId = @commentId)
END