﻿CREATE PROCEDURE [imd_user_db].[PROC_GET_SCORES_USEDDATES] 
	@CRITERIA_TYPE_ID INT,
	@SELECTED_ITEM_VALUE int
as
begin
	set nocount on;
	set transaction isolation level read uncommitted;
	
	declare @declinedId int = imd_user_db.fxDeclinedStatusId();
	select distinct CONVERT(varchar(10), isc.PERIOD_DATE, 121) AS PERIOD_DATE
	from         INDIVIDUAL_SCORES as isc INNER JOIN
			  INDIVIDUAL_GOALS as ig on isc.GOAL_ID = ig.ID INNER JOIN
			  SUBMISSIONS as s with (nolock) on isc.SUBMISSION_ID = s.ID
	where   (isc.SELECTED_ITEM_VALUE = @SELECTED_ITEM_VALUE AND isc.CRITERIA_TYPE_ID = @CRITERIA_TYPE_ID
	         AND s.[STATUS] <> @declinedId)
end