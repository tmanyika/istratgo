﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_Employee_GetActive]
	@orgUnitId int -- this is the org unit id defined in the companystructure table
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	select l.UserName, l.Pass, l.RoleId, l.Active, e.EmployeeId, e.LineManagerEmployeeId, e.OrgUnitId, e.FirstName, e.MiddleName, e.LastName, e.EmailAddress, e.JobTitleId, 
           e.StatusId, e.EmployeeNo, e.WorkingWeekId, e.Cellphone, e.BusTelephone, e.CreatedBy, e.DateCreated, e.DateUpdated, e.UpdatedBy, j.NAME AS jobTitle, 
           w.Name, w.NoOfDays, c.Company_Id as CompanyId, s.StatusName, c.ORGUNIT_NAME as CompanyName, u.ID as typeId, u.[TYPE_NAME] as typeName,
           isnull(cd.COUNTRY,277) as CountryId, e.DateOfAppointment
	from	imd_user_db.Employee AS e INNER JOIN
			imd_user_db.COMPANY_STRUCTURE as c ON e.OrgUnitId = c.ID LEFT OUTER JOIN
			imd_user_db.COMPANY_DETAIL as cd ON c.COMPANY_ID = cd.COMPANY_ID LEFT OUTER JOIN
            imd_user_db.EmployeeLogin AS l ON e.EmployeeId = l.EmployeeId LEFT OUTER JOIN
            imd_user_db.UTILITIES AS u ON l.RoleId = u.ID LEFT OUTER JOIN
            imd_user_db.COMPANY_JOB_TITLES AS j ON e.JobTitleId = j.ID LEFT OUTER JOIN
            imd_user_db.EmployeeStatus AS s ON e.StatusId = s.StatusId LEFT OUTER JOIN
            imd_user_db.WorkingWeek AS w ON e.WorkingWeekId = w.WorkingWeekId
	where     (e.OrgUnitId = @orgUnitId) and (e.StatusId in (2,4) or e.StatusId is null or l.Active = 1)
	order by e.LastName, e.FirstName
END