﻿
CREATE PROCEDURE [imd_user_db].[PROC_GET_SCORES_REPORT_DEPARTMENT] 
	@CRITERIA_TYPE_ID int,
	@SELECTED_ITEM_VALUE int,
	@SSCORE_DATE datetime,
	@ESCORE_DATE datetime,
	@ISCOMPANYID bit
as
begin
	set nocount on;
	set transaction isolation level read uncommitted;
	
	if(@ISCOMPANYID = 0)
		select     isc.ID, isc.CRITERIA_TYPE_ID, isc.SELECTED_ITEM_VALUE, isc.GOAL_ID, isc.SCORES, isc.FINAL_SCORE, isc.DATE_CREATED, isc.SUBMISSION_ID, ig.WEIGHT, 
						  ig.TARGET, ig.GOAL_DESCRIPTION, isc.PERIOD_DATE, cp.PERSPECTIVE_NAME, cs.OBJECTIVE, u.TYPE_NAME AS UNIT_MEASURE, cp.PERSPECTIVE_ID, 
							  csr.ORGUNIT_NAME AS ItemName, csr.COMPANY_ID, csr.ID AS ItemId,
							  isc.MANAGER_SCORE, isc.AGREED_SCORE, isc.RATINGVALUE, isc.FINAL_SCORE_AGREED, isc.PARENT_ID, isc.HAS_SUB_GOALS
		from         imd_user_db.COMPANY_STRUCTURE AS csr INNER JOIN
							  imd_user_db.INDIVIDUAL_SCORES AS isc INNER JOIN
							  imd_user_db.INDIVIDUAL_GOALS AS ig ON isc.GOAL_ID = ig.ID INNER JOIN
							  imd_user_db.COMPANY_STRATEGIC AS cs ON ig.STRATEGIC_ID = cs.ID INNER JOIN
							  imd_user_db.COMPANY_PERSPECTIVE AS cp ON cs.PERSPECTIVE = cp.PERSPECTIVE_ID ON csr.ID = isc.SELECTED_ITEM_VALUE LEFT OUTER JOIN
							  imd_user_db.UTILITIES AS u ON ig.UNIT_MEASURE = u.ID INNER JOIN 
			          imd_user_db.SUBMISSIONS AS sb ON isc.SUBMISSION_ID = sb.ID
		where  (sb.[STATUS] = 40) AND (csr.ID = @SELECTED_ITEM_VALUE) AND (isc.CRITERIA_TYPE_ID = @CRITERIA_TYPE_ID) AND 
		(
			isc.PERIOD_DATE BETWEEN @SSCORE_DATE AND @ESCORE_DATE
		)
		order by csr.ORGUNIT_NAME
	else
		select     isc.ID, isc.CRITERIA_TYPE_ID, isc.SELECTED_ITEM_VALUE, isc.GOAL_ID, isc.SCORES, isc.FINAL_SCORE, isc.DATE_CREATED, isc.SUBMISSION_ID, ig.WEIGHT, 
						  ig.TARGET, ig.GOAL_DESCRIPTION, isc.PERIOD_DATE, cp.PERSPECTIVE_NAME, cs.OBJECTIVE, u.TYPE_NAME AS UNIT_MEASURE, cp.PERSPECTIVE_ID, 
				  csr.ORGUNIT_NAME AS ItemName, csr.COMPANY_ID, csr.ID AS ItemId,
				  isc.MANAGER_SCORE, isc.AGREED_SCORE, isc.RATINGVALUE, isc.FINAL_SCORE_AGREED, isc.PARENT_ID, isc.HAS_SUB_GOALS
		from         imd_user_db.COMPANY_STRUCTURE AS csr INNER JOIN
							  imd_user_db.INDIVIDUAL_SCORES AS isc INNER JOIN
							  imd_user_db.INDIVIDUAL_GOALS AS ig ON isc.GOAL_ID = ig.ID INNER JOIN
							  imd_user_db.COMPANY_STRATEGIC AS cs ON ig.STRATEGIC_ID = cs.ID INNER JOIN
							  imd_user_db.COMPANY_PERSPECTIVE AS cp ON cs.PERSPECTIVE = cp.PERSPECTIVE_ID ON csr.ID = isc.SELECTED_ITEM_VALUE LEFT OUTER JOIN
							  imd_user_db.UTILITIES AS u ON ig.UNIT_MEASURE = u.ID INNER JOIN
							  imd_user_db.fxGetOrgStructHierachy(@SELECTED_ITEM_VALUE) AS fx ON csr.ID = fx.Id INNER JOIN 
			          imd_user_db.SUBMISSIONS AS sb ON isc.SUBMISSION_ID = sb.ID
		where  (sb.[STATUS] = 40) AND (isc.CRITERIA_TYPE_ID = @CRITERIA_TYPE_ID) AND 
		(
			isc.PERIOD_DATE BETWEEN @SSCORE_DATE AND @ESCORE_DATE
		) ---(csr.COMPANY_ID = @SELECTED_ITEM_VALUE) AND 
		order by csr.ORGUNIT_NAME
end