﻿


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_LeaveAccrual_Insert]
	@leaveId int, 
	@workingWeekId int, 
	@accrualTimeId int,
    @accrualRate decimal(18,4), 
    @maximumThreshold decimal(18,4), 
    @active bit, 
    @createdBy varchar(20)
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	if not(exists(select leaveId from imd_user_db.LeaveAccrual where leaveId = @leaveId and workingWeekId = @workingWeekId))
		insert into imd_user_db.LeaveAccrual
		(
			LeaveId, 
			WorkingWeekId, 
			AccrualTimeId, 
			AccrualRate, 
			MaximumThreshold, 
			Active, 
			CreatedBy, 
			DateCreated
		)
		values
		(
			@leaveId, 
			@workingWeekId, 
			@accrualTimeId, 
			@accrualRate, 
			@maximumThreshold, 
			@active, 
			@createdBy, 
			getdate()
		)
	--else
	--	update imd_user_db.LeaveAccrual
	--	set LeaveId = @leaveId, 
	--	  WorkingWeekId = @workingWeekId, 
	--	  AccrualTimeId = @accrualTimeId, 
	--	  AccrualRate = @accrualRate, 
 --         MaximumThreshold = @maximumThreshold, 
 --         Active = @active, 
 --         UpdatedBy = @createdBy, 
 --         DateUpdated = GETDATE()
	--	where leaveId = @leaveId and workingWeekId = @workingWeekId		
	select @@rowcount
END