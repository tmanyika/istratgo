﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_BulkSheet_GetByName]
	@SheetName varchar(150)
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	select 1 as RowIdx, SheetId, SheetName, DbTableName, SheetDescription, SheetNotes, Active, OrderNumber, CreatedBy, DateCreated, UpdatedBy, DateUpdated
	from imd_user_db.BulkSheet
	where (Active = 1 and SheetName like @SheetName)
END