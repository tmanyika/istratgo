﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_SSIS_BatchEmailRelay] 
	
AS
BEGIN

set nocount on;
set xact_abort on;

declare @i int, @rec int, @batchId int, @empId int,
		@outboxId int, @todayDate datetime;
declare @subject varchar(2000), @email varchar(100),
		@sqlProfile varchar(100), @body varchar(max);
		
begin try

set @i = 1;
set @rec = 0;
set @sqlProfile = imd_user_db.fxMailProfileName();

-- Get rid of mails already sent
delete imd_user_db.MailOutbox where IsSent = 1
	
begin tran
	
	-- Get emails to be relayed - Reminder Emails
	insert into imd_user_db.MailOutbox(EmployeeId, FullName, Email, BatchId, MailSubject, MailContent, MainTemplate, IsSent, DateToBeSent)
	select distinct EmployeeId, FullName, Email, BatchId, MailSubject, MailContent, MainTemplate, IsSent, DateToBeSent from imd_user_db.vw_BatchMailReminder (nolock)
	
	-- Get emails to be relayed - Batch Emails
	insert into imd_user_db.MailOutbox(EmployeeId, FullName, Email, BatchId, MailSubject, MailContent, MainTemplate, IsSent, DateToBeSent)
	select distinct EmployeeId, FullName, Email, BatchId, MailSubject, MailContent, MainTemplate, IsSent, DateToBeSent from imd_user_db.vw_BatchMailReminderNormal (nolock)
	
	select OutBoxId, EmployeeId, FullName, Email, BatchId, MailSubject, MailContent, MainTemplate, row_number() over(order by EmployeeId asc) as rId
	into #outbox
	from imd_user_db.MailOutbox
	where IsSent = 0

	select @rec=count(EmployeeId) from #outbox
	create table #list(email varchar(100),batchId int,employeeId int);
	while (@i <= @rec)
		begin
			select @outboxId = OutBoxId, @batchId = BatchId, @empId = EmployeeId, @email = Email, @body = replace(replace(MainTemplate,'{0}',FullName),'{1}', MailContent) , @subject = MailSubject from #outbox (nolock) where rId = @i
			
			if (not(exists(select email from #list where (@empId = employeeId or email = @email) and batchId = @batchId))) 
				begin
					if(@email <> '' or @email is not null)
						exec msdb.dbo.sp_send_dbmail 
							@profile_name=@sqlProfile,
							@importance = 'NORMAL',
							@recipients = @email,
							@subject = @subject,
							@body = @body,
							@body_format = 'HTML'
							
					insert into #list(email,batchId,employeeId)
					values(@email,@batchId,@empId)
				end
			
			exec imd_user_db.Proc_SSIS_MailBatchUserListInsert @mbatchId = @batchId, @mempId = @empId, @memail = @email, @moutboxId = @outboxId			
			set @i = @i + 1;
		end
	
	-- delete queued emails from the OutBox
	delete imd_user_db.MailOutbox from imd_user_db.MailOutbox o inner join imd_user_db.MailBatchUserList b 
	on o.BatchId = b.BatchId and o.EmployeeId = b.EmployeeId
	
	-- Updating Batch Count Statistics
	select batchId, count(employeeId) as total, row_number() over(order by batchId asc) as rId
	into #stats
	from imd_user_db.MailBatchUserList
	group by batchId
	
	update imd_user_db.MailBatch
	set imd_user_db.MailBatch.NoOfUsers = s.total,
		imd_user_db.MailBatch.IsSent = 1
	from  imd_user_db.MailBatch b inner join #stats s on b.batchId = s.batchId
	
	-- End of Updating Batch Count Statistics	 
	drop table #stats
	drop table #outbox
	drop table #list	
commit tran
end try
begin catch
	if(@@trancount > 0)
		rollback tran
	-- record errors into the ProcError table
	insert into imd_user_db.ProcError( ErrorNo, ErrorSeverity, ErrorState, ErrorProcedure, ErrorLine, ErrorMessage, DateCreated)	
	select
        ERROR_NUMBER() AS ErrorNumber
        ,ERROR_SEVERITY() AS ErrorSeverity
        ,ERROR_STATE() AS ErrorState
        ,ERROR_PROCEDURE() AS ErrorProcedure
        ,ERROR_LINE() AS ErrorLine
        ,ERROR_MESSAGE() AS ErrorMessage
        ,GETDATE() as DateCreated
end catch
END