﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_TalentMatrix_GetAll] 
	@companyId int
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	select MatrixId, CompanyId, TalentMatrixName, TalentMatrixDescription, LowerBoundSign, LowerBound, UpperBoundSign, UpperBound, CreatedBy, DateCreated, 
           Active, UpdatedBy, DateUpdated, PositionInMatrix
	from  imd_user_db.TalentMatrix
	where (CompanyId = @companyId)
	order by PositionInMatrix
END