﻿


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_BulkImport_JobTitles]
	@CompanyId int,
	@CreatedBy varchar(50) = null
AS
BEGIN
	set nocount on;
	set xact_abort on;
	
	declare @currDate datetime,
			@jobAreaid int;
	begin try
		set @jobAreaId = 32;
		set @currDate = getdate();	
			
		--Trim white spaces on the job title
		update imd_user_db.bulkJobTitle 
		set JobTitleName =  imd_user_db.PropCaseDelDoubleSpace(JobTitleName)
		where CompanyId = @CompanyId
		
		 --Delete invalid data and Rebuild All Table Indexes
		delete from imd_user_db.bulkJobTitle where (JobTitleName is null or JobTitleName = '')
		alter index all on imd_user_db.bulkJobTitle rebuild with (fillfactor = 80)
	
		--Get missing Job Titles only to avoid duplication
		select distinct JobTitleName, CompanyId into #job
		from
			(
				select JobTitleName, CompanyId
				from imd_user_db.bulkJobTitle with(nolock)
				where CompanyId = @CompanyId 
				union
				select imd_user_db.ProperCase(JobTitle) as JobTitle, CompanyId
				from imd_user_db.bulkEmployee with(nolock)
				where CompanyId = @CompanyId
				union
				select imd_user_db.ProperCase(AreaOfMeasure) as JobTitle, CompanyId
				from  imd_user_db.vw_BulkAdditionalData with(nolock)
				where (CompanyId = @CompanyId and AreaId = @JobAreaId)					 
			) as data
		where CompanyId = @CompanyId and (JobTitleName not in (select NAME 
																from imd_user_db.COMPANY_JOB_TITLES (nolock)
																where COMPANY_ID = @CompanyId
															   ) 
										  )	
		--Insert new Job Titles
		begin tran
			insert into imd_user_db.COMPANY_JOB_TITLES(COMPANY_ID,NAME,ACTIVE,CREATEDBY,DATECREATED)
			select CompanyId, JobTitleName, 1 as Active, @CreatedBy as CreatedBy, @currDate as DateCreated
			from [#job] with(nolock)
			where CompanyId = @CompanyId
		commit tran
		drop table [#job]
	end try
	begin catch
		set @jobAreaid = 0
		if(@@trancount > 0)
			rollback tran
		
		insert into imd_user_db.ProcError(ErrorNo, ErrorSeverity, ErrorState, ErrorProcedure, ErrorLine, ErrorMessage, DateCreated)
		select error_number() AS ErrorNumber, error_severity() AS ErrorSeverity, error_state() AS ErrorState, 
						error_procedure() AS ErrorProcedure, error_line() AS ErrorLine, error_message() AS ErrorMessage, 
						getdate() as DateCreated	
	end catch
	
	select @jobAreaid
END