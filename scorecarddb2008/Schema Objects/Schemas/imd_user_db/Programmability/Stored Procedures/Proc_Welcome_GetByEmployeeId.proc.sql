﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_Welcome_GetByEmployeeId]
	@employeeId int
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	select WelcomeId, WelcomeSectionName, VirtualPath, Active, EmployeeId, ShowAgain, Position, WelcomeDisplayCaption, PopupPageName
	from imd_user_db.vw_UserWelcome
	where EmployeeId = @employeeId
	order by Position
END