﻿


CREATE PROCEDURE [imd_user_db].[PROC_GET_USERS]

AS
begin
	set nocount on;
	set transaction isolation level read uncommitted;

	select     ua.ID, ua.LOGIN_ID, ua.[PASSWORD], ua.ORG_ID, ua.JOB_TITLE_ID, ua.NAME, ua.EMAIL_ADDRESS, ua.ACTIVE, ua.REPORTS_TO, ua.USER_TYPE, 
						  isnull(cd.COUNTRY,277) AS countryId
	from         imd_user_db.USER_ADMIN AS ua INNER JOIN
						  imd_user_db.COMPANY_STRUCTURE AS cs ON ua.ORG_ID = cs.ID INNER JOIN
						  imd_user_db.COMPANY_DETAIL AS cd ON cs.COMPANY_ID = cd.COMPANY_ID
end