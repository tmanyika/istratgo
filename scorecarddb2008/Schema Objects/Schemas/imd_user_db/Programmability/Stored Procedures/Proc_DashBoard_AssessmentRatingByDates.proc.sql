﻿
CREATE PROCEDURE [imd_user_db].[Proc_DashBoard_AssessmentRatingByDates] 
	@CRITERIA_TYPE_ID int,
	@SELECTED_ITEM_VALUE int,
	@STARTDATE datetime,
	@ENDDATE datetime
as
begin
	set nocount on;
	set transaction isolation level read uncommitted;
	
	select ItemName, PeriodMonth, PeriodYear, CRITERIA_TYPE_ID, SELECTED_ITEM_VALUE, 
		   sum(ActualValue)/cast(count(PeriodMonth) as decimal(18,2)) as ActualValue,
		   sum(DefinedActualValue)/cast(count(PeriodMonth) as decimal(18,2)) as DefinedActualValue		   	 
	from
		(
			select CRITERIA_TYPE_ID, 
				   SELECTED_ITEM_VALUE, 
				   PERIOD_DATE, 
				   PERIODMONTH, 
				   PERIODYEAR,
				   datename(m,PERIOD_DATE) as ItemName, 
				   sum(FINAL_SCORE) as ActualValue, 
				   sum(FINAL_SCORE_AGREED) as DefinedActualValue
			from imd_user_db.vw_BaseScoreReport
			where (CRITERIA_TYPE_ID = @CRITERIA_TYPE_ID 
				  AND SELECTED_ITEM_VALUE = @SELECTED_ITEM_VALUE
				  AND PERIOD_DATE BETWEEN @STARTDATE AND @ENDDATE)
			group by CRITERIA_TYPE_ID, SELECTED_ITEM_VALUE, PERIOD_DATE, PeriodMonth, PeriodYear
		) as data
	group by ItemName, PeriodMonth, PeriodYear, CRITERIA_TYPE_ID, SELECTED_ITEM_VALUE
	order by CRITERIA_TYPE_ID asc, SELECTED_ITEM_VALUE asc, PeriodYear asc, PeriodMonth asc
end