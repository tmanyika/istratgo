﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[PROC_GET_PERSPECTIVE]
	@PERSPECTIVE_ID int
AS
BEGIN
	SET NOCOUNT ON;
	SELECT     PERSPECTIVE_ID, COMPANY_ID, PERSPECTIVE_NAME, PERSPECTIVE_DESC, ACTIVE,  CREATED_BY, DATE_CREATED, DATE_UPDATED, UPDATE_BY
	FROM         COMPANY_PERSPECTIVE WITH (nolock)
	WHERE     (PERSPECTIVE_ID = @PERSPECTIVE_ID)
END