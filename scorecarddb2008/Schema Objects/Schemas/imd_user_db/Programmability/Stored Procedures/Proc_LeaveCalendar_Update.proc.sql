﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_LeaveCalendar_Update]
	@holidayDateId int,
	@countryId int, 
	@holidayDate datetime, 
	@detail varchar(100),
    @calendarYear int, 
    @active bit, 
    @updatedBy varchar(20)
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	if not(exists(select holidaydate from imd_user_db.LeaveCalendar where HolidayDateId <> @holidayDateId and CountryId = @countryId and datediff(dd,HolidayDate,@holidayDate) = 0))
		update imd_user_db.LeaveCalendar
		set CountryId = @countryId, 
			CalendarYear = @calendarYear,
			HolidayDate = @holidayDate, 
			Detail = @detail, 
			Active = @active, 
			UpdatedBy = @updatedBy, 
			DateUpdated = GETDATE()
		where HolidayDateId = @holidayDateId
	select @@rowcount
END