﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_EmploymentHist_Insert] 
	@EmployeeId int,
	@PrevPosId int = null,
	@JobTitleId int,
	@StartDate datetime,
	@EndDate datetime = null,
	@CreatedBy varchar(20)
AS
BEGIN
	set nocount on;
	set xact_abort on;
	
	declare @HistoryId int;
	declare @DaysInAYear decimal(18,2) = 365.00;
	declare @DaysOnSameRole decimal(18,2) = datediff(d,@StartDate,getdate());
	declare @YearsOnSameRole decimal(18,2) = @DaysOnSameRole/@DaysInAYear;
	
	begin try 
		begin tran
			if not(exists(select EmployeeId from EmploymentHist where EmployeeId = @EmployeeId and JobTitleId = @JobTitleId and datediff(d,@StartDate,StartDate) = 0))
				begin
					insert into imd_user_db.EmploymentHist(EmployeeId, JobTitleId, StartDate, EndDate, DaysOnSameRole, YearsOnSameRole, Active, CreatedBy, DateCreated)
					values(@EmployeeId, @JobTitleId, @StartDate, @EndDate, @DaysOnSameRole, @YearsOnSameRole, 0, @CreatedBy, getdate())
					select @HistoryId=scope_identity()
				end
			else
				select top 1 @HistoryId = HistoryId from imd_user_db.EmploymentHist
				where EmployeeId = @EmployeeId order by StartDate desc
			
			update imd_user_db.EmploymentHist
			set Active = case when HistoryId = @HistoryId then 1 
						 else 0 end,
				PreviousPosition = case when JobTitleId = @PrevPosId then 1 
								else 0 end
			where EmployeeId = @EmployeeId
		commit tran
	end try
	begin catch
		if(@@trancount > 0)
			rollback tran
	end catch
END