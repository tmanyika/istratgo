﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[PROC_ADD_PROJECT]
	@COMPANY_ID int,
	@PROJECT_NAME varchar(200),
	@PROJECT_DESC varchar(400),
	@CREATED_BY varchar(20),
	@RESPONSIBLE_PERSON_ID int = NULL
AS
BEGIN
	SET NOCOUNT ON;
	INSERT INTO COMPANY_PROJECT
                      (RESPONSIBLE_PERSON_ID,COMPANY_ID, PROJECT_NAME, PROJECT_DESC, ACTIVE, CREATED_BY, DATE_CREATED, DATE_UPDATED, UPDATE_BY)
	VALUES     (@RESPONSIBLE_PERSON_ID,@COMPANY_ID, @PROJECT_NAME, @PROJECT_DESC, 1, @CREATED_BY, getdate(), null, null)
	SELECT @@rowcount
END