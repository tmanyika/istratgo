﻿


CREATE PROCEDURE [imd_user_db].[PROC_ADD_ORG_UNIT]
            @COMPANY_ID int
           ,@ORGUNIT_NAME varchar(50)
           ,@PARENT_ORG int 
           ,@ORGUNT_TYPE int 
           ,@OWNER_ID INT
           ,@CREATED_BY varchar(20)
                    
as
begin
set nocount on;
set transaction isolation level read uncommitted;

	insert into [imd_user_db].[COMPANY_STRUCTURE]
           (
           [COMPANY_ID]
           ,[ORGUNIT_NAME]
           ,[PARENT_ORG]
           ,[ORGUNT_TYPE]
           ,OWNER_ID
           ,CREATED_BY
           ,DATE_CREATED
           ,DATE_REGISTERED
           ,ACTIVE
           )
    values
           (
            @COMPANY_ID
           ,@ORGUNIT_NAME
           ,@PARENT_ORG
           ,@ORGUNT_TYPE
           ,@OWNER_ID
           ,@CREATED_BY
           ,getdate()
           ,getdate()
           ,1
           )
   select @@rowcount
end