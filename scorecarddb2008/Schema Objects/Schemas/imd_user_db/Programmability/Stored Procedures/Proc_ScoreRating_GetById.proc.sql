﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_ScoreRating_GetById]
	@RatingId int
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	select RatingId, CompanyId, RatingValue, MinimumValue, MaximumValue, RatingDescription, Active, CreatedBy, DateCreated, DateUpdated, UpdatedBy
	from imd_user_db.ScoreRating
	where RatingId = @RatingId
END