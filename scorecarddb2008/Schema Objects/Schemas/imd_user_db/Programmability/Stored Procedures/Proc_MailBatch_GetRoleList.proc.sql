﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_MailBatch_GetRoleList] 
	@batchId int
AS
begin
	set nocount on;
	set transaction isolation level read uncommitted;
	
	declare @i int, @rec int, @roleList varchar(1000), @roleName varchar(50);
	select row_number() over(order by b.ListingId asc) as rId, u.[TYPE_NAME] as roleName
	into #BRole
	from imd_user_db.MailBatchUserRole as b 
		inner join imd_user_db.UTILITIES as u on b.RoleId = u.ID
	where b.BatchId = @batchId
	
	set @i = 1;
	select @rec=isnull(count(rId),0) from #BRole
	
	while(@i <= @rec)
		begin
			select @roleName=roleName from #BRole where @i = rId
			set @roleList = '<li>' + @roleName + '</li>'
			set @i = @i + 1;
		end	
	drop table #BRole
	
	set @roleList = '<ul>' + @roleList + '</ul>'
	select @roleList
END