﻿

CREATE PROCEDURE [imd_user_db].[Proc_DashBoard_FocusAreas1] 
	@CRITERIA_TYPE_ID int,
	@OrgUnitId int,
	@IsCompany bit
as
begin
	set nocount on;
	set transaction isolation level read uncommitted;
	
	declare @maxdate datetime,
			@valueId int;
	
	if(@IsCompany = 1)
		select top 1 @maxdate=max(PERIOD_DATE),@valueId=OrgUnitId 
		from imd_user_db.vw_FocusAreaDates 
		where OrgUnitId = @OrgUnitId and CRITERIA_TYPE_ID = @CRITERIA_TYPE_ID
		group by OrgUnitId,PERIOD_DATE
	else
		begin
			set @valueId = @OrgUnitId	
			select @maxdate=max(s.PERIOD_DATE) 
			from imd_user_db.INDIVIDUAL_SCORES as s 
			inner join imd_user_db.Employee as e 
			ON s.SELECTED_ITEM_VALUE = e.EmployeeId
			where @CRITERIA_TYPE_ID = CRITERIA_TYPE_ID 
		end
		
	select isc.ID, isc.CRITERIA_TYPE_ID, isc.SELECTED_ITEM_VALUE, isc.FINAL_SCORE, ig.[WEIGHT], ig.[TARGET], isc.PERIOD_DATE, cp.PERSPECTIVE_NAME, 
           cp.PERSPECTIVE_ID
	from  imd_user_db.INDIVIDUAL_SCORES AS isc INNER JOIN
		  imd_user_db.INDIVIDUAL_GOALS AS ig ON isc.GOAL_ID = ig.ID INNER JOIN
		  imd_user_db.COMPANY_STRATEGIC AS cs ON ig.STRATEGIC_ID = cs.ID INNER JOIN
		  imd_user_db.COMPANY_PERSPECTIVE AS cp ON cs.PERSPECTIVE = cp.PERSPECTIVE_ID LEFT OUTER JOIN
		  imd_user_db.UTILITIES AS u ON ig.UNIT_MEASURE = u.ID INNER JOIN
		  imd_user_db.SUBMISSIONS AS sb ON isc.SUBMISSION_ID = sb.ID
	where (sb.[STATUS] = 40 AND isc.PARENT_ID IS NULL) AND (isc.SELECTED_ITEM_VALUE = @valueId) AND (isc.CRITERIA_TYPE_ID = @CRITERIA_TYPE_ID) AND (DATEDIFF(dd, @maxdate, isc.PERIOD_DATE) = 0)
	order by cp.PERSPECTIVE_NAME
end