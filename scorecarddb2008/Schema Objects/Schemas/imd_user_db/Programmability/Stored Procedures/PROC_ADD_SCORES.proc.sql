﻿

CREATE PROCEDURE [imd_user_db].[PROC_ADD_SCORES]
 @CRITERIA_TYPE_ID INT ,
 @SELECTED_ITEM_VALUE INT ,
 @GOAL_ID INT,
 @SCORES VARCHAR(250) = null,
 @FINAL_SCORE DECIMAL(18,2),
 @SUBMISSION_ID INT,
 @PERIOD_DATE DATETIME,
 @JUSTIFICATION_COMMENT VARCHAR(4000),
 @RATINGVALUE DECIMAL(18,2) = null,
 @FINAL_SCORE_AGREED DECIMAL(18,2) = null,
 @EVIDENCE VARCHAR(4000) = null,
 @HAS_SUB_GOALS bit = 0,
 @PARENT_ID int = null
AS 
begin
	set nocount on;
	set transaction isolation level read uncommitted;
	
	declare @responsible_uid int,
	        @id int;
	
	set @id = 0;
	if(@CRITERIA_TYPE_ID = 33) --organisation unit
		select @responsible_uid=OWNER_ID from imd_user_db.COMPANY_STRUCTURE where id = @SELECTED_ITEM_VALUE
	else if(@CRITERIA_TYPE_ID = 49) --Project
		select @responsible_uid=RESPONSIBLE_PERSON_ID from imd_user_db.COMPANY_PROJECT where PROJECT_ID = @SELECTED_ITEM_VALUE
	else -- users
		set @responsible_uid = @SELECTED_ITEM_VALUE
			
	insert into imd_user_db.INDIVIDUAL_SCORES 
	(
		 CRITERIA_TYPE_ID,
		 SELECTED_ITEM_VALUE,
		 GOAL_ID,
		 SCORES,
		 FINAL_SCORE,
		 SUBMISSION_ID,
		 PERIOD_DATE,
		 RESPONSIBLE_USER_ID,
		 JUSTIFICATION_COMMENT,
		 RATINGVALUE,
		 FINAL_SCORE_AGREED,
		 EVIDENCE,
		 MANAGER_SCORE,
		 AGREED_SCORE,
		 HAS_SUB_GOALS,
		 PARENT_ID
	)
	values
	(
		 @CRITERIA_TYPE_ID,
		 @SELECTED_ITEM_VALUE,
		 @GOAL_ID,
		 @SCORES,
		 @FINAL_SCORE,
		 @SUBMISSION_ID,
		 @PERIOD_DATE,
		 @responsible_uid,
		 @JUSTIFICATION_COMMENT,
		 @RATINGVALUE,
		 @FINAL_SCORE_AGREED,
		 @EVIDENCE,
		 @SCORES,
		 @SCORES,
		 @HAS_SUB_GOALS,
		 @PARENT_ID
	)
	select @id=@@identity
	select @id
END