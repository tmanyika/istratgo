﻿
CREATE PROCEDURE [imd_user_db].[PROC_GET_SCORES_REPORT_EMPLOYEES_BYJOBTITLE] 
	@CRITERIA_TYPE_ID int,
	@SELECTED_ITEM_VALUE int,
	@SSCORE_DATE datetime,
	@ESCORE_DATE datetime
as
begin
	set nocount on;
	set transaction isolation level read uncommitted;
	
	select     isc.ID, isc.CRITERIA_TYPE_ID, isc.SELECTED_ITEM_VALUE, isc.GOAL_ID, isc.SCORES, isc.FINAL_SCORE, isc.DATE_CREATED, isc.SUBMISSION_ID, ig.WEIGHT, 
                      ig.TARGET, ig.GOAL_DESCRIPTION, isc.PERIOD_DATE, cp.PERSPECTIVE_NAME, cs.OBJECTIVE, u.TYPE_NAME AS UNIT_MEASURE, cp.PERSPECTIVE_ID, 
                      ISNULL(e.FirstName, '') + ' ' + ISNULL(e.MiddleName, '') + ' ' + e.LastName AS ItemName, e.EmployeeId AS ItemId, e.JobTitleId, cjt.NAME AS JobTitleName
				,isc.MANAGER_SCORE, isc.AGREED_SCORE, isc.RATINGVALUE, isc.FINAL_SCORE_AGREED, u.[TYPE_NAME] AS UNIT_MEASURE_NAME
	from         imd_user_db.UTILITIES AS u RIGHT OUTER JOIN
                      imd_user_db.INDIVIDUAL_SCORES AS isc INNER JOIN
                      imd_user_db.INDIVIDUAL_GOALS AS ig ON isc.GOAL_ID = ig.ID INNER JOIN
                      imd_user_db.COMPANY_STRATEGIC AS cs ON ig.STRATEGIC_ID = cs.ID INNER JOIN
                      imd_user_db.COMPANY_PERSPECTIVE AS cp ON cs.PERSPECTIVE = cp.PERSPECTIVE_ID INNER JOIN
                      imd_user_db.Employee AS e INNER JOIN
                      imd_user_db.COMPANY_JOB_TITLES AS cjt ON e.JobTitleId = cjt.ID ON isc.SELECTED_ITEM_VALUE = e.EmployeeId ON u.ID = ig.UNIT_MEASURE INNER JOIN
			          imd_user_db.SUBMISSIONS AS sb ON isc.SUBMISSION_ID = sb.ID
	where  (sb.[STATUS] = 40) AND 
	(e.JobTitleId = @SELECTED_ITEM_VALUE) AND 
	(isc.CRITERIA_TYPE_ID = @CRITERIA_TYPE_ID) AND 
	(
		isc.PERIOD_DATE BETWEEN @SSCORE_DATE AND @ESCORE_DATE
	)
	order by e.FirstName, e.LastName
end