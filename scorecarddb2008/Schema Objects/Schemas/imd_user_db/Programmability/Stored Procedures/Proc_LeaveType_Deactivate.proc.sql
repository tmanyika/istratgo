﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE imd_user_db.Proc_LeaveType_Deactivate
	@leaveId int,
	@updatedBy varchar(20)
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	update  imd_user_db.LeaveType
	set     Active = 0, 
			UpdatedBy = @updatedBy, 
			DateUpdated = GETDATE()
	where   (leaveId = @leaveId)
		
	select @@rowcount
END