﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE imd_user_db.Proc_ScoreRating_Deactivate
	@RatingId int,
	@Active bit,
	@UpdatedBy varchar(50)
AS
BEGIN
	set nocount on;
	
	update imd_user_db.ScoreRating
	set Active = @Active, 
	DateUpdated = getdate(), 
	UpdatedBy = @UpdatedBy
	where RatingId = @RatingId
	select @@rowcount
END