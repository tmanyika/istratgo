﻿
CREATE PROCEDURE [imd_user_db].[PROC_GET_STRATEGIC_BY_ID]
@ID INT
AS
begin
set nocount on;
set transaction isolation level read uncommitted;

select [ID]
      ,[COMPANY_ID]    
      ,[PERSPECTIVE]
      ,[OBJECTIVE]
      ,[RATIONALE]
      ,[DATE_CREATED]
      ,[ACTIVE]
      ,[CREATEDBY]
      ,[UPDATED_BY]
      ,[DATE_UPDATED]
from [COMPANY_STRATEGIC]
where ID =@ID
end