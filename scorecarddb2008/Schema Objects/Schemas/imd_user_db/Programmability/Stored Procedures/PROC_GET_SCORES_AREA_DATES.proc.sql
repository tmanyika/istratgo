﻿CREATE PROCEDURE [imd_user_db].[PROC_GET_SCORES_AREA_DATES] 
	@CRITERIA_TYPE_ID INT,
	@SubXml XML
as
begin
	set transaction isolation level READ uncommitted;
	set nocount on;
	select	nref.value('Id[1]', 'int') as areaValueId
	into #vid
	from   @SubXml.nodes('//Record/Score') AS R(nref)
	
	select distinct 
                      CONVERT(varchar(10), isc.PERIOD_DATE, 121) AS PERIOD_DATE, CONVERT(varchar(11), isc.PERIOD_DATE, 106) AS PERIOD_DATE_DESC
	from         INDIVIDUAL_SCORES AS isc INNER JOIN
						  INDIVIDUAL_GOALS AS ig ON isc.GOAL_ID = ig.ID INNER JOIN
						  SUBMISSIONS AS s WITH (nolock) ON isc.SUBMISSION_ID = s.ID INNER JOIN
						  COMPANY_STRATEGIC AS cs ON ig.STRATEGIC_ID = cs.ID
	where     (isc.CRITERIA_TYPE_ID = @CRITERIA_TYPE_ID) 
			AND (s.[STATUS] = 40) 
			AND (isc.SELECTED_ITEM_VALUE in (select areaValueId from #vid))
	order by PERIOD_DATE
	drop table #vid
end