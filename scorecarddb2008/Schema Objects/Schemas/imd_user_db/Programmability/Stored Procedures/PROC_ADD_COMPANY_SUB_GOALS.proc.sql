﻿
CREATE PROCEDURE [imd_user_db].[PROC_ADD_COMPANY_SUB_GOALS]
@PARENT_ID INT,
@STRATEGIC_ID INT,
@AREA_TYPE_ID INT,
@GOAL_DESCRIPTION VARCHAR(150),
@UNIT_MEASURE int,
@TARGET VARCHAR(150),
@WEIGHT int,
@AREA_ID INT,
@COMPANY_ID INT = NULL,
@RATIONAL VARCHAR(1000) = NULL
AS
BEGIN
	set nocount on;
	declare @rowsaffected int = 0;
	
	begin try
	 begin tran
			insert into imd_user_db.INDIVIDUAL_GOALS
			(
				 PARENT_ID,
				 STRATEGIC_ID,
				 AREA_TYPE_ID,
				 GOAL_DESCRIPTION,
				 UNIT_MEASURE,
				 [TARGET],
				 [WEIGHT],
				 AREA_ID,
				 RATIONAL,
				 ACTIVE
			)
			values
			( 
				@PARENT_ID,
				@STRATEGIC_ID,
				@AREA_TYPE_ID,
				@GOAL_DESCRIPTION,
				@UNIT_MEASURE,
				@TARGET,
				@WEIGHT,
				@AREA_ID,
				@RATIONAL,
				1
			)			
			select @rowsaffected=@@rowcount
			/*commented out updating of unit measure and target as it is affecting historical data*/
			update imd_user_db.INDIVIDUAL_GOALS
			set HAS_SUB_GOALS = 1 --,
			    --UNIT_MEASURE = null,
			    --[TARGET] = null			    
			where ID = @PARENT_ID
	  commit tran
	end try
	begin catch
		set @rowsaffected = 0
	end catch
	select @rowsaffected
END