﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE imd_user_db.Database_DeleteAllData
	AS
BEGIN
	set nocount on;
	set xact_abort on;
 
	--Keep company ID 146, 183, 205
	begin try
		  begin tran
		  delete from  imd_user_db.COMPANY_DETAIL
		  where COMPANY_ID not in (55, 146, 183, 205)
		  delete from  imd_user_db.COMPANY_JOB_TITLES
		  where COMPANY_ID not in (55, 146, 183, 205)
		  commit tran
	print 'Successful'
	end try
	begin catch
		  if(@@TRANCOUNT > 0)
				rollback tran
		  print 'Failed'
		  select error_number() AS ErrorNumber, error_severity() AS ErrorSeverity, error_state() AS ErrorState,
										error_procedure() AS ErrorProcedure, error_line() AS ErrorLine, error_message() AS ErrorMessage,
										getdate() as DateCreated
	end catch
END