﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[PROC_UPDATE_PERSPECTIVE]
	@PERSPECTIVE_ID int,
	@PERSPECTIVE_NAME varchar(1000),
	@PERSPECTIVE_DESC varchar(1000),
	@UPDATED_BY varchar(80)	
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE  COMPANY_PERSPECTIVE
	SET		PERSPECTIVE_NAME = @PERSPECTIVE_NAME,
			PERSPECTIVE_DESC =  @PERSPECTIVE_DESC,
			DATE_UPDATED = getdate(), UPDATE_BY = @UPDATED_BY
	where PERSPECTIVE_ID = @PERSPECTIVE_ID
	SELECT @@rowcount
END