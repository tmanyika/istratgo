﻿

CREATE PROCEDURE [imd_user_db].[Proc_DashBoard_FocusAreas]
	@Criteria_Type_Id int,
	@OrgUnitId int,
	@IsCompany bit
as
begin
set nocount on;
set transaction isolation level read uncommitted;

declare @maxdate datetime,
		@valueId int,
		@selValue int,
		@crValue int;


select @selValue=SELECTED_ITEM_VALUE, @crValue=CRITERIA_TYPE_ID, @maxdate=max(PERIOD_DATE) 
from imd_user_db.vw_CurrentFocusAreaDashboard (nolock)
where orgUnitId = @OrgUnitId
group by CRITERIA_TYPE_ID, SELECTED_ITEM_VALUE
--select @crValue as crValue, @selValue as selvalue, @maxdate as maxdate 
select isc.ID, isc.CRITERIA_TYPE_ID, isc.SELECTED_ITEM_VALUE, isc.FINAL_SCORE, ig.[WEIGHT], ig.[TARGET], isc.PERIOD_DATE, cp.PERSPECTIVE_NAME, 
       cp.PERSPECTIVE_ID
from  imd_user_db.INDIVIDUAL_SCORES AS isc INNER JOIN
	  imd_user_db.INDIVIDUAL_GOALS AS ig ON isc.GOAL_ID = ig.ID INNER JOIN
	  imd_user_db.COMPANY_STRATEGIC AS cs ON ig.STRATEGIC_ID = cs.ID INNER JOIN
	  imd_user_db.COMPANY_PERSPECTIVE AS cp ON cs.PERSPECTIVE = cp.PERSPECTIVE_ID INNER JOIN
	  imd_user_db.SUBMISSIONS AS sb ON isc.SUBMISSION_ID = sb.ID 
where (sb.[STATUS] = 40 AND isc.PARENT_ID IS NULL) AND (isc.SELECTED_ITEM_VALUE = @selValue) AND  (isc.CRITERIA_TYPE_ID = @crValue) AND (DATEDIFF(dd, @maxdate, isc.PERIOD_DATE) = 0)
order by cp.PERSPECTIVE_NAME
end