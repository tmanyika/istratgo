﻿
CREATE PROCEDURE [imd_user_db].[PROC_GET_SCORES_REPORT] 
	@CRITERIA_TYPE_ID INT,
	@SELECTED_ITEM_VALUE int,
	@SCORE_DATE datetime
as
begin
	set nocount on;
	set transaction isolation level read uncommitted;
	
	select     isc.ID, isc.CRITERIA_TYPE_ID, isc.SELECTED_ITEM_VALUE, isc.GOAL_ID, isc.SCORES, isc.FINAL_SCORE, isc.DATE_CREATED, isc.SUBMISSION_ID, isnull(ig.[WEIGHT],pw.[WEIGHT]) as [WEIGHT], 
                      ig.[TARGET], ig.GOAL_DESCRIPTION, isc.PERIOD_DATE, cp.PERSPECTIVE_NAME, cs.OBJECTIVE, u.[TYPE_NAME] AS UNIT_MEASURE, cp.PERSPECTIVE_ID, 
                      isc.MANAGER_SCORE, isc.AGREED_SCORE, isc.JUSTIFICATION_COMMENT, isc.MANAGER_COMMENT, isc.RATINGVALUE, isc.FINAL_SCORE_AGREED,
                      cs.RATIONALE, isc.EVIDENCE, isc.PARENT_ID, isc.HAS_SUB_GOALS
	from         imd_user_db.INDIVIDUAL_SCORES AS isc INNER JOIN
                      imd_user_db.INDIVIDUAL_GOALS AS ig ON isc.GOAL_ID = ig.ID INNER JOIN
                      imd_user_db.COMPANY_STRATEGIC AS cs ON ig.STRATEGIC_ID = cs.ID INNER JOIN
                      imd_user_db.COMPANY_PERSPECTIVE AS cp ON cs.PERSPECTIVE = cp.PERSPECTIVE_ID LEFT OUTER JOIN
                      imd_user_db.UTILITIES AS u ON ig.UNIT_MEASURE = u.ID INNER JOIN
			          imd_user_db.SUBMISSIONS AS sb ON isc.SUBMISSION_ID = sb.ID LEFT OUTER JOIN
			          imd_user_db.vw_ParentGoalWeights pw on ig.PARENT_ID = pw.ID
	where  (sb.[STATUS] = 40) AND (isc.SELECTED_ITEM_VALUE = @SELECTED_ITEM_VALUE) AND (isc.CRITERIA_TYPE_ID = @CRITERIA_TYPE_ID) AND (DATEDIFF(d, @SCORE_DATE, isc.PERIOD_DATE) = 0)
	order by cp.PERSPECTIVE_NAME, cs.OBJECTIVE
end