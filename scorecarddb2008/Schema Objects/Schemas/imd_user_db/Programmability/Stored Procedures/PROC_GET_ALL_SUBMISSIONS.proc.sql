﻿
CREATE PROCEDURE [imd_user_db].[PROC_GET_ALL_SUBMISSIONS]
AS
begin
set nocount on;
set transaction isolation level read uncommitted;

select     s.ID, s.SELECTED_ITEM_VALUE, s.CRITERIA_TYPE_ID, s.APPROVER_ID, s.[STATUS], s.DATE_SUBMITTED, c.COMPANY_ID, s.OVERALL_COMMENT, 
                      s.DATE_UPDATED
from         imd_user_db.SUBMISSIONS AS s INNER JOIN
                      imd_user_db.Employee AS a ON s.APPROVER_ID = a.EmployeeId INNER JOIN
                      imd_user_db.COMPANY_STRUCTURE AS c ON a.OrgUnitId = c.ID
end