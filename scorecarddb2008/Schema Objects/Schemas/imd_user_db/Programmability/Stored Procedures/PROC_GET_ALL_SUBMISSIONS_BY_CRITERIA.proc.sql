﻿
CREATE PROCEDURE [imd_user_db].[PROC_GET_ALL_SUBMISSIONS_BY_CRITERIA]
@CompanyId int,
@UserId int,
@IsAdmin bit = 0
AS
begin
set nocount on;
set transaction isolation level read uncommitted;

--select     s.ID, s.SELECTED_ITEM_VALUE, s.CRITERIA_TYPE_ID, s.APPROVER_ID, s.[STATUS], s.DATE_SUBMITTED, c.COMPANY_ID, s.OVERALL_COMMENT, 
--                      s.DATE_UPDATED
--from         imd_user_db.SUBMISSIONS AS s INNER JOIN
--                      imd_user_db.Employee AS a ON s.APPROVER_ID = a.EmployeeId INNER JOIN
--                      imd_user_db.COMPANY_STRUCTURE AS c ON a.OrgUnitId = c.ID
if(@IsAdmin = 1)
	SELECT     FirstName, MiddleName, LastName, EmailAddress, ID, SELECTED_ITEM_VALUE, CRITERIA_TYPE_ID, APPROVER_ID, [STATUS], JOB_TITLE_ID, OVERALL_COMMENT, DATE_SUBMITTED, 
                      DATE_UPDATED, CAPTURER_ID, CriteriaDescription, StatusDescription, LineManagerEmployeeId, OrgUnitId, COMPANY_ID, ORGUNIT_NAME
	FROM         vw_Submissions
	WHERE (COMPANY_ID = @CompanyId AND [STATUS] IN (42,331))
else
	SELECT     FirstName, MiddleName, LastName, EmailAddress, ID, SELECTED_ITEM_VALUE, CRITERIA_TYPE_ID, APPROVER_ID, [STATUS], JOB_TITLE_ID, OVERALL_COMMENT, DATE_SUBMITTED, 
                      DATE_UPDATED, CAPTURER_ID, CriteriaDescription, StatusDescription, LineManagerEmployeeId, OrgUnitId, COMPANY_ID, ORGUNIT_NAME
	FROM         vw_Submissions
	WHERE (COMPANY_ID = @CompanyId AND [STATUS] IN (42,331) AND APPROVER_ID = @UserId)
end