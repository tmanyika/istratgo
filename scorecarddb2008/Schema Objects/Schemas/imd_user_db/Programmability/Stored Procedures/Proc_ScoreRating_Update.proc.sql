﻿



-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_ScoreRating_Update]
	@RatingId int,
	@CompanyId int,
	@RatingValue decimal(18,1),
	@MinimumValue decimal(18,2) = null, 
	@MaximumValue decimal(18,2) = null,
	@RatingDescription varchar(100),
	@Active bit,
	@UpdatedBy varchar(50)
AS
BEGIN
	set nocount on;
	
	if not(exists(select RatingValue from imd_user_db.ScoreRating where Active = 1 and CompanyId = @CompanyId and RatingValue = @RatingValue and RatingId <> @RatingId)) 
		update imd_user_db.ScoreRating
		set Active = @Active, 
			RatingValue = @RatingValue,
			MinimumValue = @MinimumValue, 
			MaximumValue = @MaximumValue,
			RatingDescription = @RatingDescription,
			DateUpdated = getdate(), 
			UpdatedBy = @UpdatedBy
		where RatingId = @RatingId
	select @@rowcount
END