﻿

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_TalentMatrix_Insert] 
	@CompanyId int, 
	@TalentMatrixName varchar(300), 
	@TalentMatrixDescription varchar(2000), 
	@LowerBoundSign varchar(4), 
	@LowerBound decimal(18,1), 
	@UpperBoundSign varchar(4), 
	@UpperBound decimal(18,1), 
	@Active bit,
	@CreatedBy varchar(20),
	@PositionInMatrix int
AS
BEGIN
	set nocount on;
	
	insert into imd_user_db.TalentMatrix(PositionInMatrix, CompanyId, TalentMatrixName, TalentMatrixDescription, LowerBoundSign, LowerBound, UpperBoundSign, UpperBound, Active, CreatedBy, DateCreated)
	values(@PositionInMatrix, @CompanyId, @TalentMatrixName, @TalentMatrixDescription, @LowerBoundSign, @LowerBound, @UpperBoundSign, @UpperBound, @Active, @CreatedBy, getdate())
	select @@rowcount
END