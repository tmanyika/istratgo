﻿

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_BulkImport_EmployeeInsert] 
	 @LineManagerEmployeeId int = NULL,
     @OrgUnitId int, 
     @FirstName varchar(25), 
     @MiddleName varchar(25) = null, 
     @LastName varchar(25), 
     @EmailAddress varchar(80),
     @JobTitleId int = NULL, 
     @StatusId int = NULL, 
     @EmployeeNo varchar(15), 
     @WorkingWeekId int = NULL, 
     @Cellphone varchar(15),
     @BusTelephone varchar(15), 
     @CreatedBy varchar(20), 
     @Active bit = 1, 
     @UserName varchar(120) = null, 
     @Pass varchar(250),
     @RoleId int,
     @DateOfAppointment datetime = null,
     @TmpEmployeeId bigint
AS
BEGIN
	set nocount on;
	set xact_abort on;
	
	declare @EmployeeId int, 
			@FullName varchar(100), 
			@EmailExist bit, 
			@UNameExist bit,
			@EndDate datetime,
			@PrevPosId int,
			@Reason varchar(800);
	
	begin try
		set @PrevPosId = null;
		set @EndDate = null;		
		set @EmployeeId = 0;
		set @UNameExist = 0;
		set @EmailExist = 0;
		set @Reason = null;
		
		set @FullName = @FirstName + ' ' + @MiddleName + ' ' + @LastName;
		
		if (exists(select username from imd_user_db.EmployeeLogin with(nolock) where userName = @UserName))
			set @UNameExist = 1
		if (exists(select emailaddress from imd_user_db.Employee with(nolock) where EmailAddress = @EmailAddress))
			set @EmailExist = 1
		
		if(@EmailExist = 0 and @UNameExist = 1)
			begin
				set @UNameExist = 0
				set @UserName = @EmailAddress
			end
			
		if(@EmailExist = 0 and @UNameExist = 0)	
			begin			
				begin tran
					insert into imd_user_db.Employee(LineManagerEmployeeId, OrgUnitId, FirstName, MiddleName, LastName, EmailAddress, JobTitleId, StatusId, EmployeeNo, 
							  WorkingWeekId, Cellphone, BusTelephone, CreatedBy, DateCreated, DateUpdated, UpdatedBy, DateOfAppointment)
					values(@LineManagerEmployeeId, @OrgUnitId, @FirstName, @MiddleName, @LastName, @EmailAddress, @JobTitleId, @StatusId, @EmployeeNo, 
							@WorkingWeekId, @Cellphone, @BusTelephone, @CreatedBy, getdate(), NULL, NULL, @DateOfAppointment)
					select @EmployeeId = scope_identity()
		             
					insert into imd_user_db.EmployeeLogin(EmployeeId, UserName, Pass, RoleId, Active)
					values(@EmployeeId, @UserName, @Pass, @RoleId, @Active)
					
					insert into imd_user_db.WelcomeState(WelcomeId, EmployeeId, ShowAgain, DateLastRequested)
					select WelcomeId, @EmployeeId as EmployeeId, cast(1 as bit) as ShowAgain, getdate() as DateLastRequested
					from imd_user_db.Welcome with(nolock) 
					where Active = 1									
				commit tran
				
				if(@DateOfAppointment is not null)
					exec imd_user_db.Proc_EmploymentHist_Insert	@EmployeeId, 
																@PrevPosId, 
																@JobTitleId, 
																@DateOfAppointment, 
																@EndDate, 
																@CreatedBy
				--Update Repository	
				update imd_user_db.bulkEmployee
				set ImportFailureReason = null,
				    Added = 1				    
				where EmployeeId = @TmpEmployeeId		
			end
		--else
		--	begin
		--		set @Reason = case when @EmailExist = 1 and @UNameExist = 1 then 'Email Address & User Name Exists'
		--						   when @EmailExist = 1 and @UNameExist = 0 then 'Email Address Exists'
		--					       when @EmailExist = 0 and @UNameExist = 1 then 'User Name Exists'
		--					       else 'Profile already exists' end
		--		update imd_user_db.bulkEmployee
		--		set ImportFailureReason = @Reason,
		--		    Added = 0				    
		--		where EmployeeId = @TmpEmployeeId
		--	end
	end try
	begin catch
		if(@@trancount > 0)
			begin					 
				set @EmployeeId = -1
				rollback tran
			end
		insert into imd_user_db.ProcError(ErrorNo, ErrorSeverity, ErrorState, ErrorProcedure, ErrorLine, ErrorMessage, DateCreated)
		select error_number() AS ErrorNumber, error_severity() AS ErrorSeverity, error_state() AS ErrorState, 
						error_procedure() AS ErrorProcedure, error_line() AS ErrorLine, error_message() AS ErrorMessage, 
						getdate() as DateCreated	
	end catch
	--select @EmployeeId
END