﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_TalentMatrix_SetStatus] 
	@MatrixId int,
	@Active bit, 
	@UpdatedBy varchar(20)
AS
BEGIN
	set nocount on;
	
	update imd_user_db.TalentMatrix
	set UpdatedBy = @UpdatedBy, 
		Active = @Active,
		DateUpdated = GETDATE()
	where MatrixId = @MatrixId
	select @@rowcount
END