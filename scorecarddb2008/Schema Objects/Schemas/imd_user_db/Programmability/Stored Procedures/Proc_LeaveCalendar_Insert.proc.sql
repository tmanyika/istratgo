﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_LeaveCalendar_Insert]
	@countryId int, 
	@holidayDate datetime, 
	@detail varchar(100),
    @calendarYear int, 
    @active bit, 
    @createdBy varchar(20)
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	 
	if not(exists(select holidaydate from imd_user_db.LeaveCalendar where CountryId = @countryId and datediff(dd,HolidayDate,@holidayDate) = 0))
		insert into imd_user_db.LeaveCalendar(CalendarYear, CountryId, HolidayDate, Detail, Active, CreatedBy, DateCreated, DateUpdated, UpdatedBy)
		values(@calendarYear, @countryId, @holidayDate, @detail, @active, @createdBy, getdate(), null, null)
	else
		begin
			declare @holidayDateId int;
			select @holidayDateId=HolidayDateId from imd_user_db.LeaveCalendar where CountryId = @countryId and datediff(dd,HolidayDate,@holidayDate) = 0
			if (@holidayDateId is not null)
				update imd_user_db.LeaveCalendar
				set CountryId = @countryId, 
					CalendarYear = @calendarYear,
					HolidayDate = @holidayDate, 
					Detail = @detail, 
					Active = @active, 
					UpdatedBy = @createdBy, 
					DateUpdated = GETDATE()
				where HolidayDateId = @holidayDateId
		end
	select @@rowcount
END