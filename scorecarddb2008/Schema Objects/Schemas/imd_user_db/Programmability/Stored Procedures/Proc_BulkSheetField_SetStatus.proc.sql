﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_BulkSheetField_SetStatus]
	@FieldId int, 
	@Active bit, 
	@UpdatedBy varchar(50)
AS
BEGIN
	set nocount on;
	
	update imd_user_db.BulkSheetField
	set Active = @Active, 
		UpdatedBy = @UpdatedBy, 
		DateUpdated = getdate()
	where (FieldId = @FieldId)
	select @@rowcount
END