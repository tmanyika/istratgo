﻿
CREATE VIEW [imd_user_db].[vw_BulkProjectLinking]
AS
SELECT     TOP (100) PERCENT p.ParentId, s.SubGoalId, p.KeyFocusArea, p.StrategicObjective, s.PerformanceMeasure, s.ParentPerformanceMeasure, p.AreaOfMeasureType, p.AreaOfMeasure, p.CompanyId, 
                      s.AreaOfMeasureTypeId
FROM         imd_user_db.vw_BulkProjectListParentMeasures AS p WITH (nolock) INNER JOIN
                      imd_user_db.vw_BulkProjectListSubMeasures AS s WITH (nolock) ON p.CompanyId = s.CompanyId AND p.KeyFocusArea = s.KeyFocusArea AND p.StrategicObjective = s.StrategicObjective AND 
                      p.PerformanceMeasure = s.ParentPerformanceMeasure AND p.AreaOfMeasureType = s.AreaOfMeasureType AND p.AreaOfMeasure = s.AreaOfMeasure
ORDER BY s.ParentPerformanceMeasure