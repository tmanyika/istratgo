﻿
CREATE VIEW [imd_user_db].[vw_BatchMailReminder]
AS
select     e.EmployeeId, e.FullName, e.Email, 0 AS BatchId, 
			replace((select mt.MailSubject from imd_user_db.MailTemplate as mt where mt.MailId = 1),'{0}',t.MailSubject) as MailSubject, replace(t.MailContent collate database_default,'{2}',e.Months) as MailContent, (select MailContent from imd_user_db.MailTemplate where MailId = 1) as MainTemplate, cast(0 as bit) as IsSent, getdate() as DateToBeSent
from         imd_user_db.vw_30To60DaysReminder AS e CROSS JOIN
            imd_user_db.MailTemplate AS t
where     (t.MailId = 5) and (not (e.EmployeeId in (select EmployeeId from imd_user_db.vw_Batch0)))
union
select     e.EmployeeId, e.FullName, e.Email, -1 as BatchId, 
		replace((select tm.MailSubject from imd_user_db.MailTemplate as tm where tm.MailId = 1),'{0}',t.MailSubject) as MailSubject, t.MailContent, (select MailContent from imd_user_db.MailTemplate where MailId = 1) as MainTemplate, cast(0 as bit) as IsSent, getdate() as DateToBeSent
from       imd_user_db.vw_90DaysReminder as e CROSS JOIN
            imd_user_db.MailTemplate AS t
where     (t.MailId = 6) and (not (e.EmployeeId in (select EmployeeId from imd_user_db.vw_BatchMinus1)))
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'VIEW', @level1name = N'vw_BatchMailReminder';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'VIEW', @level1name = N'vw_BatchMailReminder';

