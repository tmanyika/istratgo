﻿
CREATE VIEW [imd_user_db].[vw_BulkRoleMeasuresToInsert]
AS
SELECT   bi.GoalId, bi.GoalParentId, d.StrategicObjectiveId, d.PerspectiveId, d.KfaId, d.KeyFocusArea, d.StrategicObjective, d.PerformanceMeasure, d.ParentPerformanceMeasure, d.AreaOfMeasureType, d.AreaOfMeasure, 
                      d.UnitOfMeasure, d.TARGET, d.WEIGHT, d.CompanyId, d.AreaId, d.AreaOfMeasureTypeId, d.UnitOfMeasureId, bi.PerformanceMeasure AS Exist
FROM         imd_user_db.vw_BulkJobTitleMeasures AS d WITH (nolock) LEFT OUTER JOIN
                      imd_user_db.vw_BulkIndividualGoals AS bi WITH (nolock) ON d.CompanyId = bi.CompanyId AND d.StrategicObjectiveId = bi.StrategicObjectiveId AND 
                      d.AreaOfMeasureTypeId = bi.AreaOfMeasureTypeId AND d.AreaId = bi.AreaId AND d.PerformanceMeasure = bi.PerformanceMeasure
WHERE     (bi.PerformanceMeasure IS NULL)