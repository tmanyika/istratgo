﻿CREATE VIEW imd_user_db.vw_BulkPerformanceMeasuresCleanup
AS
SELECT DISTINCT cs.COMPANY_ID AS CompanyId, ig.PARENT_ID AS ParentId
FROM         imd_user_db.COMPANY_STRATEGIC AS cs WITH (nolock) INNER JOIN
                      imd_user_db.INDIVIDUAL_GOALS AS ig WITH (nolock) ON cs.ID = ig.STRATEGIC_ID
WHERE     (ig.PARENT_ID IS NOT NULL)