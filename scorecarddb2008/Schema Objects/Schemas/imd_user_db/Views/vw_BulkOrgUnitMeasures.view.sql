﻿
CREATE VIEW [imd_user_db].[vw_BulkOrgUnitMeasures]
AS
SELECT     cs.ID AS StrategicObjectiveId, cs.PERSPECTIVE AS PerspectiveId, d.KfaId, d.KeyFocusArea, d.StrategicObjective, d.PerformanceMeasure, d.ParentPerformanceMeasure, d.AreaOfMeasureType, 
                      d.AreaOfMeasure, d.UnitOfMeasure, d.TARGET, d.WEIGHT, d.CompanyId, d.AreaId, d.AreaOfMeasureTypeId, d.UnitOfMeasureId
FROM         (SELECT     kfa.PERSPECTIVE_ID AS KfaId, g.KeyFocusArea, g.StrategicObjective, g.PerformanceMeasure, g.ParentPerformanceMeasure, g.AreaOfMeasureType, g.AreaOfMeasure, g.UnitOfMeasure, 
                                              g.TARGET, g.WEIGHT, g.CompanyId, j.ID AS AreaId, am.ID AS AreaOfMeasureTypeId, ms.ID AS UnitOfMeasureId
                       FROM          imd_user_db.BulkPerformanceMeasure AS g WITH (nolock) INNER JOIN
                                              imd_user_db.COMPANY_PERSPECTIVE AS kfa WITH (nolock) ON g.KeyFocusArea = kfa.PERSPECTIVE_NAME INNER JOIN
                                              imd_user_db.COMPANY_STRUCTURE AS j WITH (nolock) ON g.CompanyId = j.COMPANY_ID AND g.AreaOfMeasure = j.ORGUNIT_NAME INNER JOIN
                                              imd_user_db.UTILITIES AS am WITH (nolock) ON g.AreaOfMeasureType = am.TYPE_NAME LEFT OUTER JOIN
                                              imd_user_db.UTILITIES AS ms WITH (nolock) ON g.UnitOfMeasure = ms.TYPE_NAME) AS d INNER JOIN
                      imd_user_db.COMPANY_STRATEGIC AS cs WITH (nolock) ON d.CompanyId = cs.COMPANY_ID AND d.KfaId = cs.PERSPECTIVE AND d.StrategicObjective = cs.OBJECTIVE