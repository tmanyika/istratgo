﻿CREATE VIEW imd_user_db.vw_BulkIndividualGoals
AS
SELECT     cs.COMPANY_ID AS CompanyId, ig.STRATEGIC_ID AS StrategicObjectiveId, ig.AREA_TYPE_ID AS AreaOfMeasureTypeId, ig.AREA_ID AS AreaId, ig.GOAL_DESCRIPTION AS PerformanceMeasure, 
                      ig.ID AS GoalId, ig.PARENT_ID AS GoalParentId
FROM         imd_user_db.COMPANY_STRATEGIC AS cs WITH (nolock) INNER JOIN
                      imd_user_db.INDIVIDUAL_GOALS AS ig WITH (nolock) ON cs.ID = ig.STRATEGIC_ID