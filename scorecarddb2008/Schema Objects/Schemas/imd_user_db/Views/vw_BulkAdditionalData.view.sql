﻿CREATE VIEW imd_user_db.vw_BulkAdditionalData
AS
SELECT     bpm.AreaOfMeasure, bpm.AreaOfMeasureType, u.ID AS AreaId, u.TYPE_NAME AS DbAreaOfMeasureType, bpm.CompanyId
FROM         imd_user_db.bulkPerformanceMeasure AS bpm WITH (nolock) INNER JOIN
                      imd_user_db.UTILITIES AS u WITH (nolock) ON bpm.AreaOfMeasureType = u.TYPE_NAME