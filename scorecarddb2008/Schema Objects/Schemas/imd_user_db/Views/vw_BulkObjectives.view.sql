﻿CREATE VIEW imd_user_db.vw_BulkObjectives
AS
SELECT DISTINCT TOP (100) PERCENT KeyFocusArea, StrategicObjective, StrategicObjectiveRationale, CompanyId
FROM         imd_user_db.BulkPerformanceMeasure AS bpm WITH (nolock)
ORDER BY StrategicObjective