﻿CREATE VIEW imd_user_db.vw_ScoreReportDatesForCompany
AS
SELECT     CONVERT(varchar(10), isc.PERIOD_DATE, 121) AS PERIOD_DATE, cms.ID AS OrgUnitId, cms.COMPANY_ID AS CompanyId, sb.STATUS
FROM         imd_user_db.INDIVIDUAL_SCORES AS isc INNER JOIN
                      imd_user_db.INDIVIDUAL_GOALS AS ig ON isc.GOAL_ID = ig.ID INNER JOIN
                      imd_user_db.COMPANY_STRATEGIC AS cs ON ig.STRATEGIC_ID = cs.ID INNER JOIN
                      imd_user_db.COMPANY_STRUCTURE AS cms ON cs.COMPANY_ID = cms.COMPANY_ID INNER JOIN
                      imd_user_db.SUBMISSIONS AS sb ON isc.SUBMISSION_ID = sb.ID
WHERE     (isc.CRITERIA_TYPE_ID = 32) AND (sb.STATUS = 40)
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[42] 4[19] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "isc"
            Begin Extent = 
               Top = 16
               Left = 824
               Bottom = 259
               Right = 1046
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ig"
            Begin Extent = 
               Top = 1
               Left = 548
               Bottom = 120
               Right = 738
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cs"
            Begin Extent = 
               Top = 6
               Left = 298
               Bottom = 145
               Right = 465
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cms"
            Begin Extent = 
               Top = 29
               Left = 19
               Bottom = 148
               Right = 202
            End
            DisplayFlags = 280
            TopColumn = 8
         End
         Begin Table = "sb"
            Begin Extent = 
               Top = 120
               Left = 503
               Bottom = 298
               Right = 708
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'VIEW', @level1name = N'vw_ScoreReportDatesForCompany';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'VIEW', @level1name = N'vw_ScoreReportDatesForCompany';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'VIEW', @level1name = N'vw_ScoreReportDatesForCompany';

