﻿CREATE VIEW [imd_user_db].[vw_BulkParentExistingGoals]
AS
SELECT     cp.COMPANY_ID AS CompanyId, cp.PERSPECTIVE_ID AS KeyFocusAreaId, cs.ID AS StrategicObjectiveId, ig.ID AS GoalId, ig.AREA_TYPE_ID AS AreaTypeId, ig.AREA_ID AS AreaId, 
                      cp.PERSPECTIVE_NAME AS KeyFocusArea, cs.OBJECTIVE AS StrategicObjective, ig.GOAL_DESCRIPTION AS Goal
FROM         imd_user_db.INDIVIDUAL_GOALS AS ig WITH (nolock) INNER JOIN
                      imd_user_db.COMPANY_STRATEGIC AS cs WITH (nolock) ON ig.STRATEGIC_ID = cs.ID INNER JOIN
                      imd_user_db.COMPANY_PERSPECTIVE AS cp WITH (nolock) ON cs.PERSPECTIVE = cp.PERSPECTIVE_ID