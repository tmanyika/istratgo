﻿CREATE VIEW imd_user_db.vw_BulkEmployee
AS
SELECT     be.FirstName, be.MiddleName, be.LastName, be.EmailAddress, be.DateOfAppointment, be.EmployeeNumber, be.Cellphone, be.LandlineTelephone, be.JobTitle, be.OrganisationUnit, 
                      be.LineManagerEmail, be.UserName, be.SystemRole, be.CompanyId, be.EmployeeId, cj.ID AS JobTitleId, cs.ID AS OrgUnitId, u.ID AS RoleId
FROM         (SELECT     ID, PARENT_ID, TYPE_NAME, DESCRIPTION
                       FROM          imd_user_db.UTILITIES WITH (nolock)
                       WHERE      (PARENT_ID = 43)) AS u RIGHT OUTER JOIN
                      imd_user_db.COMPANY_JOB_TITLES AS cj WITH (nolock) RIGHT OUTER JOIN
                      imd_user_db.BulkEmployee AS be WITH (nolock) LEFT OUTER JOIN
                      imd_user_db.COMPANY_STRUCTURE AS cs WITH (nolock) ON be.CompanyId = cs.COMPANY_ID AND be.OrganisationUnit = cs.ORGUNIT_NAME ON cj.COMPANY_ID = be.CompanyId AND 
                      cj.NAME = be.JobTitle ON u.TYPE_NAME = be.SystemRole