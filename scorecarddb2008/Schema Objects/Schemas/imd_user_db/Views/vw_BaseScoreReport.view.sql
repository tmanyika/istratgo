﻿CREATE VIEW imd_user_db.vw_BaseScoreReport
AS
SELECT     s.ID, s.PARENT_ID, cs.PERSPECTIVE, cs.OBJECTIVE, sb.STATUS, u.TYPE_NAME AS UnitOfMeasure, cs.RATIONALE, g.RATIONAL, g.UNIT_MEASURE, s.CRITERIA_TYPE_ID, 
                      s.SELECTED_ITEM_VALUE, s.GOAL_ID, s.SCORES, s.MANAGER_SCORE, s.AGREED_SCORE, s.FINAL_SCORE, s.PERIOD_DATE, s.JUSTIFICATION_COMMENT, s.EVIDENCE, 
                      s.MANAGER_COMMENT, s.RATINGVALUE, s.FINAL_SCORE_AGREED, g.TARGET, g.WEIGHT, g.GOAL_DESCRIPTION, DATEPART(yyyy, s.PERIOD_DATE) AS PeriodYear, DATEPART(mm, 
                      s.PERIOD_DATE) AS PeriodMonth
FROM         imd_user_db.INDIVIDUAL_SCORES AS s WITH (nolock) INNER JOIN
                      imd_user_db.INDIVIDUAL_GOALS AS g WITH (nolock) ON s.GOAL_ID = g.ID INNER JOIN
                      imd_user_db.COMPANY_STRATEGIC AS cs WITH (nolock) ON g.STRATEGIC_ID = cs.ID INNER JOIN
                      imd_user_db.SUBMISSIONS AS sb WITH (nolock) ON s.SUBMISSION_ID = sb.ID LEFT OUTER JOIN
                      imd_user_db.UTILITIES AS u WITH (nolock) ON g.UNIT_MEASURE = u.ID
WHERE     (sb.STATUS = 40) AND (s.PARENT_ID IS NULL)
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[34] 4[24] 2[28] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "s"
            Begin Extent = 
               Top = 10
               Left = 702
               Bottom = 354
               Right = 924
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "g"
            Begin Extent = 
               Top = 0
               Left = 460
               Bottom = 325
               Right = 650
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cs"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 261
               Right = 205
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "u"
            Begin Extent = 
               Top = 164
               Left = 226
               Bottom = 341
               Right = 393
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sb"
            Begin Extent = 
               Top = 0
               Left = 993
               Bottom = 248
               Right = 1198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 24
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 2610
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 150', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'VIEW', @level1name = N'vw_BaseScoreReport';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'0
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'VIEW', @level1name = N'vw_BaseScoreReport';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'VIEW', @level1name = N'vw_BaseScoreReport';

