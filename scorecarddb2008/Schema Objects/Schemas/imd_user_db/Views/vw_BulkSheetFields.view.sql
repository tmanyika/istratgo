﻿CREATE VIEW imd_user_db.vw_BulkSheetFields
AS
SELECT     bs.SheetName, bsf.FieldId, bsf.SheetId, bsf.SheetFieldName, bsf.DbFieldName, bsf.IsRequired, bsf.Active, bsf.FieldNotes, bsf.CreatedBy, bsf.DateCreated, bsf.UpdatedBy, bsf.DateUpdated, 
                      bs.DbTableName, bs.SheetDescription, bs.SheetNotes, bsf.DataType, bsf.ExpectedValueList
FROM         imd_user_db.BulkSheet AS bs WITH (nolock) INNER JOIN
                      imd_user_db.BulkSheetField AS bsf WITH (nolock) ON bs.SheetId = bsf.SheetId