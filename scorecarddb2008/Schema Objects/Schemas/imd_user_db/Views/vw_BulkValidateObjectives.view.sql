﻿
CREATE VIEW [imd_user_db].[vw_BulkValidateObjectives]
AS
SELECT     TOP (100) PERCENT v.CompanyId, v.StrategicObjective, b.KeyFocusArea, v.TotalCount
FROM         (SELECT     CompanyId, StrategicObjective, COUNT(KeyFocusArea) AS TotalCount
                       FROM          (SELECT DISTINCT CompanyId, KeyFocusArea, StrategicObjective
                                               FROM          imd_user_db.BulkPerformanceMeasure WITH (nolock)) AS d
                       GROUP BY CompanyId, StrategicObjective) AS v INNER JOIN
                          (SELECT DISTINCT KeyFocusArea, StrategicObjective
                            FROM          imd_user_db.BulkPerformanceMeasure AS BulkPerformanceMeasure_1 WITH (nolock)) AS b ON v.StrategicObjective = b.StrategicObjective
WHERE     (v.TotalCount > 1)
ORDER BY v.StrategicObjective, b.KeyFocusArea