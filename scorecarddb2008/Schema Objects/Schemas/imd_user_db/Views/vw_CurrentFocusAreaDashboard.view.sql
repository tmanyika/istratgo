﻿CREATE view imd_user_db.vw_CurrentFocusAreaDashboard
as
select [STATUS], PERIOD_DATE, CRITERIA_TYPE_ID, SELECTED_ITEM_VALUE, OrgUnitId
from         imd_user_db.vw_CurrentFocusAreaEmployee (nolock)
union
select [STATUS], PERIOD_DATE, CRITERIA_TYPE_ID, SELECTED_ITEM_VALUE, OrgUnitId
from    imd_user_db.vw_CurrentFocusAreaOrgUnit (nolock)