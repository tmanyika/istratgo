﻿CREATE VIEW imd_user_db.vw_BulkObjectivesExisting
AS
SELECT     cp.PERSPECTIVE_ID AS KeyFocusAreaId, cs.ID AS ObjectiveId, cp.PERSPECTIVE_NAME AS KeyFocusArea, cs.OBJECTIVE AS StrategicObjective, cs.COMPANY_ID AS CompanyId
FROM         imd_user_db.COMPANY_STRATEGIC AS cs WITH (nolock) INNER JOIN
                      imd_user_db.COMPANY_PERSPECTIVE AS cp WITH (nolock) ON cs.PERSPECTIVE = cp.PERSPECTIVE_ID AND cs.COMPANY_ID = cp.COMPANY_ID