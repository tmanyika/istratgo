﻿CREATE VIEW [imd_user_db].[vw_BatchMinus1]
AS
select EmployeeId from imd_user_db.MailBatchUserList (nolock) where BatchId = -1
union
select EmployeeId from imd_user_db.UnSubscribe (nolock)