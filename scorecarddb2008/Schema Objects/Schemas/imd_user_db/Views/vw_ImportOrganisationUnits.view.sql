﻿CREATE VIEW imd_user_db.vw_ImportOrganisationUnits
AS
SELECT     cs.ORGUNIT_NAME AS OrganisationUnitName, ISNULL(lcs.ORGUNIT_NAME, '') AS ParentOrganisationUnitName, u.TYPE_NAME AS OrganisationUnitType, 
                      e.EmailAddress AS OrganisationUnitOwnerEmail
FROM         imd_user_db.COMPANY_STRUCTURE AS cs WITH (nolock) LEFT OUTER JOIN
                      imd_user_db.COMPANY_STRUCTURE AS lcs WITH (nolock) ON cs.PARENT_ORG = lcs.ID LEFT OUTER JOIN
                      imd_user_db.Employee AS e WITH (nolock) ON cs.OWNER_ID = e.EmployeeId LEFT OUTER JOIN
                      imd_user_db.UTILITIES AS u WITH (nolock) ON cs.ORGUNT_TYPE = u.ID
WHERE     (cs.COMPANY_ID = 181)