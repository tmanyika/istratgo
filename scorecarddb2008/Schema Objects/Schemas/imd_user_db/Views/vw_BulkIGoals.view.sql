﻿CREATE VIEW imd_user_db.vw_BulkIGoals
AS
SELECT     TOP (100) PERCENT imd_user_db.COMPANY_STRATEGIC.COMPANY_ID AS CompanyId, imd_user_db.INDIVIDUAL_GOALS.ID AS GoalId, 
                      imd_user_db.INDIVIDUAL_GOALS.STRATEGIC_ID AS KeyFocusAreaId, imd_user_db.INDIVIDUAL_GOALS.AREA_TYPE_ID AS AreaTypeId, imd_user_db.INDIVIDUAL_GOALS.AREA_ID AS AreaId, 
                      imd_user_db.COMPANY_STRATEGIC.OBJECTIVE AS StrategicObjective, imd_user_db.COMPANY_STRATEGIC.ID AS ObjectiveId, 
                      imd_user_db.COMPANY_PERSPECTIVE.PERSPECTIVE_NAME AS KeyFocusArea, imd_user_db.INDIVIDUAL_GOALS.GOAL_DESCRIPTION AS Goal, arm.TYPE_NAME AS AreaOfMeasureType
FROM         imd_user_db.COMPANY_STRATEGIC INNER JOIN
                      imd_user_db.INDIVIDUAL_GOALS ON imd_user_db.COMPANY_STRATEGIC.ID = imd_user_db.INDIVIDUAL_GOALS.STRATEGIC_ID INNER JOIN
                      imd_user_db.COMPANY_PERSPECTIVE ON imd_user_db.COMPANY_STRATEGIC.PERSPECTIVE = imd_user_db.COMPANY_PERSPECTIVE.PERSPECTIVE_ID INNER JOIN
                      imd_user_db.UTILITIES AS arm WITH (nolock) ON imd_user_db.INDIVIDUAL_GOALS.AREA_TYPE_ID = arm.ID