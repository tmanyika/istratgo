﻿


CREATE VIEW [imd_user_db].[vw_BulkOrgUnitListParentMeasures]
AS
SELECT  ig.GoalId as ParentId, v.*
FROM          
(	SELECT     bpm.KeyFocusArea, bpm.StrategicObjective, bpm.PerformanceMeasure, bpm.AreaOfMeasureType, bpm.AreaOfMeasure, bpm.CompanyId, 
                                              cs.PERSPECTIVE_ID AS KeyFocusAreaId, aria.ID AS AreaId, armt.ID AS AreaOfMeasureTypeId
	FROM          imd_user_db.BulkPerformanceMeasure AS bpm WITH (nolock) INNER JOIN
						  imd_user_db.COMPANY_PERSPECTIVE AS cs WITH (nolock) ON bpm.KeyFocusArea = cs.PERSPECTIVE_NAME AND bpm.CompanyId = cs.COMPANY_ID INNER JOIN
						  imd_user_db.COMPANY_STRUCTURE AS aria WITH (nolock) ON bpm.AreaOfMeasure = aria.ORGUNIT_NAME AND bpm.CompanyId = aria.COMPANY_ID INNER JOIN
						  imd_user_db.UTILITIES AS armt WITH (nolock) ON bpm.AreaOfMeasureType = armt.[TYPE_NAME]
	WHERE      (bpm.HasSubPerformanceMeasures = 'Yes') AND (armt.ID = 33)
) AS v INNER JOIN
imd_user_db.vw_BulkIGoals AS ig WITH (nolock) 
ON v.CompanyId = ig.CompanyId AND v.StrategicObjective = ig.StrategicObjective AND v.AreaOfMeasureTypeId = ig.AreaTypeId AND v.AreaId = ig.AreaId
AND v.PerformanceMeasure = ig.Goal