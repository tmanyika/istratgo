﻿
CREATE VIEW [imd_user_db].[vw_EmpTalentMatrix]
AS
SELECT     c.EmployeeId, c.YearsOnSameRole
FROM         imd_user_db.vw_EmpCurrentPosition AS c WITH (nolock)