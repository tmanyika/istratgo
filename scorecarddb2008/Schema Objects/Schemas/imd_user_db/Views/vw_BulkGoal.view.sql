﻿CREATE VIEW imd_user_db.vw_BulkGoal
AS
SELECT     cs.COMPANY_ID AS CompanyId, ig.GOAL_DESCRIPTION AS Goal, ig.ID AS GoalId, cs.ID AS ObjectiveId, cs.PERSPECTIVE AS PerspectiveId, ig.AREA_TYPE_ID AS AreaOfMeasureTypeId, 
                      ig.AREA_ID AS AreaOfMeasureId, u.TYPE_NAME AS AreaOfMeasureType, ig.ACTIVE
FROM         imd_user_db.COMPANY_STRATEGIC AS cs WITH (nolock) INNER JOIN
                      imd_user_db.INDIVIDUAL_GOALS AS ig WITH (nolock) ON cs.ID = ig.STRATEGIC_ID LEFT OUTER JOIN
                      imd_user_db.UTILITIES AS u WITH (nolock) ON ig.AREA_TYPE_ID = u.ID
WHERE     (ig.ACTIVE = 1)