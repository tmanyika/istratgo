﻿CREATE VIEW imd_user_db.vw_BulkParentMeasuresList
AS
SELECT DISTINCT ParentPerformanceMeasure, CompanyId
FROM         imd_user_db.BulkPerformanceMeasure
WHERE     (ParentPerformanceMeasure IS NOT NULL)