﻿CREATE VIEW imd_user_db.vw_CurrentFocusAreaEmployee
AS
SELECT     s.STATUS, isc.PERIOD_DATE, isc.CRITERIA_TYPE_ID, isc.SELECTED_ITEM_VALUE, e.OrgUnitId
FROM         imd_user_db.INDIVIDUAL_SCORES AS isc WITH (nolock) INNER JOIN
                      imd_user_db.SUBMISSIONS AS s WITH (nolock) ON isc.SUBMISSION_ID = s.ID INNER JOIN
                      imd_user_db.Employee AS e WITH (nolock) ON s.SELECTED_ITEM_VALUE = e.EmployeeId
WHERE     (s.STATUS = 40) AND (isc.CRITERIA_TYPE_ID = 32)