﻿CREATE VIEW imd_user_db.vw_CurrentFocusAreaOrgUnit
AS
SELECT     s.STATUS, isc.PERIOD_DATE, isc.CRITERIA_TYPE_ID, isc.SELECTED_ITEM_VALUE, cs.ID AS OrgUnitId
FROM         imd_user_db.INDIVIDUAL_SCORES AS isc WITH (nolock) INNER JOIN
                      imd_user_db.SUBMISSIONS AS s WITH (nolock) ON isc.SUBMISSION_ID = s.ID INNER JOIN
                      imd_user_db.COMPANY_STRUCTURE AS cs WITH (nolock) ON isc.SELECTED_ITEM_VALUE = cs.ID
WHERE     (s.STATUS = 40) AND (isc.CRITERIA_TYPE_ID = 33)