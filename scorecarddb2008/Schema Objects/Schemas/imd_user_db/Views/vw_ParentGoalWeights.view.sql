﻿CREATE VIEW imd_user_db.vw_ParentGoalWeights
AS
SELECT     ID, WEIGHT
FROM         imd_user_db.INDIVIDUAL_GOALS WITH (nolock)
WHERE     (WEIGHT IS NOT NULL) AND (PARENT_ID IS NULL)