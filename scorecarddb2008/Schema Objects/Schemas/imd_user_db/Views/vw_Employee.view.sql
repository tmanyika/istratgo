﻿CREATE VIEW imd_user_db.vw_Employee
AS
SELECT     l.UserName, l.Pass, l.RoleId, l.Active, e.EmployeeId, e.LineManagerEmployeeId, e.OrgUnitId, e.FirstName, e.MiddleName, e.LastName, e.EmailAddress, e.JobTitleId, e.StatusId, e.EmployeeNo, 
                      e.WorkingWeekId, e.Cellphone, e.BusTelephone, e.CreatedBy, e.DateCreated, e.DateUpdated, e.UpdatedBy, j.NAME AS jobTitle, w.Name, w.NoOfDays, c.COMPANY_ID AS CompanyId, 
                      s.StatusName, c.ORGUNIT_NAME AS CompanyName, u.ID AS typeId, u.TYPE_NAME AS typeName, e.DateOfAppointment
FROM         imd_user_db.Employee AS e INNER JOIN
                      imd_user_db.COMPANY_STRUCTURE AS c ON e.OrgUnitId = c.ID INNER JOIN
                      imd_user_db.EmployeeLogin AS l ON e.EmployeeId = l.EmployeeId LEFT OUTER JOIN
                      imd_user_db.UTILITIES AS u ON l.RoleId = u.ID LEFT OUTER JOIN
                      imd_user_db.COMPANY_JOB_TITLES AS j ON e.JobTitleId = j.ID LEFT OUTER JOIN
                      imd_user_db.EmployeeStatus AS s ON e.StatusId = s.StatusId LEFT OUTER JOIN
                      imd_user_db.WorkingWeek AS w ON e.WorkingWeekId = w.WorkingWeekId
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[56] 4[15] 2[18] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "e"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 126
               Right = 244
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 142
               Left = 681
               Bottom = 371
               Right = 864
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "l"
            Begin Extent = 
               Top = 0
               Left = 572
               Bottom = 120
               Right = 732
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "u"
            Begin Extent = 
               Top = 0
               Left = 936
               Bottom = 175
               Right = 1103
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "j"
            Begin Extent = 
               Top = 146
               Left = 389
               Bottom = 348
               Right = 550
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "s"
            Begin Extent = 
               Top = 237
               Left = 221
               Bottom = 357
               Right = 381
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "w"
            Begin Extent = 
               Top = 240
               Left = 12
               Bottom = 360
               Right = 177
            End
            DisplayFlags = 280
            TopColumn = 0
         End', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'VIEW', @level1name = N'vw_Employee';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'VIEW', @level1name = N'vw_Employee';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'VIEW', @level1name = N'vw_Employee';

