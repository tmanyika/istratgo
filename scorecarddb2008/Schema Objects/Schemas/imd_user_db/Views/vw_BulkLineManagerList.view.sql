﻿CREATE VIEW imd_user_db.vw_BulkLineManagerList
AS
SELECT     e.EmployeeId AS LineManagerEmployeeId, b.LineManagerEmail, b.EmailAddress, b.CompanyId
FROM         imd_user_db.BulkEmployee AS b WITH (nolock) INNER JOIN
                      imd_user_db.Employee AS e WITH (nolock) ON b.LineManagerEmail = e.EmailAddress