﻿CREATE VIEW imd_user_db.vw_BulkOrgStructure
AS
SELECT     b.OrganisationUnitName, b.ParentOrganisationUnitName, b.OrganisationUnitType, b.OrganisationUnitOwnerEmail, b.CompanyId, e.EmployeeId AS OwnerId, u.ID AS OrgUnitId
FROM         imd_user_db.bulkOrganisationUnit AS b WITH (nolock) LEFT OUTER JOIN
                          (SELECT     ID, PARENT_ID, TYPE_NAME, DESCRIPTION, DATE_CREATED, ACTIVE
                            FROM          imd_user_db.UTILITIES WITH (nolock)
                            WHERE      (PARENT_ID = 7)) AS u ON b.OrganisationUnitType = u.TYPE_NAME LEFT OUTER JOIN
                      imd_user_db.Employee AS e WITH (nolock) ON b.OrganisationUnitOwnerEmail COLLATE database_default = e.EmailAddress COLLATE database_default