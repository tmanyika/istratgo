﻿

CREATE VIEW [imd_user_db].[vw_BulkRoleListSubMeasures]
AS
select distinct ig.GoalId as SubGoalId, v.* from
(SELECT     bpm.KeyFocusArea, bpm.StrategicObjective, bpm.PerformanceMeasure, bpm.ParentPerformanceMeasure, ar.ID AS AreaID, bpm.AreaOfMeasureType, 
                      bpm.AreaOfMeasure, bpm.CompanyId, bpm.HasSubPerformanceMeasures, u.ID AS AreaOfMeasureTypeId
FROM         imd_user_db.BulkPerformanceMeasure AS bpm WITH (nolock) INNER JOIN
                      imd_user_db.COMPANY_JOB_TITLES AS ar WITH (nolock) ON bpm.CompanyId = ar.COMPANY_ID AND bpm.AreaOfMeasure = ar.NAME INNER JOIN
                      imd_user_db.COMPANY_PERSPECTIVE AS cp WITH (nolock) ON bpm.CompanyId = cp.COMPANY_ID AND bpm.KeyFocusArea = cp.PERSPECTIVE_NAME INNER JOIN
                      imd_user_db.UTILITIES AS u WITH (nolock) ON bpm.AreaOfMeasureType = u.TYPE_NAME
WHERE     (bpm.ParentPerformanceMeasure IS NOT NULL) AND (bpm.ParentPerformanceMeasure <> '') AND (bpm.HasSubPerformanceMeasures <> 'Yes') AND (u.ID = 32)
) as v inner join imd_user_db.vw_BulkIGoals as ig on
v.CompanyId = ig.CompanyId AND v.KeyFocusArea = ig.KeyFocusArea and v.StrategicObjective = ig.StrategicObjective AND v.AreaOfMeasureTypeId = ig.AreaTypeId AND v.AreaId = ig.AreaId
and v.PerformanceMeasure = ig.Goal