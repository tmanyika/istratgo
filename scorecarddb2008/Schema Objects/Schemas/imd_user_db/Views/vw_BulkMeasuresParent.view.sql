﻿CREATE VIEW imd_user_db.vw_BulkMeasuresParent
AS
SELECT     bpm.PerformanceMeasure AS GoalDescription, bpm.ParentPerformanceMeasure AS ParentGoal, ig.ID AS ParentMeasureId, bpm.CompanyId, cs.COMPANY_ID
FROM         imd_user_db.bulkPerformanceMeasure AS bpm WITH (nolock) INNER JOIN
                      imd_user_db.INDIVIDUAL_GOALS AS ig WITH (nolock) ON bpm.ParentPerformanceMeasure = ig.GOAL_DESCRIPTION INNER JOIN
                      imd_user_db.COMPANY_STRATEGIC AS cs WITH (nolock) ON ig.STRATEGIC_ID = cs.ID
WHERE     (bpm.ParentPerformanceMeasure IS NOT NULL)