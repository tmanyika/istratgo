﻿CREATE VIEW imd_user_db.vw_ImportEmployee
AS
SELECT     e.FirstName, e.MiddleName, e.LastName, e.EmailAddress, e.DateOfAppointment, e.EmployeeNo AS EmployeeNumber, e.Cellphone, e.BusTelephone AS LandlineTelephone, cj.NAME AS JobTitle, 
                      cs.ORGUNIT_NAME AS OrganisationUnit, pe.EmailAddress AS LineManagerEmail, l.UserName, u.TYPE_NAME AS SystemRole
FROM         imd_user_db.COMPANY_STRUCTURE AS cs WITH (nolock) RIGHT OUTER JOIN
                      imd_user_db.Employee AS e WITH (nolock) ON cs.ID = e.OrgUnitId LEFT OUTER JOIN
                      imd_user_db.Employee AS pe WITH (nolock) ON e.LineManagerEmployeeId = pe.EmployeeId LEFT OUTER JOIN
                      imd_user_db.COMPANY_JOB_TITLES AS cj WITH (nolock) ON e.JobTitleId = cj.ID LEFT OUTER JOIN
                      imd_user_db.EmployeeLogin AS l WITH (nolock) ON e.EmployeeId = l.EmployeeId LEFT OUTER JOIN
                      imd_user_db.UTILITIES AS u WITH (nolock) ON l.RoleId = u.ID