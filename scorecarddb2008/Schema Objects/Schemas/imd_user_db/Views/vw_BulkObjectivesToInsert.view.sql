﻿CREATE VIEW imd_user_db.vw_BulkObjectivesToInsert
AS
SELECT     TOP (100) PERCENT imd_user_db.COMPANY_PERSPECTIVE.PERSPECTIVE_ID AS KeyFocusAreaId, vbo.KeyFocusArea, vbo.StrategicObjective, vbo.StrategicObjectiveRationale, vbo.CompanyId, 
                      vboe.StrategicObjective AS [Exists]
FROM         imd_user_db.vw_BulkObjectives AS vbo WITH (nolock) INNER JOIN
                      imd_user_db.COMPANY_PERSPECTIVE ON vbo.KeyFocusArea = imd_user_db.COMPANY_PERSPECTIVE.PERSPECTIVE_NAME AND 
                      vbo.CompanyId = imd_user_db.COMPANY_PERSPECTIVE.COMPANY_ID LEFT OUTER JOIN
                      imd_user_db.vw_BulkObjectivesExisting AS vboe WITH (nolock) ON vbo.KeyFocusArea = vboe.KeyFocusArea AND vbo.StrategicObjective = vboe.StrategicObjective AND 
                      vbo.CompanyId = vboe.CompanyId
WHERE     (vboe.StrategicObjective IS NULL)
ORDER BY vbo.StrategicObjective