﻿CREATE VIEW imd_user_db.vw_BulkOrgStructureParent
AS
SELECT     cs.ID AS ParentId, bou.OrganisationUnitName, bou.ParentOrganisationUnitName, bou.OrganisationUnitOwnerEmail, cs.COMPANY_ID, bou.CompanyId
FROM         imd_user_db.bulkOrganisationUnit AS bou WITH (nolock) INNER JOIN
                      imd_user_db.COMPANY_STRUCTURE AS cs WITH (nolock) ON bou.ParentOrganisationUnitName = cs.ORGUNIT_NAME AND bou.CompanyId = cs.COMPANY_ID