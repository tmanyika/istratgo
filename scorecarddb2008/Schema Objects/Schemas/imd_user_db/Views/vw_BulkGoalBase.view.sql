﻿CREATE VIEW imd_user_db.vw_BulkGoalBase
AS
SELECT DISTINCT 
                      g.CompanyId, cs.COMPANY_ID, g.PerformanceMeasure, g.StrategicObjective, cs.ID AS StrategicId, 0 AS HasSubPerformanceMeasures, g.ParentPerformanceMeasure, g.AreaOfMeasureType, 
                      u.ID AS AreaOfMeasureTypeId, g.AreaOfMeasure, ms.ID AS UnitOfMeasureId, 1 AS Active, g.UnitOfMeasure, g.TARGET, g.WEIGHT, g.KeyFocusArea, cs.PERSPECTIVE AS PerspectiveId
FROM         imd_user_db.BulkPerformanceMeasure AS g WITH (nolock) INNER JOIN
                      imd_user_db.COMPANY_STRATEGIC AS cs WITH (nolock) ON g.StrategicObjective = cs.OBJECTIVE AND g.CompanyId = cs.COMPANY_ID INNER JOIN
                      imd_user_db.UTILITIES AS u WITH (nolock) ON g.AreaOfMeasureType = u.TYPE_NAME INNER JOIN
                      imd_user_db.UTILITIES AS ms WITH (nolock) ON g.UnitOfMeasure = ms.TYPE_NAME