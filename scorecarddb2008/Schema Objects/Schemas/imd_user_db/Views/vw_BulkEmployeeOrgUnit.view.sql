﻿CREATE VIEW imd_user_db.vw_BulkEmployeeOrgUnit
AS
SELECT     cs.ID AS OrgUnitId, be.EmailAddress, be.CompanyId
FROM         imd_user_db.BulkEmployee AS be WITH (nolock) INNER JOIN
                      imd_user_db.COMPANY_STRUCTURE AS cs WITH (nolock) ON be.CompanyId = cs.COMPANY_ID AND be.OrganisationUnit = cs.ORGUNIT_NAME