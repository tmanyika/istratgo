﻿
CREATE VIEW [imd_user_db].[vw_DashBoardDenominator]
AS
SELECT     isc.CRITERIA_TYPE_ID, isc.SELECTED_ITEM_VALUE, isc.PERIOD_DATE, ost.ID AS OrgUnitId, cs.COMPANY_ID
FROM         imd_user_db.INDIVIDUAL_SCORES AS isc (nolock) INNER JOIN
                      imd_user_db.INDIVIDUAL_GOALS AS ig (nolock) ON isc.GOAL_ID = ig.ID INNER JOIN
                      imd_user_db.COMPANY_STRATEGIC AS cs (nolock) ON ig.STRATEGIC_ID = cs.ID INNER JOIN
                      imd_user_db.COMPANY_STRUCTURE AS ost (nolock) ON cs.COMPANY_ID = ost.COMPANY_ID INNER JOIN
                      imd_user_db.COMPANY_PERSPECTIVE AS cp (nolock) ON cs.PERSPECTIVE = cp.PERSPECTIVE_ID
GROUP BY isc.CRITERIA_TYPE_ID, isc.SELECTED_ITEM_VALUE, ost.ID, cs.COMPANY_ID, isc.PERIOD_DATE
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "isc"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 276
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ig"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 245
               Right = 244
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cs"
            Begin Extent = 
               Top = 126
               Left = 282
               Bottom = 245
               Right = 465
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ost"
            Begin Extent = 
               Top = 246
               Left = 38
               Bottom = 365
               Right = 237
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cp"
            Begin Extent = 
               Top = 366
               Left = 38
               Bottom = 485
               Right = 243
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
E', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'VIEW', @level1name = N'vw_DashBoardDenominator';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'nd', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'VIEW', @level1name = N'vw_DashBoardDenominator';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'VIEW', @level1name = N'vw_DashBoardDenominator';

