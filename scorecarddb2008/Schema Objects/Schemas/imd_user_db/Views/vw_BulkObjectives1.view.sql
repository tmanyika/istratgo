﻿
CREATE VIEW [imd_user_db].[vw_BulkObjectives1]
AS
SELECT DISTINCT TOP (100) PERCENT cp.PERSPECTIVE_ID AS KeyFocusAreaId, bpm.KeyFocusArea, bpm.StrategicObjective, bpm.StrategicObjectiveRationale, bpm.CompanyId
FROM         imd_user_db.BulkPerformanceMeasure AS bpm WITH (nolock) INNER JOIN
                      imd_user_db.COMPANY_PERSPECTIVE AS cp WITH (nolock) ON bpm.KeyFocusArea = cp.PERSPECTIVE_NAME AND bpm.CompanyId = cp.COMPANY_ID
ORDER BY bpm.StrategicObjective