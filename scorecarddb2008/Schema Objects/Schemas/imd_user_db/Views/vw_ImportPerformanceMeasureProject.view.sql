﻿CREATE VIEW imd_user_db.vw_ImportPerformanceMeasureProject
AS
SELECT     cp.PERSPECTIVE_NAME AS KeyFocusArea, cp.PERSPECTIVE_DESC AS KeyFocusAreaDescription, cs.OBJECTIVE AS StrategicObjective, cs.RATIONALE AS StrategicObjectiveRationale, 
                      ig.GOAL_DESCRIPTION AS PerformanceMeasure, CASE WHEN ig.HAS_SUB_GOALS = 1 THEN 'Yes' ELSE 'No' END AS HasSubPerformanceMeasures, ISNULL(p.GOAL_DESCRIPTION, '') 
                      AS ParentPerformanceMeasure, atp.TYPE_NAME AS AreaOfMeasureType, ISNULL(jb.PROJECT_NAME, '') AS AreaOfMeasure, um.TYPE_NAME AS UnitOfMeasure, ig.TARGET, ig.WEIGHT
FROM         imd_user_db.COMPANY_PERSPECTIVE AS cp WITH (nolock) INNER JOIN
                      imd_user_db.COMPANY_STRATEGIC AS cs WITH (nolock) ON cp.PERSPECTIVE_ID = cs.PERSPECTIVE INNER JOIN
                      imd_user_db.INDIVIDUAL_GOALS AS ig WITH (nolock) ON cs.ID = ig.STRATEGIC_ID LEFT OUTER JOIN
                      imd_user_db.COMPANY_PROJECT AS jb WITH (nolock) ON ig.AREA_ID = jb.PROJECT_ID LEFT OUTER JOIN
                      imd_user_db.UTILITIES AS atp WITH (nolock) ON ig.AREA_TYPE_ID = atp.ID LEFT OUTER JOIN
                      imd_user_db.UTILITIES AS um WITH (nolock) ON ig.UNIT_MEASURE = um.ID LEFT OUTER JOIN
                      imd_user_db.INDIVIDUAL_GOALS AS p WITH (nolock) ON ig.PARENT_ID = p.ID
WHERE     (ig.AREA_TYPE_ID = 49)