﻿CREATE TABLE [imd_user_db].[BulkPerformanceMeasure] (
    [KeyFocusArea]                VARCHAR (1000) NULL,
    [KeyFocusAreaDescription]     VARCHAR (1000) NULL,
    [StrategicObjective]          VARCHAR (1000) NULL,
    [StrategicObjectiveRationale] VARCHAR (4000) NULL,
    [PerformanceMeasure]          VARCHAR (1000) NULL,
    [HasSubPerformanceMeasures]   VARCHAR (255)  NULL,
    [ParentPerformanceMeasure]    VARCHAR (1000) NULL,
    [AreaOfMeasureType]           VARCHAR (255)  NULL,
    [AreaOfMeasure]               VARCHAR (255)  NULL,
    [UnitOfMeasure]               VARCHAR (255)  NULL,
    [TARGET]                      VARCHAR (255)  NULL,
    [WEIGHT]                      VARCHAR (255)  NULL,
    [CompanyId]                   INT            NOT NULL,
    [DateCreated]                 DATETIME       NOT NULL,
    [Added]                       BIT            NOT NULL
);

