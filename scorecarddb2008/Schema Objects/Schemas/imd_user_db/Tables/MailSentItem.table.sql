﻿CREATE TABLE [imd_user_db].[MailSentItem] (
    [SentItemId]   BIGINT         IDENTITY (1, 1) NOT NULL,
    [EmployeeId]   INT            NOT NULL,
    [FullName]     VARCHAR (100)  COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [Email]        VARCHAR (80)   COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [BatchId]      INT            NOT NULL,
    [MailSubject]  VARCHAR (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [MailContent]  NVARCHAR (MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [MainTemplate] NVARCHAR (MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [IsSent]       BIT            NOT NULL,
    [DateToBeSent] DATETIME       NULL
);

