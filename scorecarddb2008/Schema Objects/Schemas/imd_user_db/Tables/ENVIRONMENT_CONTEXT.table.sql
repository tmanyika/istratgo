﻿CREATE TABLE [imd_user_db].[ENVIRONMENT_CONTEXT] (
    [ENV_CONTEXT_ID] INT            IDENTITY (1, 1) NOT NULL,
    [COMPANY_ID]     INT            NOT NULL,
    [AREA_TYPE_ID]   INT            NOT NULL,
    [AREA_ID]        INT            NOT NULL,
    [ENV_CONTEXT]    VARCHAR (1000) NOT NULL,
    [DATE_CREATED]   DATETIME       NOT NULL,
    [DATE_UPDATED]   DATETIME       NULL
);

