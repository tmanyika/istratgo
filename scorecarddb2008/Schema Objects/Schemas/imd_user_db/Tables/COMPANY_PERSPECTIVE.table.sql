﻿CREATE TABLE [imd_user_db].[COMPANY_PERSPECTIVE] (
    [PERSPECTIVE_ID]   INT            IDENTITY (1, 1) NOT NULL,
    [COMPANY_ID]       INT            NOT NULL,
    [PERSPECTIVE_NAME] VARCHAR (1000) NOT NULL,
    [PERSPECTIVE_DESC] VARCHAR (1000) NULL,
    [ACTIVE]           BIT            NOT NULL,
    [CREATED_BY]       VARCHAR (80)   NOT NULL,
    [DATE_CREATED]     DATETIME       NOT NULL,
    [DATE_UPDATED]     DATETIME       NULL,
    [UPDATE_BY]        VARCHAR (80)   COLLATE SQL_Latin1_General_CP1_CI_AS NULL
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Stores all companies'' perspectives.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'COMPANY_PERSPECTIVE';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Uniquely identifies a perspective in the table.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'COMPANY_PERSPECTIVE', @level2type = N'COLUMN', @level2name = N'PERSPECTIVE_ID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Company Id as define in the COMPANY_DETAIL table.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'COMPANY_PERSPECTIVE', @level2type = N'COLUMN', @level2name = N'COMPANY_ID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The name of the perspective.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'COMPANY_PERSPECTIVE', @level2type = N'COLUMN', @level2name = N'PERSPECTIVE_NAME';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The name of the perspective.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'COMPANY_PERSPECTIVE', @level2type = N'COLUMN', @level2name = N'PERSPECTIVE_DESC';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Indocates if a perspective is still in use or not. 1 = In Use, 0 =  No Longer Used.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'COMPANY_PERSPECTIVE', @level2type = N'COLUMN', @level2name = N'ACTIVE';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Login ID of the person who created the record. Login ID is defined in the USER_ADMIN table', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'COMPANY_PERSPECTIVE', @level2type = N'COLUMN', @level2name = N'CREATED_BY';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date when the record was created.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'COMPANY_PERSPECTIVE', @level2type = N'COLUMN', @level2name = N'DATE_CREATED';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date when the record was last updated.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'COMPANY_PERSPECTIVE', @level2type = N'COLUMN', @level2name = N'DATE_UPDATED';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Login ID of the person who last updated the record. Login ID is defined in the USER_ADMIN table', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'COMPANY_PERSPECTIVE', @level2type = N'COLUMN', @level2name = N'UPDATE_BY';

