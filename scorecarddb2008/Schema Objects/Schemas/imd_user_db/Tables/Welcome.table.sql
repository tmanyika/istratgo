﻿CREATE TABLE [imd_user_db].[Welcome] (
    [WelcomeId]             INT            IDENTITY (1, 1) NOT NULL,
    [WelcomeSectionName]    VARCHAR (500)  NOT NULL,
    [WelcomeDisplayCaption] VARCHAR (1000) NULL,
    [VirtualPath]           VARCHAR (500)  NULL,
    [PopupPageName]         VARCHAR (100)  NOT NULL,
    [Position]              INT            NOT NULL,
    [Active]                BIT            NOT NULL,
    [DateCreated]           DATETIME       NOT NULL,
    [CreatedBy]             VARCHAR (80)   NOT NULL,
    [DateUpdate]            DATETIME       NULL,
    [UpdatedBy]             VARCHAR (80)   NOT NULL
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Uniquely identifies a section / control of the welcome page in the table.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'Welcome', @level2type = N'COLUMN', @level2name = N'WelcomeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Welcome section name', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'Welcome', @level2type = N'COLUMN', @level2name = N'WelcomeSectionName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'what top display to the users.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'Welcome', @level2type = N'COLUMN', @level2name = N'WelcomeDisplayCaption';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Virtual path of the control to load.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'Welcome', @level2type = N'COLUMN', @level2name = N'VirtualPath';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Popup page name to display the content', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'Welcome', @level2type = N'COLUMN', @level2name = N'PopupPageName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Position on which the welcome section should be displayed.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'Welcome', @level2type = N'COLUMN', @level2name = N'Position';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Indicates if a welcome portion of the page is still active or not.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'Welcome', @level2type = N'COLUMN', @level2name = N'Active';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date when the record was created.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'Welcome', @level2type = N'COLUMN', @level2name = N'DateCreated';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The user name of the person who created the record.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'Welcome', @level2type = N'COLUMN', @level2name = N'CreatedBy';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date when the record was last updated.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'Welcome', @level2type = N'COLUMN', @level2name = N'DateUpdate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The user name of the person who last updated the record.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'Welcome', @level2type = N'COLUMN', @level2name = N'UpdatedBy';

