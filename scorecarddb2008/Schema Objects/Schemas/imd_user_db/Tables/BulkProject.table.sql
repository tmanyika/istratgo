﻿CREATE TABLE [imd_user_db].[BulkProject] (
    [ProjectName]        VARCHAR (255) NULL,
    [ProjectDescription] VARCHAR (400) NULL,
    [ProjectOwnerEmail]  VARCHAR (255) NULL,
    [CompanyId]          INT           NOT NULL,
    [DateCreated]        DATETIME      NOT NULL,
    [Added]              BIT           NOT NULL
);

