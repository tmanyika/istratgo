﻿CREATE TABLE [imd_user_db].[UTILITIES] (
    [ID]           INT          IDENTITY (1, 1) NOT NULL,
    [PARENT_ID]    INT          NULL,
    [TYPE_NAME]    VARCHAR (50) NOT NULL,
    [DESCRIPTION]  VARCHAR (50) NULL,
    [DATE_CREATED] DATETIME     NULL,
    [ACTIVE]       BIT          NULL
);

