﻿CREATE TABLE [imd_user_db].[WelcomeState] (
    [WelcomeId]         INT      NOT NULL,
    [EmployeeId]        INT      NOT NULL,
    [ShowAgain]         BIT      NOT NULL,
    [DateLastRequested] DATETIME NULL
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Stores welcome status of a section for each active user', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'WelcomeState';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Welcome Id. This is defined in the Welcome table.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'WelcomeState', @level2type = N'COLUMN', @level2name = N'WelcomeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Employee Id is defined in the Employee table', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'WelcomeState', @level2type = N'COLUMN', @level2name = N'EmployeeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Indicates if the welcome content should be shown again or not. 0 = Do not show again, 1 = Show again', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'WelcomeState', @level2type = N'COLUMN', @level2name = N'ShowAgain';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date last requested. Date on which the user requested to show or not to show again the welcome page section.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'WelcomeState', @level2type = N'COLUMN', @level2name = N'DateLastRequested';

