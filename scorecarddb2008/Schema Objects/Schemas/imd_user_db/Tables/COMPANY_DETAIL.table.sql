﻿CREATE TABLE [imd_user_db].[COMPANY_DETAIL] (
    [COMPANY_ID]      INT           IDENTITY (1, 1) NOT NULL,
    [ACCOUNT_NO]      INT           NULL,
    [COMPANY_NAME]    VARCHAR (150) NULL,
    [REGISTRATION_NO] NCHAR (20)    NULL,
    [INDUSTRY]        INT           NULL,
    [COUNTRY]         INT           NULL,
    [VAT_NO]          VARCHAR (50)  NULL,
    [ADDRESS]         VARCHAR (150) NULL,
    [CONTACT_NO]      VARCHAR (50)  NULL,
    [ACTIVE]          BIT           NULL,
    [DATE_REGISTERED] DATETIME      NOT NULL,
    [PAID]            BIT           NOT NULL
);

