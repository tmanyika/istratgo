﻿CREATE NONCLUSTERED INDEX [CompanyId_IDX]
    ON [imd_user_db].[Employee]([OrgUnitId] ASC)
    INCLUDE([BusTelephone], [Cellphone], [CreatedBy], [DateCreated], [DateUpdated], [EmailAddress], [EmployeeId], [EmployeeNo], [FirstName], [JobTitleId], [LastName], [LineManagerEmployeeId], [MiddleName], [StatusId], [UpdatedBy], [WorkingWeekId]) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
    ON [PRIMARY];

