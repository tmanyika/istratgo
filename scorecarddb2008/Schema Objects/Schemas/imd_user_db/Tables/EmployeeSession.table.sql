﻿CREATE TABLE [imd_user_db].[EmployeeSession] (
    [SessionId]       UNIQUEIDENTIFIER NOT NULL,
    [EmployeeId]      INT              NULL,
    [IPAddress]       VARCHAR (25)     COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [Browser]         VARCHAR (25)     COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [Platform]        VARCHAR (25)     COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [StartDate]       DATETIME         NOT NULL,
    [AppVersion]      VARCHAR (10)     COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [AssemblyVersion] VARCHAR (10)     COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [EndDate]         DATETIME         NULL
);

