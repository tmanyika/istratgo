﻿CREATE TABLE [imd_user_db].[LeaveTransaction] (
    [TransactionId]   BIGINT          IDENTITY (1, 1) NOT NULL,
    [EmployeeId]      INT             NOT NULL,
    [LeaveId]         INT             NOT NULL,
    [ApplicationId]   INT             NULL,
    [HolidayDateId]   INT             NULL,
    [LeaveDays]       DECIMAL (18, 4) NOT NULL,
    [TransactionDate] DATETIME        NOT NULL,
    [SourceOfTrans]   VARCHAR (60)    COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
);

