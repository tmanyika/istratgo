﻿CREATE TABLE [imd_user_db].[LeaveStatus] (
    [StatusId]    INT          IDENTITY (1, 1) NOT NULL,
    [StatusName]  VARCHAR (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [Active]      BIT          NOT NULL,
    [CreatedBy]   VARCHAR (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [DateCreated] DATETIME     NOT NULL,
    [DateUpdated] DATETIME     NULL,
    [UpdatedBy]   VARCHAR (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
);

