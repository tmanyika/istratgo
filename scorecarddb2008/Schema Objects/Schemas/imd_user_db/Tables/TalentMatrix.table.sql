﻿CREATE TABLE [imd_user_db].[TalentMatrix] (
    [MatrixId]                INT             IDENTITY (1, 1) NOT NULL,
    [CompanyId]               INT             NOT NULL,
    [TalentMatrixName]        VARCHAR (300)   NOT NULL,
    [TalentMatrixDescription] VARCHAR (2000)  NULL,
    [LowerBoundSign]          VARCHAR (4)     NOT NULL,
    [LowerBound]              DECIMAL (18, 1) NOT NULL,
    [UpperBoundSign]          VARCHAR (4)     NOT NULL,
    [UpperBound]              DECIMAL (18, 1) NOT NULL,
    [PositionInMatrix]        INT             NOT NULL,
    [Active]                  BIT             NOT NULL,
    [CreatedBy]               VARCHAR (20)    NULL,
    [DateCreated]             DATETIME        NOT NULL,
    [UpdatedBy]               VARCHAR (20)    NULL,
    [DateUpdated]             DATETIME        NULL
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Stores business rules for talent matrix', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'TalentMatrix';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Lower Bound Sign. This can be >, >=, <=, <', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'TalentMatrix', @level2type = N'COLUMN', @level2name = N'LowerBoundSign';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Lower bound value', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'TalentMatrix', @level2type = N'COLUMN', @level2name = N'LowerBound';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Upper bound sign. This can be >, >=, <=, <', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'TalentMatrix', @level2type = N'COLUMN', @level2name = N'UpperBoundSign';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Upper bound value', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'TalentMatrix', @level2type = N'COLUMN', @level2name = N'UpperBound';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'User name or full name of the person who created the record.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'TalentMatrix', @level2type = N'COLUMN', @level2name = N'CreatedBy';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date when the record was created.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'TalentMatrix', @level2type = N'COLUMN', @level2name = N'DateCreated';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'User name or full name of the person who created the record.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'TalentMatrix', @level2type = N'COLUMN', @level2name = N'UpdatedBy';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date when the record was created.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'TalentMatrix', @level2type = N'COLUMN', @level2name = N'DateUpdated';

