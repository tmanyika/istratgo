﻿CREATE TABLE [imd_user_db].[APP_MENU] (
    [MENU_ID]        INT           IDENTITY (1, 1) NOT NULL,
    [PARENT_MENU_ID] INT           NULL,
    [MENU_NAME]      VARCHAR (50)  NULL,
    [IMAGE_ICON]     VARCHAR (150) NULL,
    [MENU_PATH]      VARCHAR (50)  NULL,
    [ORDER_ID]       INT           NULL,
    [ACTIVE]         BIT           NULL,
    [POSITION]       SMALLINT      NULL
);

