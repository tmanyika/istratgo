﻿CREATE TABLE [imd_user_db].[BatchAttachment] (
    [AttachmentId] INT           IDENTITY (1, 1) NOT NULL,
    [BatchId]      INT           NOT NULL,
    [DocumentPath] VARCHAR (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Stores email batch attachment full path.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'BatchAttachment';

