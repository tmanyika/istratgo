﻿CREATE TABLE [imd_user_db].[BulkJobTitle] (
    [JobTitleName] VARCHAR (255) NULL,
    [CompanyId]    INT           NOT NULL,
    [DateCreated]  DATETIME      NOT NULL,
    [Added]        BIT           NOT NULL
);

