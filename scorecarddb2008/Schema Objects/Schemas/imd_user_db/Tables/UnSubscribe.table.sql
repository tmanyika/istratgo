﻿CREATE TABLE [imd_user_db].[UnSubscribe] (
    [UnsubscribeId] INT          IDENTITY (1, 1) NOT NULL,
    [EmployeeId]    INT          NULL,
    [EmailAddress]  VARCHAR (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Uniquely identifies a record in the table.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'UnSubscribe', @level2type = N'COLUMN', @level2name = N'UnsubscribeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Employee ID / Person ID as defined in the Employee table', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'UnSubscribe', @level2type = N'COLUMN', @level2name = N'EmployeeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Email Address of the subscriber.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'UnSubscribe', @level2type = N'COLUMN', @level2name = N'EmailAddress';

