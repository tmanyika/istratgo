﻿CREATE TABLE [imd_user_db].[BulkSheet] (
    [SheetId]          INT            IDENTITY (1, 1) NOT NULL,
    [SheetName]        VARCHAR (150)  NOT NULL,
    [DbTableName]      VARCHAR (50)   NOT NULL,
    [SheetDescription] VARCHAR (1000) NULL,
    [SheetNotes]       VARCHAR (800)  NULL,
    [Active]           BIT            NOT NULL,
    [OrderNumber]      TINYINT        NOT NULL,
    [CreatedBy]        VARCHAR (50)   NULL,
    [DateCreated]      DATETIME       NOT NULL,
    [UpdatedBy]        VARCHAR (50)   NULL,
    [DateUpdated]      DATETIME       NULL
);

