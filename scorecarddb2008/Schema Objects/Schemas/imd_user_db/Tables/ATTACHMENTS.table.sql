﻿CREATE TABLE [imd_user_db].[ATTACHMENTS] (
    [ID]              INT           IDENTITY (1, 1) NOT NULL,
    [ATTACHMENT_NAME] VARCHAR (450) NULL,
    [SUBMISSION_ID]   INT           NULL
);

