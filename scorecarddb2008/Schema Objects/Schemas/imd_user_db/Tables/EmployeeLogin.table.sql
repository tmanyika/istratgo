﻿CREATE TABLE [imd_user_db].[EmployeeLogin] (
    [EmployeeId]    INT           NOT NULL,
    [UserName]      VARCHAR (120) NOT NULL,
    [Pass]          VARCHAR (250) NOT NULL,
    [RoleId]        INT           NOT NULL,
    [Active]        BIT           NOT NULL,
    [DateUpdated]   DATETIME      NULL,
    [LastLoginTime] DATETIME      NULL
);



