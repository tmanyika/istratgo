﻿CREATE TABLE [imd_user_db].[INDIVIDUAL_GOALS] (
    [ID]               INT            IDENTITY (1, 1) NOT NULL,
    [PARENT_ID]        INT            NULL,
    [STRATEGIC_ID]     INT            NULL,
    [AREA_TYPE_ID]     INT            NULL,
    [AREA_ID]          INT            NULL,
    [UNIT_MEASURE]     INT            NULL,
    [HAS_SUB_GOALS]    BIT            NOT NULL,
    [ENV_CONTEXT_ID]   INT            NULL,
    [RATIONAL]         VARCHAR (1000) NULL,
    [TARGET]           VARCHAR (150)  NULL,
    [WEIGHT]           INT            NULL,
    [DATE_CREATED]     DATETIME       NOT NULL,
    [GOAL_DESCRIPTION] VARCHAR (1000) NULL,
    [MEASURE]          VARCHAR (600)  NULL,
    [PERIOD_DATE]      DATETIME       NOT NULL,
    [DATE_UPDATED]     DATETIME       NULL,
    [ACTIVE]           BIT            NOT NULL,
    [UPDATEDBY]        VARCHAR (100)  NULL,
    [CREATEDBY]        VARCHAR (100)  NULL
);



