﻿CREATE TABLE [imd_user_db].[EmploymentHist] (
    [HistoryId]        INT             IDENTITY (1, 1) NOT NULL,
    [EmployeeId]       INT             NOT NULL,
    [JobTitleId]       INT             NOT NULL,
    [PreviousPosition] BIT             NOT NULL,
    [StartDate]        DATETIME        NULL,
    [EndDate]          DATETIME        NULL,
    [DaysOnSameRole]   DECIMAL (18, 2) NULL,
    [YearsOnSameRole]  DECIMAL (18, 2) NULL,
    [Active]           BIT             NOT NULL,
    [CreatedBy]        VARCHAR (20)    NULL,
    [DateCreated]      DATETIME        NOT NULL,
    [UpdatedBy]        VARCHAR (20)    NULL,
    [DateUpdated]      DATETIME        NULL
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Stores job title employment history.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'EmploymentHist';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'History Id uniquely identifies the record in the table.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'EmploymentHist', @level2type = N'COLUMN', @level2name = N'HistoryId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Employee Id as defined in the Employee table', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'EmploymentHist', @level2type = N'COLUMN', @level2name = N'EmployeeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Job Title Id as defined in the COMPANY_JOB_TITLES table', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'EmploymentHist', @level2type = N'COLUMN', @level2name = N'JobTitleId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date when the employee assumed the role', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'EmploymentHist', @level2type = N'COLUMN', @level2name = N'StartDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Termination date of the role.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'EmploymentHist', @level2type = N'COLUMN', @level2name = N'EndDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Days on same role', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'EmploymentHist', @level2type = N'COLUMN', @level2name = N'DaysOnSameRole';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Years on same role', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'EmploymentHist', @level2type = N'COLUMN', @level2name = N'YearsOnSameRole';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Indicates the current position that an employee is on. 1 = Current Position, 0 = Old Position', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'EmploymentHist', @level2type = N'COLUMN', @level2name = N'Active';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'User name or full name of the person who created the record.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'EmploymentHist', @level2type = N'COLUMN', @level2name = N'CreatedBy';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date when the record was created.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'EmploymentHist', @level2type = N'COLUMN', @level2name = N'DateCreated';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'User name or full name of the person who created the record.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'EmploymentHist', @level2type = N'COLUMN', @level2name = N'UpdatedBy';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date when the record was last updated.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'EmploymentHist', @level2type = N'COLUMN', @level2name = N'DateUpdated';

