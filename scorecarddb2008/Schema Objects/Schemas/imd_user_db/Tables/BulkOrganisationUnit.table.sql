﻿CREATE TABLE [imd_user_db].[BulkOrganisationUnit] (
    [OrganisationUnitName]       VARCHAR (255) NULL,
    [ParentOrganisationUnitName] VARCHAR (255) NULL,
    [OrganisationUnitType]       VARCHAR (255) NULL,
    [OrganisationUnitOwnerEmail] VARCHAR (255) NULL,
    [CompanyId]                  INT           NOT NULL,
    [DateCreated]                DATETIME      NOT NULL,
    [Added]                      BIT           NOT NULL
);

