﻿CREATE TABLE [imd_user_db].[LeaveCalendarCompany] (
    [LinkedId]      BIGINT       IDENTITY (1, 1) NOT NULL,
    [HolidayDateId] BIGINT       NOT NULL,
    [OrgUnitId]     INT          NOT NULL,
    [Applied]       BIT          NOT NULL,
    [CreatedBy]     VARCHAR (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [DateCreated]   DATETIME     NOT NULL,
    [UpdatedBy]     VARCHAR (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [DateUpdated]   DATETIME     NULL
);

