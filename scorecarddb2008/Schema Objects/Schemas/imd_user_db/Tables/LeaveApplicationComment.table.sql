﻿CREATE TABLE [imd_user_db].[LeaveApplicationComment] (
    [CommentId]     BIGINT         IDENTITY (1, 1) NOT NULL,
    [ApplicationId] INT            NOT NULL,
    [DocumentTitle] VARCHAR (200)  COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [Comment]       VARCHAR (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [SupportingDoc] VARCHAR (200)  COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [Valid]         BIT            NOT NULL,
    [UploadedBy]    VARCHAR (20)   COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [DateCreated]   DATETIME       NOT NULL,
    [UpdatedBy]     VARCHAR (20)   COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [DateUpdated]   DATETIME       NULL
);

