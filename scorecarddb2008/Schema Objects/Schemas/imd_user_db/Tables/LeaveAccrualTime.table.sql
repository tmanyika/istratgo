﻿CREATE TABLE [imd_user_db].[LeaveAccrualTime] (
    [AccrualTimeId] INT           IDENTITY (1, 1) NOT NULL,
    [TimeName]      VARCHAR (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [TimeValue]     SMALLINT      NOT NULL,
    [Active]        BIT           NOT NULL,
    [CreatedBy]     VARCHAR (20)  COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [DateCreated]   DATETIME      NOT NULL,
    [DateUpdated]   DATETIME      NULL,
    [UpdatedBy]     VARCHAR (20)  COLLATE SQL_Latin1_General_CP1_CI_AS NULL
);

