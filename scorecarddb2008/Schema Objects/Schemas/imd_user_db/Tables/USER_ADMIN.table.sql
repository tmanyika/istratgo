﻿CREATE TABLE [imd_user_db].[USER_ADMIN] (
    [ID]            INT           IDENTITY (1, 1) NOT NULL,
    [LOGIN_ID]      VARCHAR (20)  NOT NULL,
    [PASSWORD]      VARCHAR (250) NOT NULL,
    [REPORTS_TO]    INT           NULL,
    [ORG_ID]        INT           NULL,
    [JOB_TITLE_ID]  INT           NULL,
    [NAME]          VARCHAR (100) NULL,
    [EMAIL_ADDRESS] VARCHAR (150) NULL,
    [DATE_CREATED]  DATETIME      NULL,
    [ACTIVE]        BIT           NOT NULL,
    [USER_TYPE]     INT           NULL
);

