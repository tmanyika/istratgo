﻿CREATE TABLE [imd_user_db].[MailBatch] (
    [BatchId]       INT            IDENTITY (1, 1) NOT NULL,
    [BatchName]     VARCHAR (200)  COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [AccountMailId] INT            NOT NULL,
    [MailSubject]   VARCHAR (800)  COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [MailContent]   NVARCHAR (MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [MainTemplate]  NVARCHAR (MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [NoOfUsers]     INT            NOT NULL,
    [IsSent]        BIT            NOT NULL,
    [DateToBeSent]  DATETIME       NOT NULL,
    [CreatedBy]     VARCHAR (20)   COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [DateCreated]   DATETIME       NOT NULL
);

