﻿CREATE TABLE [imd_user_db].[SSISLog] (
    [ErrorLogId]   INT            IDENTITY (1, 1) NOT NULL,
    [ErrorCode]    INT            NULL,
    [ErrorColumn]  VARCHAR (256)  COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [ErrorDetails] XML            NULL,
    [ErrorDesc]    NVARCHAR (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [ErrorStep]    NVARCHAR (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [ErrorTask]    NVARCHAR (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [PackageTime]  SMALLDATETIME  NULL,
    [PackageName]  NVARCHAR (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [UserName]     NVARCHAR (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
);

