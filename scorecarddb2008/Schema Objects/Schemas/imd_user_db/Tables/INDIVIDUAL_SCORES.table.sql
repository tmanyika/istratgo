﻿CREATE TABLE [imd_user_db].[INDIVIDUAL_SCORES] (
    [ID]                    INT             IDENTITY (1, 1) NOT NULL,
    [CRITERIA_TYPE_ID]      INT             NULL,
    [SELECTED_ITEM_VALUE]   INT             NULL,
    [GOAL_ID]               INT             NOT NULL,
    [PARENT_ID]             INT             NULL,
    [HAS_SUB_GOALS]         BIT             NOT NULL,
    [SCORES]                VARCHAR (250)   NULL,
    [MANAGER_SCORE]         VARCHAR (250)   NULL,
    [AGREED_SCORE]          VARCHAR (250)   NULL,
    [FINAL_SCORE]           DECIMAL (18, 2) NOT NULL,
    [DATE_CREATED]          DATETIME        NULL,
    [SUBMISSION_ID]         INT             NULL,
    [PERIOD_DATE]           DATETIME        NOT NULL,
    [RESPONSIBLE_USER_ID]   INT             NULL,
    [JUSTIFICATION_COMMENT] VARCHAR (4000)  NULL,
    [EVIDENCE]              VARCHAR (4000)  NULL,
    [MANAGER_COMMENT]       VARCHAR (1000)  NULL,
    [RATINGVALUE]           DECIMAL (18, 2) NULL,
    [FINAL_SCORE_AGREED]    DECIMAL (18, 2) NULL,
    [DATE_UPDATED]          DATETIME        NULL
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Correct Company Defined Weighted Score', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'INDIVIDUAL_SCORES', @level2type = N'COLUMN', @level2name = N'FINAL_SCORE_AGREED';

