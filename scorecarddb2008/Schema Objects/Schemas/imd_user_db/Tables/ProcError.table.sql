﻿CREATE TABLE [imd_user_db].[ProcError] (
    [ErrorId]        INT             IDENTITY (1, 1) NOT NULL,
    [ErrorNo]        INT             NULL,
    [ErrorSeverity]  INT             NULL,
    [ErrorState]     INT             NULL,
    [ErrorProcedure] VARCHAR (200)   COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [ErrorLine]      INT             NULL,
    [ErrorMessage]   NVARCHAR (3000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [DateCreated]    DATETIME        NOT NULL
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Stored procedure errors.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'ProcError';

