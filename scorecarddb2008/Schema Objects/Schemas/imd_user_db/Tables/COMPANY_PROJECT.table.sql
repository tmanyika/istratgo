﻿CREATE TABLE [imd_user_db].[COMPANY_PROJECT] (
    [PROJECT_ID]            INT           IDENTITY (1, 1) NOT NULL,
    [COMPANY_ID]            INT           NOT NULL,
    [PROJECT_NAME]          VARCHAR (200) NOT NULL,
    [PROJECT_DESC]          VARCHAR (400) NULL,
    [ACTIVE]                BIT           NOT NULL,
    [CREATED_BY]            VARCHAR (20)  NOT NULL,
    [DATE_CREATED]          DATETIME      NOT NULL,
    [DATE_UPDATED]          DATETIME      NULL,
    [UPDATE_BY]             VARCHAR (20)  NULL,
    [RESPONSIBLE_PERSON_ID] INT           NULL
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Stores all projects that companies will be creating.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'COMPANY_PROJECT';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Uniquely identifies a project in the table.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'COMPANY_PROJECT', @level2type = N'COLUMN', @level2name = N'PROJECT_ID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Company Id as define in the COMPANY_DETAIL table.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'COMPANY_PROJECT', @level2type = N'COLUMN', @level2name = N'COMPANY_ID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The name of the project.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'COMPANY_PROJECT', @level2type = N'COLUMN', @level2name = N'PROJECT_NAME';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Project description.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'COMPANY_PROJECT', @level2type = N'COLUMN', @level2name = N'PROJECT_DESC';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Indicates if a project is still in use or not. 1 = In Use, 0 =  No Longer Used.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'COMPANY_PROJECT', @level2type = N'COLUMN', @level2name = N'ACTIVE';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Login ID of the person who created the record. Login ID is defined in the USER_ADMIN table', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'COMPANY_PROJECT', @level2type = N'COLUMN', @level2name = N'CREATED_BY';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date when the record was created.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'COMPANY_PROJECT', @level2type = N'COLUMN', @level2name = N'DATE_CREATED';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date when the record was last updated.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'COMPANY_PROJECT', @level2type = N'COLUMN', @level2name = N'DATE_UPDATED';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Login ID of the person who last updated the record. Login ID is defined in the USER_ADMIN table', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'COMPANY_PROJECT', @level2type = N'COLUMN', @level2name = N'UPDATE_BY';

