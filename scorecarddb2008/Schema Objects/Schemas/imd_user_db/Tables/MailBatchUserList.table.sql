﻿CREATE TABLE [imd_user_db].[MailBatchUserList] (
    [BatchId]    INT          NOT NULL,
    [EmployeeId] INT          NOT NULL,
    [Email]      VARCHAR (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [Exported]   BIT          NOT NULL
);

