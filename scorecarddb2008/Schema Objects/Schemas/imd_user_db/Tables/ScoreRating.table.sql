﻿CREATE TABLE [imd_user_db].[ScoreRating] (
    [RatingId]          INT             IDENTITY (1, 1) NOT NULL,
    [CompanyId]         INT             NOT NULL,
    [RatingValue]       DECIMAL (18, 1) NOT NULL,
    [MinimumValue]      DECIMAL (18, 2) NULL,
    [MaximumValue]      DECIMAL (18, 2) NULL,
    [RatingDescription] VARCHAR (100)   COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [Active]            BIT             NOT NULL,
    [CreatedBy]         VARCHAR (50)    COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [DateCreated]       DATETIME        NOT NULL,
    [DateUpdated]       DATETIME        NULL,
    [UpdatedBy]         VARCHAR (50)    COLLATE SQL_Latin1_General_CP1_CI_AS NULL
);

