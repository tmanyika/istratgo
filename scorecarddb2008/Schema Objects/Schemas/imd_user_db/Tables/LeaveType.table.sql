﻿CREATE TABLE [imd_user_db].[LeaveType] (
    [LeaveId]          INT           IDENTITY (1, 1) NOT NULL,
    [CompanyId]        INT           NOT NULL,
    [LeaveName]        VARCHAR (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [LeaveDescription] VARCHAR (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [Active]           BIT           NOT NULL,
    [CreatedBy]        VARCHAR (20)  COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [DateCreated]      DATETIME      NOT NULL,
    [DateUpdated]      DATETIME      NULL,
    [UpdatedBy]        VARCHAR (20)  COLLATE SQL_Latin1_General_CP1_CI_AS NULL
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'STores leave type', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'LeaveType';

