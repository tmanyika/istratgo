﻿CREATE TABLE [imd_user_db].[MailServer] (
    [AccountMailId]  INT             IDENTITY (1, 1) NOT NULL,
    [AccountName]    VARCHAR (200)   COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [SqlProfileName] VARCHAR (100)   COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [UserName]       VARCHAR (100)   COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [PIN]            VARBINARY (128) NOT NULL,
    [SmtpServer]     VARCHAR (100)   COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [Port]           INT             NULL,
    [Active]         BIT             NOT NULL,
    [CreatedBy]      VARCHAR (20)    COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [DateCreated]    DATETIME        NOT NULL,
    [DateUpdated]    DATETIME        NULL,
    [UpdatedBy]      VARCHAR (20)    COLLATE SQL_Latin1_General_CP1_CI_AS NULL
);

