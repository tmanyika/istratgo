﻿CREATE TABLE [imd_user_db].[MailTemplate] (
    [MailId]           INT            IDENTITY (1, 1) NOT NULL,
    [TemplateId]       INT            NULL,
    [MailName]         VARCHAR (800)  COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [MailSubject]      VARCHAR (800)  COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [MailContent]      NVARCHAR (MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [IsMasterTemplate] BIT            NOT NULL,
    [Active]           BIT            NOT NULL,
    [CreatedBy]        VARCHAR (20)   COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [DateCreated]      DATETIME       NOT NULL,
    [DateUpdated]      DATETIME       NULL,
    [UpdatedBy]        VARCHAR (20)   COLLATE SQL_Latin1_General_CP1_CI_AS NULL
);

