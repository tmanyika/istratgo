﻿CREATE TABLE [imd_user_db].[BulkSheetField] (
    [FieldId]           INT            IDENTITY (1, 1) NOT NULL,
    [SheetId]           INT            NOT NULL,
    [SheetFieldName]    VARCHAR (120)  NOT NULL,
    [DbFieldName]       VARCHAR (40)   NOT NULL,
    [IsRequired]        BIT            NOT NULL,
    [Active]            BIT            NOT NULL,
    [FieldNotes]        VARCHAR (1000) NULL,
    [DataType]          INT            NOT NULL,
    [ExpectedValueList] VARCHAR (2000) NULL,
    [CreatedBy]         VARCHAR (50)   NULL,
    [DateCreated]       DATETIME       NOT NULL,
    [UpdatedBy]         VARCHAR (50)   NULL,
    [DateUpdated]       DATETIME       NULL
);



