﻿CREATE TABLE [imd_user_db].[BulkEmployee] (
    [FirstName]           VARCHAR (255) NULL,
    [MiddleName]          VARCHAR (255) NULL,
    [LastName]            VARCHAR (255) NULL,
    [EmailAddress]        VARCHAR (255) NULL,
    [DateOfAppointment]   VARCHAR (255) NULL,
    [EmployeeNumber]      VARCHAR (255) NULL,
    [Cellphone]           VARCHAR (255) NULL,
    [LandlineTelephone]   VARCHAR (255) NULL,
    [JobTitle]            VARCHAR (255) NULL,
    [OrganisationUnit]    VARCHAR (255) NULL,
    [LineManagerEmail]    VARCHAR (255) NULL,
    [UserName]            VARCHAR (255) NULL,
    [SystemRole]          VARCHAR (255) NULL,
    [CompanyId]           INT           NOT NULL,
    [EmployeeId]          BIGINT        IDENTITY (1, 1) NOT NULL,
    [ImportFailureReason] VARCHAR (800) NULL,
    [DateCreated]         DATETIME      NOT NULL,
    [Added]               BIT           NOT NULL
);

