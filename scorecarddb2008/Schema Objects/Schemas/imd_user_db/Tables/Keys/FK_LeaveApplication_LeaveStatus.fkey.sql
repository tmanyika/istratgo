﻿ALTER TABLE [imd_user_db].[LeaveApplication]
    ADD CONSTRAINT [FK_LeaveApplication_LeaveStatus] FOREIGN KEY ([StatusId]) REFERENCES [imd_user_db].[LeaveStatus] ([StatusId]) ON DELETE NO ACTION ON UPDATE NO ACTION;

