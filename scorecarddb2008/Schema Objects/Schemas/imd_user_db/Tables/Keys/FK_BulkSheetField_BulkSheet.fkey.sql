﻿ALTER TABLE [imd_user_db].[BulkSheetField]
    ADD CONSTRAINT [FK_BulkSheetField_BulkSheet] FOREIGN KEY ([SheetId]) REFERENCES [imd_user_db].[BulkSheet] ([SheetId]) ON DELETE NO ACTION ON UPDATE NO ACTION;

