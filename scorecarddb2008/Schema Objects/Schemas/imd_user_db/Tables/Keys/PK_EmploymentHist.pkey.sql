﻿ALTER TABLE [imd_user_db].[EmploymentHist]
    ADD CONSTRAINT [PK_EmploymentHist] PRIMARY KEY CLUSTERED ([HistoryId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);

