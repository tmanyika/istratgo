﻿ALTER TABLE [imd_user_db].[EmployeeSession]
    ADD CONSTRAINT [FK_EmployeeSession_Employee] FOREIGN KEY ([EmployeeId]) REFERENCES [imd_user_db].[Employee] ([EmployeeId]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
ALTER TABLE [imd_user_db].[EmployeeSession] NOCHECK CONSTRAINT [FK_EmployeeSession_Employee];

