﻿ALTER TABLE [imd_user_db].[MailBatchUserList]
    ADD CONSTRAINT [FK_MailBatchUserList_MailBatch] FOREIGN KEY ([BatchId]) REFERENCES [imd_user_db].[MailBatch] ([BatchId]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
ALTER TABLE [imd_user_db].[MailBatchUserList] NOCHECK CONSTRAINT [FK_MailBatchUserList_MailBatch];

