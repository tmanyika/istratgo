﻿ALTER TABLE [imd_user_db].[MailBatchUserRole]
    ADD CONSTRAINT [FK_MailBatchUserRole_MailBatch] FOREIGN KEY ([BatchId]) REFERENCES [imd_user_db].[MailBatch] ([BatchId]) ON DELETE NO ACTION ON UPDATE NO ACTION;

