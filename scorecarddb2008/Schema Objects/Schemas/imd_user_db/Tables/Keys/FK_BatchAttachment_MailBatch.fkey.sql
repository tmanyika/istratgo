﻿ALTER TABLE [imd_user_db].[BatchAttachment]
    ADD CONSTRAINT [FK_BatchAttachment_MailBatch] FOREIGN KEY ([BatchId]) REFERENCES [imd_user_db].[MailBatch] ([BatchId]) ON DELETE NO ACTION ON UPDATE NO ACTION;

