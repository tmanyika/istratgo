﻿ALTER TABLE [imd_user_db].[Employee]
    ADD CONSTRAINT [FK_Employee_EmployeeStatus] FOREIGN KEY ([StatusId]) REFERENCES [imd_user_db].[EmployeeStatus] ([StatusId]) ON DELETE NO ACTION ON UPDATE NO ACTION;

