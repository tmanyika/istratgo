﻿CREATE TABLE [imd_user_db].[LOGIN_TOKEN] (
    [ID]           INT        NOT NULL,
    [TOKEN]        NCHAR (10) NULL,
    [EXPIRES]      DATETIME   NULL,
    [ACCOUNT_NO]   INT        NULL,
    [DATE_CREATED] DATETIME   NULL
);

