﻿CREATE TABLE [imd_user_db].[COMPANY_JOB_TITLES] (
    [ID]          INT          IDENTITY (1, 1) NOT NULL,
    [COMPANY_ID]  INT          NULL,
    [NAME]        VARCHAR (50) NULL,
    [ACTIVE]      BIT          NULL,
    [ISADMIN]     BIT          NULL,
    [CREATEDBY]   VARCHAR (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [DATECREATED] DATETIME     NOT NULL
);

