﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="goals.aspx.cs" MasterPageFile="~/ui/inside.Master"
    Inherits="scorecard.ui.goals" %>

<%@ Register Src="controls/orgstructure.ascx" TagName="orgstructure" TagPrefix="uc1" %>
<%@ Register Src="controls/goalwithhirachy.ascx" TagName="goalwithhirachy" TagPrefix="uc2" %>
<asp:Content ID="Header1" runat="server" ContentPlaceHolderID="Header">
    <link href="css/formoverride.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MainContent">
    <uc2:goalwithhirachy ID="goalwithhirachy1" runat="server" />
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="OrgStructure">
    <uc1:orgstructure ID="orgstructure1" runat="server" />
</asp:Content>
