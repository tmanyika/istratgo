﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.welcome.logic;
using scorecard.welcome.interfaces;
using scorecard.implementations;
using scorecard.welcome;
using scorecard.entities;

namespace scorecard.ui
{
    public partial class guideme : System.Web.UI.Page
    {
         IWelcome start;

         public guideme()
        {
            start = new WelcomeBL();
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ChkShowAgain_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (ChkShowAgain.Checked)
                {
                    bool status = ChkShowAgain.Checked ? false : true;
                    int employeeId = Util.user.UserId;
                    int welcomeId = int.Parse(Request.QueryString["welcomeId"].ToString());
                    start.SetEmployeeWelcomeStatus(
                        new Welcome
                        {
                            EmployeeId = employeeId,
                            WelcomeId = welcomeId,
                            ShowAgain = status
                        }
                        );

                    var data = start.GetShowAgainWelcome(employeeId);
                    UserInfo user = Session["UserInfo"] as UserInfo;
                    user.ShowAgain = data.Count > 0 ? true : false;
                    Session["UserInfo"] = user;
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }
    }
}