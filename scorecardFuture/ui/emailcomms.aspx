﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ui/inside.Master" AutoEventWireup="true"
    CodeBehind="emailcomms.aspx.cs" Inherits="scorecard.ui.emailcomms" ValidateRequest="false" %>

<%@ Register Src="controls/comms/emailCommunication.ascx" TagName="emailCommunication"
    TagPrefix="uc1" %>
<%@ Register Src="controls/orgstructure.ascx" TagName="orgstructure" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header" runat="server">
    <style type="text/css">
        .lf
        {
            width: 320px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <uc1:emailCommunication ID="emailCommunication1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="OrgStructure" runat="server">
    <uc2:orgstructure ID="orgstructure1" runat="server" />
</asp:Content>
