﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ui/inside.Master" AutoEventWireup="true" CodeBehind="settings.aspx.cs" Inherits="scorecard.ui.settings" %>
<%@ Register src="controls/settings.ascx" tagname="settings" tagprefix="uc1" %>
<%@ Register src="controls/orgstructure.ascx" tagname="orgstructure" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <uc1:settings ID="settings1" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="OrgStructure" runat="server">
 <uc1:orgstructure ID="orgstructure1" runat="server" />
</asp:Content>
