﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using scorecard.implementations;
using scorecard.entities;
using scorecard.controllers;
using System.Web.UI.DataVisualization.Charting;
using HR.Human.Resources;
using System.Collections;
using Dashboard.Reporting;
using scorecard.interfaces;
using System.Text;

namespace scorecard.ui.reports
{
    public partial class dashPerformanceRatingVsNumStaff : System.Web.UI.UserControl
    {
        #region Variables

        IDashboard dash;
        IScoreRating iR;

        #endregion

        #region Default Class Constructors

        public dashPerformanceRatingVsNumStaff()
        {
            dash = new DashboardBL();
            iR = new ScoreRatingBL();
        }

        #endregion

        #region Delegates

        public void FillPerformanceRatings(int areaId, int orgUnitId, bool isCompany, string startdate, string enddate,
            string startdate1, string enddate1)
        {
            try
            {
                DataTable dTY = dash.GetPerfomanceRatings(areaId, orgUnitId, isCompany, startdate, enddate);
                DataTable dTP = dash.GetPerfomanceRatings(areaId, orgUnitId, isCompany, startdate1, enddate1);
                DataTable dT = GetData(dTY, dTP);

                if (dT.Rows.Count > Util.getDashBoardGraphMinItems())
                {
                    MyRadChart.Height = new Unit(Util.getDashBoardGraphHeight());
                    MyRadChart.Width = new Unit(Util.getDashBoardGraphWidth());
                }

                DataView dV = dT.DefaultView;
                dV.Sort = "RatingValue";

                MyRadChart.Visible = dV.Count > 0 ? true : false;
                MyRadChart.DataSource = dV;
                MyRadChart.DataBind();

                WriteData(dV);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        #endregion

        #region Telerik Event Handling Methods

        protected void RadChart_BeforeLayout(object sender, EventArgs e)
        {
            for (int i = 0; i < MyRadChart.PlotArea.YAxis.Items.Count; i++)
            {
                int integer = (int)MyRadChart.PlotArea.YAxis.Items[i].Value;
                if (MyRadChart.PlotArea.YAxis.Items[i].Value - integer > 0)
                {
                    MyRadChart.PlotArea.YAxis.Items[i].TextBlock.Text = " ";
                    MyRadChart.PlotArea.YAxis.Appearance.MajorTick.Visible = false;
                }
            }
        }

        #endregion

        #region Databinding Methods

        int GetNumOfEmployees(int ratingValue, DataTable dE)
        {
            DataRow[] rw = dE.Select(string.Format("DefinedActualValue = {0}", ratingValue));
            return rw.Length;
        }

        DataTable GetData(DataTable dTY, DataTable dTP)
        {
            ArrayList rating = new ArrayList();
            DataTable dT = new DataTable();

            dT.Columns.Add("RatingValue", typeof(string));
            dT.Columns.Add("CurrentYear", typeof(int));
            dT.Columns.Add("LastYear", typeof(int));

            int ratingVal = (int)iR.GetCompanyRating(Util.user.CompanyId, true);
            for (int i = 1; i <= ratingVal; i++)
            {
                int ratingValue = i;
                int numOfEmployeesCurrent = GetNumOfEmployees(ratingValue, dTY);
                int numOfEmployeesPrev = GetNumOfEmployees(ratingValue, dTP);
                rating.Add(ratingValue);
                DataRow nRow = dT.NewRow();
                nRow["RatingValue"] = ratingValue;
                nRow["CurrentYear"] = numOfEmployeesCurrent;
                nRow["LastYear"] = numOfEmployeesPrev;
                dT.Rows.Add(nRow);
                nRow = null;
            }

            dT.AcceptChanges();
            return dT;
        }

        void WriteData(DataView dV)
        {
            try
            {
                LblData.Text = string.Empty;

                StringBuilder str1 = new StringBuilder("");
                StringBuilder str2 = new StringBuilder("");
                StringBuilder str3 = new StringBuilder("");

                foreach (DataRowView rw in dV)
                {
                    str1.Append(string.Format("<td>{0}</td>", rw["LastYear"]));
                    str2.Append(string.Format("<td>{0}</td>", rw["CurrentYear"]));
                    str3.Append(string.Format("<td>{0}</td>", rw["RatingValue"]));
                }

                string roottable = hdnTable.Value;
                StringBuilder strB = new StringBuilder(roottable);
                string mytable = string.Format("<tr><td>Number of Staff Last Year</td>{0}</tr><tr><td>Number of Staff Current Year</td>{1}</tr><tr><td>Rating</td>{2}</tr>",
                                                str1.ToString(), str2.ToString(), str3.ToString());
                strB.Append(string.Format("{0}</table>", mytable));
                LblData.Text = strB.ToString();
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        #endregion
    }
}