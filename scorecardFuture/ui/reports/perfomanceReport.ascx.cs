﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.controllers;
using scorecard.implementations;
using scorecard.entities;
using System.Data;
using RKLib.ExportData;
using HR.Human.Resources;
using System.Collections;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using RepeaterControl.Framework;
using Scorecard.Business.Rules;
using scorecard.interfaces;

namespace scorecard.ui.reports
{
    public partial class perfomanceReport : System.Web.UI.UserControl
    {
        #region Variables

        useradmincontroller usersDef;
        goaldefinition goalDef;
        scoresmanager scoresManager;
        structurecontroller orgunits;
        jobtitlesdefinitions impl;
        IBusinessRule bR;
        IScoreRating iR;

        #endregion

        #region Properties

        string StartDate
        {
            get
            {
                return Request["dsVal"].ToString();
            }
        }

        string EndDate
        {
            get
            {
                return Request["deVal"].ToString();
            }
        }

        int AreaId
        {
            get
            {
                return int.Parse(Request["aid"].ToString());
            }
        }

        string AreaName
        {
            get
            {
                return HttpUtility.HtmlDecode(Request["areaName"]);
            }
        }

        int AreaValueId
        {
            get
            {
                return int.Parse(Request["idv"].ToString());
            }
        }

        bool IsEmployee
        {
            get
            {
                return (int.Parse(Request["emp"].ToString()) == 0) ? false : true;
            }
        }

        bool IsCompany
        {
            get
            {
                return (int.Parse(Request["idv"].ToString()) == 0) ? true : false;
            }
        }

        #endregion

        #region Default Class Constructors

        public perfomanceReport()
        {
            impl = new jobtitlesdefinitions(new jobtitleImpl());
            goalDef = new goaldefinition(new goalsImpl());
            orgunits = new structurecontroller(new structureImpl());
            usersDef = new useradmincontroller(new useradminImpl());
            scoresManager = new scoresmanager(new scorecard.implementations.scoresImpl());
            bR = new BusinessRuleBL();
            iR = new ScoreRatingBL();
        }

        #endregion

        #region Page Event Handling Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                initialisePage();
                loadData();
                fillInfo();
            }
        }

        private void initialisePage()
        {
            Session["Data"] = null;
        }

        #endregion

        #region Databinding Methods

        private void fillInfo()
        {
            try
            {
                if (rptData.Items.Count > 0)
                {
                    Literal lblCriteria = RepeaterExtensionMethods.FindControlInHeader(rptData, "lblCriteria") as Literal;
                    Literal lblName = RepeaterExtensionMethods.FindControlInHeader(rptData, "lblName") as Literal;
                    Literal lblStartDate = RepeaterExtensionMethods.FindControlInHeader(rptData, "lblStartDate") as Literal;
                    Literal lblEndDate = RepeaterExtensionMethods.FindControlInHeader(rptData, "lblEndDate") as Literal;

                    lblCriteria.Text = AreaName;
                    lblName.Text = string.IsNullOrEmpty(lblName.Text) ? getName(AreaValueId, AreaId) : lblName.Text;
                    lblStartDate.Text = Common.GetDescriptiveDate(StartDate, false);
                    lblEndDate.Text = Common.GetDescriptiveDate(EndDate, false);
                }
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        string getName(int id, int criteria)
        {
            try
            {
                if (id > 0)
                {
                    if (criteria == Util.getTypeDefinitionsRolesTypeID())
                    {
                        IEmployee emp = new EmployeeBL();
                        return !IsEmployee ? impl.getJobTitleInfo(new JobTitle { ID = id }).NAME : emp.GetById(id).FullName;
                    }
                    else if (criteria == Util.getTypeDefinitionsProjectID()) return new projectmanagement(new projectImpl()).get(id).PROJECT_NAME;
                    else return (id == 0) ? "All Departments" : orgunits.getOrgunitDetails(new Orgunit { ID = id }).ORGUNIT_NAME;
                }
            }
            catch (Exception e) { Util.LogErrors(e); }
            return string.Empty;
        }

        ArrayList GetUniqueList(List<PerformanceReport> data)
        {

            ArrayList arrItem = new ArrayList();
            ArrayList arr = new ArrayList();

            foreach (PerformanceReport item in data)
            {
                int itemId = item.ItemId;
                if (!arrItem.Contains(itemId))
                {
                    arrItem.Add(itemId);
                    arr.Add(new ItemValue
                    {
                        ItemId = item.ItemId,
                        ItemName = item.ItemName
                    });
                }
            }
            return arr;
        }

        ArrayList GetPeriodTotal(List<PerformanceReport> data, int itemId)
        {
            var tmpData = data.Where(i => i.ItemId == itemId);
            ArrayList arr = new ArrayList();

            foreach (PerformanceReport item in data)
            {
                if (!arr.Contains(item.PERIOD_DATE))
                    arr.Add(item.PERIOD_DATE);
            }
            return arr;
        }

        DataTable getAllDepartmentData()
        {
            DataTable dT = GetDataSchema();
            try
            {
                int valueId = (!IsCompany) ? AreaValueId : Util.user.OrgId;

                var dataS = scoresManager.getDepartmentScorePerformanceReport(AreaId, valueId, StartDate, EndDate, IsCompany);
                var data = dataS.Where(p => p.PARENT_ID == null).ToList();
                var empList = GetUniqueList(data);

                double totalTarget = 100;
                foreach (ItemValue item in empList)
                {
                    int itemId = item.ItemId;
                    string name = item.ItemName;

                    ItemValue obj = new ItemValue
                    {
                        ItemId = itemId,
                        ItemName = name,
                    };

                    var prd = GetPeriodTotal(data, itemId);
                    var periodTime = prd.Count;

                    double? totalActual = 0;
                    decimal definedActual = 0;
                    foreach (DateTime date in prd)
                    {
                        var finalDefinedScore = data.Where(c => c.ItemId == itemId && c.PERIOD_DATE == date).Sum(u => u.FINAL_SCORE_AGREED);
                        var finalScore = data.Where(c => c.ItemId == itemId && c.PERIOD_DATE == date).Sum(u => u.FINAL_SCORE);
                        var finalWeightTotal = data.Where(c => c.ItemId == itemId && c.PERIOD_DATE == date).Sum(u => u.WEIGHT);

                        if (finalWeightTotal == 0) periodTime = periodTime - 1;
                        else
                        {
                            var finalPercentage = (finalWeightTotal != 0) ? (finalScore / finalWeightTotal) * 100 : 0;
                            definedActual += (decimal)finalDefinedScore;
                            totalActual += finalPercentage;
                        }
                    }

                    if (periodTime > 0)
                    {
                        double prdTime = double.Parse(periodTime.ToString());
                        double averageScore = Math.Round((double)totalActual / prdTime, 2);
                        decimal averageDefinedScore = Math.Round(definedActual / (decimal)prdTime, 2);

                        DataRow nRow = dT.NewRow();
                        nRow["ItemName"] = item.ItemName;
                        nRow["TargetValue"] = totalTarget;
                        nRow["ActualValue"] = averageScore;
                        nRow["DefinedActualValue"] = averageDefinedScore;
                        dT.Rows.Add(nRow);
                        nRow = null;
                        obj = null;
                    }
                }

                dT.AcceptChanges();
                Session["Data"] = dT;
                return dT;
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return dT;
            }
        }

        DataTable getEmployeeData()
        {
            DataTable dT = GetDataSchema();
            try
            {
                var dataS = scoresManager.getEmployeeScorePerformanceReport(AreaId, AreaValueId, StartDate, EndDate);
                var data = dataS.Where(p => p.PARENT_ID == null).ToList();
                var empList = GetUniqueList(data);

                double totalTarget = 100;
                foreach (ItemValue item in empList)
                {
                    int itemId = item.ItemId;
                    string name = item.ItemName;

                    ItemValue obj = new ItemValue
                    {
                        ItemId = itemId,
                        ItemName = name,
                    };

                    var prd = GetPeriodTotal(data, itemId);
                    var periodTime = prd.Count;

                    double? totalActual = 0;
                    decimal totalDefinedActual = 0;
                    foreach (DateTime date in prd)
                    {
                        var finalDefinedScore = data.Where(c => c.ItemId == itemId && c.PERIOD_DATE == date).Sum(u => u.FINAL_SCORE_AGREED);
                        var finalScore = data.Where(c => c.ItemId == itemId && c.PERIOD_DATE == date).Sum(u => u.FINAL_SCORE);
                        var finalWeightTotal = data.Where(c => c.ItemId == itemId && c.PERIOD_DATE == date).Sum(u => u.WEIGHT);

                        if (finalWeightTotal == 0) periodTime = periodTime - 1;
                        else
                        {
                            var finalPercentage = (finalWeightTotal != 0) ? (finalScore / finalWeightTotal) * 100 : 0;
                            totalActual += finalPercentage;
                            totalDefinedActual += (decimal)finalDefinedScore;
                        }
                    }

                    if (periodTime > 0)
                    {
                        double prdTime = double.Parse(periodTime.ToString());
                        double averageScore = Math.Round((double)totalActual / prdTime, 2);
                        decimal averageDefinedScore = Math.Round(totalDefinedActual / (decimal)prdTime, 2);

                        DataRow nRow = dT.NewRow();
                        nRow["ItemName"] = item.ItemName;
                        nRow["TargetValue"] = totalTarget;
                        nRow["ActualValue"] = averageScore;
                        nRow["DefinedActualValue"] = averageDefinedScore;

                        dT.Rows.Add(nRow);
                        nRow = null;
                        obj = null;
                    }
                }

                dT.AcceptChanges();
                Session["Data"] = dT;
                return dT;
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return dT;
            }
        }

        DataTable getJotitleData()
        {
            DataTable dT = GetDataSchema();
            try
            {
                var tempData = scoresManager.getJobTitleScorePerformanceReport(AreaId, AreaValueId, StartDate, EndDate);
                var data = tempData.Where(p => p.PARENT_ID == null).ToList();
                var empList = GetUniqueList(data);

                double totalTarget = 100;
                foreach (ItemValue item in empList)
                {
                    int itemId = item.ItemId;
                    string name = item.ItemName;

                    ItemValue obj = new ItemValue
                    {
                        ItemId = itemId,
                        ItemName = name,
                    };

                    var prd = GetPeriodTotal(data, itemId);
                    var periodTime = prd.Count;

                    double? totalActual = 0;
                    decimal definedWeightedActual = 0;
                    foreach (DateTime date in prd)
                    {
                        var finalScore = data.Where(c => c.ItemId == itemId && c.PERIOD_DATE == date).Sum(u => u.FINAL_SCORE);
                        var finalWeightTotal = data.Where(c => c.ItemId == itemId && c.PERIOD_DATE == date).Sum(u => u.WEIGHT);
                        var definedWeightTotal = data.Where(c => c.ItemId == itemId && c.PERIOD_DATE == date).Sum(u => u.FINAL_SCORE_AGREED);

                        if (finalWeightTotal == 0) periodTime = periodTime - 1;
                        else
                        {
                            var finalPercentage = (finalWeightTotal != 0) ? (finalScore / finalWeightTotal) * 100 : 0;
                            totalActual += finalPercentage;
                            definedWeightedActual += (decimal)definedWeightTotal;
                        }
                    }

                    if (periodTime > 0)
                    {
                        double prdTime = double.Parse(periodTime.ToString());
                        double averageScore = Math.Round((double)totalActual / prdTime, 2);
                        decimal averageDefined = Math.Round(definedWeightedActual / (decimal)prdTime, 2);

                        DataRow nRow = dT.NewRow();
                        nRow["ItemName"] = item.ItemName;
                        nRow["TargetValue"] = totalTarget;
                        nRow["ActualValue"] = averageScore;
                        nRow["DefinedActualValue"] = averageDefined;

                        dT.Rows.Add(nRow);
                        nRow = null;
                        obj = null;
                    }
                }

                dT.AcceptChanges();
                Session["Data"] = dT;
                return dT;
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return dT;
            }
        }

        DataTable getEmployeesInSameDepartment()
        {
            DataTable dT = GetDataSchema();
            try
            {
                int areaValueId = AreaId == 0 ? Util.getTypeDefinitionsRolesTypeID() : AreaId;
                var dataS = scoresManager.getDepartmentUsersScorePerformanceReport(areaValueId, AreaValueId, StartDate, EndDate);
                var data = dataS.Where(p => p.PARENT_ID == null).ToList();
                var empList = GetUniqueList(data);

                double totalTarget = 100;
                foreach (ItemValue item in empList)
                {
                    int itemId = item.ItemId;
                    string name = item.ItemName;

                    ItemValue obj = new ItemValue
                    {
                        ItemId = itemId,
                        ItemName = name,
                    };

                    var prd = GetPeriodTotal(data, itemId);
                    var periodTime = prd.Count;

                    double? totalActual = 0;
                    decimal definedActual = 0;
                    foreach (DateTime date in prd)
                    {
                        decimal? finalDefinedScore = data.Where(c => c.ItemId == itemId && c.PERIOD_DATE == date).Sum(u => u.FINAL_SCORE_AGREED);
                        double? finalScore = data.Where(c => c.ItemId == itemId && c.PERIOD_DATE == date).Sum(u => u.FINAL_SCORE);
                        int finalWeightTotal = data.Where(c => c.ItemId == itemId && c.PERIOD_DATE == date ).Sum(u => u.WEIGHT);

                        if (finalWeightTotal == 0) periodTime = periodTime - 1;
                        else
                        {
                            var finalPercentage = (finalWeightTotal != 0) ? (finalScore / finalWeightTotal) * 100 : 0;
                            totalActual += finalPercentage;
                            definedActual += (decimal)finalDefinedScore;
                        }
                    }

                    if (periodTime > 0)
                    {
                        double? tactual = null;
                        double prdTime = double.Parse(periodTime.ToString());

                        if (totalActual != null)
                            tactual = Math.Round((double)totalActual / prdTime, 2);

                        var averageScore = tactual;
                        var averageDefinedScore = Math.Round(definedActual / (decimal)prdTime, 2);

                        DataRow nRow = dT.NewRow();
                        nRow["ItemName"] = item.ItemName;
                        nRow["TargetValue"] = totalTarget;
                        nRow["ActualValue"] = averageScore;
                        nRow["DefinedActualValue"] = averageDefinedScore;
                        dT.Rows.Add(nRow);
                        nRow = null;
                        obj = null;
                    }
                }

                dT.AcceptChanges();
                Session["Data"] = dT;
                return dT;
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return dT;
            }
        }

        void loadData()
        {
            try
            {
                bool isAllDepartment = false;
                DataTable dT = GetDataSchema();

                if (AreaId == Util.getTypeDefinitionsRolesTypeID()) dT = !IsEmployee ? getJotitleData() : getEmployeeData();
                else if (AreaId == Util.getTypeDefinitionsOrgUnitsTypeID())
                {
                    dT = getAllDepartmentData();
                    if (IsCompany) isAllDepartment = true;
                }
                else dT = getEmployeesInSameDepartment();

                if (dT.Rows.Count <= 0) ShowMessage(Messages.GetRecordNotFoundMessage());
                else
                {
                    BindControl.BindRepeater(rptData, dT);
                    if (isAllDepartment)
                    {
                        Literal lblName = RepeaterExtensionMethods.FindControlInHeader(rptData, "lblName") as Literal;
                        lblName.Text = "All Departments";
                    }
                }
            }
            catch (Exception e)
            {
                ShowMessage(Messages.GetErrorMessage());
                Util.LogErrors(e);
            }
        }

        void ShowMessage(string msg)
        {
            BindControl.BindLiteral(lblError, msg);
        }

        #endregion

        #region Button Event Handling Methods

        protected void lnkExcel_Click(object sender, EventArgs e)
        {
            string caption = (AreaId == Util.getOrganisationalTypeGroupId()) ? "Department" : "Name";
            Download(caption);
        }

        DataTable GetDataSchema()
        {
            DataTable dT = new DataTable();
            dT.Columns.Add("ItemName", typeof(string));
            dT.Columns.Add("ActualValue", typeof(string));
            dT.Columns.Add("TargetValue", typeof(string));
            dT.Columns.Add("DefinedActualValue", typeof(string));
            dT.AcceptChanges();
            return dT;
        }

        private void Download(string caption)
        {
            if (rptData.Items.Count <= 0)
            {
                lblError.Text = "There are no records to export to excel.";
                return;
            }

            Literal lblCriteria = RepeaterExtensionMethods.FindControlInHeader(rptData, "lblCriteria") as Literal;
            Literal lblName = RepeaterExtensionMethods.FindControlInHeader(rptData, "lblName") as Literal;
            Literal lblStartDate = RepeaterExtensionMethods.FindControlInHeader(rptData, "lblStartDate") as Literal;
            Literal lblEndDate = RepeaterExtensionMethods.FindControlInHeader(rptData, "lblEndDate") as Literal;

            DataTable dT = GetDataSchema();
            int[] cols = { 0, 1, 2, 3 };
            string[] headers = { "", "", "", "" };

            DataRow nRow = dT.NewRow();
            nRow["ItemName"] = "Report Name".ToUpper();
            nRow["ActualValue"] = lblCriteria.Text;
            dT.Rows.Add(nRow);
            dT.Rows.Add(dT.NewRow());//add blank row
            nRow = null;

            nRow = dT.NewRow();
            nRow["ItemName"] = caption.ToUpper();
            nRow["ActualValue"] = lblName.Text;
            dT.Rows.Add(nRow);
            dT.Rows.Add(dT.NewRow());//add blank row
            nRow = null;

            nRow = dT.NewRow();
            nRow["ItemName"] = "Start Date".ToUpper();
            nRow["ActualValue"] = lblStartDate.Text;
            dT.Rows.Add(nRow);
            dT.Rows.Add(dT.NewRow());//add blank row
            nRow = null;

            nRow = dT.NewRow();
            nRow["ItemName"] = "End Date".ToUpper();
            nRow["ActualValue"] = lblEndDate.Text;
            dT.Rows.Add(nRow);
            dT.Rows.Add(dT.NewRow());//add blank row
            nRow = null;

            nRow = dT.NewRow();
            nRow["ItemName"] = caption;
            nRow["ActualValue"] = "Average Perfomance Score";
            nRow["TargetValue"] = "Target";
            nRow["DefinedActualValue"] = "Average Defined Weighted Score";
            dT.Rows.Add(nRow);
            nRow = null;

            DataTable dS = Session["Data"] as DataTable;
            foreach (DataRow item in dS.Rows)
            {
                string name = item["ItemName"].ToString();
                string actualValue = item["ActualValue"].ToString();
                string target = item["TargetValue"].ToString();
                string definedRating = item["DefinedActualValue"].ToString();

                nRow = dT.NewRow();
                nRow["ItemName"] = name;
                nRow["ActualValue"] = actualValue;
                nRow["TargetValue"] = target;
                nRow["DefinedActualValue"] = definedRating;
                dT.Rows.Add(nRow);
                nRow = null;
            }

            dT.AcceptChanges();
            Export exp = new Export("Web");
            exp.ExportDetails(dT, cols, headers, Export.ExportFormat.Excel, Export.ExportAction.DownloadOnly, string.Format("{0}.xls", lblName.Text));
        }

        protected void lnkPdf_Click(object sender, EventArgs e)
        {
            if (rptData.Items.Count > 0)
            {
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment;filename=Department.pdf");
                Response.Cache.SetCacheability(HttpCacheability.NoCache);

                StringWriter sw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                rptData.RenderControl(hw);

                StringReader sr = new StringReader(sw.ToString());
                Document pdfDoc = new Document(PageSize.A4, 20f, 10f, 10f, 10f);
                HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                PdfWriter.GetInstance(pdfDoc, Response.OutputStream);

                pdfDoc.Open();

                htmlparser.Parse(sr);
                pdfDoc.Close();
                Response.Write(pdfDoc);
                Response.End();
            }
            else ShowMessage(Messages.GetRecordNotFoundMessage());
        }

        #endregion
    }
}