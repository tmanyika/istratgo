﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="scoreReportViewException.ascx.cs"
    Inherits="scorecard.ui.reports.scoreReportViewException" %>
<div style="background-color: White;">
    <table cellpadding="0" cellspacing="10" border="0" width="90%">
        <tr>
            <td style="width: 250px;">
                <h2>
                    Department</h2>
            </td>
            <td style="width: 250px;">
                <h2>
                    <span style="color: Black">
                        <asp:Literal ID="lblCriteria" runat="server"></asp:Literal></span></h2>
            </td>
            <td align="right">
                <span style="color: Red">
                    <asp:Literal ID="lblError" runat="server"></asp:Literal></span>
                <asp:LinkButton ID="lnkPrint" runat="server" CausesValidation="False" OnClientClick="window.print();">
                    <img runat="server" src="~/ui/assets/icons/print.png" id="imgPrint" alt="Click to Print" /></asp:LinkButton>&nbsp;&nbsp;<asp:LinkButton
                        ID="lnkExcel" runat="server" CausesValidation="False" OnClick="lnkExcel_Click">
                        <img src="~/ui/assets/icons/Excel.png" runat="server" id="imgExcel" alt="Export to Excel" /></asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td>
                <h2>
                    Report Date</h2>
            </td>
            <td colspan="2">
                <h2>
                    <span style="color: Black">
                        <asp:Literal ID="lblDate" runat="server"></asp:Literal>
                    </span>
                </h2>
            </td>
        </tr>
    </table>
</div>
<div>
    <asp:ListView ID="lstData" runat="server">
        <LayoutTemplate>
            <table class="fullwidth" cellpadding="0" cellspacing="0" border="0" width="90%">
                <thead>
                    <tr>
                        <th align="left">
                            Full Name
                        </th>
                        <th align="right">
                            Final Score (%)
                        </th>
                        <th align="right">
                            Defined Weighted Score
                        </th>
                        <th align="left">
                            Status
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr id="ItemPlaceHolder" runat="server">
                    </tr>
                </tbody>
            </table>
        </LayoutTemplate>
        <ItemTemplate>
            <tr class="odd">
                <td align="left">
                    <asp:Literal ID="lblFullName" runat="server" Text='<%#  Eval("FullName") %>'></asp:Literal>
                </td>
                <td align="right">
                    <asp:Literal ID="lblFinalScore" runat="server" Text='<%#  Eval("FinalScore") %>' />
                </td>
                <td align="right">
                    <asp:Literal ID="lblFinalRating" runat="server" Text='<%#  Eval("FinalRating") %>' />
                </td>
                <td align="left">
                    <asp:Literal ID="lblStatus" runat="server" Text='<%# Eval("Status")%>' />
                </td>
            </tr>
        </ItemTemplate>
        <EmptyDataTemplate>
            There are no records to display for the criteria you specified.
        </EmptyDataTemplate>
    </asp:ListView>
</div>
