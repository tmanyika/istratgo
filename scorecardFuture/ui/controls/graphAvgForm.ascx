﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="graphAvgForm.ascx.cs"
    Inherits="scorecard.ui.controls.graphAvgForm" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:UpdatePanel ID="UpdatePanelScoreReport" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div>
            <h1>
                &nbsp; Assessment Bar Graph Report
            </h1>
        </div>
        <div>
            <fieldset runat="server" id="pnlContactInfo">
                <legend>Assessment Criteria</legend>
                <p>
                    <label for="sf">
                        Start Date:
                    </label>
                    <span class="field_desc">&nbsp;<asp:TextBox runat="server" ID="txtStartDate" Text=""
                        ValidationGroup="Form" />
                        <asp:CalendarExtender ID="txtStartDate_CalendarExtender" runat="server" Enabled="True"
                            CssClass="calendar" Format="MM/dd/yyyy" TargetControlID="txtStartDate">
                        </asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="rqdStartDate" runat="server" ControlToValidate="txtStartDate"
                            Display="Dynamic" ErrorMessage="start date is required" ValidationGroup="Form"
                            ForeColor="Red"></asp:RequiredFieldValidator>
                    </span>
                </p>
                <p>
                    <label for="sf">
                        End Date:
                    </label>
                    <span class="field_desc">&nbsp;<asp:TextBox runat="server" ID="txtEndDate" Text=""
                        ValidationGroup="Form" />
                        <asp:CalendarExtender ID="txtEndDate_CalendarExtender" runat="server" Enabled="True"
                            CssClass="calendar" Format="MM/dd/yyyy" TargetControlID="txtEndDate">
                        </asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="rqdEndDate" runat="server" ControlToValidate="txtEndDate"
                            Display="Dynamic" ErrorMessage="end date is required" ValidationGroup="Form"
                            ForeColor="Red"></asp:RequiredFieldValidator>
                    </span>
                </p>
                <p>
                    <label for="sf">
                        Select Area / Report:
                    </label>
                    <span class="field_desc">&nbsp;<asp:DropDownList runat="server" ID="ddlArea" AutoPostBack="True"
                        OnSelectedIndexChanged="ddlArea_SelectedIndexChanged" ValidationGroup="Form" />
                        <asp:RequiredFieldValidator ID="rqdArea" runat="server" ControlToValidate="ddlArea"
                            Display="Dynamic" ErrorMessage="area is required" InitialValue="-1" ValidationGroup="Form"
                            ForeColor="Red"></asp:RequiredFieldValidator>
                    </span>
                </p>
                <p>
                    <label for="sf">
                        Select Value:
                    </label>
                    <span class="field_desc">&nbsp;<asp:DropDownList runat="server" ID="ddlValueTypes"
                        ValidationGroup="Form" />
                        <asp:RequiredFieldValidator ID="rqdVal" runat="server" ControlToValidate="ddlValueTypes"
                            Display="Dynamic" ErrorMessage="value is required" InitialValue="-1" ValidationGroup="Form"
                            ForeColor="Red"></asp:RequiredFieldValidator>
                    </span>
                </p>
                <p class="errorMsg">
                    <label for="lblMsg">
                    </label>
                    <asp:Literal ID="lblMsg" runat="server"></asp:Literal>
                </p>
                <p>
                    <label>
                        &nbsp;
                    </label>
                    <asp:Button ID="btnViewReport" runat="server" CssClass="button tooltip" Text="View Report"
                        CausesValidation="true" ValidationGroup="Form" OnClick="btnViewReport_Click" />
                </p>
            </fieldset>
        </div>
        <asp:UpdateProgress ID="UpdateProgressScoreReport" runat="server" AssociatedUpdatePanelID="UpdatePanelScoreReport">
            <ProgressTemplate>
                <img runat="server" src="~/ui/images/icons/activity.gif" alt="Please wait..." id="imgPrg" />
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
