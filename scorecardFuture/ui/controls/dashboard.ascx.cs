﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.implementations;
using scorecard.controllers;
using scorecard.entities;
using HR.Human.Resources;

namespace scorecard.ui.reports
{
    public partial class dashboard : System.Web.UI.UserControl
    {
        #region Properties

        bool IsAdmin
        {
            get { return (Util.user.UserTypeId != Util.getTypeDefinitionsUserTypeAdministrator()) ? false : true; }
        }

        #endregion

        #region  Variables

        useradmincontroller usersDef;
        goaldefinition goalDef;
        scoresmanager scoresManager;

        #endregion

        #region Delegates

        public delegate void FillObjectives(int areaId, int orgUnitId, bool isCompany, string startDate, string endDate);
        public FillObjectives FillObjectivesHandler;
        public event FillObjectives EventFillObjectives
        {
            add { FillObjectivesHandler += value; }
            remove { FillObjectivesHandler -= value; }
        }

        public delegate void FillPerspectives(int areaId, int orgUnitId, bool isCompany, string startDate, string endDate);
        public FillPerspectives FillPerspectivesHandler;
        public event FillPerspectives EventFillPerspectives
        {
            add { FillPerspectivesHandler += value; }
            remove { FillPerspectivesHandler -= value; }
        }

        public delegate void FillFocusAreas(int areaId, int orgUnitId, bool isCompany);
        public FillFocusAreas FillFocusAreasHandler;
        public event FillFocusAreas EventFillFocusAreas
        {
            add { FillFocusAreasHandler += value; }
            remove { FillFocusAreasHandler -= value; }
        }

        public delegate void FillPerformanceRatings(int areaId, int orgUnitId, bool isCompany, string startdate, string enddate,
                                                    string startdate1, string enddate1);
        public FillPerformanceRatings FillPerformanceRatingsHandler;
        public event FillPerformanceRatings EventFillPerformanceRatingsHandler
        {
            add { FillPerformanceRatingsHandler += value; }
            remove { FillPerformanceRatingsHandler -= value; }
        }

        public delegate void FillTalentMatrix(int areaId, int orgUnitId, bool isCompany, string startDate, string endDate);
        public FillTalentMatrix FillTalentMatrixHandler;
        public event FillTalentMatrix EventFillTalentMatrixHandler
        {
            add { FillTalentMatrixHandler += value; }
            remove { FillTalentMatrixHandler -= value; }
        }

        void HideControls(bool show)
        {
            dashStrategicObjectives1.Visible = show;
            dashPerspectivePerformance1.Visible = show;
            dashFocusAreaWeighting1.Visible = show;
            dashPerformanceRatingVsNumStaff1.Visible = show;
        }

        void RegisterEvents()
        {
            this.EventFillObjectives += new FillObjectives(dashStrategicObjectives1.FillObjectives);
            this.EventFillPerspectives += new FillPerspectives(dashPerspectivePerformance1.FillPerspectives);
            this.EventFillFocusAreas += new FillFocusAreas(dashFocusAreaWeighting1.FillFocusAreas);
            this.EventFillPerformanceRatingsHandler += new FillPerformanceRatings(dashPerformanceRatingVsNumStaff1.FillPerformanceRatings);
            this.EventFillTalentMatrixHandler += new FillTalentMatrix(dashTalentMatrix1.FillTalentMatrix);
        }

        #endregion

        #region Default Class Constructors

        public dashboard()
        {
            usersDef = new useradmincontroller(new useradminImpl());
            goalDef = new goaldefinition(new goalsImpl());
            scoresManager = new scoresmanager(new scorecard.implementations.scoresImpl());
        }

        #endregion

        #region Page Events Handling Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterEvents();
            if (!IsPostBack)
            {
                loadOrgStruct();
                InitialisePage();
            }
        }

        void InitialisePage()
        {
            bool isCompany = false; //(ddOrgUnit.SelectedValue.Equals("0")) ? true : false;

            int orgUnitId = int.Parse(ddOrgUnit.SelectedValue);// !isCompany ? int.Parse(ddOrgUnit.SelectedValue) : Util.user.CompanyId;
            int year = DateTime.Now.Year;
            int areaId = Util.getTypeDefinitionsRolesTypeID();

            DateTime currDate = DateTime.Now;
            DateTime prevDate = new DateTime(currDate.Year, 1, 1);

            txtStartDate.Text = string.Format("{0:MM/dd/yyyy}", prevDate);
            txtEndDate.Text = string.Format("{0:MM/dd/yyyy}", currDate);

            string sdate1 = string.Format("{0:MM/dd/yyyy}", new DateTime(prevDate.Year - 1, 1, 1));
            string edate1 = string.Format("{0:MM/dd/yyyy}", new DateTime(currDate.Year - 1, currDate.Month, currDate.Day));

            string startDate = Common.GetInternationalDateFormat(txtStartDate.Text.Trim(), DateFormat.monthDayYear);
            string endDate = Common.GetInternationalDateFormat(txtEndDate.Text.Trim(), DateFormat.monthDayYear);

            string startDate1 = Common.GetInternationalDateFormat(sdate1, DateFormat.monthDayYear);
            string endDate1 = Common.GetInternationalDateFormat(edate1, DateFormat.monthDayYear);

            HideControls(true);
            LoadControlsData(areaId, orgUnitId, year, isCompany, startDate, endDate, startDate1, endDate1);
        }

        #endregion

        #region Databinding Methods

        void loadOrgStruct()
        {
            try
            {
                var userRole = Util.user.UserTypeId;
                var userID = Util.user.UserId;
                var orgUnitId = Util.user.OrgId.ToString();

                var structure = goalDef.orgstructure.getCompanyStructureListView(new company { COMPANY_ID = Util.user.CompanyId });
                //if (!IsAdmin) structure = structure.Where(t => t.OWNER_ID == userID).ToList();
                //else
                //{
                //    var defOrgUnit = structure.Where(p => p.PARENT_ORG == 0).Single();
                //    orgUnitId = defOrgUnit.PARENT_ORG.ToString();
                //}
                BindControl.BindDropdown(ddOrgUnit, "ORGUNIT_NAME", "ID", structure.ToList());
                ddOrgUnit.SelectedValue = orgUnitId.ToString();
                //ddOrgUnit.Enabled = IsAdmin;
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void ShowMessage(string msg)
        {
            BindControl.BindLiteral(lblMsg, msg);
        }

        #endregion

        #region Button Event Handling Methods

        protected void btnViewReport_Click(object sender, EventArgs e)
        {
            //if (ddOrgUnit.SelectedIndex == 0 &&
            //    Util.user.UserTypeId != Util.getTypeDefinitionsUserTypeAdministrator())
            //{
            //    ShowMessage("You do not have permission to view at company level.");
            //    return;
            //}

            if (string.IsNullOrEmpty(txtEndDate.Text) ||
                string.IsNullOrEmpty(txtStartDate.Text))
            {
                ShowMessage("Start and End Date are required.");
                return;
            }

            int year = DateTime.Now.Year;
            int areaId = Util.getTypeDefinitionsRolesTypeID();
            int orgUnitId = int.Parse(ddOrgUnit.SelectedValue);

            bool isCompany = (orgUnitId > 0);

            string startDate = Common.GetInternationalDateFormat(txtStartDate.Text.Trim(), DateFormat.monthDayYear);
            string endDate = Common.GetInternationalDateFormat(txtEndDate.Text.Trim(), DateFormat.monthDayYear);

            if (DateTime.Parse(startDate) > DateTime.Parse(endDate))
            {
                ShowMessage("Start Date cannot be greater than End Date.");
                return;
            }

            DateTime prevDate = DateTime.Parse(startDate);
            DateTime currDate = DateTime.Parse(endDate);

            string sdate1 = string.Format("{0:MM/dd/yyyy}", new DateTime(prevDate.Year - 1, prevDate.Month, prevDate.Day));
            string edate1 = string.Format("{0:MM/dd/yyyy}", new DateTime(currDate.Year - 1, currDate.Month, currDate.Day));

            string startDate1 = Common.GetInternationalDateFormat(sdate1, DateFormat.monthDayYear);
            string endDate1 = Common.GetInternationalDateFormat(edate1, DateFormat.monthDayYear);

            HideControls(true);
            LoadControlsData(areaId, orgUnitId, year, isCompany, startDate, endDate, startDate1, endDate1);
        }

        void LoadControlsData(int areaId, int orgUnitId, int year, bool isCompany, string startDate, string endDate, string startDate1, string endDate1)
        {
            if (FillPerformanceRatingsHandler != null)
                FillPerformanceRatingsHandler(areaId, orgUnitId, isCompany, startDate, endDate, startDate1, endDate1);

            if (FillFocusAreasHandler != null)
                FillFocusAreasHandler(Util.getTypeDefinitionsOrgUnitsTypeID(), orgUnitId, isCompany);

            if (FillObjectivesHandler != null)
                FillObjectivesHandler(Util.getTypeDefinitionsOrgUnitsTypeID(), orgUnitId, isCompany, startDate, endDate);

            if (FillPerspectivesHandler != null)
                FillPerspectivesHandler(Util.getTypeDefinitionsOrgUnitsTypeID(), orgUnitId, isCompany, startDate, endDate);

            if (FillTalentMatrixHandler != null)
                FillTalentMatrixHandler(areaId, orgUnitId, isCompany, startDate, endDate);
        }

        #endregion
    }
}