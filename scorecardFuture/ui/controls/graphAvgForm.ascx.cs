﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.implementations;
using scorecard.controllers;
using scorecard.entities;
using HR.Human.Resources;

namespace scorecard.ui.controls
{
    public partial class graphAvgForm : System.Web.UI.UserControl
    {
        #region  Variables

        useradmincontroller usersDef;
        goaldefinition goalDef;
        scoresmanager scoresManager;
        jobtitlesdefinitions impl;

        #endregion

        #region Default Class Constructors

        public graphAvgForm()
        {
            impl = new jobtitlesdefinitions(new jobtitleImpl());
            usersDef = new useradmincontroller(new useradminImpl());
            goalDef = new goaldefinition(new goalsImpl());
            scoresManager = new scoresmanager(new scorecard.implementations.scoresImpl());
        }

        #endregion

        #region Page Events Handling Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                LoadAreas();
        }

        #endregion

        #region Databinding Methods

        void LoadValues()
        {
            try
            {
                int areaId = int.Parse(ddlArea.SelectedValue);
                if (areaId == Util.getTypeDefinitionsRolesTypeID()) loadJobTitles();
                else if (areaId == Util.getTypeDefinitionsProjectID()) loadProjects(Util.user.CompanyId);
                else loadOrgStruct();
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void LoadAreas()
        {
            var areaList = goalDef.appsettings.getChildSettings(new applicationsettings { PARENT_ID = Util.getTypeDefinitionsGoalAreaTypes() }).ToList();
            BindControl.BindDropdown(ddlArea, "DESCRIPTION", "TYPE_ID", "-- select area --", "0", areaList.ToList());
        }

        void ShowMessage(string msg)
        {
            BindControl.BindLiteral(lblMsg, msg);
        }

        void loadOrgStruct()
        {
            try
            {
                var userRole = Util.user.UserTypeId;
                var userID = Util.user.UserId;
                var structure = goalDef.orgstructure.getCompanyStructureListView(new company { COMPANY_ID = Util.user.CompanyId });
                //if (userRole != Util.getTypeDefinitionsUserTypeAdministrator())
                //    structure = structure.Where(t => t.OWNER_ID == userID).ToList();
                rqdVal.ErrorMessage = "select organisation unit";
                BindControl.BindDropdown(ddlValueTypes, "ORGUNIT_NAME", "ID", "- select departments/ organisational units -", "0", structure);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void loadProjects(int companyId)
        {
            projectmanagement proj = new projectmanagement(new projectImpl());

            int adminRoleId = Util.getTypeDefinitionsUserTypeAdministrator();
            int roleId = (int)Util.user.UserTypeId;
            IEnumerable<project> projL = proj.get(companyId, true);
            if (adminRoleId != Util.user.UserTypeId) projL = projL.Where(c => c.RESPONSIBLE_PERSON_ID == Util.user.UserId);
            rqdVal.ErrorMessage = "select a project";
            BindControl.BindDropdown(ddlValueTypes, "PROJECT_NAME", "PROJECT_ID", "-- select project --", "0", projL);
        }

        void loadJobTitles()
        {
            try
            {
                IEmployee obj = new EmployeeBL();
                List<Employee> titles = obj.GetByOrgStructure(Util.user.OrgId, true);
                if (Util.user.UserTypeId != Util.getTypeDefinitionsUserTypeAdministrator())
                    titles = titles.Where(t => t.LineManagerEmployeeId == Util.user.UserId || t.EmployeeId == Util.user.UserId).ToList();
                BindControl.BindDropdown(ddlValueTypes, "FullName", "EmployeeId", "-- select a user --", "0", titles);
                rqdVal.ErrorMessage = "select a user";
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void downloadReport()
        {
            if (string.IsNullOrEmpty(txtStartDate.Text) ||
                string.IsNullOrEmpty(txtEndDate.Text) ||
                ddlArea.SelectedIndex == 0 ||
                (ddlArea.SelectedValue.Equals("0") && ddlValueTypes.SelectedIndex == 0))
            {

                ShowMessage(Messages.GetRequiredFieldsMessage(true, string.IsNullOrEmpty(txtStartDate.Text) ? "Start Date" : string.Empty,
                    string.IsNullOrEmpty(txtEndDate.Text) ? "End Date" : string.Empty, ddlArea.SelectedIndex == 0 ? "Select Area / Report" : string.Empty,
                    (ddlArea.SelectedValue.Equals("0") && ddlValueTypes.SelectedIndex == 0) ? "Select Value" : string.Empty));
                return;
            }

            string startDate = Common.GetInternationalDateFormat(txtStartDate.Text.Trim(), DateFormat.monthDayYear);
            string endDate = Common.GetInternationalDateFormat(txtEndDate.Text.Trim(), DateFormat.monthDayYear);

            if (DateTime.Parse(startDate) > DateTime.Parse(endDate))
            {
                ShowMessage("Start Date cannot be greater than End Date.");
                return;
            }
            else if (DateTime.Parse(startDate).AddMonths(12) < DateTime.Parse(endDate))
            {
                ShowMessage("Date range cannot be greater than 12 months.");
                return;
            }

            string areaId = ddlArea.SelectedValue;
            string areaName = ddlArea.SelectedItem.Text;
            string areaValue = ddlValueTypes.SelectedValue;
            string script = string.Format("openNewWindow('assessment.aspx?aid={0}&idv={1}&dsVal={2}&deVal={3}&emp=0&areaName={4}');", areaId, areaValue, startDate, endDate, HttpUtility.HtmlEncode(areaName));
            AjaxScriptManager.RegisterStartupScript(Page, typeof(Page), "viewReport", script, true);
        }

        #endregion

        #region Dropdown Event Handling Methods

        protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlValueTypes.Items.Clear();
            if (ddlArea.SelectedIndex != 0)
                LoadValues();
        }

        #endregion

        protected void btnViewReport_Click(object sender, EventArgs e)
        {
            lblMsg.Text = "";
            downloadReport();
        }

    }
}