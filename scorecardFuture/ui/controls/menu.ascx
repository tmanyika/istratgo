﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="menu.ascx.cs" Inherits="scorecard.ui.controls.menu" %>
<h1>
    Welcome, <span>
        <asp:Label ID="lblWelcomeMessage" runat="server"></asp:Label>
    </span>!</h1>
<div class="pad20">
    <!-- Big buttons -->
    <asp:UpdatePanel ID="UpdatePanelWelcome" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:UpdateProgress ID="UpdateProgressWelcome" runat="server" AssociatedUpdatePanelID="UpdatePanelWelcome">
                <ProgressTemplate>
                    <div>
                        <asp:Image ID="imgProj" runat="server" ImageUrl="~/ui/images/icons/activity.gif"
                            AlternateText="Please wait.."></asp:Image></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:MultiView ID="MainView" runat="server">
                <asp:View ID="vwNotAllowed" runat="server">
                    <asp:PlaceHolder ID="plcMenuAccess" runat="server" />
                </asp:View>
                <asp:View ID="vwWelcome" runat="server">
                    <asp:Repeater ID="rptMenu" runat="server">
                        <HeaderTemplate>
                            <ul class="dash">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <li><a href='<%# Eval("MENU_PATH") %>' title='<%# Eval("MENU_NAME") %>' class='<%# Eval("MENU_NAME") %>'>
                                <img src='assets/icons/<%# Eval("IMAGE_ICON") %>' alt='<%# Eval("MENU_NAME") %>' /><span>
                                    <asp:Literal ID="lblCaption" runat="server" Text='<%# Eval("MENU_NAME") %>'></asp:Literal></span></a>
                                </a> </li>
                        </ItemTemplate>
                        <FooterTemplate>
                            </ul></FooterTemplate>
                    </asp:Repeater>
                </asp:View>
                <asp:View ID="vwGetStarted" runat="server">
                    <div id="WelcomeDiv">
                        <asp:DataList ID="dlstGettingStarted" runat="server" RepeatColumns="3" RepeatLayout="Table"
                            RepeatDirection="Vertical" CellPadding="4" CellSpacing="2" Border="0">
                            <HeaderTemplate>
                                <div class="big">
                                    Click any of the following to get started</div>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <a href='<%# GetUrl(Eval("PopupPageName"), Eval("WelcomeId")) %>' onclick="return openStandardWindow(this.href);"
                                    target="_blank"><span class="links">
                                        <%# Eval("WelcomeDisplayCaption")%></span></a>
                            </ItemTemplate>
                            <FooterTemplate>
                            </FooterTemplate>
                        </asp:DataList>
                    </div>
                    <div id="CheckBoxDiv">
                        <asp:CheckBox ID="ChkShowAgain" runat="server" AutoPostBack="true" Checked="false"
                            OnCheckedChanged="ChkShowAgain_CheckedChanged" Text="Do not show this again"
                            TextAlign="Left" />
                    </div>
                </asp:View>
            </asp:MultiView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!-- End of Big buttons -->
</div>
<hr />
