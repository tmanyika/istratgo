﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="login.ascx.cs" Inherits="scorecard.ui.controls.login" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<div id="box">
    <table align="center">
        <tr>
            <td>
                <label>
                    <b>Username:</b></label>
            </td>
            <td>
                <asp:TextBox ID="txtUsername" class="mf" runat="server" ValidationGroup="Login" />
            </td>
        </tr>
        <tr>
            <td>
                <label>
                    <b>Password: </b>
                </label>
            </td>
            <td>
                <asp:TextBox ID="txtPassword" class="mf" TextMode="Password" runat="server" ValidationGroup="Login" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                &nbsp;
                <asp:Label ID="lblInvalid" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:Button ID="loginUsers" runat="server" Text="Login" class="login" OnClick="loginUsers_Click"
                    ValidationGroup="Login" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:ValidationSummary ID="vsLogin" runat="server" HeaderText="Please correct the following"
                    ShowMessageBox="True" ShowSummary="False" />
            </td>
            <td>
                <asp:LinkButton ID="lnkRegister" runat="server" class="login" OnClick="lnkRegister_Click"
                    Style="font-weight: 700; color: #000000; text-decoration: none" CausesValidation="False"><span class="loginLinks">Sign Up</span></asp:LinkButton>
                &nbsp;<a id="showModalPopupClientButton" href="#" class="login" style="font-weight: 700;
                    color: #000000; text-decoration: none"><span class="loginLinks">Forgot Password</span></a>
            </td>
        </tr>
    </table>
</div>
<asp:Panel ID="programmaticPopup" runat="server" Style="display: none" CssClass="modalPopup"
    Height="400px">
    <asp:Panel ID="programmaticPopupDragHandle" runat="server" CssClass="modalDragHandle">
        <h2>
            <b>Forgot Password</b></h2>
    </asp:Panel>
    <div class="modalHeightSpacer">
        &nbsp;
    </div>
    <div class="clr">
        <div class="floatLeft">
            <label>
                <b>Email Address:</b></label>
        </div>
        <div class="floatLeft" style="width: 5px;">
            <asp:RequiredFieldValidator ID="rfvLoginDetails" runat="server" ControlToValidate="txtUserlogin"
                Display="Dynamic" ErrorMessage="*" ValidationGroup="ForgotPassword" ForeColor="Red"></asp:RequiredFieldValidator>
        </div>
        <div class="floatLeft">
            <asp:TextBox ID="txtUserlogin" class="mf" runat="server" ValidationGroup="ForgotPassword"
                Width="200px" />
        </div>
        <div class="floatLeft">
            <asp:Button ID="btnResetPassword" runat="server" class="login" OnClick="btnResetPassword_Click"
                Text="Submit" ValidationGroup="ForgotPassword" />
        </div>
        <div class="floatLeft">
            <input type="button" id="hideModalPopupViaClientButton" class="login" value="Cancel" />
        </div>
    </div>
    <div class="clr">
        <br />
    </div>
    <div class="clr">
        <asp:RegularExpressionValidator ID="RegEmlExp" runat="server" ControlToValidate="txtUserlogin"
            Display="Dynamic" ErrorMessage="invalid email address" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
            ValidationGroup="tPassword"></asp:RegularExpressionValidator>
        <span style="color: Red">
            <asp:Literal ID="lblError" runat="server"></asp:Literal></span></div>
</asp:Panel>
<asp:Button runat="server" ID="hiddenTargetControlForModalPopup" Style="display: none" />
<ajaxToolkit:ModalPopupExtender runat="server" ID="programmaticModalPopup" BehaviorID="programmaticModalPopupBehavior"
    TargetControlID="hiddenTargetControlForModalPopup" PopupControlID="programmaticPopup"
    BackgroundCssClass="modalBackground" DropShadow="True" PopupDragHandleControlID="programmaticPopupDragHandle">
</ajaxToolkit:ModalPopupExtender>
<script type="text/javascript">
    // Add click handlers for buttons to show and hide modal popup on pageLoad
    function pageLoad() {
        $addHandler($get("showModalPopupClientButton"), 'click', showModalPopupViaClient);
        $addHandler($get("hideModalPopupViaClientButton"), 'click', hideModalPopupViaClient);
    }

    function showModalPopupViaClient(ev) {
        ev.preventDefault();
        var modalPopupBehavior = $find('programmaticModalPopupBehavior');
        modalPopupBehavior.show();
    }

    function hideModalPopupViaClient(ev) {
        ev.preventDefault();
        var modalPopupBehavior = $find('programmaticModalPopupBehavior');
        modalPopupBehavior.hide();
    }
</script>
