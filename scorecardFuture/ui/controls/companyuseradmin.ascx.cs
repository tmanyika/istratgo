﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.controllers;
using scorecard.implementations;
using scorecard.entities;
using System.Web.Security;

namespace scorecard.ui.controls
{
    public partial class companyuseradmin : System.Web.UI.UserControl
    {
        useradmincontroller userAdmin;
        structurecontroller orgunits;
        jobtitlesdefinitions impl;


        public companyuseradmin()
        {
            orgunits = new structurecontroller(new structureImpl());
            userAdmin = new useradmincontroller(new useradminImpl());
            impl = new jobtitlesdefinitions(new jobtitleImpl());

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadUsers();
                loadOrgunits();
                loadJobtitles();
                loadUsersReportsTo();
                loadUserTypes();
            }
        }

        void loadUsers()
        {
            try
            {
                var users = userAdmin.getAllCompanyUsers(new company { COMPANY_ID = Util.user.CompanyId }).Where(u => u.ACTIVE == true).ToList();
                dtPagerUsers.Visible = (users.Count <= dtPagerUsers.PageSize) ? false : true;
                BindControl.BindListView(lstVUsers, users);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void loadUsersReportsTo()
        {
            try
            {
                var userList = userAdmin.getAllCompanyUsers(new company { COMPANY_ID = Util.user.CompanyId }).Where(u => u.ACTIVE == true);
                if (userList.Count() > 0)
                {
                    ddlUsersReportsTo.Items.Clear();
                    ddlUsersReportsTo.DataSource = userList;
                    ddlUsersReportsTo.DataTextField = "NAME";
                    ddlUsersReportsTo.DataValueField = "ID";
                    ddlUsersReportsTo.DataBind();
                }
                else
                {
                    ddlUsersReportsTo.Items.Clear();
                    ddlUsersReportsTo.Items.Add(new ListItem { Text = "No Users In organisational unit", Value = "0" });
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void loadUserTypes()
        {
            try
            {
                var accountTypes = userAdmin.appsettings.getChildSettings(new applicationsettings { PARENT_ID = Util.getTypeDefinitionsSystemUserTypesParentId() }).ToList();
                ddlUserType.DataSource = accountTypes;
                ddlUserType.DataTextField = "NAME";
                ddlUserType.DataValueField = "TYPE_ID";
                ddlUserType.DataBind();
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void clearText()
        {
            mdlUserForm.Hide();
            lnkUpdateUser.Visible = false;
            lnkSaveAddUser.Visible = true;
            txtLoginName.Disabled = false;
            txtConfirmPassword.Disabled = false;
            txtPassword.Disabled = false;
            pnlPassword.Visible = true;
            txtConfirmPassword.Value = "";
            txtContactName.Value = "";
            txtEmailAddress.Value = "";
            txtLoginName.Value = "";
            ddlJobTitle.SelectedIndex = -1;
            ddlOrgUnit.SelectedIndex = -1;
            ddlUserType.SelectedIndex = -1;
            ddlUsersReportsTo.SelectedIndex = -1;

        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            clearText();
            loadUsers();
            loadOrgunits();
            loadJobtitles();
            loadUsersReportsTo();
            loadUserTypes();
            mdlUserForm.Show();
        }

        public string getRoleName(int roleID)
        {
            return impl.getJobTitleInfo(new JobTitle { ID = roleID }).NAME;
        }

        public string getAreaName(int areaID)
        {
            return orgunits.getOrgunitDetails(new Orgunit { ID = areaID }).ORGUNIT_NAME;
        }

        void loadOrgunits()
        {
            try
            {
                var structure = orgunits.getCompanyStructure(new company { COMPANY_ID = Util.user.CompanyId });
                ddlOrgUnit.DataSource = structure;
                ddlOrgUnit.DataTextField = "ORGUNIT_NAME";
                ddlOrgUnit.DataValueField = "ID";
                ddlOrgUnit.DataBind();
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void loadJobtitles()
        {
            try
            {
                var jobtitles = impl.getAllCompanyJobTitles(new company { COMPANY_ID = Util.user.CompanyId });
                ddlJobTitle.DataSource = jobtitles;
                ddlJobTitle.DataTextField = "NAME";
                ddlJobTitle.DataValueField = "ID";
                ddlJobTitle.DataBind();
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void createNewUser()
        {
            try
            {
                lblMsg.Text = "";
                var password = txtConfirmPassword.Value.Trim();
                var finalPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(password, "sha1");
                var user = new entities.useradmin
                {
                    NAME = txtContactName.Value.Trim(),
                    EMAIL_ADDRESS = txtEmailAddress.Value.Trim(),
                    JOB_TITLE_ID = int.Parse(ddlJobTitle.SelectedValue),
                    ORG_ID = int.Parse(ddlOrgUnit.SelectedValue),
                    PASSWD = finalPassword,
                    LOGIN_ID = txtLoginName.Value.Trim(),
                    REPORTS_TO = int.Parse(ddlUsersReportsTo.SelectedValue),
                    USER_TYPE = int.Parse(ddlUserType.SelectedValue)
                };
                string msg;
                if (userAdmin.addUserAccount(user, out msg) > 0)
                {
                    sendMail(user, password);
                    loadUsers();
                    loadJobtitles();
                    mdlUserForm.Hide();
                }
                else
                {
                    lblMsg.Text = msg;
                    mdlUserForm.Show();
                }
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void sendMail(entities.useradmin user, string password)
        {
            try
            {
                var subject = "Scorecard Workflow Notification :) ";
                var message = "You have a new profile created on online balance scorecard, Your user account Information below  <br><br> User Id :" + user.LOGIN_ID + "<br> Password :" + password + "<br><br> Please follow link <a href='" + Util.getConfigurationSettingServerAddress() + "'> here </a> to manage your scorecard <br> Do not reply to this email, It was sent via an automated system <br><br> Best Regards <br> Balance Scorecard Team";
                Util.SendEmailNotification(user.EMAIL_ADDRESS, message, subject);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        protected void lnkSaveAddUser_Click(object sender, EventArgs e)
        {
            createNewUser();
        }

        protected void lnkCancelAddUser_Click(object sender, EventArgs e)
        {
            clearText();
        }

        protected void lstVUsers_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            try
            {
                var userID = lstVUsers.Items[e.NewEditIndex].FindControl("lblID") as Label;
                if (userID != null && userID.Text != "")
                    editUser(int.Parse(userID.Text));
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void updateUserAccount()
        {
            try
            {
                var userAccount = new entities.useradmin
                {
                    ORG_ID = int.Parse(ddlOrgUnit.SelectedValue),
                    NAME = txtContactName.Value.Trim(),
                    EMAIL_ADDRESS = txtEmailAddress.Value.Trim(),
                    ID = int.Parse(lblUserID.Text),
                    JOB_TITLE_ID = int.Parse(ddlJobTitle.SelectedValue),
                    REPORTS_TO = int.Parse(ddlUsersReportsTo.SelectedValue),
                    USER_TYPE = int.Parse(ddlUserType.SelectedValue)
                };
                if (userAdmin.updateUserAccount(userAccount) > 0)
                {
                    mdlUserForm.Hide();
                    loadUsers();
                }
                else
                {
                    mdlUserForm.Show();
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void deleteUserAccount(ListViewCommandEventArgs e)
        {
            try
            {
                var lblUserID = e.Item.FindControl("lblID") as Label;
                if (lblUserID != null)
                {
                    userAdmin.deactivateUserAccountI(new entities.useradmin { ID = int.Parse(lblUserID.Text) });
                    loadUsers();
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void editUser(int userId)
        {
            try
            {
                clearText();

                loadOrgunits();
                loadJobtitles();
                loadUsersReportsTo();
                loadUserTypes();

                var UserInfo = userAdmin.getUserAccountInformationById(new entities.useradmin { ID = userId });
                txtLoginName.Disabled = true;
                lnkUpdateUser.Visible = true;
                lnkSaveAddUser.Visible = false;
                lblUserID.Text = userId.ToString();
                pnlPassword.Visible = false;

                txtLoginName.Value = UserInfo.LOGIN_ID;
                txtEmailAddress.Value = UserInfo.EMAIL_ADDRESS;
                txtContactName.Value = UserInfo.NAME;

                if (ddlJobTitle.Items.FindByValue(UserInfo.JOB_TITLE_ID.ToString()) != null)
                    ddlJobTitle.SelectedValue = UserInfo.JOB_TITLE_ID.ToString();
                if (ddlOrgUnit.Items.FindByValue(UserInfo.ORG_ID.ToString()) != null)
                    ddlOrgUnit.SelectedValue = UserInfo.ORG_ID.ToString();
                if (ddlUsersReportsTo.Items.FindByValue(UserInfo.REPORTS_TO.ToString()) != null)
                    ddlUsersReportsTo.SelectedValue = UserInfo.REPORTS_TO.ToString();
                if (ddlUserType.Items.FindByValue(UserInfo.REPORTS_TO.ToString()) != null)
                    ddlUserType.SelectedValue = UserInfo.USER_TYPE.ToString();
                ModalPopupExtender1.Show();
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        protected void lnkUpdateUser_Click(object sender, EventArgs e)
        {
            updateUserAccount();
        }

        protected void lstVUsers_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                switch (e.CommandName)
                {
                    case "deleteItem":
                        deleteUserAccount(e);
                        break;
                }
            }
        }

        protected void ddlOrgUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadUsersReportsTo();
            mdlUserForm.Show();
        }

        protected void ddlJobTitle_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        protected void ddlUsersReportsTo_SelectedIndexChanged(object sender, EventArgs e)
        {
            mdlUserForm.Hide();
        }

        protected void lnkCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("index.aspx");
        }

        #region Data Pager

        protected void DataPagerUser_PreRender(object sender, EventArgs e)
        {
            loadUsers();
        }

        #endregion
    }
}