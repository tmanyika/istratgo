﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.implementations;
using HR.Leave;

namespace scorecard.ui.controls.hr
{
    public partial class leaveAccrualTimes : System.Web.UI.UserControl
    {
        #region Properties

        int CompId
        {
            get { return Util.user.CompanyId; }
        }

        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) InitialisePage();
        }

        #endregion

        #region Utility Methods

        void InitialisePage()
        {
            LoadLeaveAccrualTime();
        }

        void LoadLeaveAccrualTime()
        {
            ILeaveAccrualTime obj = new LeaveAccrualTimeBL();
            BindControl.BindListView(lstData, obj.GetByStatus(true));
            pager.Visible = (lstData.Items.Count <= pager.PageSize) ? false : true;
        }

        #endregion

        #region Button Event Handling

        protected void lnkAdd_Click(object sender, EventArgs e)
        {
            BindControl.BindLiteral(lblMsg, "");
            lstData.InsertItemPosition = InsertItemPosition.LastItem;
            LoadLeaveAccrualTime();
        }

        protected void lnkCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("index.aspx", false);
        }

        #endregion

        #region Utility

        public string GetBoolean(object boolVal)
        {
            if (boolVal == DBNull.Value) return "Yes";
            return bool.Parse(boolVal.ToString()) ? "Yes" : "No";
        }

        private void CancelUpdate()
        {
            lstData.EditIndex = -1;
            lstData.InsertItemPosition = InsertItemPosition.None;
            LoadLeaveAccrualTime();
        }

        void DeleteItem(ListViewCommandEventArgs e)
        {
            int leaveId = int.Parse((e.Item.FindControl("hdnAccrualTimeId") as HiddenField).Value);
            string updatedBy = Util.user.LoginId;

            ILeaveAccrualTime obj = new LeaveAccrualTimeBL();
            bool deleted = obj.Delete(leaveId, updatedBy);
            if (deleted) LoadLeaveAccrualTime();
            string msg = deleted ? Messages.GetDeleteMessage() : Messages.GetDeleteFailedMessage();
            BindControl.BindLiteral(lblMsg, msg);
        }

        void UpdateItem(ListViewCommandEventArgs e)
        {
            try
            {
                int leaveId = int.Parse((e.Item.FindControl("hdnTimeId") as HiddenField).Value);
                Int16 timeValue = Int16.Parse((e.Item.FindControl("txtTimeVal") as TextBox).Text.Trim());
                
                bool active = (e.Item.FindControl("chkActive") as CheckBox).Checked;
                string name = (e.Item.FindControl("txtName") as TextBox).Text;
                string updatedBy = Util.user.LoginId;

                LeaveAccrualTime obj = new LeaveAccrualTime
                {
                    TimeName = name,
                    TimeValue = timeValue,
                    AccrualTimeId = leaveId,
                    CreatedBy = updatedBy,
                    UpdatedBy = updatedBy,
                    Active = active
                };

                ILeaveAccrualTime objl = new LeaveAccrualTimeBL();
                bool saved = objl.UpdateLeaveAccrualTime(obj);
                BindControl.BindLiteral(lblMsg, saved ? Messages.GetUpdateMessage() : Messages.GetUpdateFailedMessage());
                if (saved)
                {
                    lstData.EditIndex = -1;
                    LoadLeaveAccrualTime();
                }

            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        void AddItem(ListViewCommandEventArgs e)
        {
            try
            {
                Int16 timeValue = Int16.Parse((e.Item.FindControl("txtTimeVali") as TextBox).Text.Trim());
               
                bool active = (e.Item.FindControl("chkActivei") as CheckBox).Checked;
                
                string name = (e.Item.FindControl("txtNamei") as TextBox).Text;
                string updatedBy = Util.user.LoginId;

                LeaveAccrualTime obj = new LeaveAccrualTime
                {
                    TimeValue = timeValue,
                    TimeName = name,
                    AccrualTimeId = 0,
                    CreatedBy = updatedBy,
                    UpdatedBy = updatedBy,
                    Active = active
                };

                ILeaveAccrualTime objl = new LeaveAccrualTimeBL();
                bool saved = objl.AddLeaveAccrualTime(obj);
                BindControl.BindLiteral(lblMsg, saved ? Messages.GetUpdateMessage() : Messages.GetUpdateFailedMessage());
                if (saved)
                {
                    lstData.InsertItemPosition = InsertItemPosition.None;
                    LoadLeaveAccrualTime();
                }
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        #endregion

        #region ListView Event Handling Methods including Slide Paging

        protected void lstData_PagePropertiesChanged(object sender, EventArgs e)
        {
            LoadLeaveAccrualTime();
        }

        protected void lstData_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            try
            {
                lstData.EditIndex = e.NewEditIndex;
                lstData.InsertItemPosition = InsertItemPosition.None;
                LoadLeaveAccrualTime();
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        protected void lstData_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                BindControl.BindLiteral(lblMsg, "");
                if (e.Item.ItemType == ListViewItemType.InsertItem ||
               e.Item.ItemType == ListViewItemType.DataItem)
                {
                    switch (e.CommandName)
                    {
                        case "AddItem":
                            AddItem(e);
                            break;
                        case "UpdateItem":
                            UpdateItem(e);
                            break;
                        case "DeleteItem":
                            DeleteItem(e);
                            break;
                        case "CancelUpdate":
                            CancelUpdate();
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        #endregion
    }
}