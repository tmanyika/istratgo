﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.implementations;
using HR.Leave;
using System.Data;
using System.Email.Communication;
using System.Net.Mail;

namespace scorecard.ui.controls.hr
{
    public partial class leaveApplication : System.Web.UI.UserControl
    {
        #region Page Event Handling Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitialiseObjects();
                FillApplictions();
            }
        }

        void InitialiseObjects()
        {
            Session["Docs"] = null;
        }

        #endregion

        #region Databinding Methods

        void FillApplictions()
        {
            ILeaveApplication obj = new LeaveApplicationBL();
            List<LeaveApplication> data = obj.GetByApplicantId(Util.user.UserId);
            BindControl.BindListView(lstData, data);
            pager.Visible = data.Count > pager.PageSize ? true : false;
            mView.SetActiveView(vwData);
        }

        void BindDropDown()
        {
            ILeaveType obj = new LeaveTypeBL();
            List<LeaveType> data = obj.GetByStatus(Util.user.CompanyId, true);
            BindControl.BindDropdown(ddLeave, "LeaveName", "LeaveId", data);
        }

        void AddLeaveTransaction(int applicationId)
        {
            try
            {
                ILeaveCalendar lvc = new LeaveCalendarBL();
                ILeaveApplication obj = new LeaveApplicationBL();

                int leaveId = int.Parse(ddLeave.SelectedValue);
                int noofdays = int.Parse(txtNoOfDays.Text);
                int anoofdays = noofdays - 1;
                string notes = txtNotes.Text;

                DateTime sDate = DateTime.Parse(Common.GetInternationalDateFormat(txtDate.Text, DateFormat.dayMonthYear));
                if (Common.IsWeekendDate(sDate))
                {
                    ShowMessage(lblErr, "You cannot supply a weekend Start Date.");
                    return;
                }

                DateTime eDate = obj.AddBusinessDays(sDate, anoofdays);
                object docsXml = (Session["Docs"] == null) ? null : Common.GetXML(Session["Docs"] as DataTable, "Data", "Record");
                int noOfHolidays = lvc.GetNumberOfHolidays(sDate, eDate,Util.user.CountryId);

                eDate = obj.AddBusinessDays(sDate, noOfHolidays + anoofdays);//get final end date
                LeaveApplication lv = new LeaveApplication
                {
                    ApprovedByEmployeeId = null,
                    DateCreated = DateTime.Now,
                    DateUpdated = DateTime.Now,
                    Docs = docsXml,
                    EmployeeId = Util.user.UserId,
                    EndDate = eDate,
                    LeaveApplicationId = applicationId,
                    LeaveId = leaveId,
                    LeaveNotes = notes,
                    NoOfLeaveDays = noofdays,
                    RejectReason = string.Empty,
                    StartDate = sDate,
                    StatusId = Util.getLeaveStatusSubmittedId(),
                    SubmittedEmployeeId = Util.user.UserId
                };

                if (!obj.ApplicationValid(lv))
                {
                    ShowMessage(lblErr, Messages.GetLeaveApplicationOverlappingMessaging(eDate, sDate, "."));
                    return;
                }

                bool saved = false;
                if (applicationId <= 0)
                {
                    applicationId = obj.SubmitApplication(lv);
                    saved = (applicationId > 0) ? true : false;
                }
                else { saved = obj.UpdateApplication(lv); }

                if (saved)
                {
                    InitialiseObjects();
                    FillApplictions();
                    ShowMessage(lblMsg, Messages.GetSaveMessage());
                    SendEmail(applicationId, true, lblMsg);
                }
                else ShowMessage(lblErr, Messages.GetSaveFailedMessage());
            }
            catch (Exception ex)
            {
                string err = ex.ToString();
                Util.LogErrors(ex);
                ShowMessage(lblErr, Messages.GetErrorMessage());
            }
        }

        public void SendEmail(int applicationId, bool submitting, Literal lbl)
        {
            try
            {
                string result;
                string approverName;
                string approverEmail;
                string status;

                MailAddress recipient;

                ILeaveApplication obj = new LeaveApplicationBL();
                string data = obj.BuildEmailData(applicationId, submitting, out approverName, out approverEmail, out status, out recipient);
                if (!submitting)
                {
                    approverName = Util.user.FullName;
                    approverEmail = Util.user.Email;
                }

                if (data == string.Empty || (submitting && approverEmail == string.Empty))
                {
                    ShowMessage(lblErr, string.Format("{0} {1}", lbl.Text, Messages.GetEmailFailedMessage()));
                    return;
                }

                MailAddressCollection addy = new MailAddressCollection();
                IMailRelay er = new MailRelayBL();

                addy.Add(recipient);

                object[] mydata = (submitting) ? new object[] { Util.user.FullName, data } :
                                        new object[] { string.Format("{0} ({1})", approverName, approverEmail), status, data };
                int emailId = (submitting) ? Util.getLeaveSubmissionEmailId() : Util.getLeaveApprovalEmailId();

                MailRelay mr = new MailRelay
                {
                    MailId = emailId,
                    Recipient = addy,
                    Data = mydata
                };

                bool sent = er.SendMail(mr, out result);
            }
            catch (Exception ex)
            {
                string err = ex.ToString();
                Util.LogErrors(ex);
                ShowMessage(lbl, string.Format("{0} {1}", lblErr.Text, Messages.GetEmailFailedMessage()));
            }
        }

        #endregion

        #region Utility Methods

        void ButtonVisibility(bool newapplication)
        {
            btnAddLeave.Visible = newapplication ? true : false;
            btnUpdateLeave.Visible = newapplication ? false : true;
        }

        public bool GetVisibilityStatus(object statusId)
        {
            if (statusId == DBNull.Value) return true;
            return (int.Parse(statusId.ToString()) == Util.getLeaveStatusDeclinedId()) ? true : false;
        }

        void ClearTextBoxes()
        {
            lblErr.Text = "";
            txtDate.Text = "";
            txtNotes.Text = "";
            txtNoOfDays.Text = "";
        }

        void ShowMessage(Literal lbl, string msg)
        {
            lbl.Text = msg;
        }

        void UpdateItem(ListViewCommandEventArgs e)
        {
            try
            {
                int applicationId = int.Parse((e.Item.FindControl("hdnId") as HiddenField).Value);

                ILeaveApplication leave = new LeaveApplicationBL();
                LeaveApplication obj = leave.GetById(applicationId);

                ClearTextBoxes();
                BindDropDown();

                string sDate = string.Format("{0}/{1}/{2}", obj.StartDate.Day, obj.StartDate.Month, obj.StartDate.Year);

                txtDate.Text = sDate;
                txtNotes.Text = obj.LeaveNotes;
                txtNoOfDays.Text = obj.NoOfLeaveDays.ToString();
                if (ddLeave.Items.FindByValue(obj.LeaveId.ToString()) != null)
                    ddLeave.SelectedValue = obj.LeaveId.ToString();
                mView.SetActiveView(vwForm);
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        #endregion

        #region Button Event Handling Methods

        protected void lnkAdd_Click(object sender, EventArgs e)
        {
            ButtonVisibility(true);
            ClearTextBoxes();
            BindDropDown();
            mView.SetActiveView(vwForm);
        }

        protected void btnAddLeave_Click(object sender, EventArgs e)
        {
            AddLeaveTransaction(0);
        }

        protected void btnUpdateLeave_Click(object sender, EventArgs e)
        {
            AddLeaveTransaction(int.Parse(hdnId.Value));
        }

        protected void btnCancelLeave_Click(object sender, EventArgs e)
        {
            mView.SetActiveView(vwData);
        }

        #endregion

        #region ListView Event Handling Methods

        protected void lstData_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            pager.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            FillApplictions();
        }

        protected void lstData_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            //try
            //{
            //    BindControl.BindLiteral(lblMsg, "");
            //    if (e.Item.ItemType == ListViewItemType.InsertItem ||
            //   e.Item.ItemType == ListViewItemType.DataItem)
            //    {
            //        switch (e.CommandName)
            //        {
            //            case "UpdateItem":
            //                UpdateItem(e);
            //                break;
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
            //    Util.LogErrors(ex);
            //}
        }

        protected void lstData_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            try
            {
                int applicationId = int.Parse((lstData.Items[e.NewEditIndex].FindControl("hdnId") as HiddenField).Value);

                ILeaveApplication leave = new LeaveApplicationBL();
                LeaveApplication obj = leave.GetById(applicationId);

                ClearTextBoxes();
                BindDropDown();

                string sDate = string.Format("{0}/{1}/{2}", obj.StartDate.Day.ToString("D2"), obj.StartDate.Month.ToString("D2"), obj.StartDate.Year);
                lnkDoc.HRef = string.Format("~/ui/leavedoc.aspx?id={0}&show=true", applicationId);

                txtDate.Text = sDate;
                txtNotes.Text = obj.LeaveNotes;
                txtNoOfDays.Text = obj.NoOfLeaveDays.ToString();
                if (ddLeave.Items.FindByValue(obj.LeaveId.ToString()) != null)
                    ddLeave.SelectedValue = obj.LeaveId.ToString();

                hdnId.Value = applicationId.ToString();
                ButtonVisibility(false);
                mView.SetActiveView(vwForm);
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        #endregion
    }
}