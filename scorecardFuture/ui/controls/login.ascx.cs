﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.controllers;
using scorecard.implementations;
using System.Web.Security;
using System.Text;
using scorecard.entities;
using System.Configuration;
using HR.Human.Resources;
using Scorecard.Business.Rules;
using System.Net.Mail;
using System.Email.Communication;

namespace scorecard.ui.controls
{
    public partial class login : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        void loginUser()
        {
            try
            {
                var userName = txtUsername.Text.ToLower().Trim();
                var password = FormsAuthentication.HashPasswordForStoringInConfigFile(txtPassword.Text.Trim(), "sha1");

                IEmployee obj = new EmployeeBL();
                IBusinessRule brule = new BusinessRuleBL();
                Employee user = obj.GetByUserName(userName);
                if (user.UserName != null && string.CompareOrdinal(user.Pass, password) == 0)
                {
                    Session["UserInfo"] = new UserInfo
                    {
                        CompanyId = user.CompanyId,
                        LoginId = user.UserName,
                        OrgId = user.OrgUnitId,
                        UserTypeId = user.RoleId,
                        UserId = user.EmployeeId,
                        FullName = user.FullName,
                        Email = user.EmailAddress,
                        ReportToUserId = user.LineManagerEmployeeId,
                        CountryId = (int)user.CountryId,
                        ShowAgain = user.Start.ShowAgain
                    };

                    try { obj.UpdateLastLoginTime(user.EmployeeId); }
                    catch (Exception e) { Util.LogErrors(e); }

                    bool subValid = brule.SubscriptionValid(user.CompanyId);
                    string urlRedirect = subValid ? Util.getLandingPageUrl() : Util.getUpGradePageUrl();
                    Response.Redirect(urlRedirect, false);
                }
                else
                {
                    lblInvalid.Text = "invalid username / password";
                }
            }
            catch (Exception e)
            {
                lblInvalid.Text = Messages.GetErrorMessage();
                Util.LogErrors(e);
            }
        }

        void resetPassword()
        {
            try
            {
                string userName = txtUserlogin.Text.Trim();
                IEmployee obj = new EmployeeBL();
                Employee user = obj.GetByEmailAddress(userName);
                if (user.UserName == null)
                {                    //failed to evaluate user reset password
                    lblError.Text = "The email address you supplied is not valid";
                    programmaticModalPopup.Show();
                }
                else
                {
                    var password = Util.getRandomString(8);
                    var finalPassword = Util.HashPassword(password);

                    user.Pass = finalPassword;
                    user.UpdatedBy = user.UserName;

                    if (obj.UpdateLogin(user))
                    {
                        sendForgotPINMail(user, password);
                        programmaticModalPopup.Hide();
                    }
                    else
                    {
                        lblError.Text = "Account information is not valid";
                        programmaticModalPopup.Show();
                    }
                }
            }
            catch (Exception e)
            {
                lblInvalid.Text = Messages.GetErrorMessage();
                Util.LogErrors(e);
            }
        }

        void sendForgotPINMail(Employee user, string password)
        {
            try
            {
                string result = string.Empty;
                int emailId = Util.getForgotPasswordEmailId();

                MailAddressCollection addy = new MailAddressCollection();
                addy.Add(new MailAddress(user.EmailAddress, user.FullName));

                object[] mydata = { Util.getEmailDisplayName(), user.UserName, password };
                IMailRelay objm = new MailRelayBL();
                MailRelay obj = new MailRelay
                {
                    MailId = emailId,
                    Recipient = addy,
                    Data = mydata
                };
                objm.SendMail(obj, out result);
                lblInvalid.Text = "Account password was emailed to your email address";
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                BindControl.BindLiteral(lblError, Messages.GetErrorMessage());
            }
        }

        protected void loginUsers_Click(object sender, EventArgs e)
        {
            loginUser();
        }

        protected void lnkRegister_Click(object sender, EventArgs e)
        {
            Response.Redirect("registration.aspx", true);
        }

        protected void lnkChangePassword_Click(object sender, EventArgs e)
        {
            txtUserlogin.Text = txtUsername.Text;
            programmaticModalPopup.Show();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            programmaticModalPopup.Hide();
        }

        protected void btnResetPassword_Click(object sender, EventArgs e)
        {
            resetPassword();
        }

    }
}