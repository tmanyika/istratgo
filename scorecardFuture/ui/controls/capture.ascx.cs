﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.controllers;
using scorecard.entities;
using scorecard.interfaces;
using scorecard.implementations;
using HR.Human.Resources;
using HierarchicalData;
using System.Data;
using System.Collections;
using System.Text;
using Scorecard.Business.Rules;
using vb = Microsoft.VisualBasic;
using System.Net.Mail;
using System.Email.Communication;
using Telerik.Web.UI;
using System.IO;

namespace scorecard.ui.controls
{
    public partial class capture : System.Web.UI.UserControl
    {
        #region Member Variables, Properties
        goaldefinition goalDef;
        useradmincontroller usersDef;
        scoresmanager scoresManager;
        structurecontroller orgunits;
        IBusinessRule bR;
        IScoreRating iR;

        #endregion

        #region Default Constructors

        public capture()
        {
            orgunits = new structurecontroller(new structureImpl());
            goalDef = new goaldefinition(new goalsImpl());
            usersDef = new useradmincontroller(new useradminImpl());
            scoresManager = new scoresmanager(new scoresImpl());
            bR = new BusinessRuleBL();
            iR = new ScoreRatingBL();
        }

        #endregion

        #region Page Load Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            ClearLabels();
            if (!IsPostBack)
                LoadSubmissions();
        }

        void ClearLabels()
        {
            lblDateUsed.Text = string.Empty;
            LblDelete.Text = string.Empty;
            lblMsg.Text = string.Empty;
            LblNoData.Text = string.Empty;
            LblOutCome.Text = string.Empty;
        }

        void LoadSubmissions()
        {
            hdnUpdate.Value = "0";
            MainView.SetActiveView(vwData);
            DataTable dT = scoresManager.getPendingSubmissions(Util.user.UserId);
            BindControl.BindListView(lvSubmittedWorkflows, dT);
            lvSubmittedWorkflows.Visible = (dT.Rows.Count <= 0) ? false : true;
            LblNoData.Text = dT.Rows.Count > 0 ? string.Empty : Messages.GetRecordNotFoundMessage();
        }

        void initialiseForm()
        {
            try
            {
                txtDate.Text = string.Empty;
                MainView.SetActiveView(vwForm);
                getGoalsCriteriaTypes();
                BindControl.BindRepeater(rptFile, new List<attachmentDocuments>());
                SetVisibilityOfButtons(false);
                rptFile.Visible = rptFile.Items.Count > 0 ? true : false;
                string fullPath = Server.MapPath(Util.getConfigurationSettingUploadFolder());
                RdAsyncUpd.TargetFolder = fullPath;
                RdAsyncUpd.TemporaryFolder = fullPath;
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(LblNoData, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        void SetVisibilityOfButtons(bool visibul)
        {
            docFieldSet.Visible = visibul;
            btnNew.Visible = visibul;
            btnSaveLater.Visible = visibul;
        }

        #endregion

        #region Databinding Methods

        public string getObjectName(int id, int criteria)
        {
            try
            {
                if (id > 0)
                {
                    if (criteria == Util.getTypeDefinitionsRolesTypeID())
                    {
                        IEmployee obj = new EmployeeBL();
                        return obj.GetById(id).FullName;
                    }
                    else if (criteria == Util.getTypeDefinitionsProjectID()) return new projectmanagement(new projectImpl()).get(id).PROJECT_NAME;
                    else return orgunits.getOrgunitDetails(new Orgunit { ID = id }).ORGUNIT_NAME;
                }
            }
            catch (Exception e) { Util.LogErrors(e); }
            return string.Empty;
        }

        public string getTypeName(int id)
        {
            return goalDef.appsettings.getApplicationSetting(new entities.applicationsettings { TYPE_ID = id }).NAME;
        }

        int getUserAccountInfo(int ID)
        {
            try
            {
                if (ID == Util.getTypeDefinitionsRolesTypeID())
                {
                    IEmployee obj = new EmployeeBL();
                    var user = obj.GetById(int.Parse(ddlValueTypes.SelectedValue));
                    return user.JobTitleId == null ? 0 : (int)user.JobTitleId;
                }
                else return int.Parse(ddlValueTypes.SelectedValue);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return 0;
            }
        }

        void loadGoals()
        {
            try
            {
                if (ddlValueTypes.SelectedIndex == 0)
                {
                    pnlNoData.Visible = false;
                    BindControl.BindListView(lvGoals, new List<goals>());
                    return;
                }

                int areaTypeId = int.Parse(ddlArea.SelectedValue);
                int areaValueId = int.Parse(ddlValueTypes.SelectedValue);

                if (areaTypeId == Util.getTypeDefinitionsRolesTypeID())
                {
                    IEmployee obj = new EmployeeBL();
                    areaValueId = (int)obj.GetById(areaValueId).JobTitleId;
                }
                //var objectives = goalDef.listGeneralGoals(new
                //company
                //{
                //    COMPANY_ID = Util.user.CompanyId
                //}).Where(t => t.AREA_TYPE_ID == areaTypeId && t.AREA_ID == areaValueId).ToList();
                var objectives = goalDef.listGeneralGoals(Util.user.CompanyId, areaTypeId, areaValueId);
                pnlNoData.Visible = (objectives.Rows.Count <= 0) ? true : false;
                SetVisibilityOfButtons((objectives.Rows.Count > 0) ? true : false);
                BindControl.BindListView(lvGoals, objectives);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void getGoalsCriteriaTypes()
        {
            var areaList = goalDef.appsettings.getChildSettings(new applicationsettings { PARENT_ID = Util.getTypeDefinitionsGoalAreaTypes() }).ToList();
            BindControl.BindDropdown(ddlArea, "DESCRIPTION", "TYPE_ID", "- select area -", "-1", areaList);
        }

        void loadSetupTypes(DropDownList ddlPerspective)
        {
            try
            {
                var setuptypes = goalDef.appsettings.getChildSettings(new entities.applicationsettings { PARENT_ID = Util.getTypeDefinitionsPerspectivesParentID() });
                BindControl.BindDropdown(ddlValueTypes, "NAME", "TYPE_ID", setuptypes);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void loadJobTitles()
        {
            try
            {
                IEmployee obj = new EmployeeBL();
                List<Employee> titles = obj.GetByOrgStructure(Util.user.OrgId, true);

                if (Util.user.UserTypeId != Util.getTypeDefinitionsUserTypeAdministrator())
                    titles = titles.Where(t => t.LineManagerEmployeeId == Util.user.UserId || t.EmployeeId == Util.user.UserId).ToList();
                BindControl.BindDropdown(ddlValueTypes, "FullName", "EmployeeId", "- select user -", "-1", titles);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void loadStrategicGoals()
        {
            try
            {
                var strategic = goalDef.listStrategicGoals(new company { COMPANY_ID = Util.user.CompanyId });
                BindControl.BindDropdown(ddlValueTypes, "OBJECTIVE", "ID", "- select project -", "-1", strategic);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        public string getCurrentStore(int ID)
        {
            try
            {
                //  var score = scoresManager.getScoresByGoalID(new entities.goals { ID = ID , PERSPECTIVE = int.Parse(ddlValueTypes.SelectedValue)});
                //  return score.Single().SCORES.ToString();
                return string.Empty;
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return string.Empty;
            }
        }

        public string getFinalScore(int ID)
        {
            try
            {
                //var score = scoresManager.getScoresByGoalID(new entities.goals { ID = ID, PERSPECTIVE = int.Parse(ddlValueTypes.SelectedValue) });
                //return (score != null) ? score[0].FINAL_SCORE.ToString() : "0";
                //return (score != null) ? score.Single().FINAL_SCORE.ToString() : "0";
                return string.Empty;
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return "0";
            }
        }

        void loadAreas()
        {
            try
            {
                var userRole = Util.user.UserTypeId;
                var userID = Util.user.UserId;
                var dV = orgunits.getActiveCompanyOrgStructure(Util.user.CompanyId);

                if (userRole != Util.getTypeDefinitionsUserTypeAdministrator())
                    dV.RowFilter = string.Format("OWNER_ID = {0}", userID);
                BindControl.BindDropdown(ddlValueTypes, "ORGUNIT_NAME", "ID", "- select org unit -", "-1", dV);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void loadProjects(int companyId)
        {
            projectmanagement proj = new projectmanagement(new projectImpl());
            int adminRoleId = Util.getTypeDefinitionsUserTypeAdministrator();

            ddlValueTypes.DataTextField = "PROJECT_NAME";
            ddlValueTypes.DataValueField = "PROJECT_ID";
            int roleId = (int)Util.user.UserTypeId;
            IEnumerable<project> projL = proj.get(companyId, true);
            if (adminRoleId != Util.user.UserTypeId) projL = projL.Where(c => c.RESPONSIBLE_PERSON_ID == Util.user.UserId);
            BindControl.BindDropdown(ddlValueTypes, "PROJECT_NAME", "PROJECT_ID", "- select project -", "-1", projL);
        }

        void setCriteriaSelected()
        {
            try
            {
                int areaId = int.Parse(ddlArea.SelectedValue);
                if (areaId == Util.getTypeDefinitionsRolesTypeID()) loadJobTitles();
                else if (areaId == Util.getTypeDefinitionsProjectID()) loadProjects(Util.user.CompanyId);
                else loadAreas();
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        private entities.useradmin getApproverID(int id, int criteria)
        {
            try
            {
                if (criteria == Util.getTypeDefinitionsRolesTypeID())
                {
                    var approver_id = usersDef.getUserAccountInformation(new entities.useradmin { ID = (int)Util.user.ReportToUserId });
                    return approver_id;
                }
                else if (criteria == Util.getTypeDefinitionsProjectID())
                {
                    var proj = new projectmanagement(new projectImpl()).get(id);
                    var approver_id = new scorecard.entities.useradmin
                    {
                        ID = proj.RESPONSIBLE_PERSON_ID == null ? (int)Util.user.ReportToUserId : (int)proj.RESPONSIBLE_PERSON_ID,
                        NAME = proj.RESPONSIBLE_NAME,
                        EMAIL_ADDRESS = proj.RESPONSIBLE_EMAIL
                    };
                    return approver_id;
                }
                else
                {
                    var userId = goalDef.orgstructure.getOrgunitDetails(new Orgunit { ID = id }).OWNER_ID;
                    var approver_id = usersDef.getUserAccountInformation(new entities.useradmin { ID = userId });
                    return approver_id;
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return new entities.useradmin();
            }
        }

        bool dateAlreadyCaptured(string periodDate, int criteriaId, int itemValId)
        {
            List<scores> scoreLst = scoresManager.getUsedItemDates(criteriaId, itemValId);
            string tmpDate = periodDate.Replace('/', '-');
            foreach (scores obj in scoreLst)
            {
                DateTime dbdate = DateTime.Parse(obj.PERIOD_DATE_VAL);
                DateTime mydate = DateTime.Parse(periodDate);
                if (dbdate.Subtract(mydate).TotalDays == 0)
                    return true;
            }
            return false;
        }

        bool informationValid()
        {
            int suppliedItems = 0;
            foreach (ListViewItem item in lvGoals.Items)
            {
                HiddenField hdnUnitMeasureId = item.FindControl("hdnUnitMeasureId") as HiddenField;
                Literal lblError = item.FindControl("lblError") as Literal;
                TextBox txtScore = item.FindControl("txtScore") as TextBox;

                lblError.Text = "";
                int unitOfMeasure = int.Parse(hdnUnitMeasureId.Value);
                string val = txtScore.Text;

                if (!string.IsNullOrEmpty(val))
                {
                    if (bR.UnitOfMeasureRequireNumericValue(unitOfMeasure))
                    {
                        bool valNumeric = vb.Information.IsNumeric(val);
                        if (valNumeric) suppliedItems += 1;
                        else lblError.Text = Messages.NumericValueRequired;
                    }
                    else if (bR.UnitOfMeasureRequireYesNoValue(unitOfMeasure))
                    {
                        bool yesNoValid = bR.UnitOfMeasureRequireYesNoValueValid(val);
                        if (yesNoValid) suppliedItems += 1;
                        else lblError.Text = Messages.YesNoValueRequired;
                    }
                }
                else
                {
                    suppliedItems = 0;
                    lblError.Text = "* required.";
                }
            }
            return (suppliedItems == lvGoals.Items.Count) ? true : false;
        }

        void calculateValues(bool saveForLater)
        {
            try
            {
                var selectedItem = int.Parse(ddlValueTypes.SelectedValue);
                var selectedArea = int.Parse(ddlArea.SelectedValue);

                if (selectedItem <= 0 || selectedArea <= 0)
                {
                    lblDateUsed.Text = "area and value are required fields.";
                    return;
                }

                var periodDate = Common.GetInternationalDateFormat(txtDate.Text, DateFormat.dayMonthYear);

                ClearLabels();
                bool updateMode = string.Compare(hdnUpdate.Value, "0", true) == 0 ? false :
                                                                                    true;
                bool valid = saveForLater ? true : informationValid();
                bool dateCaptured = updateMode ? false :
                                                 dateAlreadyCaptured(periodDate, selectedArea, selectedItem);
                bool changetosavelater = false;

                if (dateCaptured || !valid)
                {
                    lblDateUsed.Text = dateCaptured ? "You cannot capture information twice for the same date." : "Correct errors below.";
                    return;
                }

                var prevStatusId = updateMode ? int.Parse(hdnStatusId.Value) : 0;
                var submissionId = updateMode ? int.Parse(hdnSubmissionId.Value) : 0;
                var approver = new useradmincontroller(new useradminImpl()).getLineManagerUserAccount(selectedArea, selectedItem);
                var setarea = string.Format("The resource for Approving the captured scores could not be found. Please make sure this is set in the {0} section.",
                                                ddlValueTypes.SelectedItem.Text);

                if (Util.BlockCapturingIfApproveIdIsEmpty && !saveForLater)
                {
                    if (string.IsNullOrEmpty(approver.EMAIL_ADDRESS) || approver.ID <= 0)
                    {
                        lblMsg.Text = setarea;
                        return;
                    }
                }
                else
                {
                    saveForLater = true;
                    changetosavelater = true;
                }

                var newRequest = new submissionWorkflow
                {
                    CRITERIA_TYPE_ID = int.Parse(ddlArea.SelectedValue),
                    SELECTED_ITEM_VALUE = int.Parse(ddlValueTypes.SelectedValue),
                    STATUS = (saveForLater || approver.ID <= 0) ? Util.getTypeDefinitionsWorkFlowSaveForLaterId() :
                                            Util.getTypeDefinitionsWorkFlowNewRequestId(),
                    APPROVER_ID = approver.ID,
                    CAPTURER_ID = Util.user.UserId,
                    ID = submissionId
                };

                var requestID = updateMode ? scoresManager.updateWorkFlow(newRequest) : scoresManager.createWorkFlow(newRequest);
                if (requestID > 0)
                {
                    decimal ratingVal = iR.GetCompanyRating(Util.user.CompanyId, true);
                    for (int i = 0; i < lvGoals.Items.Count; i++)
                    {
                        var lblTotal = lvGoals.Items[i].FindControl("lblTotal") as Label;
                        var lblCalc = lvGoals.Items[i].FindControl("lblUnitMeasureID") as Label;
                        var lblWeight = lvGoals.Items[i].FindControl("lblWeight") as Label;
                        var lblTarget = lvGoals.Items[i].FindControl("lblTarget") as Label;
                        var txtScore = lvGoals.Items[i].FindControl("txtScore") as TextBox;
                        var lblID = lvGoals.Items[i].FindControl("lblID") as Label;
                        var txtComment = lvGoals.Items[i].FindControl("txtComment") as TextBox;
                        var txtEvidence = lvGoals.Items[i].FindControl("txtEvidence") as TextBox;

                        if (lvGoals.Items[i].ItemType == ListViewItemType.DataItem)
                        {
                            var comment = txtComment.Text;
                            var evidence = txtEvidence.Text;
                            var unitMeasure = int.Parse(lblCalc.Text);
                            var weight = decimal.Parse(lblWeight.Text);
                            var item_id = int.Parse(lblID.Text);

                            decimal answer = 0;

                            if (string.IsNullOrEmpty(txtScore.Text))
                            {
                                answer = 0;
                            }
                            else
                            {
                                if (bR.UnitOfMeasureRequireNumericValue(unitMeasure))
                                {
                                    var target = decimal.Parse(lblTarget.Text);
                                    var score = decimal.Parse(txtScore.Text);
                                    answer = bR.FinalScore(unitMeasure, weight, score, target);
                                    lblTotal.Text = answer.ToString();
                                }
                                else
                                {
                                    if (txtScore.Text.Trim().ToLower() != "yes") lblTotal.Text = "0";
                                    else
                                    {
                                        lblTotal.Text = weight.ToString();
                                        answer = weight;
                                    }
                                }
                            }

                            decimal myscore = decimal.Parse(answer.ToString());
                            decimal finalAgreedScore = bR.GetDefinedWeightedScore(myscore, ratingVal);

                            scores _scoreDef = new scores
                            {
                                EVIDENCE = evidence,
                                JUSTIFICATION_COMMENT = comment,
                                CRITERIA_TYPE_ID = int.Parse(ddlArea.SelectedValue),
                                SELECTED_ITEM_VALUE = int.Parse(ddlValueTypes.SelectedValue),
                                SCORES = txtScore.Text.Trim(),
                                GOAL_ID = int.Parse(lblID.Text),
                                FINAL_SCORE = double.Parse(answer.ToString()),
                                SUBMISSION_ID = requestID,
                                PERIOD_DATE = DateTime.Parse(periodDate),
                                RATINGVALUE = ratingVal,
                                FINAL_SCORE_AGREED = finalAgreedScore,
                                ID = item_id,
                                STATUS_ID = prevStatusId
                            };
                            if (!updateMode) scoresManager.createNewScore(_scoreDef);
                            else scoresManager.updateScore(_scoreDef);
                        }
                    }

                    uploadFiles(updateMode ? submissionId : requestID);//createAttachements(updateMode ? submissionId : requestID);
                    pnlSubmitted.Visible = true;
                    pnlScores.Visible = false;
                    MainView.SetActiveView(vwForm);
                    //loadGoals(selectedItem, selectedArea, updateMode ? submissionId : requestID);//loadGoals();
                    if (!saveForLater)
                        sendMail(approver);
                    LblOutCome.Text = saveForLater ? string.Format("Scores have been successfully saved for later. {0}", changetosavelater ? setarea : string.Empty) : "Scores submitted succesfully, email has been sent to your manager.";
                }
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void uploadFiles(int submissionId)
        {
            try
            {
                foreach (UploadedFile myFile in RdAsyncUpd.UploadedFiles)
                {
                    string tmpName = myFile.GetName();
                    string fName = tmpName;//.Replace(" ", "");
                    string fullPath = string.Format(@"{0}{1}", Server.MapPath(Util.getConfigurationSettingUploadFolder()), fName);

                    FileInfo fInfo = new FileInfo(fullPath);
                    if (!fInfo.Exists)
                        myFile.SaveAs(fullPath);
                    fInfo = null;
                    attachmentDocuments docs = new attachmentDocuments
                    {
                        SUBMISSION_ID = submissionId,
                        ATTACHMENT_NAME = fName
                    };
                    scoresManager.createAttachment(docs);
                }
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void createAttachements(int submissionId)
        {
            try
            {
                for (int i = 0; i < 2; i++)
                {
                    var attachmentsUpload = this.FindControl("ftpAttachDocuments_" + i) as FileUpload;
                    if (attachmentsUpload != null && attachmentsUpload.PostedFile.ContentLength > 0)
                    {
                        attachmentDocuments docs = new attachmentDocuments
                        {
                            SUBMISSION_ID = submissionId,
                            ATTACHMENT_NAME = attachmentsUpload.PostedFile.FileName
                        };
                        attachmentsUpload.SaveAs(Server.MapPath(Util.getConfigurationSettingUploadFolder()) + attachmentsUpload.PostedFile.FileName);
                        scoresManager.createAttachment(docs);
                    }
                }
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void sendMail(entities.useradmin user)
        {
            try
            {
                string result = string.Empty;
                int emailId = Util.getWorkflowApprovalRequestEmailId();

                MailAddressCollection addy = new MailAddressCollection();
                addy.Add(new MailAddress(user.EMAIL_ADDRESS, user.NAME));

                IMailRelay objm = new MailRelayBL();
                MailRelay obj = new MailRelay
                {
                    MailId = emailId,
                    Recipient = addy,
                    Data = { }
                };
                objm.SendMail(obj, out result);
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        #endregion

        #region Dropdown Event Handling Methods

        protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlValueTypes.Items.Clear();
            pnlSubmitted.Visible = false;
            if (ddlArea.SelectedIndex == 0)
            {
                pnlScores.Visible = false;
                return;
            }
            else
            {
                setCriteriaSelected();
                loadGoals();
                pnlScores.Visible = true;
            }
        }

        protected void ddlValueTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlSubmitted.Visible = false;
            if (ddlValueTypes.SelectedIndex == 0)
            {
                pnlScores.Visible = false;
                return;
            }
            else
            {
                loadGoals();
                pnlScores.Visible = true;
            }
        }

        #endregion

        #region Button Event Handling Methods

        protected void btnBack_Click(object sender, EventArgs e)
        {
            pnlSubmitted.Visible = false;
            LoadSubmissions();
            UpdatePanelCapture.Update();
        }

        protected void lnkSubmitNew_Click(object sender, EventArgs e)
        {
            EnableControl(true);
            initialiseForm();
            ResetSelectedIndexes();
            pnlSubmitted.Visible = false;
            pnlScores.Visible = false;
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            calculateValues(false);
        }

        protected void btnSaveLater_Click(object sender, EventArgs e)
        {
            calculateValues(true);
        }

        void ResetSelectedIndexes()
        {
            ddlArea.SelectedIndex = 0;
            ddlValueTypes.SelectedIndex = 0;
            txtDate.Text = string.Empty;
            ClearLabels();
        }

        void EnableControl(bool enable)
        {
            ddlArea.Enabled = enable;
            ddlValueTypes.Enabled = enable;
            txtDate.Enabled = enable;
        }

        protected void btnNewScore_Click(object sender, EventArgs e)
        {
            hdnUpdate.Value = "0";
            initialiseForm();
            EnableControl(true);
        }

        #endregion

        #region ListView Event Handling Methods

        protected void lvSubmittedWorkflows_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                string commandName = e.CommandName.ToLower();
                var lblSubmittedId = e.Item.FindControl("lblID") as Label;
                var submitId = int.Parse(lblSubmittedId.Text);
                switch (commandName)
                {
                    case "view":
                        var lblItemSelected = e.Item.FindControl("lblItemSelected") as Label;
                        var lblCriteria = e.Item.FindControl("lblCriteria") as Label;
                        var hdnPrevStatusId = e.Item.FindControl("hdnPrevStatusId") as HiddenField;

                        hdnAreaId.Value = lblCriteria.Text.Trim();
                        hdnValueId.Value = lblItemSelected.Text.Trim();
                        hdnSubmissionId.Value = lblSubmittedId.Text;
                        hdnUpdate.Value = "1";
                        hdnStatusId.Value = hdnPrevStatusId.Value;

                        getGoalsCriteriaTypes();
                        if (ddlArea.Items.FindByValue(hdnAreaId.Value) != null)
                            ddlArea.SelectedValue = hdnAreaId.Value;

                        setCriteriaSelected();
                        if (ddlValueTypes.Items.FindByValue(hdnValueId.Value) != null)
                            ddlValueTypes.SelectedValue = hdnValueId.Value;

                        loadGoals(int.Parse(hdnValueId.Value), int.Parse(hdnAreaId.Value), int.Parse(lblSubmittedId.Text));
                        EnableControl(false);
                        MainView.SetActiveView(vwForm);
                        pnlScores.Visible = true;
                        pnlSubmitted.Visible = false;
                        break;
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        protected void lvSubmittedWorkflows_ItemDeleting(object sender, ListViewDeleteEventArgs e)
        {
            try
            {
                var lblSubmittedId = lvSubmittedWorkflows.Items[e.ItemIndex].FindControl("lblID") as Label;
                var submitId = int.Parse(lblSubmittedId.Text);
                DeleteCapturedScore(submitId);
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void DeleteCapturedScore(int submitId)
        {
            try
            {
                var result = scoresManager.deleteScores(submitId);
                if (result > 0)
                {
                    LoadSubmissions();
                    UpdatePanelCapture.Update();
                }
                BindControl.BindLiteral(LblNoData, result > 0 ? Messages.GetDeleteMessage() : Messages.GetDeleteFailedMessage());
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                BindControl.BindLiteral(LblNoData, Messages.GetErrorMessage());
            }
        }

        void loadGoals(int selectedId, int criteria, int submittedId)
        {
            try
            {
                var objectives = scoresManager.getSubmittedScores(new submissionWorkflow
                {
                    ID = submittedId,
                    CRITERIA_TYPE_ID = criteria,
                    SELECTED_ITEM_VALUE = selectedId
                });

                var totalCount = objectives.Count;
                SetVisibilityOfButtons((totalCount > 0) ? true : false);

                BindControl.BindListView(lvGoals, objectives);
                getAttachments(submittedId);
                if (totalCount > 0)
                {
                    string date = String.Format("{0:dd/MM/yyyy}", objectives[0].PERIOD_DATE);
                    txtDate.Text = date;
                }
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void getAttachments(int submittedId)
        {
            try
            {
                var attachments = scoresManager.getAllAttachments(new submissionWorkflow
                {
                    ID = submittedId
                });
                BindControl.BindRepeater(rptFile, attachments);
                rptFile.Visible = attachments.Count > 0 ? true : false;
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        public string getfilePath(object fileName)
        {
            return string.Format("{0}{1}", Util.getConfigurationSettingServerAttachmentsUrl(), fileName);
        }

        #endregion

        #region DataList Event Handling Methods

        protected void rptFile_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.ToLower().Equals("Delete".ToLower()))
                {
                    if (vb.Information.IsNumeric(e.CommandArgument))
                    {
                        int submissionId = int.Parse(hdnSubmissionId.Value);
                        int attachmentId = int.Parse(e.CommandArgument.ToString());
                        int result = scoresManager.deleteAttachment(attachmentId);
                        LblDelete.Text = (result > 0) ? Messages.GetDeleteMessage() : Messages.GetDeleteFailedMessage();
                        if (result > 0) getAttachments(submissionId);
                    }
                }
                else LblDelete.Text = Messages.GetDeleteFailedMessage();
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        #endregion
    }
}