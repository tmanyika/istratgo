﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.controllers;
using scorecard.implementations;
using scorecard.entities;

namespace scorecard.ui.controls
{
    public partial class mainmenu : System.Web.UI.UserControl
    {
        #region Variables

        menucontroller accessManager;

        #endregion

        #region Default Constructors

        public mainmenu()
        {
            accessManager = new menucontroller(new rolebasedImpl());
        }

        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            loadMenus();
        }

        #endregion

        #region Databindings

        private void loadMenus()
        {
            var userRole = Util.user.UserTypeId;
            var companyID = Util.user.CompanyId;
            BindControl.BindMenu(AppMenu, accessManager.getAccess(companyID, (int)userRole));
        }

        #endregion
    }
}