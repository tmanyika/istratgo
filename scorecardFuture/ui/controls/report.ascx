﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="report.ascx.cs" Inherits="scorecard.ui.controls.report" %>
<h1>
    &nbsp;Run Reports
</h1>
<fieldset runat="server" id="pnlContactInfo">
    <legend>Select Report Input</legend>
    <p>
        <label for="sf">
            Select Area:
        </label>
        &nbsp;<span class="field_desc"><asp:DropDownList runat="server" ID="ddlArea" AutoPostBack="True"
            OnSelectedIndexChanged="ddlArea_SelectedIndexChanged" Width="130px" />
        </span>
    </p>
    <p>
        <label for="sf">
            Select Value:
        </label>
        &nbsp;<span class="field_desc"><asp:DropDownList runat="server" ID="ddlValueTypes"
            AutoPostBack="True" OnSelectedIndexChanged="ddlValueTypes_SelectedIndexChanged"
            Width="130px" />
        </span>
    </p>
</fieldset>
<div id="tabs-3">
    <p>
        <asp:LinkButton ID="btnRunReport" runat="server" class="button tooltip" title="Run Report"
            ><span> Extract Report </span></asp:LinkButton>
    </p>
</div>
