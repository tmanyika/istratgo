﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ui/inside.Master" AutoEventWireup="true"
    CodeBehind="bulkimport.aspx.cs" Inherits="scorecard.ui.bulkimport" ValidateRequest="false" %>

<%@ Register Src="controls/orgstructure.ascx" TagName="orgstructure" TagPrefix="uc1" %>
<%@ Register Src="controls/bulkupload/uploadFile.ascx" TagName="uploadFile" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <uc2:uploadFile ID="uploadFile1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="OrgStructure" runat="server">
    <uc1:orgstructure ID="orgstructure1" runat="server" />
</asp:Content>
