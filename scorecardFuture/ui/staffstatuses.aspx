﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ui/inside.Master" AutoEventWireup="true" CodeBehind="staffstatuses.aspx.cs" Inherits="scorecard.ui.staffstatuses" %>
<%@ Register src="controls/hr/employeeStatuses.ascx" tagname="employeeStatuses" tagprefix="uc1" %>
<%@ Register src="controls/orgstructure.ascx" tagname="orgstructure" tagprefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <uc1:employeeStatuses ID="employeeStatuses1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="OrgStructure" runat="server">
    <uc2:orgstructure ID="orgstructure1" runat="server" />
</asp:Content>
