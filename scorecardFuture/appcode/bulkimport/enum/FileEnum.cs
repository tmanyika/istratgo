﻿using System;

namespace Bulk.Import
{
    public enum FileEnum
    {
        None = 0,
        ExcelXls = 1,
        ExcelXlsX = 2
    }

    public enum DataType
    {
        Date = 0,
        Decimal = 1,
        Integer = 2,
        String = 4
    }
}