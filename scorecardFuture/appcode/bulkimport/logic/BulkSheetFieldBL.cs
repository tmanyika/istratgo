﻿using System;
using System.Data;
using System.Collections.Generic;
using scorecard.implementations;

namespace Bulk.Import
{
    public class BulkSheetFieldBL : IBulkSheetField
    {
        #region Properties

        public string ConnectionString
        {
            get { return Util.GetconnectionString(); }
        }

        public BulkSheetFieldDAL Dal
        {
            get { return new BulkSheetFieldDAL(ConnectionString); }
        }

        public IBulkSheet Bs
        {
            get { return new BulkSheetBL(); }
        }

        #endregion

        #region Default Constructor

        public BulkSheetFieldBL() { }

        #endregion

        #region Public Methods & Functions

        public BulkSheetField GetRow(IDataRecord rw)
        {
            DateTime? defDate = null;
            return new BulkSheetField
            {
                Active = bool.Parse(rw["Active"].ToString()),
                CreatedBy = rw["CreatedBy"].ToString(),
                DataType = int.Parse(rw["DataType"].ToString()),
                DateCreated = DateTime.Parse(rw["DateCreated"].ToString()),
                DateUpdated = (rw["DateUpdated"] != DBNull.Value) ? DateTime.Parse(rw["DateUpdated"].ToString()) : defDate,
                DbFieldName = (rw["DbFieldName"] != DBNull.Value) ? rw["DbFieldName"].ToString() : string.Empty,
                ExpectedValueList = (rw["ExpectedValueList"] != DBNull.Value) ? rw["ExpectedValueList"].ToString() : string.Empty,
                FieldId = int.Parse(rw["FieldId"].ToString()),
                FieldNotes = (rw["FieldNotes"] != DBNull.Value) ? rw["FieldNotes"].ToString() : string.Empty,
                IsRequired = bool.Parse(rw["IsRequired"].ToString()),
                SheetFieldName = (rw["SheetFieldName"] != DBNull.Value) ? rw["SheetFieldName"].ToString() : string.Empty,
                SheetId = int.Parse(rw["SheetId"].ToString()),
                UpdatedBy = (rw["UpdatedBy"] != DBNull.Value) ? rw["UpdatedBy"].ToString() : string.Empty,
                Sheet = Bs.GetBriefRow(rw)
            };
        }

        public BulkSheetField GetField(int fieldId)
        {
            using (IDataReader data = Dal.Get(fieldId))
                return (data.Read()) ? GetRow(data) : new BulkSheetField();
        }

        public List<BulkSheetField> GetBySheetId(int sheetId)
        {
            List<BulkSheetField> obj = new List<BulkSheetField>();
            using (IDataReader data = Dal.GetSheetFields(sheetId))
            {
                while (data.Read())
                    obj.Add(GetRow(data));
            }
            return obj;
        }

        public List<BulkSheetField> GetBySheetName(string sheetName, bool active)
        {
            List<BulkSheetField> obj = new List<BulkSheetField>();
            using (IDataReader data = Dal.GetSheetFields(sheetName, active))
            {
                while (data.Read())
                    obj.Add(GetRow(data));
            }
            return obj;
        }

        public List<BulkSheetField> GetBySheetId(int sheetId, bool status)
        {
            List<BulkSheetField> obj = new List<BulkSheetField>();
            using (IDataReader data = Dal.GetSheetFields(sheetId, status))
            {
                while (data.Read())
                    obj.Add(GetRow(data));
            }
            return obj;
        }

        public List<BulkSheetField> GetAll(bool status)
        {
            List<BulkSheetField> obj = new List<BulkSheetField>();
            using (IDataReader data = Dal.GetSheetFields(status))
            {
                while (data.Read())
                    obj.Add(GetRow(data));
            }
            return obj;
        }

        public bool UpdateField(BulkSheetField obj)
        {
            int result = Dal.Update(obj);
            return (result > 0);
        }

        public bool SetStatus(BulkSheetField obj)
        {
            int result = Dal.UpdateStatus(obj);
            return (result > 0);
        }

        public bool AddField(BulkSheetField obj)
        {
            int result = Dal.Save(obj);
            return (result > 0);
        }

        #endregion
    }
}