﻿using System;
using System.Data;
using System.IO;
using System.Collections.Generic;

namespace Bulk.Import
{
    public interface IBulkImport
    {
        FileEnum GetFileTypeEnum(string extn);

        DataSet GetData(Stream stream, FileEnum fileType);
        DataSet AddAdditionalColumn(DataSet dS, List<BulkSheet> sheetList, string additionalFieldName, object defaultValue);
        DataTable ValidationResultsOfObjectives(int companyId);

        bool DeleteImportedData(int companyId);
        bool UpdateJobTitles(int companyId, string createdBy);
        bool UpdateProjects(int companyId, string createdBy);
        bool UpdateOrgUnits(int companyId, string createdBy);
        bool UpdateEmployees(int companyId, string createdBy);
        bool LinkInformation(int companyId, string createdBy);
        bool UpdatePerformanceMeasures(int companyId, string createdBy, out string outcome);
        bool ImportData(DataSet dS, List<BulkSheet> sheetList, List<BulkSheetField> fieldList, string additionalFieldName, object defaultValue, out string outcome);

        void SaveAllData(int companyId, string createdBy);
    }
}
