﻿using System;
using System.Data;
using System.Collections.Generic;

namespace Bulk.Import
{
    public interface IBulkSheet
    {
        BulkSheet GetRow(IDataRecord rw);
        BulkSheet GetBriefRow(IDataRecord rw);

        BulkSheet GetSheet(int sheetId);
        BulkSheet GetSheet(string sheetName);

        List<BulkSheet> GetAll();
        List<BulkSheet> GetByStatus(bool status);

        bool UpdateSheet(BulkSheet obj);
        bool SetStatus(BulkSheet obj);
        bool AddSheet(BulkSheet obj);

        bool SheetExist(List<BulkSheet> sheets, string sheetName, out string outcome);
        bool ValidateSheet(DataTable dT, string sheetName, out string outcome);
        bool ValidateSheet(DataTable dT, int sheetId, int sheetPos, out string outcome);
        bool ValidateSheets(DataSet dS, List<BulkSheet> sheetList, out string outcome);
    }
}
