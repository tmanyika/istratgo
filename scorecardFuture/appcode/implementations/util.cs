﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.IO;
using System.Net.Mail;
using System.Web.Security;
using scorecard.entities;
using vb = Microsoft.VisualBasic;
using System.Collections;
using System.Text;
using System.Security.Cryptography;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

/* @Copy Albertoncaffeine.net */

namespace scorecard.implementations
{
    public static class Util
    {
        static readonly string PasswordHash = "P@ssw0rd";
        static readonly string SaltKey = "S@LT&KEY";
        static readonly string VIKey = "@1B2c3D4e5F6g7H8";
        static string Operators = "=,>,<,>=,<=";

        public static List<ListItem> GetOperatorList()
        {
            List<ListItem> lItem = new List<ListItem>();
            string[] data = Operators.Split(',');
            foreach (string strVal in data)
                lItem.Add(new ListItem
                {
                    Text = strVal,
                    Value = strVal
                });
            return lItem;
        }

        public static void LogErrors(Exception error)
        {
            try
            {
                StreamWriter LogErrors = new StreamWriter(HttpContext.Current.Server.MapPath("~/logs/log.txt"), true);
                LogErrors.WriteLine(LogErrors.NewLine + DateTime.Now.ToLongDateString() + " ************************************************LOG " + error.HelpLink + " LOG ****************************************************************");
                LogErrors.Write(LogErrors.NewLine + error.Message + " Stack Trace ------------->> " + error.StackTrace + " Source ------->>" + error.Source + " Inner Exception ------- >>" + error.InnerException);
                LogErrors.Write(LogErrors.NewLine + " *******************************************************************************************************************");
                LogErrors.Close();
            }
            catch
            {
            }
        }

        public static bool IsStandardVersion
        {
            get { return bool.Parse(ConfigurationManager.AppSettings["IsStandardVersion"].ToString()); }
        }

        public static string GetCssClass
        {
            get
            {
                return IsStandardVersion ? ConfigurationManager.AppSettings["StandardVersionCssClass"].ToString() :
                    ConfigurationManager.AppSettings["EnterpriseVersionCssClass"].ToString();
            }
        }

        public static UserInfo user
        {
            get { return HttpContext.Current.Session["UserInfo"] as UserInfo; }
        }

        public static string ScoreDivTargetTimesWeight
        {
            get { return ConfigurationManager.AppSettings["ScoreDivTargetTimesWeight"].ToString(); }
        }

        public static string TargetDivScoreTimesWeight
        {
            get { return ConfigurationManager.AppSettings["TargetDivScoreTimesWeight"].ToString(); }
        }

        public static int ConnectionStringTimeOut
        {
            get { return int.Parse(ConfigurationManager.AppSettings["ConnectionStringTimeOut"].ToString()); }
        }

        public static string UnitOfMeasureYesNoExpectedValues
        {
            get { return ConfigurationManager.AppSettings["UnitOfMeasureYesNoExpectedValues"].ToString(); }
        }

        public static bool NotApplicableAllowed
        {
            get { return bool.Parse(ConfigurationManager.AppSettings["NotApplicableAllowed"].ToString()); }
        }

        public static string NotApplicableExpectedValues
        {
            get { return ConfigurationManager.AppSettings["NotApplicableExpectedValue"].ToString(); }
        }

        public static string UnitOfMeasureRequireYesNoValues
        {
            get { return ConfigurationManager.AppSettings["UnitOfMeasureRequireYesNoValues"].ToString(); }
        }

        public static string UnitOfMeasureRequireNumericIDs
        {
            get { return ConfigurationManager.AppSettings["UnitOfMeasureRequireNumericIDs"].ToString(); }
        }

        public static ArrayList getCompanyOwnerIds()
        {
            ArrayList obj = new ArrayList();
            string[] compIds = ConfigurationManager.AppSettings["OwnerCompanyIds"].ToString().Split(',');
            foreach (string strVal in compIds)
                obj.Add(strVal);
            return obj;
        }

        public static bool canAccessSecurity(string compId)
        {
            return getCompanyOwnerIds().Contains(compId);
        }

        public static ArrayList getMenuIdsTobeHidden()
        {
            ArrayList obj = new ArrayList();
            string[] compIds = ConfigurationManager.AppSettings["MenusToHide"].ToString().Split(',');
            foreach (string strVal in compIds)
                obj.Add(strVal);
            return obj;
        }

        public static bool menuShouldBeHidden(string menuId)
        {
            return getMenuIdsTobeHidden().Contains(menuId);
        }

        public static void SendEmailNotification(string emailAddress, string message, string subject)
        {
            try
            {

                MailMessage msg = new MailMessage();
                SmtpClient sender = new SmtpClient();

                string admin_email_address = ConfigurationManager.AppSettings["from-email-address"].ToString();
                string displayName = getEmailDisplayName();

                msg.To.Add(emailAddress);
                msg.From = new MailAddress(admin_email_address, displayName);
                msg.Priority = MailPriority.High;
                msg.Subject = subject;
                msg.Body = message;
                msg.IsBodyHtml = true;
                sender.Send(msg);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        public static int MaximumMatrixSize
        {
            get { return int.Parse(ConfigurationManager.AppSettings["MaximumMatrixSize"].ToString()); }
        }

        public static int getDashBoardGraphHeight()
        {
            return int.Parse(ConfigurationManager.AppSettings["DashboardHeight"].ToString());
        }

        public static int getDashBoardGraphWidth()
        {
            return int.Parse(ConfigurationManager.AppSettings["DashboardWidth"].ToString());
        }

        public static int getGraphHeight()
        {
            return int.Parse(ConfigurationManager.AppSettings["Height"].ToString());
        }

        public static int getGraphWidth()
        {
            return int.Parse(ConfigurationManager.AppSettings["Width"].ToString());
        }

        public static int getGraphMinItems()
        {
            return int.Parse(ConfigurationManager.AppSettings["GraphMinItems"].ToString());
        }

        public static int getDashBoardGraphMinItems()
        {
            return int.Parse(ConfigurationManager.AppSettings["DashBoardGraphMinItems"].ToString());
        }

        public static int getMaxNumOfWords()
        {
            return int.Parse(ConfigurationManager.AppSettings["MaxNumOfWords"].ToString());
        }

        public static string getRandomString(int size)
        {
            string strPwdchar = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";//"abcdefghijklmnopqrstuvwxyz0123456789#+@&$ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string strPwd = "";
            Random rnd = new Random();
            for (int i = 0; i <= size; i++)
            {
                int iRandom = rnd.Next(0, strPwdchar.Length - 1);
                strPwd += strPwdchar.Substring(iRandom, 1);
            }
            return strPwd;
        }

        public static string getGraphLabelName(string strVal)
        {
            string[] strV = strVal.Split(' ');
            int maxWords = getMaxNumOfWords();
            if (strV.Length > maxWords)
            {
                int idx = 1;
                StringBuilder strB = new StringBuilder("");
                foreach (string val in strV)
                {
                    if (idx > maxWords) break;
                    strB.Append(string.Format("{0} ", val));
                    idx++;
                }
                return strB.ToString().Trim();
            }
            return strVal;
        }

        public static int getRegWelcomeEmailId()
        {
            return int.Parse(ConfigurationManager.AppSettings["RegWelcomeEmailId"].ToString());
        }

        public static int getOrganisationalTypeGroupId()
        {
            return int.Parse(ConfigurationManager.AppSettings["GroupId"].ToString());
        }

        public static int getForgotPasswordEmailId()
        {
            return int.Parse(ConfigurationManager.AppSettings["ForgotPasswordEmailId"].ToString());
        }

        public static int getNewUserEmailId()
        {
            return int.Parse(ConfigurationManager.AppSettings["NewUserEmailId"].ToString());
        }

        public static int getWorkflowApprovalEmailId()
        {
            return int.Parse(ConfigurationManager.AppSettings["WorkflowApprovalEmailId"].ToString());
        }

        public static int getWorkflowApprovalRequestEmailId()
        {
            return int.Parse(ConfigurationManager.AppSettings["WorkflowApprovalRequestEmailId"].ToString());
        }

        public static int getLeaveApprovalEmailId()
        {
            return int.Parse(ConfigurationManager.AppSettings["LeaveApprovalEmailId"].ToString());
        }

        public static int getLeaveSubmissionEmailId()
        {
            return int.Parse(ConfigurationManager.AppSettings["LeaveSubmissionEmailId"].ToString());
        }

        public static int getLeaveStatusCancelledId()
        {
            return int.Parse(ConfigurationManager.AppSettings["CancelledStatusId"].ToString());
        }

        public static int getLeaveStatusDeclinedId()
        {
            return int.Parse(ConfigurationManager.AppSettings["DeclinedStatusId"].ToString());
        }

        public static int getLeaveStatusSubmittedId()
        {
            return int.Parse(ConfigurationManager.AppSettings["SubmitStatusId"].ToString());
        }

        public static int getLeaveStatusApprovedId()
        {
            return int.Parse(ConfigurationManager.AppSettings["ApprovedStatusId"].ToString());
        }

        public static string getEmailDisplayName()
        {
            return ConfigurationManager.AppSettings["from-email-display-name"].ToString();
        }

        public static string getFromEmailAddress()
        {
            return ConfigurationManager.AppSettings["from-email-address"].ToString();
        }

        public static string GetconnectionString()
        {
            return ConfigurationManager.ConnectionStrings["connectdb"].ToString();
        }

        public static int getTypeDefinitionsSetupTypeParentID()
        {
            return int.Parse(ConfigurationManager.AppSettings["typeDefinitionsSetupTypeParentID"].ToString());
        }

        public static int getTypeDefinitionsCountryTypeParentID()
        {
            return int.Parse(ConfigurationManager.AppSettings["typeDefinitionsCountryTypeParentID"].ToString());
        }

        public static int getTypeDefinitionsIndustryTypeParentID()
        {
            return int.Parse(ConfigurationManager.AppSettings["typeDefinitionsIndustryTypeParentID"].ToString());

        }

        public static int getTypeDefinitionsPerspectivesParentID()
        {
            return int.Parse(ConfigurationManager.AppSettings["typeDefinitionsPerspectivesParentID"].ToString());
        }

        public static int getTypeDefinitionsGoalAreaTypes()
        {
            return int.Parse(ConfigurationManager.AppSettings["typeDefinitionsGoalAreaTypes"].ToString());
        }

        public static int getTypeDefinitionsRolesTypeID()
        {
            return int.Parse(ConfigurationManager.AppSettings["typeDefinitionsRolesTypeID"].ToString());
        }

        public static int getTypeDefinitionsProjectID()
        {
            return int.Parse(ConfigurationManager.AppSettings["typeDefinitionsProjectID"].ToString());
        }

        public static int getTypeDefinitionsOrgUnitsTypeID()
        {
            return int.Parse(ConfigurationManager.AppSettings["typeDefinitionsOrgUnitsTypeID"].ToString());
        }
        public static int getTypeDefinitionsUnitOfMeasureTypesID()
        {
            return int.Parse(ConfigurationManager.AppSettings["typeDefinitionsUnitOfMeasureTypesID"].ToString());
        }
        public static int getTypeDefinitionsUnitOfMeasureYesNo()
        {
            return int.Parse(ConfigurationManager.AppSettings["typeDefinitionsUnitOfMeasureYesNo"].ToString());
        }
        public static int getTypeDefinitionsParentWorkFlowApprovalId()
        {
            return int.Parse(ConfigurationManager.AppSettings["typeDefinitionsParentWorkFlowApprovalId"].ToString());
        }
        public static int getTypeDefinitionsWorkFlowApprovalId()
        {
            return int.Parse(ConfigurationManager.AppSettings["typeDefinitionsWorkFlowApprovalId"].ToString());
        }
        public static int getTypeDefinitionsWorkFlowRejectedId()
        {
            return int.Parse(ConfigurationManager.AppSettings["typeDefinitionsWorkFlowRejectedId"].ToString());
        }
        public static bool isManagerSavingForlater(int statusId)
        {
            return (statusId == getTypeDefinitionsWorkFlowApprovalId()) ? false : true;
        }
        public static int getTypeDefinitionsWorkFlowNewRequestId()
        {
            return int.Parse(ConfigurationManager.AppSettings["typeDefinitionsWorkFlowNewRequestId"].ToString());
        }
        public static int getTypeDefinitionsWorkFlowSaveForLaterId()
        {
            return int.Parse(ConfigurationManager.AppSettings["typeDefinitionsWorkFlowSaveForLaterId"].ToString());
        }
        public static int getTypeDefinitionsWorkFlowApprovalInProgressId()
        {
            return int.Parse(ConfigurationManager.AppSettings["typeDefinitionsWorkFlowApprovalInProgressId"].ToString());
        }
        public static string getConfigurationSettingUploadFolder()
        {
            return ConfigurationManager.AppSettings["configurationSettingUploadFolder"].ToString();
        }
        public static string getConfigurationSettingLeaveUploadFolder()
        {
            string leavedocsFolder = ConfigurationManager.AppSettings["leaveDocFolder"].ToString();
            string rootPath = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["configurationSettingUploadFolder"].ToString());
            return string.Format(@"{0}\{1}", rootPath, leavedocsFolder);
        }
        public static string getConfigurationSettingServerAddress()
        {
            return ConfigurationManager.AppSettings["configurationSettingServerAddress"].ToString();
        }
        public static string getConfigurationSettingServerAttachmentsUrl()
        {
            return ConfigurationManager.AppSettings["configurationSettingServerAttachmentsUrl"].ToString();
        }
        public static int getTypeDefinitionsSystemUserTypesParentId()
        {
            return int.Parse(ConfigurationManager.AppSettings["typeDefinitionsSystemUserTypesParentId"].ToString());
        }
        public static int getTypeDefinitionsUserTypeAdministrator()
        {
            return int.Parse(ConfigurationManager.AppSettings["typeDefinitionsUserTypeAdministrator"].ToString());
        }
        public static int getTypeDefinitionsUserTypeManager()
        {
            return int.Parse(ConfigurationManager.AppSettings["typeDefinitionsUserTypeManager"].ToString());
        }
        public static int getTypeDefinitionsUserTypeNormalUser()
        {
            return int.Parse(ConfigurationManager.AppSettings["typeDefinitionsUserTypeNormalUser"].ToString());
        }
        public static int getSystemSettingsMenuId()
        {
            return int.Parse(ConfigurationManager.AppSettings["SystemSettingsId"].ToString());
        }
        public static string HashPassword(string password)
        {
            return FormsAuthentication.HashPasswordForStoringInConfigFile(password, "sha1");
        }
        public static int getTrialPeriodDays()
        {
            return int.Parse(ConfigurationManager.AppSettings["TrialPeriodDays"].ToString());
        }
        public static string getUpGradePageUrl()
        {
            return ConfigurationManager.AppSettings["UpGradePage"].ToString();
        }
        public static string getLandingPageUrl()
        {
            return ConfigurationManager.AppSettings["LandingPage"].ToString();
        }

        public static int BulkCopyBatchSize
        {
            get { return int.Parse(ConfigurationManager.AppSettings["BulkCopyBatchSize"].ToString()); }
        }

        public static string getCompanyName()
        {
            return ConfigurationManager.AppSettings["Company"].ToString();
        }

        public static int getCompanyID()
        {
            return int.Parse(ConfigurationManager.AppSettings["CompanyID"].ToString());
        }

        public static bool BlockCapturingIfApproveIdIsEmpty
        {
            get
            {
                return bool.Parse(ConfigurationManager.AppSettings["BlockCapturingIfApproveIdIsEmpty"].ToString());
            }
        }

        public static int MaximumScoreRatingValues
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings["MaximumScoreRatingValues"].ToString());
            }
        }

        public static string Encrypt(string plainText)
        {
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

            byte[] keyBytes = new Rfc2898DeriveBytes(PasswordHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
            var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.Zeros };
            var encryptor = symmetricKey.CreateEncryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));

            byte[] cipherTextBytes;

            using (var memoryStream = new MemoryStream())
            {
                using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                {
                    cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                    cryptoStream.FlushFinalBlock();
                    cipherTextBytes = memoryStream.ToArray();
                    cryptoStream.Close();
                }
                memoryStream.Close();
            }
            return Convert.ToBase64String(cipherTextBytes);
        }
        public static string Decrypt(string encryptedText)
        {
            byte[] cipherTextBytes = Convert.FromBase64String(encryptedText);
            byte[] keyBytes = new Rfc2898DeriveBytes(PasswordHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
            var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.None };

            var decryptor = symmetricKey.CreateDecryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));
            var memoryStream = new MemoryStream(cipherTextBytes);
            var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
            byte[] plainTextBytes = new byte[cipherTextBytes.Length];

            int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
            memoryStream.Close();
            cryptoStream.Close();
            return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount).TrimEnd("\0".ToCharArray());
        }

        public static bool IsVbNumeric(object strVal)
        {
            return vb.Information.IsNumeric(strVal);
        }

        public static bool GetVisibility(object parentId)
        {
            return (parentId == null);
        }

        public static bool GetParentVisibility(object hassubgoals)
        {
            return (hassubgoals == null || hassubgoals == DBNull.Value)
                ? true : !bool.Parse(hassubgoals.ToString());
        }

        public static bool GetSubVisibility(object parentId, object hassubgoals)
        {
            bool hassubgoal = bool.Parse(hassubgoals.ToString());
            return (parentId == null && hassubgoal) ? false : true;
        }

        public static bool IsNumeric(string strVal)
        {
            string numericRegEx = @"^\d+(\.\d{1,2})?$";
            Regex reg = new Regex(numericRegEx);
            return reg.IsMatch(strVal);
        }
    }
}