﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using scorecard.interfaces;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using scorecard.entities;
using System.Data;

/* @Copy Albertoncaffeine.net */

namespace scorecard.implementations
{
    public class scoresImpl : Iscores
    {
        #region Iscores Members

        public List<scores> getScoresByGoalID(goals _goal)
        {
            try
            {
                List<scores> company = new List<scores>();
                using (SqlDataReader read = SqlHelper.ExecuteReader(Util.GetconnectionString(), "PROC_GET_SCORES_BY_GOALID", _goal.ID, _goal.PERSPECTIVE))
                {
                    while (read.Read())
                    {
                        company.Add(new scores
                        {
                            ID = read["ID"] != DBNull.Value ? int.Parse(read["ID"].ToString()) : 0,
                            CRITERIA_TYPE_ID = read["CRITERIA_TYPE_ID"] != DBNull.Value ? int.Parse(read["CRITERIA_TYPE_ID"].ToString()) : 0,
                            SELECTED_ITEM_VALUE = read["SELECTED_ITEM_VALUE"] != DBNull.Value ? int.Parse(read["SELECTED_ITEM_VALUE"].ToString()) : 0,
                            GOAL_ID = read["GOAL_ID"] != DBNull.Value ? int.Parse(read["GOAL_ID"].ToString()) : 0,
                            SCORES = read["SCORES"] != DBNull.Value ? read["SCORES"].ToString() : string.Empty,
                            FINAL_SCORE = read["FINAL_SCORE"] != DBNull.Value ? Math.Round(double.Parse(read["FINAL_SCORE"].ToString()), 2) : 0,
                            DATE_CREATED = read["DATE_CREATED"] != DBNull.Value ? DateTime.Parse(read["DATE_CREATED"].ToString()) : DateTime.Now,
                            SUBMISSION_ID = read["SUBMISSION_ID"] != DBNull.Value ? int.Parse(read["SUBMISSION_ID"].ToString()) : 0,
                            PERIOD_DATE = DateTime.Parse(read["PERIOD_DATE"].ToString())
                        });
                    }
                    return company;
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
        }

        public List<scores> getUsedScoreDates(int criteriaId, int itemValueId)
        {
            try
            {
                List<scores> score = new List<scores>();
                using (SqlDataReader read = SqlHelper.ExecuteReader(Util.GetconnectionString(), "PROC_GET_SCORES_USEDDATES", criteriaId, itemValueId))
                {
                    while (read.Read())
                        score.Add(new scores
                        {
                            PERIOD_DATE_VAL = read["PERIOD_DATE"].ToString(),

                        });
                    return score;
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
        }

        public int updateScoreDate(int submissionId, string periodDate)
        {
            try
            {
                List<scores> score = new List<scores>();
                object result = SqlHelper.ExecuteScalar(Util.GetconnectionString(), "PROC_GET_SCORES_UPDATE_PERIODDATE", submissionId, periodDate);

                return (result != DBNull.Value) ? int.Parse(result.ToString()) : 0;
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return 0;
            }
        }

        public bool scoreDateAlreadyExist(int criteriaId, int itemValueId, int submissionId, string periodDate)
        {
            try
            {
                List<scores> score = new List<scores>();
                object result = SqlHelper.ExecuteScalar(Util.GetconnectionString(), "PROC_GET_SCORES_ISDATE_USED", criteriaId, itemValueId, submissionId, periodDate);
                return (result != DBNull.Value) ? bool.Parse(result.ToString()) : false;
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return true;
            }
        }

        public List<scores> getScoreDates(int compId, bool isCompId)
        {
            try
            {
                List<scores> score = new List<scores>();
                using (SqlDataReader read = SqlHelper.ExecuteReader(Util.GetconnectionString(), "PROC_GET_SCORES_USEDDATES_BYORGUNIT", compId, isCompId))
                {
                    while (read.Read())
                        score.Add(new scores { PERIOD_DATE_VAL = read["PERIOD_DATE"].ToString() });
                    return score;
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
        }

        public List<scores> getScoreDates(int criteriaId, int itemValueId)
        {
            try
            {
                List<scores> score = new List<scores>();
                using (SqlDataReader read = SqlHelper.ExecuteReader(Util.GetconnectionString(), "PROC_GET_SCORES_DATES", criteriaId, itemValueId))
                {
                    while (read.Read())
                    {
                        score.Add(new scores
                        {
                            PERIOD_DATE_VAL = read["PERIOD_DATE"].ToString(),
                            PERIOD_DATE_DESC = read["PERIOD_DATE_DESC"].ToString(),
                        });
                    }
                    return score;
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
        }

        public List<scores> getReportDates(int criteriaId, string xmlData)
        {
            try
            {
                List<scores> score = new List<scores>();
                using (SqlDataReader read = SqlHelper.ExecuteReader(Util.GetconnectionString(), "PROC_GET_SCORES_AREA_DATES", criteriaId, xmlData))
                {
                    while (read.Read())
                    {
                        score.Add(new scores
                        {
                            PERIOD_DATE_VAL = read["PERIOD_DATE"].ToString(),
                            PERIOD_DATE_DESC = read["PERIOD_DATE_DESC"].ToString(),
                        });
                    }
                    return score;
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
        }

        public List<PerformanceReport> getDepartmentScorePerformanceReport(int criteriaId, int itemValueId, string sdate, string edate, bool isCompany)
        {
            try
            {
                decimal? defDec = null;
                int? defInt = null;
                List<PerformanceReport> score = new List<PerformanceReport>();
                using (SqlDataReader read = SqlHelper.ExecuteReader(Util.GetconnectionString(), "PROC_GET_SCORES_REPORT_DEPARTMENT", criteriaId, itemValueId, sdate, edate, isCompany))
                {
                    while (read.Read())
                    {
                        score.Add(new PerformanceReport
                        {
                            ID = int.Parse(read["ID"].ToString()),
                            CRITERIA_TYPE_ID = int.Parse(read["CRITERIA_TYPE_ID"].ToString()),
                            SELECTED_ITEM_VALUE = int.Parse(read["SELECTED_ITEM_VALUE"].ToString()),
                            GOAL_ID = int.Parse(read["GOAL_ID"].ToString()),
                            SCORES = read["SCORES"] != DBNull.Value ? read["SCORES"].ToString() : string.Empty,
                            FINAL_SCORE = double.Parse(read["FINAL_SCORE"].ToString()),
                            FINAL_SCORE_AGREED = (read["FINAL_SCORE_AGREED"] != DBNull.Value) ? Math.Round(decimal.Parse(read["FINAL_SCORE_AGREED"].ToString()), 1) : 0,
                            RATINGVALUE = (read["RATINGVALUE"] != DBNull.Value) ? decimal.Parse(read["RATINGVALUE"].ToString()) : defDec,
                            DATE_CREATED = DateTime.Parse(read["DATE_CREATED"].ToString()),
                            SUBMISSION_ID = int.Parse(read["SUBMISSION_ID"].ToString()),
                            WEIGHT = int.Parse(read["WEIGHT"].ToString()),
                            UNIT_MEASURE_NAME = read["UNIT_MEASURE"].ToString(),
                            TARGET = read["TARGET"] != DBNull.Value ? read["TARGET"].ToString() : string.Empty,
                            GOAL_DESCRIPTION = read["GOAL_DESCRIPTION"].ToString(),
                            PERIOD_DATE = DateTime.Parse(read["PERIOD_DATE"].ToString()),
                            PERSPECTIVE_NAME = read["PERSPECTIVE_NAME"].ToString(),
                            OBJECTIVE = read["OBJECTIVE"].ToString(),
                            PERSPECTIVE_ID = int.Parse(read["PERSPECTIVE_ID"].ToString()),
                            ItemId = (read["ItemId"] != DBNull.Value) ? int.Parse(read["ItemId"].ToString()) : 0,
                            ItemName = (read["ItemName"] != DBNull.Value) ? read["ItemName"].ToString() : string.Empty,
                            HAS_SUB_GOALS = bool.Parse(read["HAS_SUB_GOALS"].ToString()),
                            PARENT_ID = (read["PARENT_ID"] != DBNull.Value) ? int.Parse(read["PARENT_ID"].ToString()) : defInt
                        });
                    }
                    return score;
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
        }

        public List<PerformanceReport> getDepartmentUsersScorePerformanceReport(int criteriaId, int itemValueId, string sdate, string edate)
        {
            try
            {
                int? defInt = null;
                List<PerformanceReport> score = new List<PerformanceReport>();
                using (SqlDataReader read = SqlHelper.ExecuteReader(Util.GetconnectionString(), "PROC_GET_SCORES_REPORT_USERSDEPARTMENT", criteriaId, itemValueId, sdate, edate))
                {
                    while (read.Read())
                    {
                        score.Add(new PerformanceReport
                        {
                            ID = int.Parse(read["ID"].ToString()),
                            CRITERIA_TYPE_ID = int.Parse(read["CRITERIA_TYPE_ID"].ToString()),
                            SELECTED_ITEM_VALUE = int.Parse(read["SELECTED_ITEM_VALUE"].ToString()),
                            GOAL_ID = int.Parse(read["GOAL_ID"].ToString()),
                            SCORES = read["SCORES"] != DBNull.Value ? read["SCORES"].ToString() : string.Empty,
                            FINAL_SCORE = Math.Round(double.Parse(read["FINAL_SCORE"].ToString()), 2),
                            FINAL_SCORE_AGREED = (read["FINAL_SCORE_AGREED"] != DBNull.Value) ? Math.Round(decimal.Parse(read["FINAL_SCORE_AGREED"].ToString()), 2) : 0,
                            DATE_CREATED = DateTime.Parse(read["DATE_CREATED"].ToString()),
                            SUBMISSION_ID = int.Parse(read["SUBMISSION_ID"].ToString()),
                            WEIGHT = (read["WEIGHT"] != DBNull.Value) ? int.Parse(read["WEIGHT"].ToString()) : 0,
                            UNIT_MEASURE_NAME = read["UNIT_MEASURE"].ToString(),
                            TARGET = read["TARGET"] != DBNull.Value ? read["TARGET"].ToString() : string.Empty,
                            GOAL_DESCRIPTION = read["GOAL_DESCRIPTION"].ToString(),
                            PERIOD_DATE = DateTime.Parse(read["PERIOD_DATE"].ToString()),
                            PERSPECTIVE_NAME = read["PERSPECTIVE_NAME"].ToString(),
                            OBJECTIVE = read["OBJECTIVE"].ToString(),
                            PERSPECTIVE_ID = int.Parse(read["PERSPECTIVE_ID"].ToString()),
                            ItemId = (read["ItemId"] != DBNull.Value) ? int.Parse(read["ItemId"].ToString()) : 0,
                            ItemName = (read["ItemName"] != DBNull.Value) ? read["ItemName"].ToString() : string.Empty,
                            PARENT_ID = (read["PARENT_ID"] != DBNull.Value) ? int.Parse(read["PARENT_ID"].ToString()) : defInt
                        });
                    }
                    return score;
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
        }

        public List<PerformanceReport> getEmployeeScorePerformanceReport(int criteriaId, int itemValueId, string sdate, string edate)
        {
            try
            {
                decimal? defDec = null;
                int? defInt = null;

                List<PerformanceReport> score = new List<PerformanceReport>();
                using (SqlDataReader read = SqlHelper.ExecuteReader(Util.GetconnectionString(), "PROC_GET_SCORES_REPORT_EMPLOYEES", criteriaId, itemValueId, sdate, edate))
                {
                    while (read.Read())
                    {
                        score.Add(new PerformanceReport
                        {
                            ID = int.Parse(read["ID"].ToString()),
                            CRITERIA_TYPE_ID = int.Parse(read["CRITERIA_TYPE_ID"].ToString()),
                            SELECTED_ITEM_VALUE = int.Parse(read["SELECTED_ITEM_VALUE"].ToString()),
                            GOAL_ID = int.Parse(read["GOAL_ID"].ToString()),
                            SCORES = read["SCORES"] != DBNull.Value ? read["SCORES"].ToString() : string.Empty,
                            FINAL_SCORE = Math.Round(double.Parse(read["FINAL_SCORE"].ToString()), 2),
                            FINAL_SCORE_AGREED = (read["FINAL_SCORE_AGREED"] != DBNull.Value) ? Math.Round(decimal.Parse(read["FINAL_SCORE_AGREED"].ToString()), 2) : 0,
                            RATINGVALUE = (read["RATINGVALUE"] != DBNull.Value) ? decimal.Parse(read["RATINGVALUE"].ToString()) : defDec,
                            DATE_CREATED = DateTime.Parse(read["DATE_CREATED"].ToString()),
                            SUBMISSION_ID = int.Parse(read["SUBMISSION_ID"].ToString()),
                            WEIGHT = (read["WEIGHT"] != DBNull.Value) ? int.Parse(read["WEIGHT"].ToString()) : 0,
                            UNIT_MEASURE_NAME = read["UNIT_MEASURE"].ToString(),
                            TARGET = read["TARGET"] != DBNull.Value ? read["TARGET"].ToString() : string.Empty,
                            GOAL_DESCRIPTION = read["GOAL_DESCRIPTION"].ToString(),
                            PERIOD_DATE = DateTime.Parse(read["PERIOD_DATE"].ToString()),
                            PERSPECTIVE_NAME = read["PERSPECTIVE_NAME"].ToString(),
                            OBJECTIVE = read["OBJECTIVE"].ToString(),
                            PERSPECTIVE_ID = int.Parse(read["PERSPECTIVE_ID"].ToString()),
                            ItemId = (read["ItemId"] != DBNull.Value) ? int.Parse(read["ItemId"].ToString()) : 0,
                            ItemName = (read["ItemName"] != DBNull.Value) ? read["ItemName"].ToString() : string.Empty,
                            PARENT_ID = (read["PARENT_ID"] != DBNull.Value) ? int.Parse(read["PARENT_ID"].ToString()) : defInt,
                            HAS_SUB_GOALS = bool.Parse(read["HAS_SUB_GOALS"].ToString())
                        });
                    }
                    return score;
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
        }

        public List<PerformanceReport> getJobTitleScorePerformanceReport(int criteriaId, int itemValueId, string sdate, string edate)
        {
            try
            {
                decimal? defDec = null;
                int? defInt = null;
                List<PerformanceReport> score = new List<PerformanceReport>();
                using (SqlDataReader read = SqlHelper.ExecuteReader(Util.GetconnectionString(), "PROC_GET_SCORES_REPORT_EMPLOYEES_BYJOBTITLE", criteriaId, itemValueId, sdate, edate))
                {
                    while (read.Read())
                    {
                        score.Add(new PerformanceReport
                        {
                            ID = int.Parse(read["ID"].ToString()),
                            CRITERIA_TYPE_ID = int.Parse(read["CRITERIA_TYPE_ID"].ToString()),
                            SELECTED_ITEM_VALUE = int.Parse(read["SELECTED_ITEM_VALUE"].ToString()),
                            GOAL_ID = int.Parse(read["GOAL_ID"].ToString()),
                            SCORES = read["SCORES"] != DBNull.Value ? read["SCORES"].ToString() : string.Empty,
                            RATINGVALUE = read["RATINGVALUE"] != DBNull.Value ? decimal.Parse(read["RATINGVALUE"].ToString()) : defDec,
                            FINAL_SCORE = Math.Round(double.Parse(read["FINAL_SCORE"].ToString()), 2),
                            FINAL_SCORE_AGREED = (read["FINAL_SCORE_AGREED"] != DBNull.Value) ? Math.Round(decimal.Parse(read["FINAL_SCORE_AGREED"].ToString()), 2) : 0,
                            DATE_CREATED = DateTime.Parse(read["DATE_CREATED"].ToString()),
                            SUBMISSION_ID = int.Parse(read["SUBMISSION_ID"].ToString()),
                            WEIGHT = (read["WEIGHT"] != DBNull.Value) ? int.Parse(read["WEIGHT"].ToString()) : 0,
                            UNIT_MEASURE_NAME = read["UNIT_MEASURE"].ToString(),
                            TARGET = read["TARGET"] != DBNull.Value ? read["TARGET"].ToString() : string.Empty,
                            GOAL_DESCRIPTION = read["GOAL_DESCRIPTION"].ToString(),
                            PERIOD_DATE = DateTime.Parse(read["PERIOD_DATE"].ToString()),
                            PERSPECTIVE_NAME = read["PERSPECTIVE_NAME"].ToString(),
                            OBJECTIVE = read["OBJECTIVE"].ToString(),
                            PERSPECTIVE_ID = int.Parse(read["PERSPECTIVE_ID"].ToString()),
                            ItemId = (read["ItemId"] != DBNull.Value) ? int.Parse(read["ItemId"].ToString()) : 0,
                            ItemName = (read["ItemName"] != DBNull.Value) ? read["ItemName"].ToString() : string.Empty,
                            PARENT_ID = (read["PARENT_ID"] != DBNull.Value) ? int.Parse(read["PARENT_ID"].ToString()) : defInt
                        });
                    }
                    return score;
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
        }

        public List<reportexception> getScoreExceptionReport(int orgUnitId, string date)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            SqlDataReader read = null;
            try
            {
                double? defDec = null;
                List<reportexception> score = new List<reportexception>();
                read = SqlHelper.ExecuteReader(con, "PROC_GET_SCORES_EXCEPTIONREPORT", orgUnitId, date);
                while (read.Read())
                {
                    score.Add(new reportexception
                    {
                        FinalRating = (read["FinalScoreAgreed"] != DBNull.Value) ?
                                       Math.Round(double.Parse(read["FinalScoreAgreed"].ToString()), 2) : defDec,
                        FinalScore = (read["FinalScore"] != DBNull.Value) ?
                                    Math.Round(double.Parse(read["FinalScore"].ToString()), 2) : defDec,
                        FullName = read["FullName"].ToString(),
                        Status = read["StatusName"].ToString()
                    });
                }
                return score;
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
            finally
            {
                read.Dispose();
                read.Close();
                con.Dispose();
                con.Close();
            }
        }

        public List<scores> getScoreReport(int criteriaId, int itemValueId, string dateValue)
        {
            try
            {
                decimal? defDec = null;
                int? defInt = null;
                List<scores> score = new List<scores>();
                using (SqlDataReader read = SqlHelper.ExecuteReader(Util.GetconnectionString(), "PROC_GET_SCORES_REPORT", criteriaId, itemValueId, dateValue))
                {
                    while (read.Read())
                    {
                        score.Add(new scores
                        {
                            ID = int.Parse(read["ID"].ToString()),
                            CRITERIA_TYPE_ID = int.Parse(read["CRITERIA_TYPE_ID"].ToString()),
                            SELECTED_ITEM_VALUE = int.Parse(read["SELECTED_ITEM_VALUE"].ToString()),
                            GOAL_ID = int.Parse(read["GOAL_ID"].ToString()),
                            SCORES = read["SCORES"] != DBNull.Value ? read["SCORES"].ToString() : string.Empty,
                            AGREED_SCORE = (read["AGREED_SCORE"] != DBNull.Value) ? !string.IsNullOrEmpty(read["AGREED_SCORE"].ToString()) ?
                                            read["AGREED_SCORE"].ToString() : read["SCORES"].ToString() : string.Empty,
                            FINAL_SCORE = Math.Round(double.Parse(read["FINAL_SCORE"].ToString()), 2),
                            DATE_CREATED = DateTime.Parse(read["DATE_CREATED"].ToString()),
                            SUBMISSION_ID = int.Parse(read["SUBMISSION_ID"].ToString()),
                            WEIGHT = (read["WEIGHT"] != DBNull.Value) ? int.Parse(read["WEIGHT"].ToString()) : 0,
                            UNIT_MEASURE_NAME = read["UNIT_MEASURE"].ToString(),
                            TARGET = read["TARGET"] != DBNull.Value ? read["TARGET"].ToString() : string.Empty,
                            GOAL_DESCRIPTION = read["GOAL_DESCRIPTION"].ToString(),
                            PERIOD_DATE = DateTime.Parse(read["PERIOD_DATE"].ToString()),
                            PERSPECTIVE_NAME = read["PERSPECTIVE_NAME"].ToString(),
                            OBJECTIVE = read["OBJECTIVE"].ToString(),
                            PERSPECTIVE_ID = int.Parse(read["PERSPECTIVE_ID"].ToString()),
                            FINAL_SCORE_AGREED = (read["FINAL_SCORE_AGREED"] != DBNull.Value) ? Math.Round(decimal.Parse(read["FINAL_SCORE_AGREED"].ToString()), 2) : 0,
                            JUSTIFICATION_COMMENT = (read["JUSTIFICATION_COMMENT"] != DBNull.Value) ? read["JUSTIFICATION_COMMENT"].ToString() : string.Empty,
                            MANAGER_COMMENT = (read["MANAGER_COMMENT"] != DBNull.Value) ? read["MANAGER_COMMENT"].ToString() : string.Empty,
                            MANAGER_SCORE = (read["MANAGER_SCORE"] != DBNull.Value) ? !string.IsNullOrEmpty(read["MANAGER_SCORE"].ToString()) ?
                                            read["MANAGER_SCORE"].ToString() : read["SCORES"].ToString() : string.Empty,
                            RATINGVALUE = (read["RATINGVALUE"] != DBNull.Value) ? Math.Round(decimal.Parse(read["RATINGVALUE"].ToString()), 1) : defDec,
                            RATIONALE = read["RATIONALE"] != DBNull.Value ? read["RATIONALE"].ToString() : string.Empty,
                            EVIDENCE = read["EVIDENCE"] != DBNull.Value ? read["EVIDENCE"].ToString() : string.Empty,
                            PARENT_ID = read["PARENT_ID"] != DBNull.Value ? int.Parse(read["PARENT_ID"].ToString()) : defInt,
                            HAS_SUB_GOALS = bool.Parse(read["HAS_SUB_GOALS"].ToString())
                        });
                    }
                    return score;
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
        }

        public List<scores> getScoreReport(int criteriaId, int itemValueId, string startDate, string endDate)
        {
            try
            {
                decimal? defDec = null;
                List<scores> score = new List<scores>();
                using (SqlDataReader read = SqlHelper.ExecuteReader(Util.GetconnectionString(), "PROC_GET_SCORES_REPORT_BY_PERIOD", criteriaId, itemValueId, startDate, endDate))
                {
                    while (read.Read())
                    {
                        score.Add(new scores
                        {
                            WEIGHT = (read["WEIGHT"] != DBNull.Value) ? int.Parse(read["WEIGHT"].ToString()) : 0,
                            CRITERIA_TYPE_ID = int.Parse(read["CRITERIA_TYPE_ID"].ToString()),
                            SELECTED_ITEM_VALUE = int.Parse(read["SELECTED_ITEM_VALUE"].ToString()),
                            FINAL_SCORE = Math.Round(double.Parse(read["FINAL_SCORE"].ToString()), 2),
                            FINAL_SCORE_AGREED = (read["FINAL_SCORE_AGREED"] != DBNull.Value) ? Math.Round(decimal.Parse(read["FINAL_SCORE_AGREED"].ToString()), 2) : 0,
                            RATINGVALUE = read["RATINGVALUE"] != DBNull.Value ? Math.Round(decimal.Parse(read["RATINGVALUE"].ToString()), 1) : defDec,
                            UNIT_MEASURE_NAME = read["UNIT_MEASURE"].ToString(),
                            TARGET = read["TARGET"] != DBNull.Value ? read["TARGET"].ToString() : string.Empty,
                            GOAL_DESCRIPTION = read["GOAL_DESCRIPTION"].ToString(),
                            PERSPECTIVE_NAME = read["PERSPECTIVE_NAME"].ToString(),
                            OBJECTIVE = read["OBJECTIVE"].ToString(),
                            PERSPECTIVE_ID = int.Parse(read["PERSPECTIVE_ID"].ToString())
                        });
                    }
                    return score;
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
        }

        public List<scores> getAggregateReport(int criteriaId, string areaValueId, string date)
        {
            try
            {
                decimal? defDec = null;
                int? defInt = null;
                List<scores> score = new List<scores>();
                using (SqlDataReader read = SqlHelper.ExecuteReader(Util.GetconnectionString(), "PROC_GET_SCORES_REPORT_AGGREGATE", criteriaId, areaValueId, date))
                {
                    while (read.Read())
                    {
                        score.Add(new scores
                        {
                            ID = int.Parse(read["ID"].ToString()),
                            CRITERIA_TYPE_ID = int.Parse(read["CRITERIA_TYPE_ID"].ToString()),
                            SELECTED_ITEM_VALUE = int.Parse(read["SELECTED_ITEM_VALUE"].ToString()),
                            GOAL_ID = int.Parse(read["GOAL_ID"].ToString()),
                            SCORES = read["SCORES"] != DBNull.Value ? read["SCORES"].ToString() : string.Empty,
                            FINAL_SCORE = (read["FINAL_SCORE"] != DBNull.Value) ? Math.Round(double.Parse(read["FINAL_SCORE"].ToString()), 2) : 0,
                            FINAL_SCORE_AGREED = (read["FINAL_SCORE_AGREED"] != DBNull.Value) ? Math.Round(decimal.Parse(read["FINAL_SCORE_AGREED"].ToString()), 2) : 0,
                            RATINGVALUE = read["RATINGVALUE"] != DBNull.Value ? decimal.Parse(read["RATINGVALUE"].ToString()) : defDec,
                            DATE_CREATED = DateTime.Parse(read["DATE_CREATED"].ToString()),
                            SUBMISSION_ID = int.Parse(read["SUBMISSION_ID"].ToString()),
                            WEIGHT = read["WEIGHT"] != DBNull.Value ? int.Parse(read["WEIGHT"].ToString()) : 0,
                            UNIT_MEASURE_NAME = read["UNIT_MEASURE_NAME"] != DBNull.Value ? read["UNIT_MEASURE_NAME"].ToString() : string.Empty,
                            TARGET = read["TARGET"] != DBNull.Value ? read["TARGET"].ToString() : string.Empty,
                            GOAL_DESCRIPTION = read["GOAL_DESCRIPTION"].ToString(),
                            PERIOD_DATE = DateTime.Parse(read["PERIOD_DATE"].ToString()),
                            PERSPECTIVE_NAME = read["PERSPECTIVE_NAME"].ToString(),
                            OBJECTIVE = read["OBJECTIVE"].ToString(),
                            PERSPECTIVE_ID = int.Parse(read["PERSPECTIVE_ID"].ToString()),
                            FULL_NAME = (criteriaId == Util.getTypeDefinitionsRolesTypeID()) ? read["NAME"].ToString() : "",
                            USER_ID = (criteriaId == Util.getTypeDefinitionsRolesTypeID()) ? int.Parse(read["USERID"].ToString()) : 0,
                            ORGUNIT_NAME = (criteriaId == Util.getTypeDefinitionsOrgUnitsTypeID()) ? read["ORGUNIT_NAME"].ToString() : "",
                            ORG_ID = (criteriaId == Util.getTypeDefinitionsOrgUnitsTypeID()) ? int.Parse(read["ORGUNIT_ID"].ToString()) : 0,
                            PROJECT_NAME = (criteriaId == Util.getTypeDefinitionsProjectID()) ? read["PROJECT_NAME"].ToString() : "",
                            PROJECT_ID = (criteriaId == Util.getTypeDefinitionsProjectID()) ? int.Parse(read["PROJECT_ID"].ToString()) : 0,
                            PARENT_ID = read["PARENT_ID"] != DBNull.Value ? int.Parse(read["PARENT_ID"].ToString()) : defInt,
                            HAS_SUB_GOALS = bool.Parse(read["HAS_SUB_GOALS"].ToString())
                        });
                    }
                    return score;
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
        }

        public List<scores> getSubmittedScores(submissionWorkflow _flow)
        {
            try
            {
                decimal? defDec = null;
                int? defInt = null;
                List<scores> company = new List<scores>();
                using (SqlDataReader read = SqlHelper.ExecuteReader(Util.GetconnectionString(), "PROC_GET_SCORES_BY_OBJECT", _flow.CRITERIA_TYPE_ID, _flow.SELECTED_ITEM_VALUE, _flow.ID))
                {
                    while (read.Read())
                    {
                        company.Add(new scores
                        {
                            ID = read["ID"] == DBNull.Value ? 0 : int.Parse(read["ID"].ToString()),
                            CRITERIA_TYPE_ID = read["CRITERIA_TYPE_ID"] == DBNull.Value ? 0 : int.Parse(read["CRITERIA_TYPE_ID"].ToString()),
                            SELECTED_ITEM_VALUE = read["SELECTED_ITEM_VALUE"] == DBNull.Value ? 0 : int.Parse(read["SELECTED_ITEM_VALUE"].ToString()),
                            GOAL_ID = read["GOAL_ID"] == DBNull.Value ? 0 : int.Parse(read["GOAL_ID"].ToString()),
                            SCORES = read["SCORES"] == DBNull.Value ? string.Empty : read["SCORES"].ToString(),
                            FINAL_SCORE = (read["FINAL_SCORE"] == DBNull.Value) ? 0 : Math.Round(double.Parse(read["FINAL_SCORE"].ToString()), 2),
                            FINAL_SCORE_AGREED = (read["FINAL_SCORE_AGREED"] != DBNull.Value) ? Math.Round(decimal.Parse(read["FINAL_SCORE_AGREED"].ToString()), 2) : 0,
                            DATE_CREATED = read["DATE_CREATED"] == DBNull.Value ? DateTime.Now : DateTime.Parse(read["DATE_CREATED"].ToString()),
                            SUBMISSION_ID = read["SUBMISSION_ID"] == DBNull.Value ? 0 : int.Parse(read["SUBMISSION_ID"].ToString()),
                            WEIGHT = read["WEIGHT"] == DBNull.Value ? 0 : int.Parse(read["WEIGHT"].ToString()),
                            UNIT_MEASURE = read["UNIT_MEASURE"] == DBNull.Value ? 0 : int.Parse(read["UNIT_MEASURE"].ToString()),
                            UNIT_MEASURE_NAME = read["UNIT_MEASURE_NAME"] == DBNull.Value ? string.Empty : read["UNIT_MEASURE_NAME"].ToString(),
                            TARGET = read["TARGET"] == DBNull.Value ? string.Empty : read["TARGET"].ToString(),
                            GOAL_DESCRIPTION = read["GOAL_DESCRIPTION"] == DBNull.Value ? string.Empty : read["GOAL_DESCRIPTION"].ToString(),
                            PERIOD_DATE = DateTime.Parse(read["PERIOD_DATE"].ToString()),
                            RATINGVALUE = read["RATINGVALUE"] != DBNull.Value ? Math.Round(decimal.Parse(read["RATINGVALUE"].ToString()), 1) : defDec,
                            AGREED_SCORE = read["AGREED_SCORE"] != DBNull.Value ? read["AGREED_SCORE"].ToString() : read["SCORES"].ToString(),
                            MANAGER_SCORE = read["MANAGER_SCORE"] != DBNull.Value ? read["MANAGER_SCORE"].ToString() : read["SCORES"].ToString(),
                            MANAGER_COMMENT = read["MANAGER_COMMENT"] == DBNull.Value ? string.Empty : read["MANAGER_COMMENT"].ToString(),
                            OVERALL_COMMENT = read["OVERALL_COMMENT"] == DBNull.Value ? string.Empty : read["OVERALL_COMMENT"].ToString(),
                            VISIBLE = true,
                            EVIDENCE = read["EVIDENCE"] == DBNull.Value ? string.Empty : read["EVIDENCE"].ToString(),
                            JUSTIFICATION_COMMENT = read["JUSTIFICATION_COMMENT"] == DBNull.Value ? string.Empty : read["JUSTIFICATION_COMMENT"].ToString(),
                            HAS_SUB_GOALS = bool.Parse(read["HAS_SUB_GOALS"].ToString()),
                            PARENT_ID = read["PARENT_ID"] == DBNull.Value ? defInt : int.Parse(read["PARENT_ID"].ToString()),
                            NOT_APPLICABLE = bool.Parse(read["NOT_APPLICABLE"].ToString())
                        });
                    }
                    return company;
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
        }

        public DataTable getSubmittedScoresData(submissionWorkflow _flow)
        {
            return SqlHelper.ExecuteDataset(Util.GetconnectionString(), "PROC_GET_SCORES_BY_OBJECT", _flow.CRITERIA_TYPE_ID, _flow.SELECTED_ITEM_VALUE, _flow.ID).Tables[0];
        }

        public int updateWorkflowComment(submissionWorkflow _flow)
        {
            try
            {
                return (int)SqlHelper.ExecuteScalar(Util.GetconnectionString(), "PROC_UPDATE_SCORECARD_COMMENT",
                    _flow.ID,
                    _flow.OVERALL_COMMENT
                 );
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return 0;
            }
        }

        public int reverseWorkFlow(submissionWorkflow submission)
        {
            try
            {
                return (int)SqlHelper.ExecuteScalar(Util.GetconnectionString(), "PROC_UPDATE_SUBMISSION_REVERSE_STATUS",
                    submission.ID,
                    submission.STATUS
                 );
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return 0;
            }
        }
        public int updateWorkflowStatus(submissionWorkflow _flow)
        {
            try
            {
                return (int)SqlHelper.ExecuteScalar(Util.GetconnectionString(), "PROC_UPDATE_SCORECARD_STATUS",
                    _flow.ID,
                    _flow.STATUS,
                    _flow.OVERALL_COMMENT
                 );
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return 0;
            }
        }

        public int updateWorkflowItem(scores _flow)
        {
            return (int)SqlHelper.ExecuteScalar(Util.GetconnectionString(), "PROC_UPDATE_SCORECARD_ITEMDATA",
                _flow.ID,
                _flow.MANAGER_SCORE,
                _flow.AGREED_SCORE,
                _flow.FINAL_SCORE_AGREED,
                _flow.FINAL_SCORE,
                _flow.MANAGER_COMMENT
             );
        }

        public int updateSubmissionWorkflow(submissionWorkflow _workflow)
        {
            try
            {
                return (int)SqlHelper.ExecuteScalar(Util.GetconnectionString(), "PROC_UPDATE_SUBMISSIONS",
                    _workflow.SELECTED_ITEM_VALUE,
                    _workflow.CRITERIA_TYPE_ID,
                    _workflow.STATUS,
                    _workflow.APPROVER_ID,
                    _workflow.CAPTURER_ID,
                    _workflow.ID
                 );
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return 0;
            }
        }

        public int createSubmissionWorkflow(submissionWorkflow _workflow)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try
            {
                return (int)SqlHelper.ExecuteScalar(con, "PROC_ADD_SUBMISSIONS",
                    _workflow.SELECTED_ITEM_VALUE,
                    _workflow.CRITERIA_TYPE_ID,
                    _workflow.STATUS,
                    _workflow.APPROVER_ID,
                    _workflow.CAPTURER_ID
                 );
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return 0;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public int updateScore(scores _scores)
        {
            try
            {
                return (int)SqlHelper.ExecuteScalar(Util.GetconnectionString(), "PROC_UPDATE_SCORE",
                    _scores.CRITERIA_TYPE_ID,
                    _scores.SELECTED_ITEM_VALUE,
                    _scores.GOAL_ID,
                    _scores.SCORES,
                    _scores.FINAL_SCORE,
                    _scores.SUBMISSION_ID,
                    _scores.PERIOD_DATE,
                    _scores.JUSTIFICATION_COMMENT,
                    _scores.RATINGVALUE,
                    _scores.FINAL_SCORE_AGREED,
                    _scores.EVIDENCE,
                    _scores.ID,
                    _scores.STATUS_ID,
                    _scores.HAS_SUB_GOALS,
                    _scores.PARENT_ID,
                    _scores.UPDATE_MNG_SCORE,
                    _scores.NOT_APPLICABLE
                 );
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return 0;
            }
        }

        public int createNewScore(scores _scores)
        {
            try
            {
                return (int)SqlHelper.ExecuteScalar(Util.GetconnectionString(), "PROC_ADD_SCORES",
                    _scores.CRITERIA_TYPE_ID,
                    _scores.SELECTED_ITEM_VALUE,
                    _scores.GOAL_ID,
                    _scores.SCORES,
                    _scores.FINAL_SCORE,
                    _scores.SUBMISSION_ID,
                    _scores.PERIOD_DATE,
                    _scores.JUSTIFICATION_COMMENT,
                    _scores.RATINGVALUE,
                    _scores.FINAL_SCORE_AGREED,
                    _scores.EVIDENCE,
                    _scores.HAS_SUB_GOALS,
                    _scores.PARENT_ID,
                    _scores.NOT_APPLICABLE
                 );
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return 0;
            }
        }

        public int deleteScoresByGoalID(goals _goal)
        {

            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try
            {
                return SqlHelper.ExecuteNonQuery(con, "PROC_DELETE_ALL_SCORES_BY_GOAL_ID",
                  _goal.ID, _goal.PERSPECTIVE
                 );
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return 0;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public List<submissionWorkflow> getApprovalWorkflowItems(submissionWorkflow _approver)
        {
            try
            {
                List<submissionWorkflow> attachments = new List<submissionWorkflow>();
                using (SqlDataReader read = SqlHelper.ExecuteReader(Util.GetconnectionString(), "PROC_GET_ALL_SUBMISSIONS_BY_APPROVER_ID", _approver.ID))
                {
                    while (read.Read())
                    {
                        attachments.Add(new submissionWorkflow
                        {
                            ID = read.GetInt32(0),
                            SELECTED_ITEM_VALUE = read.GetInt32(1),
                            CRITERIA_TYPE_ID = read.GetInt32(2),
                            APPROVER_ID = read.GetInt32(3),
                            STATUS = read.GetInt32(4),
                            DATE_SUBMITTED = read.GetDateTime(5),
                            COMPANY_ID = read.GetInt32(6)
                        });
                    }
                    return attachments;
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
        }

        public List<submissionWorkflow> getApprovalWorkflowItems()
        {
            try
            {
                List<submissionWorkflow> attachments = new List<submissionWorkflow>();
                using (SqlDataReader read = SqlHelper.ExecuteReader(Util.GetconnectionString(), "PROC_GET_ALL_SUBMISSIONS"))
                {
                    while (read.Read())
                    {
                        attachments.Add(new submissionWorkflow
                        {
                            ID = int.Parse(read["ID"].ToString()),
                            SELECTED_ITEM_VALUE = int.Parse(read["SELECTED_ITEM_VALUE"].ToString()),
                            CRITERIA_TYPE_ID = int.Parse(read["CRITERIA_TYPE_ID"].ToString()),
                            APPROVER_ID = int.Parse(read["APPROVER_ID"].ToString()),
                            STATUS = int.Parse(read["STATUS"].ToString()),
                            DATE_SUBMITTED = DateTime.Parse(read["DATE_SUBMITTED"].ToString()),
                            COMPANY_ID = int.Parse(read["COMPANY_ID"].ToString()),
                            OVERALL_COMMENT = (read["OVERALL_COMMENT"] != DBNull.Value) ? read["OVERALL_COMMENT"].ToString() : string.Empty
                        });
                    }
                    return attachments;
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
        }

        public DataTable getApprovalWorkflowItems(int companyId, int userId, bool isAdmin)
        {
            return SqlHelper.ExecuteDataset(Util.GetconnectionString(), "PROC_GET_ALL_SUBMISSIONS_BY_CRITERIA", companyId, userId, isAdmin).Tables[0];
        }

        public int createAttachment(attachmentDocuments _attachment)
        {
            try
            {
                return SqlHelper.ExecuteNonQuery(Util.GetconnectionString(), "PROC_ADD_ATTACHMENTS",
                  _attachment.ATTACHMENT_NAME, _attachment.SUBMISSION_ID
                 );
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return 0;
            }
        }

        public int deleteSubmittedScore(int submitId)
        {
            try
            {
                return (int)SqlHelper.ExecuteScalar(Util.GetconnectionString(), "PROC_DELETE_SUBMITTED_SCORE", submitId);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return 0;
            }
        }

        public int deleteWorkFlow(int submitId)
        {
            try
            {
                return (int)SqlHelper.ExecuteScalar(Util.GetconnectionString(), "PROC_DELETE_SUBMITTED_WORKFLOW", submitId);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return 0;
            }
        }

        public int updateParentScore(int parentId)
        {
            try
            {
                return (int)SqlHelper.ExecuteScalar(Util.GetconnectionString(), "PROC_UPDATE_PARENT_FINALSCORE", parentId);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return 0;
            }
        }

        public int deleteAttachment(int attachmentId)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try
            {
                return (int)SqlHelper.ExecuteScalar(con, "PROC_DELETE_ATTACHMENT", attachmentId);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return 0;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public List<attachmentDocuments> getAllAttachments(submissionWorkflow _wrkflow)
        {
            try
            {
                List<attachmentDocuments> attachments = new List<attachmentDocuments>();
                using (SqlDataReader read = SqlHelper.ExecuteReader(Util.GetconnectionString(), "PROC_GET_ALL_ATTACHMENTS", _wrkflow.ID))
                {
                    while (read.Read())
                    {
                        attachments.Add(new attachmentDocuments
                        {
                            ID = read.GetInt32(0),
                            ATTACHMENT_NAME = read.GetString(1),
                            SUBMISSION_ID = read.GetInt32(2)
                        });
                    }
                    return attachments;
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
        }

        public totals getSubmittedScoreTotals(submissionWorkflow _flow)
        {
            try
            {
                List<totals> totals = new List<totals>();
                using (SqlDataReader read = SqlHelper.ExecuteReader(Util.GetconnectionString(), "PROC_GET_SCORES_BY_OBJECT_TOTALS", _flow.ID))
                {
                    while (read.Read())
                    {
                        totals.Add(new totals
                        {
                            SUM_FINAL_SCORE = read.GetInt32(0),
                            SUM_WEIGHT = read.GetInt32(1),
                            PERCENTAGE = read.GetInt32(2)
                        });
                    }
                    return totals[0];
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
        }

        public DataTable getSavedAndDeclinedSubmissions(int capturerId)
        {
            return SqlHelper.ExecuteDataset(Util.GetconnectionString(), "Proc_CaptureScores_GetSavedAndDeclined", capturerId).Tables[0];
        }

        #endregion
    }
}