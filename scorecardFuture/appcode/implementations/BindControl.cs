﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.UI;
using Saplin.Controls;
using Geekees.Common.Controls;
using Telerik.Web.UI;

namespace scorecard.implementations
{
    /// <summary>
    /// This class binds Controls to a datasource
    /// </summary>
    public static class BindControl
    {
        #region DataControls Binding Methods

        /// <summary>
        /// Binds Literal
        /// </summary>
        /// <param name="lbl">Literal ID</param>
        /// <param name="msg">Message to bind to the literal</param>
        public static void BindLiteral(Literal lbl, string msg)
        {
            lbl.Text = msg;
        }

        /// <summary>
        /// Binds Web User Control
        /// </summary>
        /// <param name="PlHolder">PlaceHolder</param>
        /// <param name="controlPath">Web User URL</param>
        public static void BindWebUserControlByUrl(PlaceHolder PlHolder, string controlPath)
        {
            PlHolder.Controls.Clear();
            if (controlPath.Length != 0) PlHolder.Controls.Add(new UserControl().LoadControl(controlPath));
        }

        /// <summary>
        /// Binds Gridview
        /// </summary>
        /// <param name="gdv">GridView control name or id</param>
        /// <param name="dataSource">Data to be bound. This can be a DataSet, DataTable or a List</param>
        public static void BindGridView(GridView gdv, object dataSource)
        {
            int rows = 0;
            if (dataSource is DataTable)
            {
                rows = (dataSource as DataTable).Rows.Count;
                if (rows <= 0) (dataSource as DataTable).Rows.Add((dataSource as DataTable).NewRow());
            }

            gdv.ShowHeaderWhenEmpty = true;
            gdv.ShowFooter = true;
            gdv.DataSource = dataSource;
            gdv.DataBind();

            if (rows <= 0) gdv.Rows[0].Visible = false;
        }

        /// <summary>
        /// Binds Gridview
        /// </summary>
        /// <param name="gdv">GridView control name or id</param>
        /// <param name="dataSource">Data to be bound. This can be a DataSet, DataTable or a List</param>
        public static void BindGridViewNormal(GridView gdv, object dataSource)
        {
            gdv.DataSource = dataSource;
            gdv.DataBind();
        }

        /// <summary>
        /// Binds a Repeater
        /// </summary>
        /// <param name="rpt">Repeater control name or id</param>
        /// <param name="dataSource">Data to be bound. This can be a DataSet, DataTable or a List</param>
        public static void BindRepeater(Repeater rpt, object dataSource)
        {
            rpt.DataSource = dataSource;
            rpt.DataBind();
        }

        /// <summary>
        /// Binds a ListView
        /// </summary>
        /// <param name="lstV">ListView conrol name or id</param>
        /// <param name="dataSource">Data to be bound. This can be a DataSet, DataTable or a List</param>
        public static void BindListView(ListView lstV, object dataSource)
        {
            lstV.DataSource = dataSource;
            lstV.DataBind();
        }

        /// <summary>
        /// Binds a ListView
        /// </summary>
        /// <param name="lstV">ListView conrol name or id</param>
        /// <param name="dataKeyNames">Data Key Names</param>
        /// <param name="dataSource">Data to be bound. This can be a DataSet, DataTable or a List</param>
        public static void BindListView(ListView lstV, string[] dataKeyNames, object dataSource)
        {
            lstV.DataKeyNames = dataKeyNames;
            lstV.DataSource = dataSource;
            lstV.DataBind();
        }

        /// <summary>
        /// Binds a DataList
        /// </summary>
        /// <param name="dlst">DataList control name or id</param>
        /// <param name="dataSource">Data to be bound. This can be a DataSet, DataTable or a List</param>
        public static void BindDataList(DataList dlst, object dataSource)
        {
            dlst.DataSource = dataSource;
            dlst.DataBind();
        }

        /// <summary>
        /// Binds a Checkbox List control with no select value text
        /// </summary>
        /// <param name="chk">CheckBoxList ID / Name</param>
        /// <param name="dataTextField">Data Text Field Name</param>
        /// <param name="dataValueField">Data Value Field Name</param>
        /// <param name="dataSource">Data Source</param>
        public static void BindChecklist(CheckBoxList chk, string dataTextField, string dataValueField, object dataSource)
        {
            chk.Items.Clear();
            chk.DataTextField = dataTextField;
            chk.DataValueField = dataValueField;
            chk.DataSource = dataSource;
            chk.DataBind();
        }

        /// <summary>
        /// Binds a dropdown control with no select value text
        /// </summary>
        /// <param name="drpd">Dropdown ID / Name</param>
        /// <param name="dataTextField">Data Text Field Name</param>
        /// <param name="dataValueField">Data Value Field Name</param>
        /// <param name="dataSource">Data Source</param>
        public static void BindDropdown(DropDownList drpd, string dataTextField, string dataValueField, object dataSource)
        {
            drpd.Items.Clear();
            drpd.DataTextField = dataTextField;
            drpd.DataValueField = dataValueField;
            drpd.DataSource = dataSource;
            drpd.DataBind();
        }

        /// <summary>
        /// Binds a dropdown control with no select value text
        /// </summary>
        /// <param name="drpd">Dropdown ID / Name</param>
        /// <param name="dataTextField">Data Text Field Name</param>
        /// <param name="dataValueField">Data Value Field Name</param>
        /// <param name="dataSource">Data Source</param>
        public static void BindDropdown(RadComboBox drpd, string dataTextField, string dataValueField, object dataSource)
        {
            drpd.Items.Clear();
            drpd.DataTextField = dataTextField;
            drpd.DataValueField = dataValueField;
            drpd.DataSource = dataSource;
            drpd.DataBind();
        }

        /// <summary>
        /// Binds a dropdown control with no select value text
        /// </summary>
        /// <param name="drpd">Dropdown ID / Name</param>
        /// <param name="dataTextField">Data Text Field Name</param>
        /// <param name="dataValueField">Data Value Field Name</param>
        /// <param name="dataSource">Data Source</param>
        public static void BindDropdown(DropDownList drpd, string dataTextField, string dataValueField, string defaultValue, object dataSource)
        {
            drpd.Items.Clear();
            drpd.DataTextField = dataTextField;
            drpd.DataValueField = dataValueField;
            drpd.DataSource = dataSource;
            drpd.DataBind();
            drpd.SelectedValue = defaultValue;
        }

        /// <summary>
        /// Binds a dropdown control with no select value text
        /// </summary>
        /// <param name="drpd">Dropdown ID / Name</param>
        ///<param name="startNumber">Starting Number</param>
        ///<param name="selectedVal">The selected number</param>
        ///<param name="span">Number of items to be added</param>
        public static void BindDropdown(DropDownList drpd, int startNumber, int selectedVal, int span)
        {
            drpd.Items.Clear();
            for (int i = 1; i <= span; i++)
            {
                int newVal = startNumber + i;
                bool valSelected = (selectedVal == newVal) ? true : false;
                drpd.Items.Add(new ListItem
                {
                    Value = newVal.ToString(),
                    Text = newVal.ToString(),
                    Selected = valSelected
                });
            }
        }

        /// <summary>
        /// Binds a dropdown control with no select value text
        /// </summary>
        /// <param name="drpd">Dropdown ID / Name</param>
        /// <param name="dataSource">Data Source</param>
        public static void BindDropdown(DropDownList drpd, object dataSource)
        {
            drpd.Items.Clear();
            drpd.DataSource = dataSource;
            drpd.DataBind();
        }

        /// <summary>
        /// Binds a dropdown control with no select value text
        /// </summary>
        /// <param name="drpd">Dropdown ID / Name</param>
        /// <param name="dataSource">Data Source</param>
        public static void BindDropdown(DropDownCheckBoxes drpd, object dataSource)
        {
            drpd.Items.Clear();
            drpd.DataSource = dataSource;
            drpd.DataBind();
        }

        /// <summary>
        /// Binds a dropdown list with the select option
        /// </summary>
        /// <param name="drpd">Drop Down Name / ID</param>
        /// <param name="dataTextField">Data Text Field Name</param>
        /// <param name="dataValueField">Data Value Field Name</param>
        /// <param name="selectText">Select Text to display</param>
        /// <param name="selectedValue">Value attached to the Select Text</param>
        /// <param name="dataSource">Data Source</param>
        public static void BindDropdown(DropDownCheckBoxes drpd, string dataTextField, string dataValueField, string selectText, string selectedValue, object dataSource)
        {
            drpd.Items.Clear();
            drpd.DataTextField = dataTextField;
            drpd.DataValueField = dataValueField;
            drpd.DataSource = dataSource;
            drpd.DataBind();
            if (!string.IsNullOrEmpty(selectText)) drpd.Items.Insert(0, new ListItem(selectText, selectedValue));
        }

        /// <summary>
        /// Binds a dropdown list with the select option
        /// </summary>
        /// <param name="drpd">Drop Down Name / ID</param>
        /// <param name="dataTextField">Data Text Field Name</param>
        /// <param name="dataValueField">Data Value Field Name</param>
        /// <param name="selectText">Select Text to display</param>
        /// <param name="selectedValue">Value attached to the Select Text</param>
        /// <param name="dataSource">Data Source</param>
        public static void BindDropdown(DropDownList drpd, string dataTextField, string dataValueField, string selectText, string selectedValue, object dataSource)
        {
            drpd.Items.Clear();
            drpd.DataTextField = dataTextField;
            drpd.DataValueField = dataValueField;
            drpd.DataSource = dataSource;
            drpd.DataBind();
            if (!string.IsNullOrEmpty(selectText)) drpd.Items.Insert(0, new ListItem(selectText, selectedValue));
        }

        /// <summary>
        /// Binds a list box
        /// </summary>
        /// <param name="lstBx">Listbox Name</param>
        /// <param name="dataTextField">Data Value Field Name</param>
        /// <param name="dataValueField">Datavalue Field Name</param>
        /// <param name="dataSource">Data Source</param>
        public static void BindListBox(ListBox lstBx, string dataTextField, string dataValueField, object dataSource)
        {
            lstBx.DataValueField = dataValueField;
            lstBx.DataTextField = dataTextField;
            lstBx.DataSource = dataSource;
            lstBx.DataBind();
        }

        /// <summary>
        /// Binds a list box
        /// </summary>
        /// <param name="lstBx">Listbox Name</param>
        /// <param name="dataSource">Data Source</param>
        public static void BindListBox(ListBox lstBx, object dataSource)
        {
            lstBx.DataSource = dataSource;
            lstBx.DataBind();
        }

        /// <summary>
        /// Binds ASP.NET Tree View
        /// </summary>
        /// <param name="aspTree">ASP.NET Tree View Name / ID</param>
        /// <param name="dataSource">Data Source</param>
        public static void BindTree(TreeView aspTree, object dataSource)
        {
            aspTree.DataSource = dataSource;
            aspTree.DataBind();
        }

        /// <summary>
        /// Binds ASP.NET Tree View
        /// </summary>
        /// <param name="aspTree">ASP.NET Tree View Name / ID</param>
        /// <param name="dataSource">Data Source</param>
        public static void BindTree(ASTreeView aspTree, object dataSource)
        {
            ASTreeViewDataTableColumnDescriptor descripter = new ASTreeViewDataTableColumnDescriptor("ORGUNIT_NAME", "ID", "PARENT_ORG");
            aspTree.ForceRenderInitialScript();
            aspTree.DataSourceDescriptor = descripter;
            aspTree.DataSource = dataSource;
            aspTree.DataBind();
        }

        /// <summary>
        /// Binds ASP.NET Menu Control
        /// </summary>
        /// <param name="aspTree">ASP.NET Tree View Name / ID</param>
        /// <param name="dataSource">Data Source</param>
        public static void BindMenu(Menu aspMenu, object dataSource)
        {
            aspMenu.DataSource = dataSource;
            aspMenu.DataBind();
        }

        /// <summary>
        /// Binds multiple selection drop down list
        /// </summary>
        /// <param name="drpd">DropDown ID</param>
        /// <param name="dataTextField">Data Text Field </param>
        /// <param name="dataValueField">Data Value Field</param>
        /// <param name="dataSource">Data Source</param>
        public static void BindDropdown(DropDownCheckBoxes drpd, string dataTextField, string dataValueField, object dataSource)
        {
            drpd.Items.Clear();
            drpd.DataTextField = dataTextField;
            drpd.DataValueField = dataValueField;
            drpd.DataSource = dataSource;
            drpd.DataBind();
        }

        #endregion
    }
}