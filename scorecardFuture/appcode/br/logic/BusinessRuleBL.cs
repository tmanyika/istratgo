﻿using System;
using scorecard.entities;
using scorecard.interfaces;
using scorecard.implementations;
using System.Collections.Generic;

namespace Scorecard.Business.Rules
{
    public class BusinessRuleBL : IBusinessRule
    {
        Icompany comp;
        const decimal PERCENT = 100;

        public BusinessRuleBL()
        {
            comp = new companyImpl();
        }

        public bool SubscriptionValid(int companyId)
        {
            company obj = comp.getCompanyInfo(new company { COMPANY_ID = companyId });

            int trialPeriod = Util.getTrialPeriodDays();
            double lapsedPeriod = DateTime.Now.Subtract(obj.DATE_REGISTERED).TotalDays;
            bool periodLapsed = lapsedPeriod > trialPeriod ? true : false;
            return (obj.PAID || !periodLapsed) ? true : false;
        }

        private bool TargetDivScoreTimesWeight(int unitOfMeasure)
        {
            string[] measures = Util.TargetDivScoreTimesWeight.Split(',');
            foreach (string strval in measures)
                if (strval.Equals(unitOfMeasure.ToString()))
                    return true;

            return false;
        }

        private bool ScoreDivTargetTimesWeight(int unitOfMeasure)
        {
            string[] measures = Util.TargetDivScoreTimesWeight.Split(',');
            foreach (string strval in measures)
                if (strval.Equals(unitOfMeasure.ToString()))
                    return true;

            return false;
        }

        public bool UnitOfMeasureRequireNumericValue(int unitOfMeasure)
        {
            string[] Ids = Util.UnitOfMeasureRequireNumericIDs.Split(',');
            foreach (string strval in Ids)
                if (strval.Equals(unitOfMeasure.ToString()))
                    return true;

            return false;
        }

        public bool NotApplicableExpectedValueValid(string score)
        {
            if (!Util.NotApplicableAllowed) return false;

            string[] Ids = Util.NotApplicableExpectedValues.Split(',');
            foreach (string strval in Ids)
                if (string.Compare(strval.ToLower(), score.ToLower(), true) == 0)
                    return true;

            return false;
        }

        public bool UnitOfMeasureRequireYesNoValueValid(string score)
        {
            string[] Ids = Util.UnitOfMeasureYesNoExpectedValues.Split(',');
            foreach (string strval in Ids)
                if (string.Compare(strval.ToLower(), score.ToLower(), true) == 0)
                    return true;

            return false;
        }

        public bool UnitOfMeasureRequireYesNoValue(int unitOfMeasure)
        {
            string[] measures = Util.UnitOfMeasureRequireYesNoValues.Split(',');
            foreach (string strval in measures)
                if (strval.Equals(unitOfMeasure.ToString()))
                    return true;

            return false;
        }

        public decimal FinalScore(int unitOfMeasure, decimal weight, decimal score, decimal target)
        {
            decimal result = 0;
            if (TargetDivScoreTimesWeight(unitOfMeasure))
                result = ((target / score) * 100m); // target/score*weight
            else if (ScoreDivTargetTimesWeight(unitOfMeasure))
                result = ((score / target) * 100m); // score/target*weight
            else //existing units
                result = ((score / target) * 100m); // score/target*weight
            decimal answer = ((result / 100m) * weight);
            return Math.Round(answer, 2);
        }

        public decimal? GetScoringRate(List<ScoreRating> data, decimal finalscore, decimal? maxValue, decimal? maxRating, bool applyproportion)
        {
            decimal? rating = null;
            decimal percentScore = finalscore;

            if (percentScore <= 0) return 0;
            if (percentScore > maxValue) return maxRating;

            foreach (ScoreRating item in data)
            {
                if (percentScore >= item.MinimumValue && percentScore <= item.MaximumValue)
                {
                    rating = item.RatingValue;
                    break;
                }
            }

            decimal? propRate = null;

            if (applyproportion && rating != null)
                propRate = (finalscore > PERCENT) ? rating : (decimal)rating * (finalscore / PERCENT);
            else propRate = rating;

            return (propRate != null) ? Math.Round((decimal)propRate, 2) : propRate;
        }

        public decimal? GetScoringRate(List<ScoreRating> data, decimal weight, decimal finalscore, decimal? maxValue, decimal? maxRating)
        {
            decimal? rating = null;
            decimal percentScore = (weight == 0) ? 0 : (finalscore / weight) * PERCENT;

            if (percentScore <= 0) return 0;
            if (percentScore > maxValue) return maxRating;

            foreach (ScoreRating item in data)
            {
                if (percentScore >= item.MinimumValue && percentScore <= item.MaximumValue)
                {
                    rating = item.RatingValue;
                    break;
                }
            }
            return (rating == null) ? Math.Round((decimal)rating, 2) : rating;
        }

        public decimal GetDefinedWeightedScore(decimal finalScore, decimal ratingVal)
        {
            decimal weightedDefinedScore = (finalScore / PERCENT) * ratingVal;
            return Math.Round(weightedDefinedScore, 2);
        }

        public void GetMaxValues(List<ScoreRating> data, out decimal? maxRate, out decimal? maxValue)
        {
            maxRate = null;
            maxValue = null;
            foreach (ScoreRating item in data)
            {
                if (maxValue < item.MaximumValue)
                {
                    maxValue = item.MaximumValue;
                    maxRate = item.RatingValue;
                }
            }
        }
    }
}