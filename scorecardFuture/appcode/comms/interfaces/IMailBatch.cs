﻿using System;
using System.Collections.Generic;
using HR.Human.Resources;

namespace System.Email.Communication
{
    public interface IMailBatch
    {
        bool AddMailingList(int batchId, string mailingList);
        int AddMailBatch(MailBatch obj);
        string GetRoleList(int batchId);

        MailBatch GetById(int batchId);
        List<Employee> GetRecipients(int batchId);
        List<MailBatch> GetAll();
    }
}
