﻿using System;
using System.Collections.Generic;
using HR.Human.Resources;

namespace System.Email.Communication
{
    public interface IMailRelay
    {
        bool SendMail(MailRelay obj, out string result);
        bool SendMail(int batchId, bool recordTrail, out string result);
        
        MailRelay GetBatchMail(int batchId);
    }
}
