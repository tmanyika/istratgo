﻿using System;
using System.Collections.Generic;
using System.Email.Communication;

namespace System.Email.Communication
{
    public interface IMailTemplate
    {
        bool AddMailTemplate(MailTemplate obj);
        bool UpdateMailTemplate(MailTemplate obj);
        bool Delete(int mailId, string updatedBy);
        
        MailTemplate GetById(int mailId);
        List<MailTemplate> GetByStatus(bool active);
        List<MailTemplate> GetMasterTemplates(bool active, bool ismaster);
        List<MailTemplate> GetAll();
    }
}
