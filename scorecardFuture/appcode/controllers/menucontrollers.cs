﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using scorecard.interfaces;
using scorecard.entities;
using scorecard.implementations;
using HierarchicalData;

/* @Copy Albertoncaffeine.net */

namespace scorecard.controllers
{
    public class menucontroller
    {

        Irolebased rolebased;
        public settingsmanager appsettings;

        public menucontroller(Irolebased _rolebased)
        {
            rolebased = _rolebased;
            appsettings = new settingsmanager(new applicationsettingsImpl());
        }
        public int createAccess(menuaccess _menu)
        {
            return rolebased.createAccess(_menu);
        }
        public int deleteAccessByCompany(menuaccess _menu)
        {
            return rolebased.deleteAccessByCompany(_menu);
        }

        public List<menu> getAllMenus()
        {
            return rolebased.getAllMenus();
        }

        public HierarchicalDataSet getAccess(int companyId, int userRole)
        {
            return rolebased.getAccess(companyId, userRole);
        }

        public List<menuaccess> getAccessByCompany(company _company)
        {
            return rolebased.getAccessByCompany(_company);
        }
    }
}