﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
/* @Copy Albertoncaffeine.net */
namespace scorecard.entities
{
    public class applicationsettings
    {
        public int TYPE_ID { get; set; }
        public int PARENT_ID { get; set; }
        public string NAME { get; set; }
        public string DESCRIPTION { get; set; }
        public DateTime DATE_CREATED { get; set; }
        public bool ACTIVE { get; set; }
    }
}