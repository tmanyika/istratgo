﻿using System;
using System.Collections.Generic;
using System.Data;

namespace HR.Leave
{
    public interface ILeaveAccrualTime
    {
        bool AddLeaveAccrualTime(LeaveAccrualTime obj);
        bool UpdateLeaveAccrualTime(LeaveAccrualTime obj);
        bool Delete(int accrualTimeId, string updatedBy);

        LeaveAccrualTime GetById(int accrualTimeId);
        LeaveAccrualTime GetRecord(DataRow rw);
        List<LeaveAccrualTime> GetByStatus(bool active);
        List<LeaveAccrualTime> GetAll();
    }
}
