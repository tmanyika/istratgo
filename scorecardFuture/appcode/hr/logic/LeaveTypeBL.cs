﻿using System;
using System.Collections.Generic;
using System.Data;
using scorecard.implementations;

namespace HR.Leave
{
    public class LeaveTypeBL : ILeaveType
    {
        #region Properties

        public string ConnectionString
        {
            get { return Util.GetconnectionString(); }
        }

        public LeaveTypeDAL Dal
        {
            get { return new LeaveTypeDAL(ConnectionString); }
        }

        #endregion

        #region Default Constructors

        public LeaveTypeBL() { }

        #endregion

        #region Methods & Functions

        public bool AddLeaveType(LeaveType obj)
        {
            int result = Dal.Save(obj);
            return result > 0 ? true : false;
        }

        public bool UpdateLeaveType(LeaveType obj)
        {
            int result = Dal.Update(obj);
            return result > 0 ? true : false;
        }

        public bool Delete(int leaveId, string updatedBy)
        {
            int result = Dal.Deactivate(leaveId, updatedBy);
            return result > 0 ? true : false;
        }

        public List<LeaveType> GetByStatus(int companyId, bool active)
        {
            List<LeaveType> obj = new List<LeaveType>();
            DataTable dT = Dal.Get(companyId, active);
            if (dT.Rows.Count <= 0) return obj;

            foreach (DataRow rw in dT.Rows)
                obj.Add(GetRecord(rw));
            return obj;
        }

        public List<LeaveType> GetByCompanyId(int companyId)
        {
            List<LeaveType> obj = new List<LeaveType>();
            DataTable dT = Dal.Get(companyId);
            if (dT.Rows.Count <= 0) return obj;

            foreach (DataRow rw in dT.Rows)
                obj.Add(GetRecord(rw));
            return obj;
        }

        public LeaveType GetById(int leaveId)
        {
            DataTable dT = Dal.GetById(leaveId);
            if (dT.Rows.Count <= 0) return new LeaveType();
            DataRow rw = dT.Rows[0];
            return GetRecord(rw);
        }

        public LeaveType GetRecord(DataRow rw)
        {
            DateTime? defDate = null;
            return new LeaveType
            {
                Active = bool.Parse(rw["active"].ToString()),
                CompanyId = int.Parse(rw["companyId"].ToString()),
                CreatedBy = rw["createdby"].ToString(),
                DateCreated = DateTime.Parse(rw["datecreated"].ToString()),
                DateUpdated = (rw["dateupdated"] != DBNull.Value) ? DateTime.Parse(rw["dateupdated"].ToString()) : defDate,
                Description = (rw["leaveDescription"] != DBNull.Value) ? rw["leaveDescription"].ToString() : string.Empty,
                LeaveId = int.Parse(rw["leaveId"].ToString()),
                LeaveName = rw["leaveName"].ToString(),
                UpdatedBy = (rw["updatedby"] != DBNull.Value) ? rw["updatedby"].ToString() : string.Empty
            };
        }

        #endregion
    }
}