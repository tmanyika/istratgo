﻿using System;
using System.Collections.Generic;
using scorecard.implementations;
using System.Data;
using Universe.Nations;

namespace HR.Leave
{
    public class LeaveCalendarBL : ILeaveCalendar
    {
        #region Properties

        public string ConnectionString
        {
            get { return Util.GetconnectionString(); }
        }

        public LeaveCalendarDAL Dal
        {
            get { return new LeaveCalendarDAL(ConnectionString); }
        }

        #endregion

        #region Default Class Constructors

        public LeaveCalendarBL() { }

        #endregion

        #region Methods & Functions

        public bool AddLeaveCalendar(LeaveCalendar obj)
        {
            int result = Dal.Save(obj);
            return result > 0 ? true : false;
        }

        public bool UpdateLeaveCalendar(LeaveCalendar obj)
        {
            int result = Dal.Update(obj);
            return result > 0 ? true : false;
        }

        public bool ApplyLeaveCalendar(LeaveCalendar obj, int orgUnitId)
        {
            int result = Dal.UpdateLeave(obj, orgUnitId);
            return result > 0 ? true : false;
        }

        public bool Delete(int holidayDateId, string updatedBy)
        {
            int result = Dal.Deactivate(holidayDateId, updatedBy);
            return result > 0 ? true : false;
        }

        public List<LeaveCalendar> GetAppliedHolidays(int orgUnitId, int calendarYear)
        {
            List<LeaveCalendar> obj = new List<LeaveCalendar>();
            DataTable dT = Dal.Get(orgUnitId, calendarYear);
            if (dT.Rows.Count <= 0) return obj;
            foreach (DataRow rw in dT.Rows)
                obj.Add(GetCompCalendarRecord(rw));
            return obj;
        }

        public List<LeaveCalendar> GetByStatus(int countryId, int calendarYear, bool status)
        {
            List<LeaveCalendar> obj = new List<LeaveCalendar>();
            DataTable dT = Dal.Get(countryId, calendarYear, status);
            if (dT.Rows.Count <= 0) return obj;
            foreach (DataRow rw in dT.Rows)
                obj.Add(GetRecord(rw));
            return obj;
        }

        public LeaveCalendar GetById(int holidayDateId)
        {
            DataTable dT = Dal.Get(holidayDateId);
            if (dT.Rows.Count <= 0) return new LeaveCalendar();
            DataRow rw = dT.Rows[0];
            return GetRecord(rw);
        }

        public int GetNumberOfHolidays(DateTime sDate, DateTime eDate, int countryId)
        {
            return Dal.GetHolidayDays(sDate, eDate, countryId);
        }

        public LeaveCalendar GetCompCalendarRecord(DataRow rw)
        {
            DateTime? defDate = null;
            return new LeaveCalendar
            {
                LinkedId = int.Parse(rw["linkedId"].ToString()),
                CreatedBy = rw["createdby"].ToString(),
                DateCreated = DateTime.Parse(rw["datecreated"].ToString()),
                DateUpdated = (rw["dateupdated"] != DBNull.Value) ? DateTime.Parse(rw["dateupdated"].ToString()) : defDate,
                HolidayDateId = Int64.Parse(rw["holidayDateId"].ToString()),
                UpdatedBy = (rw["updatedby"] != DBNull.Value) ? rw["updatedby"].ToString() : string.Empty,
                HolidayDate = DateTime.Parse(rw["HolidayDate"].ToString()),
                CalendarYear = int.Parse(rw["calendarYear"].ToString()),
                Applied = (rw["Applied"] == DBNull.Value) ? false : bool.Parse(rw["Applied"].ToString())
            };
        }

        public LeaveCalendar GetRecord(DataRow rw)
        {
            DateTime? defDate = null;
            return new LeaveCalendar
            {
                Active = bool.Parse(rw["active"].ToString()),
                CountryId = int.Parse(rw["countryId"].ToString()),
                CreatedBy = rw["createdby"].ToString(),
                DateCreated = DateTime.Parse(rw["datecreated"].ToString()),
                DateUpdated = (rw["dateupdated"] != DBNull.Value) ? DateTime.Parse(rw["dateupdated"].ToString()) : defDate,
                HolidayDateId = Int64.Parse(rw["holidayDateId"].ToString()),
                Detail = rw["detail"].ToString(),
                UpdatedBy = (rw["updatedby"] != DBNull.Value) ? rw["updatedby"].ToString() : string.Empty,
                HolidayDate = DateTime.Parse(rw["HolidayDate"].ToString()),
                CalendarYear = int.Parse(rw["calendarYear"].ToString()),
                Nation = new Country
                {
                    CountryId = int.Parse(rw["countryId"].ToString()),
                    CountryName = rw["countryName"].ToString()
                }
            };
        }

        #endregion
    }
}