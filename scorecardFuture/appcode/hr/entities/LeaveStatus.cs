﻿using System;

namespace HR.Leave
{
    public class LeaveStatus
    {
        public int StatusId { get; set; }

        public string StatusName { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public bool Active { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
    }
}