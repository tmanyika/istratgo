﻿using System;

namespace HR.Leave.Documents
{
    public class LeaveApplicationComment
    {
        public int CommentId { get; set; }
        public int LeaveApplicationId { get; set; }

        public bool Valid { get; set; }

        public string DocumentTitle { get; set; }
        public string Comment { get; set; }
        public string SupportingDoc { get; set; }
        public string UploadedBy { get; set; }
        public string UpdatedBy { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
    }
}