﻿using System;

namespace HR.Leave
{
    public class LeaveAccrual
    {
        public int LeaveAccrualId { get; set; }
        public int LeaveId { get; set; }
        public int WorkingWeekId { get; set; }
        public int AccrualTimeId { get; set; }

        public decimal AccrualRate { get; set; }
        public decimal MaximumThreshold { get; set; }

        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public bool Active { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }

        public LeaveType Leave { get; set; }
        public WorkingWeek WorkWeek { get; set; }
        public LeaveAccrualTime TimeAccrual { get; set; }
    }
}