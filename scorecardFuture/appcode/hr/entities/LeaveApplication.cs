﻿using System;
using HR.Human.Resources;
using scorecard.entities;

namespace HR.Leave
{
    public class LeaveApplication
    {
        public int LeaveApplicationId { get; set; }
        public int EmployeeId { get; set; }
        public int LeaveId { get; set; }
        public int StatusId { get; set; }
        public int NoOfLeaveDays { get; set; }
        public int SubmittedEmployeeId { get; set; }
        public int? ApprovedByEmployeeId { get; set; }

        public string LeaveNotes { get; set; }
        public string RejectReason { get; set; }

        public object Docs { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public Employee Staff { get; set; }
        public LeaveType Leave { get; set; }
        public JobTitle Position { get; set; }
        public LeaveStatus Status { get; set; }
    }
}