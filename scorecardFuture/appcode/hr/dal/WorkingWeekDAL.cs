﻿using System;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

namespace HR.Leave
{
    public class WorkingWeekDAL
    {
        #region Variables

        private string _conString;

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return _conString; }
            set { _conString = value; }
        }

        #endregion

        #region Default Constructors

        public WorkingWeekDAL(string conString)
        {
            _conString = conString;
        }

        #endregion

        #region Public Methods and Functions

        public DataTable Get(int companyId)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_WorkingWeek_GetByCompanyId", companyId).Tables[0];
        }

        public DataTable Get(int companyId, bool active)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_WorkingWeek_GetActive", companyId, active).Tables[0];
        }

        public DataTable GetById(int workingWeekId)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_WorkingWeek_GetById", workingWeekId).Tables[0];
        }

        public int Save(WorkingWeek obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_WorkingWeek_Insert", obj.CompanyId,
                                                        obj.Name, obj.NoOfDays, obj.Active, obj.CreatedBy);
            return result;
        }

        public int Update(WorkingWeek obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_WorkingWeek_Update", obj.WorkingWeekId, obj.Name, 
                                                        obj.NoOfDays, obj.Active, obj.UpdatedBy);
            return result;
        }

        public int Deactivate(int workingWeekId, string updatedBy)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_WorkingWeek_Deactivate", workingWeekId, updatedBy);
            return result;
        }

        #endregion
    }
}