﻿using System;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

namespace HR.Leave
{
    public class LeaveApplicationDAL
    {
        #region Variables

        private string _conString;

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return _conString; }
            set { _conString = value; }
        }

        #endregion

        #region Default Constructors

        public LeaveApplicationDAL(string conString)
        {
            _conString = conString;
        }

        #endregion

        #region Public Methods & Functions

        public int Save(LeaveApplication obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_LeaveApplication_Insert", obj.EmployeeId, obj.LeaveId,
                                                        obj.LeaveNotes, obj.StatusId, obj.StartDate, obj.EndDate, obj.NoOfLeaveDays,
                                                        obj.SubmittedEmployeeId, obj.Docs);
            return result;
        }

        public int Update(LeaveApplication obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_LeaveApplication_Update", obj.LeaveApplicationId, obj.EmployeeId,
                                                        obj.LeaveId, obj.LeaveNotes, obj.StatusId, obj.StartDate, obj.EndDate, obj.NoOfLeaveDays,
                                                        obj.SubmittedEmployeeId, obj.RejectReason);
            return result;
        }

        public int UpdateStatus(LeaveApplication obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_LeaveApplication_UpdateStatus", obj.LeaveApplicationId, obj.StatusId,
                                                        obj.ApprovedByEmployeeId, obj.RejectReason);
            return result;
        }

        public DataTable GetById(int applicationId)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_LeaveApplication_GetById", applicationId).Tables[0];
        }

        public DataTable GetByStatusId(int statusId)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_LeaveApplication_GetByStatusId", statusId).Tables[0];
        }

        public DataTable GetByApprover(int approverId)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_LeaveApplication_GetByApproverId", approverId).Tables[0];
        }

        public DataTable GetByApplicant(int applicantId)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_LeaveApplication_GetByApplicantId", applicantId).Tables[0];
        }

        public DataTable GetByCompany(int companyId)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_LeaveApplication_GetByCompanyId", companyId).Tables[0];
        }

        public DataTable Get(int approverId, int statusId)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_LeaveApplication_GetSubmittedLeave", approverId, statusId).Tables[0];
        }

        public object Validate(LeaveApplication obj)
        {
            return SqlHelper.ExecuteScalar(ConnectionString, "Proc_LeaveApplication_IsValid", obj.LeaveApplicationId, obj.EmployeeId, obj.StartDate, obj.EndDate);
        }

        #endregion
    }
}