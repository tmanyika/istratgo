﻿using System;
using System.Collections.Generic;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

namespace HR.Leave.Documents
{
    public class LeaveApplicationCommentDAL
    {
        #region Variables

        private string _conString;

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return _conString; }
            set { _conString = value; }
        }

        #endregion

        #region Default Constructors

        public LeaveApplicationCommentDAL(string conString)
        {
            _conString = conString;
        }

        #endregion

        #region Methods & Functions

        public DataTable Get(int applicationId)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_LeaveApplicationComment_Get", applicationId).Tables[0];
        }

        public DataTable GetById(int commentId)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_LeaveApplicationComment_GetById", commentId).Tables[0];
        }

        public int Delete(int commentId, string updatedBy)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_LeaveApplicationComment_Delete", commentId, updatedBy);
            return result;
        }

        public int Save(LeaveApplicationComment obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_LeaveApplicationComment_Insert", obj.LeaveApplicationId,
                obj.DocumentTitle, obj.Comment, obj.SupportingDoc, obj.Valid, obj.UploadedBy);
            return result;
        }

        #endregion
    }
}