﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using scorecard.entities;
using System.Data;

/* @Copy Albertoncaffeine.net */
namespace scorecard.interfaces
{
    public interface Igoals
    {
        int defineStrategicGoal(strategic _goalDef);
        int updateStrategicGoal(strategic _goalDef);
        int deleteStrategicGoal(strategic _goalDef);
        int addEnvironmentContext(environmentcontext obj);

        environmentcontext getEnvironmentContext(environmentcontext obj);
        List<strategic> listStrategicGoals(company _company);
        DataTable listGeneralGoals(int companyId, int areaTypeId, int areaId);
        strategic getStrategicInfo(int strategicId);

        int defineGeneralGoal(goals _goalDef);
        int defineSubGeneralGoal(goals _goalDef);
        int updateGeneralGoal(goals _goalDef);
        int updateSubGeneralGoal(goals _goalDef);
        int deleteGeneralGoal(goals _goalDef);

        List<goals> listGeneralGoals(company _company);
        goals getGoal(int id);

        
    }
}