﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using scorecard.entities;

/* @Copy Albertoncaffeine.net */

namespace scorecard.interfaces
{
    public interface Isettings
    {
        List<applicationsettings> getParentSettings();
        List<applicationsettings> getChildSettings(applicationsettings appSetting);
        int AddNewSettings(applicationsettings appSetting);
        int DeleteSettings(applicationsettings appSetting);
        int UpdateSetting(applicationsettings appSetting);
        applicationsettings getApplicationSetting(applicationsettings appSetting);
    }
}