﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using scorecard.entities;

/* @Copy Albertoncaffeine.net */

namespace scorecard.interfaces
{
    public interface Iauth
    {
        useradmin loginUser(useradmin user, token _grantedToken);
        useradmin resetPassword(useradmin user);
    }
}