﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="graphComparisonForm.ascx.cs"
    Inherits="scorecard.ui.controls.graphComparisonForm" %>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="cc1" %>
<style type="text/css">
    label
    {
        width: 220px !important;
    }
</style>
<asp:UpdatePanel ID="UpdatePanelScoreReport" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div>
            <h1>
                &nbsp;Scorecard Comparison Graphical Reporting
            </h1>
        </div>
        <div>
            <fieldset runat="server" id="pnlContactInfo">
                <legend>Comparison Report Criteria </legend>
                <div class="clr">
                    <div class="divFloatLeft">
                        <label for="sf">
                            Report Name:
                        </label>
                    </div>
                    <div class="divFloatLeft">
                        <span class="field_desc">
                            <asp:DropDownList ID="ddRepName" runat="server">
                                <asp:ListItem Value="actualvstargetcomp.aspx">Actual vs Target - Bar Graph</asp:ListItem>
                            </asp:DropDownList>
                        </span>
                    </div>
                </div>
                <div class="clr">
                    <div class="divFloatLeft">
                        <label for="sf">
                            Select Area:
                        </label>
                    </div>
                    <div class="divFloatLeft">
                        <span class="field_desc">
                            <asp:DropDownList runat="server" ID="ddlArea" AutoPostBack="True" OnSelectedIndexChanged="ddlArea_SelectedIndexChanged"
                                ValidationGroup="Form" />
                            <asp:RequiredFieldValidator ID="rqdArea" runat="server" ControlToValidate="ddlArea"
                                Display="Dynamic" ErrorMessage="area is required" InitialValue="0" ValidationGroup="Form"
                                ForeColor="Red"></asp:RequiredFieldValidator>
                        </span>
                    </div>
                </div>
                <div class="clr">
                    <div class="divFloatLeft">
                        <label for="sf">
                            Select Value:
                        </label>
                    </div>
                    <div class="divFloatLeft">
                        <span class="field_desc">
                            <cc1:DropDownCheckBoxes ID="ddlValueType" runat="server" UseSelectAllNode="true"
                                UseButtons="true" Width="250px" AutoPostBack="True" OnSelectedIndexChanged="ddlValueTypes_SelectedIndexChanged">
                                <Texts OkButton="Yes" CancelButton="No" SelectAllNode="select all" SelectBoxCaption=" - select an option -" />
                            </cc1:DropDownCheckBoxes>
                        </span>
                    </div>
                </div>
                <div class="clr">
                    <div class="divFloatLeft">
                        <label for="sf">
                            Report Date:
                        </label>
                    </div>
                    <div class="divFloatLeft">
                        <span class="field_desc">
                            <asp:DropDownList ID="ddDate" runat="server" ValidationGroup="Form" />
                            <asp:RequiredFieldValidator ID="rdqDate" runat="server" ControlToValidate="ddDate"
                                Display="Dynamic" ErrorMessage="select date" ForeColor="Red" InitialValue="0"
                                ValidationGroup="Form"></asp:RequiredFieldValidator>
                        </span>
                    </div>
                </div>
                <div class="errorMsg">
                    <label for="lblMsg">
                    </label>
                    <asp:Literal ID="lblMsg" runat="server"></asp:Literal>
                </div>
                <div class="clr">
                    <br />
                </div>
                <div class="clr">
                    <label for="btnViewReport">
                        &nbsp;
                    </label>
                    <asp:Button ID="btnViewReport" runat="server" CssClass="button tooltip" Text="View Report"
                        ValidationGroup="Form" CausesValidation="true" OnClick="btnViewReport_Click" />
                </div>
            </fieldset>
        </div>
        <asp:UpdateProgress ID="UpdateProgressScoreReport" runat="server" AssociatedUpdatePanelID="UpdatePanelScoreReport">
            <ProgressTemplate>
                <img runat="server" src="~/ui/images/icons/activity.gif" alt="Please wait..." id="imgPrg" />
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
