﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="uploadFile.ascx.cs"
    Inherits="scorecard.ui.controls.bulkupload.uploadFile" %>
<div>
    <table cellpadding="2" cellspacing="0" border="0" class="helpTable" width="700px">
        <tr>
            <td colspan="2">
                <h1>
                    Upload Bulk Data
                </h1>
            </td>
        </tr>
        <tr>
            <td style="height: 20px" colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <b>A. Steps To Upload Bulk Data</b><br />
                <br />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div>
                    <div>
                        <a href="templates/DataTemplate.xlsx" target="_blank"><span style="color: Blue">Click
                            Here</span></a> to download data template<br />
                        <br />
                        Fill in the following sheets in the template data file downloaded from the link
                        above:
                        <br />
                        <br />
                    </div>
                    <div>
                        <asp:Repeater ID="rptSheets" runat="server">
                            <ItemTemplate>
                                <div>
                                    <%# Eval("OrderNumber")%>.
                                    <%# Eval("SheetName") %>
                                    (<em style=""><%# Eval("SheetNotes") %></em>)
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                    <div>
                        <br />
                    </div>
                    <div>
                        Upload the completed data file using the upload section below:</div>
                </div>
            </td>
        </tr>
        <tr>
            <td style="height: 30px" colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <p>
                    Locate Document (<span style="color: Red"><i>only *.xlsx or *.xls files are allowed.
                        Maximum File Size <b>10MB</b></i></span>)
                </p>
            </td>
        </tr>
        <tr>
            <td style="height: 30px" colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <telerik:RadUpload ID="RdUpload" runat="server" Skin="Web20" AllowedFileExtensions="xlsx,xls"
                    ControlObjectsVisibility="None" InputSize="40" OverwriteExistingFiles="True"
                    MaxFileSize="10485760">
                </telerik:RadUpload>
            </td>
        </tr>
        <tr>
            <td style="height: 30px" colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Button ID="btnNew" runat="server" class="button tooltip" OnClick="btnNew_Click"
                    Text="Upload Data" title="Upload Data" />&nbsp;
                <asp:Button ID="lnkDone" runat="server" class="button tooltip" title="Go To Home Screen"
                    Text="Go To Home Screen" CausesValidation="false" PostBackUrl="~/ui/index.aspx">
                </asp:Button>
            </td>
        </tr>
        <tr>
            <td style="height: 30px" colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <span style="color: Red">
                    <asp:Literal ID="lblError" runat="server"></asp:Literal></span>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Repeater ID="rptInvalidObjectives" runat="server">
                    <HeaderTemplate>
                        <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                            <thead>
                                <tr>
                                    <td>
                                        Strategic Objective
                                    </td>
                                    <td>
                                        Key Focus Area
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <%# Eval("StrategicObjective")%>
                            </td>
                            <td>
                                <%# Eval("KeyFocusArea")%>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        <tr>
                            <td colspan="2" style="color: Red">
                                <b>NB</b>: You cannot have one (1) Strategic Objective linked to many Key Focus
                                Areas.<br />
                                Please correct your performance measures on Strategic Objectives listed above before
                                you upload again
                            </td>
                        </tr>
                        </tbody> </table></FooterTemplate>
                </asp:Repeater>
            </td>
        </tr>
    </table>
</div>
