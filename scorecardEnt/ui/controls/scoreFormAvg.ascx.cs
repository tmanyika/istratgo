﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.implementations;
using scorecard.controllers;
using scorecard.entities;
using HR.Human.Resources;

namespace scorecard.ui.controls
{
    public partial class scoreFormAvg : System.Web.UI.UserControl
    {
        #region  Variables

        useradmincontroller usersDef;
        goaldefinition goalDef;
        scoresmanager scoresManager;

        #endregion

        #region Default Class Constructors

        public scoreFormAvg()
        {
            usersDef = new useradmincontroller(new useradminImpl());
            goalDef = new goaldefinition(new goalsImpl());
            scoresManager = new scoresmanager(new scorecard.implementations.scoresImpl());
        }

        #endregion

        #region Page Events Handling Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                getGoalsCriteriaTypes();
                ddlValueTypes.Items.Insert(0, new ListItem { Text = "-- select value --", Value = "0" });
            }
        }

        #endregion

        #region Databinding Methods

        void getGoalsCriteriaTypes()
        {
            var areaList = goalDef.appsettings.getChildSettings(new applicationsettings { PARENT_ID = Util.getTypeDefinitionsGoalAreaTypes() }).ToList();
            BindControl.BindDropdown(ddlArea, "DESCRIPTION", "TYPE_ID", "-- select area --", "0", areaList.ToList());
        }

        void loadOrgStruct()
        {
            try
            {
                var userRole = Util.user.UserTypeId;
                var userID = Util.user.UserId;
                var structure = goalDef.orgstructure.getCompanyStructureListView(new company { COMPANY_ID = Util.user.CompanyId });
                //if (userRole != Util.getTypeDefinitionsUserTypeAdministrator()) structure = structure.Where(t => t.OWNER_ID == userID).ToList();
                rqdVal.ErrorMessage = "select organisation unit";
                BindControl.BindDropdown(ddlValueTypes, "ORGUNIT_NAME", "ID", "- select organisation unit -", "0", structure.ToList());
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void loadProjects(int companyId)
        {
            projectmanagement proj = new projectmanagement(new projectImpl());

            int adminRoleId = Util.getTypeDefinitionsUserTypeAdministrator();
            int roleId = (int)Util.user.UserTypeId;
            IEnumerable<project> projL = proj.get(companyId, true);
            if (adminRoleId != Util.user.UserTypeId) projL = projL.Where(c => c.RESPONSIBLE_PERSON_ID == Util.user.UserId);
            rqdVal.ErrorMessage = "select a project";
            BindControl.BindDropdown(ddlValueTypes, "PROJECT_NAME", "PROJECT_ID", "-- select project --", "0", projL);
        }

        void loadJobTitles()
        {
            try
            {
                IEmployee obj = new EmployeeBL();
                List<Employee> titles = obj.GetByOrgStructure(Util.user.OrgId, true);

                if (Util.user.UserTypeId != Util.getTypeDefinitionsUserTypeAdministrator())
                    titles = titles.Where(t => t.LineManagerEmployeeId == Util.user.UserId || t.EmployeeId == Util.user.UserId).ToList();
                rqdVal.ErrorMessage = "select a user";
                BindControl.BindDropdown(ddlValueTypes, "FullName", "EmployeeId", "-- select a user --", "0", titles);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void setCriteriaSelected()
        {
            try
            {
                int areaId = int.Parse(ddlArea.SelectedValue);
                if (areaId == Util.getTypeDefinitionsRolesTypeID()) loadJobTitles();
                else if (areaId == Util.getTypeDefinitionsProjectID()) loadProjects(Util.user.CompanyId);
                else loadOrgStruct();
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void ShowMessage(string msg)
        {
            BindControl.BindLiteral(lblMsg, msg);
        }

        bool IsValid()
        {
            bool isValid = false;
            if (ddDate.SelectedIndex == 0)
            {
                if (!string.IsNullOrEmpty(txtEndDate.Text) && !string.IsNullOrEmpty(txtStartDate.Text)) isValid = true;
                else
                {
                    ShowMessage(Messages.GetRequiredFieldsMessage(true, string.IsNullOrEmpty(txtStartDate.Text) ? "Start Date" : string.Empty,
                       string.IsNullOrEmpty(txtEndDate.Text) ? "End Date" : string.Empty, ddlArea.SelectedIndex == 0 ? "Select Area / Report" : string.Empty,
                       (ddlArea.SelectedValue.Equals("0") && ddlValueTypes.SelectedIndex == 0) ? "Select Value" : string.Empty));
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(txtEndDate.Text) && !string.IsNullOrEmpty(txtStartDate.Text))
                {
                    ShowMessage("You cannot select Report Date, Start & End Date at the same time.");
                    isValid = false;
                }
                else isValid = true;
            }
            return isValid;
        }

        void downloadPeriodicReport()
        {
            if (string.IsNullOrEmpty(txtStartDate.Text) ||
                string.IsNullOrEmpty(txtEndDate.Text) ||
                ddlArea.SelectedIndex == 0 ||
                (ddlArea.SelectedValue.Equals("0") && ddlValueTypes.SelectedIndex == 0))
            {

                ShowMessage(Messages.GetRequiredFieldsMessage(true, string.IsNullOrEmpty(txtStartDate.Text) ? "Start Date" : string.Empty,
                    string.IsNullOrEmpty(txtEndDate.Text) ? "End Date" : string.Empty, ddlArea.SelectedIndex == 0 ? "Select Area / Report" : string.Empty,
                    (ddlArea.SelectedValue.Equals("0") && ddlValueTypes.SelectedIndex == 0) ? "Select Value" : string.Empty));
                return;
            }

            string startDate = Common.GetInternationalDateFormat(txtStartDate.Text.Trim(), DateFormat.monthDayYear);
            string endDate = Common.GetInternationalDateFormat(txtEndDate.Text.Trim(), DateFormat.monthDayYear);

            if (DateTime.Parse(startDate) > DateTime.Parse(endDate))
            {
                ShowMessage("Start Date cannot be greater than End Date.");
                return;
            }

            string areaId = ddlArea.SelectedValue;
            string areaName = ddlArea.SelectedItem.Text;
            string areaValue = ddlValueTypes.SelectedValue;
            string script = string.Format("openNewWindow('avgpreview.aspx?aid={0}&idv={1}&dsVal={2}&deVal={3}&emp=1&areaName={4}');", areaId, areaValue, startDate, endDate, HttpUtility.HtmlEncode(areaName));
            AjaxScriptManager.RegisterStartupScript(Page, typeof(Page), "viewReport", script, true);
        }

        void downloadReport(bool addEventToButton)
        {
            if (ddlArea.SelectedIndex == 0 || ddlValueTypes.SelectedIndex == 0 || ddDate.SelectedIndex == 0)
            {
                ShowMessage(Messages.GetRequiredFieldsMessage(true, ddDate.SelectedIndex == 0 ? "Report Date" : string.Empty,
                    ddlArea.SelectedIndex == 0 ? "Area" : string.Empty, ddlValueTypes.SelectedIndex == 0 ? "Value" : string.Empty));
                return;
            }

            string script = addEventToButton ? string.Format("return openNewWindow('preview.aspx?aid={0}&idv={1}&dVal={2}');", ddlArea.SelectedValue, ddlValueTypes.SelectedValue, ddDate.SelectedValue) :
                                                  string.Format("openNewWindow('preview.aspx?aid={0}&idv={1}&dVal={2}');", ddlArea.SelectedValue, ddlValueTypes.SelectedValue, ddDate.SelectedValue);
            if (addEventToButton) btnViewReport.OnClientClick = script;
            else AjaxScriptManager.RegisterStartupScript(Page, typeof(Page), "myReport", script, true);
        }

        #endregion

        #region Dropdown Event Handling Methods

        protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddDate.Items.Clear();
            ddlValueTypes.Items.Clear();
            setCriteriaSelected();
        }

        protected void ddlValueTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<scores> dates = scoresManager.getScoreDateValues(int.Parse(ddlArea.SelectedValue), int.Parse(ddlValueTypes.SelectedValue));
            BindControl.BindDropdown(ddDate, "PERIOD_DATE_DESC", "PERIOD_DATE_VAL", "- select date -", "0", dates);
        }

        #endregion

        #region Button Event Handling Methods

        protected void btnViewReport_Click(object sender, EventArgs e)
        {
            bool valid = IsValid();
            if (valid)
            {
                if (ddDate.SelectedIndex > 0) downloadReport(false);
                else downloadPeriodicReport();
            }
        }

        #endregion
    }
}