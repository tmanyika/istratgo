﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="scoreForm.ascx.cs" Inherits="scorecard.ui.controls.scoreForm" %>
<asp:UpdatePanel ID="UpdatePanelScoreReport" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div>
            <h1>
                &nbsp; Scores Report
            </h1>
        </div>
        <div>
            <fieldset runat="server" id="pnlContactInfo">
                <legend>Scores Report Criteria </legend>
                <p>
                    <label for="sf">
                        Select Area:
                    </label>
                    &nbsp;<span class="field_desc"><asp:DropDownList runat="server" ID="ddlArea" AutoPostBack="True"
                        OnSelectedIndexChanged="ddlArea_SelectedIndexChanged" ValidationGroup="Form" />
                        <asp:RequiredFieldValidator ID="rqdArea" runat="server" ControlToValidate="ddlArea"
                            Display="Dynamic" ErrorMessage="area is required" InitialValue="0" ValidationGroup="Form"
                            ForeColor="Red"></asp:RequiredFieldValidator>
                    </span>
                </p>
                <p>
                    <label for="sf">
                        Select Value:
                    </label>
                    &nbsp;<span class="field_desc"><asp:DropDownList runat="server" ID="ddlValueTypes"
                        AutoPostBack="True" OnSelectedIndexChanged="ddlValueTypes_SelectedIndexChanged"
                        ValidationGroup="Form" />
                        <asp:RequiredFieldValidator ID="rqdVal" runat="server" ControlToValidate="ddlValueTypes"
                            Display="Dynamic" ErrorMessage="value is required" InitialValue="0" ValidationGroup="Form"
                            ForeColor="Red"></asp:RequiredFieldValidator>
                    </span>
                </p>
                <p>
                    <label for="sf">
                        Report Date:
                    </label>
                    &nbsp;<span class="field_desc"><asp:DropDownList runat="server" ID="ddDate" ValidationGroup="Form" />
                        <asp:RequiredFieldValidator ID="rdqDate" runat="server" ControlToValidate="ddDate"
                            Display="Dynamic" ErrorMessage="select date" InitialValue="0" ValidationGroup="Form"
                            ForeColor="Red"></asp:RequiredFieldValidator>
                    </span>
                </p>
                <p class="errorMsg">
                    <label for="lblMsg">
                    </label>
                    <asp:Literal ID="lblMsg" runat="server"></asp:Literal>
                </p>
                <p>
                    <label>
                        &nbsp;
                    </label>
                    <asp:Button ID="btnViewReport" runat="server" CssClass="button tooltip" Text="View Report"
                        ValidationGroup="Form" OnClick="btnViewReport_Click" CausesValidation="true" />
                </p>
            </fieldset>
        </div>
        <asp:UpdateProgress ID="UpdateProgressScoreReport" runat="server" AssociatedUpdatePanelID="UpdatePanelScoreReport">
            <ProgressTemplate>
                <img runat="server" src="~/ui/images/icons/activity.gif" alt="Please wait..." id="imgPrg" />
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
