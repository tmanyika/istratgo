﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.controllers;
using scorecard.implementations;
using scorecard.entities;
using HierarchicalData;
using Geekees.Common.Controls;
using HR.Human.Resources;

namespace scorecard.ui.controls
{
    public partial class mngorgstructure : System.Web.UI.UserControl
    {
        structurecontroller orgunits;
        jobtitlesdefinitions impl;
        scorecard.controllers.registration _registration;
        useradmincontroller userAdmin;

        public mngorgstructure()
        {
            orgunits = new structurecontroller(new structureImpl());
            _registration = new scorecard.controllers.registration(new customerImpl());
            impl = new jobtitlesdefinitions(new jobtitleImpl());
            userAdmin = new useradmincontroller(new useradminImpl());
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                clearLabels();
                loadCompanyStructure();
                loadSetupTypes();
                loadParentUsers();
            }
        }

        private void clearLabels()
        {
            lblMsg.Text = "";
        }

        void loadCompanyStructure()
        {
            try
            {
                var dataSource = orgunits.getActiveCompanyOrgStructure(Util.user.CompanyId);
                var dS = new HierarchicalDataSet(dataSource, "ID", "PARENT_ORG");
                BindControl.BindTree(trvOrg, dS);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void loadUsers(DropDownList ddlUsers)
        {
            try
            {
                IEmployee obj = new EmployeeBL();
                var data = obj.GetByCompanyId(true, Util.user.CompanyId);
                //List<Employee> data = obj.GetActive(Util.user.OrgId);
                BindControl.BindDropdown(ddlUsers, "FullName", "EmployeeId", data);
                BindControl.BindDropdown(ddlResponsiblePerson, "FullName", "EmployeeId", data);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void loadParentUsers()
        {
            try
            {
                IEmployee obj = new EmployeeBL();
                var users = obj.GetByCompanyId(true, Util.user.CompanyId);
                BindControl.BindDropdown(ddlResponsiblePerson, "FullName", "EmployeeId", users);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }


        bool isDataValid(string name)
        {
            return string.IsNullOrEmpty(name) ? false : true;
        }

        public string getUserInfo(int userID)
        {
            try
            {
                IEmployee obj = new EmployeeBL();
                return obj.GetById(userID).FullName;
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return "";
            }
        }
        void loadSetupTypes()
        {
            try
            {
                var setuptypes = _registration.appsettings.getChildSettings(new entities.applicationsettings { PARENT_ID = Util.getTypeDefinitionsSetupTypeParentID() });
                ddlSetupType.DataSource = setuptypes;
                ddlSetupType.DataTextField = "NAME";
                ddlSetupType.DataValueField = "TYPE_ID";
                ddlSetupType.DataBind();
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        int getValueSelected(string value)
        {
            if (value.Length == 0) { return 0; }
            return int.Parse(value.Replace("\\", ""));
        }

        void loadCurrentOrgstructureInfo(int id)
        {
            var info = orgunits.getOrgunitDetails(new Orgunit { ID = id });
            lblOrgID.Text = info.ID.ToString();
            txtOrgName.Text = info.ORGUNIT_NAME;
            if (ddlSetupType.Items.FindByValue(info.ORGUNT_TYPE.ToString()) != null)
                ddlSetupType.SelectedValue = info.ORGUNT_TYPE.ToString();
            if (ddlResponsiblePerson.Items.FindByValue(info.OWNER_ID.ToString()) != null)
                ddlResponsiblePerson.SelectedValue = info.OWNER_ID.ToString();
        }

        void loadCurrentStructureByParent(int id)
        {
            var currentChildElements = orgunits.getOrgstructureByParentID(new Orgunit { PARENT_ORG = id });
            dtpgrChildOrgs.Visible = true;
            lvChildOrgUnits.InsertItemPosition = InsertItemPosition.LastItem;
            if (currentChildElements.Count <= 0)
            {
                lvChildOrgUnits.DataSource = new List<Orgunit>();
                lvChildOrgUnits.DataBind();
            }
            if (currentChildElements.Count <= dtpgrChildOrgs.PageSize) { dtpgrChildOrgs.Visible = false; }
            lvChildOrgUnits.DataSource = currentChildElements;
            lvChildOrgUnits.DataBind();
            var _ddlUsers = lvChildOrgUnits.InsertItem.FindControl("ddlNewUsers") as DropDownList;
            loadUsers(_ddlUsers);
        }

        protected void lvChildOrgUnits_PagePropertiesChanged(object sender, EventArgs e)
        {
            keepPopupAlive();
        }

        protected void lvChildOrgUnits_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.InsertItem || e.Item.ItemType == ListViewItemType.DataItem)
            {
                switch (e.CommandName)
                {
                    case "addNewItem":
                        createNewOrgUnit(e);
                        break;
                    case "updateItem":
                        updateOrgUnit(e);
                        break;
                    case "editItem":
                        lvChildOrgUnits.EditIndex = e.Item.DataItemIndex;
                        keepPopupAlive();
                        break;
                    case "deleteItem":
                        deleteOrgUnit(e);
                        break;
                    case "cancelUpdate":
                        cancelUpdate();

                        break;
                }
            }
        }
        void createNewOrgUnit(ListViewCommandEventArgs e)
        {
            try
            {
                var txtName = e.Item.FindControl("txtName") as TextBox;
                var ddlUsers = e.Item.FindControl("ddlNewUsers") as DropDownList;

                if (!isDataValid(txtName.Text))
                {
                    lblMsg.Text = "Organisation unit name is required.";
                    return;
                }

                orgunits.addOrgUnit(new Orgunit
                {
                    ORGUNIT_NAME = txtName.Text.Trim(),
                    ORGUNT_TYPE = int.Parse(ddlSetupType.SelectedValue),
                    PARENT_ORG = int.Parse(Session["ItemSelected"].ToString()),
                    COMPANY_ID = Util.user.CompanyId,
                    OWNER_ID = int.Parse(ddlUsers.SelectedValue),
                    ACTIVE = true,
                    UPDATED_BY = Util.user.LoginId,
                    CREATED_BY = Util.user.LoginId
                });
                keepPopupAlive();
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }
        void updateOrgUnit(ListViewCommandEventArgs e)
        {
            try
            {
                var lblID = e.Item.FindControl("lblID") as Label;
                var txtName = e.Item.FindControl("txtName") as TextBox;
                var ddlUsers = e.Item.FindControl("ddlUsers") as DropDownList;

                if (!isDataValid(txtName.Text))
                {
                    lblMsg.Text = "Organisation unit name is required.";
                    return;
                }

                orgunits.updateOrgUnit(new Orgunit
                {
                    ORGUNIT_NAME = txtName.Text.Trim(),
                    ORGUNT_TYPE = int.Parse(ddlSetupType.SelectedValue),
                    ID = int.Parse(lblID.Text),
                    PARENT_ORG = int.Parse(lblOrgID.Text),
                    OWNER_ID = int.Parse(ddlUsers.SelectedValue),
                    ACTIVE = true,
                    UPDATED_BY = Util.user.LoginId,
                    CREATED_BY = Util.user.LoginId
                });
                lvChildOrgUnits.EditIndex = -1;
                lvChildOrgUnits.InsertItemPosition = InsertItemPosition.None;
                keepPopupAlive();
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void deleteOrgUnit(ListViewCommandEventArgs e)
        {
            try
            {
                var lblID = e.Item.FindControl("lblID") as Label;
                var result = orgunits.deleteOrgUnit(new Orgunit
                {
                    ID = int.Parse(lblID.Text),
                    ACTIVE = false,
                    UPDATED_BY = Util.user.LoginId,
                    CREATED_BY = Util.user.LoginId
                });
                lblMsg.Text = (result > 0) ? Messages.GetDeleteMessage() : Messages.GetDeleteFailedMessage();
                lvChildOrgUnits.EditIndex = -1;
                lvChildOrgUnits.InsertItemPosition = InsertItemPosition.None;
                keepPopupAlive();
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void cancelUpdate()
        {
            lvChildOrgUnits.EditIndex = -1;
            lvChildOrgUnits.InsertItemPosition = InsertItemPosition.None;
            loadCurrentStructureByParent(int.Parse(lblOrgID.Text));
            mdlShowOrgStructureInfo.Show();
        }

        void keepPopupAlive()
        {
            ddlSetupType.SelectedIndex = -1;
            loadCurrentOrgstructureInfo(int.Parse(Session["ItemSelected"].ToString()));
            loadCurrentStructureByParent(int.Parse(Session["ItemSelected"].ToString()));
            mdlShowOrgStructureInfo.Show();
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            loadCompanyStructure();
            mdlShowOrgStructureInfo.Hide();
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            updateOrganisationInfo();
            keepPopupAlive();
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }

        void updateOrganisationInfo()
        {
            try
            {
                if (!isDataValid(txtOrgName.Text))
                {
                    lblMsg.Text = "Organisation name is required.";
                    return;
                }

                int orgId = int.Parse(lblOrgID.Text);
                var orgInfo = new Orgunit
                {
                    ID = orgId,
                    ORGUNIT_NAME = txtOrgName.Text.Trim(),
                    ORGUNT_TYPE = int.Parse(ddlSetupType.SelectedValue),
                    OWNER_ID = int.Parse(ddlResponsiblePerson.SelectedValue),
                    ACTIVE = true,
                    UPDATED_BY = Util.user.LoginId,
                    CREATED_BY = Util.user.LoginId
                };
                int result = orgunits.updateOrgUnit(orgInfo);
                if (result > 0)
                {
                    loadCurrentOrgstructureInfo(orgId);
                    lblMsg.Text = Messages.GetSaveMessage();
                    mdlShowOrgStructureInfo.Show();
                }
                else lblMsg.Text = Messages.GetSaveFailedMessage();
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        protected void lvChildOrgUnits_ItemDataBound(object sender, ListViewItemEventArgs e)
        {

            if (e.Item.ItemType == ListViewItemType.DataItem || e.Item.ItemType == ListViewItemType.InsertItem)
            {
                var ddlUsers = e.Item.FindControl("ddlUsers") as DropDownList;
                if (ddlUsers != null)
                {
                    loadUsers(ddlUsers);
                }
            }

        }

        protected void trvOrgstruct_OnSelectedNodeChanged(object src, ASTreeViewNodeSelectedEventArgs e)
        {
            try
            {
                var userRole = Util.user.UserTypeId;
                if (userRole == Util.getTypeDefinitionsUserTypeAdministrator())
                {
                    var treeInfo = src as ASTreeView;
                    var itemSelected = treeInfo.GetSelectedNode();
                    loadCurrentOrgstructureInfo(int.Parse(itemSelected.NodeValue));
                    loadCurrentStructureByParent(int.Parse(itemSelected.NodeValue));
                    Session["ItemSelected"] = itemSelected.NodeValue;
                    keepPopupAlive();
                }
                else
                {
                    Response.Redirect("noaccess.aspx");
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        protected void trvOrg_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                clearLabels();
                var userRole = Util.user.UserTypeId;
                if (userRole == Util.getTypeDefinitionsUserTypeAdministrator())
                {
                    var treeInfo = sender as TreeView;
                    var itemSelected = treeInfo.SelectedNode;
                    loadCurrentOrgstructureInfo(int.Parse(itemSelected.Value));
                    loadCurrentStructureByParent(int.Parse(itemSelected.Value));
                    Session["ItemSelected"] = itemSelected.Value;
                    keepPopupAlive();
                }
                else
                {
                    Response.Redirect("noaccess.aspx");
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }
    }
}