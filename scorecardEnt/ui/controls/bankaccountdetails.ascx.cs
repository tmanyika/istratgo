﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.entities;
using scorecard.implementations;
using scorecard.controllers;

namespace scorecard.ui.controls
{
    public partial class bankaccountdetails : System.Web.UI.UserControl
    {
        #region Properties

        int compId
        {
            get { return (Session["UserInfo"] as UserInfo).CompanyId; }
        }

        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) initialisePage();
        }


        #endregion

        #region Utility Methods

        void initialisePage()
        {
            clearTextBoxes();
            loadBankDetails();
        }

        void clearTextBoxes()
        {
            txtAccountNumber.Value = "";
            txtBankName.Value = "";
            txtBranch.Value = "";
            txtBranchCode.Value = "";
            txtAccountNumber.Value = "";
            txtAccType.Value = "";
        }

        void loadBankDetails()
        {
            bankaccount obj = new companyregistration(new companyImpl()).getCompanyAccountDetails(compId);
            btnComplete.CommandArgument = obj.ACCOUNT_NO == null ? false.ToString().ToLower() : true.ToString().ToLower();

            if (obj.ACCOUNT_NO != null)
            {
                txtBankName.Value = obj.BANK_NAME;
                txtBranch.Value = obj.BRANCH_NAME;
                txtBranchCode.Value = obj.BRANCH_CODE;
                txtAccountNumber.Value = obj.ACCOUNT_NO;
                txtAccType.Value = obj.ACCOUNT_TYPE;
            }
        }

        void saveBankDetails(bool mode)
        {
            try
            {
                bankaccount account = new bankaccount
                {
                    BANK_NAME = txtBankName.Value.Trim(),
                    BRANCH_CODE = txtBranchCode.Value.Trim(),
                    BRANCH_NAME = txtBranch.Value.Trim(),
                    ACCOUNT_NO = txtAccountNumber.Value.Trim(),
                    ACCOUNT_TYPE = txtAccType.Value.Trim(),
                    COMPANY_ID = compId
                };

                if (compId > 0)
                {
                    companyregistration obj = new companyregistration(new companyImpl());
                    int result = mode ? obj.updateRegisteredCompanyAccountDetails(account) : obj.addRegisteredCompanyAccountDetails(account);
                    string msg = result > 0 ? Messages.GetUpdateMessage() : Messages.GetUpdateFailedMessage();
                    BindControl.BindLiteral(lblError, msg);
                }
                else BindControl.BindLiteral(lblError, Messages.GetSaveFailedMessage());
            }
            catch (Exception e)
            {
                BindControl.BindLiteral(lblError, Messages.GetErrorMessage());
                Util.LogErrors(e);
            }
        }

        #endregion

        #region Button Event Handling

        protected void btnComplete_Click(object sender, EventArgs e)
        {
            bool mode = bool.Parse((sender as Button).CommandArgument);
            saveBankDetails(mode);
        }

        #endregion

    }
}