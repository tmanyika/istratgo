﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="leaveTypes.ascx.cs"
    Inherits="scorecard.ui.controls.leaveTypes" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<h1>
    Leave Types Management</h1>
<asp:UpdatePanel ID="UpdatePanelLeave" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div>
            <asp:ListView ID="lstLeaveType" runat="server"
                OnItemEditing="lstLeaveType_ItemEditing" 
                OnItemCommand="lstLeaveType_ItemCommand" 
                onpagepropertieschanging="lstLeaveType_PagePropertiesChanging">
                <LayoutTemplate>
                    <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                        <thead>
                            <tr>
                                <td>
                                    Leave Type
                                </td>
                                <td>
                                    Description
                                </td>
                                <td>
                                    Active
                                </td>
                                <td>
                                    Edit
                                </td>
                                <td>
                                    Delete
                                </td>
                            </tr>
                            <tr id="itemPlaceholder" runat="server">
                            </tr>
                        </thead>
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <tbody>
                        <tr class="odd">
                            <td>
                                <asp:HiddenField ID="hdnLeaveId" runat="server" Value='<%# Eval("LeaveId") %>' />
                                <asp:Literal ID="lblName" runat="server" Text='<%# Eval("LeaveName")%>'></asp:Literal>
                            </td>
                            <td>
                                <%# Eval("Description") %>
                            </td>
                            <td>
                                <asp:Literal ID="lblActive" runat="server" Text='<%# GetBoolean(Eval("Active")) %>' />
                            </td>
                            <td>
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="lnkEdit" CommandName="Edit" class="ui-state-default ui-corner-all"
                                        title="Edit Record" CausesValidation="false"><span class="ui-icon ui-icon-pencil">
                                        </span>
                                    </asp:LinkButton>
                                </ul>
                            </td>
                            <td>
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="lnkDelete" CommandName="DeleteItem" class="ui-state-default ui-corner-all"
                                        title="Delete Record" OnClientClick="return confirmDelete();" CausesValidation="false"><span class="ui-icon ui-icon-trash">
                        </span></asp:LinkButton></ul>
                            </td>
                        </tr>
                    </tbody>
                </ItemTemplate>
                <EditItemTemplate>
                    <tbody>
                        <tr class="odd">
                            <td>
                                <asp:TextBox runat="server" ID="txtName" Text='<%# Eval("LeaveName")%>' ValidationGroup="Update" />
                                <asp:RequiredFieldValidator ID="rqdName" runat="server" ErrorMessage="*" ValidationGroup="Update"
                                    ControlToValidate="txtName" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                <asp:HiddenField ID="hdnLeaveIdE" runat="server" Value='<%# Eval("LeaveId") %>' />
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtDesc" Text='<%# Eval("Description")%>' />
                            </td>
                            <td>
                                <asp:CheckBox ID="chkActive" runat="server" Checked='<%# Eval("Active") %>' />
                            </td>
                            <td>
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="lnkUpdate" CommandName="UpdateItem" class="ui-state-default ui-corner-all"
                                        title="Update Record" ValidationGroup="Update"><span class="ui-icon ui-icon-disk"></span></asp:LinkButton>
                                </ul>
                            </td>
                            <td>
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="lnkCancelUpdate" CommandName="CancelUpdate" class="ui-state-default ui-corner-all"
                                        title="Cancel Update" CausesValidation="false"><span class="ui-icon ui-icon-cancel"></span></asp:LinkButton>
                                </ul>
                            </td>
                        </tr>
                    </tbody>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <tbody>
                        <tr class="odd">
                            <td>
                                <asp:HiddenField ID="hdnLeaveTypeId" runat="server" />
                                <asp:TextBox runat="server" ID="txtNamei" ValidationGroup="Add" />
                                <asp:RequiredFieldValidator ID="rqdNameI" runat="server" ErrorMessage="*" ValidationGroup="Add"
                                    ControlToValidate="txtNamei" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtDesci" />
                            </td>
                            <td>
                                <asp:CheckBox ID="chkActivei" runat="server" Checked="True" />
                            </td>
                            <td>
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="lnkSave" CommandName="AddItem" class="ui-state-default ui-corner-all"
                                        title="Update Record" ValidationGroup="Add"><span class="ui-icon ui-icon-disk"></span></asp:LinkButton>
                                </ul>
                            </td>
                            <td>
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="lnkCancelUpdate" CommandName="CancelUpdate" class="ui-state-default ui-corner-all"
                                        title="Cancel Update" CausesValidation="false"><span class="ui-icon ui-icon-cancel"></span></asp:LinkButton>
                                </ul>
                            </td>
                        </tr>
                    </tbody>
                </InsertItemTemplate>
                <EmptyDataTemplate>
                    <p class="errorMsg">
                        There are no records to display.
                    </p>
                </EmptyDataTemplate>
            </asp:ListView>
            <br />
            <asp:DataPager ID="pager" runat="server" PagedControlID="lstLeaveType">
                <Fields>
                    <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True" ShowNextPageButton="False"
                        ShowPreviousPageButton="False" />
                    <asp:NumericPagerField />
                    <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True" ShowNextPageButton="False"
                        ShowPreviousPageButton="False" />
                </Fields>
            </asp:DataPager>
            <br />
            <br />
            <p class="errorMsg">
                <asp:Literal ID="lblMsg" runat="server"></asp:Literal></p>
        </div>
        <div id="tabs-3">
            <p>
                <asp:Button ID="btnNew" runat="server" class="button" Text="Add New Leave Type" OnClick="lnkAdd_Click"
                    CausesValidation="False" />
                <asp:Button ID="lnkCancel0" runat="server" class="button" Text="Done" OnClick="lnkCancel_Click"
                    CausesValidation="false" />
            </p>
        </div>
        <asp:UpdateProgress ID="UpdateProgressLeave" runat="server" AssociatedUpdatePanelID="UpdatePanelLeave">
            <ProgressTemplate>
                <div>
                    <asp:Image ID="imgProj" runat="server" ImageUrl="~/ui/images/icons/activity.gif"
                        AlternateText="Please wait.."></asp:Image></div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
