﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.implementations;
using HR.Leave;

namespace scorecard.ui.controls.hr
{
    public partial class leaveAccruals : System.Web.UI.UserControl
    {
        #region Properties

        int CompId
        {
            get { return Util.user.CompanyId; }
        }

        private ILeaveType BlLeave
        {
            get { return new LeaveTypeBL(); }
        }

        private IWorkingWeek BlWork
        {
            get { return new WorkingWeekBL(); }
        }

        private ILeaveAccrualTime BlTime
        {
            get { return new LeaveAccrualTimeBL(); }
        }

        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) InitialisePage();
        }

        #endregion

        #region Utility Methods

        List<LeaveType> GetLeaveTypes()
        {
            return BlLeave.GetByStatus(CompId, true);
        }

        List<LeaveAccrualTime> GetAccrualTime()
        {
            return  BlTime.GetByStatus(true);
        }

        List<WorkingWeek> GetWorkWeeks()
        {
           return BlWork.GetByStatus(CompId, true);
        }

        void InitialisePage()
        {
            LoadLeaveAccrual();
        }

        void LoadLeaveAccrual()
        {
            ILeaveAccrual obj = new LeaveAccrualBL();
            BindControl.BindListView(lstData, obj.GetByStatus(CompId, true));
            pager.Visible = (lstData.Items.Count <= pager.PageSize) ? false : true;
        }

        void BindListViewFooter()
        {
            DropDownList ddLeavei = lstData.InsertItem.FindControl("ddLeavei") as DropDownList;
            DropDownList ddWekinWiki = lstData.InsertItem.FindControl("ddWekinWiki") as DropDownList;
            DropDownList ddTimei = lstData.InsertItem.FindControl("ddTimei") as DropDownList;

            if (ddLeavei != null) BindControl.BindDropdown(ddLeavei, GetLeaveTypes());
            if (ddWekinWiki != null) BindControl.BindDropdown(ddWekinWiki, GetWorkWeeks());
            if (ddTimei != null) BindControl.BindDropdown(ddTimei, GetAccrualTime());
        }

        void BindDropDowns(int idx)
        {
            var ddLeave = lstData.Items[idx].FindControl("ddLeave") as DropDownList;
            var ddWekinWik = lstData.Items[idx].FindControl("ddWekinWik") as DropDownList;
            var ddTime = lstData.Items[idx].FindControl("ddTime") as DropDownList;

            if (ddLeave != null)
            {
                BindControl.BindDropdown(ddLeave, GetLeaveTypes());
                string leaveId = (lstData.Items[idx].FindControl("hdnLeaveId") as HiddenField).Value;
                if (ddLeave.Items.FindByValue(leaveId) != null) ddLeave.SelectedValue = leaveId;
            }

            if (ddWekinWik != null)
            {
                BindControl.BindDropdown(ddWekinWik, GetWorkWeeks());
                string wekWikId = (lstData.Items[idx].FindControl("hdnWorkingWeekId") as HiddenField).Value;
                if (ddWekinWik.Items.FindByValue(wekWikId) != null) ddWekinWik.SelectedValue = wekWikId;
            }

            if (ddTime != null)
            {
                BindControl.BindDropdown(ddTime, GetAccrualTime());
                string timeId = (lstData.Items[idx].FindControl("hdnAccrualTimeId") as HiddenField).Value;
                if (ddTime.Items.FindByValue(timeId) != null) ddTime.SelectedValue = timeId;
            }
        }

        #endregion

        #region Button Event Handling

        protected void lnkAdd_Click(object sender, EventArgs e)
        {
            BindControl.BindLiteral(lblMsg, "");
            lstData.InsertItemPosition = InsertItemPosition.LastItem;
            LoadLeaveAccrual();
            BindListViewFooter();
        }

        protected void lnkCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("index.aspx", false);
        }

        #endregion

        #region Utility

        public string GetBoolean(object boolVal)
        {
            if (boolVal == DBNull.Value) return "Yes";
            return bool.Parse(boolVal.ToString()) ? "Yes" : "No";
        }

        private void CancelUpdate()
        {
            lstData.EditIndex = -1;
            lstData.InsertItemPosition = InsertItemPosition.None;
            LoadLeaveAccrual();
        }

        void DeleteItem(ListViewCommandEventArgs e)
        {
            int leaveId = int.Parse((e.Item.FindControl("hdnLeaveAccrualId") as HiddenField).Value);
            string updatedBy = Util.user.LoginId;

            ILeaveAccrual obj = new LeaveAccrualBL();
            bool deleted = obj.Delete(leaveId, updatedBy);
            if (deleted) LoadLeaveAccrual();
            string msg = deleted ? Messages.GetDeleteMessage() : Messages.GetDeleteFailedMessage();
            BindControl.BindLiteral(lblMsg, msg);
        }

        void UpdateItem(ListViewCommandEventArgs e)
        {
            try
            {
                int leaveAccrualId = int.Parse((e.Item.FindControl("hdnLeaveAccrualIdE") as HiddenField).Value);
                int leaveId = int.Parse((e.Item.FindControl("ddLeave") as DropDownList).SelectedValue);
                int wekWikId = int.Parse((e.Item.FindControl("ddWekinWik") as DropDownList).SelectedValue);
                int accrualTimeId = int.Parse((e.Item.FindControl("ddTime") as DropDownList).SelectedValue);

                decimal accrualRate = decimal.Parse((e.Item.FindControl("txtAccrualRate") as TextBox).Text.Trim());
                decimal threshold = decimal.Parse((e.Item.FindControl("txtMaximum") as TextBox).Text.Trim());

                bool active = (e.Item.FindControl("chkActive") as CheckBox).Checked;
                string updatedBy = Util.user.LoginId;

                LeaveAccrual obj = new LeaveAccrual
                {
                    LeaveAccrualId = leaveAccrualId,
                    LeaveId = leaveId,
                    AccrualTimeId = accrualTimeId,
                    MaximumThreshold = threshold,
                    AccrualRate = accrualRate,
                    WorkingWeekId = wekWikId,
                    CreatedBy = updatedBy,
                    UpdatedBy = updatedBy,
                    Active = active
                };

                ILeaveAccrual objl = new LeaveAccrualBL();
                bool saved = objl.UpdateLeaveAccrual(obj);
                BindControl.BindLiteral(lblMsg, saved ? Messages.GetUpdateMessage() : Messages.GetLeaveAccrualRateErrorMessage());
                if (saved)
                {
                    lstData.EditIndex = -1;
                    LoadLeaveAccrual();
                }

            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        void AddItem(ListViewCommandEventArgs e)
        {
            try
            {
                int leaveId = int.Parse((e.Item.FindControl("ddLeavei") as DropDownList).SelectedValue);
                int wekWikId = int.Parse((e.Item.FindControl("ddWekinWiki") as DropDownList).SelectedValue);
                int accrualTimeId = int.Parse((e.Item.FindControl("ddTimei") as DropDownList).SelectedValue);

                decimal accrualRate = decimal.Parse((e.Item.FindControl("txtAccrualRatei") as TextBox).Text.Trim());
                decimal threshold = decimal.Parse((e.Item.FindControl("txtMaximumi") as TextBox).Text.Trim());

                bool active = (e.Item.FindControl("chkActivei") as CheckBox).Checked;
                string updatedBy = Util.user.LoginId;

                LeaveAccrual obj = new LeaveAccrual
                {
                    LeaveAccrualId = 0,
                    LeaveId = leaveId,
                    AccrualTimeId = accrualTimeId,
                    MaximumThreshold = threshold,
                    AccrualRate = accrualRate,
                    WorkingWeekId = wekWikId,
                    CreatedBy = updatedBy,
                    UpdatedBy = updatedBy,
                    Active = active
                };

                ILeaveAccrual objl = new LeaveAccrualBL();
                bool saved = objl.AddLeaveAccrual(obj);
                BindControl.BindLiteral(lblMsg, saved ? Messages.GetUpdateMessage() : Messages.GetLeaveAccrualRateErrorMessage());
                if (saved)
                {
                    lstData.InsertItemPosition = InsertItemPosition.None;
                    LoadLeaveAccrual();
                }
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        #endregion

        #region ListView Event Handling Methods including Slide Paging

        protected void lstData_PagePropertiesChanged(object sender, EventArgs e)
        {
            LoadLeaveAccrual();
        }

        protected void lstData_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            try
            {
                lstData.EditIndex = e.NewEditIndex;
                lstData.InsertItemPosition = InsertItemPosition.None;
                LoadLeaveAccrual();
                BindDropDowns(e.NewEditIndex);
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        protected void lstData_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                BindControl.BindLiteral(lblMsg, "");
                if (e.Item.ItemType == ListViewItemType.InsertItem ||
               e.Item.ItemType == ListViewItemType.DataItem)
                {
                    switch (e.CommandName)
                    {
                        case "AddItem":
                            AddItem(e);
                            break;
                        case "UpdateItem":
                            UpdateItem(e);
                            break;
                        case "DeleteItem":
                            DeleteItem(e);
                            break;
                        case "CancelUpdate":
                            CancelUpdate();
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        #endregion
    }
}