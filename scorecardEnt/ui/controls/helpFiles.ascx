﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="helpFiles.ascx.cs" Inherits="scorecard.ui.controls.helpFiles" %>
<div>
    <table cellpadding="2" cellspacing="0" border="0" class="helpTable">
        <tr>
            <td colspan="4">
                <h1>
                    Getting Started
                </h1>
            </td>
        </tr>
        <tr>
            <td colspan="4" style="height: 20px">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <b>A.</b>
            </td>
            <td>
                <a href="userguide/index.html" target="_blank">View Online User Guide</a>
            </td>
            <td>
                <b>B.</b>
            </td>
            <td>
                <a href="userguide/iStratgoUserGuideV1.0.pdf" target="_blank">Download PDF User Guide</a>
            </td>
        </tr>
         <tr>
            <td colspan="4" style="height: 30px">
                &nbsp;
            </td>
        </tr>
    </table>
</div>
