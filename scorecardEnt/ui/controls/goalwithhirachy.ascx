﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="goalwithhirachy.ascx.cs"
    Inherits="scorecard.ui.controls.goalwithhirachy" %>
<%@ Import Namespace="scorecard.implementations" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:UpdatePanel ID="UpdatePanelGoal" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <h1>
            &nbsp; Performance Measures
        </h1>
        <fieldset runat="server" id="pnlContactInfo">
            <legend>Setup Your Performance Measures</legend>
            <p>
                <label for="sf">
                    Select Area:
                </label>
                &nbsp;<span class="field_desc"><asp:DropDownList runat="server" ID="ddlArea" AutoPostBack="True"
                    OnSelectedIndexChanged="ddlArea_SelectedIndexChanged" Width="130px" />
                </span>
                <asp:RequiredFieldValidator ID="Rfd" runat="server" ControlToValidate="ddlArea" Display="Dynamic"
                    ErrorMessage="*" ForeColor="Red" InitialValue="-1" ValidationGroup="New"></asp:RequiredFieldValidator>
            </p>
            <p>
                <label for="sf">
                    Select Value:
                </label>
                &nbsp;<span class="field_desc"><asp:DropDownList runat="server" ID="ddlValueTypes"
                    AutoPostBack="True" OnSelectedIndexChanged="ddlValueTypes_SelectedIndexChanged"
                    Width="130px" />
                    <asp:RequiredFieldValidator ID="RfdV" runat="server" ControlToValidate="ddlValueTypes"
                        Display="Dynamic" ErrorMessage="*" ForeColor="Red" InitialValue="-1" ValidationGroup="New"></asp:RequiredFieldValidator>
                </span>
            </p>
        </fieldset>
        <fieldset runat="server" id="Fieldset2">
            <legend>Context of Environment</legend>
            <p>
                &nbsp;<span class="field_desc">
                    <div style="float: left; width: 90%">
                        <asp:TextBox runat="server" ID="txtEnvContext" Width="95%" ValidationGroup="Environment"
                            TextMode="MultiLine" Height="20px" />
                    </div>
                    <div style="float: left; text-align: left">
                        <asp:Button ID="btnContextSave" runat="server" CssClass="button" Text="Save" CausesValidation="true"
                            ValidationGroup="Environment" OnClick="btnContextSave_Click" />
                        <asp:HiddenField ID="hdnContextId" runat="server" />
                    </div>
                    <div style="clear: both" />
                </span>
            </p>
        </fieldset>
        <asp:ListView ID="lvGoals" runat="server" OnItemCommand="lvGoal_ItemCommand" OnItemEditing="lvGoal_ItemEditing"
            OnItemDataBound="lvGoals_ItemDataBound">
            <LayoutTemplate>
                <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                    <thead>
                        <tr id="trFirst" runat="server">
                            <td>
                            </td>
                            <td align="left" width="300px">
                                Strategic Objective
                            </td>
                            <td align="left" width="300px">
                                Performance Measure
                            </td>
                            <td style="width: 170px" width="280px">
                                Rationale
                            </td>
                            <td align="left" width="100px">
                                Target
                            </td>
                            <td align="left" width="200px">
                                Unit Of Measurement
                            </td>
                            <td align="left" width="100px">
                                Weight
                            </td>
                            <td width="80px">
                                Edit
                            </td>
                            <td width="80px">
                                Delete
                            </td>
                        </tr>
                        <tr id="ItemPlaceHolder" runat="server">
                        </tr>
                    </thead>
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tbody>
                    <tr class="odd">
                        <td>
                            <asp:LinkButton ID="lnkExpand" runat="server" CommandName="expand" CommandArgument='<%# Eval("STRATEGIC_ID") %>'
                                ToolTip="add a sub measure" Visible='<%# Util.GetVisibility(Eval("PARENT_ID")) %>'>+</asp:LinkButton>
                        </td>
                        <td>
                            <asp:Label ID="lblID" runat="server" Visible="false" Text='<%# Eval("ID") %>' />
                            <asp:HiddenField ID="hdnHasSubGoals" runat="server" Value=' <%# Eval("HAS_SUB_GOALS")%>' />
                            <asp:Literal ID="lblStrategic" runat="server" Text='<%# getStrategicObjective(int.Parse(Eval("STRATEGIC_ID").ToString()))%>'></asp:Literal>
                            <asp:HiddenField ID="hdnWeightVal" runat="server" Value=' <%# Eval("WEIGHT")%>' />
                        </td>
                        <td>
                            <asp:Literal ID="lblGoalDesc" runat="server" Text='<%# Eval("GOAL_DESCRIPTION")%>' />
                        </td>
                        <td>
                            <%# Eval("RATIONAL")%>
                        </td>
                        <td>
                            <asp:Literal ID="lblTarget" runat="server" Text='<%# Eval("TARGET")%>' Visible='<%# Util.GetSubVisibility(Eval("PARENT_ID"),Eval("HAS_SUB_GOALS")) %>' />
                        </td>
                        <td>
                            <asp:Literal ID="lblUnitOfMeasureName" runat="server" Text='<%# getTypeName(Eval("UNIT_MEASURE"))%>'
                                Visible='<%# Util.GetSubVisibility(Eval("PARENT_ID"),Eval("HAS_SUB_GOALS")) %>' />
                        </td>
                        <td>
                            <asp:Literal ID="LblWeight" runat="server" Text='<%# Eval("WEIGHT")%>' Visible='<%# Util.GetVisibility(Eval("PARENT_ID")) %>'></asp:Literal>
                            &nbsp;%
                        </td>
                        <td>
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkEdit" CommandName="Edit" class="ui-state-default ui-corner-all"
                                    title="Edit Record"><span class="ui-icon ui-icon-pencil">
                        </span></asp:LinkButton>
                            </ul>
                        </td>
                        <td>
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkDelete" CommandName="deleteGoal" class="ui-state-default ui-corner-all"
                                    title="Delete Record" OnClientClick="return confirmDelete();"><span class="ui-icon ui-icon-trash">
                        </span></asp:LinkButton>
                            </ul>
                        </td>
                    </tr>
                    <asp:Repeater ID="rptMeasureData" runat="server" Visible="false" OnItemCommand="rptMeasureData_OnItemCommand">
                        <ItemTemplate>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:HiddenField ID="hdnParentId" runat="server" Value='<%# Eval("PARENT_ID") %>' />
                                    <asp:HiddenField ID="hdnSubId" runat="server" Value='<%# Eval("ID") %>' />
                                    <asp:HiddenField ID="hdnHasSubGoals" runat="server" Value=' <%# Eval("HAS_SUB_GOALS")%>' />
                                </td>
                                <td>
                                    <%# Eval("GOAL_DESCRIPTION")%>
                                </td>
                                <td>
                                    <%# Eval("RATIONAL")%>
                                </td>
                                <td>
                                    <%# Eval("TARGET")%>
                                </td>
                                <td>
                                    <%# getTypeName(Eval("UNIT_MEASURE"))%>
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <ul id="icons">
                                        <asp:LinkButton runat="server" ID="lnkEdit" CommandName="EditGoal" class="ui-state-default ui-corner-all"
                                            title="Edit Record"><span class="ui-icon ui-icon-pencil">
                        </span></asp:LinkButton>
                                    </ul>
                                </td>
                                <td>
                                    <ul id="icons">
                                        <asp:LinkButton runat="server" ID="lnkDelete" CommandName="DeleteGoal" class="ui-state-default ui-corner-all"
                                            title="Delete Record" OnClientClick="return confirmDelete();"><span class="ui-icon ui-icon-trash">
                        </span></asp:LinkButton>
                                    </ul>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>
            </ItemTemplate>
            <EditItemTemplate>
                <tbody>
                    <tr class="odd">
                        <td>
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlEditStrategic" Width="200px" />
                            <asp:Label ID="lblID" runat="server" Visible="false" Text='<%# Eval("ID") %>' />
                            <asp:HiddenField ID="hdnWeight" runat="server" Value=' <%# Eval("WEIGHT")%>' />
                            <asp:HiddenField ID="hdnUnitMeasure" runat="server" Value=' <%# Eval("UNIT_MEASURE")%>' />
                            <asp:HiddenField ID="hdnStrategic" runat="server" Value=' <%# Eval("STRATEGIC_ID")%>' />
                            <asp:HiddenField ID="hdnHasSubGoals" runat="server" Value=' <%# Eval("HAS_SUB_GOALS")%>' />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtEditDescription" Text='<%# Eval("GOAL_DESCRIPTION")%>'
                                Visible='<%# Util.GetParentVisibility(Eval("HAS_SUB_GOALS")) %>' />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtEditRational" Text='<%# Eval("RATIONAL")%>' Width="170px"
                                Visible='<%# Util.GetParentVisibility(Eval("HAS_SUB_GOALS")) %>' />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtEditTarget" Text='<%# Eval("TARGET")%>' Width="40px"
                                Visible='<%# Util.GetParentVisibility(Eval("HAS_SUB_GOALS")) %>' />
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlUnitOfMeasure" />
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlEditWeight" Width="70px" />
                        </td>
                        <td>
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkSave" CommandName="updateGoals" class="ui-state-default ui-corner-all"
                                    title="Update Record"><span class="ui-icon ui-icon-disk">
                        </span></asp:LinkButton>
                            </ul>
                        </td>
                        <td>
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkCancelUpdate" CommandName="cancelUpdate" class="ui-state-default ui-corner-all"
                                    title="Cancel Update"><span class="ui-icon ui-icon-cancel">
                        </span></asp:LinkButton>
                            </ul>
                        </td>
                    </tr>
                    <asp:Repeater ID="rptMeasureData" runat="server" Visible="false" OnItemCommand="rptMeasureData_OnItemCommand">
                        <ItemTemplate>
                            <tr>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td>
                                    <%# Eval("GOAL_DESCRIPTION")%>
                                </td>
                                <td>
                                    <%# Eval("RATIONAL")%>
                                </td>
                                <td>
                                    <%# Eval("TARGET")%>
                                </td>
                                <td>
                                    <%# getTypeName(Eval("UNIT_MEASURE"))%>
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                </td>
                                <td>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>
            </EditItemTemplate>
            <InsertItemTemplate>
                <tbody>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlStrategic" Width="200px" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtDescription" Width="170px" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtRational" Width="170px" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtTarget" Width="40px" />
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlUnitOfMeasure" Width="160px" />
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlWeight" Width="70px" />
                        </td>
                        <td>
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkSave" class="ui-state-default ui-corner-all"
                                    title="Save this Record" CommandName="addNewGoal"><span class="ui-icon ui-icon-disk">
                        </span></asp:LinkButton>
                            </ul>
                        </td>
                        <td>
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkCancel" class="ui-state-default ui-corner-all"
                                    title="Cancel Save " CommandName="cancelUpdate"><span class="ui-icon ui-icon-cancel">
                        </span></asp:LinkButton>
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </InsertItemTemplate>
        </asp:ListView>
        <br />
        <asp:DataPager ID="dtPagerlvGoals" runat="server" PagedControlID="lvGoals" PageSize="50"
            OnPreRender="dtPagerlvGoals_PreRender">
            <Fields>
                <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True" ShowNextPageButton="False"
                    ShowPreviousPageButton="False" />
                <asp:NumericPagerField />
                <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True" ShowNextPageButton="False"
                    ShowPreviousPageButton="False" />
            </Fields>
        </asp:DataPager>
        <div>
            <br />
            <br />
            <span style="color: Red" class="errorMsg">
                <asp:Literal ID="LblError" runat="server" /></span></div>
        <div>
            <p class="errorMsg">
                <asp:Label ID="lblMsg" runat="server" Style="color: #FF3300" />
                <br />
            </p>
        </div>
        <div id="tabs-3">
            <p>
                <asp:Button ID="btnNew" runat="server" CssClass="button" Text="Add New Performance Measure"
                    OnClick="btnNew_Click" ValidationGroup="New" />
                <asp:Button ID="lnkCancel0" runat="server" CssClass="button" Text="Done" CausesValidation="false"
                    PostBackUrl="~/ui/index.aspx" />
            </p>
        </div>
        <asp:UpdateProgress ID="UpdateProgressGoal" runat="server" AssociatedUpdatePanelID="UpdatePanelGoal">
            <ProgressTemplate>
                <div>
                    <asp:Image ID="imgOrg" runat="server" ImageUrl="~/ui/images/icons/activity.gif" AlternateText="Please wait..">
                    </asp:Image></div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:Panel ID="mdlPanelShowForm" runat="server" CssClass="modalPopup" Height="400px"
            Width="640px" ScrollBars="None" GroupingText="Sub Performance Measure">
            <br />
            <p>
                <label for="lf">
                    Strategic Objective
                </label>
                <span class="field_desc">:&nbsp;
                    <asp:Literal ID="LblStrategic" runat="server"></asp:Literal>
                    <asp:HiddenField ID="hdnGoalId" runat="server" />
                    <asp:HiddenField ID="hdnStrategicId" runat="server" />
                    <asp:HiddenField ID="hdnParentId" runat="server" />
                    <asp:HiddenField ID="hdnWeightS" runat="server" />
                </span>
            </p>
            <br />
            <p>
                <label for="lf">
                    Performance Measure
                </label>
                <span class="field_desc">:&nbsp;&nbsp;<asp:Literal ID="LblPerformance" runat="server"></asp:Literal></span>
            </p>
            <br />
            <p>
                <label for="lf">
                    Weight
                </label>
                <span class="field_desc">:&nbsp;&nbsp;<asp:Literal ID="LblWeight" runat="server"></asp:Literal></span>
            </p>
            <br />
            <p>
                <label for="lf">
                    Sub Performance Measure
                </label>
                <span class="field_desc">:&nbsp;
                    <asp:TextBox runat="server" ID="txtDescription" Width="300px" ValidationGroup="Sub" /></span><asp:RequiredFieldValidator
                        ID="rqdValDesc" runat="server" ErrorMessage="*" ControlToValidate="txtDescription"
                        Display="Dynamic" ForeColor="Red" ValidationGroup="Sub"></asp:RequiredFieldValidator>
            </p>
            <p>
                <label for="lf">
                    Rationale
                </label>
                <span class="field_desc">:&nbsp;
                    <asp:TextBox runat="server" ID="txtRational" Width="300px" ValidationGroup="Sub" />
                    <asp:RequiredFieldValidator ID="rvdR" runat="server" ErrorMessage="*" ControlToValidate="txtRational"
                        Display="Dynamic" ForeColor="Red" ValidationGroup="Sub" Enabled="false"></asp:RequiredFieldValidator>
                </span>
            </p>
            <p>
                <label for="lf">
                    Target
                </label>
                <span class="field_desc">:&nbsp;
                    <asp:TextBox runat="server" ID="txtTarget" Width="100px" ValidationGroup="Sub" />
                    <asp:RequiredFieldValidator ID="rqdVT" runat="server" ErrorMessage="*" ControlToValidate="txtTarget"
                        Display="Dynamic" ForeColor="Red" ValidationGroup="Sub"></asp:RequiredFieldValidator>
                </span>
            </p>
            <p>
                <label for="lf">
                    Unit Of Measure
                </label>
                <span class="field_desc">:&nbsp;
                    <asp:DropDownList runat="server" ID="ddlUnitOfMeasures" Width="300px" ValidationGroup="Sub" />
                </span>
            </p>
            <p>
                <span class="errorMsg">
                    <asp:Literal ID="LblModalError" runat="server"></asp:Literal>
                </span>
            </p>
            <br />
            <div>
                <label for="lf">
                    &nbsp;</label><span class="field_desc">&nbsp;&nbsp;
                        <asp:Button runat="server" ID="lnkSaveSub" CssClass="button" Text="Submit" OnClick="lnkSaveSub_Click"
                            CausesValidation="true" ValidationGroup="Sub" Width="100px" />
                        &nbsp;
                        <asp:Button runat="server" ID="lnkCancelSub" CssClass="button" Text="Cancel" CausesValidation="false"
                            Width="100px" />
                    </span>
            </div>
        </asp:Panel>
        <asp:Button ID="btnMeasureDeatils" runat="server" Style="display: none" />
        <ajaxToolkit:ModalPopupExtender ID="mdlShowSubMeasureInfo" runat="server" TargetControlID="btnMeasureDeatils"
            PopupControlID="mdlPanelShowForm" BackgroundCssClass="modalBackground" />
    </ContentTemplate>
</asp:UpdatePanel>
