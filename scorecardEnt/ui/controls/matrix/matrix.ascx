﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="matrix.ascx.cs" Inherits="scorecard.ui.controls.matrix.matrix" %>
<%@ Import Namespace="scorecard.implementations" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<h1>
    Matrix Management
</h1>
<asp:UpdatePanel ID="UpdatePanelMatrix" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:ListView ID="lstMatrix" runat="server" OnPagePropertiesChanged="lstMatrix_PagePropertiesChanged"
            OnItemEditing="lstMatrix_ItemEditing" OnItemCommand="lstMatrix_ItemCommand">
            <LayoutTemplate>
                <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                    <thead>
                        <tr>
                            <td align="left" valign="top" style="width: 130px">
                                Years in Same<br />
                                Role
                            </td>
                            <td align="left" valign="top" style="width: 150px">
                                Years in Same<br />
                                Role Condition
                            </td>
                            <td align="left" valign="top" style="width: 180px">
                                Matrix
                            </td>
                            <td align="left" valign="top" style="width: 180px">
                                Description
                            </td>
                            <td align="left" valign="top" style="width: 150px">
                                Company Rating<br />
                                Condition
                            </td>
                            <td align="left" valign="top" style="width: 130px">
                                Company Rating
                            </td>
                            <td align="left" valign="top" style="width: 100px">
                                Position
                            </td>
                            <td align="left" valign="top" style="width: 80px">
                                Active
                            </td>
                            <td align="left" valign="top" style="width: 80px">
                                Edit
                            </td>
                            <td align="left" valign="top" style="width: 80px">
                                Delete
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr id="itemPlaceholder" runat="server" />
                    </tbody>
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tr class="odd">
                    <td align="left" valign="top">
                        <%# Eval("LowerBound")%>
                        <asp:HiddenField ID="hdnMatrixId" runat="server" Value='<%# Eval("MatrixId") %>' />
                    </td>
                    <td align="left">
                        <%# Eval("LowerBoundSign")%>
                    </td>
                    <td align="left" valign="top">
                        <%# Eval("TalentMatrixName")%>
                    </td>
                    <td align="left" valign="top">
                        <%# Eval("TalentMatrixDescription")%>
                    </td>
                    <td align="left">
                        <%# Eval("UpperBoundSign")%>
                    </td>
                    <td align="left" valign="top">
                        <%# Eval("UpperBound")%>
                    </td>
                    <td align="left" valign="top">
                        <%# Eval("PositionInMatrix")%>
                    </td>
                    <td align="left" valign="top">
                        <%# Common.GetBooleanYesNo(Eval("active"))%>
                    </td>
                    <td valign="top">
                        <ul id="icons">
                            <asp:LinkButton runat="server" ID="lnkEdit" CommandName="Edit" class="ui-state-default ui-corner-all"
                                title="Edit Record" CausesValidation="false"><span class="ui-icon ui-icon-pencil">
                            </span></asp:LinkButton>
                        </ul>
                    </td>
                    <td valign="top">
                        <ul id="icons">
                            <asp:LinkButton runat="server" ID="lnkDelete" CommandName="deleteItem" class="ui-state-default ui-corner-all"
                                title="Delete Record" OnClientClick="return confirmDelete();" CausesValidation="false"><span class="ui-icon ui-icon-trash">
                        </span></asp:LinkButton>
                        </ul>
                    </td>
                </tr>
            </ItemTemplate>
            <EditItemTemplate>
                <tbody>
                    <tr class="odd">
                        <td align="left" valign="top">
                            <asp:TextBox runat="server" ID="txtLowerBoundE" Text='<%# Eval("LowerBound")%>' ValidationGroup="Edit"
                                Width="60px" />
                            <asp:RequiredFieldValidator ID="RqdInfoE" runat="server" ErrorMessage="* - required"
                                ValidationGroup="Edit" ControlToValidate="txtLowerBoundE" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="CmpValid" runat="server" ErrorMessage="only numeric values are allowed"
                                ValidationGroup="Edit" ControlToValidate="txtLowerBoundE" ForeColor="Red" Operator="DataTypeCheck"
                                Type="Double"></asp:CompareValidator>
                            <asp:HiddenField ID="hdnMatrixIdE" runat="server" Value='<%# Eval("MatrixId") %>' />
                            <asp:HiddenField ID="hdnLowerBoundSignE" runat="server" Value='<%# Eval("LowerBoundSign") %>' />
                            <asp:HiddenField ID="hdnUpperBoundSignE" runat="server" Value='<%# Eval("UpperBoundSign") %>' />
                        </td>
                        <td align="left" valign="top">
                            <asp:DropDownList ID="ddLowerBoundSignE" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td align="left" valign="top">
                            <asp:TextBox runat="server" ID="txtTalentMatrixNameE" Text='<%# Eval("TalentMatrixName")%>'
                                Width="150px" />
                            <asp:RequiredFieldValidator ID="rqdVald" runat="server" ErrorMessage="* - required"
                                ValidationGroup="Edit" ControlToValidate="txtTalentMatrixNameE" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" valign="top">
                            <asp:TextBox runat="server" ID="txtTalentMatrixDescriptionE" Text='<%# Eval("TalentMatrixDescription")%>'
                                Width="150px" />
                        </td>
                        <td align="left" valign="top">
                            <asp:DropDownList ID="ddUpperBoundSignE" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td align="left" valign="top">
                            <asp:TextBox runat="server" ID="txtUpperBoundE" Text='<%# Eval("UpperBound")%>' ValidationGroup="Edit"
                                Width="60px" />
                            <asp:RequiredFieldValidator ID="rqdUBound" runat="server" ErrorMessage="* - required"
                                ValidationGroup="Edit" ControlToValidate="txtUpperBoundE" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="cvdUBound" runat="server" ErrorMessage="only numeric values are allowed"
                                ValidationGroup="Edit" ControlToValidate="txtUpperBoundE" ForeColor="Red" Operator="DataTypeCheck"
                                Type="Double"></asp:CompareValidator>
                        </td>
                        <td align="left" valign="top">
                            <asp:TextBox runat="server" ID="txtPositionInMatrixE" Text='<%# Eval("PositionInMatrix")%>'
                                ValidationGroup="Edit" Width="50px" />
                            <asp:RequiredFieldValidator ID="rqdTalent" runat="server" ErrorMessage="* - required"
                                ValidationGroup="Edit" ControlToValidate="txtPositionInMatrixE" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="cvdTalent" runat="server" ErrorMessage="only numeric values are allowed"
                                ValidationGroup="Edit" ControlToValidate="txtPositionInMatrixE" ForeColor="Red"
                                Operator="DataTypeCheck" Type="Integer"></asp:CompareValidator>
                        </td>
                        <td align="left" valign="top">
                            <asp:CheckBox ID="chkActiveE" runat="server" Checked='<%# Eval("Active") %>' />
                        </td>
                        <td valign="top">
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkUpdate" CommandName="updateItem" ValidationGroup="Edit"
                                    class="ui-state-default ui-corner-all" title="Update Record"><span class="ui-icon ui-icon-disk"></span></asp:LinkButton>
                            </ul>
                        </td>
                        <td valign="top">
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkCancelUpdate" CommandName="cancelUpdate" class="ui-state-default ui-corner-all"
                                    title="Cancel Update"><span class="ui-icon ui-icon-cancel"></span></asp:LinkButton>
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </EditItemTemplate>
            <InsertItemTemplate>
                <tbody>
                    <tr class="odd">
                        <td align="left" valign="top">
                            <asp:TextBox runat="server" ID="txtLowerBound" ValidationGroup="Edit" Width="60px" /><br />
                            <asp:RequiredFieldValidator ID="RqdInfoI" runat="server" ErrorMessage="* - required"
                                ValidationGroup="Edit" ControlToValidate="txtLowerBound" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="CmpValidI" runat="server" ErrorMessage="only numeric values are allowed"
                                ValidationGroup="Edit" ControlToValidate="txtLowerBound" ForeColor="Red" Operator="DataTypeCheck"
                                Type="Double"></asp:CompareValidator>
                        </td>
                        <td align="left" valign="top">
                            <asp:DropDownList ID="ddLowerBoundSign" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td align="left" valign="top">
                            <asp:TextBox runat="server" ID="txtTalentMatrixName" Width="150px" /><br />
                            <asp:RequiredFieldValidator ID="rqdValdI" runat="server" ErrorMessage="* - required"
                                ValidationGroup="Edit" ControlToValidate="txtTalentMatrixName" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" valign="top">
                            <asp:TextBox runat="server" ID="txtTalentMatrixDescription" Width="150px" />
                        </td>
                        <td align="left" valign="top">
                            <asp:DropDownList ID="ddUpperBoundSign" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td align="left" valign="top">
                            <asp:TextBox runat="server" ID="txtUpperBound" ValidationGroup="Edit" Width="60px" /><br />
                            <asp:RequiredFieldValidator ID="rqdUBoundI" runat="server" ErrorMessage="* - required"
                                ValidationGroup="Edit" ControlToValidate="txtUpperBound" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="cvdUBoundI" runat="server" ErrorMessage="only numeric values are allowed"
                                ValidationGroup="Edit" ControlToValidate="txtUpperBound" ForeColor="Red" Operator="DataTypeCheck"
                                Type="Double"></asp:CompareValidator>
                        </td>
                        <td align="left" valign="top">
                            <asp:TextBox runat="server" ID="txtPositionInMatrix" ValidationGroup="Edit" Width="50px" />
                            <asp:RequiredFieldValidator ID="rqdTalent" runat="server" ErrorMessage="* - required"
                                ValidationGroup="Edit" ControlToValidate="txtPositionInMatrix" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="cvdTalent" runat="server" ErrorMessage="only numeric values are allowed"
                                ValidationGroup="Edit" ControlToValidate="txtPositionInMatrix" ForeColor="Red"
                                Operator="DataTypeCheck" Type="Integer"></asp:CompareValidator>
                        </td>
                        <td align="left" valign="top">
                            <asp:CheckBox ID="chkActive" runat="server" Checked="true" />
                        </td>
                        <td valign="top">
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkSave" CommandName="insertItem" ValidationGroup="Insert"
                                    class="ui-state-default ui-corner-all" title="Add New Matrix"><span class="ui-icon ui-icon-disk"></span></asp:LinkButton>
                            </ul>
                        </td>
                        <td valign="top">
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkCancelUpdate" CommandName="cancelUpdate" class="ui-state-default ui-corner-all"
                                    title="Cancel Update"><span class="ui-icon ui-icon-cancel"></span></asp:LinkButton>
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </InsertItemTemplate>
            <EmptyDataTemplate>
                <p class="errorMsg">
                    There are no records to display.
                </p>
            </EmptyDataTemplate>
        </asp:ListView>
        <br />
        <asp:DataPager ID="pager" runat="server" PagedControlID="lstMatrix" PageSize="15">
            <Fields>
                <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True" ShowNextPageButton="False"
                    ShowPreviousPageButton="False" />
                <asp:NumericPagerField />
                <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True" ShowNextPageButton="False"
                    ShowPreviousPageButton="False" />
            </Fields>
        </asp:DataPager>
        <br />
        <br />
        <p class="errorMsg">
            <asp:Literal ID="lblMsg" runat="server"></asp:Literal></p>
        <div id="tabs-3">
            <p>
                <asp:Button ID="btnNew" runat="server" class="button" Text="Add New Matrix" OnClick="lnkAdd_Click"
                    CausesValidation="False" />
                &nbsp;<asp:Button ID="lnkCancel0" runat="server" class="button" Text="Cancel" OnClick="lnkCancel_Click"
                    CausesValidation="false" />
            </p>
        </div>
        <asp:UpdateProgress ID="UpdateProgressMatrix" runat="server" AssociatedUpdatePanelID="UpdatePanelMatrix">
            <ProgressTemplate>
                <div>
                    <asp:Image ID="imgMatrix" runat="server" ImageUrl="~/ui/images/icons/activity.gif"
                        AlternateText="Please wait.."></asp:Image></div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
