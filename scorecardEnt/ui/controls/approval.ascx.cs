﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using scorecard.controllers;
using scorecard.entities;
using scorecard.interfaces;
using scorecard.implementations;
using HR.Human.Resources;
using System.Net.Mail;
using System.Email.Communication;
using Scorecard.Business.Rules;

using vb = Microsoft.VisualBasic;

namespace scorecard.ui.controls
{
    public partial class approval : System.Web.UI.UserControl
    {
        scoresmanager scoreManager;
        useradmincontroller usersDef;
        goaldefinition goalDef;
        structurecontroller orgunits;
        IBusinessRule bR;
        IScoreRating iR;

        private UserInfo user
        {
            get { return Util.user; }
        }

        public approval()
        {
            scoreManager = new scoresmanager(new scorecard.implementations.scoresImpl());
            usersDef = new useradmincontroller(new useradminImpl());
            goalDef = new goaldefinition(new goalsImpl());
            orgunits = new structurecontroller(new structureImpl());
            bR = new BusinessRuleBL();
            iR = new ScoreRatingBL();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetLabels();
            if (!IsPostBack)
            {
                loadAllWorkflows();
                getScoreCardStatus();
            }
        }

        private void SetLabels()
        {
            lblMsg.Text = "You do not have any pending workflows for your review.";
            lblLstView.Text = string.Empty;
            lblPopupMsg.Text = string.Empty;
        }

        void loadAllWorkflows()
        {
            try
            {
                pnlNoData.Visible = false;

                var company_id = user.CompanyId;
                var userRole = user.UserTypeId;
                var userID = user.UserId;
                var isAdmin = (userRole != Util.getTypeDefinitionsUserTypeAdministrator()) ? false : true;
                var workflows = scoreManager.getApprovalWorkflowItems(company_id, userID, isAdmin);
                var recCount = workflows.Rows.Count;
                //var workflows = scoreManager.getApprovalWorkflowItems().Where(s => s.STATUS == Util.getTypeDefinitionsWorkFlowNewRequestId() && s.COMPANY_ID == company_id);
                //if (userRole != Util.getTypeDefinitionsUserTypeAdministrator())
                //    workflows = workflows.Where(t => t.APPROVER_ID == userID).ToList();
                if (recCount <= 0 || recCount < dtPagerSubmittedWorkflows.PageSize) { dtPagerSubmittedWorkflows.Visible = false; }
                if (recCount <= 0) { pnlNoData.Visible = true; }
                BindControl.BindListView(lvSubmittedWorkflows, workflows);
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        public object GetValue(object dataVal)
        {
            return (dataVal == null) ? string.Empty : dataVal.ToString();
        }

        public string getTypeName(int id)
        {
            return usersDef.appsettings.getApplicationSetting(new entities.applicationsettings { TYPE_ID = id }).NAME;
        }

        public string getApproverName(int id)
        {
            try
            {
                IEmployee emp = new EmployeeBL();
                return emp.GetById(id).FullName; ;
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return "";
            }
        }

        public string getObjectName(int id, int criteria)
        {
            try
            {
                if (id > 0)
                {
                    if (criteria == Util.getTypeDefinitionsRolesTypeID())
                    {
                        IEmployee obj = new EmployeeBL();
                        return obj.GetById(id).FullName;
                    }
                    else if (criteria == Util.getTypeDefinitionsProjectID()) return new projectmanagement(new projectImpl()).get(id).PROJECT_NAME;
                    else return orgunits.getOrgunitDetails(new Orgunit { ID = id }).ORGUNIT_NAME;
                }
            }
            catch (Exception e) { Util.LogErrors(e); }
            return "";
        }

        void loadGoals(int selectedId, int criteria, int submittedId)
        {
            try
            {
                decimal? defDec = null;
                var objectives = scoreManager.getSubmittedScores(new submissionWorkflow
                {
                    ID = submittedId,
                    CRITERIA_TYPE_ID = criteria,
                    SELECTED_ITEM_VALUE = selectedId
                });

                var totalCount = objectives.Count;
                var finalScore = objectives.Sum(u => Math.Round(u.FINAL_SCORE, 2));
                var finalWeightTotal = objectives.Sum(u => u.WEIGHT);
                var finalPercentage = (finalScore / double.Parse(finalWeightTotal.ToString()) * 100);
                var definedWeightedScore = objectives.Sum(u => (u.FINAL_SCORE_AGREED != null) ? u.FINAL_SCORE_AGREED : 0);

                scores totalDetails = new scores
                {
                    GOAL_DESCRIPTION = "<b>Totals</b> ",
                    WEIGHT = finalWeightTotal,
                    FINAL_SCORE = Math.Round(finalPercentage, 2),
                    FINAL_SCORE_AGREED = (definedWeightedScore != null) ? Math.Round((decimal)definedWeightedScore, 2) : defDec,
                    VISIBLE = false
                };

                objectives.Add(totalDetails);
                BindControl.BindListView(lvGoals, objectives);
                getAttachments(submittedId);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void getScoreCardStatus()
        {
            var areaList = goalDef.appsettings.getChildSettings(new applicationsettings { PARENT_ID = Util.getTypeDefinitionsParentWorkFlowApprovalId() }).Where(t => t.TYPE_ID != Util.getTypeDefinitionsWorkFlowNewRequestId()
                && t.TYPE_ID != Util.getTypeDefinitionsWorkFlowSaveForLaterId()).ToList();
            BindControl.BindDropdown(ddlStatus, "NAME", "TYPE_ID", "- select option -", Util.getTypeDefinitionsWorkFlowNewRequestId().ToString(), areaList);
        }

        void loadSetupTypes(DropDownList ddlPerspective)
        {
            try
            {
                var setuptypes = goalDef.appsettings.getChildSettings(new entities.applicationsettings { PARENT_ID = Util.getTypeDefinitionsPerspectivesParentID() });
                BindControl.BindDropdown(ddlPerspective, "NAME", "TYPE_ID", setuptypes);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void loadJobTitles(DropDownList ddlJobTitles)
        {
            try
            {
                IEmployee obj = new EmployeeBL();
                var titles = obj.GetAll(Util.user.OrgId);
                BindControl.BindDropdown(ddlJobTitles, "FullName", "EmployeeId", titles);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void loadStrategicGoals(DropDownList ddlStrategic)
        {
            try
            {
                var strategic = goalDef.listStrategicGoals(new company { COMPANY_ID = Util.user.CompanyId });
                BindControl.BindDropdown(ddlStrategic, "OBJECTIVE", "ID", strategic);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        public string getCurrentStore(int ID)
        {
            try
            {
                var score = scoreManager.getScoresByGoalID(new entities.goals { ID = ID, PERSPECTIVE = int.Parse(hdnValueId.Value) });
                return score.Single().SCORES.ToString();
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return "";
            }
        }

        public string getFinalStore(int ID)
        {
            try
            {
                var score = scoreManager.getScoresByGoalID(new entities.goals { ID = ID, PERSPECTIVE = int.Parse(hdnValueId.Value) });
                return score.Single().FINAL_SCORE.ToString();
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return "0";
            }
        }

        void loadAreas(DropDownList ddlAreas)
        {
            try
            {

                var userRole = Util.user.UserTypeId;
                var userID = int.Parse(Session["userId"].ToString());
                var structure = goalDef.orgstructure.getActiveCompanyOrgStructure(new company { COMPANY_ID = Util.user.CompanyId });

                if (userRole != Util.getTypeDefinitionsUserTypeAdministrator())
                    structure.RowFilter = string.Format("OWNER_ID = {0}", userID);
                BindControl.BindDropdown(ddlAreas, "ORGUNIT_NAME", "ID", structure);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        protected void lvSubmittedWorkflows_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                switch (e.CommandName)
                {
                    case "View":
                        var lblItemSelected = e.Item.FindControl("lblItemSelected") as Label;
                        var lblCriteria = e.Item.FindControl("lblCriteria") as Label;
                        var lblSubmittedId = e.Item.FindControl("lblID") as Label;
                        var hdnOverallComment = e.Item.FindControl("hdnOverallComment") as HiddenField;

                        hdnAreaId.Value = lblCriteria.Text.Trim();
                        hdnValueId.Value = lblItemSelected.Text.Trim();
                        hdnSubmissionId.Value = lblSubmittedId.Text;

                        txtComment.Text = hdnOverallComment.Value;
                        lblID.Text = lblSubmittedId.Text;
                        lblArea.Text = getTypeName(int.Parse(hdnAreaId.Value));
                        lblName.Text = getObjectName(int.Parse(hdnValueId.Value), int.Parse(hdnAreaId.Value));

                        loadGoals(int.Parse(hdnValueId.Value), int.Parse(hdnAreaId.Value), int.Parse(lblSubmittedId.Text));
                        mdlShowScoreDetails.Show();
                        break;
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void getAttachments(int submittedId)
        {
            try
            {
                var attachments = scoreManager.getAllAttachments(new submissionWorkflow
                {
                    ID = submittedId
                });
                dtlstAttachments.DataSource = attachments;
                dtlstAttachments.DataBind();
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        public string getfilePath(string fileName)
        {
            return Util.getConfigurationSettingServerAttachmentsUrl() + fileName;
        }

        bool informationValid()
        {
            bool valid = true;
            if (int.Parse(ddlStatus.SelectedValue) != Util.getTypeDefinitionsWorkFlowApprovalId())
                return valid;

            foreach (ListViewItem item in lvGoals.Items)
            {
                HiddenField hdnUnitMeasureId = item.FindControl("hdnUnitMeasureId") as HiddenField;
                Literal lblError = item.FindControl("lblError") as Literal;
                Literal lblError2 = item.FindControl("lblError2") as Literal;
                TextBox txtManagerScore = item.FindControl("txtManagerScore") as TextBox;
                TextBox txtAgreedScore = item.FindControl("txtAgreedScore") as TextBox;

                lblError.Text = "";
                lblError2.Text = "";

                int unitOfMeasure = int.Parse(hdnUnitMeasureId.Value);
                string mngval = txtManagerScore.Text;
                string agreedval = txtAgreedScore.Text;

                bool mngnotnull = string.IsNullOrEmpty(mngval);
                bool agreednotnull = string.IsNullOrEmpty(agreedval);
                bool isnumericval = bR.UnitOfMeasureRequireNumericValue(unitOfMeasure);
                bool isyesnoval = bR.UnitOfMeasureRequireYesNoValue(unitOfMeasure);

                //manager score validation
                if (!mngnotnull)
                {
                    if (isnumericval)
                    {
                        bool valNumeric = vb.Information.IsNumeric(mngval);
                        if (!valNumeric)
                        {
                            valid = false;
                            lblError.Text = Messages.NumericValueRequired;
                        }
                    }
                    else if (isyesnoval)
                    {
                        bool yesNoValid = bR.UnitOfMeasureRequireYesNoValueValid(mngval);
                        if (!yesNoValid)
                        {
                            valid = false;
                            lblError.Text = Messages.YesNoValueRequired;
                        }
                    }
                }

                //agreed value validation
                if (!agreednotnull)
                {
                    if (isnumericval)
                    {
                        bool valNumeric = vb.Information.IsNumeric(agreedval);
                        if (!valNumeric)
                        {
                            valid = false;
                            lblError2.Text = Messages.NumericValueRequired;
                        }
                    }
                    else if (isyesnoval)
                    {
                        bool yesNoValid = bR.UnitOfMeasureRequireYesNoValueValid(agreedval);
                        if (!yesNoValid)
                        {
                            valid = false;
                            lblError2.Text = Messages.YesNoValueRequired;
                        }
                    }
                }
            }
            return valid;
        }

        bool UpdateWorkFlowItems()
        {
            try
            {
                int lastItemIdx = lvGoals.Items.Count - 1;
                foreach (ListViewItem item in lvGoals.Items)
                {
                    if (item.DataItemIndex == lastItemIdx) return true;
                    var lblID = item.FindControl("lblID") as Label;
                    var lblWeight = item.FindControl("lblWeight") as Label;
                    var lblTarget = item.FindControl("lblTarget") as Label;
                    var lblTotal = item.FindControl("lblTotal") as Label;
                    var lblRatingValue = item.FindControl("lblRatingValue") as Label;
                    var hdnOrgScoreValue = item.FindControl("hdnOrgScoreValue") as HiddenField;
                    var hdnUnitMeasure = item.FindControl("hdnUnitMeasureId") as HiddenField;
                    var txtManagerScore = item.FindControl("txtManagerScore") as TextBox;
                    var txtMngComment = item.FindControl("txtMngComment") as TextBox;
                    var txtAgreedScore = item.FindControl("txtAgreedScore") as TextBox;

                    string mngscore = txtManagerScore.Text;
                    string agreedScore = txtAgreedScore.Text;

                    if (item.ItemType == ListViewItemType.DataItem)
                    {
                        var mngcomment = txtMngComment.Text;
                        var unitMeasure = int.Parse(hdnUnitMeasure.Value);
                        var weight = decimal.Parse(lblWeight.Text);
                        var item_id = int.Parse(lblID.Text);
                        var orgScore = hdnOrgScoreValue.Value;

                        decimal answer = 0;
                        decimal ratingVal = decimal.Parse(lblRatingValue.Text);

                        bool unitmeasureisnumeric = bR.UnitOfMeasureRequireNumericValue(unitMeasure);
                        if (unitmeasureisnumeric)
                        {
                            var target = decimal.Parse(lblTarget.Text);
                            if (string.IsNullOrEmpty(agreedScore)) answer = 0;
                            else
                            {
                                var score = decimal.Parse(agreedScore);
                                answer = bR.FinalScore(unitMeasure, weight, score, target);
                            }
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(mngscore))
                            {
                                answer = 0;
                            }
                            else
                            {
                                if (mngscore.Trim().ToLower() != "yes") answer = 0;
                                else answer = weight;
                            }
                        }

                        decimal myscore = decimal.Parse(answer.ToString());
                        decimal finalAgreedScore = bR.GetDefinedWeightedScore(myscore, ratingVal);
                        scores _scoreDef = new scores
                        {
                            ID = item_id,
                            MANAGER_SCORE = mngscore,
                            AGREED_SCORE = agreedScore,
                            FINAL_SCORE_AGREED = finalAgreedScore,
                            FINAL_SCORE = (double)answer,
                            MANAGER_COMMENT = mngcomment,
                        };

                        scoreManager.updateWorkflowItem(_scoreDef);
                        _scoreDef = null;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
            return false;
        }

        void changeStatus()
        {
            try
            {
                if (int.Parse(ddlStatus.SelectedValue) != Util.getTypeDefinitionsWorkFlowNewRequestId())
                {
                    bool valid = informationValid();
                    if (!valid)
                    {
                        lblPopupMsg.Text = "Supply valid information.";
                        mdlShowScoreDetails.Show();
                        return;
                    }

                    string comment = txtComment.Text;

                    int submissionId = int.Parse(lblID.Text);
                    int statusId = int.Parse(ddlStatus.SelectedValue);
                    var result = scoreManager.updateWorkflowStatus(new submissionWorkflow
                    {
                        STATUS = statusId,
                        ID = submissionId,
                        OVERALL_COMMENT = comment
                    });
                    if (result > 0)
                    {
                        bool saved = UpdateWorkFlowItems();
                        if (saved)
                        {
                            loadAllWorkflows();
                            int areaId = int.Parse(hdnAreaId.Value);
                            int valId = int.Parse(hdnValueId.Value);
                            int userId = 0;
                            if (areaId == Util.getTypeDefinitionsOrgUnitsTypeID())
                                userId = new structurecontroller(new structureImpl()).getOrgunitDetails(new Orgunit { ID = valId }).OWNER_ID;
                            else if (areaId == Util.getTypeDefinitionsProjectID())
                                userId = (int)new projectmanagement(new projectImpl()).get(valId).RESPONSIBLE_PERSON_ID;
                            else if (areaId == Util.getTypeDefinitionsRolesTypeID())
                                userId = valId;
                            IEmployee obj = new EmployeeBL();
                            string statusDesc = ddlStatus.SelectedItem.Text;
                            var user = obj.GetById(userId);
                            sendMail(user, statusDesc);
                        }
                        else//reverse updated
                        {
                            int newrequestId = Util.getTypeDefinitionsWorkFlowNewRequestId();
                            scoreManager.updateWorkflowStatus(new submissionWorkflow
                                {
                                    STATUS = newrequestId,
                                    ID = submissionId,
                                    OVERALL_COMMENT = comment
                                });
                            pnlNoData.Visible = true;
                            lblMsg.Text = Messages.GetSaveFailedMessage();
                            mdlShowScoreDetails.Hide();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void sendMail(Employee user, string statusDesc)
        {
            try
            {
                string result = string.Empty;
                int emailId = Util.getWorkflowApprovalEmailId();

                MailAddressCollection addy = new MailAddressCollection();
                addy.Add(new MailAddress(user.EmailAddress, user.FullName));

                object[] subjectData = { statusDesc.ToLower() };
                object[] mydata = { statusDesc };

                IMailRelay objm = new MailRelayBL();
                MailRelay obj = new MailRelay
                {
                    MailId = emailId,
                    Recipient = addy,
                    Data = mydata,
                    SubjectData = subjectData
                };
                objm.SendMail(obj, out result);
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void UpdateWorkflowItem(ListViewCommandEventArgs e)
        {
            try
            {
                var lblID = e.Item.FindControl("lblID") as Label;
                var lblWeight = e.Item.FindControl("lblWeight") as Label;
                var lblTarget = e.Item.FindControl("lblTarget") as Label;
                var lblTotal = e.Item.FindControl("lblTotal") as Label;
                var lblFinalAgreedTotal = e.Item.FindControl("lblFinalAgreedTotal") as Label;
                var lblRatingValue = e.Item.FindControl("lblRatingValue") as Label;
                var hdnOrgScoreValue = e.Item.FindControl("hdnOrgScoreValue") as HiddenField;
                var hdnUnitMeasure = e.Item.FindControl("hdnUnitMeasureId") as HiddenField;
                var txtManagerScore = e.Item.FindControl("txtManagerScore") as TextBox;
                var txtMngComment = e.Item.FindControl("txtMngComment") as TextBox;
                var txtAgreedScore = e.Item.FindControl("txtAgreedScore") as TextBox;

                if (e.Item.ItemType == ListViewItemType.DataItem)
                {
                    decimal answer = 0;

                    var mngcomment = txtMngComment.Text;
                    var unitMeasure = int.Parse(hdnUnitMeasure.Value);
                    var weight = decimal.Parse(lblWeight.Text);
                    var item_id = int.Parse(lblID.Text);
                    var orgScore = hdnOrgScoreValue.Value;
                    var ratingValue = decimal.Parse(lblRatingValue.Text);

                    string mngscore = txtManagerScore.Text;
                    string agreedScore = txtAgreedScore.Text;

                    bool unitmeasureisnumeric = bR.UnitOfMeasureRequireNumericValue(unitMeasure);
                    if (unitmeasureisnumeric)
                    {
                        var target = decimal.Parse(lblTarget.Text);
                        var score = decimal.Parse(agreedScore);
                        answer = bR.FinalScore(unitMeasure, weight, score, target);
                    }
                    else
                    {
                        if (mngscore.Trim().ToLower() != "yes") answer = 0;
                        else answer = weight;
                    }

                    decimal myscore = decimal.Parse(answer.ToString());
                    decimal finalAgreedScore = bR.GetDefinedWeightedScore(myscore, ratingValue);
                    scores _scoreDef = new scores
                    {
                        ID = item_id,
                        MANAGER_SCORE = mngscore,
                        AGREED_SCORE = agreedScore,
                        FINAL_SCORE_AGREED = finalAgreedScore,
                        FINAL_SCORE = (double)answer,
                        MANAGER_COMMENT = mngcomment
                    };

                    int result = scoreManager.updateWorkflowItem(_scoreDef);
                    _scoreDef = null;

                    if (result > 0)
                    {
                        loadGoals(int.Parse(hdnValueId.Value), int.Parse(hdnAreaId.Value), int.Parse(hdnSubmissionId.Value));
                        mdlShowScoreDetails.Show();
                        lblLstView.Text = Messages.GetSaveMessage();
                    }

                    #region updating labels
                    //if (result > 0)
                    //{
                    //    lblTotal.Text = Math.Round(myscore, 2).ToString();
                    //    decimal finalAgreed = decimal.Parse(lblFinalAgreedTotal.Text);
                    //    try
                    //    {
                    //        ListViewItem totalsLv = lvGoals.Items[lvGoals.Items.Count - 1];
                    //        if (totalsLv != null)
                    //        {
                    //            var lblFinal_AgreedTotal = totalsLv.FindControl("lblFinalAgreedTotal") as Label;
                    //            if (lblFinal_AgreedTotal != null)
                    //            {
                    //                decimal totalV = decimal.Parse(lblFinal_AgreedTotal.Text);
                    //                decimal ntotalV = totalV - finalAgreed + finalAgreedScore;
                    //                lblFinal_AgreedTotal.Text = Math.Round(ntotalV, 2).ToString();
                    //            }
                    //        }
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        Util.LogErrors(ex);
                    //    }
                    //    lblTotal.Text = answer.ToString();
                    //    lblFinalAgreedTotal.Text = Math.Round(finalAgreedScore, 2).ToString();
                    //}
                    #endregion
                }
            }
            catch (Exception ex)
            {
                lblLstView.Text = Messages.GetErrorMessage();
                Util.LogErrors(ex);
            }
            finally
            {
                mdlShowScoreDetails.Show();
            }
        }

        void UpdateOveralComment()
        {
            try
            {
                int submissionId = int.Parse(hdnSubmissionId.Value);
                string comment = txtComment.Text;

                var result = scoreManager.updateWorkflowComment(new submissionWorkflow
                {
                    ID = submissionId,
                    OVERALL_COMMENT = comment
                });

                lblPopupMsg.Text = (result > 0) ? Messages.GetSaveMessage() : Messages.GetSaveFailedMessage();
                //txtComment.Text = comment;
                if (result > 0)
                {
                    foreach (ListViewDataItem item in lvSubmittedWorkflows.Items)
                    {
                        Label lblID = item.FindControl("lblID") as Label;
                        HiddenField hdnOverallComment = item.FindControl("hdnOverallComment") as HiddenField;
                        if (lblID != null)
                            if (lblID.Text.Equals(submissionId.ToString()))
                            {
                                hdnOverallComment.Value = comment;
                                break;
                            }
                    }
                }
            }
            catch (Exception ex)
            {
                lblPopupMsg.Text = Messages.GetErrorMessage();
                Util.LogErrors(ex);
            }
            finally
            {
                mdlShowScoreDetails.Show();
            }
        }

        protected void btnSaveAll_Click(object sender, EventArgs e)
        {
            bool saved = UpdateWorkFlowItems();
            lblLstView.Text = saved ? Messages.GetSaveMessage() : Messages.GetSaveFailedMessage();
            mdlShowScoreDetails.Show();
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            changeStatus();
        }

        protected void btnUpdateComment_Click(object sender, EventArgs e)
        {
            UpdateOveralComment();
        }

        protected void lnkCancel_Click(object sender, EventArgs e)
        {
            pnlNoData.Visible = false;
            mdlShowScoreDetails.Hide();
        }

        protected void lnkDone_Click(object sender, EventArgs e)
        {
            Response.Redirect("index.aspx");
        }

        protected void lvGoals_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("updateItem"))
                UpdateWorkflowItem(e);
        }
    }
}