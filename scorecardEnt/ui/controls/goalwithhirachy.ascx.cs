﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.controllers;
using scorecard.implementations;
using scorecard.entities;
using Scorecard.Business.Rules;
using System.Text;
using vb = Microsoft.VisualBasic;

namespace scorecard.ui.controls
{
    public partial class goalwithhirachy : System.Web.UI.UserControl
    {
        goaldefinition goalDef;
        scoresmanager scoreManager;
        IBusinessRule bR;
        string envContextHolder;

        public goalwithhirachy()
        {
            goalDef = new goaldefinition(new goalsImpl());
            scoreManager = new scoresmanager(new scorecard.implementations.scoresImpl());
            bR = new BusinessRuleBL();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ResetLabels();
            if (!IsPostBack)
            {
                getGoalsCriteriaTypes();
                initialiseObjects();
            }
        }

        private void initialiseObjects()
        {
            SetCache(true);
        }

        private void ResetLabels()
        {
            lblMsg.Text = "";
            LblError.Text = "";
        }

        public string getTypeName(object id)
        {
            if (id == null || id == DBNull.Value) return string.Empty;
            else if (int.Parse(id.ToString()) <= 0) return string.Empty;
            return goalDef.appsettings.getApplicationSetting(
                new entities.applicationsettings
                {
                    TYPE_ID = int.Parse(id.ToString())
                }).NAME;
        }

        void getGoalsCriteriaTypes()
        {
            var areaList = goalDef.appsettings.getChildSettings(new applicationsettings { PARENT_ID = Util.getTypeDefinitionsGoalAreaTypes() }).ToList();
            BindControl.BindDropdown(ddlArea, "NAME", "TYPE_ID", "- select -", "-1", areaList);
            if (ddlArea.SelectedIndex == 0)
                ddlValueTypes.Items.Add(new ListItem { Text = "- select -", Value = "-1" });
        }

        void loadJobTitles(DropDownList ddlJobTitles)
        {
            try
            {
                UserInfo user = Session["UserInfo"] as UserInfo;
                var titles = goalDef.jobtitles.getAllCompanyJobTitles(new company { COMPANY_ID = user.CompanyId }).Where(j => j.ACTIVE == true);
                BindControl.BindDropdown(ddlJobTitles, "NAME", "ID", "- select -", "-1", titles);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void loadWeights(DropDownList ddlWeights)
        {
            if (ddlWeights != null)
                for (int i = 1; i <= 100; i++)
                    ddlWeights.Items.Add(new ListItem { Text = i.ToString() + "%", Value = i.ToString() });
        }

        void loadStrategicGoals(DropDownList ddlStrategic)
        {
            try
            {
                var strategic = goalDef.listStrategicGoals(new company { COMPANY_ID = Util.user.CompanyId });
                ddlStrategic.DataSource = strategic;
                ddlStrategic.DataTextField = "OBJECTIVE";
                ddlStrategic.DataValueField = "ID";
                ddlStrategic.DataBind();
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void loadUnitsOfMeasure(DropDownList ddlMeasure)
        {
            try
            {
                var unitsOfMeasure = goalDef.appsettings.getChildSettings(new applicationsettings
                {
                    PARENT_ID = Util.getTypeDefinitionsUnitOfMeasureTypesID()
                }).ToList();
                BindControl.BindDropdown(ddlMeasure, "NAME", "TYPE_ID", unitsOfMeasure);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void loadProjects(int companyId)
        {
            projectmanagement proj = new projectmanagement(new projectImpl());
            UserInfo user = Session["UserInfo"] as UserInfo;
            int adminRoleId = Util.getTypeDefinitionsUserTypeAdministrator();

            ddlValueTypes.DataTextField = "PROJECT_NAME";
            ddlValueTypes.DataValueField = "PROJECT_ID";
            int roleId = (int)Util.user.UserTypeId;
            IEnumerable<project> projL = proj.get(companyId, true);
            if (adminRoleId != user.UserTypeId) projL = projL.Where(c => c.RESPONSIBLE_PERSON_ID == user.UserId);
            BindControl.BindDropdown(ddlValueTypes, "PROJECT_NAME", "PROJECT_ID", "- select -", "-1", projL);
        }

        void loadAreas(DropDownList ddOrgUnit)
        {
            try
            {
                var userRole = Util.user.UserTypeId;
                var userID = Util.user.UserId;
                var structure = goalDef.orgstructure.getActiveCompanyOrgStructure(Util.user.CompanyId);
                if (userRole != Util.getTypeDefinitionsUserTypeAdministrator())
                    structure.RowFilter = string.Format("OWNER_ID = {0}", userID);
                BindControl.BindDropdown(ddOrgUnit, "ORGUNIT_NAME", "ID", "- select -", "-1", structure);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void loadInitialPerspectives()
        {

            var ddlStrategic = lvGoals.InsertItem.FindControl("ddlStrategic") as DropDownList;
            var ddlAreas = lvGoals.InsertItem.FindControl("ddlArea") as DropDownList;
            var ddlRoles = lvGoals.InsertItem.FindControl("ddlRole") as DropDownList;
            var ddlWeights = lvGoals.InsertItem.FindControl("ddlWeight") as DropDownList;
            var ddlUnitOfMeasure = lvGoals.InsertItem.FindControl("ddlUnitOfMeasure") as DropDownList;

            if (ddlAreas != null) loadAreas(ddlAreas);
            if (ddlStrategic != null) loadStrategicGoals(ddlStrategic);
            if (ddlRoles != null) loadJobTitles(ddlRoles);
            if (ddlWeights != null) loadWeights(ddlWeights);
            if (ddlUnitOfMeasure != null) loadUnitsOfMeasure(ddlUnitOfMeasure);
        }

        List<entities.goals> getGoals()
        {
            if (ddlArea.SelectedIndex > 0 && ddlValueTypes.SelectedIndex > 0)
            {
                var data = goalDef.listGeneralGoals(new company { COMPANY_ID = Util.user.CompanyId });
                var objectives = data.Where(t => t.AREA_TYPE_ID == int.Parse(ddlArea.SelectedValue) &&
                                            t.AREA_ID == int.Parse(ddlValueTypes.SelectedValue)).ToList();
                return objectives;
            }
            return new List<entities.goals>();
        }

        void SetCache(bool reload)
        {
            if (Cache["Data"] == null) reload = true;
            if (reload) Cache["Data"] = getGoals();
        }

        void loadGoals(bool reload)
        {
            try
            {
                SetCache(reload);
                var objectives = (Cache["Data"] as List<entities.goals>).Where(t => t.PARENT_ID == null).ToList();
                if (objectives.Count > 0) SetContextId(objectives[0]);
                dtPagerlvGoals.Visible = (objectives.Count > dtPagerlvGoals.PageSize) ? true : false;
                BindControl.BindListView(lvGoals, objectives);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void ShowMessage(Literal lbl, string msg)
        {
            lbl.Text = msg;
        }

        void ShowMessage(Label lbl, string msg)
        {
            lbl.Text = msg;
        }

        public string getRoleName(int roleID)
        {
            return goalDef.jobtitles.getJobTitleInfo(new JobTitle { ID = roleID }).NAME;
        }

        public string getAreaName(int areaID)
        {
            return goalDef.orgstructure.getOrgunitDetails(new Orgunit { ID = areaID }).ORGUNIT_NAME;
        }

        public string getStrategicObjective(int id)
        {
            var obj = goalDef.getStrategicById(id);
            return obj.OBJECTIVE;
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            if (CanAdd(0, 0))
            {
                lvGoals.InsertItemPosition = InsertItemPosition.LastItem;
                loadGoals(false);
                loadInitialPerspectives();
            }
        }

        bool infoValid(string description, string target, int unitofmeasure, bool hasSubGoals)
        {
            bool valid = true;
            StringBuilder strB = new StringBuilder("");
            if (!hasSubGoals && string.IsNullOrEmpty(description))
            {
                valid = false;
                strB.Append("- Goal is required<br/>");
            }

            if (!hasSubGoals && bR.UnitOfMeasureRequireNumericValue(unitofmeasure))
            {
                if (!vb.Information.IsNumeric(target))
                {
                    valid = false;
                    strB.Append("- Target should be numeric<br/>");
                }
            }
            else if (!hasSubGoals && bR.UnitOfMeasureRequireYesNoValue(unitofmeasure))
            {
                if (!bR.UnitOfMeasureRequireYesNoValueValid(target))
                {
                    valid = false;
                    strB.Append("- Invalid Target value. Only Yes or No is allowed<br/>");
                }
            }

            LblError.Text = valid ? string.Empty : string.Format("<p style='color:Red;'>Please correct the following:<br/></p>{0}", strB.ToString());
            return valid;
        }

        void addSubMeasure(bool update)
        {
            try
            {
                string desc = txtDescription.Text.Trim();
                string rationale = txtRational.Text.Trim();
                string target = txtTarget.Text.Trim();

                int areaId = int.Parse(ddlValueTypes.SelectedValue);
                int areaTypeId = int.Parse(ddlArea.SelectedValue);
                int unitofmeasure = int.Parse(ddlUnitOfMeasures.SelectedValue);
                int strategicId = update ? 0 : int.Parse(hdnStrategicId.Value);
                int parentId = update ? 0 : int.Parse(hdnParentId.Value);
                int weight = update ? 0 : int.Parse(hdnWeightS.Value);
                int goalId = update ? int.Parse(hdnGoalId.Value) : 0;

                var goalD = new scorecard.entities.goals
                {
                    ID = goalId,
                    PARENT_ID = parentId,
                    AREA_TYPE_ID = areaTypeId,
                    AREA_ID = areaId,
                    COMPANY_ID = Util.user.CompanyId,
                    GOAL_DESCRIPTION = desc,
                    TARGET = target,
                    UNIT_MEASURE = unitofmeasure,
                    WEIGHT = weight,
                    STRATEGIC_ID = strategicId,
                    ORG_ID = areaTypeId,
                    RATIONAL = rationale
                };

                int added = !update ? goalDef.defineSubGeneralGoal(goalD) : goalDef.updateSubGeneralGoal(goalD);
                ShowMessage(LblError, added > 0 ? Messages.GetSaveMessage() : Messages.GetSaveFailedMessage());
                if (added > 0)
                    loadGoals(true);
            }
            catch (Exception ex)
            {
                ShowMessage(LblError, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        void createNewGoal(ListViewCommandEventArgs e)
        {
            try
            {
                int? defInt = null;

                var txtDescription = e.Item.FindControl("txtDescription") as TextBox;
                var txtRational = e.Item.FindControl("txtRational") as TextBox;
                var txtTarget = e.Item.FindControl("txtTarget") as TextBox;
                var txtUnitOfMeasure = e.Item.FindControl("ddlUnitOfMeasure") as DropDownList;
                var ddlWeight = e.Item.FindControl("ddlWeight") as DropDownList;
                var ddlStrategic = e.Item.FindControl("ddlStrategic") as DropDownList;
                var envContext = string.IsNullOrEmpty(txtEnvContext.Text) ? string.Empty : txtEnvContext.Text.Trim();
                var contextId = string.IsNullOrEmpty(hdnContextId.Value) ? defInt : int.Parse(hdnContextId.Value);
                var rational = txtRational.Text;
                var description = txtDescription.Text;
                var target = txtTarget.Text;

                int areaId = int.Parse(ddlValueTypes.SelectedValue);
                int unitMeasure = int.Parse(txtUnitOfMeasure.SelectedValue);

                bool valid = infoValid(description, target, unitMeasure, false);
                if (CanAdd(int.Parse(ddlWeight.SelectedValue), 0) && valid)
                {
                    int envContextId = goalDef.defineGeneralGoal(new scorecard.entities.goals
                     {
                         AREA_TYPE_ID = int.Parse(ddlArea.SelectedValue),
                         AREA_ID = int.Parse(ddlValueTypes.SelectedValue),
                         COMPANY_ID = Util.user.CompanyId,
                         GOAL_DESCRIPTION = txtDescription.Text.Trim(),
                         TARGET = txtTarget.Text.Trim(),
                         UNIT_MEASURE = int.Parse(txtUnitOfMeasure.SelectedValue),
                         WEIGHT = int.Parse(ddlWeight.SelectedValue),
                         STRATEGIC_ID = int.Parse(ddlStrategic.SelectedValue),
                         ORG_ID = int.Parse(ddlArea.SelectedValue),
                         ENV_CONTEXT = envContext,
                         ENV_CONTEXT_ID = contextId,
                         RATIONAL = rational
                     });
                    lvGoals.InsertItemPosition = InsertItemPosition.None;
                    loadGoals(true);
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void updateGoal(ListViewCommandEventArgs e)
        {
            try
            {
                int? defInt = null;
                var id = e.Item.FindControl("lblID") as Label;
                var txtDescription = e.Item.FindControl("txtEditDescription") as TextBox;
                var txtEditRational = e.Item.FindControl("txtEditRational") as TextBox;
                var txtTarget = e.Item.FindControl("txtEditTarget") as TextBox;
                var txtUnitOfMeasure = e.Item.FindControl("ddlUnitOfMeasure") as DropDownList;
                var ddlWeight = e.Item.FindControl("ddlEditWeight") as DropDownList;
                var ddlStrategic = e.Item.FindControl("ddlEditStrategic") as DropDownList;
                var rational = txtEditRational.Text;
                var envContext = txtEnvContext.Text;
                var contextId = string.IsNullOrEmpty(hdnContextId.Value) ? defInt : int.Parse(hdnContextId.Value);

                bool hasSubGoals = bool.Parse((e.Item.FindControl("hdnHasSubGoals") as HiddenField).Value);

                string target = txtTarget.Text;
                string description = txtDescription.Text;

                int? unitOfMeasure = hasSubGoals ? defInt : int.Parse(txtUnitOfMeasure.SelectedValue);
                int weight = int.Parse(ddlWeight.SelectedValue);
                int myId = int.Parse(id.Text);
                int strategicId = int.Parse(ddlStrategic.SelectedValue);
                int areaId = int.Parse(ddlValueTypes.SelectedValue);
                int areaTypeId = int.Parse(ddlArea.SelectedValue);

                bool valid = infoValid(description, target, (unitOfMeasure == null) ? 0 : (int)unitOfMeasure, hasSubGoals);
                bool addItem = CanAdd(weight, myId);
                if (addItem && valid)
                {
                    goalDef.updateGeneralGoal(new scorecard.entities.goals
                    {
                        ID = myId,
                        AREA_TYPE_ID = areaTypeId,
                        AREA_ID = areaId,
                        COMPANY_ID = Util.user.CompanyId,
                        GOAL_DESCRIPTION = description,
                        TARGET = target,
                        UNIT_MEASURE = unitOfMeasure,
                        WEIGHT = weight,
                        STRATEGIC_ID = strategicId,
                        RATIONAL = rational,
                        ENV_CONTEXT = envContext,
                        ENV_CONTEXT_ID = contextId,
                        HAS_SUB_GOALS = hasSubGoals
                    });
                    lvGoals.InsertItemPosition = InsertItemPosition.None;
                    lvGoals.EditIndex = -1;
                    loadGoals(true);
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void deleteGoalObjective(ListViewCommandEventArgs e)
        {
            try
            {
                var lblID = e.Item.FindControl("lblID") as Label;
                deleteGoal(int.Parse(lblID.Text));
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void deleteGoal(int goalId)
        {
            try
            {
                int deleted = goalDef.deleteGeneralGoal(new scorecard.entities.goals
                {
                    ID = goalId,
                    UPDATE_BY = Util.user.LoginId
                });

                ShowMessage(LblError, deleted > 0 ? Messages.GetDeleteMessage() : Messages.GetDeleteFailedMessage());
                if (deleted > 0)
                    loadGoals(true);
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void cancelUpdate()
        {
            lvGoals.EditIndex = -1;
            lvGoals.InsertItemPosition = InsertItemPosition.None;
            loadGoals(false);
        }

        bool CanAdd(int currentlyAddedWeight, int goalId)
        {
            lblMsg.Text = "";
            const int maximum = 100;
            var goals = getGoals();
            int total = goals.Where(i => i.ID != goalId && i.PARENT_ID == null).Sum(g => g.WEIGHT);
            int totalWeighting = currentlyAddedWeight + total;
            if ((totalWeighting == maximum && currentlyAddedWeight == 0) ||
                (totalWeighting > maximum && currentlyAddedWeight != 0))
            {
                lblMsg.Text = string.Format("Your total weight should not exceed {0}%. You have a total of {1}%", maximum, totalWeighting);
                return false;
            }
            return true;
        }

        #region ListView Event Handling Methods

        protected void lvGoal_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            LblError.Text = "";
            envContextHolder = txtEnvContext.Text;
            if (e.Item.ItemType == ListViewItemType.InsertItem ||
                e.Item.ItemType == ListViewItemType.DataItem)
            {
                var strategicId = e.CommandArgument.ToString();
                switch (e.CommandName)
                {
                    case "addNewGoal":
                        createNewGoal(e);
                        break;
                    case "updateGoals":
                        updateGoal(e);
                        break;
                    case "deleteGoal":
                        deleteGoalObjective(e);
                        break;
                    case "cancelUpdate":
                        cancelUpdate();
                        break;
                    case "expand":
                        clearForm();
                        showSubMeasureForm(strategicId, e);
                        break;
                }
            }
        }

        protected void lvGoals_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListViewItemType.DataItem)
                {
                    if (lvGoals.EditIndex == -1)
                    {
                        int parentId = int.Parse((e.Item.FindControl("lblID") as Label).Text);
                        bool hdnHasSubGoals = bool.Parse((e.Item.FindControl("hdnHasSubGoals") as HiddenField).Value);
                        Repeater rptData = e.Item.FindControl("rptMeasureData") as Repeater;
                        if (rptData != null && hdnHasSubGoals)
                        {
                            var data = (Cache["Data"] as List<entities.goals>).Where(t => t.PARENT_ID == parentId).ToList();
                            BindControl.BindRepeater(rptData, data);
                            rptData.Visible = (data.Count <= 0) ? false : true;
                        }
                    }
                    else
                    {
                        int parentId = int.Parse((e.Item.FindControl("lblID") as Label).Text);
                        bool hdnHasSubGoals = bool.Parse((e.Item.FindControl("hdnHasSubGoals") as HiddenField).Value);
                        Repeater rptData = e.Item.FindControl("rptMeasureData") as Repeater;
                        if (rptData != null && hdnHasSubGoals)
                        {
                            var data = (Cache["Data"] as List<entities.goals>).Where(t => t.PARENT_ID == parentId).ToList();
                            BindControl.BindRepeater(rptData, data);
                            rptData.Visible = (data.Count <= 0) ? false : true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        protected void lvGoal_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            try
            {
                lvGoals.EditIndex = e.NewEditIndex;
                lvGoals.InsertItemPosition = InsertItemPosition.None;
                loadGoals(false);
                txtEnvContext.Text = envContextHolder;

                var ddlStrategic = lvGoals.Items[e.NewEditIndex].FindControl("ddlEditStrategic") as DropDownList;
                var ddlWeights = lvGoals.Items[e.NewEditIndex].FindControl("ddlEditWeight") as DropDownList;
                var ddlUnitOfMeasure = lvGoals.Items[e.NewEditIndex].FindControl("ddlUnitOfMeasure") as DropDownList;

                HiddenField hdnWeight = lvGoals.Items[e.NewEditIndex].FindControl("hdnWeight") as HiddenField;
                HiddenField hdnUnitMeasure = lvGoals.Items[e.NewEditIndex].FindControl("hdnUnitMeasure") as HiddenField;
                HiddenField hdnStrategic = lvGoals.Items[e.NewEditIndex].FindControl("hdnStrategic") as HiddenField;
                HiddenField hdnHasSubGoals = lvGoals.Items[e.NewEditIndex].FindControl("hdnHasSubGoals") as HiddenField;
                bool hasSubGoals = bool.Parse(hdnHasSubGoals.Value);

                if (ddlStrategic != null)
                {
                    loadStrategicGoals(ddlStrategic);
                    if (hdnStrategic != null)
                        if (ddlStrategic.Items.FindByValue(hdnStrategic.Value) != null)
                            ddlStrategic.SelectedValue = hdnStrategic.Value;
                }
                if (ddlWeights != null)
                {
                    loadWeights(ddlWeights);
                    if (hdnWeight != null)
                        if (ddlWeights.Items.FindByValue(hdnWeight.Value) != null)
                            ddlWeights.SelectedValue = hdnWeight.Value;
                }
                if (ddlUnitOfMeasure != null)
                {
                    loadUnitsOfMeasure(ddlUnitOfMeasure);
                    ddlUnitOfMeasure.Visible = !hasSubGoals;
                    if (hdnUnitMeasure != null)
                        if (ddlUnitOfMeasure.Items.FindByValue(hdnUnitMeasure.Value) != null)
                            ddlUnitOfMeasure.SelectedValue = hdnUnitMeasure.Value;
                }
                #region Old Code
                //if (lvGoals.EditItem.FindControl("lblID") != null)
                //{
                //    string id = (lvGoals.EditItem.FindControl("lblID") as Label).Text;
                //    entities.goals goal = new goaldefinition(new goalsImpl()).getGoalById(int.Parse(id));
                //    if (ddlStrategic.Items.FindByValue(goal.STRATEGIC_ID.ToString()) != null)
                //        ddlStrategic.SelectedValue = goal.STRATEGIC_ID.ToString();
                //    if (ddlWeights.Items.FindByValue(goal.WEIGHT.ToString()) != null)
                //        ddlWeights.SelectedValue = goal.WEIGHT.ToString();
                //    if (ddlUnitOfMeasure.Items.FindByValue(goal.UNIT_MEASURE.ToString()) != null)
                //        ddlUnitOfMeasure.SelectedValue = goal.UNIT_MEASURE.ToString();
                //}
                #endregion
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        protected void lvGoal_PagePropertiesChanged(object sender, EventArgs e)
        {
            loadGoals(false);
        }

        protected void rptMeasureData_OnItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                HiddenField hdnSubId = e.Item.FindControl("hdnSubId") as HiddenField;
                HiddenField hdnHasSubGoals = e.Item.FindControl("hdnHasSubGoals") as HiddenField;
                HiddenField hdnParentId = e.Item.FindControl("hdnParentId") as HiddenField;
                if (e.CommandName.ToLower().Equals("EditGoal".ToLower()))
                {
                    clearForm();
                    loadUnitsOfMeasure(ddlUnitOfMeasures);

                    int goalId = int.Parse(hdnSubId.Value);
                    var goalInfo = goalDef.getGoalById(goalId);
                    var parentGoalInfo = goalDef.getGoalById((int)goalInfo.PARENT_ID);

                    LblPerformance.Text = parentGoalInfo.GOAL_DESCRIPTION;
                    LblWeight.Text = string.Format("{0} %", parentGoalInfo.WEIGHT);
                    LblStrategic.Text = getStrategicObjective(parentGoalInfo.STRATEGIC_ID);

                    txtDescription.Text = goalInfo.GOAL_DESCRIPTION;
                    txtTarget.Text = goalInfo.TARGET;
                    if (ddlUnitOfMeasures.Items.FindByValue(goalInfo.UNIT_MEASURE.ToString()) != null)
                        ddlUnitOfMeasures.SelectedValue = goalInfo.UNIT_MEASURE.ToString();

                    hdnGoalId.Value = goalId.ToString();
                    hdnStrategicId.Value = goalInfo.STRATEGIC_ID.ToString();
                    hdnWeightS.Value = goalInfo.WEIGHT.ToString();
                    hdnParentId.Value = goalInfo.PARENT_ID.ToString();

                    lnkSaveSub.CommandArgument = "1";
                    lnkSaveSub.Text = "Update";
                    mdlShowSubMeasureInfo.Show();
                }
                else if (e.CommandName.ToLower().Equals("DeleteGoal".ToLower()))
                {
                    int goalId = int.Parse(hdnSubId.Value);
                    deleteGoal(goalId);
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        #endregion

        void clearForm()
        {
            txtDescription.Text = "";
            txtRational.Text = "";
            txtTarget.Text = "";
        }

        void showSubMeasureForm(string strategicId, ListViewCommandEventArgs e)
        {
            try
            {
                var parentId = (e.Item.FindControl("lblID") as Label).Text;
                var strategy = (e.Item.FindControl("lblStrategic") as Literal).Text;
                var goalDesc = (e.Item.FindControl("lblGoalDesc") as Literal).Text;
                var weight = (e.Item.FindControl("hdnWeightVal") as HiddenField).Value;

                hdnStrategicId.Value = strategicId;
                hdnParentId.Value = parentId;
                hdnWeightS.Value = weight;

                LblStrategic.Text = strategy;
                LblPerformance.Text = goalDesc;
                LblWeight.Text = string.Format("{0} %", weight);
                loadUnitsOfMeasure(ddlUnitOfMeasures);

                lnkSaveSub.CommandArgument = "0";
                lnkSaveSub.Text = "Submit";
                mdlShowSubMeasureInfo.Show();
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            lvGoals.EditIndex = -1;
            ddlValueTypes.Items.Clear();
            clearValueHolders();
            setCriteriaSelected();
        }

        void clearValueHolders()
        {
            hdnContextId.Value = string.Empty;
            txtEnvContext.Text = string.Empty;
        }

        void setCriteriaSelected()
        {
            try
            {
                int areaId = int.Parse(ddlArea.SelectedValue);
                if (areaId == Util.getTypeDefinitionsRolesTypeID())
                    loadJobTitles(ddlValueTypes);
                else if (areaId == Util.getTypeDefinitionsProjectID())
                    loadProjects(Util.user.CompanyId);
                else loadAreas(ddlValueTypes);

                lvGoals.InsertItemPosition = InsertItemPosition.None;
                loadGoals(true);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        protected void ddlValueTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            lvGoals.InsertItemPosition = InsertItemPosition.None;
            clearValueHolders();
            if (ddlValueTypes.SelectedIndex > 0)
                loadGoals(true);
        }

        private void SetContextId(scorecard.entities.goals obj)
        {
            if (obj.ENV_CONTEXT_ID == null)
            {
                var envData = goalDef.getEnvironmentContext(new environmentcontext
                {
                    COMPANY_ID = Util.user.CompanyId,
                    AREA_ID = int.Parse(ddlArea.SelectedValue),
                    AREA_TYPE_ID = int.Parse(ddlValueTypes.SelectedValue)
                });
                if (envData.ENV_CONTEXT_ID > 0)
                {
                    hdnContextId.Value = envData.ENV_CONTEXT_ID.ToString();
                    txtEnvContext.Text = envData.ENV_CONTEXT;
                }
            }
            else
            {
                hdnContextId.Value = obj.ENV_CONTEXT_ID.ToString();
                txtEnvContext.Text = obj.ENV_CONTEXT;
            }
        }

        protected void lnkCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("index.aspx");
        }

        protected void dtPagerlvGoals_PreRender(object sender, EventArgs e)
        {
            if (lvGoals.EditIndex == -1 &&
               lvGoals.InsertItemPosition != InsertItemPosition.LastItem)
                loadGoals(true);
        }

        protected void btnContextSave_Click(object sender, EventArgs e)
        {
            addContextOfEnvironment();
        }

        private void addContextOfEnvironment()
        {
            string envContext = txtEnvContext.Text;
            int areadId = int.Parse(ddlArea.SelectedValue);
            int areaTypeId = int.Parse(ddlValueTypes.SelectedValue);
            int compId = Util.user.CompanyId;
            int contextId = string.IsNullOrEmpty(hdnContextId.Value) ? 0 : int.Parse(hdnContextId.Value);

            int contxtId = goalDef.addEnvironmentContext(new environmentcontext
            {
                AREA_ID = areadId,
                AREA_TYPE_ID = areaTypeId,
                COMPANY_ID = compId,
                DATE_CREATED = DateTime.Now,
                DATE_UPDATED = DateTime.Now,
                ENV_CONTEXT = envContext,
                ENV_CONTEXT_ID = contextId
            });

            hdnContextId.Value = contxtId.ToString();
            lblMsg.Text = Messages.GetSaveMessage();
        }

        protected void lnkSaveSub_Click(object sender, EventArgs e)
        {
            string arg = (sender as Button).CommandArgument.ToString();
            bool update = string.IsNullOrEmpty(arg) ||
                          arg.Equals("1") ? true : false;

            addSubMeasure(update);
        }

        protected void lnkCancelSub_Click(object sender, EventArgs e)
        {
            mdlShowSubMeasureInfo.Hide();
        }
    }
}