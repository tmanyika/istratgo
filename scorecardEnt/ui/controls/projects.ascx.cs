﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.implementations;
using scorecard.entities;
using scorecard.controllers;
using HR.Human.Resources;

namespace scorecard.ui.controls
{
    public partial class projects : System.Web.UI.UserControl
    {
        #region Properties

        int compId
        {
            get { return Util.user.CompanyId; }
        }

        #endregion

        #region Private Variables

        useradmincontroller usersDef;

        #endregion

        #region Default Constructor

        public projects()
        {
            usersDef = new useradmincontroller(new useradminImpl());
        }

        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMsg.Text = "";
            if (!IsPostBack) initialisePage();
        }

        #endregion

        #region Utility Methods

        void initialisePage()
        {
            loadProjects();
        }

        void loadProjects()
        {
            try
            {
                var pjs = new projectmanagement(new projectImpl()).get(compId, true);
                BindControl.BindListView(lstProject, pjs);
                if (pjs.Count <= pager.PageSize) pager.Visible = false;
                if (pjs.Count <= 0) lstProject.InsertItemPosition = InsertItemPosition.LastItem;
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void loadInitialUsers()
        {
            DropDownList ddPersonI = lstProject.InsertItem.FindControl("ddPersonI") as DropDownList;
            loadUsers(ddPersonI, "");
        }

        void loadUsers(DropDownList ddUsers, string uId)
        {
            try
            {
                var userRole = Util.user.UserTypeId;
                var userID = Util.user.UserId;
                IEmployee obj = new EmployeeBL();
                var titles = obj.GetByCompanyId(true, Util.user.CompanyId); //obj.GetActive(Util.user.OrgId);
                if (userRole != Util.getTypeDefinitionsUserTypeAdministrator())
                    titles = titles.Where(t => t.LineManagerEmployeeId == userID || t.EmployeeId == userID).ToList();

                ddUsers.DataTextField = "FullName";
                ddUsers.DataValueField = "EmployeeId";
                BindControl.BindDropdown(ddUsers, titles);
                if (ddUsers.Items.FindByValue(uId) != null) ddUsers.SelectedValue = uId;
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        public string GetBoolean(object boolVal)
        {
            if (boolVal == DBNull.Value) return "Unknown";
            return bool.Parse(boolVal.ToString()) ? "Yes" : "No";
        }

        void cancelUpdate()
        {
            lstProject.EditIndex = -1;
            lstProject.InsertItemPosition = InsertItemPosition.None;
            loadProjects();
        }

        void deleteProject(int pid)
        {
            try
            {
                string updatedBy = Util.user.FullName;
                project obj = new project
                {
                    PROJECT_ID = pid,
                    ACTIVE = false,
                    UPDATE_BY = updatedBy
                };

                bool deleted = new projectmanagement(new projectImpl()).setStatus(obj);
                if (deleted) loadProjects();
                string msg = deleted ? Messages.GetDeleteMessage() : Messages.GetDeleteFailedMessage();
                BindControl.BindLiteral(lblMsg, msg);
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        void addProject(ListViewCommandEventArgs e)
        {
            try
            {
                TextBox txtDesc = e.Item.FindControl("txtDescI") as TextBox;
                TextBox txtName = e.Item.FindControl("txtNameI") as TextBox;
                DropDownList ddPerson = e.Item.FindControl("ddPersonI") as DropDownList;
                CheckBox chkActiveE = e.Item.FindControl("chkActiveI") as CheckBox;

                if (!isDataValid(txtName.Text))
                {
                    lblMsg.Text = "Project name is required.";
                    return;
                }

                string userName = Util.user.FullName;
                project proj = new project
                {
                    COMPANY_ID = compId,
                    PROJECT_NAME = txtName.Text,
                    PROJECT_DESC = txtDesc.Text,
                    PROJECT_ID = 0,
                    CREATED_BY = userName,
                    UPDATE_BY = userName,
                    RESPONSIBLE_PERSON_ID = int.Parse(ddPerson.SelectedValue)
                };

                projectmanagement obj = new projectmanagement(new projectImpl());
                bool saved = obj.add(proj);
                if (saved)
                {
                    string msg = Messages.GetSaveMessage();
                    lstProject.InsertItemPosition = InsertItemPosition.None;
                    loadProjects();
                    BindControl.BindLiteral(lblMsg, msg);
                }
                else BindControl.BindLiteral(lblMsg, Messages.GetSaveFailedMessage());
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void updateProject(ListViewCommandEventArgs e)
        {
            try
            {
                TextBox txtDesc = e.Item.FindControl("txtDescE") as TextBox;
                TextBox txtName = e.Item.FindControl("txtNameE") as TextBox;
                HiddenField hdnprojId = e.Item.FindControl("hdnprojId") as HiddenField;
                DropDownList ddPerson = e.Item.FindControl("ddPerson") as DropDownList;
                CheckBox chkActiveE = e.Item.FindControl("chkActiveE") as CheckBox;

                if (!isDataValid(txtName.Text))
                {
                    lblMsg.Text = "Project name is required.";
                    return;
                }

                string userName = Util.user.FullName;
                project proj = new project
                {
                    COMPANY_ID = compId,
                    PROJECT_NAME = txtName.Text,
                    PROJECT_DESC = txtDesc.Text,
                    PROJECT_ID = int.Parse(hdnprojId.Value),
                    CREATED_BY = userName,
                    UPDATE_BY = userName,
                    RESPONSIBLE_PERSON_ID = int.Parse(ddPerson.SelectedValue)
                };

                projectmanagement obj = new projectmanagement(new projectImpl());
                bool saved = obj.update(proj);
                if (saved)
                {
                    string msg = Messages.GetUpdateMessage();
                    lstProject.EditIndex = -1;
                    lstProject.InsertItemPosition = InsertItemPosition.None;
                    loadProjects();
                    BindControl.BindLiteral(lblMsg, msg);
                }
                else BindControl.BindLiteral(lblMsg, Messages.GetSaveFailedMessage());
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        bool isDataValid(string name)
        {
            return string.IsNullOrEmpty(name) ? false : true;
        }

        #endregion

        #region ListView Event Handling Methods including Slide Paging

        protected void PagerCommand(object sender, DataPagerCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Next":
                    //guard against going off the end of the list
                    e.NewStartRowIndex = Math.Min(e.Item.Pager.StartRowIndex + e.Item.Pager.MaximumRows, e.Item.Pager.TotalRowCount - e.Item.Pager.MaximumRows);
                    e.NewMaximumRows = e.Item.Pager.MaximumRows;
                    break;
                case "Previous":
                    //guard against going off the begining of the list
                    e.NewStartRowIndex = Math.Max(0, e.Item.Pager.StartRowIndex - e.Item.Pager.MaximumRows);
                    e.NewMaximumRows = e.Item.Pager.MaximumRows;
                    break;
                case "Last":
                    //the
                    e.NewStartRowIndex = e.Item.Pager.TotalRowCount - e.Item.Pager.MaximumRows;
                    e.NewMaximumRows = e.Item.Pager.MaximumRows;
                    break;
                case "First":
                default:
                    e.NewStartRowIndex = 0;
                    e.NewMaximumRows = e.Item.Pager.MaximumRows;
                    break;
            }
        }

        protected void CurrentPageChanged(object sender, EventArgs e)
        {
            TextBox txtCurrentPage = sender as TextBox;
            DataPager pager = this.lstProject.FindControl("pager") as DataPager;
            int startRowIndex = Math.Min((int.Parse(txtCurrentPage.Text) - 1) * pager.MaximumRows, pager.TotalRowCount - pager.MaximumRows);
            pager.SetPageProperties(startRowIndex, pager.MaximumRows, true);
        }

        protected void lstProject_PagePropertiesChanged(object sender, EventArgs e)
        {
            loadProjects();
        }

        protected void lstProject_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            try
            {
                lstProject.EditIndex = e.NewEditIndex;
                lstProject.InsertItemPosition = InsertItemPosition.None;
                loadProjects();

                var ddUsers = lstProject.Items[e.NewEditIndex].FindControl("ddPerson") as DropDownList;
                HiddenField hdnUID = lstProject.Items[e.NewEditIndex].FindControl("hdnUID") as HiddenField;
                loadUsers(ddUsers, hdnUID.Value);
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        protected void lstProject_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                switch (e.CommandName)
                {
                    case "addProject":
                        addProject(e);
                        break;
                    case "updateProject":
                        updateProject(e);
                        break;
                    case "deleteItem":
                        int pid = int.Parse((e.Item.FindControl("hdnprojId") as HiddenField).Value);
                        deleteProject(pid);
                        break;
                    case "cancelUpdate":
                        cancelUpdate();
                        break;
                }
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        #endregion

        #region Button Event Handling Methods

        protected void lnkCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("index.aspx");
        }

        protected void lnkAdd_Click(object sender, EventArgs e)
        {
            lstProject.InsertItemPosition = InsertItemPosition.LastItem;
            loadProjects();
            loadInitialUsers();
        }

        #endregion
    }
}