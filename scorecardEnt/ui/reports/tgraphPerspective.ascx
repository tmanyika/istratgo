﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="tgraphPerspective.ascx.cs" Inherits="scorecard.ui.reports.tgraphPerspective" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Charting" TagPrefix="telerik" %>
<div style="background-color: White; padding-left: 200px;">
    <table cellpadding="0" cellspacing="10" border="0">
        <tr>
            <td style="width: 250px;">
                <h2>
                    Area</h2>
            </td>
            <td style="width: 250px;">
                <h2>
                    <span style="color: Black">
                        <asp:Literal ID="lblCriteria" runat="server"></asp:Literal></span></h2>
            </td>
            <td align="right">
                <span style="color: Red">
                    <asp:Literal ID="lblError" runat="server"></asp:Literal></span><asp:LinkButton ID="lnkPrint"
                        runat="server" CausesValidation="False" OnClientClick="window.print();">
                        <img runat="server" src="~/ui/assets/icons/print.png" id="imgPrint" alt="Click to Print" /></asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td>
                <h2>
                    Name</h2>
            </td>
            <td colspan="2">
                <h2>
                    <span style="color: Black">
                        <asp:Literal ID="lblName" runat="server"></asp:Literal>
                    </span>
                </h2>
            </td>
        </tr>
        <tr>
            <td>
                <h2>
                    Report Date</h2>
            </td>
            <td colspan="2">
                <h2>
                    <span style="color: Black">
                        <asp:Literal ID="lblDate" runat="server"></asp:Literal>
                    </span>
                </h2>
            </td>
        </tr>
    </table>
</div>
<div>
    <br />
</div>
<div style="padding-left: 150px;">
  <telerik:RadChart ID="RadChartPes" runat="server" AutoTextWrap="True" Skin="Mac" AutoLayout="True"
        Height="800px" Width="600px" SeriesOrientation="Horizontal">
        <Appearance Corners="Round, Round, Round, Round, 6">
            <FillStyle FillType="Image">
                <FillSettings BackgroundImage="{chart}" ImageDrawMode="Flip" ImageFlip="FlipX">
                </FillSettings>
            </FillStyle>
            <Border Color="138, 138, 138" />
        </Appearance>
        <Series>
            <telerik:ChartSeries Name="Actual" DataYColumn="ActualValue" >
                <Appearance>
                    <FillStyle MainColor="55, 167, 226" SecondColor="22, 85, 161">
                        <FillSettings GradientMode="Vertical">
                        </FillSettings>
                    </FillStyle>
                    <TextAppearance TextProperties-Color="Black">
                    </TextAppearance>
                </Appearance>
            </telerik:ChartSeries>
            <telerik:ChartSeries Name="Target" DataYColumn="TargetValue">
                <Appearance>
                    <FillStyle MainColor="223, 87, 60" SecondColor="200, 38, 37">
                        <FillSettings GradientMode="Vertical">
                        </FillSettings>
                    </FillStyle>
                    <TextAppearance TextProperties-Color="Black">
                    </TextAppearance>
                </Appearance>
            </telerik:ChartSeries>
        </Series>
        <Legend>
            <Appearance Dimensions-Margins="15.4%, 3%, 1px, 1px" 
                Position-AlignedPosition="TopRight">
                <ItemMarkerAppearance Figure="Square">
                    <Border Color="134, 134, 134" />
                </ItemMarkerAppearance>
                <FillStyle MainColor="">
                </FillStyle>
                <Border Color="Transparent" />
            </Appearance>
        </Legend>
        <PlotArea>
            <XAxis DataLabelsColumn="Perspective">
                <Appearance Color="134, 134, 134" MajorTick-Color="134, 134, 134">
                    <MajorGridLines Color="209, 222, 227" PenStyle="Solid" />
                    <TextAppearance TextProperties-Color="51, 51, 51">
                    </TextAppearance>
                </Appearance>
                <AxisLabel>
                    <Appearance RotationAngle="270">
                    </Appearance>
                    <TextBlock>
                        <Appearance TextProperties-Color="51, 51, 51">
                        </Appearance>
                    </TextBlock>
                </AxisLabel>
                <Items>
                    <telerik:ChartAxisItem>
                    </telerik:ChartAxisItem>
                    <telerik:ChartAxisItem Value="1">
                    </telerik:ChartAxisItem>
                    <telerik:ChartAxisItem Value="2">
                    </telerik:ChartAxisItem>
                    <telerik:ChartAxisItem Value="3">
                    </telerik:ChartAxisItem>
                    <telerik:ChartAxisItem Value="4">
                    </telerik:ChartAxisItem>
                    <telerik:ChartAxisItem Value="5">
                    </telerik:ChartAxisItem>
                    <telerik:ChartAxisItem Value="6">
                    </telerik:ChartAxisItem>
                    <telerik:ChartAxisItem Value="7">
                    </telerik:ChartAxisItem>
                </Items>
            </XAxis>
            <YAxis>
                <Appearance Color="134, 134, 134" MajorTick-Color="134, 134, 134" 
                    MinorTick-Color="134, 134, 134" MinorTick-Width="0">
                    <MajorGridLines Color="209, 222, 227" />
                    <MinorGridLines Color="233, 239, 241" />
                    <TextAppearance TextProperties-Color="51, 51, 51">
                    </TextAppearance>
                </Appearance>
                <AxisLabel>
                    <Appearance RotationAngle="0">
                    </Appearance>
                    <TextBlock>
                        <Appearance TextProperties-Color="51, 51, 51">
                        </Appearance>
                    </TextBlock>
                </AxisLabel>
            </YAxis>
            <YAxis2>
                <AxisLabel>
                    <Appearance RotationAngle="0">
                    </Appearance>
                </AxisLabel>
            </YAxis2>
            <Appearance>
                <FillStyle FillType="Solid" MainColor="White">
                </FillStyle>
                <Border Color="134, 134, 134" />
            </Appearance>
        </PlotArea>
        <ChartTitle>
            <Appearance Position-AlignedPosition="Top">
                <FillStyle MainColor="">
                </FillStyle>
            </Appearance>
            <TextBlock Appearance-AutoTextWrap="False" Text="Strategic Objectives">
                <Appearance AutoTextWrap="False" TextProperties-Font="Tahoma, 13pt" TextProperties-Color="Blue">
                </Appearance>
            </TextBlock>
        </ChartTitle>
    </telerik:RadChart>
</div>
