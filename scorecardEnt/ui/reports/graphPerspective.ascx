﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="graphPerspective.ascx.cs"
    Inherits="scorecard.ui.reports.graphPerspective" %>
<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<div style="background-color: White; padding-left: 200px;">
    <table cellpadding="0" cellspacing="10" border="0">
        <tr>
            <td style="width: 250px;">
                <h2>
                    Area</h2>
            </td>
            <td style="width: 250px;">
                <h2>
                    <span style="color: Black">
                        <asp:Literal ID="lblCriteria" runat="server"></asp:Literal></span></h2>
            </td>
            <td align="right">
                <span style="color: Red">
                    <asp:Literal ID="lblError" runat="server"></asp:Literal></span><asp:LinkButton ID="lnkPrint"
                        runat="server" CausesValidation="False" OnClientClick="window.print();">
                        <img runat="server" src="~/ui/assets/icons/print.png" id="imgPrint" alt="Click to Print" /></asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td>
                <h2>
                    Name</h2>
            </td>
            <td colspan="2">
                <h2>
                    <span style="color: Black">
                        <asp:Literal ID="lblName" runat="server"></asp:Literal>
                    </span>
                </h2>
            </td>
        </tr>
        <tr>
            <td>
                <h2>
                    Report Date</h2>
            </td>
            <td colspan="2">
                <h2>
                    <span style="color: Black">
                        <asp:Literal ID="lblDate" runat="server"></asp:Literal>
                    </span>
                </h2>
            </td>
        </tr>
    </table>
</div>
<div>
    <br />
</div>
<div style="padding-left: 150px;">
    <asp:Chart ID="orgChart" runat="server" Height="450px" Width="600px" ImageType="Png"
        BorderlineDashStyle="Solid" Palette="BrightPastel" BackSecondaryColor="White"
        BackGradientStyle="TopBottom" BorderWidth="2" BackColor="WhiteSmoke" BorderColor="26, 59, 105">
        <Legends>
            <asp:Legend LegendStyle="Row" IsTextAutoFit="True" Docking="Bottom" Name="Default"
                BackColor="Transparent" Font="Trebuchet MS, 8.25pt, style=Bold" Alignment="Far">
                <Position Y="87" Height="8.351166" Width="45.19481" X="49.0679665"></Position>
            </asp:Legend>
        </Legends>
        <BorderSkin SkinStyle="Emboss"></BorderSkin>
        <Series>
            <asp:Series Name="Actual" ChartArea="ChartArea1" LegendText="Actual %" BorderColor="180, 26, 59, 105">
            </asp:Series>
            <asp:Series Name="Target" ChartArea="ChartArea1" LegendText="Target %" BorderColor="180, 26, 59, 105">
            </asp:Series>
        </Series>
        <ChartAreas>
            <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BackSecondaryColor="White"
                BackColor="Gainsboro" ShadowColor="Transparent" BackGradientStyle="TopBottom">
                <InnerPlotPosition Height="94.08608" Width="80.60287" X="10.39713" Y="0.913917542" />
                <Area3DStyle Rotation="10" Perspective="10" Inclination="15" IsRightAngleAxes="False"
                    WallWidth="0" IsClustered="False"></Area3DStyle>
                <Position Y="5" Height="75" Width="90.43796" X="0.562042236"></Position>
                <AxisY LineColor="64, 64, 64, 64" IsLabelAutoFit="False" Title="Scores" TitleFont="Trebuchet MS, 9.25pt, style=Bold">
                    <LabelStyle Font="Trebuchet MS, 8.25pt" />
                    <MajorGrid LineColor="64, 64, 64, 64" />
                </AxisY>
                <AxisX LineColor="64, 64, 64, 64" IsLabelAutoFit="False" Title="Strategic Objectives" TitleFont="Trebuchet MS, 9.25pt, style=Bold">
                    <LabelStyle Font="Trebuchet MS, 8.25pt" />
                    <MajorGrid LineColor="64, 64, 64, 64" />
                </AxisX>
            </asp:ChartArea>
        </ChartAreas>
    </asp:Chart>
</div>
