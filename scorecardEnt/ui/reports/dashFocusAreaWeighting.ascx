﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="dashFocusAreaWeighting.ascx.cs"
    Inherits="scorecard.ui.reports.dashFocusAreaWeighting" %>
<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<div style="padding-left: 100px;">
    <h2>
        <b>Current Strategic Objective Weighting</b></h2>
</div>
<div>
    <asp:Chart ID="orgChart" runat="server" Height="400px" Width="400px">
        <Series>
            <asp:Series ChartType="Pie" Name="Series1" ChartArea="ChartArea1">
            </asp:Series>
        </Series>
        <ChartAreas>
            <asp:ChartArea Name="ChartArea1" Area3DStyle-Enable3D="true">
            </asp:ChartArea>
        </ChartAreas>
    </asp:Chart>
    <b class="dashboardred">
        <asp:Literal ID="LblNoDataMsg" runat="server" Text="There are no current Strategic Objectives Weighting to display."></asp:Literal></b>
</div>
