﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using scorecard.interfaces;
using scorecard.entities;
using scorecard.implementations;
using System.Data;

/* @Copy Albertoncaffeine.net */

namespace scorecard.controllers
{
    public class companyregistration
    {
        Icompany registration;

        public companyregistration(Icompany _companyRegistration)
        {
            registration = _companyRegistration;
        }

        public int registerCompany(company _company)
        {
            return registration.addCompany(_company);
        }

        public int updateRegisteredCompany(company _company)
        {
            return registration.updateCompany(_company);
        }

        public int deleteRegisteredCompany(company _company)
        {
            return registration.deleteCompany(_company);
        }

        public List<company> getRegisteredCompaniesByCustomer(customer _customer)
        {
            return registration.getCompanyByCustomer(_customer);
        }

        public company getRegisteredInfo(company _companyInfo)
        {
            return registration.getCompanyInfo(_companyInfo);
        }

        public List<company> getAllRegisteredCompanies()
        {
            return registration.getAllRegisteredCompanies();
        }

        public DataTable getAllRegisteredCompaniesList()
        {
            return registration.getAllRegisteredCompaniesList();
        }

        public int addRegisteredCompanyAccountDetails(bankaccount account)
        {
            return registration.addCompanyAccountDetails(account);
        }

        public int updateRegisteredCompanyAccountDetails(bankaccount account)
        {
            return registration.updateCompanyAccountDetails(account);
        }

        public bankaccount getCompanyAccountDetails(int companyId)
        {
            return registration.getCompanyAccountDetails(companyId);
        }

        public bool updateCompanyPaidStatus(int compId, bool paid)
        {
            return registration.updateCompanyPaidStatus(compId, paid);
        }
    }
}