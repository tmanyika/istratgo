﻿using System;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

namespace HR.Leave
{
    public class LeaveAccrualDAL
    {
        #region Variables

        private string _conString;

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return _conString; }
            set { _conString = value; }
        }

        #endregion

        #region Default Constructors

        public LeaveAccrualDAL(string conString)
        {
            _conString = conString;
        }

        #endregion

        #region Public Methods and Functions

        public DataTable GetAll(int companyId)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_LeaveAccrual_GetAll", companyId).Tables[0];
        }

        public DataTable Get(int companyId, bool active)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_LeaveAccrual_GetActive", companyId, active).Tables[0];
        }

        public DataTable GetById(int leaveAccrualId)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_LeaveAccrual_GetById", leaveAccrualId).Tables[0];
        }

        public int Save(LeaveAccrual obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_LeaveAccrual_Insert", obj.LeaveId, obj.WorkingWeekId, obj.AccrualTimeId,
                                                        obj.AccrualRate, obj.MaximumThreshold, obj.Active, obj.CreatedBy);
            return result;
        }

        public int Update(LeaveAccrual obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_LeaveAccrual_Update", obj.LeaveAccrualId, obj.LeaveId, obj.WorkingWeekId,
                                                        obj.AccrualTimeId, obj.AccrualRate, obj.MaximumThreshold, obj.Active, obj.CreatedBy);
            return result;
        }

        public int Deactivate(int leaveAccrualId, string updatedBy)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_LeaveAccrual_Deactivate", leaveAccrualId, updatedBy);
            return result;
        }

        #endregion
    }
}