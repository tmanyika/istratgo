﻿using System;
using System.Collections.Generic;
using System.Data;
using scorecard.implementations;

namespace HR.Leave
{
    public class LeaveStatusBL : ILeaveStatus
    {
        #region Properties

        public string ConnectionString
        {
            get { return Util.GetconnectionString(); }
        }

        public LeaveStatusDAL Dal
        {
            get { return new LeaveStatusDAL(ConnectionString); }
        }

        #endregion

        public LeaveStatusBL() { }

        public bool AddLeaveStatus(LeaveStatus obj)
        {
            int result = Dal.Save(obj);
            return result > 0 ? true : false;
        }

        public bool UpdateLeaveStatus(LeaveStatus obj)
        {
            int result = Dal.Update(obj);
            return result > 0 ? true : false;
        }

        public bool Delete(int statusId, string updatedBy)
        {
            int result = Dal.Deactivate(statusId, updatedBy);
            return result > 0 ? true : false;
        }

        public List<LeaveStatus> GetByStatus( bool active)
        {
            List<LeaveStatus> obj = new List<LeaveStatus>();
            DataTable dT = Dal.Get(active);

            if (dT.Rows.Count <= 0) return obj;
            foreach (DataRow rw in dT.Rows)
                obj.Add(GetRecord(rw));

            return obj;
        }

        public List<LeaveStatus> GetAll()
        {
            List<LeaveStatus> obj = new List<LeaveStatus>();
            DataTable dT = Dal.GetAll();

            if (dT.Rows.Count <= 0) return obj;
            foreach (DataRow rw in dT.Rows)
                obj.Add(GetRecord(rw));

            return obj;
        }

        public LeaveStatus GetById(int statusId)
        {
            DataTable dT = Dal.GetById(statusId);
            if (dT.Rows.Count <= 0) return new LeaveStatus();
            DataRow rw = dT.Rows[0];
            return GetRecord(rw);
        }

        public LeaveStatus GetRecord(DataRow rw)
        {
            DateTime? defDate = null;
            return new LeaveStatus
            {
                Active = bool.Parse(rw["active"].ToString()),
                CreatedBy = rw["createdby"].ToString(),
                DateCreated = DateTime.Parse(rw["datecreated"].ToString()),
                DateUpdated = (rw["dateupdated"] != DBNull.Value) ? DateTime.Parse(rw["dateupdated"].ToString()) : defDate,
                StatusId = int.Parse(rw["statusId"].ToString()),
                StatusName = rw["statusName"].ToString(),
                UpdatedBy = (rw["updatedby"] != DBNull.Value) ? rw["updatedby"].ToString() : string.Empty
            };
        }
    }
}