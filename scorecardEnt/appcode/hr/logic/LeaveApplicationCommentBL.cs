﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using scorecard.implementations;
using System.Data;

namespace HR.Leave.Documents
{
    public class LeaveApplicationCommentBL : ILeaveApplicationComment
    {
        #region Properties

        public string ConnectionString
        {
            get { return Util.GetconnectionString(); }
        }

        public LeaveApplicationCommentDAL Dal
        {
            get { return new LeaveApplicationCommentDAL(ConnectionString); }
        }

        #endregion

        #region Default Constructors

        public LeaveApplicationCommentBL() { }

        #endregion

        #region Public Function & Methods

        public bool AddComment(LeaveApplicationComment obj)
        {
            int result = Dal.Save(obj);
            return (result > 0) ? true : false;
        }

        public bool DeleteComments(int commentId, string updatedBy)
        {
            int result = Dal.Delete(commentId, updatedBy);
            return (result > 0) ? true : false;
        }

        LeaveApplicationComment GetRow(DataRow rw)
        {
            DateTime? defDate = null;
            return new LeaveApplicationComment
            {
                Comment = (rw["comment"] != DBNull.Value) ? rw["comment"].ToString() : string.Empty,
                CommentId = int.Parse(rw["commentId"].ToString()),
                DateCreated = DateTime.Parse(rw["datecreated"].ToString()),
                DateUpdated = rw["dateupdated"] != DBNull.Value ? DateTime.Parse(rw["dateupdated"].ToString()) : defDate,
                DocumentTitle = (rw["documentTitle"] != DBNull.Value) ? rw["documentTitle"].ToString() : string.Empty,
                LeaveApplicationId = int.Parse(rw["ApplicationId"].ToString()),
                SupportingDoc = (rw["supportingDoc"] != DBNull.Value) ? rw["supportingDoc"].ToString() : string.Empty,
                UpdatedBy = (rw["updatedBy"] != DBNull.Value) ? rw["updatedBy"].ToString() : string.Empty,
                UploadedBy = rw["uploadedBy"].ToString(),
                Valid = bool.Parse(rw["valid"].ToString())
            };
        }

        public LeaveApplicationComment GetComment(int commentId)
        {
            DataTable dT = Dal.GetById(commentId);
            return (dT.Rows.Count > 0) ? GetRow(dT.Rows[0]) : new LeaveApplicationComment();
        }

        public List<LeaveApplicationComment> GetComments(int applicationId)
        {
            DataTable dT = Dal.Get(applicationId);
            List<LeaveApplicationComment> obj = new List<LeaveApplicationComment>();
            foreach (DataRow rw in dT.Rows) obj.Add(GetRow(rw));
            return obj;
        }

        public DataTable GetSchema()
        {
            DataTable dT = new DataTable("Docs");
            DataColumn id = new DataColumn("CommentId", typeof(long));
            id.AutoIncrement = true;
            id.AutoIncrementSeed = 1;
            id.AutoIncrementStep = 1;

            DataColumn[] cols = { id, new DataColumn("ApplicationId", typeof(int)), new DataColumn("DocumentTitle",typeof(string)), 
                                  new DataColumn("Comment",typeof(string)) , new DataColumn("SupportingDoc",typeof(string)) , 
                                  new DataColumn("Valid",typeof(bool)), new DataColumn("UploadedBy",typeof(string)),
                                  new DataColumn("DateCreated",typeof(DateTime))};
            dT.Columns.AddRange(cols);
            dT.AcceptChanges();
            return dT;
        }
        #endregion
    }
}