﻿using System;
using System.Collections.Generic;
using scorecard.implementations;
using System.Data;
using HR.Human.Resources;
using System.Text;
using System.Net.Mail;

namespace HR.Leave
{
    public class LeaveApplicationBL : ILeaveApplication
    {
        #region Properties

        public string ConnectionString
        {
            get { return Util.GetconnectionString(); }
        }

        public ILeaveType BlLeave
        {
            get { return new LeaveTypeBL(); }
        }

        public ILeaveStatus BlStatus
        {
            get { return new LeaveStatusBL(); }
        }

        public IEmployee BlEmp
        {
            get { return new EmployeeBL(); }
        }

        public LeaveApplicationDAL Dal
        {
            get { return new LeaveApplicationDAL(ConnectionString); }
        }

        #endregion

        #region Default Class Constructors

        public LeaveApplicationBL() { }

        #endregion

        #region Public Methods & Functions

        public DateTime AddBusinessDays(DateTime dt, int nDays)
        {
            int weeks = nDays / 5;
            nDays %= 5;
            while (dt.DayOfWeek == DayOfWeek.Saturday || dt.DayOfWeek == DayOfWeek.Sunday)
                dt = dt.AddDays(1);

            while (nDays-- > 0)
            {
                dt = dt.AddDays(1);
                if (dt.DayOfWeek == DayOfWeek.Saturday)
                    dt = dt.AddDays(2);
            }
            return dt.AddDays(weeks * 7);
        }

        public int SubmitApplication(LeaveApplication obj)
        {
            int result = Dal.Save(obj);
            return result;
        }

        public bool UpdateApplication(LeaveApplication obj)
        {
            int result = Dal.Update(obj);
            return result > 0 ? true : false;
        }

        public bool ApplicationValid(LeaveApplication obj)
        {
            object result = Dal.Validate(obj);
            return (result == DBNull.Value) ? true : !bool.Parse(result.ToString());
        }

        public bool UpdateStatus(LeaveApplication obj)
        {
            int result = Dal.UpdateStatus(obj);
            return result > 0 ? true : false;
        }

        public LeaveApplication GetById(int applicationId)
        {
            DataTable dT = Dal.GetById(applicationId);
            if (dT.Rows.Count <= 0) return new LeaveApplication();
            return GetRow(dT.Rows[0]);
        }

        LeaveApplication GetRow(DataRow rw)
        {
            int? defInt = null;
            DateTime? defDate = null;

            return new LeaveApplication
            {
                ApprovedByEmployeeId = (rw["ApprovedByEmployeeId"] != DBNull.Value) ?
                                        int.Parse(rw["ApprovedByEmployeeId"].ToString()) : defInt,
                DateCreated = DateTime.Parse(rw["DateCreated"].ToString()),
                DateUpdated = (rw["DateUpdated"] != DBNull.Value) ? DateTime.Parse(rw["DateUpdated"].ToString()) : defDate,
                EmployeeId = int.Parse(rw["EmployeeId"].ToString()),
                EndDate = DateTime.Parse(rw["EndDate"].ToString()),
                LeaveApplicationId = int.Parse(rw["ApplicationId"].ToString()),
                LeaveId = int.Parse(rw["LeaveId"].ToString()),
                LeaveNotes = (rw["LeaveNotes"] != DBNull.Value) ? rw["LeaveNotes"].ToString() : string.Empty,
                NoOfLeaveDays = int.Parse(rw["NoOfLeaveDays"].ToString()),
                StartDate = DateTime.Parse(rw["StartDate"].ToString()),
                StatusId = int.Parse(rw["StatusId"].ToString()),
                SubmittedEmployeeId = int.Parse(rw["SubmittedEmployeeId"].ToString()),
                Leave = BlLeave.GetRecord(rw),
                Staff = BlEmp.GetEmployeeRow(rw),
                Position = BlEmp.GetJobRow(rw),
                Status = BlStatus.GetRecord(rw),
                RejectReason = (rw["RejectReason"] != DBNull.Value) ? rw["RejectReason"].ToString() : string.Empty
            };
        }

        public List<LeaveApplication> GetByApproverId(int approverId)
        {
            List<LeaveApplication> obj = new List<LeaveApplication>();
            DataTable dT = Dal.GetByApprover(approverId);
            if (dT.Rows.Count <= 0) return obj;
            foreach (DataRow rw in dT.Rows) obj.Add(GetRow(rw));
            return obj;
        }

        public List<LeaveApplication> GetByApplicantId(int applicantId)
        {
            List<LeaveApplication> obj = new List<LeaveApplication>();
            DataTable dT = Dal.GetByApplicant(applicantId);
            if (dT.Rows.Count <= 0) return obj;
            foreach (DataRow rw in dT.Rows) obj.Add(GetRow(rw));
            return obj;
        }

        public List<LeaveApplication> GetByCompanyId(int companyId)
        {
            List<LeaveApplication> obj = new List<LeaveApplication>();
            DataTable dT = Dal.GetByCompany(companyId);
            if (dT.Rows.Count <= 0) return obj;
            foreach (DataRow rw in dT.Rows) obj.Add(GetRow(rw));
            return obj;
        }

        public List<LeaveApplication> GetSubmittedLeave(int approverId, int statusId)
        {
            List<LeaveApplication> obj = new List<LeaveApplication>();
            DataTable dT = Dal.Get(approverId, statusId);
            if (dT.Rows.Count <= 0) return obj;
            foreach (DataRow rw in dT.Rows) obj.Add(GetRow(rw));
            return obj;
        }

        public string BuildEmailData(int applicationId, bool submitting, out string approverName, out string approverEmail, out string status, out MailAddress recip)
        {
            approverEmail = string.Empty;
            approverName = string.Empty;
            status = string.Empty;
            recip = new MailAddress(Util.user.Email, Util.user.FullName);

            try
            {
                LeaveApplication data = GetById(applicationId);
                if (data == null) return string.Empty;

                IEmployee obj = new EmployeeBL();
                Employee approver = obj.GetById((int)Util.user.ReportToUserId);
                Employee applicant = obj.GetById(data.EmployeeId);

                recip = (submitting) ? new MailAddress(approver.EmailAddress, approver.FullName) :
                    new MailAddress(applicant.EmailAddress, applicant.FullName);
                status = data.Status.StatusName.ToLower();
                approverEmail = approver.EmailAddress;
                approverName = approver.FullName;

                StringBuilder strB = new StringBuilder("<div><table cellpadding='2' cellspacing='0' border='0'>");
                strB.Append(string.Format("<tr><td><b>Applicant</b></td><td>{0} ( {1} )</td></tr>", data.Staff.FullName, data.Staff.EmailAddress));
                strB.Append(string.Format("<tr><td><b>Leave Type</b></td><td>{0}</td></tr>", data.Leave.LeaveName));
                strB.Append(string.Format("<tr><td><b>Start Date</b></td><td>{0}</td></tr>", Common.GetDescriptiveDate(data.StartDate, true)));
                strB.Append(string.Format("<tr><td><b>End Date</b></td><td>{0}</td></tr>", Common.GetDescriptiveDate(data.EndDate, true)));
                strB.Append(string.Format("<tr><td><b>Date Submitted</b></td><td>{0}</td></tr>", Common.GetDescriptiveDateWithTime(data.DateCreated, true)));
                strB.Append(string.Format("<tr><td style='width:350px'><b>Number of Leave Days</b></td><td>{0}</td></tr>", data.NoOfLeaveDays));

                if (data.LeaveNotes != string.Empty) strB.Append(string.Format("<tr><td><b>Leave Note</b></td><td>{0}</td></tr>", data.LeaveNotes));
                if (data.RejectReason != string.Empty) strB.Append(string.Format("<tr><td><b>Rejection Reason</b></td><td>{0}</td></tr>", data.RejectReason));
                if (!submitting) strB.Append(string.Format("<tr><td><b>Approved By</b></td><td>{0} ( {1} )</td></tr>", approverName, approverEmail));

                strB.Append("</table></div>");
                return strB.ToString();
            }
            catch (Exception ex)
            {
                string err = ex.ToString();
                Util.LogErrors(ex);
            }
            return string.Empty;
        }
        #endregion
    }
}