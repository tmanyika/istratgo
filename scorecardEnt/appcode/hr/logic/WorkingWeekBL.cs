﻿using System;
using System.Collections.Generic;
using System.Data;
using scorecard.implementations;

namespace HR.Leave
{
    public class WorkingWeekBL : IWorkingWeek
    {
        #region Properties

        public string ConnectionString
        {
            get { return Util.GetconnectionString(); }
        }

        public WorkingWeekDAL Dal
        {
            get { return new WorkingWeekDAL(ConnectionString); }
        }

        #endregion

        #region Default Constructors

        public WorkingWeekBL() { }

        #endregion

        #region Public Methods & Functions

        public bool AddWorkingWeek(WorkingWeek obj)
        {
            int result = Dal.Save(obj);
            return result > 0 ? true : false;
        }

        public bool UpdateWorkingWeek(WorkingWeek obj)
        {
            int result = Dal.Update(obj);
            return result > 0 ? true : false;
        }

        public bool Delete(int leaveId, string updatedBy)
        {
            int result = Dal.Deactivate(leaveId, updatedBy);
            return result > 0 ? true : false;
        }

        public List<WorkingWeek> GetByStatus(int companyId, bool active)
        {
            List<WorkingWeek> obj = new List<WorkingWeek>();
            DataTable dT = Dal.Get(companyId, active);
            if (dT.Rows.Count <= 0) return obj;

            foreach (DataRow rw in dT.Rows)
                obj.Add(GetRecord(rw));
            return obj;
        }

        public List<WorkingWeek> GetByCompanyId(int companyId)
        {
            List<WorkingWeek> obj = new List<WorkingWeek>();
            DataTable dT = Dal.Get(companyId);
            if (dT.Rows.Count <= 0) return obj;

            foreach (DataRow rw in dT.Rows)
                obj.Add(GetRecord(rw));
            return obj;
        }

        public WorkingWeek GetById(int leaveId)
        {
            DataTable dT = Dal.GetById(leaveId);
            if (dT.Rows.Count <= 0) return new WorkingWeek();
            DataRow rw = dT.Rows[0];
            return GetRecord(rw);
        }

        public WorkingWeek GetRecord(DataRow rw)
        {
            DateTime? defDate = null;
            return new WorkingWeek
            {
                Active = (rw["active"] != DBNull.Value) ? bool.Parse(rw["active"].ToString()) : false,
                CompanyId = (rw["companyId"] != DBNull.Value) ? int.Parse(rw["companyId"].ToString()) : 0,
                CreatedBy = (rw["createdby"] != DBNull.Value) ? rw["createdby"].ToString() : string.Empty,
                DateCreated = (rw["datecreated"] != DBNull.Value) ? DateTime.Parse(rw["datecreated"].ToString()) : DateTime.Now,
                DateUpdated = (rw["dateupdated"] != DBNull.Value) ? DateTime.Parse(rw["dateupdated"].ToString()) : defDate,
                WorkingWeekId = (rw["workingWeekId"] != DBNull.Value) ? int.Parse(rw["workingWeekId"].ToString()) : 0,
                Name = (rw["Name"] != DBNull.Value) ? rw["Name"].ToString() : string.Empty,
                NoOfDays = (rw["noofdays"] != DBNull.Value) ? int.Parse(rw["noofdays"].ToString()) : 0,
                UpdatedBy = (rw["updatedby"] != DBNull.Value) ? rw["updatedby"].ToString() : string.Empty
            };
        }

        #endregion
    }
}