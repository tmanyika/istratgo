﻿using System;
using Universe.Nations;

namespace HR.Leave
{
    [Serializable]
    public class LeaveCalendar : LeaveCalendarCompany
    {
        public Int64 HolidayDateId { get; set; }
        public int CountryId { get; set; }
        public int CalendarYear { get; set; }

        public bool Active { get; set; }

        public string Detail { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public Country Nation { get; set; }
        public DateTime HolidayDate { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
    }
}