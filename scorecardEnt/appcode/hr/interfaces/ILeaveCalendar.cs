﻿using System;
using System.Collections.Generic;
using System.Data;

namespace HR.Leave
{
    public interface ILeaveCalendar
    {
        bool AddLeaveCalendar(LeaveCalendar obj);
        bool UpdateLeaveCalendar(LeaveCalendar obj);
        bool Delete(int holidayDateId, string updatedBy);
        bool ApplyLeaveCalendar(LeaveCalendar obj, int orgUnitId);

        int GetNumberOfHolidays(DateTime sDate, DateTime eDate, int countryId);

        LeaveCalendar GetRecord(DataRow rw);
        LeaveCalendar GetById(int holidayDateId);
        List<LeaveCalendar> GetAppliedHolidays(int orgUnitId, int calendarYear);
        List<LeaveCalendar> GetByStatus(int countryId, int calendarYear, bool status);
    }
}
