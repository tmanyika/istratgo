﻿using System;
using System.Data;
using System.Collections.Generic;
using scorecard.entities;

namespace scorecard.interfaces
{
    public interface IScoreRating
    {
        bool AddRating(ScoreRating obj);
        bool UpdateRating(ScoreRating obj);
        bool DeactivateRating(ScoreRating obj);

        decimal GetCompanyRating(int companyId, bool active);

        ScoreRating GetById(int ratingId);
        ScoreRating GetRow(IDataRecord rw);

        List<ScoreRating> GetAll(int companyId);
        List<ScoreRating> GetByStatus(int companyId, bool active);
    }
}
