﻿using System;
using System.Collections.Generic;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

namespace Bulk.Import
{
    public class BulkSheetDAL
    {
        #region Variables

        private string _conString;

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return _conString; }
            set { _conString = value; }
        }

        #endregion

        #region Default Constructors

        public BulkSheetDAL(string conString)
        {
            _conString = conString;
        }

        #endregion

        #region Methods & Functions

        public IDataReader Get()
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_BulkSheet_GetAll");
        }

        public IDataReader Get(int sheetId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_BulkSheet_GetById", sheetId);
        }

        public IDataReader Get(bool active)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_BulkSheet_GetByStatus", active);
        }

        public IDataReader Get(string sheetName)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_BulkSheet_GetByName", sheetName);
        }

        public int Save(BulkSheet obj)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_BulkSheet_Insert", obj.SheetName, obj.DbTableName, obj.SheetDescription,
                                                                                               obj.SheetNotes, obj.Active, obj.CreatedBy);
            return (result == DBNull.Value) ? 0 : int.Parse(result.ToString());
        }

        public int Update(BulkSheet obj)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_BulkSheet_Update", obj.SheetId, obj.SheetName, obj.DbTableName, obj.SheetDescription,
                                                                                               obj.SheetNotes, obj.Active, obj.UpdatedBy);
            return (result == DBNull.Value) ? 0 : int.Parse(result.ToString());
        }

        public int UpdateStatus(BulkSheet obj)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_BulkSheet_SetStatus", obj.SheetId, obj.Active, obj.UpdatedBy);
            return (result == DBNull.Value) ? 0 : int.Parse(result.ToString());
        }

        #endregion

    }
}