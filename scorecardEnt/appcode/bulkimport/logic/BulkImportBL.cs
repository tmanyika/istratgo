﻿using System;
using System.Data;
using System.IO;
using Excel;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Data.SqlClient;
using scorecard.implementations;
using vb = Microsoft.VisualBasic.Information;
using System.Globalization;
using Microsoft.ApplicationBlocks.Data;


namespace Bulk.Import
{
    public class BulkImportBL : IBulkImport
    {
        int BulkCopyBatchSize = Util.BulkCopyBatchSize;
        IBulkSheet Isheet;

        public BulkImportDAL Dal
        {
            get { return new BulkImportDAL(Util.GetconnectionString()); }
        }

        public BulkImportBL()
        {
            Isheet = new BulkSheetBL();
        }

        public FileEnum GetFileTypeEnum(string extn)
        {
            FileEnum fileType = FileEnum.None;
            switch (extn)
            {
                case ".xls":
                    fileType = FileEnum.ExcelXls;
                    break;
                case ".xlsx":
                    fileType = FileEnum.ExcelXlsX;
                    break;
            }

            return fileType;
        }

        public DataSet GetData(Stream stream, FileEnum fileType)
        {
            IExcelDataReader excelReader = null;
            switch (fileType)
            {
                case FileEnum.ExcelXls:
                    //1. Reading from a binary Excel file ('97-2003 format; *.xls)
                    excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
                    break;
                case FileEnum.ExcelXlsX:
                    //2. Reading from a OpenXml Excel file (2007 format; *.xlsx)
                    excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                    break;
            }

            DataSet dS = new DataSet();
            if (excelReader != null)
            {
                //3. DataSet - Create column names from first row
                excelReader.IsFirstRowAsColumnNames = true;
                dS = excelReader.AsDataSet();
            }
            return dS;
        }

        public DataSet AddAdditionalColumn(DataSet dS, List<BulkSheet> sheetList, string additionalFieldName, object defaultValue)
        {
            foreach (DataTable dT in dS.Tables)
            {
                string outcome = string.Empty;
                string sheetName = dT.TableName;
                bool exist = Isheet.SheetExist(sheetList, sheetName, out outcome);
                if (exist && !dS.Tables[sheetName].Columns.Contains(additionalFieldName))
                {
                    DataColumn col = new DataColumn(additionalFieldName, typeof(int));
                    col.DefaultValue = defaultValue;
                    dS.Tables[sheetName].Columns.Add(col);
                    col = null;
                }
                //else dS.Tables.Remove(sheetName);
            }
            dS.AcceptChanges();
            return dS;
        }

        bool IsInteger(object dataVal)
        {
            if (dataVal == null || dataVal == DBNull.Value)
                return false;
            int result;
            return (vb.IsNumeric(dataVal) && int.TryParse(dataVal.ToString(), NumberStyles.None, null, out result));
        }

        public bool ValiDateData(DataTable dT, List<BulkSheetField> fieldList, string sheetName, int sheetNo, out string outcome)
        {
            outcome = string.Empty;
            int rowIdx = 0;
            int colSpan = dT.Columns.Count + 1;
            bool allDataValid = true;

            string tableHeader = string.Format("{1}Validation Results for Sheet [{0}] Column / Fields", sheetName, sheetNo);
            StringBuilder strTable = new StringBuilder(@"<table id='dataValidation' cellpadding='0' cellspacing='0' border='0'><thead>");
            try
            {
                //Build Table Headers
                strTable.Append(string.Format("<tr><td colspan='{0}'>{1}</td></tr>", colSpan, tableHeader));
                strTable.Append("<tr><td>Row #</td>");
                foreach (DataColumn col in dT.Columns)
                    strTable.Append(string.Format("<td>{0}</td>", col.ColumnName));
                strTable.Append("</tr></thead><tbody>");

                foreach (DataRow rw in dT.Rows)
                {
                    rowIdx += 1;
                    string row = string.Format("<tr><td>{0}</td>", rowIdx);
                    bool rowValid = true;
                    StringBuilder strRow = new StringBuilder();
                    foreach (DataColumn col in dT.Columns)
                    {
                        string sheetColName = col.ColumnName.Trim().ToLower();
                        var data = rw[sheetColName];
                        var fieldData = fieldList.Where(f => f.SheetFieldName.Trim().ToLower() == sheetColName &&
                                                            f.Active == true).First();
                        if (fieldData != null)
                        {
                            int dataType = fieldData.DataType;
                            string valResult = string.Empty;
                            if (data != null && data != DBNull.Value)
                            {
                                switch (dataType)
                                {
                                    case (int)DataType.Date:
                                        if (!vb.IsDate(data))
                                        {
                                            rowValid = false;
                                            allDataValid = false;
                                            valResult = "date value is expected";
                                        }
                                        break;
                                    case (int)DataType.Decimal:
                                        if (!vb.IsNumeric(data))
                                        {
                                            rowValid = false;
                                            allDataValid = false;
                                            valResult = "a numeric value is expected";
                                        }
                                        break;
                                    case (int)DataType.Integer:
                                        if (!IsInteger(data))
                                        {
                                            rowValid = false;
                                            allDataValid = false;
                                            valResult = "an numeric value with no decimals is required";
                                        }
                                        break;
                                    case (int)DataType.String:
                                        valResult = string.Empty;
                                        break;
                                }
                            }
                            else if (fieldData.IsRequired && (data == null || data == DBNull.Value))
                            {
                                rowValid = false;
                                valResult = "a value is required in this field";
                            }
                            strRow.Append(string.Format("<td>{0}</td>", valResult));
                        }
                        else
                        {
                            rowValid = false;
                            strRow.Append(string.Format("<td>{0}</td>", "this column is not required"));
                        }
                    }

                    if (!rowValid)
                        strTable.Append(string.Format("{0}{1}</tr>", row, strRow.ToString()));
                    strRow = null;
                }
            }
            catch (Exception ex)
            {
                strTable.Append(string.Format("<tr><td colspan='{0}'>The system encountered an error when processing [{1}] sheet. Error details:{2}{3}</td></tr>", colSpan, sheetName, Environment.NewLine, ex.ToString()));
                Util.LogErrors(ex);
            }

            strTable.Append("</tbody></table>");
            outcome = !allDataValid ? string.Empty : strTable.ToString();
            return allDataValid;
        }

        public bool ImportData(DataSet dS, List<BulkSheet> sheetList, List<BulkSheetField> fieldList, string additionalFieldName, object defaultValue, out string outcome)
        {
            outcome = string.Empty;
            int sheetNo = 0;
            bool allprocessed = true;
            StringBuilder strB = new StringBuilder();
            foreach (DataTable dT in dS.Tables)
            {
                string msg = string.Empty;
                string sheetName = dT.TableName.Trim().ToLower();
                var sheet = sheetList.Where(p => p.SheetName.ToLower() == sheetName).FirstOrDefault();
                if (sheet != null)
                {
                    try
                    {
                        if (!string.IsNullOrEmpty(additionalFieldName) && !dT.Columns.Contains(additionalFieldName))
                        {
                            DataColumn col = new DataColumn(additionalFieldName, typeof(int));
                            col.DefaultValue = defaultValue;
                            dT.Columns.Add(col);
                            dT.AcceptChanges();
                            col = null;
                        }

                        sheetNo += 1;
                        var sheetFields = fieldList.Where(p => p.Sheet.SheetName.Trim().ToLower() == sheetName).ToList();
                        bool sheetValid = ValiDateData(dT, sheetFields, sheetName, sheetNo, out outcome);

                        if (!sheetValid) strB.Append(outcome);
                        else
                        {
                            string dbSheetTableName = sheet.DbTableName;
                            var fieldData = fieldList.Where(f => f.Sheet.SheetName.ToLower() == sheetName && f.Active == true);
                            if (fieldData != null)
                            {
                                using (SqlConnection cn = new SqlConnection(Util.GetconnectionString()))
                                {
                                    cn.Open();
                                    using (SqlBulkCopy sheetCopy = new SqlBulkCopy(cn))
                                    {
                                        sheetCopy.BatchSize = BulkCopyBatchSize;
                                        sheetCopy.BulkCopyTimeout = SqlCmdTimeout.CommandTmeout;
                                        foreach (BulkSheetField field in fieldData)
                                        {
                                            if (dT.Columns.Contains(field.SheetFieldName))
                                                sheetCopy.ColumnMappings.Add(new SqlBulkCopyColumnMapping
                                                {
                                                    DestinationColumn = field.DbFieldName,
                                                    SourceColumn = field.SheetFieldName
                                                });
                                        }

                                        sheetCopy.DestinationTableName = dbSheetTableName;
                                        sheetCopy.WriteToServer(dT);
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        allprocessed = false;
                        strB.Append(string.Format("Error when processing [{0}] sheet.", sheetName, Environment.NewLine));
                        Util.LogErrors(ex);
                    }
                }
            }
            return allprocessed;
        }

        public bool UpdateJobTitles(int companyId, string createdBy)
        {
            try
            {
                int result = Dal.SaveJobTitles(companyId, createdBy);
                return (result > 0);
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return false;
            }
        }

        public bool UpdateProjects(int companyId, string createdBy)
        {
            try
            {
                int result = Dal.SaveProjects(companyId, createdBy);
                return (result > 0);
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return false;
            }
        }

        public bool UpdateOrgUnits(int companyId, string createdBy)
        {
            try
            {
                int result = Dal.SaveOrgUnits(companyId, createdBy);
                return (result > 0);
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return false;
            }
        }

        public bool UpdateEmployees(int companyId, string createdBy)
        {
            try
            {
                int result = Dal.SaveEmployees(companyId, createdBy);
                return (result > 0);
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return false;
            }
        }

        public bool LinkInformation(int companyId, string createdBy)
        {
            try
            {
                int result = Dal.LinkData(companyId, createdBy);
                return (result > 0);
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return false;
            }
        }

        public bool UpdatePerformanceMeasures(int companyId, string createdBy, out string outcome)
        {
            outcome = string.Empty;
            try
            {
                int result = Dal.SavePerformanceMeasures(companyId, createdBy, out outcome);
                if (!string.IsNullOrEmpty(outcome))
                    outcome = outcome.Replace(@"\r\n", "<br/>");
                return (result > 0);
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return false;
            }
        }

        public bool DeleteImportedData(int companyId)
        {
            int result = Dal.CleanupData(companyId);
            return (result > 0);
        }

        public void SaveAllData(int companyId, string createdBy)
        {
            Dal.SaveAll(companyId, createdBy);
        }

        public DataTable ValidationResultsOfObjectives(int companyId)
        {
            return Dal.GetValidationResultsOfObjectives(companyId);
        }
    }
}