﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using scorecard.interfaces;
using scorecard.entities;
using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using System.Data;

/* @Copy Albertoncaffeine.net */

namespace scorecard.implementations
{
    public class goalsImpl : Igoals
    {
        #region Igoals Members

        public int addEnvironmentContext(environmentcontext obj)
        {
            return (int)SqlHelper.ExecuteScalar(Util.GetconnectionString(), "PROC_ENVIRONMENT_CONTEXT_UPDATE", obj.ENV_CONTEXT_ID,
                                            obj.COMPANY_ID, obj.AREA_ID, obj.AREA_TYPE_ID, obj.ENV_CONTEXT);
        }

        public environmentcontext getEnvironmentContext(environmentcontext obj)
        {
            environmentcontext data = new environmentcontext
                {
                    ENV_CONTEXT_ID = 0,
                    ENV_CONTEXT = string.Empty
                };

            using (SqlDataReader info = SqlHelper.ExecuteReader(Util.GetconnectionString(), "PROC_ENVIRONMENT_CONTEXT_GET", obj.COMPANY_ID, obj.AREA_ID, obj.AREA_TYPE_ID))
            {
                if (info.Read())
                {
                    data.ENV_CONTEXT_ID = int.Parse(info["ENV_CONTEXT_ID"].ToString());
                    data.ENV_CONTEXT = (info["ENV_CONTEXT"] != DBNull.Value) ? info["ENV_CONTEXT"].ToString() : string.Empty;
                }
            }
            return data;
        }

        public int defineStrategicGoal(strategic _goalDef)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try
            {
                return SqlHelper.ExecuteNonQuery(con, "PROC_ADD_STRATEGIC_GOALS",
                  _goalDef.COMPANY_ID,
                  _goalDef.PERSPECTIVE,
                  _goalDef.OBJECTIVE,
                  _goalDef.RATIONALE,
                  _goalDef.UPDATE_BY
                 );
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return 0;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public int updateStrategicGoal(strategic _goalDef)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try
            {
                return SqlHelper.ExecuteNonQuery(con, "PROC_UPDATE_STRATEGIC_GOAL",
                  _goalDef.ID,
                  _goalDef.COMPANY_ID,
                  _goalDef.PERSPECTIVE,
                  _goalDef.OBJECTIVE,
                  _goalDef.RATIONALE,
                  _goalDef.UPDATE_BY
                 );
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return 0;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public int deleteStrategicGoal(strategic _goalDef)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try
            {
                object result = SqlHelper.ExecuteScalar(con, "PROC_DELETE_STRATEGIC_GOAL",
                  _goalDef.ID,
                  _goalDef.UPDATE_BY
                 );
                return (result == DBNull.Value) ? 0 : int.Parse(result.ToString());
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return 0;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public strategic getStrategicInfo(int strategicId)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            SqlDataReader read = null;
            try
            {
                read = SqlHelper.ExecuteReader(con, "PROC_GET_STRATEGIC_BY_ID", strategicId);
                if (read.Read())
                {
                    return new strategic
                     {
                         ID = int.Parse(read["ID"].ToString()),
                         COMPANY_ID = int.Parse(read["COMPANY_ID"].ToString()),
                         PERSPECTIVE = int.Parse(read["PERSPECTIVE"].ToString()),
                         OBJECTIVE = read["OBJECTIVE"] != DBNull.Value ?
                                     read["OBJECTIVE"].ToString() : string.Empty,
                         DATE_CREATED = DateTime.Parse(read["DATE_CREATED"].ToString()),
                         RATIONALE = read["RATIONALE"] != DBNull.Value ?
                             read["RATIONALE"].ToString() : string.Empty
                     };
                }
                return new strategic();
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
            finally
            {
                read.Dispose();
                read.Close();
                con.Dispose();
                con.Close();
            }
        }
        public List<strategic> listStrategicGoals(company _company)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            SqlDataReader read = null;
            try
            {
                List<strategic> listStrategic = new List<strategic>();
                read = SqlHelper.ExecuteReader(con, "PROC_GET_ALL_STRATEGIC_COMPANY_GOALS", _company.COMPANY_ID);
                while (read.Read())
                {
                    strategic str = new strategic
                    {
                        ID = (read["ID"] == DBNull.Value) ? 0 : int.Parse(read["ID"].ToString()),
                        COMPANY_ID = (read["COMPANY_ID"] == DBNull.Value) ? 0 : int.Parse(read["COMPANY_ID"].ToString()),
                        PERSPECTIVE = (read["PERSPECTIVE"] == DBNull.Value) ? 0 : int.Parse(read["PERSPECTIVE"].ToString()),
                        OBJECTIVE = read["OBJECTIVE"] != DBNull.Value ?
                                    read["OBJECTIVE"].ToString() : string.Empty,
                        DATE_CREATED = DateTime.Parse(read["DATE_CREATED"].ToString()),
                        RATIONALE = read["RATIONALE"] != DBNull.Value ?
                            read["RATIONALE"].ToString() : string.Empty
                    };
                    listStrategic.Add(str);
                }
                return listStrategic;
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
            finally
            {
                read.Dispose();
                read.Close();
                con.Dispose();
                con.Close();
            }
        }

        public int defineSubGeneralGoal(goals _goalDef)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try
            {
                return (int)SqlHelper.ExecuteScalar(con, "PROC_ADD_COMPANY_SUB_GOALS",
                    _goalDef.PARENT_ID,
                    _goalDef.STRATEGIC_ID,
                    _goalDef.AREA_TYPE_ID,
                    _goalDef.GOAL_DESCRIPTION,
                    _goalDef.UNIT_MEASURE,
                    _goalDef.TARGET,
                    _goalDef.WEIGHT,
                    _goalDef.AREA_ID,
                    _goalDef.COMPANY_ID,
                    _goalDef.RATIONAL
                 );
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return 0;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public int defineGeneralGoal(goals _goalDef)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try
            {
                return (int)SqlHelper.ExecuteScalar(con, "PROC_ADD_COMPANY_GOALS",
                  _goalDef.STRATEGIC_ID,
                  _goalDef.AREA_TYPE_ID,
                  _goalDef.GOAL_DESCRIPTION,
                  _goalDef.UNIT_MEASURE,
                  _goalDef.TARGET,
                  _goalDef.WEIGHT,
                  _goalDef.AREA_ID,
                  _goalDef.ENV_CONTEXT,
                  _goalDef.COMPANY_ID,
                  _goalDef.ENV_CONTEXT_ID,
                  _goalDef.RATIONAL
                 );
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return 0;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public int updateSubGeneralGoal(goals _goalDef)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try
            {
                return (int)SqlHelper.ExecuteScalar(con, "PROC_UPDATE_COMPANY_SUB_GOAL",
                  _goalDef.ID,
                  _goalDef.STRATEGIC_ID,
                  _goalDef.AREA_TYPE_ID,
                  _goalDef.GOAL_DESCRIPTION,
                  _goalDef.UNIT_MEASURE,
                  _goalDef.TARGET,
                  _goalDef.WEIGHT,
                  _goalDef.AREA_ID,
                  _goalDef.RATIONAL
                 );
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return 0;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public int updateGeneralGoal(goals _goalDef)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try
            {
                return (int)SqlHelper.ExecuteScalar(con, "PROC_UPDATE_COMPANY_GOAL",
                  _goalDef.ID,
                  _goalDef.STRATEGIC_ID,
                  _goalDef.AREA_TYPE_ID,
                  _goalDef.GOAL_DESCRIPTION,
                  _goalDef.UNIT_MEASURE,
                  _goalDef.TARGET,
                  _goalDef.WEIGHT,
                  _goalDef.AREA_ID,
                  _goalDef.RATIONAL,
                  _goalDef.ENV_CONTEXT,
                  _goalDef.ENV_CONTEXT_ID,
                  _goalDef.COMPANY_ID,
                  _goalDef.HAS_SUB_GOALS
                 );
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return 0;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public int deleteGeneralGoal(goals _goalDef)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try
            {
                object result = SqlHelper.ExecuteScalar(con, "PROC_DELETE_COMPANY_GOAL",
                  _goalDef.ID,
                  _goalDef.UPDATE_BY);
                return (result != DBNull.Value) ? int.Parse(result.ToString()) : 0;
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return 0;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public List<goals> listGeneralGoalsByCriteria(int companyId, int areaTypeId, int areaValueId)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            SqlDataReader read = null;
            try
            {
                int? defInt = null;
                List<goals> listStrategic = new List<goals>();
                read = SqlHelper.ExecuteReader(con, " PROC_GET_ALL_COMPANY_GOALS_BY_AREA_AND_TYPEID", companyId, areaTypeId, areaValueId);
                while (read.Read())
                {
                    goals str = new goals
                    {
                        ID = int.Parse(read["ID"].ToString()),
                        COMPANY_ID = int.Parse(read["COMPANY_ID"].ToString()),
                        STRATEGIC_ID = int.Parse(read["STRATEGIC_ID"].ToString()),
                        AREA_TYPE_ID = int.Parse(read["AREA_TYPE_ID"].ToString()),
                        GOAL_DESCRIPTION = read["GOAL_DESCRIPTION"].ToString(),
                        UNIT_MEASURE = (read["UNIT_MEASURE"] == DBNull.Value) ? defInt : int.Parse(read["UNIT_MEASURE"].ToString()),
                        TARGET = read["TARGET"].ToString(),
                        WEIGHT = int.Parse(read["WEIGHT"].ToString()),
                        DATE_CREATED = DateTime.Parse(read["DATE_CREATED"].ToString()),
                        AREA_ID = int.Parse(read["AREA_ID"].ToString()),
                        PERIOD_DATE = DateTime.Parse(read["PERIOD_DATE"].ToString()),
                        ENV_CONTEXT_ID = (read["ENV_CONTEXT_ID"] != DBNull.Value) ? int.Parse(read["ENV_CONTEXT_ID"].ToString()) : defInt,
                        ENV_CONTEXT = read["ENV_CONTEXT"] != DBNull.Value ? read["ENV_CONTEXT"].ToString() : string.Empty,
                        RATIONAL = read["RATIONAL"] != DBNull.Value ? read["RATIONAL"].ToString() : string.Empty,
                        PARENT_ID = read["PARENT_ID"] != DBNull.Value ? int.Parse(read["PARENT_ID"].ToString()) : defInt,
                        HAS_SUB_GOALS = bool.Parse(read["HAS_SUB_GOALS"].ToString())
                    };
                    listStrategic.Add(str);
                }
                read.Dispose();
                return listStrategic;
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
            finally
            {
                read.Dispose();
                read.Close();
                con.Dispose();
                con.Close();
            }
        }

        public List<goals> listGeneralGoals(company _company)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            SqlDataReader read = null;
            try
            {
                int? defInt = null;
                List<goals> listStrategic = new List<goals>();
                read = SqlHelper.ExecuteReader(con, "PROC_GET_ALL_COMPANY_GOALS", _company.COMPANY_ID);
                while (read.Read())
                {
                    goals str = new goals
                    {
                        ID = int.Parse(read["ID"].ToString()),
                        COMPANY_ID = int.Parse(read["COMPANY_ID"].ToString()),
                        STRATEGIC_ID = int.Parse(read["STRATEGIC_ID"].ToString()),
                        AREA_TYPE_ID = int.Parse(read["AREA_TYPE_ID"].ToString()),
                        GOAL_DESCRIPTION = read["GOAL_DESCRIPTION"].ToString(),
                        UNIT_MEASURE = (read["UNIT_MEASURE"] == DBNull.Value) ? defInt : int.Parse(read["UNIT_MEASURE"].ToString()),
                        TARGET = read["TARGET"].ToString(),
                        WEIGHT = (read["WEIGHT"] != DBNull.Value) ? int.Parse(read["WEIGHT"].ToString()) : 0,
                        DATE_CREATED = DateTime.Parse(read["DATE_CREATED"].ToString()),
                        AREA_ID = int.Parse(read["AREA_ID"].ToString()),
                        PERIOD_DATE = DateTime.Parse(read["PERIOD_DATE"].ToString()),
                        ENV_CONTEXT_ID = (read["ENV_CONTEXT_ID"] != DBNull.Value) ? int.Parse(read["ENV_CONTEXT_ID"].ToString()) : defInt,
                        ENV_CONTEXT = read["ENV_CONTEXT"] != DBNull.Value ? read["ENV_CONTEXT"].ToString() : string.Empty,
                        RATIONAL = read["RATIONAL"] != DBNull.Value ? read["RATIONAL"].ToString() : string.Empty,
                        PARENT_ID = read["PARENT_ID"] != DBNull.Value ? int.Parse(read["PARENT_ID"].ToString()) : defInt,
                        HAS_SUB_GOALS = bool.Parse(read["HAS_SUB_GOALS"].ToString())
                    };
                    listStrategic.Add(str);
                }
                read.Dispose();
                return listStrategic;
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
            finally
            {
                read.Dispose();
                read.Close();
                con.Dispose();
                con.Close();
            }
        }

        public DataTable listGeneralGoals(int companyId, int areaTypeId, int areaId)
        {
            return SqlHelper.ExecuteDataset(Util.GetconnectionString(), "PROC_GET_ALL_COMPANY_GOALS_BYCRITERIA", companyId, areaTypeId, areaId).Tables[0];
        }

        public goals getGoal(int id)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            SqlDataReader read = null;
            try
            {
                int? defInt = null;
                read = SqlHelper.ExecuteReader(con, "PROC_GET_GOAL_BYID", id);
                if (read.Read())
                {
                    return new goals
                     {
                         ID = int.Parse(read["ID"].ToString()),
                         COMPANY_ID = int.Parse(read["COMPANY_ID"].ToString()),
                         STRATEGIC_ID = int.Parse(read["STRATEGIC_ID"].ToString()),
                         AREA_TYPE_ID = int.Parse(read["AREA_TYPE_ID"].ToString()),
                         GOAL_DESCRIPTION = read["GOAL_DESCRIPTION"].ToString(),
                         UNIT_MEASURE = (read["UNIT_MEASURE"] != DBNull.Value) ? int.Parse(read["UNIT_MEASURE"].ToString()) : defInt,
                         TARGET = (read["TARGET"] != DBNull.Value) ? read["TARGET"].ToString() : string.Empty,
                         WEIGHT = (read["WEIGHT"] == DBNull.Value) ? 0 : int.Parse(read["WEIGHT"].ToString()),
                         DATE_CREATED = DateTime.Parse(read["DATE_CREATED"].ToString()),
                         AREA_ID = int.Parse(read["AREA_ID"].ToString()),
                         PARENT_ID = (read["PARENT_ID"] == DBNull.Value) ? defInt : int.Parse(read["PARENT_ID"].ToString()),
                         HAS_SUB_GOALS = bool.Parse(read["HAS_SUB_GOALS"].ToString())
                     };

                }
                return new goals();
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
            finally
            {
                read.Dispose();
                read.Close();
                con.Dispose();
                con.Close();
            }
        }
        #endregion
    }
}