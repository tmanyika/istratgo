﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using scorecard.interfaces;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using scorecard.entities;

/* @Copy Albertoncaffeine.net */

namespace scorecard.implementations
{
    public class jobtitleImpl : Ijobtitles
    {
        #region Ijobtitles Members

        public int addJobTitle(JobTitle _title)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try{
                return SqlHelper.ExecuteNonQuery(con, "PROC_ADD_JOB_TITLES",
                _title.COMPANY_ID,
                _title.NAME
                 );
            }
            catch (Exception e){
                Util.LogErrors(e);
                return 0;
            }
            finally{
                con.Close();
                con.Dispose();
            }
        }

        public int updateJobTitle(JobTitle _title)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try{
                return SqlHelper.ExecuteNonQuery(con, "PROC_UPDATE_JOB_TITLE",
                _title.ID,
                _title.NAME
                 );
            }
            catch (Exception e){
                Util.LogErrors(e);
                return 0;
            }
            finally{
                con.Close();
                con.Dispose();
            }
        }

        public int deleteJobTitle(JobTitle _title)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try{
                return SqlHelper.ExecuteNonQuery(con, "PROC_DELETE_JOB_TITLE",
                _title.ID                
                 );
            }
            catch (Exception e){
                Util.LogErrors(e);
                return 0;
            }
            finally{
                con.Close();
                con.Dispose();
            }
        }

        public List<JobTitle> getAllCompanyJobTitles(company _company){
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            SqlDataReader read = null;
            try {
                List<JobTitle> jobtitles = new List<JobTitle>();
                read = SqlHelper.ExecuteReader(con, "PROC_GET_ALL_TITLES_BY_COMPANY", _company.COMPANY_ID);
                while (read.Read())
                {
                    jobtitles.Add(new JobTitle
                    {
                        ID = read.GetInt32(0),
                        NAME = read.GetString(2),
                        COMPANY_ID = read.GetInt32(1),
                        ACTIVE = read.GetBoolean(3)
                    });
                }
                return jobtitles;
            }
            catch (Exception ex){
                Util.LogErrors(ex);
                return null;
            }
            finally{
                read.Dispose();
                read.Close();
                con.Dispose();
                con.Close();
            }
        }

        public JobTitle getJobTitleInfo(JobTitle _title)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            SqlDataReader read = null;
            try
            {
                List<JobTitle> jobtitles = new List<JobTitle>();
                read = SqlHelper.ExecuteReader(con, "PROC_GET_TITLE_BY_ID", _title.ID);
                while (read.Read()){
                    jobtitles.Add(new JobTitle
                    {
                        ID = read.GetInt32(0),
                        NAME = read.GetString(2),
                        COMPANY_ID = read.GetInt32(1),
                        ACTIVE = read.GetBoolean(3)
                    });
                }
                return jobtitles[0];
            }
            catch (Exception ex){
                Util.LogErrors(ex);
                return null;
            }
            finally{
                read.Dispose();
                read.Close();
                con.Dispose();
                con.Close();
            }
        }

        #endregion
    }
}