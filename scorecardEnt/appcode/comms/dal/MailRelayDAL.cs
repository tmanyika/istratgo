﻿using System;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

namespace System.Email.Communication
{
    public class MailRelayDAL
    {
        #region Variables

        private string _conString;

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return _conString; }
            set { _conString = value; }
        }

        #endregion

        #region Default Constructors

        public MailRelayDAL(string conString)
        {
            _conString = conString;
        }

        #endregion

        #region Public Methods & Functions

        public DataTable Get(int mailId)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_EmailRelay_Get", mailId).Tables[0];
        }

        public DataTable GetByBatchId(int batchId)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_EmailRelay_GetByBatchId", batchId).Tables[0];
        }   

        #endregion
    }
}