﻿using System;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

namespace System.Email.Communication
{
    public class MailTemplateDAL
    {
        #region Variables

        private string _conString;

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return _conString; }
            set { _conString = value; }
        }

        #endregion

        #region Default Constructors

        public MailTemplateDAL(string conString)
        {
            _conString = conString;
        }

        #endregion

        #region Public Methods & Functions

        public DataTable GetAll()
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_EmailTemplate_GetAll").Tables[0];
        }

        public DataTable Get(bool active)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_EmailTemplate_GetActive", active).Tables[0];
        }

        public DataTable GetById(int mailId)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_EmailTemplate_GetById", mailId).Tables[0];
        }

        public DataTable Get(bool active, bool ismaster)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_EmailTemplate_GetTemplates", active, ismaster).Tables[0];
        }


        public int Save(MailTemplate obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_EmailTemplate_Insert", obj.TemplateId, obj.MailName,
                                                        obj.MailSubject, obj.MailContent, obj.Active, obj.IsMasterTemplate, obj.CreatedBy);
            return result;
        }

        public int Update(MailTemplate obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_EmailTemplate_Update", obj.TemplateId, obj.MailId, obj.MailName,
                                                        obj.MailSubject, obj.MailContent, obj.Active, obj.IsMasterTemplate, obj.UpdatedBy);
            return result;
        }

        public int Deactivate(int mailId, string updatedBy)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_EmailTemplate_Deactivate", mailId, updatedBy);
            return result;
        }

        #endregion
    }
}