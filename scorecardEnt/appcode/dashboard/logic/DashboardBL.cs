﻿using System;
using System.Data;
using System.Linq;
using scorecard.implementations;
using scorecard.entities;
using System.Collections.Generic;
using System.Collections;

namespace Dashboard.Reporting
{
    public class DashboardBL : IDashboard
    {
        #region Properties

        public string ConnectionString
        {
            get { return Util.GetconnectionString(); }
        }

        public DashBoardDAL Dal
        {
            get { return new DashBoardDAL(ConnectionString); }
        }

        #endregion

        #region Default Constructors

        public DashboardBL() { }

        #endregion

        #region Public Methods & Functions

        public DataTable GetObjectives(int areaId, int orgUnitId, bool isCompany, string startDate, string endDate)
        {
            return Dal.GetObjective(areaId, orgUnitId, isCompany, startDate, endDate);
        }

        public DataTable GetPerspectives(int areaId, int orgUnitId, bool isCompany, string startDate, string endDate)
        {
            return Dal.GetPerspective(areaId, orgUnitId, isCompany, startDate, endDate);
        }

        public DataTable GetFocusAreas(int areaId, int orgUnitId, bool isCompany, string startDate, string endDate)
        {
            return Dal.GetFocusArea(areaId, orgUnitId, isCompany, startDate, endDate);
        }

        public DataTable GetFocusAreas(int areaId, int orgUnitId, bool isCompany)
        {
            DataTable dS = Dal.GetFocusArea(areaId, orgUnitId, isCompany);

            List<scores> objectives = new List<scores>();
            foreach (DataRow read in dS.Rows)
            {
                objectives.Add(new scores
                {
                    ID = int.Parse(read["ID"].ToString()),
                    CRITERIA_TYPE_ID = int.Parse(read["CRITERIA_TYPE_ID"].ToString()),
                    SELECTED_ITEM_VALUE = int.Parse(read["SELECTED_ITEM_VALUE"].ToString()),
                    FINAL_SCORE = double.Parse(read["FINAL_SCORE"].ToString()),
                    WEIGHT = int.Parse(read["WEIGHT"].ToString()),
                    TARGET = read["TARGET"] != DBNull.Value ? read["TARGET"].ToString() : string.Empty,
                    PERIOD_DATE = DateTime.Parse(read["PERIOD_DATE"].ToString()),
                    PERSPECTIVE_NAME = read["PERSPECTIVE_NAME"].ToString(),
                    PERSPECTIVE_ID = int.Parse(read["PERSPECTIVE_ID"].ToString()),
                });
            }

            DataTable dT = new DataTable();
            dT.Columns.Add("Perspective", typeof(string));
            dT.Columns.Add("PerspectiveValue", typeof(int));
            string prevPerspective = "";
            foreach (scores item in objectives)
            {
                string nxtPerspective = item.PERSPECTIVE_NAME;
                if (string.Compare(nxtPerspective, prevPerspective) != 0)
                {
                    int pid = item.PERSPECTIVE_ID;
                    int total = objectives.Where(c => c.PERSPECTIVE_ID == pid).Sum(u => u.WEIGHT);

                    DataRow nRow = dT.NewRow();
                    nRow["Perspective"] = string.Format("{0} {1}%", Util.getGraphLabelName(nxtPerspective), total);
                    nRow["PerspectiveValue"] = total;
                    dT.Rows.Add(nRow);
                    nRow = null;
                }
                prevPerspective = nxtPerspective;
            }
            dT.AcceptChanges();
            return dT;
        }

        #region Performance Ratings

        public DataTable GetUserPerfomanceRating(int areaId, int orgUnitId, bool isCompany, int year)
        {
            return Dal.GetPerfomanceRating(areaId, orgUnitId, isCompany, year);
        }

        public DataTable GetUserPerfomanceRating(int areaId, int orgUnitId, bool isCompany, string startDate, string endDate)
        {
            return Dal.GetPerfomanceRating(areaId, orgUnitId, isCompany, startDate, endDate);
        }

        DataTable GetDataSchema()
        {
            DataTable dT = new DataTable();
            dT.Columns.Add("ItemName", typeof(string));
            dT.Columns.Add("ActualValue", typeof(string));
            dT.Columns.Add("TargetValue", typeof(string));
            dT.Columns.Add("DefinedActualValue", typeof(int));
            dT.AcceptChanges();
            return dT;
        }

        ArrayList GetUniqueList(List<PerformanceReport> data)
        {
            ArrayList arrItem = new ArrayList();
            ArrayList arr = new ArrayList();

            foreach (PerformanceReport item in data)
            {
                int itemId = item.ItemId;
                if (!arrItem.Contains(itemId))
                {
                    arrItem.Add(itemId);
                    arr.Add(new ItemValue
                    {
                        ItemId = item.ItemId,
                        ItemName = item.ItemName
                    });
                }
            }
            return arr;
        }

        ArrayList GetPeriodTotal(List<PerformanceReport> data, int itemId)
        {
            var tmpData = data.Where(i => i.ItemId == itemId);
            ArrayList arr = new ArrayList();

            foreach (PerformanceReport item in data)
            {
                if (!arr.Contains(item.PERIOD_DATE))
                    arr.Add(item.PERIOD_DATE);
            }
            return arr;
        }

        public DataTable GetPerfomanceRatings(int areaId, int orgUnitId, bool isCompany, string startDate, string endDate)
        {
            return GetUserPerfomanceRating(areaId, orgUnitId, isCompany, startDate, endDate);
        }

        //public DataTable GetPerfomanceRatings(int areaId, int orgUnitId, bool isCompany, string startDate, string endDate)
        //{
        //    DataTable dS = GetUserPerfomanceRating(areaId, orgUnitId, isCompany, startDate, endDate);
        //    List<PerformanceReport> data = new List<PerformanceReport>();

        //    foreach (DataRow read in dS.Rows)
        //    {
        //        data.Add(new PerformanceReport
        //        {
        //            ID = int.Parse(read["ID"].ToString()),
        //            CRITERIA_TYPE_ID = int.Parse(read["CRITERIA_TYPE_ID"].ToString()),
        //            SELECTED_ITEM_VALUE = int.Parse(read["SELECTED_ITEM_VALUE"].ToString()),
        //            GOAL_ID = int.Parse(read["GOAL_ID"].ToString()),
        //            SCORES = read["SCORES"] != DBNull.Value ? read["SCORES"].ToString() : string.Empty,
        //            FINAL_SCORE = double.Parse(read["FINAL_SCORE"].ToString()),
        //            FINAL_SCORE_AGREED = (read["FINAL_SCORE_AGREED"] != DBNull.Value) ? Math.Round(decimal.Parse(read["FINAL_SCORE_AGREED"].ToString()), 1) : 0,
        //            WEIGHT = int.Parse(read["WEIGHT"].ToString()),
        //            UNIT_MEASURE_NAME = read["UNIT_MEASURE"].ToString(),
        //            TARGET = read["TARGET"] != DBNull.Value ? read["TARGET"].ToString() : string.Empty,
        //            GOAL_DESCRIPTION = read["GOAL_DESCRIPTION"].ToString(),
        //            PERIOD_DATE = DateTime.Parse(read["PERIOD_DATE"].ToString()),
        //            PERSPECTIVE_NAME = read["PERSPECTIVE_NAME"].ToString(),
        //            OBJECTIVE = read["OBJECTIVE"].ToString(),
        //            PERSPECTIVE_ID = int.Parse(read["PERSPECTIVE_ID"].ToString()),
        //            ItemId = (read["ItemId"] != DBNull.Value) ? int.Parse(read["ItemId"].ToString()) : 0,
        //            ItemName = (read["ItemName"] != DBNull.Value) ? read["ItemName"].ToString() : string.Empty
        //        });
        //    }

        //    DataTable dT = GetDataSchema();
        //    if (data.Count > 0)
        //    {
        //        double totalTarget = 100;
        //        var empList = GetUniqueList(data);
        //        foreach (ItemValue item in empList)
        //        {
        //            int itemId = item.ItemId;
        //            string name = item.ItemName;

        //            ItemValue obj = new ItemValue
        //            {
        //                ItemId = itemId,
        //                ItemName = name,
        //            };

        //            var prd = GetPeriodTotal(data, itemId);
        //            var periodTime = prd.Count;

        //            double totalActual = 0;
        //            double definedActual = 0;
        //            foreach (DateTime date in prd)
        //            {
        //                var finalDefinedScore = data.Where(c => c.ItemId == itemId && c.PERIOD_DATE == date).Sum(u => u.FINAL_SCORE_AGREED);
        //                var finalScore = data.Where(c => c.ItemId == itemId && c.PERIOD_DATE == date).Sum(u => u.FINAL_SCORE);
        //                var finalWeightTotal = data.Where(c => c.ItemId == itemId && c.PERIOD_DATE == date).Sum(u => u.WEIGHT);

        //                if (finalWeightTotal == 0) periodTime = periodTime - 1;
        //                else
        //                {
        //                    var finalPercentage = (finalWeightTotal != 0) ? (finalScore / (double)finalWeightTotal) * 100 : 0;
        //                    totalActual += finalPercentage;
        //                    definedActual += (double)finalDefinedScore;
        //                }
        //            }

        //            if (periodTime > 0)
        //            {
        //                var averageScore = Math.Round(totalActual / (double)periodTime, 0, MidpointRounding.AwayFromZero);
        //                var averageDefinedScore = Math.Round(definedActual / (double)periodTime, 0, MidpointRounding.AwayFromZero);

        //                DataRow nRow = dT.NewRow();
        //                nRow["ItemName"] = item.ItemName;
        //                nRow["TargetValue"] = totalTarget;
        //                nRow["ActualValue"] = averageScore;
        //                nRow["DefinedActualValue"] = averageDefinedScore;
        //                dT.Rows.Add(nRow);
        //                nRow = null;
        //                obj = null;
        //            }
        //        }
        //        dT.AcceptChanges();
        //    }

        //    return dT;
        //}
        #endregion

        public DataTable GetTalentMatrix(int areaId, int orgUnitId, bool isCompany, string startDate, string endDate)
        {
            return Dal.GetTalentMatrixData(areaId, orgUnitId, isCompany, startDate, endDate);
        }

        public DataTable GetAssessmentRatings(int areaId, int valueId, string startDate, string endDate)
        {
            return Dal.GetAssessmentData(areaId, valueId, startDate, endDate);
        }

        #endregion
    }
}