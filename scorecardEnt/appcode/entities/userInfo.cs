﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace scorecard.entities
{
    [Serializable]
    public class UserInfo
    {
        public int CountryId { get; set; }
        public int CompanyId { get; set; }
        public int OrgId { get; set; }
        public int? UserTypeId { get; set; }
        public int UserId { get; set; }
        public int? ReportToUserId { get; set; }

        public string LoginId { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        
        public bool ShowAgain { get; set; }

        public UserInfo() { }
    }
}