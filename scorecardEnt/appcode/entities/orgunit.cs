﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HR.Human.Resources;

/* @Copy Albertoncaffeine.net */

namespace scorecard.entities
{
    public class Orgunit
    {
        public int ID { get; set; }
        public int OWNER_ID { get; set; }
        public int COMPANY_ID { get; set; }
        public int PARENT_ORG { get; set; }
        public int ORGUNT_TYPE { get; set; }

        public string ORGUNIT_NAME { get; set; }
        public string CREATED_BY { get; set; }
        public string UPDATED_BY { get; set; }

        public DateTime? DATE_CREATED { get; set; }
        public DateTime? DATE_UPDATED { get; set; }
        public Employee Owner { get; set; }

        public bool ACTIVE { get; set; }
    }
}