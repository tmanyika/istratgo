﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/* @Copy Albertoncaffeine.net */

namespace scorecard.entities
{
    public class roles
    {
        public int ROLE_ID { get; set; }
        public int ROLE_TYPE{ get;set; }
        public string ROLE_NAME { get; set; }
        public bool ACTIVE { get; set; }
        public DateTime DATE_CREATED { get; set; }
    }
}