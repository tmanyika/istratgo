﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using scorecard.entities;
using System.Data;
using HierarchicalData;

/* @Copy Albertoncaffeine.net */

namespace scorecard.interfaces
{
    public interface Irolebased {
        int createAccess(menuaccess _menu);
        int deleteAccessByCompany(menuaccess _menu);
        List<menu> getAllMenus();     
        List<menuaccess> getAccessByCompany(company _company);
        HierarchicalDataSet getAccess(int companyId, int userRole); 
    }
}