﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using scorecard.entities;

/* @Copy Albertoncaffeine.net */

namespace scorecard.interfaces
{
    public interface Ijobtitles
    {
        int addJobTitle(JobTitle _title);
        int updateJobTitle(JobTitle _title);
        int deleteJobTitle(JobTitle _title);
        
        List<JobTitle> getAllCompanyJobTitles(company _company);
        JobTitle getJobTitleInfo(JobTitle _title);
    }
}