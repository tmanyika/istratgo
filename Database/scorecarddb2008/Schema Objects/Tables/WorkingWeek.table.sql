﻿CREATE TABLE [imd_user_db].[WorkingWeek] (
    [WorkingWeekId] INT           IDENTITY (1, 1) NOT NULL,
    [CompanyId]     INT           NOT NULL,
    [Name]          VARCHAR (200) NOT NULL,
    [NoOfDays]      SMALLINT      NOT NULL,
    [Active]        BIT           NOT NULL,
    [CreatedBy]     VARCHAR (20)  NOT NULL,
    [DateCreated]   DATETIME      NOT NULL,
    [DateUpdated]   DATETIME      NULL,
    [UpdatedBy]     VARCHAR (20)  NULL
);

