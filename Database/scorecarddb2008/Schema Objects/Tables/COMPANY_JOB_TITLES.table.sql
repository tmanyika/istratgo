﻿CREATE TABLE [imd_user_db].[COMPANY_JOB_TITLES] (
    [ID]          INT          IDENTITY (1, 1) NOT NULL,
    [COMPANY_ID]  INT          NULL,
    [NAME]        VARCHAR (50) COLLATE Latin1_General_CI_AS NULL,
    [ACTIVE]      BIT          NULL,
    [ISADMIN]     BIT          NULL,
    [CREATEDBY]   VARCHAR (50) NULL,
    [DATECREATED] DATETIME     NOT NULL
);

