﻿CREATE TABLE [imd_user_db].[USER_ADMIN] (
    [ID]            INT           IDENTITY (1, 1) NOT NULL,
    [LOGIN_ID]      VARCHAR (20)  COLLATE Latin1_General_CI_AS NOT NULL,
    [PASSWORD]      VARCHAR (250) COLLATE Latin1_General_CI_AS NOT NULL,
    [REPORTS_TO]    INT           NULL,
    [ORG_ID]        INT           NULL,
    [JOB_TITLE_ID]  INT           NULL,
    [NAME]          VARCHAR (100) COLLATE Latin1_General_CI_AS NULL,
    [EMAIL_ADDRESS] VARCHAR (150) COLLATE Latin1_General_CI_AS NULL,
    [DATE_CREATED]  DATETIME      NULL,
    [ACTIVE]        BIT           NOT NULL,
    [USER_TYPE]     INT           NULL
);

