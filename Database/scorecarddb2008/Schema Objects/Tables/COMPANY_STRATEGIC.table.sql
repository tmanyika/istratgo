﻿CREATE TABLE [imd_user_db].[COMPANY_STRATEGIC] (
    [ID]           INT           IDENTITY (1, 1) NOT NULL,
    [COMPANY_ID]   INT           NULL,
    [PERSPECTIVE]  INT           NULL,
    [OBJECTIVE]    VARCHAR (150) COLLATE Latin1_General_CI_AS NULL,
    [DATE_CREATED] DATETIME      NULL
);

