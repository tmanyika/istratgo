﻿CREATE TABLE [imd_user_db].[LeaveBalance] (
    [EmployeeId]      INT             NOT NULL,
    [LeaveId]         INT             NOT NULL,
    [Accumulated]     DECIMAL (18, 4) NOT NULL,
    [Balance]         DECIMAL (18, 4) NOT NULL,
    [DaysTaken]       DECIMAL (18, 4) NOT NULL,
    [LimitValue]      DECIMAL (18, 4) NOT NULL,
    [DateLastUpdated] DATETIME        NOT NULL
);

