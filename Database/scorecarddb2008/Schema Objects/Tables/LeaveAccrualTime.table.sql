﻿CREATE TABLE [imd_user_db].[LeaveAccrualTime] (
    [AccrualTimeId] INT           IDENTITY (1, 1) NOT NULL,
    [TimeName]      VARCHAR (200) NOT NULL,
    [TimeValue]     SMALLINT      NOT NULL,
    [Active]        BIT           NOT NULL,
    [CreatedBy]     VARCHAR (20)  NOT NULL,
    [DateCreated]   DATETIME      NOT NULL,
    [DateUpdated]   DATETIME      NULL,
    [UpdatedBy]     VARCHAR (20)  NULL
);

