﻿CREATE TABLE [imd_user_db].[MailTemplate] (
    [MailId]           INT            IDENTITY (1, 1) NOT NULL,
    [TemplateId]       INT            NULL,
    [MailName]         VARCHAR (800)  NOT NULL,
    [MailSubject]      VARCHAR (800)  NOT NULL,
    [MailContent]      NVARCHAR (MAX) NOT NULL,
    [IsMasterTemplate] BIT            NOT NULL,
    [Active]           BIT            NOT NULL,
    [CreatedBy]        VARCHAR (20)   NOT NULL,
    [DateCreated]      DATETIME       NOT NULL,
    [DateUpdated]      DATETIME       NULL,
    [UpdatedBy]        VARCHAR (20)   NULL
);

