﻿CREATE TABLE [imd_user_db].[LeaveAccrual] (
    [LeaveAccrualId]   INT             IDENTITY (1, 1) NOT NULL,
    [LeaveId]          INT             NOT NULL,
    [WorkingWeekId]    INT             NULL,
    [AccrualTimeId]    INT             NOT NULL,
    [AccrualRate]      DECIMAL (18, 4) NOT NULL,
    [MaximumThreshold] DECIMAL (18, 4) NOT NULL,
    [Active]           BIT             NOT NULL,
    [CreatedBy]        VARCHAR (20)    NOT NULL,
    [DateCreated]      DATETIME        NOT NULL,
    [DateUpdated]      DATETIME        NULL,
    [UpdatedBy]        VARCHAR (20)    NULL
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Monthly, Yearly e.t.c. Monthly = 1, Quarterly = 3, Bi-Annually = 6, Yearly = 12', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'LeaveAccrual', @level2type = N'COLUMN', @level2name = N'AccrualTimeId';

