﻿ALTER TABLE [imd_user_db].[LeaveCalendar]
    ADD CONSTRAINT [PK_LeaveCalendar] PRIMARY KEY CLUSTERED ([HolidayDateId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);

