﻿ALTER TABLE [imd_user_db].[Employee]
    ADD CONSTRAINT [FK_Employee_Employee] FOREIGN KEY ([LineManagerEmployeeId]) REFERENCES [imd_user_db].[Employee] ([EmployeeId]) ON DELETE NO ACTION ON UPDATE NO ACTION;

