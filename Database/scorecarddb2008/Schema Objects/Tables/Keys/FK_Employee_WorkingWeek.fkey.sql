﻿ALTER TABLE [imd_user_db].[Employee]
    ADD CONSTRAINT [FK_Employee_WorkingWeek] FOREIGN KEY ([WorkingWeekId]) REFERENCES [imd_user_db].[WorkingWeek] ([WorkingWeekId]) ON DELETE NO ACTION ON UPDATE NO ACTION;

