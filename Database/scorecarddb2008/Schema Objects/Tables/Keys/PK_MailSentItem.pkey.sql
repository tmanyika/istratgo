﻿ALTER TABLE [imd_user_db].[MailSentItem]
    ADD CONSTRAINT [PK_MailSentItem] PRIMARY KEY CLUSTERED ([SentItemId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);

