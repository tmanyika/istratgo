﻿CREATE TABLE [imd_user_db].[MailServer] (
    [AccountMailId]  INT             IDENTITY (1, 1) NOT NULL,
    [AccountName]    VARCHAR (200)   NOT NULL,
    [SqlProfileName] VARCHAR (100)   NOT NULL,
    [UserName]       VARCHAR (100)   NOT NULL,
    [PIN]            VARBINARY (128) NOT NULL,
    [SmtpServer]     VARCHAR (100)   NULL,
    [Port]           INT             NULL,
    [Active]         BIT             NOT NULL,
    [CreatedBy]      VARCHAR (20)    NOT NULL,
    [DateCreated]    DATETIME        NOT NULL,
    [DateUpdated]    DATETIME        NULL,
    [UpdatedBy]      VARCHAR (20)    NULL
);

