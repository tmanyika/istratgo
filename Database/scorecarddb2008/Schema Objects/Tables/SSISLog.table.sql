﻿CREATE TABLE [imd_user_db].[SSISLog] (
    [ErrorLogId]   INT            IDENTITY (1, 1) NOT NULL,
    [ErrorCode]    INT            NULL,
    [ErrorColumn]  VARCHAR (256)  NULL,
    [ErrorDetails] XML            NULL,
    [ErrorDesc]    NVARCHAR (256) NULL,
    [ErrorStep]    NVARCHAR (256) NULL,
    [ErrorTask]    NVARCHAR (256) NULL,
    [PackageTime]  SMALLDATETIME  NULL,
    [PackageName]  NVARCHAR (256) NULL,
    [UserName]     NVARCHAR (128) NULL
);

