﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [imd_user_db].[fxDecrypt]
(
	@strVal varchar(128)
)
RETURNS varchar(128)
AS
BEGIN
	declare @decValue varchar(128)
	set @decValue=convert(varchar(128), DecryptByKey(@strVal))
	return @decValue
END
