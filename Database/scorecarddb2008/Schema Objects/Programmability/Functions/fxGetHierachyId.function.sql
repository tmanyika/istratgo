﻿CREATE FUNCTION imd_user_db.fxGetHierachyId 
(	
	@parentId int
)
RETURNS TABLE 
AS
RETURN 
(
	WITH MyCTE
		AS (
			SELECT menu_id
			FROM app_menu
			WHERE parent_menu_id = @parentId
			UNION ALL
			SELECT app_menu.menu_id
			FROM app_menu
			INNER JOIN MyCTE ON app_menu.parent_menu_id = MyCTE.menu_id
			WHERE app_menu.parent_menu_id <> @parentId)
	SELECT menu_id FROM MyCTE			
)
