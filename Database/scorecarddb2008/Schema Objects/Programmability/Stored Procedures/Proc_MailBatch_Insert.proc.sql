﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_MailBatch_Insert]
	@accountMailId int, 
	@batchName varchar(100),
	@mailSubject varchar(800),
    @mailContent nvarchar(max), 
    @mailId int, 
    @noOfUsers int = 0, 
    @dateToBeSent datetime, 
    @sendNow bit,
    @Xml xml, 
    @createdBy varchar(20)
AS
begin
	set nocount on;
	set transaction isolation level read uncommitted;
	
	declare @mainTemplate nvarchar(max), @sent int;
	declare @todayDate datetime, @batchId int, @subject varchar(100);
	
	set @sent = 0;
	set @todayDate = getdate();
	
	select nref.value('Id[1]', 'int') as roleId
	into #Rxml
	from  @Xml.nodes('//Role/Data') AS R(nref)
	
	select @mainTemplate=MailContent, @subject=MailSubject from imd_user_db.MailTemplate where MailId = @mailId
	
	begin try
		begin transaction		
			insert into imd_user_db.MailBatch(AccountMailId, BatchName, MailSubject, MailContent, MainTemplate, NoOfUsers, IsSent, DateToBeSent, CreatedBy, DateCreated)
			values(@accountMailId, @batchName, replace(@subject,'{0}',@mailSubject), @mailContent, @mainTemplate, @noOfUsers, 0, @dateToBeSent, @createdBy, @todayDate)
			select @batchId = scope_identity()
			
			insert into imd_user_db.MailBatchUserRole(BatchId, RoleId)
			select @batchId, roleId from #Rxml
			
			set @sent = 1			
		commit transaction
	end try
	begin catch
		rollback transaction
		set @sent = 0
	end catch	
	
	--if (@sendNow = 1)
	--	begin
	--		declare @fullName varchar(50), @email varchar(50), @emlSubject varchar(800), @emailBody nvarchar(max);
	--		declare @sqlProfile varchar(100), @i int, @rec int;
			
	--		begin try
	--			select l.UserName, e.FirstName + ' ' + isnull(e.MiddleName,'') + ' ' + e.LastName as fullName, e.EmailAddress 
	--			into #UList
	--			from imd_user_db.Employee as e inner join imd_user_db.EmployeeLogin as l on e.EmployeeId = l.EmployeeId 
	--			where l.RoleId in (select roleId from #Rxml)
				
	--			select row_number() over (order by u.emailAddress asc) as rId, s.SqlProfileName, m.MailSubject, replace(replace(MainTemplate,'{0}',u.fullName),'{1}',MailContent) as mailBody, u.emailAddress  
	--			into #MailList
	--			from imd_user_db.MailBatch as m 
	--				inner join imd_user_db.MailServer as s on m.AccountMailId = s.AccountMailId 
	--				cross join	#UList as u 
	--			where m.BatchId = @batchId
				
	--			set @i = 1;
	--			select @rec=count(emailAddress) from #UList;
				
	--			while (@i <= @rec)
	--				begin
	--					select @sqlProfile=SqlProfileName, @email=emailAddress, @emailBody=mailBody, @emlSubject=MailSubject from #MailList where rId = @i
	--					exec msdb.dbo.sp_send_dbmail 
	--									@profile_name=@sqlProfile,
	--									@importance = 'NORMAL',
	--									@recipients = @email,
	--									@subject = @emlSubject,
	--									@body = @emailBody,
	--									@body_format = 'HTML'
	--					set @i = @i + 1;
	--				end	
				
	--			begin transaction
	--				update imd_user_db.MailBatch
	--				set NoOfUsers = @rec,
	--					IsSent = 1
	--				where BatchId = @batchId
	--				insert into imd_user_db.MailBatchUserList(BatchId,Email,LoginId)
	--				select @batchId,UserName,EmailAddress from #UList
	--			commit transaction
					
	--			drop table #UList
	--			drop table #MailList
	--		end try
	--		begin catch
	--			if(@@TRANCOUNT > 1)
	--				begin
	--					set @sent = 0
	--					rollback transaction
	--				end
	--		end catch
	--	end
	select @batchId --@sent
end
