﻿
CREATE PROCEDURE [imd_user_db].[PROC_ADD_COMPANY]

            @ACCOUNT_NO INT 
           ,@COMPANY_NAME VARCHAR(150)
           ,@REGISTRATION_NO NCHAR(20)
           ,@INDUSTRY INT
           ,@COUNTRY INT
           ,@VAT_NO NCHAR(20)
           ,@ADDRESS VARCHAR(150)
           ,@CONTACT_NO NCHAR(20)

AS

INSERT INTO COMPANY_DETAIL
           ([ACCOUNT_NO]
           ,[COMPANY_NAME]
           ,[REGISTRATION_NO]
           ,[INDUSTRY]
           ,[COUNTRY]
           ,[VAT_NO]
           ,[ADDRESS]
           ,[CONTACT_NO]
           )
     VALUES
           (
            @ACCOUNT_NO
           ,@COMPANY_NAME
           ,@REGISTRATION_NO
           ,@INDUSTRY
           ,@COUNTRY
           ,@VAT_NO
           ,@ADDRESS
           ,@CONTACT_NO
             )

