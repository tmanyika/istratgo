﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_LeaveStatus_GetActive]
	@active bit
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	select   StatusId,  StatusName,  Active, CreatedBy, DateCreated, DateUpdated, UpdatedBy
	from     imd_user_db.LeaveStatus
	where     (Active = @active)
	order by StatusName
END

