﻿
CREATE PROCEDURE [imd_user_db].[PROC_GET_ACCESS_BY_COMPANY_ID] 
	@COMPANY_ID INT 
as
begin
set nocount on;
set transaction isolation level read uncommitted;

declare @date datetime;
set @date = getdate(); 
select distinct
	MENU_ACCESS.ID, 
	MENU_ACCESS.MENU_ID,
	MENU_ACCESS.USER_TYPE_ID, 
	MENU_ACCESS.COMPANY_ID, 
	isnull(MENU_ACCESS.DATE_CREATED,@date) as DATE_CREATED, 
	isnull(APP_MENU.MENU_NAME,'') as MENU_NAME, 
	isnull(APP_MENU.IMAGE_ICON,'') as IMAGE_ICON, 
	APP_MENU.MENU_PATH, 
	isnull(APP_MENU.ORDER_ID,0) as ORDER_ID, 
	isnull(APP_MENU.POSITION,0) as position,
	isnull(APP_MENU.ACTIVE, 0) as ACTIVE
from  APP_MENU with(nolock) INNER JOIN
      MENU_ACCESS with(nolock) ON APP_MENU.MENU_ID = MENU_ACCESS.MENU_ID
where COMPANY_ID=@COMPANY_ID 
order by isnull(APP_MENU.POSITION,0)
end
