﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_LeaveType_Insert]
	@companyId int, 
	@leaveName varchar(150), 
	@leaveDescription varchar(300),
	@active bit, 
	@createdBy varchar(20)
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	insert into imd_user_db.LeaveType(CompanyId, LeaveName, LeaveDescription, Active, CreatedBy, DateCreated)
	values(@companyId, @leaveName, @leaveDescription, @active, @createdBy, getdate())
		
	select @@rowcount
END
