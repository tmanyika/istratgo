﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_MailBatch_GetById]
	@batchId int
AS
begin
	set nocount on;
	set transaction isolation level read uncommitted;
	
	select    b.BatchName, b.BatchId, b.AccountMailId, b.MailSubject, b.MailContent, b.MainTemplate, b.NoOfUsers, b.IsSent, b.DateToBeSent, b.CreatedBy, b.DateCreated, s.AccountName, 
                      s.SqlProfileName, s.UserName, s.PIN, s.SmtpServer, s.Port, s.Active, s.DateUpdated, s.UpdatedBy
	from         imd_user_db.MailBatch as b INNER JOIN
                      imd_user_db.MailServer as s on b.AccountMailId = s.AccountMailId
	where b.BatchId = @batchId
END
