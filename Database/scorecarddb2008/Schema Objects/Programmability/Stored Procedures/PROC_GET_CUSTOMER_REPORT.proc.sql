﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[PROC_GET_CUSTOMER_REPORT] 

AS
BEGIN
	set nocount on
	set transaction isolation level read uncommitted;
	select  [Company Name], Industry, [Registration No], [First Name], Surname, [Email Address], [Cellphone Number], [Account No], [Date Registered], [Last Login Time]
	from  imd_user_db.vw_Customers
END
