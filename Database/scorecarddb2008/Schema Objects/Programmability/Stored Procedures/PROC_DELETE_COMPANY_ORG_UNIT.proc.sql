﻿

CREATE PROCEDURE [imd_user_db].[PROC_DELETE_COMPANY_ORG_UNIT]
	@ID INT,
	@UPDATED_BY varchar(20),
	@ACTIVE bit
AS
begin
set nocount on;
set transaction isolation level read uncommitted;

update [imd_user_db].[COMPANY_STRUCTURE]
set DATE_UPDATED = getdate(),
	ACTIVE = @ACTIVE,
	UPDATED_BY = @UPDATED_BY
where ID = @ID 
select @@rowcount
end
