﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[PROC_ADD_PERSPECTIVE]
	@COMPANY_ID int,
	@PERSPECTIVE_NAME varchar(200),
	@PERSPECTIVE_DESC varchar(400),
	@CREATED_BY varchar(20)
AS
BEGIN
	SET NOCOUNT ON;
	INSERT INTO COMPANY_PERSPECTIVE
                      (COMPANY_ID, PERSPECTIVE_NAME, PERSPECTIVE_DESC,  ACTIVE, CREATED_BY, DATE_CREATED, DATE_UPDATED, UPDATE_BY)
	VALUES     (@COMPANY_ID, @PERSPECTIVE_NAME, @PERSPECTIVE_DESC, 1, @CREATED_BY, getdate(), null, null)
	SELECT @@rowcount
END

