﻿CREATE PROCEDURE [imd_user_db].[PROC_ADD_ATTACHMENTS] 
 @ATTACHMENT_NAME VARCHAR(450),
 @SUBMISSION_ID INT 
AS

INSERT ATTACHMENTS
(
 ATTACHMENT_NAME,
 SUBMISSION_ID
)
VALUES
(
 @ATTACHMENT_NAME,
 @SUBMISSION_ID
)