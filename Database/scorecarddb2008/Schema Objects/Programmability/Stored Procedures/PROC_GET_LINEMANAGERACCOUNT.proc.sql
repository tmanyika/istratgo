﻿

CREATE PROCEDURE [imd_user_db].[PROC_GET_LINEMANAGERACCOUNT] 
	@areaId int,
	@areaValueId int
AS
begin
	set transaction isolation level read uncommitted;
    set nocount on;
    declare @lineMgrId int;
    set @lineMgrId = 0;
    
	if(@areaId = 32) -- roles
		select @lineMgrId=isnull(LineManagerEmployeeId,0) from imd_user_db.Employee where EmployeeId = @areaValueId
	else if (@areaId = 33) -- Organisational Unit
		select @lineMgrId=isnull(LineManagerEmployeeId,0) from imd_user_db.Employee where EmployeeId = (select top 1 OWNER_ID from imd_user_db.COMPANY_STRUCTURE where ID = @areaValueId)
	else if(@areaId = 49) -- Project
		select @lineMgrId=isnull(LineManagerEmployeeId,0) from imd_user_db.Employee where EmployeeId = (select top 1 RESPONSIBLE_PERSON_ID from imd_user_db.COMPANY_PROJECT where PROJECT_ID = @areaValueId)
	
	select     ua.EmployeeId as ID, l.UserName as LOGIN_ID, l.Pass as [PASSWORD], ua.OrgUnitId as ORG_ID, ua.JobTitleId as JOB_TITLE_ID, ua.FirstName + ' ' + isnull(ua.MiddleName,'') + ' ' + ua.LastName as NAME, ua.EmailAddress as EMAIL_ADDRESS, l.ACTIVE, ua.LineManagerEmployeeId as REPORTS_TO, l.RoleId as USER_TYPE, cs.ORGUNIT_NAME
	from        imd_user_db.Employee AS ua inner join
				imd_user_db.EmployeeLogin as l on ua.EmployeeId = l.EmployeeId left outer join
				imd_user_db.COMPANY_STRUCTURE AS cs ON ua.OrgUnitId = cs.ID
	where     (ua.EmployeeId = @lineMgrId)
END



