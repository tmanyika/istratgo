﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_LeaveApplication_GetByStatusId]
	@statusId int
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	select     o.ORGUNIT_NAME AS CompanyName, a.ApplicationId, a.EmployeeId, a.LeaveId, a.RejectReason, a.LeaveNotes, a.StatusId, s.StatusName, j.NAME AS JobTitle, 
                      s.Active, s.CreatedBy, s.UpdatedBy, l.LeaveName, a.StartDate, a.EndDate, a.NoOfLeaveDays, a.SubmittedEmployeeId, a.ApprovedByEmployeeId, a.DateCreated, 
                      a.DateUpdated, e.LineManagerEmployeeId, e.OrgUnitId, e.FirstName, e.MiddleName, e.LastName, e.EmailAddress, e.JobTitleId, 
                      e.EmployeeNo, e.Cellphone, e.BusTelephone, l.CompanyId, l.LeaveDescription
	from         imd_user_db.LeaveApplication AS a INNER JOIN
                 imd_user_db.Employee AS e ON a.EmployeeId = e.EmployeeId INNER JOIN
                      imd_user_db.COMPANY_STRUCTURE AS o ON e.OrgUnitId = o.ID INNER JOIN
                      imd_user_db.COMPANY_JOB_TITLES AS j ON e.JobTitleId = j.ID INNER JOIN
                      imd_user_db.LeaveType AS l ON a.LeaveId = l.LeaveId INNER JOIN
                      imd_user_db.LeaveStatus AS s ON a.StatusId = s.StatusId
	where     (a.StatusId = @statusId)
END
