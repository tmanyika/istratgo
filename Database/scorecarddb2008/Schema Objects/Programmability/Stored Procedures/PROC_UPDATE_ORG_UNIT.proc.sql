﻿

CREATE PROCEDURE [imd_user_db].[PROC_UPDATE_ORG_UNIT]   
            @ORG_UNIT_ID INT            
           ,@ORGUNIT_NAME varchar(50)
           ,@PARENT_ORG int 
           ,@ORGUNT_TYPE int
           ,@OWNER_ID int
           ,@UPDATED_BY varchar(20)
AS
begin
set nocount on;
set transaction isolation level read uncommitted;

update [imd_user_db].[COMPANY_STRUCTURE]
set ORGUNIT_NAME=@ORGUNIT_NAME, 
	ORGUNT_TYPE=@ORGUNT_TYPE, 
	OWNER_ID=@OWNER_ID,
	DATE_UPDATED = getdate(),
	UPDATED_BY = @UPDATED_BY
where ID = @ORG_UNIT_ID
select @@rowcount
end


