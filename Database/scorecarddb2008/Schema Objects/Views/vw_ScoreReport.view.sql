﻿CREATE VIEW imd_user_db.vw_ScoreReport
AS
SELECT     cp.PERSPECTIVE_ID, cp.PERSPECTIVE_NAME, cs.OBJECTIVE, ig.UNIT_MEASURE, ig.TARGET, ig.WEIGHT, ig.DATE_CREATED, ids.SCORES, ids.FINAL_SCORE, 
                      ids.DATE_CREATED AS SCORE_DATE_CREATED, ids.CRITERIA_TYPE_ID, ids.SELECTED_ITEM_VALUE
FROM         imd_user_db.INDIVIDUAL_GOALS AS ig WITH (nolock) INNER JOIN
                      imd_user_db.COMPANY_STRATEGIC AS cs WITH (nolock) ON ig.STRATEGIC_ID = cs.ID INNER JOIN
                      imd_user_db.INDIVIDUAL_SCORES AS ids WITH (nolock) ON ig.ID = ids.GOAL_ID INNER JOIN
                      imd_user_db.COMPANY_PERSPECTIVE AS cp WITH (nolock) ON cs.PERSPECTIVE = cp.PERSPECTIVE_ID
WHERE     (ids.CRITERIA_TYPE_ID = 33)

GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[26] 4[19] 2[13] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ig"
            Begin Extent = 
               Top = 6
               Left = 470
               Bottom = 219
               Right = 660
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cs"
            Begin Extent = 
               Top = 6
               Left = 265
               Bottom = 159
               Right = 432
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ids"
            Begin Extent = 
               Top = 6
               Left = 698
               Bottom = 209
               Right = 903
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cp"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 215
               Right = 227
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 13
         Width = 284
         Width = 1500
         Width = 2025
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 2715
         Width = 1500
         Width = 1935
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 2835
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'VIEW', @level1name = N'vw_ScoreReport';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'VIEW', @level1name = N'vw_ScoreReport';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'VIEW', @level1name = N'vw_ScoreReport';

