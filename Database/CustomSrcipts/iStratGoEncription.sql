use iStratGo;

set nocount on;
set transaction isolation level read uncommitted;

-- Create Master Key ---------------- see tutorial on http://msdn.microsoft.com/en-us/library/ms179331.aspx--

if not exists (select * from sys.symmetric_keys where name = N'##MS_DatabaseMasterKey##')
	create master key encryption by password = '3891@Macd'
go

select * from sys.symmetric_keys

-- End of Create Master Key ----------------

-- Create Certificate ----------

if not exists (select * from sys.certificates where name = N'FieldCertificate')
begin
	open master key decryption by password = '3891@Macd'
	create certificate FieldCertificate with subject = 'Encrypted Fields'
end

go

select * from sys.certificates

------ end of create certificate -----------

-- Create Symmetric Key ----

create symmetric key FieldSymmetricKey with algorithm = TRIPLE_DES
encryption by certificate FieldCertificate;

-- End of Create Symmetric Key ----
--**************************************************--
-- Examples of Testing Encryption --
declare @strVal varbinary(128);

open master key decryption by password = '3891@Macd'
open symmetric key FieldSymmetricKey
decryption by certificate FieldCertificate;
select @strVal=EncryptByKey(Key_GUID('FieldSymmetricKey'),'Torrence');

-- Testing Decryption
open master key decryption by password = '3891@Macd'
open symmetric key FieldSymmetricKey
decryption by certificate FieldCertificate;
select @strVal as encVal, convert(varchar, DecryptByKey(@strVal)) as decVal

go