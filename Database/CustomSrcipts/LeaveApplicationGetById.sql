-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE imd_user_db.Proc_LeaveApplication_GetById
	@ApplicationId int
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	declare @recAffected int;
	
	set @recAffected = 0;
	begin try
		begin transaction	
			update imd_user_db.LeaveApplication
			set StatusId = @StatusId, 
				DateUpdated = getdate(),
				ApprovedByEmployeeId = @ApprovedByEmployeeId
			where ApplicationId = @ApplicationId
			select @recAffected=@@ROWCOUNT
			
			if(@Comments <> '')
				insert into imd_user_db.LeaveApplicationComment(ApplicationId, Comment, SupportingDoc, Valid, UploadedBy, DateCreated)
				values  (@ApplicationId, @Comments, @SupportingDoc, 1, @PostedBy, getdate())
		commit transaction
	end try
	begin catch
		if(@@TRANCOUNT > 0)
			rollback transaction
	end catch
	select @recAffected
END
GO
