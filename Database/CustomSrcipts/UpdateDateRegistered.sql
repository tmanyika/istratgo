set nocount on;
set transaction isolation level read uncommitted;

update imd_user_db.COMPANY_STRUCTURE
set DATE_REGISTERED = case when id % 2 = 0 then dateadd(dd,-30,getdate())
							when id % 2 = 0 then dateadd(dd,-60,getdate())
							else dateadd(dd,-91,getdate()) end

select * from imd_user_db.COMPANY_STRUCTURE
