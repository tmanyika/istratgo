--EXEC sp_configure filestream_access_level, 2

--ALTER DATABASE ScoreCard

--ADD FILEGROUP FileStreamIstratgo

--  CONTAINS FILESTREAM

--GO

--ALTER DATABASE ScoreCard

--ADD FILE (NAME='FileStreamIstratgo', FILENAME='D:\Mydata\Projects\databases\FileStream')
--TO FILEGROUP FileStreamIstratgo 
--GO

--USE ScoreCard

--ALTER Table imd_user_db.TrainingHistory

--SET (filestream_on=FileStreamIstratgo)

--GO

--USE ScoreCard

--ALTER Table imd_user_db.TrainingHistory

--ADD ProofDocument varbinary(max) FILESTREAM null

--GO