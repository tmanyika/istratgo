﻿using System;
using System.Data;
using System.Collections.Generic;
using scorecard.interfaces;
using scorecard.entities;

namespace scorecard.implementations
{
    public class ScoreRatingBL : IScoreRating
    {
        public ScoreRatingDAL Dal
        {
            get { return new ScoreRatingDAL(Util.GetconnectionString()); }
        }

        public bool AddRating(ScoreRating obj)
        {
            int result = Dal.Save(obj);
            return (result > 0) ? true : false;
        }

        public bool UpdateRating(ScoreRating obj)
        {
            int result = Dal.Update(obj);
            return (result > 0) ? true : false;
        }

        public bool DeactivateRating(ScoreRating obj)
        {
            int result = Dal.Deactivate(obj);
            return (result > 0) ? true : false;
        }

        public decimal GetCompanyRating(int companyId, bool active)
        {
            decimal ratingVal = Dal.GetMax(companyId, active);
            return ratingVal;
        }

        public ScoreRating GetById(int ratingId)
        {
            using (IDataReader data = Dal.Get(ratingId))
            {
                if (data.Read())
                    return GetRow(data);
            }
            return new ScoreRating();
        }

        public ScoreRating GetRow(IDataRecord rw)
        {
            DateTime? defDate = null;
            decimal? defDec = null;
            return new ScoreRating
            {
                Active = bool.Parse(rw["Active"].ToString()),
                CompanyId = int.Parse(rw["CompanyId"].ToString()),
                CreatedBy = rw["CreatedBy"].ToString(),
                DateCreated = DateTime.Parse(rw["DateCreated"].ToString()),
                DateUpdated = (rw["DateUpdated"] != DBNull.Value) ? DateTime.Parse(rw["DateUpdated"].ToString()) : defDate,
                MinimumValue = (rw["MinimumValue"] != DBNull.Value) ? decimal.Parse(rw["MinimumValue"].ToString()) : defDec,
                MaximumValue = (rw["MaximumValue"] != DBNull.Value) ? decimal.Parse(rw["MaximumValue"].ToString()) : defDec,
                RatingDescription = rw["RatingDescription"].ToString(),
                RatingId = int.Parse(rw["RatingId"].ToString()),
                RatingValue = Math.Round(decimal.Parse(rw["RatingValue"].ToString()), 1),
                UpdatedBy = (rw["UpdatedBy"] != DBNull.Value) ? rw["UpdatedBy"].ToString() : string.Empty,
            };
        }

        public List<ScoreRating> GetAll(int companyId)
        {
            List<ScoreRating> obj = new List<ScoreRating>();
            using (IDataReader data = Dal.GetAll(companyId))
            {
                while (data.Read())
                    obj.Add(GetRow(data));
            }
            return obj;
        }

        public List<ScoreRating> GetByStatus(int companyId, bool active)
        {
            List<ScoreRating> obj = new List<ScoreRating>();
            using (IDataReader data = Dal.Get(companyId, active))
            {
                while (data.Read())
                    obj.Add(GetRow(data));
            }
            return obj;
        }
    }
}