﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace scorecard.entities
{
    public class ScoreRating
    {
        public int RatingId { get; set; }
        public int CompanyId { get; set; }

        public decimal RatingValue { get; set; }
        public decimal? MinimumValue { get; set; }
        public decimal? MaximumValue { get; set; }

        public string RatingDescription { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public bool Active { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
    }
}