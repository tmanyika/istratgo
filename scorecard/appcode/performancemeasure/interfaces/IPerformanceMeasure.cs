﻿using System;
using System.Data;
using Scorecard.Entities;
using System.Collections.Generic;

namespace Scorecard.Interfaces
{
    public interface IPerformanceMeasure
    {
        PerformanceMeasure GetBriefRow(IDataRecord rw);
        List<PerformanceMeasure> GetByAreaValueId(int areaId, int areaValueId, bool active);
        List<PerformanceMeasure> GetByAreaEmployeeId(int areaId, int employeeId, bool active);
    }
}
