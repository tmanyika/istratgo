﻿using System;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

namespace Scorecard.DataAccess
{
    public class PerformanceMeasureDAL
    {
        #region Variables

        private string _conString;

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return _conString; }
            set { _conString = value; }
        }

        #endregion

        #region Default Constructors

        public PerformanceMeasureDAL(string conString)
        {
            _conString = conString;
        }

        #endregion

        #region Public Methods and Functions

        public IDataReader Get(int areaId, int areaValueId, bool active)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_PerformanceMeasure_GetByAreaIdAndValueId", areaId, areaValueId, active);
        }

        public IDataReader Get(bool active, int areaId, int employeeId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_PerformanceMeasure_GetByAreaIdAndEmployeeId", areaId, employeeId, active);
        }

        #endregion
    }
}