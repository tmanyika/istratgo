﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Scorecard.Entities
{
    public class PerformanceMeasure
    {
        public long MeasureId { get; set; }
        public int? ParentId { get; set; }
        public int StrategicId { get; set; }
        public int AreaId { get; set; }
        public int AreaValueId { get; set; }

        public int? UnitOfMeasureId { get; set; }
        public int Weight { get; set; }
        public int? EnvironmentContextId { get; set; }

        public string PerformanceMeasureName { get; set; }
        public string Target { get; set; }
        public string Rationale { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public bool HasSubGoals { get; set; }
        public bool Active { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
    }
}