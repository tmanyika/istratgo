﻿using System;
using System.Collections.Generic;
using System.Linq;
using Scorecard.Interfaces;
using scorecard.implementations;
using Scorecard.Entities;
using System.Data;
using Scorecard.DataAccess;

namespace Scorecard.Logic
{
    public class PerformanceMeasureBL : IPerformanceMeasure
    {
        #region Properties

        public string ConnectionString
        {
            get { return Util.GetconnectionString(); }
        }


        public PerformanceMeasureDAL Dal
        {
            get { return new PerformanceMeasureDAL(ConnectionString); }
        }

        #endregion

        #region Methods & Functions

        public List<PerformanceMeasure> GetByAreaEmployeeId(int areaId, int employeeId, bool active)
        {
            List<PerformanceMeasure> pList = new List<PerformanceMeasure>();
            using (var data = Dal.Get(active, areaId, employeeId))
            {
                while (data.Read())
                {
                    pList.Add(GetRow(data));
                }
            }
            return pList;
        }

        public List<PerformanceMeasure> GetByAreaValueId(int areaId, int areaValueId, bool active)
        {
            List<PerformanceMeasure> pList = new List<PerformanceMeasure>();
            using (var data = Dal.Get(areaId, areaValueId, active))
            {
                while (data.Read())
                {
                    pList.Add(GetRow(data));
                }
            }
            return pList;
        }

        public PerformanceMeasure GetRow(IDataRecord rw)
        {
            return new PerformanceMeasure
            {
                MeasureId = long.Parse(rw["MeasureId"].ToString()),
                PerformanceMeasureName = (rw["PerformanceMeasureName"] != DBNull.Value) ? rw["PerformanceMeasureName"].ToString() : string.Empty
            };
        }

        public PerformanceMeasure GetBriefRow(IDataRecord rw)
        {
            return new PerformanceMeasure
            {
                MeasureId = (rw["MeasureId"] != DBNull.Value) ? long.Parse(rw["MeasureId"].ToString()) : 0,
                PerformanceMeasureName = (rw["PerformanceMeasureName"] != DBNull.Value) ? rw["PerformanceMeasureName"].ToString() : string.Empty,
            };
        }

        #endregion
    }
}