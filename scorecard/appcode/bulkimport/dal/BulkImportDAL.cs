﻿using System;
using System.Collections.Generic;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using System.Text;

namespace Bulk.Import
{
    public class BulkImportDAL
    {
        #region Variables

        private string _conString;

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return _conString; }
            set { _conString = value; }
        }

        #endregion

        #region Default Constructors

        public BulkImportDAL(string conString)
        {
            _conString = conString;
        }

        #endregion

        #region Methods & Functions

        /// <summary>
        /// Executes a bulk update of all uploaded company data
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="createdBy"></param>
        public void SaveAll(int companyId, string createdBy)
        {
            SqlHelper.ExecuteNonQuery(ConnectionString, "Proc_BulkImport_InsertMain", companyId, createdBy);
        }

        public int SaveJobTitles(int companyId, string createdBy)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_BulkImport_JobTitles", companyId, createdBy);
            return (result == DBNull.Value) ? 0 : int.Parse(result.ToString());
        }

        public int SaveProjects(int companyId, string createdBy)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_BulkImport_Projects", companyId, createdBy);
            return (result == DBNull.Value) ? 0 : int.Parse(result.ToString());
        }

        public int SaveOrgUnits(int companyId, string createdBy)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_BulkImport_OrgUnits", companyId, createdBy);
            return (result == DBNull.Value) ? 0 : int.Parse(result.ToString());
        }

        public int SaveEmployees(int companyId, string createdBy)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_BulkImport_Employees", companyId, createdBy);
            return (result == DBNull.Value) ? 0 : int.Parse(result.ToString());
        }

        public int LinkData(int companyId, string createdBy)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_BulkImport_LinkInformation", companyId, createdBy);
            return (result == DBNull.Value) ? 0 : int.Parse(result.ToString());
        }

        public int SavePerformanceMeasures(int companyId, string createdBy, out string outcome)
        {
            outcome = string.Empty;
            StringBuilder strB = new StringBuilder();
            DataSet dS = SqlHelper.ExecuteDataset(ConnectionString, "Proc_BulkImport_PerformanceMeasures", companyId, createdBy);
            if (dS.Tables.Count > 0)
            {
                int lastTableIdx = dS.Tables.Count - 1;
                object resalt = dS.Tables[lastTableIdx].Rows[0][0];
                outcome = (resalt == DBNull.Value) ? string.Empty : resalt.ToString();
            }
            int result = string.IsNullOrEmpty(outcome) ? 0 : 1;
            return result;
        }

        public int CleanupData(int companyId)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_BulkImport_CleanupCompanyData", companyId);
            return (result == DBNull.Value) ? 0 : int.Parse(result.ToString());
        }

        public DataTable GetValidationResultsOfObjectives(int companyId)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_BulkImport_ValidateObjectives", companyId).Tables[0];
        }

        #endregion
    }
}