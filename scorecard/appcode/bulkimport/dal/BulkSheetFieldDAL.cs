﻿using System;
using System.Collections.Generic;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

namespace Bulk.Import
{
    public class BulkSheetFieldDAL
    {
        #region Variables

        private string _conString;

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return _conString; }
            set { _conString = value; }
        }

        #endregion

        #region Default Constructors

        public BulkSheetFieldDAL(string conString)
        {
            _conString = conString;
        }

        #endregion

        #region Methods & Functions

        public IDataReader Get(int fieldId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_BulkSheetField_GetBySheetId", fieldId);
        }

        public IDataReader GetSheetFields(int sheetId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_BulkSheetField_GetBySheetId", sheetId);
        }

        public IDataReader GetSheetFields(string sheetName, bool active)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_BulkSheetField_GetBySheetName", sheetName, active);
        }

        public IDataReader GetSheetFields(int sheetId, bool active)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_BulkSheetField_GetSheetActiveFields", sheetId, active);
        }

        public IDataReader GetSheetFields(bool active)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_BulkSheetField_GetAllFields", active);
        }

        public int Save(BulkSheetField obj)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_BulkSheetField_Insert", obj.SheetId, obj.SheetFieldName, obj.DbFieldName, obj.IsRequired,
                                                                            obj.Active, obj.FieldNotes, obj.DataType, obj.ExpectedValueList, obj.CreatedBy);
            return (result == DBNull.Value) ? 0 : int.Parse(result.ToString());
        }

        public int Update(BulkSheetField obj)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_BulkSheetField_Update", obj.FieldId, obj.SheetId, obj.SheetFieldName, obj.DbFieldName, obj.IsRequired,
                                                                            obj.Active, obj.FieldNotes, obj.DataType, obj.ExpectedValueList, obj.UpdatedBy);
            return (result == DBNull.Value) ? 0 : int.Parse(result.ToString());
        }

        public int UpdateStatus(BulkSheetField obj)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_BulkSheetField_SetStatus", obj.FieldId, obj.Active, obj.UpdatedBy);
            return (result == DBNull.Value) ? 0 : int.Parse(result.ToString());
        }

        #endregion
    }
}