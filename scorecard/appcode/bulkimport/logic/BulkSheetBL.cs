﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using scorecard.implementations;
using System.Text;
using System.Linq;
using vb = Microsoft.VisualBasic.Information;
using System.Text.RegularExpressions;

namespace Bulk.Import
{
    public class BulkSheetBL : IBulkSheet
    {
        #region Variables

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return Util.GetconnectionString(); }
        }

        public IBulkSheetField Ibsf
        {
            get
            {
                return new BulkSheetFieldBL();
            }
        }

        public BulkSheetDAL Dal
        {
            get { return new BulkSheetDAL(ConnectionString); }
        }

        #endregion

        public BulkSheetBL()
        {

        }

        #region Public Methods & Functions

        public BulkSheet GetRow(IDataRecord rw)
        {
            DateTime? defDate = null;
            return new BulkSheet
            {
                Active = bool.Parse(rw["Active"].ToString()),
                CreatedBy = rw["CreatedBy"].ToString(),
                DateCreated = DateTime.Parse(rw["DateCreated"].ToString()),
                DateUpdated = (rw["DateUpdated"] != DBNull.Value) ? DateTime.Parse(rw["DateUpdated"].ToString()) : defDate,
                DbTableName = (rw["DbTableName"] != DBNull.Value) ? rw["DbTableName"].ToString() : string.Empty,
                OrderNumber = Int16.Parse(rw["OrderNumber"].ToString()),
                RowIdx = int.Parse(rw["RowIdx"].ToString()),
                SheetDescription = (rw["SheetDescription"] != DBNull.Value) ? rw["SheetDescription"].ToString() : string.Empty,
                SheetId = int.Parse(rw["SheetId"].ToString()),
                SheetName = (rw["SheetName"] != DBNull.Value) ? rw["SheetName"].ToString() : string.Empty,
                SheetNotes = (rw["SheetNotes"] != DBNull.Value) ? rw["SheetNotes"].ToString() : string.Empty,
                UpdatedBy = (rw["UpdatedBy"] != DBNull.Value) ? rw["UpdatedBy"].ToString() : string.Empty
            };
        }

        public BulkSheet GetRowIncludingArea(IDataRecord rw)
        {
            DateTime? defDate = null;
            return new BulkSheet
            {
                Active = bool.Parse(rw["Active"].ToString()),
                Area = new BulkSheetArea
                {
                    AreaId = (rw["AreaId"] != DBNull.Value) ? int.Parse(rw["AreaId"].ToString()) : 0,
                    AreaName = (rw["AreaName"] != DBNull.Value) ? rw["AreaName"].ToString() : string.Empty
                },
                AreaId = (rw["AreaId"] != DBNull.Value) ? int.Parse(rw["AreaId"].ToString()) : 0,
                CreatedBy = rw["CreatedBy"].ToString(),
                DateCreated = DateTime.Parse(rw["DateCreated"].ToString()),
                DateUpdated = (rw["DateUpdated"] != DBNull.Value) ? DateTime.Parse(rw["DateUpdated"].ToString()) : defDate,
                DbTableName = (rw["DbTableName"] != DBNull.Value) ? rw["DbTableName"].ToString() : string.Empty,
                OrderNumber = Int16.Parse(rw["OrderNumber"].ToString()),
                RowIdx = int.Parse(rw["RowIdx"].ToString()),
                SheetDescription = (rw["SheetDescription"] != DBNull.Value) ? rw["SheetDescription"].ToString() : string.Empty,
                SheetId = int.Parse(rw["SheetId"].ToString()),
                SheetName = (rw["SheetName"] != DBNull.Value) ? rw["SheetName"].ToString() : string.Empty,
                SheetNotes = (rw["SheetNotes"] != DBNull.Value) ? rw["SheetNotes"].ToString() : string.Empty,
                UpdatedBy = (rw["UpdatedBy"] != DBNull.Value) ? rw["UpdatedBy"].ToString() : string.Empty
            };
        }

        public BulkSheet GetBriefRow(IDataRecord rw)
        {
            return new BulkSheet
            {
                DbTableName = (rw["DbTableName"] != DBNull.Value) ? rw["DbTableName"].ToString() : string.Empty,
                SheetId = (rw["SheetId"] != DBNull.Value) ? int.Parse(rw["SheetId"].ToString()) : 0,
                SheetName = (rw["SheetName"] != DBNull.Value) ? rw["SheetName"].ToString() : string.Empty,
            };
        }

        public BulkSheet GetSheet(int sheetId)
        {
            using (IDataReader data = Dal.Get(sheetId))
                return (data.Read()) ? GetRow(data) : new BulkSheet();
        }

        public List<BulkSheet> GetAll()
        {
            List<BulkSheet> obj = new List<BulkSheet>();
            using (IDataReader data = Dal.Get())
            {
                while (data.Read())
                    obj.Add(GetRow(data));
            }
            return obj;
        }

        public List<BulkSheet> GetByStatus(bool active)
        {
            List<BulkSheet> obj = new List<BulkSheet>();
            using (IDataReader data = Dal.Get(active))
            {
                while (data.Read())
                    obj.Add(GetRow(data));
            }
            return obj;
        }

        public List<BulkSheet> GetByAreaIdAndStatus(int areaId, bool active)
        {
            List<BulkSheet> obj = new List<BulkSheet>();
            using (IDataReader data = Dal.Get(areaId, active))
            {
                while (data.Read())
                    obj.Add(GetRowIncludingArea(data));
            }
            return obj;
        }

        public BulkSheet GetSheet(string sheetName)
        {
            using (IDataReader data = Dal.Get(sheetName))
                return (data.Read()) ? GetRow(data) : new BulkSheet();
        }

        public bool UpdateSheet(BulkSheet obj)
        {
            int result = Dal.Update(obj);
            return (result > 0);
        }

        public bool SetStatus(BulkSheet obj)
        {
            int result = Dal.UpdateStatus(obj);
            return (result > 0);
        }

        public bool AddSheet(BulkSheet obj)
        {
            int result = Dal.Save(obj);
            return (result > 0);
        }

        public bool SheetExist(List<BulkSheet> sheets, string sheetName, out string outcome)
        {
            var data = sheets.Where(p => p.SheetName.ToLower() == sheetName.ToLower()).FirstOrDefault();
            outcome = string.Empty;// string.Format("The sheet [{0}] you specified in your workbook is not part of expected collection that can be imported!", sheetName);
            return (data != null);
        }

        public bool ValidateSheet(DataTable dT, int sheetId, int sheetPos, out string outcome)
        {
            outcome = string.Empty;

            bool fieldnotavailable = false;
            bool rowremoved = false;

            var sheet = GetSheet(sheetId);
            if (sheet.SheetId <= 0)
            {
                outcome = string.Format("The sheet # [{0}] you specified in your workbook is not part of the expected collection that can be imported!", sheetPos);
                return false;
            }

            var sheetf = Ibsf.GetBySheetId(sheetId, true);
            string missingfieldHeader = string.Format("The following fields are missing in your [{0}] sheet:{1}", sheet.SheetName, Environment.NewLine);
            string rowremovedHeader = string.Format("The following row numbers have been removed as they are not valid in [{0}] sheet:{1}", sheet.SheetName, Environment.NewLine);

            StringBuilder strsB = new StringBuilder();
            StringBuilder strrB = new StringBuilder();

            foreach (DataColumn col in dT.Columns)
            {
                string colName = col.ColumnName;
                var data = sheetf.Where(p => p.SheetFieldName == colName).FirstOrDefault();
                if (data == null)
                {
                    fieldnotavailable = true;
                    strsB.Append(string.Format("{0}{1}", colName, Environment.NewLine));
                }
            }

            foreach (DataRow rw in dT.Rows)
            {
                foreach (DataColumn col in dT.Columns)
                {
                    string colName = col.ColumnName.Trim();
                    var dataf = sheetf.Where(p => p.SheetFieldName.Trim().ToLower() == colName.ToLower()).FirstOrDefault();
                    var coldata = rw[colName];
                    if (dataf.IsRequired && coldata == null)
                    {
                        rowremoved = true;
                        strrB.Append(string.Format("{0}{1}", colName, Environment.NewLine));
                        rw.Delete();
                    }
                }
            }

            dT.AcceptChanges();
            outcome = string.Format("{0}\r\n{1}", fieldnotavailable ? string.Format("{0}{1}", missingfieldHeader, strsB.ToString()) : string.Empty,
                                    rowremoved ? string.Format("{0}{1}", rowremovedHeader, strrB.ToString()) : string.Empty);

            return !fieldnotavailable;
        }

        public bool ValidateSheet(DataTable dT, string sheetName, out string outcome)
        {
            bool valid = true;
            bool rowremoved = false;

            var sheetf = Ibsf.GetBySheetName(sheetName, true);
            string missingfieldHeader = string.Format("The following fields are missing in your [{0}] sheet:{1}", sheetName, Environment.NewLine);
            string rowremovedHeader = string.Format("The following row numbers are not valid in [{0}] sheet:{1}", sheetName, Environment.NewLine);

            StringBuilder strsB = new StringBuilder();
            StringBuilder strrB = new StringBuilder();

            foreach (BulkSheetField fld in sheetf)
            {
                string colName = fld.SheetFieldName;
                if (!dT.Columns.Contains(colName) && fld.IsRequired)
                {
                    valid = false;
                    strsB.Append(string.Format("{0}{1}", colName, Environment.NewLine));
                }
            }

            DataTable dTc = dT.Clone();

            int rowNumber = 0;
            foreach (DataRow rw in dT.Rows)
            {
                rowNumber += 1;
                foreach (DataColumn col in dT.Columns)
                {
                    string colName = col.ColumnName;
                    var dataf = sheetf.Where(p => p.SheetFieldName == colName).FirstOrDefault();
                    var coldata = rw[colName];
                    if (dataf.IsRequired && (vb.IsNothing(coldata) || vb.IsDBNull(coldata)))
                    {
                        //valid = false;
                        rowremoved = true;
                        strrB.Append(string.Format("{0} - {1}{2}", rowNumber, colName, Environment.NewLine));
                        //rw.Delete();
                        break;
                    }
                    else
                    {
                        if (dataf.IsRequired && (!(vb.IsNothing(coldata) || vb.IsDBNull(coldata))) &&
                                !string.IsNullOrEmpty(dataf.RegExpression))
                        {
                            if (coldata is string)
                            {
                                Regex regex = new Regex(dataf.RegExpression, RegexOptions.IgnoreCase);
                                if (regex.IsMatch(coldata.ToString()))
                                {
                                    dTc.ImportRow(rw);
                                }
                                else
                                {
                                    rowremoved = false;
                                    strrB.Append(string.Format("{0} - {1}{2}", rowNumber, colName, Environment.NewLine));
                                }
                            }
                        }
                        else dTc.ImportRow(rw);
                    }
                }
            }

            dTc.AcceptChanges();
            dT = null;//discard original contents
            dT = dTc.Copy();
            outcome = string.Format("{0}\r\n{1}", !valid ? string.Format("{0}{1}", missingfieldHeader, strsB.ToString()) : string.Empty,
                                    rowremoved ? string.Format("{0}{1}", rowremovedHeader, strrB.ToString()) : string.Empty);

            return valid;
        }

        public bool ValidateSheets(DataSet dS, List<BulkSheet> sheetList, out string outcome)
        {
            int validCount = 0;
            StringBuilder strB = new StringBuilder();
            DataSet dsN = new DataSet();

            foreach (DataTable tabCol in dS.Tables)
            {
                string sheetName = tabCol.TableName;
                bool sheetExist = SheetExist(sheetList, sheetName, out outcome);
                if (sheetExist)
                {
                    var dT = dS.Tables[sheetName];
                    string msg = string.Empty;
                    bool valid = ValidateSheet(dT, sheetName, out msg);
                    if (!string.IsNullOrEmpty(msg))
                        strB.Append(string.Format("{0}{1}", outcome, Environment.NewLine));
                    if (valid)
                    {
                        dsN.Tables.Add(dT.Copy());
                        validCount += 1;
                    }
                }
            }
            dsN.AcceptChanges();
            dS = null;
            dS = dsN.Copy();
            outcome = strB.ToString();
            return (validCount > 0);
        }

        #endregion
    }
}