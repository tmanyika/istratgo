﻿using System;
using System.Data;

namespace Bulk.Import
{
    public class BulkSheetField
    {
        public int FieldId { get; set; }
        public int SheetId { get; set; }
        public int DataType { get; set; }

        public string ExpectedValueList { get; set; }
        public string RegExpression { get; set; }
        public string SheetFieldName { get; set; }
        public string DbFieldName { get; set; }
        public string FieldNotes { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public bool Active { get; set; }
        public bool IsRequired { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }

        public BulkSheet Sheet { get; set; }
    }
}