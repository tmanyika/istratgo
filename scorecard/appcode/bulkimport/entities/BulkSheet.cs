﻿using System;
using System.Data;

namespace Bulk.Import
{
    public class BulkSheet
    {
        public int SheetId { get; set; }
        public int RowIdx { get; set; }
        public int AreaId { get; set; }

        public Int16 OrderNumber { get; set; }

        public string SheetName { get; set; }
        public string DbTableName { get; set; }
        public string SheetDescription { get; set; }
        public string SheetNotes { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public bool Active { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }

        public BulkSheetArea Area { get; set; }
    }
}