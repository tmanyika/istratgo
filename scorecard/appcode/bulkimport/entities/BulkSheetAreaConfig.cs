﻿using System;
using System.Data;
using System.Configuration;

namespace Bulk.Import
{
    public static class BulkSheetAreaConfig
    {
        public static int PerformanceManagementAreaId
        {
            get { return int.Parse(ConfigurationManager.AppSettings["PerformanceManagementAreaId"].ToString()); }
        }

        public static int ThreeSixtyDegreeEvaluationAreaId
        {
            get { return int.Parse(ConfigurationManager.AppSettings["ThreeSixtyDegreeEvaluationAreaId"].ToString()); }
        }
    }
}