﻿using System;
using System.Data;

namespace Bulk.Import
{
    public class BulkSheetArea
    {
        public int AreaId { get; set; }

        public bool Active { get; set; }

        public string AreaName { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
    }
}