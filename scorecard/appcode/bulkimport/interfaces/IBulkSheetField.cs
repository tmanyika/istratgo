﻿using System;
using System.Data;
using System.Collections.Generic;

namespace Bulk.Import
{
    public interface IBulkSheetField
    {
        BulkSheetField GetRow(IDataRecord rw);
        BulkSheetField GetField(int fieldId);

        List<BulkSheetField> GetBySheetId(int sheetId);
        List<BulkSheetField> GetBySheetId(int sheetId, bool status);
        List<BulkSheetField> GetBySheetName(string sheetName, bool active);
        List<BulkSheetField> GetAll(bool status);

        bool UpdateField(BulkSheetField obj);
        bool SetStatus(BulkSheetField obj);
        bool AddField(BulkSheetField obj);
    }
}
