﻿using System;
using System.Collections.Generic;
using System.Data;
using scorecard.implementations;

namespace HR.Human.Resources
{
    public class EmployeeStatusBL : IEmployeeStatus
    {
        #region Properties

        public string ConnectionString
        {
            get { return Util.GetconnectionString(); }
        }

        public EmployeeStatusDAL Dal
        {
            get { return new EmployeeStatusDAL(ConnectionString); }
        }

        #endregion

        #region Default Constructors

        public EmployeeStatusBL() { }

        #endregion

        #region Public Methods & Functions

        public bool AddEmployeeStatus(EmployeeStatus obj)
        {
            int result = Dal.Save(obj);
            return result > 0 ? true : false;
        }

        public bool UpdateEmployeeStatus(EmployeeStatus obj)
        {
            int result = Dal.Update(obj);
            return result > 0 ? true : false;
        }

        public bool Delete(int statusId, string updatedBy)
        {
            int result = Dal.Deactivate(statusId, updatedBy);
            return result > 0 ? true : false;
        }

        public List<EmployeeStatus> GetByStatus(bool active)
        {
            List<EmployeeStatus> obj = new List<EmployeeStatus>();
            DataTable dT = Dal.Get(active);

            if (dT.Rows.Count <= 0) return obj;
            foreach (DataRow rw in dT.Rows)
                obj.Add(GetRecord(rw));

            return obj;
        }

        public List<EmployeeStatus> GetAll()
        {
            List<EmployeeStatus> obj = new List<EmployeeStatus>();
            DataTable dT = Dal.GetAll();

            if (dT.Rows.Count <= 0) return obj;
            foreach (DataRow rw in dT.Rows)
                obj.Add(GetRecord(rw));

            return obj;
        }

        public EmployeeStatus GetById(int statusId)
        {
            DataTable dT = Dal.GetById(statusId);
            if (dT.Rows.Count <= 0) return new EmployeeStatus();
            DataRow rw = dT.Rows[0];
            return GetRecord(rw);
        }

        public EmployeeStatus GetRecord(DataRow rw)
        {
            DateTime? defDate = null;
            return new EmployeeStatus
            {
                Active = bool.Parse(rw["active"].ToString()),
                CreatedBy = rw["createdby"].ToString(),
                DateCreated = DateTime.Parse(rw["datecreated"].ToString()),
                DateUpdated = (rw["dateupdated"] != DBNull.Value) ? DateTime.Parse(rw["dateupdated"].ToString()) : defDate,
                StatusId = (rw["statusId"] != DBNull.Value) ? int.Parse(rw["statusId"].ToString()) : 0,
                StatusName = (rw["statusName"] != DBNull.Value) ? rw["statusName"].ToString() : string.Empty,
                UpdatedBy = (rw["updatedby"] != DBNull.Value) ? rw["updatedby"].ToString() : string.Empty
            };
        }

        #endregion
    }
}