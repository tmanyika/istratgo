﻿using System;
using System.Collections.Generic;
using System.Data;
using scorecard.implementations;

namespace HR.Leave
{
    public class LeaveAccrualTimeBL : ILeaveAccrualTime
    {
        #region Properties

        public string ConnectionString
        {
            get { return Util.GetconnectionString(); }
        }

        public LeaveAccrualTimeDAL Dal
        {
            get { return new LeaveAccrualTimeDAL(ConnectionString); }
        }

        #endregion

        #region Default Constructors

        public LeaveAccrualTimeBL() { }

        #endregion

        #region Public Methods & Functions

        public bool AddLeaveAccrualTime(LeaveAccrualTime obj)
        {
            int result = Dal.Save(obj);
            return result > 0 ? true : false;
        }

        public bool UpdateLeaveAccrualTime(LeaveAccrualTime obj)
        {
            int result = Dal.Update(obj);
            return result > 0 ? true : false;
        }

        public bool Delete(int statusId, string updatedBy)
        {
            int result = Dal.Deactivate(statusId, updatedBy);
            return result > 0 ? true : false;
        }

        public List<LeaveAccrualTime> GetByStatus(bool active)
        {
            List<LeaveAccrualTime> obj = new List<LeaveAccrualTime>();
            DataTable dT = Dal.Get(active);

            if (dT.Rows.Count <= 0) return obj;
            foreach (DataRow rw in dT.Rows)
                obj.Add(GetRecord(rw));

            return obj;
        }

        public List<LeaveAccrualTime> GetAll()
        {
            List<LeaveAccrualTime> obj = new List<LeaveAccrualTime>();
            DataTable dT = Dal.GetAll();

            if (dT.Rows.Count <= 0) return obj;
            foreach (DataRow rw in dT.Rows)
                obj.Add(GetRecord(rw));

            return obj;
        }

        public LeaveAccrualTime GetById(int statusId)
        {
            DataTable dT = Dal.GetById(statusId);
            if (dT.Rows.Count <= 0) return new LeaveAccrualTime();
            DataRow rw = dT.Rows[0];
            return GetRecord(rw);
        }

        public LeaveAccrualTime GetRecord(DataRow rw)
        {
            DateTime? defDate = null;
            return new LeaveAccrualTime
            {
                Active = bool.Parse(rw["active"].ToString()),
                CreatedBy = rw["createdby"].ToString(),
                DateCreated = DateTime.Parse(rw["datecreated"].ToString()),
                DateUpdated = (rw["dateupdated"] != DBNull.Value) ? DateTime.Parse(rw["dateupdated"].ToString()) : defDate,
                AccrualTimeId = int.Parse(rw["accrualTimeId"].ToString()),
                TimeValue = int.Parse(rw["timeValue"].ToString()),
                TimeName = rw["timeName"].ToString(),
                UpdatedBy = (rw["updatedby"] != DBNull.Value) ? rw["updatedby"].ToString() : string.Empty
            };
        }

        #endregion
    }
}