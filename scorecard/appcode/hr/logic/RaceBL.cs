﻿using System;
using System.Collections.Generic;
using System.Data;
using scorecard.implementations;

namespace HR.Human.Resources
{
    public class RaceBL : IRace
    {
        #region Properties

        public string ConnectionString
        {
            get { return Util.GetconnectionString(); }
        }

        public RaceDAL Dal
        {
            get { return new RaceDAL(ConnectionString); }
        }

        #endregion

        #region Default Constructors

        public RaceBL() { }

        #endregion

        #region Methods & Functions

        public bool Add(Race obj)
        {
            int result = Dal.Save(obj);
            return result > 0 ? true : false;
        }

        public bool Update(Race obj)
        {
            int result = Dal.Update(obj);
            return result > 0 ? true : false;
        }

        public bool SetStatus(Race obj)
        {
            int result = Dal.UpdateStatus(obj);
            return result > 0 ? true : false;
        }

        public List<Race> GetByStatus(bool active)
        {
            List<Race> obj = new List<Race>();
            DataTable dT = Dal.Get(active);
            if (dT.Rows.Count <= 0) return obj;

            foreach (DataRow rw in dT.Rows)
                obj.Add(GetRow(rw));
            return obj;
        }

        public Race GetById(int raceId)
        {
            DataTable dT = Dal.GetById(raceId);
            if (dT.Rows.Count <= 0) return new Race();
            DataRow rw = dT.Rows[0];
            return GetRow(rw);
        }

        public Race GetRow(DataRow rw)
        {
            DateTime? defDate = null;
            return new Race
            {
                Active = bool.Parse(rw["Active"].ToString()),
                CreatedBy = rw["CreatedBy"].ToString(),
                DateCreated = DateTime.Parse(rw["DateCreated"].ToString()),
                DateUpdated = (rw["DateUpdated"] != DBNull.Value) ? DateTime.Parse(rw["DateUpdated"].ToString()) : defDate,
                RaceDescription = (rw["RaceDescription"] != DBNull.Value) ? rw["RaceDescription"].ToString() : string.Empty,
                RaceId = int.Parse(rw["RaceId"].ToString()),
                RaceName = rw["RaceName"].ToString(),
                UpdatedBy = (rw["UpdatedBy"] != DBNull.Value) ? rw["UpdatedBy"].ToString() : string.Empty
            };
        }

        public Race GetBriefRow(DataRow rw)
        {
            return new Race
            {
                RaceId = (rw["RaceId"] != DBNull.Value) ? int.Parse(rw["RaceId"].ToString()) : 0,
                RaceName = (rw["RaceName"] != DBNull.Value) ? rw["RaceName"].ToString() : string.Empty
            };
        }

        #endregion
    }
}