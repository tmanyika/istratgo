﻿using System;
using System.Data;
using scorecard.implementations;

namespace HR.Leave
{
    public class LeaveReportBL : ILeaveReport
    {
        #region Properties

        public string ConnectionString
        {
            get { return Util.GetconnectionString(); }
        }

        public LeaveReportDAL Dal
        {
            get { return new LeaveReportDAL(ConnectionString); }
        }

        #endregion

        #region Default Class Constructors

        public LeaveReportBL() { }

        #endregion

        #region Default Class Constructors

        public DataTable GetLeaveBalances(int orgUnitId, string leaveTypeXML)
        {
            return Dal.Get(orgUnitId, leaveTypeXML);
        }

        public DataTable GetEmployeesOnLeave(int orgUnitId, string leaveTypeXML)
        {
            return Dal.GetOnLeave(orgUnitId, leaveTypeXML);
        }

        public DataTable GetEmployeesOverThreshold(int orgUnitId, string leaveTypeXML)
        {
            return Dal.GetOverThreshold(orgUnitId, leaveTypeXML);
        }

        public DataTable GetNegativeLeaveBalances(int orgUnitId, string leaveTypeXML)
        {
            return Dal.GetNegativeLeave(orgUnitId, leaveTypeXML);
        }

        #endregion
    }
}