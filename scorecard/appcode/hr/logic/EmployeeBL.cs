﻿using System;
using System.Collections.Generic;
using scorecard.implementations;
using System.Data;
using HR.Human.Resources.Security;
using HR.Leave;
using scorecard.entities;
using scorecard.welcome;
using vb = Microsoft.VisualBasic;
using Scorecard.Entities;

namespace HR.Human.Resources
{
    public class EmployeeBL : IEmployee
    {
        #region Properties

        public string ConnectionString
        {
            get { return Util.GetconnectionString(); }
        }

        public EmployeeDAL Dal
        {
            get { return new EmployeeDAL(ConnectionString); }
        }

        public IWorkingWeek Week
        {
            get { return new WorkingWeekBL(); }
        }

        public IEmployeeStatus Status
        {
            get { return new EmployeeStatusBL(); }
        }

        public IRace Rac
        {
            get { return new RaceBL(); }
        }

        #endregion

        #region Default Constructors

        public EmployeeBL() { }

        #endregion

        #region Public Methods & Functions

        public bool AddEmployee(Employee obj)
        {
            int result = Dal.Save(obj);
            return result > 0 ? true : false;
        }

        public bool UpdateEmployee(Employee obj)
        {
            int result = Dal.Update(obj);
            return result > 0 ? true : false;
        }

        public bool UpdateLogin(Employee obj)
        {
            int result = Dal.UpdateLogin(obj);
            return result > 0 ? true : false;
        }

        public bool UpdateLastLoginTime(int employeeId)
        {
            int result = Dal.UpdateLastTimeLoggeIn(employeeId);
            return result > 0 ? true : false;
        }

        public Employee GetById(int employeeId)
        {
            DataTable dT = Dal.GetById(employeeId);
            return (dT.Rows.Count > 0) ? GetRow(dT.Rows[0]) : new Employee();
        }

        public Employee GetByUserName(string userName)
        {
            DataTable dT = Dal.GetByUserName(userName);
            return (dT.Rows.Count > 0) ? GetRow(dT.Rows[0], dT.Columns) : new Employee();
        }

        public Employee GetByEmailAddress(string email)
        {
            DataTable dT = Dal.GetByEmail(email);
            return (dT.Rows.Count > 0) ? GetRow(dT.Rows[0], dT.Columns) : new Employee();
        }

        public List<Employee> GetAll(int orgUnitId)
        {
            List<Employee> obj = new List<Employee>();
            DataTable dT = Dal.GetAll(orgUnitId);
            if (dT.Rows.Count <= 0) return obj;
            foreach (DataRow rw in dT.Rows) obj.Add(GetRow(rw));
            return obj;
        }

        public List<Employee> GetByManager(int lineManagerId, bool active)
        {
            List<Employee> obj = new List<Employee>();
            DataTable dT = Dal.GetSubordinates(lineManagerId, active);
            if (dT.Rows.Count <= 0) return obj;
            foreach (DataRow rw in dT.Rows) obj.Add(GetRow(rw));
            return obj;
        }

        public List<Employee> GetByOrgStructureUp(int orgUnitId, bool active)
        {
            List<Employee> obj = new List<Employee>();
            DataTable dT = Dal.GetAllUp(orgUnitId, active);
            if (dT.Rows.Count <= 0) return obj;
            foreach (DataRow rw in dT.Rows) obj.Add(GetRow(rw));
            return obj;
        }

        public DataTable GetByOrgStruchaUp(int orgUnitId, bool active)
        {
            return Dal.GetAllUp(orgUnitId, active);
        }

        public List<Employee> GetByOrgStructure(int orgUnitId, bool active)
        {
            List<Employee> obj = new List<Employee>();
            DataTable dT = Dal.GetAll(orgUnitId, active);
            if (dT.Rows.Count <= 0) return obj;
            foreach (DataRow rw in dT.Rows) obj.Add(GetRow(rw));
            return obj;
        }

        public List<Employee> GetByCompanyId(bool active, int companyId)
        {
            List<Employee> obj = new List<Employee>();
            DataTable dT = Dal.GetAll(active, companyId);
            if (dT.Rows.Count <= 0) return obj;
            foreach (DataRow rw in dT.Rows) obj.Add(GetRow(rw));
            return obj;
        }

        public List<Employee> GetActive(int orgUnitId)
        {
            List<Employee> obj = new List<Employee>();
            DataTable dT = Dal.GetActive(orgUnitId);
            if (dT.Rows.Count <= 0) return obj;
            foreach (DataRow rw in dT.Rows) obj.Add(GetRow(rw));
            return obj;
        }

        EmployeeStatus GetStatusRow(DataRow rw)
        {
            return Status.GetRecord(rw);
        }

        WorkingWeek GetWeekRow(DataRow rw)
        {
            return Week.GetRecord(rw);
        }

        public JobTitle GetJobRow(DataRow rw)
        {
            return new JobTitle
            {
                ID = (rw["JobTitleId"] != DBNull.Value) ? int.Parse(rw["JobTitleId"].ToString()) : 0,
                NAME = (rw["jobTitle"] != DBNull.Value) ? vb.Strings.StrConv(rw["jobTitle"].ToString(), vb.VbStrConv.ProperCase) : string.Empty
            };
        }

        public Orgunit GetCompanyRow(DataRow rw)
        {
            return new Orgunit
            {
                ID = int.Parse(rw["OrgUnitId"].ToString()),
                ORGUNIT_NAME = rw["CompanyName"] != DBNull.Value ? vb.Strings.StrConv(rw["CompanyName"].ToString(), vb.VbStrConv.ProperCase) : string.Empty
            };
        }

        public Role GetRoleRow(DataRow rw)
        {
            return new Role
            {
                RoleId = (rw["typeId"] != DBNull.Value) ? int.Parse(rw["typeId"].ToString()) : 0,
                RoleName = rw["typeName"] != DBNull.Value ? vb.Strings.StrConv(rw["typeName"].ToString(), vb.VbStrConv.ProperCase) : string.Empty
            };
        }

        public Employee GetEmployeeNameRow(IDataRecord rw)
        {
            return new Employee
                    {
                        EmployeeId = int.Parse(rw["EmployeeId"].ToString()),
                        FirstName = (rw["FirstName"] != DBNull.Value) ? vb.Strings.StrConv(rw["FirstName"].ToString(), vb.VbStrConv.ProperCase) : string.Empty,
                        MiddleName = (rw["MiddleName"] != DBNull.Value) ? vb.Strings.StrConv(rw["MiddleName"].ToString(), vb.VbStrConv.ProperCase) : string.Empty,
                        LastName = (rw["LastName"] != DBNull.Value) ? vb.Strings.StrConv(rw["LastName"].ToString(), vb.VbStrConv.ProperCase) : string.Empty,
                        FullName = vb.Strings.StrConv(string.Format("{0} {1} {2}", rw["FirstName"], rw["MiddleName"], rw["LastName"]), vb.VbStrConv.ProperCase)
                    };
        }

        public Employee GetEmployeeRow(DataRow rw)
        {
            return new Employee
            {
                BusTelephone = (rw["BusTelephone"] != DBNull.Value) ? rw["BusTelephone"].ToString() : string.Empty,
                Cellphone = (rw["Cellphone"] != DBNull.Value) ? rw["Cellphone"].ToString() : string.Empty,
                EmailAddress = rw["EmailAddress"].ToString().ToLower(),
                EmployeeId = int.Parse(rw["EmployeeId"].ToString()),
                EmployeeNo = (rw["EmployeeNo"] != DBNull.Value) ? rw["EmployeeNo"].ToString() : string.Empty,
                FirstName = vb.Strings.StrConv(rw["FirstName"].ToString(), vb.VbStrConv.ProperCase),
                FullName = vb.Strings.StrConv(string.Format("{0} {1} {2}", rw["LastName"], rw["MiddleName"], rw["FirstName"]), vb.VbStrConv.ProperCase),
                LastName = vb.Strings.StrConv(rw["LastName"].ToString(), vb.VbStrConv.ProperCase),
                MiddleName = (rw["MiddleName"] != DBNull.Value) ? vb.Strings.StrConv(rw["MiddleName"].ToString(), vb.VbStrConv.ProperCase) : string.Empty,
                OrgUnitId = int.Parse(rw["OrgUnitId"].ToString())
            };
        }

        public Employee GetBriefRow(DataRow rw)
        {
            return new Employee
            {
                BusTelephone = (rw["BusTelephone"] != DBNull.Value) ? rw["BusTelephone"].ToString() : string.Empty,
                Cellphone = (rw["Cellphone"] != DBNull.Value) ? rw["Cellphone"].ToString() : string.Empty,
                EmailAddress = rw["EmailAddress"].ToString().ToLower(),
                EmployeeId = int.Parse(rw["EmployeeId"].ToString()),
                EmployeeNo = (rw["EmployeeNo"] != DBNull.Value) ? rw["EmployeeNo"].ToString() : string.Empty,
                FirstName = vb.Strings.StrConv(rw["FirstName"].ToString(), vb.VbStrConv.ProperCase),
                FullName = vb.Strings.StrConv(string.Format("{0} {1} {2}", rw["LastName"], rw["MiddleName"], rw["FirstName"]), vb.VbStrConv.ProperCase),
                LastName = vb.Strings.StrConv(rw["LastName"].ToString(), vb.VbStrConv.ProperCase),
                MiddleName = (rw["MiddleName"] != DBNull.Value) ? vb.Strings.StrConv(rw["MiddleName"].ToString(), vb.VbStrConv.ProperCase) : string.Empty,
                OrgUnitId = int.Parse(rw["OrgUnitId"].ToString()),
                Organisation = GetCompanyRow(rw),
                Position = GetJobRow(rw)
            };
        }

        public Employee GetRow(DataRow rw)
        {
            DateTime? defDate = null;
            int? defInt = null;
            return new Employee
            {
                Active = bool.Parse(rw["Active"].ToString()),
                BusTelephone = (rw["BusTelephone"] != DBNull.Value) ? rw["BusTelephone"].ToString() : string.Empty,
                Cellphone = (rw["Cellphone"] != DBNull.Value) ? rw["Cellphone"].ToString() : string.Empty,
                Organisation = GetCompanyRow(rw),
                Citizenship = (rw["Citizenship"] != DBNull.Value) ? vb.Strings.StrConv(rw["Citizenship"].ToString(), vb.VbStrConv.ProperCase) : string.Empty,
                CreatedBy = rw["CreatedBy"].ToString(),
                Creed = Rac.GetBriefRow(rw),
                DateCreated = DateTime.Parse(rw["DateCreated"].ToString()),
                DateOfAppointment = (rw["DateOfAppointment"] != DBNull.Value) ? DateTime.Parse(rw["DateOfAppointment"].ToString()) : defDate,
                DateOfBirth = (rw["DateOfBirth"] != DBNull.Value) ? DateTime.Parse(rw["DateOfBirth"].ToString()) : defDate,
                DateUpdated = (rw["DateUpdated"] != DBNull.Value) ? DateTime.Parse(rw["DateUpdated"].ToString()) : defDate,
                EmailAddress = rw["EmailAddress"].ToString().ToLower(),
                EmployeeId = int.Parse(rw["EmployeeId"].ToString()),
                EmployeeNo = (rw["EmployeeNo"] != DBNull.Value) ? rw["EmployeeNo"].ToString() : string.Empty,
                FirstName = vb.Strings.StrConv(rw["FirstName"].ToString(), vb.VbStrConv.ProperCase),
                FullName = vb.Strings.StrConv(string.Format("{0} {1} {2}", rw["FirstName"], rw["MiddleName"], rw["LastName"]), vb.VbStrConv.ProperCase),
                IDNumber = (rw["IDNumber"] != DBNull.Value) ? rw["IDNumber"].ToString().ToUpper() : string.Empty,
                JobTitleId = (rw["JobTitleId"] != DBNull.Value) ? int.Parse(rw["JobTitleId"].ToString()) : defInt,
                LastName = vb.Strings.StrConv(rw["LastName"].ToString(), vb.VbStrConv.ProperCase),
                LineManagerEmployeeId = (rw["LineManagerEmployeeId"] != DBNull.Value) ? int.Parse(rw["LineManagerEmployeeId"].ToString()) : int.Parse(rw["EmployeeId"].ToString()),
                MiddleName = (rw["MiddleName"] != DBNull.Value) ? vb.Strings.StrConv(rw["MiddleName"].ToString(), vb.VbStrConv.ProperCase) : string.Empty,
                OrgUnitId = int.Parse(rw["OrgUnitId"].ToString()),
                Pass = (rw["Pass"] != DBNull.Value) ? rw["Pass"].ToString() : string.Empty,
                PassportNo = (rw["PassportNo"] != DBNull.Value) ? rw["PassportNo"].ToString().ToUpper() : string.Empty,
                RaceId = (rw["RaceId"] != DBNull.Value) ? int.Parse(rw["RaceId"].ToString()) : defInt,
                RoleId = (rw["RoleId"] != DBNull.Value) ? int.Parse(rw["RoleId"].ToString()) : defInt,
                StatusId = (rw["StatusId"] != DBNull.Value) ? int.Parse(rw["StatusId"].ToString()) : defInt,
                UpdatedBy = (rw["UpdatedBy"] != DBNull.Value) ? rw["UpdatedBy"].ToString() : string.Empty,
                UserName = (rw["UserName"] != DBNull.Value) ? rw["UserName"].ToString() : string.Empty,
                WorkingWeekId = (rw["WorkingWeekId"] != DBNull.Value) ? int.Parse(rw["WorkingWeekId"].ToString()) : defInt,
                CompanyId = int.Parse(rw["CompanyId"].ToString()),
                CountryId = rw["CountryId"] != DBNull.Value ? int.Parse(rw["CountryId"].ToString()) : defInt,
                Position = GetJobRow(rw),
                Status = GetStatusRow(rw),
                WekinWeek = GetWeekRow(rw),
                UserRole = GetRoleRow(rw)

            };
        }

        public Employee GetRow(DataRow rw, DataColumnCollection cols)
        {
            DateTime? defDate = null;
            int? defInt = null;
            bool activated = true;

            if (cols.Contains("Activated"))
            {
                activated = (rw["Activated"] != DBNull.Value) ? bool.Parse(rw["Activated"].ToString()) : true;
            }

            return new Employee
            {
                Active = bool.Parse(rw["Active"].ToString()),
                BusTelephone = (rw["BusTelephone"] != DBNull.Value) ? rw["BusTelephone"].ToString() : string.Empty,
                Cellphone = (rw["Cellphone"] != DBNull.Value) ? rw["Cellphone"].ToString() : string.Empty,
                Organisation = GetCompanyRow(rw),
                CreatedBy = rw["CreatedBy"].ToString(),
                DateCreated = DateTime.Parse(rw["DateCreated"].ToString()),
                DateUpdated = (rw["DateUpdated"] != DBNull.Value) ? DateTime.Parse(rw["DateUpdated"].ToString()) : defDate,
                EmailAddress = rw["EmailAddress"].ToString(),
                EmployeeId = int.Parse(rw["EmployeeId"].ToString()),
                EmployeeNo = (rw["EmployeeNo"] != DBNull.Value) ? rw["EmployeeNo"].ToString() : string.Empty,
                FirstName = rw["FirstName"].ToString(),
                FullName = string.Format("{0} {1} {2}", rw["FirstName"], rw["MiddleName"], rw["LastName"]),
                JobTitleId = (rw["JobTitleId"] != DBNull.Value) ? int.Parse(rw["JobTitleId"].ToString()) : defInt,
                LastName = rw["LastName"].ToString(),
                LineManagerEmployeeId = (rw["LineManagerEmployeeId"] != DBNull.Value) ? int.Parse(rw["LineManagerEmployeeId"].ToString()) : int.Parse(rw["EmployeeId"].ToString()),
                MiddleName = (rw["MiddleName"] != DBNull.Value) ? rw["MiddleName"].ToString() : string.Empty,
                OrgUnitId = int.Parse(rw["OrgUnitId"].ToString()),
                Pass = (rw["Pass"] != DBNull.Value) ? rw["Pass"].ToString() : string.Empty,
                RoleId = (rw["RoleId"] != DBNull.Value) ? int.Parse(rw["RoleId"].ToString()) : defInt,
                StatusId = (rw["StatusId"] != DBNull.Value) ? int.Parse(rw["StatusId"].ToString()) : defInt,
                UpdatedBy = (rw["UpdatedBy"] != DBNull.Value) ? rw["UpdatedBy"].ToString() : string.Empty,
                UserName = (rw["UserName"] != DBNull.Value) ? rw["UserName"].ToString() : string.Empty,
                WorkingWeekId = (rw["WorkingWeekId"] != DBNull.Value) ? int.Parse(rw["WorkingWeekId"].ToString()) : defInt,
                CompanyId = int.Parse(rw["CompanyId"].ToString()),
                CountryId = rw["CountryId"] != DBNull.Value ? int.Parse(rw["CountryId"].ToString()) : defInt,
                PasswordIsTemp = cols.Contains("PasswordIsTemp") ? bool.Parse(rw["PasswordIsTemp"].ToString()) : false,
                Position = GetJobRow(rw),
                Status = GetStatusRow(rw),
                WekinWeek = GetWeekRow(rw),
                UserRole = GetRoleRow(rw),
                Start = new Welcome
                {
                    ShowAgain = cols.Contains("TotalShowAgain") ? rw["TotalShowAgain"] != DBNull.Value ?
                                int.Parse(rw["TotalShowAgain"].ToString()) > 0 ? true : false : false : false
                },
                Activation = new CompanyActivation
                {
                    Activated = activated
                }
            };
        }

        #endregion
    }
}