﻿using System;
using System.Collections.Generic;

namespace HR.Leave
{
    public interface ILeaveAccrual
    {
        bool AddLeaveAccrual(LeaveAccrual obj);
        bool UpdateLeaveAccrual(LeaveAccrual obj);
        bool Delete(int leaveAccrualId, string updatedBy);

        LeaveAccrual GetById(int leaveAccrualId);
        List<LeaveAccrual> GetByStatus(int companyId, bool active);
        List<LeaveAccrual> GetAll(int companyId);
    }
}
