﻿using System;
using System.Collections.Generic;
using System.Data;

namespace HR.Human.Resources
{
    public interface IRace
    {
        bool Add(Race obj);
        bool Update(Race obj);
        bool SetStatus(Race obj);

        Race GetById(int raceId);
        Race GetRow(DataRow rw);
        Race GetBriefRow(DataRow rw);
        List<Race> GetByStatus(bool active);
    }
}
