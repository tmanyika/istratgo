﻿using System;
using System.Collections.Generic;
using scorecard.entities;
using System.Data;
using HR.Human.Resources.Security;

namespace HR.Human.Resources
{
    public interface IEmployee
    {
        bool AddEmployee(Employee obj);
        bool UpdateEmployee(Employee obj);
        bool UpdateLogin(Employee obj);
        bool UpdateLastLoginTime(int employeeId);

        JobTitle GetJobRow(DataRow rw);
        Orgunit GetCompanyRow(DataRow rw);
        Role GetRoleRow(DataRow rw);

        Employee GetEmployeeRow(DataRow rw);
        Employee GetBriefRow(DataRow rw);
        Employee GetById(int employeeId);
        Employee GetByUserName(string userName);
        Employee GetByEmailAddress(string email);
        Employee GetEmployeeNameRow(IDataRecord rw);

        List<Employee> GetAll(int orgUnitId);
        List<Employee> GetByOrgStructure(int orgUnitId, bool active);
        List<Employee> GetByOrgStructureUp(int orgUnitId, bool active);
        List<Employee> GetByCompanyId(bool active, int companyId);
        List<Employee> GetActive(int orgUnitId);
        List<Employee> GetByManager(int lineManagerId, bool active);

        DataTable GetByOrgStruchaUp(int orgUnitId, bool active);
    }
}