﻿using System;
using System.Collections.Generic;
using System.Data;

namespace HR.Leave
{
    public interface ILeaveType
    {
        bool AddLeaveType(LeaveType obj);
        bool UpdateLeaveType(LeaveType obj);
        bool Delete(int leaveId, string updatedBy);

        LeaveType GetById(int leaveId);
        LeaveType GetRecord(DataRow rw);
        List<LeaveType> GetByStatus(int companyId, bool active);
        List<LeaveType> GetByCompanyId(int companyId);
    }
}
