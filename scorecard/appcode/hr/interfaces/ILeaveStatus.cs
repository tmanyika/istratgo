﻿using System;
using System.Collections.Generic;
using System.Data;

namespace HR.Leave
{
    public interface ILeaveStatus
    {
        bool AddLeaveStatus(LeaveStatus obj);
        bool UpdateLeaveStatus(LeaveStatus obj);
        bool Delete(int statusId, string updatedBy);

        LeaveStatus GetById(int statusId);
        LeaveStatus GetRecord(DataRow rw);
        List<LeaveStatus> GetByStatus(bool active);
        List<LeaveStatus> GetAll();
    }
}
