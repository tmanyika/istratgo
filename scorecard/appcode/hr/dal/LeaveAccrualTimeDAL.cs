﻿using System;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

namespace HR.Leave
{
    public class LeaveAccrualTimeDAL
    {
        #region Variables

        private string _conString;

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return _conString; }
            set { _conString = value; }
        }

        #endregion

        #region Default Constructors

        public LeaveAccrualTimeDAL(string conString)
        {
            _conString = conString;
        }

        #endregion

        #region Public Methods and Functions

        public DataTable GetAll()
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_LeaveAccrualTime_GetAll").Tables[0];
        }

        public DataTable Get(bool active)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_LeaveAccrualTime_GetActive", active).Tables[0];
        }

        public DataTable GetById(int statusId)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_LeaveAccrualTime_GetById", statusId).Tables[0];
        }

        public int Save(LeaveAccrualTime obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_LeaveAccrualTime_Insert", obj.TimeName, obj.TimeValue,
                                                       obj.Active, obj.CreatedBy);
            return result;
        }

        public int Update(LeaveAccrualTime obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_LeaveAccrualTime_Update", obj.AccrualTimeId, obj.TimeName, obj.TimeValue,
                                                        obj.Active, obj.UpdatedBy);
            return result;
        }

        public int Deactivate(int statusId, string updatedBy)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_LeaveAccrualTime_Deactivate", statusId, updatedBy);
            return result;
        }

        #endregion
    }
}