﻿using System;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using HR.Human.Resources.Security;

namespace HR.Human.Resources
{
    public class EmployeeDAL
    {
        #region Variables

        private string _conString;

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return _conString; }
            set { _conString = value; }
        }

        #endregion

        #region Default Constructors

        public EmployeeDAL(string conString)
        {
            _conString = conString;
        }

        #endregion

        #region Public Methods and Functions

        public DataTable GetAll(bool active, int companyId)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_Employee_GetByCompanyId", companyId, active).Tables[0];
        }

        public DataTable GetAll(int orgUnitId, bool active)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_Employee_GetByOrgUnitId", orgUnitId, active).Tables[0];
        }

        public DataTable GetAllUp(int orgUnitId, bool active)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_Employee_GetByOrgUnitIdUpHiraki", orgUnitId, active).Tables[0];
        }

        public DataTable GetAll(int orgUnitId)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_Employee_GetAll", orgUnitId).Tables[0];
        }

        public DataTable GetActive(int orgUnitId)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_Employee_GetActive", orgUnitId).Tables[0];
        }

        public DataTable GetById(int employeeId)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_Employee_GetById", employeeId).Tables[0];
        }

        public DataTable GetByUserName(string username)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_Employee_GetByUserName", username).Tables[0];
        }

        public DataTable GetByEmail(string email)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_Employee_GetByEmail", email).Tables[0];
        }

        public int Save(Employee obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_Employee_Insert", obj.LineManagerEmployeeId,
                                                        obj.OrgUnitId, obj.FirstName, obj.MiddleName, obj.LastName, obj.EmailAddress,
                                                        obj.JobTitleId, obj.StatusId, obj.EmployeeNo, obj.WorkingWeekId, obj.Cellphone,
                                                        obj.BusTelephone, obj.CreatedBy, obj.Active, obj.UserName, obj.Pass, obj.RoleId,
                                                        obj.DateOfAppointment, obj.RaceId, obj.PassportNo, obj.IDNumber, obj.DateOfBirth, obj.Citizenship);
            return result;
        }

        public int Update(Employee obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_Employee_Update", obj.EmployeeId, obj.LineManagerEmployeeId,
                                                        obj.OrgUnitId, obj.FirstName, obj.MiddleName, obj.LastName, obj.EmailAddress,
                                                        obj.JobTitleId, obj.StatusId, obj.EmployeeNo, obj.WorkingWeekId, obj.Cellphone,
                                                        obj.BusTelephone, obj.UpdatedBy, obj.Active, obj.RoleId, obj.DateOfAppointment, obj.RaceId, 
                                                        obj.PassportNo, obj.IDNumber, obj.DateOfBirth, obj.Citizenship);
            return result;
        }

        public int UpdateLogin(Employee obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_Employee_UpdateLogin", obj.UserName, obj.Active, obj.Pass,
                                                    obj.RoleId, obj.UpdatedBy);
            return result;
        }

        public int UpdateLastTimeLoggeIn(int employeeId)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_Employee_UpdateLastLoginTime", employeeId);
            return result;
        }

        public DataTable GetSubordinates(int lineManagerId, bool active)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_Employee_GetByLineManagerId", lineManagerId, active).Tables[0];
        }

        #endregion
    }
}