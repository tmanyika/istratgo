﻿using System;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

namespace HR.Human.Resources
{
    public class RaceDAL
    {
        #region Variables

        private string _conString;

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return _conString; }
            set { _conString = value; }
        }

        #endregion

        #region Default Constructors

        public RaceDAL(string conString)
        {
            _conString = conString;
        }

        #endregion

        #region Public Methods and Functions

        public DataTable Get(bool active)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_Race_GetActive", active).Tables[0];
        }

        public DataTable GetById(int raceId)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_Race_GetById", raceId).Tables[0];
        }

        public int Save(Race obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_Race_Insert", obj.RaceName, obj.RaceDescription, obj.Active, obj.CreatedBy);
            return result;
        }

        public int Update(Race obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_Race_Update", obj.RaceId, obj.RaceName, obj.RaceDescription, obj.Active, obj.UpdatedBy);
            return result;
        }

        public int UpdateStatus(Race obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_Race_SetStatus", obj.RaceId, obj.Active, obj.UpdatedBy);
            return result;
        }

        #endregion
    }
}