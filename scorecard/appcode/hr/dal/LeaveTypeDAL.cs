﻿using System;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

namespace HR.Leave
{
    public class LeaveTypeDAL
    {
        #region Variables

        private string _conString;

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return _conString; }
            set { _conString = value; }
        }

        #endregion

        #region Default Constructors

        public LeaveTypeDAL(string conString)
        {
            _conString = conString;
        }

        #endregion

        #region Public Methods and Functions

        public DataTable Get(int companyId)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_LeaveType_GetByCompanyId", companyId).Tables[0];
        }

        public DataTable Get(int companyId, bool active)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_LeaveType_GetActive", companyId, active).Tables[0];
        }

        public DataTable GetById(int leaveId)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_LeaveType_GetById", leaveId).Tables[0];
        }

        public int Save(LeaveType obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_LeaveType_Insert", obj.CompanyId, obj.LeaveName,
                                                        obj.Description, obj.Active, obj.CreatedBy);
            return result;
        }

        public int Update(LeaveType obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_LeaveType_Update", obj.LeaveId, obj.LeaveName,
                                                        obj.Description, obj.Active, obj.UpdatedBy);
            return result;
        }

        public int Deactivate(int leaveId, string updatedBy)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_LeaveType_Deactivate", leaveId, updatedBy);
            return result;
        }

        #endregion
    }
}