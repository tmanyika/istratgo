﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.Human.Resources
{
    public class Race
    {
        public int RaceId { get; set; }
        public string RaceName { get; set; }
        public string RaceDescription { get; set; }

        public bool Active { get; set; }

        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
    }
}