﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.Human.Resources
{
    public class EmploymentHist
    {
        public int HistoryId { get; set; }
        public int EmployeeId { get; set; }
        public int JobTitleId { get; set; }
        public int DaysOnSameRole { get; set; }
        public int YearsOnSameRole { get; set; }

        public bool Active { get; set; }
      
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
    }
}