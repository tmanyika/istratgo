﻿using System;
using System.Data;
using System.Collections.Generic;
using ScoreCard.Common.Entities;
using ScoreCard.Common.Interfaces;
using ScoreCard.Common.DataAccess;
using scorecard.implementations;

namespace ScoreCard.Common.Logic
{
    public class ScoreCardAreaBL : IScoreCardArea
    {
        #region Properties

        public string ConnectionString
        {
            get { return Util.GetconnectionString(); }
        }

        public ScoreCardAreaDAL Dal
        {
            get { return new ScoreCardAreaDAL(ConnectionString); }
        }

        #endregion

        #region Default Constructors

        public ScoreCardAreaBL() { }

        #endregion

        #region Public Methods & Functions

        public bool AddScoreCardArea(ScoreCardArea obj)
        {
            int result = Dal.Save(obj);
            return result > 0 ? true : false;
        }

        public bool UpdateScoreCardArea(ScoreCardArea obj)
        {
            int result = Dal.Update(obj);
            return result > 0 ? true : false;
        }

        public bool SetStatus(ScoreCardArea obj)
        {
            int result = Dal.Deactivate(obj);
            return result > 0 ? true : false;
        }

        public List<ScoreCardArea> GetByStatus(bool active)
        {
            List<ScoreCardArea> obj = new List<ScoreCardArea>();
            using (IDataReader reader = Dal.Get(active))
            {
                while (reader.Read())
                    obj.Add(GetRow(reader));
            };
            return obj;
        }

        public List<ScoreCardArea> GetByArea(int areaId)
        {
            List<ScoreCardArea> obj = new List<ScoreCardArea>();
            using (IDataReader reader = Dal.Get(areaId))
            {
                while (reader.Read())
                    obj.Add(GetRow(reader));
            };
            return obj;
        }

        public List<ScoreCardArea> GetAllAreas()
        {
            List<ScoreCardArea> obj = new List<ScoreCardArea>();
            using (IDataReader reader = Dal.GetAll())
            {
                while (reader.Read())
                    obj.Add(GetRow(reader));
            };
            return obj;
        }

        public ScoreCardArea GetById(int areaId)
        {
            using (IDataReader reader = Dal.Get(areaId))
            {
                if (reader.Read())
                    return GetRow(reader);
            }
            return new ScoreCardArea();
        }

        public ScoreCardArea GetRow(IDataRecord rw)
        {
            DateTime? defDate = null;
            return new ScoreCardArea
            {
                Active = bool.Parse(rw["Active"].ToString()),
                AreaAliasName = (rw["AreaAliasName"] == DBNull.Value) ?
                                          string.Empty : rw["AreaAliasName"].ToString(),
                AreaName = rw["AreaName"].ToString(),
                AreaDescription = (rw["AreaDescription"] == DBNull.Value) ?
                                         string.Empty : rw["AreaDescription"].ToString(),
                AreaId = int.Parse(rw["AreaId"].ToString()),                
                CreatedBy = (rw["CreatedBy"] == DBNull.Value) ?
                                        string.Empty : rw["CreatedBy"].ToString(),
                DateCreated = DateTime.Parse(rw["DateCreated"].ToString()),
                DateUpdated = (rw["DateUpdated"] == DBNull.Value) ?
                                    defDate : DateTime.Parse(rw["DateUpdated"].ToString()),
                UpdatedBy = (rw["UpdatedBy"] == DBNull.Value) ?
                                    string.Empty : rw["UpdatedBy"].ToString()
            };
        }

        public ScoreCardArea GetBriefRow(IDataRecord rw)
        {
            return new ScoreCardArea
            {
                AreaId = (rw["AreaId"] == DBNull.Value) ?
                                    0 : int.Parse(rw["AreaId"].ToString()),
                AreaName = (rw["AreaName"] == DBNull.Value) ?
                                    string.Empty : rw["AreaName"].ToString(),
                AreaAliasName = (rw["AreaAliasName"] == DBNull.Value) ?
                                    string.Empty : rw["AreaAliasName"].ToString()
            };
        }

        #endregion
    }
}