﻿using System;
using System.Data;

using System.Collections.Generic;
using ScoreCard.Common.Interfaces;
using ScoreCard.Common.Entities;
using ScoreCard.Common.DataAccess;
using scorecard.implementations;

namespace ScoreCard.Common.Logic
{
    public class ScoreCardStatusBL : IScoreCardStatus
    {
        #region Properties

        public string ConnectionString
        {
            get { return Util.GetconnectionString(); }
        }

        public ScoreCardStatusDAL Dal
        {
            get { return new ScoreCardStatusDAL(ConnectionString); }
        }

        #endregion

        public ScoreCardStatusBL() { }

        public bool AddScoreCardStatus(ScoreCardStatus obj)
        {
            int result = Dal.Save(obj);
            return result > 0 ? true : false;
        }

        public bool UpdateScoreCardStatus(ScoreCardStatus obj)
        {
            int result = Dal.Update(obj);
            return result > 0 ? true : false;
        }

        public bool Delete(int statusId, string updatedBy)
        {
            int result = Dal.Deactivate(statusId, updatedBy);
            return result > 0 ? true : false;
        }

        public List<ScoreCardStatus> GetByStatus( bool active)
        {
            List<ScoreCardStatus> obj = new List<ScoreCardStatus>();
            DataTable dT = Dal.Get(active);

            if (dT.Rows.Count <= 0) return obj;
            foreach (DataRow rw in dT.Rows)
                obj.Add(GetRecord(rw));

            return obj;
        }

        public List<ScoreCardStatus> GetAll()
        {
            List<ScoreCardStatus> obj = new List<ScoreCardStatus>();
            DataTable dT = Dal.GetAll();

            if (dT.Rows.Count <= 0) return obj;
            foreach (DataRow rw in dT.Rows)
                obj.Add(GetRecord(rw));

            return obj;
        }

        public ScoreCardStatus GetById(int statusId)
        {
            DataTable dT = Dal.GetById(statusId);
            if (dT.Rows.Count <= 0) return new ScoreCardStatus();
            DataRow rw = dT.Rows[0];
            return GetRecord(rw);
        }

        public ScoreCardStatus GetRecord(DataRow rw)
        {
            DateTime? defDate = null;
            return new ScoreCardStatus
            {
                Active = bool.Parse(rw["active"].ToString()),
                CreatedBy = rw["createdby"].ToString(),
                DateCreated = DateTime.Parse(rw["datecreated"].ToString()),
                DateUpdated = (rw["dateupdated"] != DBNull.Value) ? DateTime.Parse(rw["dateupdated"].ToString()) : defDate,
                StatusId = int.Parse(rw["statusId"].ToString()),
                StatusName = rw["statusName"].ToString(),
                UpdatedBy = (rw["updatedby"] != DBNull.Value) ? rw["updatedby"].ToString() : string.Empty
            };
        }
    }
}