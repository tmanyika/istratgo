﻿using System;
using System.Configuration;

namespace ScoreCard.Common.Config
{
    public static class ScoreCardAreaConfig
    {
        public static int EmployeeAreaId
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings["EmployeeAreaId"].ToString());
            }
        }

        public static int JobTitleAreaId
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings["JobTitleAreaId"].ToString());
            }
        }

        public static int OrgUnitAreaId
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings["OrgUnitAreaId"].ToString());
            }
        }

        public static int ProjectAreaId
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings["ProjectAreaId"].ToString());
            }
        }

        public static int SavedForLaterId
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings["ProjectAreaId"].ToString());
            }
        }

        public static int SubmittedId
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings["SubmittedId"].ToString());
            }
        }

        public static int ApprovalInProgressId
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings["ApprovalInProgressId"].ToString());
            }
        }

        public static int ApprovedId
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings["ApprovedId"].ToString());
            }
        }

        public static int DeclinedId
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings["DeclinedId"].ToString());
            }
        }

        public static int ClosedId
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings["ClosedId"].ToString());
            }
        }
    }
}