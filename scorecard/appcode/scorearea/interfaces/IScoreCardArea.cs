﻿using System;
using System.Data;
using ScoreCard.Common.Entities;
using System.Collections.Generic;

namespace ScoreCard.Common.Interfaces
{
    public interface IScoreCardArea
    {
        bool AddScoreCardArea(ScoreCardArea obj);
        bool UpdateScoreCardArea(ScoreCardArea obj);
        bool SetStatus(ScoreCardArea obj);

        List<ScoreCardArea> GetByStatus(bool active);
        List<ScoreCardArea> GetAllAreas();

        ScoreCardArea GetById(int areaId);
        ScoreCardArea GetRow(IDataRecord rw);
        ScoreCardArea GetBriefRow(IDataRecord rw);
    }
}
