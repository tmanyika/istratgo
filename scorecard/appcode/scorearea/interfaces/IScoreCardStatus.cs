﻿using System;
using System.Collections.Generic;
using System.Data;
using ScoreCard.Common.Entities;

namespace ScoreCard.Common.Interfaces
{
    public interface IScoreCardStatus
    {
        bool AddScoreCardStatus(ScoreCardStatus obj);
        bool UpdateScoreCardStatus(ScoreCardStatus obj);
        bool Delete(int statusId, string updatedBy);

        ScoreCardStatus GetById(int statusId);
        ScoreCardStatus GetRecord(DataRow rw);
        List<ScoreCardStatus> GetByStatus(bool active);
        List<ScoreCardStatus> GetAll();
    }
}
