﻿using System;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using ScoreCard.Common.Entities;

namespace ScoreCard.Common.DataAccess
{
    public class ScoreCardAreaDAL
    {
        #region Variables

        private string _conString;

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return _conString; }
            set { _conString = value; }
        }

        #endregion

        #region Default Constructors

        public ScoreCardAreaDAL(string conString)
        {
            _conString = conString;
        }

        #endregion

        #region Public Methods and Functions

        public int Save(ScoreCardArea obj)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_ScoreCardArea_Insert", obj.AreaName, obj.AreaAliasName,
                                                                                                   obj.AreaDescription, obj.Active, obj.CreatedBy);
            return (result == DBNull.Value) ? 0 : int.Parse(result.ToString());
        }

        public int Update(ScoreCardArea obj)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_ScoreCardArea_Update", obj.AreaId, obj.AreaName, obj.AreaAliasName,
                                                                                                obj.AreaDescription, obj.Active, obj.UpdatedBy);
            return (result == DBNull.Value) ? 0 : int.Parse(result.ToString());
        }

        public int Deactivate(ScoreCardArea obj)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_ScoreCardArea_Deactivate", obj.AreaId, obj.Active, obj.UpdatedBy);
            return (result == DBNull.Value) ? 0 : int.Parse(result.ToString());
        }

        public IDataReader Get(int areaId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_ScoreCardArea_GetById", areaId);
        }

        public IDataReader Get(bool active)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_ScoreCardArea_GetByStatus", active);
        }

        public IDataReader GetAll()
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_ScoreCardArea_GetAll");
        }

        #endregion
    }
}