﻿using System;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using ScoreCard.Common.Entities;

namespace ScoreCard.Common.DataAccess
{
    public class ScoreCardStatusDAL
    {
        #region Variables

        private string _conString;

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return _conString; }
            set { _conString = value; }
        }

        #endregion

        #region Default Constructors

        public ScoreCardStatusDAL(string conString)
        {
            _conString = conString;
        }

        #endregion

        #region Public Methods and Functions

        public DataTable GetAll()
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_ScoreCardStatus_GetAll").Tables[0];
        }

        public DataTable Get(bool active)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_ScoreCardStatus_GetActive", active).Tables[0];
        }

        public DataTable GetById(int statusId)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_ScoreCardStatus_GetById", statusId).Tables[0];
        }

        public int Save(ScoreCardStatus obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_ScoreCardStatus_Insert", obj.StatusName,
                                                       obj.Active, obj.CreatedBy);
            return result;
        }

        public int Update(ScoreCardStatus obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_ScoreCardStatus_Update", obj.StatusId, obj.StatusName,
                                                        obj.Active, obj.UpdatedBy);
            return result;
        }

        public int Deactivate(int statusId, string updatedBy)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_ScoreCardStatus_Deactivate", statusId, updatedBy);
            return result;
        }

        #endregion
    }
}