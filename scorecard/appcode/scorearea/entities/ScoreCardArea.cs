﻿using System;

namespace ScoreCard.Common.Entities
{
    public class ScoreCardArea
    {
        public int AreaId { get; set; }
        public string AreaName { get; set; }
        public string AreaAliasName { get; set; }
        public string AreaDescription { get; set; }

        public bool Active { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }

        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
    }
}