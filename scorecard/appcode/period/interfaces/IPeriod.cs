﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ScoreCard.Common.Entities;
using System.Data;

namespace ScoreCard.Common.Interfaces
{
    public interface IPeriod
    {
        bool Add(Period obj);
        bool Update(Period obj);
        bool Deactivate(Period obj);

        Period GetById(int periodId);
        Period GetRow(IDataRecord rw);
        Period GetBriefRow(IDataRecord rw);

        List<Period> GetByStatus(bool active);
    }
}
