﻿using System;

namespace ScoreCard.Common.Entities
{
    public class Period
    {
        public int PeriodId { get; set; }
        public int? NumberOfMonths { get; set; }

        public bool Active { get; set; }

        public string PeriodName { get; set; }
        public string PeriodDescription { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
    }
}