﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ScoreCard.Common.DataAccess;
using scorecard.implementations;
using ScoreCard.Common.Entities;
using System.Data;
using ScoreCard.Common.Interfaces;

namespace ScoreCard.Common.Logic
{
    public class PeriodBL : IPeriod
    {
        #region Properties

        public string ConnectionString
        {
            get { return Util.GetconnectionString(); }
        }

        public PeriodDAL Dal
        {
            get { return new PeriodDAL(ConnectionString); }
        }

        #endregion

        #region Default Constructors

        public PeriodBL() { }

        #endregion

        #region Public Methods & Functions

        public bool Add(Period obj)
        {
            int result = Dal.Save(obj);
            return (result > 0);
        }

        public bool Update(Period obj)
        {
            int result = Dal.Update(obj);
            return (result > 0);
        }

        public bool Deactivate(Period obj)
        {
            int result = Dal.UpdateStatus(obj);
            return (result > 0);
        }

        public Period GetById(int periodId)
        {
            using (IDataReader rw = Dal.Get(periodId))
            {
                return (rw.Read()) ? GetRow(rw) : new Period();
            }
        }

        public List<Period> GetByStatus(bool active)
        {
            List<Period> obj = new List<Period>();
            using (IDataReader rw = Dal.Get(active))
            {
                while (rw.Read())
                    obj.Add(GetRow(rw));
            }
            return obj;
        }

        public Period GetRow(IDataRecord rw)
        {
            DateTime? defDate = null;
            int? defInt = null;

            return new Period
            {
                Active = bool.Parse(rw["Active"].ToString()),
                CreatedBy = rw["CreatedBy"].ToString(),
                DateCreated = DateTime.Parse(rw["DateCreated"].ToString()),
                DateUpdated = (rw["DateUpdated"] != DBNull.Value) ? DateTime.Parse(rw["DateUpdated"].ToString()) : defDate,
                PeriodId = int.Parse(rw["PeriodId"].ToString()),
                PeriodName = rw["PeriodName"].ToString(),
                PeriodDescription = (rw["PeriodDescription"] != DBNull.Value) ? rw["PeriodDescription"].ToString() : string.Empty,
                NumberOfMonths = (rw["NumberOfMonths"] != DBNull.Value) ? int.Parse(rw["NumberOfMonths"].ToString()) : defInt,
                UpdatedBy = (rw["UpdatedBy"] != DBNull.Value) ? rw["UpdatedBy"].ToString() : string.Empty
            };
        }

        public Period GetBriefRow(IDataRecord rw)
        {
            return new Period
            {
                PeriodId = (rw["PeriodId"] != DBNull.Value) ? int.Parse(rw["PeriodId"].ToString()) : 0,
                PeriodName = (rw["PeriodName"] != DBNull.Value) ? rw["PeriodName"].ToString() : string.Empty
            };
        }

        #endregion
    }
}