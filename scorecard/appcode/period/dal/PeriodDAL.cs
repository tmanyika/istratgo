﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.ApplicationBlocks.Data;
using System.Data;
using HR.Incentives.Entities;
using ScoreCard.Common.Entities;

namespace ScoreCard.Common.DataAccess
{
    public class PeriodDAL
    {
        #region Variables

        private string _conString;

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return _conString; }
            set { _conString = value; }
        }

        #endregion

        #region Default Constructors

        public PeriodDAL(string conString)
        {
            _conString = conString;
        }

        #endregion

        #region Public Methods and Functions

        public IDataReader Get(bool active)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_Period_GetByStatus", active);
        }

        public IDataReader Get(int periodId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_Period_GetById", periodId);
        }

        public int Save(Period obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_Period_Insert", obj.PeriodName, obj.PeriodDescription,
                                                                                                obj.NumberOfMonths, obj.Active, obj.CreatedBy);
            return result;
        }

        public int Update(Period obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_Period_Update", obj.PeriodId, obj.PeriodName, obj.PeriodDescription,
                                                                                                obj.NumberOfMonths, obj.Active, obj.UpdatedBy);
            return result;
        }

        public int UpdateStatus(Period obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_Period_Deactivate", obj.PeriodId, obj.Active, obj.UpdatedBy);
            return result;
        }

        #endregion
    }
}