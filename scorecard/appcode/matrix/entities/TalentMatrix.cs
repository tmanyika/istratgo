﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.Human.Resources
{
    public class TalentMatrix
    {
        public int MatrixId { get; set; }
        public int CompanyId { get; set; }
        public int PositionInMatrix { get; set; }

        public decimal LowerBound { get; set; }
        public decimal UpperBound { get; set; }

        public bool Active { get; set; }

        public string TalentMatrixName { get; set; }
        public string TalentMatrixDescription { get; set; }
        public string LowerBoundSign { get; set; }
        public string UpperBoundSign { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
    }
}