﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Scorecard.Entities;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

namespace Scorecard.DataAccess
{
    public class CompanyActivationDAL
    {
        #region Variables

        private string _conString;

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return _conString; }
            set { _conString = value; }
        }

        #endregion

           #region Default Constructors

        public CompanyActivationDAL(string conString)
        {
            _conString = conString;
        }

        #endregion

        #region Public Methods and Functions

        public int Update(CompanyActivation obj)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_CompanyActivation_Update", obj.CompanyId, obj.ActivationCode);
            return (result == DBNull.Value) ? 0 : int.Parse(result.ToString());
        }

        public IDataReader Get(CompanyActivation obj)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_CompanyActivation_Get", obj.CompanyId, obj.ActivationCode);
        }

        #endregion
    }
}