﻿using System;
using Scorecard.Entities;
using System.Data;

namespace Scorecard.Interfaces
{
    public interface ICompanyActivation
    {
        CompanyActivation GetDetails(CompanyActivation obj);
        bool ActivateAccount(CompanyActivation obj);
    }
}
