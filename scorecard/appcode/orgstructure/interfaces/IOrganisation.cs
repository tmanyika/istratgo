﻿using System;
using Scorecard.Entities;
using System.Data;

namespace Scorecard.Interfaces
{
    public interface IOrganisation
    {
        Organisation GetBriefRow(IDataRecord rw);
    }
}
