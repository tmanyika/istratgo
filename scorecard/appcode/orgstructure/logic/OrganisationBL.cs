﻿using System;
using Scorecard.Interfaces;
using Scorecard.Entities;
using System.Data;

namespace Scorecard.Logic
{
    public class OrganisationBL : IOrganisation
    {
        public Organisation GetBriefRow(IDataRecord rw)
        {
            return new Organisation
                      {
                          OrganisationId = (rw["OrgUnitId"] != DBNull.Value) ? int.Parse(rw["OrgUnitId"].ToString()) : 0,
                          OrganisationName = (rw["OrgUnitName"] != DBNull.Value) ? rw["OrgUnitName"].ToString() : string.Empty
                      };
        }
    }
}