﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Scorecard.Interfaces;
using Scorecard.DataAccess;
using scorecard.implementations;
using Scorecard.Entities;
using System.Data;

namespace Scorecard.Logic
{
    public class CompanyActivationBL : ICompanyActivation
    {
        #region Properties

        public string ConnectionString
        {
            get { return Util.GetconnectionString(); }
        }

        public CompanyActivationDAL Dal
        {
            get { return new CompanyActivationDAL(ConnectionString); }
        }

        #endregion

        #region Default Constructors

        public CompanyActivationBL()
        {
        }

        #endregion

        #region Methods & Functions

        public bool ActivateAccount(CompanyActivation obj)
        {
            int result = Dal.Update(obj);
            return result > 0;
        }

        public CompanyActivation GetDetails(CompanyActivation obj)
        {
            DateTime? defDate = null;
            using (IDataReader data = Dal.Get(obj))
            {
                return (data.Read()) ? new CompanyActivation
                {
                    Activated = bool.Parse(data["Activated"].ToString()),
                    ActivationCode = data["ActivationCode"].ToString(),
                    ActivationDate = (data["ActivationDate"] == DBNull.Value) ? defDate : DateTime.Parse(data["ActivationDate"].ToString()),
                    CompanyId = int.Parse(data["CompanyId"].ToString()),
                    DateCreated = DateTime.Parse(data["DateCreated"].ToString()),
                    UserName = data["UserName"].ToString()

                } : new CompanyActivation();
            }

        }

        #endregion
    }
}