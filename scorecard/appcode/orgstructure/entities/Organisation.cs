﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Scorecard.Entities
{
    public class Organisation
    {
        public int OrganisationId { get; set; }
        public string OrganisationName { get; set; }
    }
}