﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Scorecard.Entities
{
    public class CompanyActivation
    {
        public int CompanyId { get; set; }
        public string ActivationCode { get; set; }
        public string UserName { get; set; }
        public bool Activated { get; set; }
        public DateTime? ActivationDate { get; set; }
        public DateTime DateCreated { get; set; }
    }
}