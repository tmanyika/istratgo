﻿using System;
using System.Collections.Generic;
using System.Data;
using HR.EmployeeTraining.Entities;

namespace HR.EmployeeTraining.Interfaces
{
    public interface ITrainingCategory
    {
        bool Add(TrainingCategory obj);
        bool Update(TrainingCategory obj);
        bool SetStatus(TrainingCategory obj);

        TrainingCategory GetById(int categoryId);
        TrainingCategory GetRow(IDataRecord rw);
        TrainingCategory GetBriefRow(IDataRecord rw);

        List<TrainingCategory> GetByStatus(int companyId, bool active);
        List<TrainingCategory> GetAllByCompanyId(int companyId);
    }
}
