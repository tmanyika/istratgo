﻿using System;
using System.Collections.Generic;
using System.Data;
using HR.EmployeeTraining.Entities;

namespace HR.EmployeeTraining.Interfaces
{
    public interface ITrainingStatus
    {
        bool Add(TrainingStatus obj);
        bool Update(TrainingStatus obj);
        bool SetStatus(TrainingStatus obj);

        TrainingStatus GetById(int leaveId);
        TrainingStatus GetRow(IDataRecord rw);
        TrainingStatus GetBriefRow(IDataRecord rw);

        List<TrainingStatus> GetByStatus(int companyId, bool active);
        List<TrainingStatus> GetAllByCompanyId(int companyId);
    }
}
