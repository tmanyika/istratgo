﻿using System;
using System.Collections.Generic;
using HR.EmployeeTraining.Entities;
using System.Data;

namespace HR.EmployeeTraining.Interfaces
{
    public interface ITrainingReport
    {
        TrainingReport GetTrainingRow(IDataRecord rw);

        List<TrainingReport> GetTrainingOrgUnitReport(int orgUnitId, int? employeeId, int? lineManagerId, DateTime startDate, DateTime endDate);
        List<TrainingReport> GetTrainingCompanyReport(int companyId, int? employeeId, int? lineManagerId, DateTime startDate, DateTime endDate);
    }
}
