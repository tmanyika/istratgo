﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Education.Accreditation
{
    public interface INQFLevel
    {
        bool Add(NQFLevel obj);
        bool Update(NQFLevel obj);
        bool SetStatus(NQFLevel obj);

        NQFLevel GetById(int nqfLevelId);
        NQFLevel GetRow(IDataRecord rw);
        NQFLevel GetBriefRow(IDataRecord rw);

        List<NQFLevel> GetByCountryId(int countryId, bool active);
        List<NQFLevel> GetByStatus(bool active);
    }
}
