﻿using System;
using System.Collections.Generic;
using System.Data;
using HR.EmployeeTraining.Entities;

namespace HR.EmployeeTraining.Interfaces
{
    public interface ITrainingCourse
    {
        bool Add(TrainingCourse obj);
        bool Update(TrainingCourse obj);
        bool SetStatus(TrainingCourse obj);

        TrainingCourse GetById(int courseId);
        TrainingCourse GetRow(IDataRecord rw);
        TrainingCourse GetBriefRow(IDataRecord rw);

        List<TrainingCourse> GetByStatus(int companyId, bool active);
        List<TrainingCourse> GetAll(int companyId);
        List<TrainingCourse> GetByCategoryId(int categoryId, bool active);
    }
}
