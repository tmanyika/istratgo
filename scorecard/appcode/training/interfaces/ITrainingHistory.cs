﻿using System;
using System.Collections.Generic;
using System.Linq;
using HR.EmployeeTraining.Entities;
using System.Data;

namespace HR.EmployeeTraining.Interfaces
{
    public interface ITrainingHistory
    {
        bool AddProgressStatus(TrainingHistory obj);
        bool SetValidStatus(TrainingHistory obj);

        TrainingHistory GetRow(IDataRecord rw);
        List<TrainingHistory> GetByStatus(int trainingId, bool valid);
    }
}
