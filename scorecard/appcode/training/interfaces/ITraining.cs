﻿using System;
using System.Collections.Generic;
using System.Data;
using HR.EmployeeTraining.Entities;

namespace HR.EmployeeTraining.Interfaces
{
    public interface ITraining
    {
        bool Add(Training obj);
        bool Update(Training obj);

        Training GetById(long trainingId);
        Training GetRow(IDataRecord rw);

        List<Training> GetByOrgUnitId(int orgunitId);
        List<Training> GetByEmployeeId(int employeeId);
        List<Training> GetByCompanyId(int companyId);
    }
}
