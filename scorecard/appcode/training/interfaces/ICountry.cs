﻿using System;
using System.Data;
using Global.Framework.Entities;

namespace Global.Framework.Interfaces
{
    public interface ICountry
    {
        Country GetBriefRow(IDataRecord rw);
    }
}
