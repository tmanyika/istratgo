﻿using System;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using HR.EmployeeTraining.Entities;

namespace HR.EmployeeTraining.DataAccess
{
    public class TrainingCourseDAL
    {
        #region Variables

        private string _conString;

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return _conString; }
            set { _conString = value; }
        }

        #endregion

        #region Default Constructors

        public TrainingCourseDAL(string conString)
        {
            _conString = conString;
        }

        #endregion

        #region Public Methods and Functions

        public IDataReader Get(int companyId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_TrainingCourse_GetByCompanyId", companyId);
        }

        public IDataReader Get(int companyId, bool active)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_TrainingCourse_GetActive", companyId, active);
        }

        public IDataReader GetCourse(int courseId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_TrainingCourse_GetById", courseId);
        }

        public int Save(TrainingCourse obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_TrainingCourse_Insert", obj.CompanyId, obj.CategoryId, obj.NqfLevelId,
                                                                                                    obj.TrainingName, obj.TrainingDescription, obj.ExamRequired,
                                                                                                    obj.Active, obj.CreatedBy);
            return result;
        }

        public int Update(TrainingCourse obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_TrainingCourse_Update", obj.CourseId, obj.CompanyId, obj.CategoryId, obj.NqfLevelId,
                                                                                                    obj.TrainingName, obj.TrainingDescription, obj.ExamRequired,
                                                                                                    obj.Active, obj.UpdatedBy);
            return result;
        }

        public int UpdateStatus(TrainingCourse obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_TrainingCourse_Deactivate", obj.CourseId, obj.Active, obj.UpdatedBy);
            return result;
        }

        public IDataReader GetByCategory(int categoryId, bool active)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_TrainingCourse_GetByCategoryId", categoryId, active);
        }

        #endregion

    }
}