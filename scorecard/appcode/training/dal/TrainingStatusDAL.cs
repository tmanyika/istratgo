﻿using System;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using HR.EmployeeTraining.Entities;

namespace HR.EmployeeTraining.DataAccess
{
    public class TrainingStatusDAL
    {
        #region Variables

        private string _conString;

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return _conString; }
            set { _conString = value; }
        }

        #endregion

        #region Default Constructors

        public TrainingStatusDAL(string conString)
        {
            _conString = conString;
        }

        #endregion

        #region Public Methods and Functions

        public IDataReader Get(int companyId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_TrainingStatus_GetByCompanyId", companyId);
        }

        public IDataReader Get(int companyId, bool active)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_TrainingStatus_GetActive", companyId, active);
        }

        public IDataReader GetById(int statusId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_TrainingStatus_GetById", statusId);
        }

        public int Save(TrainingStatus obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_TrainingStatus_Insert", obj.CompanyId, obj.StatusName, obj.StatusDescription, 
                                                                                                      obj.Active, obj.CreatedBy);
            return result;
        }

        public int Update(TrainingStatus obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_TrainingStatus_Update", obj.StatusId, obj.CompanyId, obj.StatusName, 
                                                                                                        obj.StatusDescription, obj.Active, obj.UpdatedBy);
            return result;
        }

        public int UpdateStatus(TrainingStatus obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_TrainingStatus_Deactivate", obj.StatusId, obj.Active, obj.UpdatedBy);
            return result;
        }

        #endregion
    }
}