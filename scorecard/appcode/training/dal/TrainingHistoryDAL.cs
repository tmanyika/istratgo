﻿using System;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using HR.EmployeeTraining.Entities;
using System.Data.SqlClient;
using System.IO;
using System.Data.SqlTypes;
using scorecard.implementations;

namespace HR.EmployeeTraining.DataAccess
{
    public class TrainingHistoryDAL
    {
        #region Variables

        private string _conString;

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return _conString; }
            set { _conString = value; }
        }

        #endregion

        #region Default Constructors

        public TrainingHistoryDAL(string conString)
        {
            _conString = conString;
        }

        #endregion

        #region Public Methods and Functions

        public IDataReader Get(int trainingId, bool valid)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_TrainingHistory_GetByTrainingId", trainingId, valid);
        }

        public int Save(TrainingHistory obj)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_TrainingHistory_Insert", obj.TrainingId, obj.StatusId, obj.DateAchieved, obj.Comments, obj.ValidDocument, 
                                                                                                    obj.DocumentName, obj.FileExtension, obj.FileSize, obj.MimeType, obj.CreatedBy);

            return (result != DBNull.Value) ? int.Parse(result.ToString()) : 0;
        }

        public int SetValidityStatus(TrainingHistory obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_TrainingHistory_UpdateValidityStatus", obj.TrailId, obj.TrainingId, obj.ValidDocument,
                                                                                                                    obj.ReasonForDeletion, obj.UpdatedBy);
            return result;
        }

        public int Delete(Guid trailId)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_TrainingHistory_Delete", trailId);
            return result;
        }

        #endregion
    }
}