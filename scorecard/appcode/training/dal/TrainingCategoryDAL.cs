﻿using System;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using HR.EmployeeTraining.Entities;

namespace HR.EmployeeTraining.DataAccess
{
    public class TrainingCategoryDAL
    {
        #region Variables

        private string _conString;

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return _conString; }
            set { _conString = value; }
        }

        #endregion

        #region Default Constructors

        public TrainingCategoryDAL(string conString)
        {
            _conString = conString;
        }

        #endregion

        #region Public Methods and Functions

        public IDataReader Get(int companyId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_TrainingCategory_GetByCompanyId", companyId);
        }

        public IDataReader Get(int companyId, bool active)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_TrainingCategory_GetActive", companyId, active);
        }

        public IDataReader GetById(int categoryId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_TrainingCategory_GetById", categoryId);
        }

        public int Save(TrainingCategory obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_TrainingCategory_Insert", obj.CompanyId, obj.CategoryName,
                                                                                                obj.CategoryDescription, obj.Active, obj.CreatedBy);
            return result;
        }

        public int Update(TrainingCategory obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_TrainingCategory_Update", obj.CategoryId, obj.CompanyId, obj.CategoryName,
                                                                                                obj.CategoryDescription, obj.Active, obj.UpdatedBy);
            return result;
        }

        public int UpdateStatus(TrainingCategory obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_TrainingCategory_Deactivate", obj.CategoryId, obj.Active, obj.UpdatedBy);
            return result;
        }

        #endregion
    }
}