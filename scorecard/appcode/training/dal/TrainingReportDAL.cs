﻿using System;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

namespace HR.EmployeeTraining.DataAccess
{
    public class TrainingReportDAL
    {
        #region Variables

        private string _conString;

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return _conString; }
            set { _conString = value; }
        }

        #endregion

        #region Default Constructors

        public TrainingReportDAL(string conString)
        {
            _conString = conString;
        }

        #endregion

        #region Public Methods and Functions

        public IDataReader GetOrgUnitReport(int orgUnitId, int? employeeId, int? lineManagerId, DateTime startDate, DateTime endDate)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_TrainingReport_GetForOrgUnit", orgUnitId, employeeId, lineManagerId, startDate, endDate);
        }

        public IDataReader GetCompanyReport(int companyId, int? employeeId, int? lineManagerId, DateTime startDate, DateTime endDate)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_TrainingReport_GetForCompany", companyId, employeeId, lineManagerId, startDate, endDate);
        }

        #endregion
    }
}