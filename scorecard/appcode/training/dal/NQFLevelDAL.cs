﻿using System;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

namespace Education.Accreditation
{
    public class NQFLevelDAL
    {
        #region Variables

        private string _conString;

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return _conString; }
            set { _conString = value; }
        }

        #endregion

        #region Default Constructors

        public NQFLevelDAL(string conString)
        {
            _conString = conString;
        }

        #endregion

        #region Public Methods and Functions

        public IDataReader Get(bool active)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_NQFLevel_GetActive", active);
        }

        public IDataReader Get(int countryId, bool active)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_NQFLevel_GetByCountryId", countryId, active);
        }

        public IDataReader Get(int nqfLevelId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_NQFLevel_GetById", nqfLevelId);
        }

        public int Save(NQFLevel obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_NQFLevel_Insert", obj.CountryId, obj.NqfLevelName, obj.NqfLevelDescription, obj.NqfLevelCode,
                                                                                                        obj.Active, obj.CreatedBy);
            return result;
        }

        public int Update(NQFLevel obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_NQFLevel_Update", obj.NqfLevelId, obj.CountryId, obj.NqfLevelName, obj.NqfLevelDescription,
                                                                                                        obj.NqfLevelCode, obj.Active, obj.UpdatedBy);
            return result;
        }

        public int UpdateStatus(NQFLevel obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_NQFLevel_Deactivate", obj.NqfLevelId, obj.Active, obj.UpdatedBy);
            return result;
        }

        #endregion
    }
}