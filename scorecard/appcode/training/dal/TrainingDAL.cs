﻿using System;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using HR.EmployeeTraining.Entities;

namespace HR.EmployeeTraining.DataAccess
{
    public class TrainingDAL
    {
        #region Variables

        private string _conString;

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return _conString; }
            set { _conString = value; }
        }

        #endregion

        #region Default Constructors

        public TrainingDAL(string conString)
        {
            _conString = conString;
        }

        #endregion

        #region Public Methods and Functions

        public IDataReader GetAll(int companyId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_Training_GetByCompanyId", companyId);
        }

        public IDataReader GetForEmployee(int employeeId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_Training_GetByEmployeeId", employeeId);
        }

        public IDataReader Get(int orgUnitId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_Training_GetByOrgUnitId", orgUnitId);
        }

        public IDataReader GetTraining(long trainingId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_Training_GetById", trainingId);
        }

        public int Save(Training obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_Training_Insert", obj.EmployeeId, obj.CourseId, obj.RelatedGoalId,
                                                                                                    obj.ReasonForTraining, obj.StartDate, obj.EndDate, obj.StatusId,
                                                                                                    obj.CreatedBy);
            return result;
        }

        public int Update(Training obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_Training_Update", obj.TrainingId, obj.EmployeeId, obj.CourseId, obj.RelatedGoalId,
                                                                                                    obj.ReasonForTraining, obj.StartDate, obj.EndDate, obj.StatusId,
                                                                                                    obj.UpdatedBy);
            return result;
        }

        #endregion
    }
}