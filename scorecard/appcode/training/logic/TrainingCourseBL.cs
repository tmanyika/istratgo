﻿using System;
using System.Collections.Generic;
using System.Data;
using scorecard.implementations;
using HR.EmployeeTraining.Interfaces;
using HR.EmployeeTraining.Entities;
using HR.EmployeeTraining.DataAccess;
using Education.Accreditation;

namespace HR.EmployeeTraining.Logic
{
    public class TrainingCourseBL : ITrainingCourse
    {
        #region Properties

        public string ConnectionString
        {
            get { return Util.GetconnectionString(); }
        }

        public TrainingCourseDAL Dal
        {
            get { return new TrainingCourseDAL(ConnectionString); }
        }

        readonly ITrainingCategory cat;
        readonly INQFLevel nqf;

        #endregion

        #region Default Constructors

        public TrainingCourseBL()
        {
            cat = new TrainingCategoryBL();
            nqf = new NQFLevelBL();
        }

        #endregion

        #region Methods & Functions

        public bool Add(TrainingCourse obj)
        {
            int result = Dal.Save(obj);
            return result > 0 ? true : false;
        }

        public bool Update(TrainingCourse obj)
        {
            int result = Dal.Update(obj);
            return result > 0 ? true : false;
        }

        public bool SetStatus(TrainingCourse obj)
        {
            int result = Dal.UpdateStatus(obj);
            return result > 0 ? true : false;
        }

        public List<TrainingCourse> GetByStatus(int companyId, bool active)
        {
            List<TrainingCourse> obj = new List<TrainingCourse>();
            using (IDataReader rw = Dal.Get(companyId, active))
            {
                while (rw.Read())
                    obj.Add(GetRow(rw));
            }
            return obj;
        }

        public List<TrainingCourse> GetByCategoryId(int categoryId, bool active)
        {
            List<TrainingCourse> obj = new List<TrainingCourse>();
            using (IDataReader rw = Dal.GetByCategory(categoryId, active))
            {
                while (rw.Read())
                    obj.Add(GetRow(rw));
            }
            return obj;
        }

        public List<TrainingCourse> GetAll(int companyId)
        {
            List<TrainingCourse> obj = new List<TrainingCourse>();
            using (IDataReader rw = Dal.Get(companyId))
            {
                while (rw.Read())
                    obj.Add(GetRow(rw));
            }
            return obj;
        }

        public TrainingCourse GetById(int courseId)
        {
            using (IDataReader rw = Dal.GetCourse(courseId))
            {
                return (rw.Read()) ? GetRow(rw) : new TrainingCourse();
            }
        }

        public TrainingCourse GetRow(IDataRecord rw)
        {
            int? defInt = null;
            DateTime? defDate = null;

            return new TrainingCourse
            {
                Active = bool.Parse(rw["Active"].ToString()),
                CreatedBy = rw["CreatedBy"].ToString(),
                Cat = cat.GetBriefRow(rw),
                CategoryId = int.Parse(rw["CategoryId"].ToString()),
                CompanyId = int.Parse(rw["CompanyId"].ToString()),
                CourseId = int.Parse(rw["CourseId"].ToString()),
                DateCreated = DateTime.Parse(rw["DateCreated"].ToString()),
                DateUpdated = (rw["DateUpdated"] != DBNull.Value) ? DateTime.Parse(rw["DateUpdated"].ToString()) : defDate,
                ExamRequired = bool.Parse(rw["ExamRequired"].ToString()),
                Nqf = nqf.GetBriefRow(rw),
                NqfLevelId = (rw["NqfLevelId"] != DBNull.Value) ? int.Parse(rw["NqfLevelId"].ToString()) : defInt,
                TrainingName = rw["TrainingName"].ToString(),
                TrainingDescription = (rw["TrainingDescription"] != DBNull.Value) ? rw["TrainingDescription"].ToString() : string.Empty,
                UpdatedBy = (rw["UpdatedBy"] != DBNull.Value) ? rw["UpdatedBy"].ToString() : string.Empty
            };
        }

        public TrainingCourse GetBriefRow(IDataRecord rw)
        {
            return new TrainingCourse
            {
                CourseId = (rw["CourseId"] != DBNull.Value) ? int.Parse(rw["CourseId"].ToString()) : 0,
                TrainingName = (rw["TrainingName"] != DBNull.Value) ? rw["TrainingName"].ToString() : string.Empty
            };
        }

        #endregion
    }
}