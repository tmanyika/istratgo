﻿using System;
using System.Collections.Generic;
using System.Data;
using scorecard.implementations;
using Global.Framework.Interfaces;
using Global.Framework.Logic;

namespace Education.Accreditation
{
    public class NQFLevelBL : INQFLevel
    {
        #region Properties

        public string ConnectionString
        {
            get { return Util.GetconnectionString(); }
        }

        public ICountry Cl
        {
            get { return new CountryBL(); }
        }

        public NQFLevelDAL Dal
        {
            get { return new NQFLevelDAL(ConnectionString); }
        }

        #endregion

        #region Default Constructors

        public NQFLevelBL() { }

        #endregion

        #region Methods & Functions

        public bool Add(NQFLevel obj)
        {
            int result = Dal.Save(obj);
            return result > 0 ? true : false;
        }

        public bool Update(NQFLevel obj)
        {
            int result = Dal.Update(obj);
            return result > 0 ? true : false;
        }

        public bool SetStatus(NQFLevel obj)
        {
            int result = Dal.UpdateStatus(obj);
            return result > 0 ? true : false;
        }

       public List<NQFLevel> GetByStatus(bool active)
        {
            List<NQFLevel> obj = new List<NQFLevel>();
            using (IDataReader rw = Dal.Get(active))
            {
                while (rw.Read())
                    obj.Add(GetRow(rw));
            }
            return obj;
        }

        public List<NQFLevel> GetByCountryId(int countryId, bool active)
        {
            List<NQFLevel> obj = new List<NQFLevel>();
            using (IDataReader rw = Dal.Get(countryId, active))
            {
                while (rw.Read())
                    obj.Add(GetRow(rw));
            }
            return obj;
        }

        public NQFLevel GetById(int nqfLevelId)
        {
            using (IDataReader rw = Dal.Get(nqfLevelId))
            {
                return (rw.Read()) ? GetRow(rw) : new NQFLevel();
            }
        }

        public NQFLevel GetRow(IDataRecord rw)
        {
            DateTime? defDate = null;
            return new NQFLevel
            {
                Active = bool.Parse(rw["Active"].ToString()),
                CountryId = int.Parse(rw["CountryId"].ToString()),
                CreatedBy = rw["CreatedBy"].ToString(),
                DateCreated = DateTime.Parse(rw["DateCreated"].ToString()),
                DateUpdated = (rw["DateUpdated"] != DBNull.Value) ? DateTime.Parse(rw["DateUpdated"].ToString()) : defDate,
                Nation = Cl.GetBriefRow(rw),
                NqfLevelCode = (rw["NqfLevelCode"] != DBNull.Value) ? rw["NqfLevelCode"].ToString() : string.Empty,
                NqfLevelDescription = (rw["NqfLevelDescription"] != DBNull.Value) ? rw["NqfLevelDescription"].ToString() : string.Empty,
                NqfLevelId = int.Parse(rw["NqfLevelId"].ToString()),
                NqfLevelName = rw["NqfLevelName"].ToString(),
                UpdatedBy = (rw["UpdatedBy"] != DBNull.Value) ? rw["UpdatedBy"].ToString() : string.Empty
            };
        }

        public NQFLevel GetBriefRow(IDataRecord rw)
        {
            return new NQFLevel
            {
                NqfLevelId = (rw["NqfLevelId"] != DBNull.Value) ? int.Parse(rw["NqfLevelId"].ToString()) : 0,
                NqfLevelName = (rw["NqfLevelName"] != DBNull.Value) ? rw["NqfLevelName"].ToString() : string.Empty
            };
        }

        #endregion
    }
}