﻿using System;
using System.Collections.Generic;
using System.Data;
using scorecard.implementations;
using HR.EmployeeTraining.Interfaces;
using HR.EmployeeTraining.Entities;
using HR.EmployeeTraining.DataAccess;
using Education.Accreditation;
using Scorecard.Interfaces;
using Scorecard.Logic;
using HR.Human.Resources;
using Scorecard.Entities;
using vb = Microsoft.VisualBasic.Information;

namespace HR.EmployeeTraining.Logic
{
    public class TrainingBL : ITraining
    {
        #region Properties

        public string ConnectionString
        {
            get { return Util.GetconnectionString(); }
        }

        public TrainingDAL Dal
        {
            get { return new TrainingDAL(ConnectionString); }
        }

        readonly ITrainingCourse cos;
        readonly IPerformanceMeasure mes;
        readonly ITrainingStatus stat;
        readonly IEmployee emp;
        readonly IOrganisation org;
        readonly ITrainingCategory cat;

        #endregion

        #region Default Constructors

        public TrainingBL()
        {
            cos = new TrainingCourseBL();
            mes = new PerformanceMeasureBL();
            emp = new EmployeeBL();
            org = new OrganisationBL();
            cat = new TrainingCategoryBL();
            stat = new TrainingStatusBL();
        }

        #endregion

        #region Methods & Functions

        public bool Add(Training obj)
        {
            int result = Dal.Save(obj);
            return result > 0 ? true : false;
        }

        public bool Update(Training obj)
        {
            int result = Dal.Update(obj);
            return result > 0 ? true : false;
        }

        public List<Training> GetByEmployeeId(int employeeId)
        {
            List<Training> obj = new List<Training>();
            using (IDataReader rw = Dal.GetForEmployee(employeeId))
            {
                while (rw.Read())
                    obj.Add(GetRow(rw));
            }
            return obj;
        }

        public List<Training> GetByOrgUnitId(int orgunitId)
        {
            List<Training> obj = new List<Training>();
            using (IDataReader rw = Dal.Get(orgunitId))
            {
                while (rw.Read())
                    obj.Add(GetRow(rw));
            }
            return obj;
        }

        public List<Training> GetByCompanyId(int companyId)
        {
            List<Training> obj = new List<Training>();
            using (IDataReader rw = Dal.GetAll(companyId))
            {
                while (rw.Read())
                    obj.Add(GetRow(rw));
            }
            return obj;
        }

        public Training GetById(long trainingId)
        {
            using (IDataReader rw = Dal.GetTraining(trainingId))
            {
                return (rw.Read()) ? GetRow(rw) : new Training();
            }
        }

        public Training GetRow(IDataRecord rw)
        {
            int? defInt = null;
            DateTime? defDate = null;

            return new Training
            {
                Cat = cat.GetBriefRow(rw),
                Course = cos.GetBriefRow(rw),
                CourseId = int.Parse(rw["CourseId"].ToString()),
                CreatedBy = rw["CreatedBy"].ToString(),
                DateCreated = DateTime.Parse(rw["DateCreated"].ToString()),
                DateUpdated = (rw["DateUpdated"] != DBNull.Value) ? DateTime.Parse(rw["DateUpdated"].ToString()) : defDate,
                Emp = emp.GetEmployeeNameRow(rw),
                EmployeeId = int.Parse(rw["EmployeeId"].ToString()),
                EndDate = DateTime.Parse(rw["EndDate"].ToString()),
                Measure = mes.GetBriefRow(rw),
                OrgUnit = org.GetBriefRow(rw),
                ReasonForTraining = (rw["ReasonForTraining"] != DBNull.Value) ? rw["ReasonForTraining"].ToString() : string.Empty,
                RelatedGoalId = (rw["RelatedGoalId"] != DBNull.Value) ? int.Parse(rw["RelatedGoalId"].ToString()) : defInt,
                StartDate = DateTime.Parse(rw["StartDate"].ToString()),
                Status = stat.GetBriefRow(rw),
                StatusId = (rw["StatusId"] != DBNull.Value) ? int.Parse(rw["StatusId"].ToString()) : defInt,
                TrainingId = int.Parse(rw["TrainingId"].ToString()),
                UpdatedBy = (rw["UpdatedBy"] != DBNull.Value) ? rw["UpdatedBy"].ToString() : string.Empty
            };
        }

        #endregion
    }
}