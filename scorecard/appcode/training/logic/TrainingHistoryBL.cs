﻿using System;
using System.Collections.Generic;
using System.Data;
using scorecard.implementations;
using HR.EmployeeTraining.Interfaces;
using HR.EmployeeTraining.DataAccess;
using HR.EmployeeTraining.Entities;

namespace HR.EmployeeTraining.Logic
{
    public class TrainingHistoryBL : ITrainingHistory
    {
        #region Properties

        public string ConnectionString
        {
            get { return Util.GetconnectionString(); }
        }

        public TrainingHistoryDAL Dal
        {
            get { return new TrainingHistoryDAL(ConnectionString); }
        }

        #endregion

        #region Default Constructors

        public TrainingHistoryBL() { }

        #endregion

        #region Methods & Functions

        public bool AddProgressStatus(TrainingHistory obj)
        {
            int result = Dal.Save(obj);
            return result > 0 ? true : false;
        }

        public bool SetValidStatus(TrainingHistory obj)
        {
            int result = Dal.SetValidityStatus(obj);
            return result > 0 ? true : false;
        }

        public List<TrainingHistory> GetByStatus(int trainingId, bool valid)
        {
            List<TrainingHistory> obj = new List<TrainingHistory>();
            using (IDataReader rw = Dal.Get(trainingId, valid))
            {
                while (rw.Read())
                {
                    obj.Add(GetRow(rw));
                }
            }
            return obj;
        }

        public TrainingHistory GetRow(IDataRecord rw)
        {
            DateTime? defDate = null;
            return new TrainingHistory
            {
                Comments = (rw["Comments"] != DBNull.Value) ? rw["Comments"].ToString() : string.Empty,
                Course = new TrainingCourse
                {
                    TrainingName = (rw["TrainingName"] != DBNull.Value) ? rw["TrainingName"].ToString() : string.Empty
                },
                CreatedBy = rw["CreatedBy"].ToString(),
                DateAchieved = (rw["DateAchieved"] != DBNull.Value) ? DateTime.Parse(rw["DateAchieved"].ToString()) : defDate,
                DateCreated = DateTime.Parse(rw["DateCreated"].ToString()),
                DateUpdated = (rw["DateUpdated"] != DBNull.Value) ? DateTime.Parse(rw["DateUpdated"].ToString()) : defDate,
                DocumentName = (rw["DocumentName"] != DBNull.Value) ? rw["DocumentName"].ToString() : string.Empty,
                FileExtension = (rw["FileExtension"] != DBNull.Value) ? rw["FileExtension"].ToString() : string.Empty,
                FileSize = long.Parse(rw["FileSize"].ToString()),
                MimeType = (rw["MimeType"] != DBNull.Value) ? rw["MimeType"].ToString() : string.Empty,
                Status = new TrainingStatus
                {
                    StatusName = (rw["StatusName"] != DBNull.Value) ? rw["StatusName"].ToString() : string.Empty
                },
                StatusId = int.Parse(rw["StatusId"].ToString()),
                TrailId = new Guid(rw["TrailId"].ToString()),
                TrainingId = int.Parse(rw["TrainingId"].ToString()),
                UpdatedBy = (rw["UpdatedBy"] != DBNull.Value) ? rw["UpdatedBy"].ToString() : string.Empty
            };
        }

        #endregion
    }
}