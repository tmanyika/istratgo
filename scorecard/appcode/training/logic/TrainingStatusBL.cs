﻿using System;
using System.Collections.Generic;
using System.Data;
using scorecard.implementations;
using HR.EmployeeTraining.Interfaces;
using HR.EmployeeTraining.DataAccess;
using HR.EmployeeTraining.Entities;

namespace HR.EmployeeTraining.Logic
{
    public class TrainingStatusBL : ITrainingStatus
    {
        #region Properties

        public string ConnectionString
        {
            get { return Util.GetconnectionString(); }
        }

        public TrainingStatusDAL Dal
        {
            get { return new TrainingStatusDAL(ConnectionString); }
        }

        #endregion

        #region Default Constructors

        public TrainingStatusBL() { }

        #endregion

        #region Methods & Functions

        public bool Add(TrainingStatus obj)
        {
            int result = Dal.Save(obj);
            return result > 0 ? true : false;
        }

        public bool Update(TrainingStatus obj)
        {
            int result = Dal.Update(obj);
            return result > 0 ? true : false;
        }

        public bool SetStatus(TrainingStatus obj)
        {
            int result = Dal.UpdateStatus(obj);
            return result > 0 ? true : false;
        }

        public List<TrainingStatus> GetByStatus(int companyId, bool active)
        {
            List<TrainingStatus> obj = new List<TrainingStatus>();
            using (IDataReader rw = Dal.Get(companyId, active))
            {
                while (rw.Read())
                    obj.Add(GetRow(rw));
            }
            return obj;
        }

        public List<TrainingStatus> GetAllByCompanyId(int companyId)
        {
            List<TrainingStatus> obj = new List<TrainingStatus>();
            using (IDataReader rw = Dal.Get(companyId))
            {
                while (rw.Read())
                    obj.Add(GetRow(rw));
            }
            return obj;
        }

        public TrainingStatus GetById(int statusId)
        {
            using (IDataReader rw = Dal.Get(statusId))
            {
                return (rw.Read()) ? GetRow(rw) : new TrainingStatus();
            }
        }

        public TrainingStatus GetRow(IDataRecord rw)
        {
            DateTime? defDate = null;
            return new TrainingStatus
            {
                Active = bool.Parse(rw["Active"].ToString()),
                CompanyId = int.Parse(rw["CompanyId"].ToString()),
                CreatedBy = rw["CreatedBy"].ToString(),
                DateCreated = DateTime.Parse(rw["DateCreated"].ToString()),
                DateUpdated = (rw["DateUpdated"] != DBNull.Value) ? DateTime.Parse(rw["DateUpdated"].ToString()) : defDate,
                StatusId = int.Parse(rw["StatusId"].ToString()),
                StatusName = rw["StatusName"].ToString(),
                StatusDescription= (rw["StatusDescription"] != DBNull.Value) ? rw["StatusDescription"].ToString() : string.Empty,
                UpdatedBy = (rw["UpdatedBy"] != DBNull.Value) ? rw["UpdatedBy"].ToString() : string.Empty
            };
        }

        public TrainingStatus GetBriefRow(IDataRecord rw)
        {
            return new TrainingStatus
            {
                StatusId = (rw["StatusId"] != DBNull.Value) ? int.Parse(rw["StatusId"].ToString()) : 0,
                StatusName = (rw["StatusName"] != DBNull.Value) ? rw["StatusName"].ToString() : string.Empty
            };
        }

        #endregion
    }
}