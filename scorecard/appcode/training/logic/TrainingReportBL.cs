﻿using System;
using System.Collections.Generic;
using HR.EmployeeTraining.Interfaces;
using HR.EmployeeTraining.DataAccess;
using scorecard.implementations;
using HR.EmployeeTraining.Entities;
using System.Data;

namespace HR.EmployeeTraining.Logic
{
    public class TrainingReportBL : ITrainingReport
    {
        #region Properties

        public string ConnectionString
        {
            get { return Util.GetconnectionString(); }
        }

        public TrainingReportDAL Dal
        {
            get { return new TrainingReportDAL(ConnectionString); }
        }

        #endregion

        #region Default Constructors

        public TrainingReportBL() { }

        #endregion

        #region Public Methods & Functions

        public TrainingReport GetTrainingRow(IDataRecord rw)
        {
            decimal? defDec = null;
            return new TrainingReport
            {
                CategoryName = rw["CategoryName"].ToString(),
                EndDate = DateTime.Parse(rw["EndDate"].ToString()),
                FirstName = rw["FirstName"].ToString(),
                LastName = rw["LastName"].ToString(),
                LatestRating = rw["LatestRating"] != DBNull.Value ? Math.Round(decimal.Parse(rw["LatestRating"].ToString()), 2) : defDec,
                MiddleName = rw["MiddleName"] != DBNull.Value ? rw["MiddleName"].ToString() : string.Empty,
                OrgUnitName = rw["OrgUnitName"].ToString(),
                PerformanceMeasureName = rw["PerformanceMeasureName"] != DBNull.Value ? rw["PerformanceMeasureName"].ToString() : string.Empty,
                ReasonForTraining = rw["ReasonForTraining"] != DBNull.Value ? rw["ReasonForTraining"].ToString() : string.Empty,
                StartDate = DateTime.Parse(rw["StartDate"].ToString()),
                StatusName = rw["StatusName"] != DBNull.Value ? rw["StatusName"].ToString() : string.Empty,
                TrainingName = rw["TrainingName"] != DBNull.Value ? rw["TrainingName"].ToString() : string.Empty
            };
        }

        public List<TrainingReport> GetTrainingOrgUnitReport(int orgUnitId, int? employeeId, int? lineManagerId, DateTime startDate, DateTime endDate)
        {
            List<TrainingReport> data = new List<TrainingReport>();
            using (IDataReader item = Dal.GetOrgUnitReport(orgUnitId, employeeId, lineManagerId, startDate, endDate))
            {
                while (item.Read())
                {
                    data.Add(GetTrainingRow(item));
                }
            }
            return data;
        }

        public List<TrainingReport> GetTrainingCompanyReport(int companyId, int? employeeId, int? lineManagerId, DateTime startDate, DateTime endDate)
        {
            List<TrainingReport> data = new List<TrainingReport>();
            using (IDataReader item = Dal.GetCompanyReport(companyId, employeeId, lineManagerId,startDate, endDate))
            {
                while (item.Read())
                {
                    data.Add(GetTrainingRow(item));
                }
            }
            return data;
        }

        #endregion
    }
}