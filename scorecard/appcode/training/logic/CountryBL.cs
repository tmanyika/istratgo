﻿using System;
using System.Collections.Generic;
using Global.Framework.Interfaces;
using System.Data;
using Global.Framework.Entities;

namespace Global.Framework.Logic
{
    public class CountryBL : ICountry
    {
        public Country GetBriefRow(IDataRecord rw)
        {
            return new Country
            {
                CountryId = (rw["CountryId"] != DBNull.Value) ? int.Parse(rw["CountryId"].ToString()) : 0,
                CountryName = (rw["CountryName"] != DBNull.Value) ? rw["CountryName"].ToString() : string.Empty
            };
        }
    }
}