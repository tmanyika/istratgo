﻿using System;
using System.Collections.Generic;
using System.Data;
using scorecard.implementations;
using HR.EmployeeTraining.Interfaces;
using HR.EmployeeTraining.Entities;
using HR.EmployeeTraining.DataAccess;

namespace HR.EmployeeTraining.Logic
{
    public class TrainingCategoryBL : ITrainingCategory
    {
        #region Properties

        public string ConnectionString
        {
            get { return Util.GetconnectionString(); }
        }

        public TrainingCategoryDAL Dal
        {
            get { return new TrainingCategoryDAL(ConnectionString); }
        }

        #endregion

        #region Default Constructors

        public TrainingCategoryBL() { }

        #endregion

        #region Methods & Functions

        public bool Add(TrainingCategory obj)
        {
            int result = Dal.Save(obj);
            return result > 0 ? true : false;
        }

        public bool Update(TrainingCategory obj)
        {
            int result = Dal.Update(obj);
            return result > 0 ? true : false;
        }

        public bool SetStatus(TrainingCategory obj)
        {
            int result = Dal.UpdateStatus(obj);
            return result > 0 ? true : false;
        }

        public List<TrainingCategory> GetByStatus(int companyId, bool active)
        {
            List<TrainingCategory> obj = new List<TrainingCategory>();
            using (IDataReader rw = Dal.Get(companyId, active))
            {
                while (rw.Read())
                    obj.Add(GetRow(rw));
            }
            return obj;
        }

        public List<TrainingCategory> GetAllByCompanyId(int companyId)
        {
            List<TrainingCategory> obj = new List<TrainingCategory>();
            using (IDataReader rw = Dal.Get(companyId))
            {
                while (rw.Read())
                    obj.Add(GetRow(rw));
            }
            return obj;
        }

        public TrainingCategory GetById(int categoryId)
        {
            using (IDataReader rw = Dal.Get(categoryId))
            {
                return (rw.Read()) ? GetRow(rw) : new TrainingCategory();
            }
        }

        public TrainingCategory GetRow(IDataRecord rw)
        {
            DateTime? defDate = null;
            return new TrainingCategory
            {
                Active = bool.Parse(rw["Active"].ToString()),
                CompanyId = int.Parse(rw["CompanyId"].ToString()),
                CreatedBy = rw["CreatedBy"].ToString(),
                DateCreated = DateTime.Parse(rw["DateCreated"].ToString()),
                DateUpdated = (rw["DateUpdated"] != DBNull.Value) ? DateTime.Parse(rw["DateUpdated"].ToString()) : defDate,
                CategoryId = int.Parse(rw["CategoryId"].ToString()),
                CategoryName = rw["CategoryName"].ToString(),
                CategoryDescription = (rw["CategoryDescription"] != DBNull.Value) ? rw["CategoryDescription"].ToString() : string.Empty,
                UpdatedBy = (rw["UpdatedBy"] != DBNull.Value) ? rw["UpdatedBy"].ToString() : string.Empty
            };
        }

        public TrainingCategory GetBriefRow(IDataRecord rw)
        {
            return new TrainingCategory
            {
                CategoryId = (rw["CategoryId"] != DBNull.Value) ? int.Parse(rw["CategoryId"].ToString()) : 0,
                CategoryName = (rw["CategoryName"] != DBNull.Value) ? rw["CategoryName"].ToString() : string.Empty
            };
        }

        #endregion
    }
}