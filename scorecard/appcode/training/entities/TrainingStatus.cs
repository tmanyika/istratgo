﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.EmployeeTraining.Entities
{
    public class TrainingStatus
    {
        public int StatusId { get; set; }
        public int CompanyId { get; set; }

        public bool Active { get; set; }

        public string StatusName { get; set; }
        public string StatusDescription { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }

    }
}