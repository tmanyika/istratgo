﻿using System;

namespace HR.EmployeeTraining.Entities
{
    public class TrainingReport
    {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string CategoryName { get; set; }
        public string TrainingName { get; set; }
        public string StatusName { get; set; }
        public string OrgUnitName { get; set; }
        public string ReasonForTraining { get; set; }
        public string PerformanceMeasureName { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public decimal? LatestRating { get; set; }

    }
}