﻿using System;
using System.Collections.Generic;

namespace HR.EmployeeTraining.Entities
{
    public class TrainingCategory
    {
        public int CategoryId { get; set; }
        public int CompanyId { get; set; }

        public bool Active { get; set; }

        public string CategoryName { get; set; }
        public string CategoryDescription { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }

    }
}