﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Global.Framework.Entities;

namespace Education.Accreditation
{
    public class NQFLevel
    {
        public int NqfLevelId { get; set; }
        public int CountryId { get; set; }
        public bool Active { get; set; }

        public string NqfLevelName { get; set; }
        public string NqfLevelCode { get; set; } 
        public string NqfLevelDescription { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }

        public Country Nation { get; set; }

    }
}