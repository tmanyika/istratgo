﻿using System;
using Education.Accreditation;

namespace HR.EmployeeTraining.Entities
{
    public class TrainingCourse
    {
        public int CourseId { get; set; }
        public int CompanyId { get; set; }
        public int CategoryId { get; set; }
        public int? NqfLevelId { get; set; }

        public string TrainingName { get; set; }
        public string TrainingDescription { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public bool ExamRequired { get; set; }
        public bool Active { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }

        public TrainingCategory Cat { get; set; }
        public NQFLevel Nqf { get; set; }
    }
}