﻿using System;

namespace HR.EmployeeTraining.Entities
{
    public class TrainingHistory
    {
        public Guid TrailId { get; set; }

        public int TrainingId { get; set; }
        public int StatusId { get; set; }

        public long FileSize { get; set; }

        public bool ValidDocument { get; set; }

        public string Comments { get; set; }
        public string ReasonForDeletion { get; set; }
        public string DocumentName { get; set; }
        public string FileExtension { get; set; }
        public string MimeType { get; set; }

        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime? DateAchieved { get; set; }
        public DateTime? DateUpdated { get; set; }

        public TrainingStatus Status { get; set; }
        public TrainingCourse Course { get; set; }
    }
}