﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Global.Framework.Entities
{
    public class Country
    {
        public int CountryId { get; set; }

        public string CountryName { get; set; }
        public string CountryCode { get; set; }
        public string InternationalDialingCode { get; set; }

        public bool Active { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
    }
}