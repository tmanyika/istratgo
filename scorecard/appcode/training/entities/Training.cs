﻿using System;
using Education.Accreditation;
using Scorecard.Entities;
using HR.Human.Resources;

namespace HR.EmployeeTraining.Entities
{
    public class Training
    {
        public int TrainingId { get; set; }
        public int EmployeeId { get; set; }
        public int CourseId { get; set; }
        public int? RelatedGoalId { get; set; }
        public int? StatusId { get; set; }

        public string ReasonForTraining { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }

        public PerformanceMeasure Measure { get; set; }
        public TrainingCourse Course { get; set; }
        public TrainingStatus Status { get; set; }
        public Employee Emp { get; set; }
        public Organisation OrgUnit { get; set; }
        public TrainingCategory Cat { get; set; }
    }
}