﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using scorecard.implementations;
using HR.Human.Resources;

namespace System.Email.Communication
{
    public class MailRelay
    {
        public int MailId { get; set; }

        public string MailContent { get; set; }
        public string MailSubject { get; set; }

        public object[] Data { get; set; }
        public object[][] UData { get; set; }
        public object[] SubjectData { get; set; }

        public MailServer Server { get; set; }
        public SmtpClient MailClient { get; set; }
        public MailAddress Sender { get; set; }
        public MailAddressCollection Recipient { get; set; }
        public List<Employee> AddressList { get; set; }
    }
}