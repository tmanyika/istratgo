﻿using System;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

namespace System.Email.Communication
{
    public class MailServerDAL
    {
        #region Variables

        private string _conString;

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return _conString; }
            set { _conString = value; }
        }

        #endregion

        #region Default Constructors

        public MailServerDAL(string conString)
        {
            _conString = conString;
        }

        #endregion

        #region Public Methods & Functions

        public DataTable GetAll()
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_MailServer_GetAll").Tables[0];
        }

        public DataTable Get(bool active)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_MailServer_GetActive", active).Tables[0];
        }

        public DataTable GetById(int accountId)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_MailServer_GetById", accountId).Tables[0];
        }

        public int Save(MailServer obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_MailServer_Insert", obj.AccountMailId, obj.AccountName,
                                                        obj.SqlProfileName, obj.UserName, obj.PIN, obj.SmtpServer, 
                                                        obj.Port, obj.Active, obj.CreatedBy);
            return result;
        }

        //public int Update(MailServer obj)
        //{
        //    int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_MailServer_Insert", obj.AccountMailId,
        //                                                 obj.AccountName, obj.SqlProfileName, obj.UserName, obj.PIN, 
        //                                                 obj.SmtpServer, obj.Port, obj.Active, obj.UpdatedBy);
        //    return result;
        //}

        public int Deactivate(int accountId, string updatedBy)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_MailServer_Deactivate", accountId, updatedBy);
            return result;
        }

        #endregion
    }
}