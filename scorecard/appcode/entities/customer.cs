﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
/* @Copy Albertoncaffeine.net */
namespace scorecard.entities
{
    public class customer : useradmin
    {

        public int ACCOUNT_NO { get; set; }
        public int SETUP_TYPE { get; set; }
        public string PASSWORD { get; set; }
        public string FIRST_NAME { get; set; }
        public string LAST_NAME { get; set; }
        public string CELL_NUMBER { get; set; }
        public string USER_NAME { get; set; }
        public DateTime? DATE_OF_BIRTH { get; set; }
        public string COMPANY_NAME { get; set; }
        public string JOB_TITLE_NAME { get; set; }

    }
}