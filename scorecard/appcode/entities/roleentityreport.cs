﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/* @Copy Albertoncaffeine.net */

namespace scorecard.entities
{
    public class roleentityreport
    {
        public int CRITERIA_TYPE_ID;
        public string SCORES;
        public double FINAL_SCORE;
        public int AREA_TYPE_ID;
        public int AREA_ID;
        public string GOAL_DESCRIPTION;
        public string TARGET;
        public int WEIGHT;
        public string OBJECTIVE;
        public string NAME;
        public string EMAIL_ADDRESS;
        public string JOB_TITLE;
        public int USER_ID;
        public int GOAL_ID;
        public int STRATEGIC;
    }
}