﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Scorecard.Entities;
/* @Copy Albertoncaffeine.net */
namespace scorecard.entities
{
    public class company
    {
        public int COMPANY_ID { get; set; }
        public int ACCOUNT_NO { get; set; }
        public int? INDUSTRY { get; set; }
        public int? COUNTRY { get; set; }

        public string COMPANY_NAME { get; set; }
        public string REGISTRATION_NO { get; set; }
        public string VAT_NO { get; set; }
        public string ADDRESS { get; set; }
        public string CONTACT_NO { get; set; }

        public bool? ACTIVE { get; set; }
        public bool PAID { get; set; }

        public DateTime DATE_REGISTERED { get; set; }
    }
}