﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
/* @Copy Albertoncaffeine.net */
namespace scorecard.entities
{
    public class JobTitle
    {
        public int ID { get; set; }
        public int COMPANY_ID { get; set; }
        public string NAME { get; set; }
        public string CONTRACT_FILE { get; set; }
        public string CREATEDBY { get; set; }
        public bool ACTIVE { get; set; }
        public DateTime? DATECREATED { get; set; }
    }
}