﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/* @Copy Albertoncaffeine.net */

namespace scorecard.entities
{
    public class menu {

        public int MENU_ID { get; set; }
        public int PARENT_MENU_ID { get; set; }
        public string MENU_NAME { get; set; }
        public string IMAGE_ICON { get; set; }
        public string MENU_PATH { get; set; }
        public int ORDER_ID { get; set; }
        public bool ACTIVE { get; set; }
        public int POSITION { get; set; }
    }

    public class menuaccess : menu  {
        public int ID { get; set; }
        public int USER_TYPE_ID { get; set; }
        public int COMPANY_ID { get; set; }
        public DateTime? DATE_CREATED { get; set; }
    }
}