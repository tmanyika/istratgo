﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using scorecard.interfaces;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using scorecard.entities;
using vb = Microsoft.VisualBasic;

/* @Copy Albertoncaffeine.net */

namespace scorecard.implementations
{
    public class jobtitleImpl : Ijobtitles
    {
        #region Ijobtitles Members

        public bool deleteContract(int id)
        {
            int result = (int)SqlHelper.ExecuteScalar(Util.GetconnectionString(), "PROC_DELETE_JOB_TITLE_CONTRACT", id);
            return result > 0;
        }

        public int addJobTitle(JobTitle _title)
        {
            try
            {
                object result = SqlHelper.ExecuteScalar(Util.GetconnectionString(), "PROC_ADD_JOB_TITLES", _title.COMPANY_ID, _title.NAME, _title.CONTRACT_FILE,
                                                                                                            _title.ACTIVE, _title.CREATEDBY);
                return (result != DBNull.Value) ? int.Parse(result.ToString()) : 0;
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return 0;
            }
        }

        public int updateJobTitle(JobTitle _title)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try
            {
                object result = SqlHelper.ExecuteScalar(Util.GetconnectionString(), "PROC_UPDATE_JOB_TITLE", _title.ID, _title.NAME, _title.CONTRACT_FILE,
                                                                                                            _title.ACTIVE, _title.CREATEDBY);
                return (result != DBNull.Value) ? int.Parse(result.ToString()) : 0;
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return 0;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public int deleteJobTitle(JobTitle _title)
        {
            try
            {
                object result = SqlHelper.ExecuteScalar(Util.GetconnectionString(), "PROC_DELETE_JOB_TITLE", _title.ID);
                return (result != DBNull.Value) ? int.Parse(result.ToString()) : 0;
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return 0;
            }
        }

        public List<JobTitle> getAllCompanyJobTitles(company _company)
        {
            try
            {
                DateTime? defDate = null;
                List<JobTitle> jobtitles = new List<JobTitle>();
                using (SqlDataReader read = SqlHelper.ExecuteReader(Util.GetconnectionString(), "PROC_GET_ALL_TITLES_BY_COMPANY", _company.COMPANY_ID))
                {
                    while (read.Read())
                    {
                        jobtitles.Add(new JobTitle
                        {
                            ID = int.Parse(read["ID"].ToString()),
                            NAME = vb.Strings.StrConv(read["NAME"].ToString(), vb.VbStrConv.ProperCase),
                            COMPANY_ID = int.Parse(read["COMPANY_ID"].ToString()),
                            ACTIVE = bool.Parse(read["ACTIVE"].ToString()),
                            CONTRACT_FILE = (read["CONTRACT_FILE"] != DBNull.Value) ? read["CONTRACT_FILE"].ToString() : string.Empty,
                            CREATEDBY = (read["CREATEDBY"] != DBNull.Value) ? read["CREATEDBY"].ToString() : string.Empty,
                            DATECREATED = (read["DATECREATED"] != DBNull.Value) ? DateTime.Parse(read["DATECREATED"].ToString()) : defDate
                        });
                    }
                    return jobtitles;
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
        }

        public JobTitle getJobTitleInfo(JobTitle _title)
        {
            try
            {
                DateTime? defDate = null;
                using (SqlDataReader read = SqlHelper.ExecuteReader(Util.GetconnectionString(), "PROC_GET_TITLE_BY_ID", _title.ID))
                {
                    if (read.Read())
                    {
                        return new JobTitle
                        {
                            ID = int.Parse(read["ID"].ToString()),
                            NAME = vb.Strings.StrConv(read["NAME"].ToString(), vb.VbStrConv.ProperCase),
                            COMPANY_ID = int.Parse(read["COMPANY_ID"].ToString()),
                            ACTIVE = bool.Parse(read["ACTIVE"].ToString()),
                            CONTRACT_FILE = (read["CONTRACT_FILE"] != DBNull.Value) ? read["CONTRACT_FILE"].ToString() : string.Empty,
                            CREATEDBY = (read["CREATEDBY"] != DBNull.Value) ? read["CREATEDBY"].ToString() : string.Empty,
                            DATECREATED = (read["DATECREATED"] != DBNull.Value) ? DateTime.Parse(read["DATECREATED"].ToString()) : defDate
                        };
                    }
                    return new JobTitle();
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
        }

        #endregion
    }
}