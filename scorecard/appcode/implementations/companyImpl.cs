﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using scorecard.interfaces;
using scorecard.entities;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

/* @Copy Albertoncaffeine.net */

namespace scorecard.implementations
{
    public class companyImpl : Icompany
    {
        #region Icompany Members

        public bool updateCompanyPaidStatus(int compId, bool paid)
        {
            int result = (int)SqlHelper.ExecuteScalar(Util.GetconnectionString(), "PROC_UPDATE_COMPANY_PAIDSTATUS", compId, paid);
            return result > 0 ? true : false;
        }

        public int addCompany(company _companyInfo)
        {
            try
            {
                return SqlHelper.ExecuteNonQuery(Util.GetconnectionString(), "PROC_ADD_COMPANY",
                 _companyInfo.ACCOUNT_NO,
                 _companyInfo.COMPANY_NAME,
                 _companyInfo.REGISTRATION_NO,
                 _companyInfo.INDUSTRY,
                 _companyInfo.COUNTRY,
                 _companyInfo.VAT_NO,
                 _companyInfo.ADDRESS,
                 _companyInfo.CONTACT_NO
                 );
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return 0;
            }
        }

        public int updateCompany(company _companyInfo)
        {
            try
            {
                return SqlHelper.ExecuteNonQuery(Util.GetconnectionString(), "PROC_UPDATE_COMPANY",
                 _companyInfo.COMPANY_ID,
                 _companyInfo.ACCOUNT_NO,
                 _companyInfo.COMPANY_NAME,
                 _companyInfo.REGISTRATION_NO,
                 _companyInfo.INDUSTRY,
                 _companyInfo.COUNTRY,
                 _companyInfo.VAT_NO,
                 _companyInfo.ADDRESS,
                 _companyInfo.CONTACT_NO
                 );
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return 0;
            }
        }

        public int deleteCompany(company _companyInfo)
        {
            try
            {
                return SqlHelper.ExecuteNonQuery(Util.GetconnectionString(), "PROC_DELETE_COMPANY",
                 _companyInfo.COMPANY_ID
                 );
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return 0;
            }
        }

        public List<company> getCompanyByCustomer(customer _customer)
        {
            try
            {
                List<company> company = new List<company>();
                using (SqlDataReader read = SqlHelper.ExecuteReader(Util.GetconnectionString(), "PROC_GET_COMPANY_BY_CUSTOMER", _customer.ACCOUNT_NO))
                {
                    while (read.Read())
                    {
                        company.Add(new company
                        {
                            COMPANY_ID = read.GetInt32(0),
                            ACCOUNT_NO = read.GetInt32(1),
                            COMPANY_NAME = read.GetString(2),
                            REGISTRATION_NO = read.GetString(3),
                            INDUSTRY = read.GetInt32(4),
                            VAT_NO = read.GetString(5),
                            ADDRESS = read.GetString(6),
                            CONTACT_NO = read.GetString(7),
                            ACTIVE = read.GetBoolean(8)
                        });
                    }
                }
                return company;
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
        }

        public DataTable getAllRegisteredCompaniesList()
        {
            return SqlHelper.ExecuteDataset(Util.GetconnectionString(), "PROC_GET_ALL_REGISTEREDCOMPANIES").Tables[0];
        }

        public List<company> getAllRegisteredCompanies()
        {
            try
            {
                List<company> company = new List<company>();
                using (SqlDataReader read = SqlHelper.ExecuteReader(Util.GetconnectionString(), "PROC_GET_ALL_COMPANIES"))
                {
                    while (read.Read())
                    {
                        company.Add(new company
                        {
                            COMPANY_ID = read.GetInt32(0),
                            ACCOUNT_NO = read.GetInt32(1),
                            COMPANY_NAME = read.GetString(2),
                            REGISTRATION_NO = read.GetString(3),
                            INDUSTRY = read.GetInt32(4),
                            VAT_NO = read.GetString(5),
                            ADDRESS = read.GetString(6),
                            CONTACT_NO = read.GetString(7),
                            ACTIVE = read.GetBoolean(8)
                        });
                    }
                }
                return company;
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
        }

        public company getCompanyInfo(company _companyInfo)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            SqlDataReader read = null;
            try
            {
                read = SqlHelper.ExecuteReader(con, "PROC_GET_COMPANY", _companyInfo.COMPANY_ID);
                int? defInt = null;
                bool? defBool = null;

                if (read.Read())
                {
                    return new company
                    {
                        COMPANY_ID = int.Parse(read["company_id"].ToString()),
                        ACCOUNT_NO = int.Parse(read["ACCOUNT_NO"].ToString()),
                        COUNTRY = read["COUNTRY"] != DBNull.Value ? int.Parse(read["COUNTRY"].ToString()) : defInt,
                        COMPANY_NAME = read["COMPANY_NAME"] != DBNull.Value ? read["COMPANY_NAME"].ToString() : string.Empty,
                        REGISTRATION_NO = read["REGISTRATION_NO"] != DBNull.Value ? read["REGISTRATION_NO"].ToString() : string.Empty,
                        INDUSTRY = read["INDUSTRY"] != DBNull.Value ? int.Parse(read["INDUSTRY"].ToString()) : defInt,
                        VAT_NO = read["VAT_NO"] != DBNull.Value ? read["VAT_NO"].ToString() : string.Empty,
                        ADDRESS = read["ADDRESS"] != DBNull.Value ? read["ADDRESS"].ToString() : string.Empty,
                        CONTACT_NO = read["CONTACT_NO"] != DBNull.Value ? read["CONTACT_NO"].ToString() : string.Empty,
                        ACTIVE = read["ACTIVE"] != DBNull.Value ? bool.Parse(read["ACTIVE"].ToString()) : defBool,
                        PAID = bool.Parse(read["Paid"].ToString()),
                        DATE_REGISTERED = DateTime.Parse(read["DATE_REGISTERED"].ToString())
                    };
                }
                return new company();
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return new company();
            }
        }

        public int addCompanyAccountDetails(bankaccount account)
        {
            try
            {
                return (int)SqlHelper.ExecuteScalar(Util.GetconnectionString(), "PROC_ADD_ACCOUNT_DETAILS",
                 account.COMPANY_ID,
                 account.BANK_NAME,
                 account.BRANCH_NAME,
                 account.BRANCH_CODE,
                 account.ACCOUNT_NO,
                 account.ACCOUNT_TYPE
                 );
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return 0;
            }
        }

        public int updateCompanyAccountDetails(bankaccount account)
        {
            try
            {
                return (int)SqlHelper.ExecuteScalar(Util.GetconnectionString(), "PROC_UPDATE_ACCOUNT_DETAILS",
                 account.COMPANY_ID,
                 account.BANK_NAME,
                 account.BRANCH_NAME,
                 account.BRANCH_CODE,
                 account.ACCOUNT_NO,
                 account.ACCOUNT_TYPE
                 );
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return 0;
            }
        }

        public bankaccount getCompanyAccountDetails(int companyId)
        {
            try
            {
                using (SqlDataReader reader = SqlHelper.ExecuteReader(Util.GetconnectionString(), "PROC_GET_ACCOUNT_DETAILS", companyId))
                {
                    if (reader.Read())
                    {
                        return new bankaccount
                        {
                            COMPANY_ID = companyId,
                            BANK_NAME = reader["BANK_NAME"].ToString(),
                            ACCOUNT_NO = reader["ACCOUNT_NO"].ToString(),
                            ACCOUNT_TYPE = reader["ACCOUNT_TYPE"].ToString(),
                            BRANCH_CODE = reader["BRANCH_CODE"].ToString(),
                            BRANCH_NAME = reader["BRANCH_NAME"].ToString()
                        };
                    }
                }
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
            return new bankaccount();
        }

        #endregion
    }
}