﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using scorecard.implementations;
using HR.Incentives.DataAccess;
using HR.Incentives.Entities;
using System.Data;
using ScoreCard.Common.Entities;
using ScoreCard.Common.Interfaces;
using ScoreCard.Common.Logic;
using HR.Incentives.Interfaces;
using scorecard.entities;

namespace HR.Incentives.Logic
{
    public class IncentiveBL : IIncentive
    {
        #region Properties

        public string ConnectionString
        {
            get { return Util.GetconnectionString(); }
        }

        public IncentiveDAL Dal
        {
            get { return new IncentiveDAL(ConnectionString); }
        }

        public IPeriod Prdl
        {
            get { return new PeriodBL(); }
        }

        #endregion

        #region Default Constructors

        public IncentiveBL() { }

        #endregion

        #region Public Methods & Functions

        public bool Add(Incentive obj)
        {
            int result = Dal.Save(obj);
            return (result > 0);
        }

        public bool Update(Incentive obj)
        {
            int result = Dal.Update(obj);
            return (result > 0);
        }

        public bool SetStatus(Incentive obj)
        {
            int result = Dal.Deactivate(obj);
            return (result > 0);
        }

        public Incentive GetById(int incentiveId)
        {
            using (IDataReader rw = Dal.Get(incentiveId))
            {
                return (rw.Read()) ? GetRow(rw) : new Incentive();
            }
        }

        public List<Incentive> GetByOrgUnitId(bool active, int companyId, int orgUnitId)
        {
            List<Incentive> obj = new List<Incentive>();
            using (IDataReader rw = Dal.Get(active, companyId, orgUnitId))
            {
                while (rw.Read())
                    obj.Add(GetRow(rw));
            }
            return obj;
        }

        public List<Incentive> GetByStatus(int companyId, int? orgUnitId, bool active)
        {
            List<Incentive> obj = new List<Incentive>();
            using (IDataReader rw = Dal.Get(companyId, orgUnitId, active))
            {
                while (rw.Read())
                    obj.Add(GetRow(rw));
            }
            return obj;
        }

        public Incentive GetRow(IDataRecord rw)
        {
            DateTime? defDate = null;
            int? defInt = null;
            return new Incentive
            {
                Active = bool.Parse(rw["Active"].ToString()),
                CompanyId = int.Parse(rw["CompanyId"].ToString()),
                Company = new Orgunit
                {
                    ID = (rw["OrgUnitId"] != DBNull.Value) ? int.Parse(rw["OrgUnitId"].ToString()) : 0,
                    ORGUNIT_NAME = (rw["OrgUnitName"] != DBNull.Value) ? rw["OrgUnitName"].ToString() : string.Empty
                },
                CreatedBy = rw["CreatedBy"].ToString(),
                DateCreated = DateTime.Parse(rw["DateCreated"].ToString()),
                DateUpdated = (rw["DateUpdated"] != DBNull.Value) ? DateTime.Parse(rw["DateUpdated"].ToString()) : defDate,
                IncentiveDescription = (rw["IncentiveDescription"] != DBNull.Value) ? rw["IncentiveDescription"].ToString() : string.Empty,
                IncentiveId = int.Parse(rw["IncentiveId"].ToString()),
                IncentiveName = rw["IncentiveName"].ToString(),
                OrgUnitId = (rw["OrgUnitId"] != DBNull.Value) ? int.Parse(rw["OrgUnitId"].ToString()) : defInt,
                PeriodId = int.Parse(rw["PeriodId"].ToString()),
                UpdatedBy = (rw["UpdatedBy"] != DBNull.Value) ? rw["UpdatedBy"].ToString() : string.Empty,
                YearPeriod = Prdl.GetBriefRow(rw)
            };
        }

        public Incentive GetBriefRow(IDataRecord rw)
        {
            return new Incentive
            {
                IncentiveId = (rw["IncentiveId"] != DBNull.Value) ? int.Parse(rw["IncentiveId"].ToString()) : 0,
                IncentiveName = (rw["IncentiveName"] != DBNull.Value) ? rw["IncentiveName"].ToString() : string.Empty
            };
        }

        #endregion
    }
}