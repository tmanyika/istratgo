﻿using System;
using HR.Incentives.Interfaces;
using scorecard.implementations;
using HR.Incentives.DataAccess;
using HR.Incentives.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Data;

namespace HR.Incentives.Logic
{
    public class IncentiveReportBL : IIncentiveReport
    {
        #region Properties
        readonly IIncentiveThreshold thresh;

        public string ConnectionString
        {
            get { return Util.GetconnectionString(); }
        }

        public IncentiveReportDAL Dal
        {
            get { return new IncentiveReportDAL(ConnectionString); }
        }

        #endregion

        #region Default Constructors

        public IncentiveReportBL()
        {
            thresh = new IncentiveThresholdBL();
        }

        #endregion

        #region Public Methods & Functions

        public IncentiveReport GetIncentiveRow(IDataRecord rw)
        {
            decimal? defDec = null;
            decimal? myRating = (rw["Rating"] == DBNull.Value) ? defDec : Math.Round(decimal.Parse(rw["Rating"].ToString()), 2);
            decimal? salary = (rw["SalaryAmount"] == DBNull.Value) ? defDec : Math.Round(decimal.Parse(rw["SalaryAmount"].ToString()), 2);

            string middleName = (rw["MiddleName"] != DBNull.Value) ? rw["MiddleName"].ToString() : string.Empty;

            return new IncentiveReport
            {
                CompanyId = int.Parse(rw["CompanyId"].ToString()),
                FirstName = rw["FirstName"].ToString(),
                IncentivePayable = defDec,
                LastName = rw["LastName"].ToString(),
                MiddleName = middleName,
                OrgUnitId = int.Parse(rw["OrgUnitId"].ToString()),
                PercentOfSalaryType = defDec,
                Rating = myRating,
                SalaryAmount = salary
            };
        }

        public IncentiveReport GetSalaryVsRatingRow(IDataRecord rw)
        {
            decimal? defDec = null;
            decimal? myRating = (rw["Rating"] == DBNull.Value) ? defDec : Math.Round(decimal.Parse(rw["Rating"].ToString()), 2);
            decimal? salary = (rw["SalaryAmount"] == DBNull.Value) ? defDec : Math.Round(decimal.Parse(rw["SalaryAmount"].ToString()), 2);

            return new IncentiveReport
            {
                FirstName = rw["FirstName"].ToString(),
                IncentivePayable = defDec,
                LastName = rw["LastName"].ToString(),
                PercentOfSalaryType = defDec,
                Rating = myRating,
                SalaryAmount = salary
            };
        }

        public List<IncentiveReport> GetSalaryVsRatingReport(int? orgUnitId, int companyId, int salaryTypeId, DateTime startDate, DateTime endDate)
        {
            List<IncentiveReport> data = new List<IncentiveReport>();
            using (IDataReader item = Dal.GetSalaryVsRating(orgUnitId, companyId, salaryTypeId, startDate, endDate))
            {
                while (item.Read())
                {
                    data.Add(GetSalaryVsRatingRow(item));
                }
            }
            return data;
        }

        private List<IncentiveReport> GetIncentiveData(int? orgUnitId, int companyId, int salaryTypeId, DateTime startDate, DateTime endDate)
        {
            List<IncentiveReport> data = new List<IncentiveReport>();
            using (IDataReader item = Dal.GetIncentiveReport(orgUnitId, companyId, salaryTypeId, startDate, endDate))
            {
                while (item.Read())
                {
                    data.Add(GetIncentiveRow(item));
                }
            }
            return data;
        }

        private IncentiveThreshold GetIncentiveInfo(List<IncentiveThreshold> threshData, IncentiveReport employee, DateTime startDate, DateTime endDate)
        {
            foreach (var item in threshData)
            {
                if ((item.StartDate <= startDate && item.EndDate >= endDate) &&
                    (employee.Rating >= item.MinimumRating && employee.Rating <= item.MaximumRating) &&
                    (employee.CompanyId == item.CompanyId && employee.OrgUnitId == item.OrgUnitId)
                   )
                {
                    return item;
                }
                else if ((item.StartDate <= startDate && item.EndDate >= endDate) &&
                    (employee.Rating >= item.MinimumRating && employee.Rating <= item.MaximumRating) &&
                    (employee.CompanyId == item.CompanyId && item.OrgUnitId == null)
                   )
                {
                    return item;
                }
            }

            return new IncentiveThreshold
            {
                ThresholdId = -1
            };
        }

        public List<IncentiveReport> GetIncentiveReport(int? orgUnitId, int companyId, int incentiveId, int salaryTypeId, DateTime startDate, DateTime endDate)
        {
            const decimal PERCENT = 100;

            List<IncentiveReport> data = GetIncentiveData(orgUnitId, companyId, salaryTypeId, startDate, endDate);
            List<IncentiveThreshold> threshData = thresh.GetByCompanyId(companyId, startDate, endDate, incentiveId, salaryTypeId, true);

            if (threshData.Count > 0)
            {
                foreach (var item in data)
                {
                    if (item.Rating != null && item.SalaryAmount != null)
                    {
                        var threshInfo = GetIncentiveInfo(threshData, item, startDate, endDate);
                        if (threshInfo.ThresholdId > 0)
                        {
                            item.PercentOfSalaryType = threshInfo.RewardPercentage;
                            item.IncentivePayable = (threshInfo.RewardPercentage / PERCENT) * item.SalaryAmount;
                        }
                    }
                }                
            }

            return data;
        }

        #endregion
    }
}