﻿using System;
using System.Collections.Generic;
using System.Data;
using scorecard.implementations;
using HR.Incentives.Entities;
using HR.Incentives.DataAccess;
using HR.Incentives.Interfaces;

namespace HR.Incentives.Logic
{
    public class SalaryTypeBL : ISalaryType
    {
        #region Properties

        public string ConnectionString
        {
            get { return Util.GetconnectionString(); }
        }

        public SalaryTypeDAL Dal
        {
            get { return new SalaryTypeDAL(ConnectionString); }
        }

        #endregion

        #region Default Constructors

        public SalaryTypeBL() { }

        #endregion

        #region Methods & Functions

        public bool Add(SalaryType obj)
        {
            int result = Dal.Save(obj);
            return result > 0 ? true : false;
        }

        public bool Update(SalaryType obj)
        {
            int result = Dal.Update(obj);
            return result > 0 ? true : false;
        }

        public bool SetStatus(SalaryType obj)
        {
            int result = Dal.UpdateStatus(obj);
            return result > 0 ? true : false;
        }

        public List<SalaryType> GetByStatus(int companyId, bool active)
        {
            List<SalaryType> obj = new List<SalaryType>();
            using (IDataReader rw = Dal.Get(companyId, active))
            {
                while (rw.Read())
                    obj.Add(GetRow(rw));
            }
            return obj;
        }

        public List<SalaryType> GetAllByCompanyId(int companyId)
        {
            List<SalaryType> obj = new List<SalaryType>();
            using (IDataReader rw = Dal.Get(companyId))
            {
                while (rw.Read())
                    obj.Add(GetRow(rw));
            }
            return obj;
        }

        public SalaryType GetById(int salaryTypeId)
        {
            using (IDataReader rw = Dal.Get(salaryTypeId))
            {
                return (rw.Read()) ? GetRow(rw) : new SalaryType();
            }
        }

        public SalaryType GetRow(IDataRecord rw)
        {
            DateTime? defDate = null;
            return new SalaryType
            {
                Active = bool.Parse(rw["Active"].ToString()),
                CompanyId = int.Parse(rw["CompanyId"].ToString()),
                CreatedBy = rw["CreatedBy"].ToString(),
                DateCreated = DateTime.Parse(rw["DateCreated"].ToString()),
                DateUpdated = (rw["DateUpdated"] != DBNull.Value) ? DateTime.Parse(rw["DateUpdated"].ToString()) : defDate,
                SalaryTypeId = int.Parse(rw["SalaryTypeId"].ToString()),
                SalaryTypeName = rw["SalaryTypeName"].ToString(),
                SalaryTypeDescription = (rw["SalaryTypeDescription"] != DBNull.Value) ? rw["SalaryTypeDescription"].ToString() : string.Empty,
                UpdatedBy = (rw["UpdatedBy"] != DBNull.Value) ? rw["UpdatedBy"].ToString() : string.Empty
            };
        }

        public SalaryType GetBriefRow(IDataRecord rw)
        {
            return new SalaryType
            {
                SalaryTypeId = (rw["SalaryTypeId"] != DBNull.Value) ? int.Parse(rw["SalaryTypeId"].ToString()) : 0,
                SalaryTypeName = (rw["SalaryTypeName"] != DBNull.Value) ? rw["SalaryTypeName"].ToString() : string.Empty
            };
        }

        #endregion
    }
}