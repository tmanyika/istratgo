﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using scorecard.implementations;
using HR.Incentives.DataAccess;
using HR.Incentives.Entities;
using System.Data;
using ScoreCard.Common.Entities;
using ScoreCard.Common.Interfaces;
using ScoreCard.Common.Logic;
using HR.Incentives.Interfaces;
using scorecard.entities;

namespace HR.Incentives.Logic
{
    public class EmployeeSalaryBL : IEmployeeSalary
    {
        #region Properties

        public string ConnectionString
        {
            get { return Util.GetconnectionString(); }
        }

        public EmployeeSalaryDAL Dal
        {
            get { return new EmployeeSalaryDAL(ConnectionString); }
        }

        public ISalaryType Sal
        {
            get { return new SalaryTypeBL(); }
        }

        #endregion

        #region Default Constructors

        public EmployeeSalaryBL() { }

        #endregion

        #region Public Methods & Functions

        public bool DeleteSalaryInfo(long employeeSalaryId)
        {
            int deleted = Dal.Delete(employeeSalaryId);
            return (deleted > 0);
        }

        public bool UpdateSalaryInfo(EmployeeSalary obj)
        {
            int updated = Dal.Update(obj);
            return (updated > 0);
        }

        public bool AddSalaryInfo(EmployeeSalary obj)
        {
            int added = Dal.Save(obj);
            return (added > 0);
        }

        public EmployeeSalary GetById(long employeeSalaryId)
        {
            using (IDataReader rw = Dal.Get(employeeSalaryId))
            {
                return (rw.Read()) ? GetRow(rw) : new EmployeeSalary();
            }
        }

        public List<EmployeeSalary> GetByEmployeeId(int companyId, int employeeId, bool active)
        {
            List<EmployeeSalary> obj = new List<EmployeeSalary>();
            using (IDataReader rw = Dal.Get(companyId, employeeId, active))
            {
                while (rw.Read())
                    obj.Add(GetRow(rw));
            }
            return obj;
        }

        public EmployeeSalary GetRow(IDataRecord rw)
        {
            DateTime? defDate = null;
            return new EmployeeSalary
            {
                Active = (rw["Active"] != DBNull.Value) ? bool.Parse(rw["Active"].ToString()) : false,
                CreatedBy = (rw["CreatedBy"] != DBNull.Value) ? rw["CreatedBy"].ToString() : string.Empty,
                DateCreated = (rw["DateCreated"] != DBNull.Value) ? DateTime.Parse(rw["DateCreated"].ToString()) : DateTime.Now.Date,
                DateUpdated = (rw["DateUpdated"] != DBNull.Value) ? DateTime.Parse(rw["DateUpdated"].ToString()) : defDate,
                EmployeeId = (rw["EmployeeId"] != DBNull.Value) ? int.Parse(rw["EmployeeId"].ToString()) : 0,
                EmployeeSalaryId = (rw["EmployeeSalaryId"] != DBNull.Value) ? int.Parse(rw["EmployeeSalaryId"].ToString()) : 0,
                Salary = Sal.GetBriefRow(rw),
                SalaryAmount = (rw["SalaryAmount"] != DBNull.Value) ? decimal.Parse(rw["SalaryAmount"].ToString()) : 0,
                SalaryTypeId = (rw["SalaryTypeId"] != DBNull.Value) ? int.Parse(rw["SalaryTypeId"].ToString()) : 0,
                UpdatedBy = (rw["UpdatedBy"] != DBNull.Value) ? rw["UpdatedBy"].ToString() : string.Empty
            };
        }

        #endregion
    }
}