﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using scorecard.implementations;
using HR.Incentives.DataAccess;
using HR.Incentives.Entities;
using System.Data;
using ScoreCard.Common.Entities;
using ScoreCard.Common.Interfaces;
using ScoreCard.Common.Logic;
using HR.Incentives.Interfaces;
using scorecard.entities;

namespace HR.Incentives.Logic
{
    public class IncentiveThresholdBL : IIncentiveThreshold
    {
        #region Properties

        public string ConnectionString
        {
            get { return Util.GetconnectionString(); }
        }

        public IncentiveThresholdDAL Dal
        {
            get { return new IncentiveThresholdDAL(ConnectionString); }
        }

        public IIncentive Inc
        {
            get { return new IncentiveBL(); }
        }

        public ISalaryType Stype
        {
            get { return new SalaryTypeBL(); }
        }

        #endregion

        #region Default Constructors

        public IncentiveThresholdBL() { }

        #endregion

        #region Public Methods & Functions

        public bool Add(IncentiveThreshold obj)
        {
            int result = Dal.Save(obj);
            return (result > 0);
        }

        public bool Update(IncentiveThreshold obj)
        {
            int result = Dal.Update(obj);
            return (result > 0);
        }

        public bool SetStatus(IncentiveThreshold obj)
        {
            int result = Dal.Deactivate(obj);
            return (result > 0);
        }

        public List<IncentiveThreshold> Search(int companyId, int? orgUnitId, bool active, int? incentiveId, int? year)
        {
            List<IncentiveThreshold> obj = new List<IncentiveThreshold>();
            using (IDataReader rw = Dal.Get(companyId, orgUnitId, active, incentiveId, year))
            {
                while (rw.Read())
                    obj.Add(GetRow(rw));
            }
            return obj;
        }

        public List<IncentiveThreshold> GetByStatus(int companyId, int? orgUnitId, bool active)
        {
            List<IncentiveThreshold> obj = new List<IncentiveThreshold>();
            using (IDataReader rw = Dal.Get(companyId, orgUnitId, active))
            {
                while (rw.Read())
                    obj.Add(GetRow(rw));
            }
            return obj;
        }

        public IncentiveThreshold GetById(long thresholdId)
        {
            using (IDataReader rw = Dal.Get(thresholdId))
            {
                return (rw.Read()) ? GetRow(rw) : new IncentiveThreshold();
            }
        }

        public List<IncentiveThreshold> GetByCompanyId(int companyId, DateTime startDate, DateTime endDate, int incentiveId, int salaryTypeId, bool active)
        {
            List<IncentiveThreshold> obj = new List<IncentiveThreshold>();
            using (IDataReader rw = Dal.Get(companyId, startDate, endDate, incentiveId, salaryTypeId, active))
            {
                while (rw.Read())
                    obj.Add(GetRow(rw));
            }
            return obj;
        }

        public IncentiveThreshold GetRow(IDataRecord rw)
        {
            DateTime? defDate = null;
            int? defInt = null;
            return new IncentiveThreshold
            {
                Active = bool.Parse(rw["Active"].ToString()),
                CompanyId = int.Parse(rw["CompanyId"].ToString()),
                CreatedBy = rw["CreatedBy"].ToString(),
                DateCreated = DateTime.Parse(rw["DateCreated"].ToString()),
                DateUpdated = (rw["DateUpdated"] != DBNull.Value) ? DateTime.Parse(rw["DateUpdated"].ToString()) : defDate,
                EndDate = DateTime.Parse(rw["EndDate"].ToString()),
                IncentiveId = int.Parse(rw["IncentiveId"].ToString()),
                IsForCompany = (rw["CompanyId"] != DBNull.Value && rw["OrgUnitId"] == DBNull.Value),
                MaximumRating = Math.Round(decimal.Parse(rw["MaximumRating"].ToString()), 2),
                MinimumRating = Math.Round(decimal.Parse(rw["MinimumRating"].ToString()), 2),
                OrgUnitId = (rw["OrgUnitId"] != DBNull.Value) ? int.Parse(rw["OrgUnitId"].ToString()) : defInt,
                Company = new Orgunit
                {
                    ID = (rw["OrgUnitId"] != DBNull.Value) ? int.Parse(rw["OrgUnitId"].ToString()) : 0,
                    ORGUNIT_NAME = (rw["OrgUnitName"] != DBNull.Value) ? rw["OrgUnitName"].ToString() : string.Empty
                },
                Reward = Inc.GetBriefRow(rw),
                RewardConstant = decimal.Parse(rw["RewardConstant"].ToString()),
                RewardPercentage = decimal.Parse(rw["RewardPercentage"].ToString()),
                SalaryTypeId = int.Parse(rw["SalaryTypeId"].ToString()),
                Salary = Stype.GetBriefRow(rw),
                StartDate = DateTime.Parse(rw["StartDate"].ToString()),
                ThresholdId = int.Parse(rw["ThresholdId"].ToString()),
                UpdatedBy = (rw["UpdatedBy"] != DBNull.Value) ? rw["UpdatedBy"].ToString() : string.Empty
            };
        }

        public IncentiveThreshold GetBriefRow(IDataRecord rw)
        {
            return new IncentiveThreshold
            {
                EndDate = DateTime.Parse(rw["EndDate"].ToString()),
                IncentiveId = int.Parse(rw["IncentiveId"].ToString()),
                MaximumRating = decimal.Parse(rw["MaximumRating"].ToString()),
                MinimumRating = decimal.Parse(rw["MinimumRating"].ToString()),
                RewardConstant = decimal.Parse(rw["RewardConstant"].ToString()),
                RewardPercentage = decimal.Parse(rw["RewardPercentage"].ToString()),
                SalaryTypeId = int.Parse(rw["SalaryTypeId"].ToString()),
                Reward = Inc.GetBriefRow(rw),
                StartDate = DateTime.Parse(rw["StartDate"].ToString())
            };
        }

        #endregion
    }
}