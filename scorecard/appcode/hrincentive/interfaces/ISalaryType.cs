﻿using System;
using System.Collections.Generic;
using System.Data;
using HR.Incentives.Entities;

namespace HR.Incentives.Interfaces
{
    public interface ISalaryType
    {
        bool Add(SalaryType obj);
        bool Update(SalaryType obj);
        bool SetStatus(SalaryType obj);

        SalaryType GetById(int leaveId);
        SalaryType GetRow(IDataRecord rw);
        SalaryType GetBriefRow(IDataRecord rw);

        List<SalaryType> GetByStatus(int companyId, bool active);
        List<SalaryType> GetAllByCompanyId(int companyId);
    }
}
