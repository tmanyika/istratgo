﻿using System;
using System.Collections.Generic;
using HR.Incentives.Entities;
using System.Data;

namespace HR.Incentives.Interfaces
{
    public interface IIncentiveReport
    {
        IncentiveReport GetIncentiveRow(IDataRecord rw);
        IncentiveReport GetSalaryVsRatingRow(IDataRecord rw);

        List<IncentiveReport> GetSalaryVsRatingReport(int? orgUnitId, int companyId, int salaryTypeId, DateTime startDate, DateTime endDate);
        List<IncentiveReport> GetIncentiveReport(int? orgUnitId, int companyId, int incentiveId, int salaryTypeId, DateTime startDate, DateTime endDate);
    }
}
