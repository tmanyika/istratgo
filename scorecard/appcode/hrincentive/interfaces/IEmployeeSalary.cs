﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HR.Incentives.Entities;
using System.Data;

namespace HR.Incentives.Interfaces
{
    public interface IEmployeeSalary
    {
        bool DeleteSalaryInfo(long employeeSalaryId);
        bool UpdateSalaryInfo(EmployeeSalary obj);
        bool AddSalaryInfo(EmployeeSalary obj);

        EmployeeSalary GetRow(IDataRecord rw);

        EmployeeSalary GetById(long employeeSalaryId);
        List<EmployeeSalary> GetByEmployeeId(int companyId, int employeeId, bool active);
    }
}
