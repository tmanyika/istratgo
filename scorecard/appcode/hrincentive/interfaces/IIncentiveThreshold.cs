﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HR.Incentives.Entities;
using System.Data;

namespace HR.Incentives.Interfaces
{
    public interface IIncentiveThreshold
    {
        bool Add(IncentiveThreshold obj);
        bool Update(IncentiveThreshold obj);
        bool SetStatus(IncentiveThreshold obj);

        IncentiveThreshold GetRow(IDataRecord rw);
        IncentiveThreshold GetBriefRow(IDataRecord rw);
        IncentiveThreshold GetById(long thresholdId);

        List<IncentiveThreshold> GetByStatus(int companyId, int? orgUnitId, bool active);
        List<IncentiveThreshold> Search(int companyId, int? orgUnitId, bool active, int? incentiveId, int? year);
        List<IncentiveThreshold> GetByCompanyId(int companyId, DateTime startDate, DateTime endDate, int incentiveId, int salaryTypeId, bool active);
    }
}
