﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HR.Incentives.Entities;
using System.Data;

namespace HR.Incentives.Interfaces
{
    public interface IIncentive
    {
        bool Add(Incentive obj);
        bool Update(Incentive obj);
        bool SetStatus(Incentive obj);

        Incentive GetById(int incentiveId);
        Incentive GetRow(IDataRecord rw);
        Incentive GetBriefRow(IDataRecord rw);

        List<Incentive> GetByStatus(int companyId, int? orgUnitId, bool active);
        List<Incentive> GetByOrgUnitId(bool active, int companyId, int orgUnitId);
    }
}
