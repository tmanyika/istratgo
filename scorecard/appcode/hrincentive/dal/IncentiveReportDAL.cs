﻿using System;
using System.Collections.Generic;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

namespace HR.Incentives.DataAccess
{
    public class IncentiveReportDAL
    {
        #region Variables

        private string _conString;

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return _conString; }
            set { _conString = value; }
        }

        #endregion

        #region Default Constructors

        public IncentiveReportDAL(string conString)
        {
            _conString = conString;
        }

        #endregion

        #region Public Methods and Functions

        public IDataReader GetSalaryVsRating(int? orgUnitId, int companyId, int salaryTypeId, DateTime startDate, DateTime endDate)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_IncentiveReport_GetSalaryVsRating", orgUnitId, companyId, salaryTypeId, startDate, endDate);
        }

        public IDataReader GetIncentiveReport(int? orgUnitId, int companyId, int salaryTypeId, DateTime startDate, DateTime endDate)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_IncentiveReport_GetIncentiveSalaryReport", orgUnitId, companyId, salaryTypeId, startDate, endDate);
        }

        #endregion
    }
}