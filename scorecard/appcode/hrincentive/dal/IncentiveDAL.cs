﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.ApplicationBlocks.Data;
using System.Data;
using HR.Incentives.Entities;

namespace HR.Incentives.DataAccess
{
    public class IncentiveDAL
    {
        #region Variables

        private string _conString;

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return _conString; }
            set { _conString = value; }
        }

        #endregion

        #region Default Constructors

        public IncentiveDAL(string conString)
        {
            _conString = conString;
        }

        #endregion

        #region Public Methods and Functions

        public IDataReader Get(int companyId, int? orgUnitId, bool active)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_Incentive_GetByStatus", companyId, orgUnitId, active);
        }

        public IDataReader Get( bool active, int companyId, int? orgUnitId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_Incentive_GetByOrgUnitId", companyId, orgUnitId, active);
        }

        public IDataReader Get(int incentiveId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_Incentive_GetById", incentiveId);
        }

        public int Update(Incentive obj)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_Incentive_Update", obj.IncentiveId, obj.CompanyId, obj.OrgUnitId, obj.IncentiveName,
                                                                                                obj.IncentiveDescription, obj.PeriodId, obj.Active, obj.UpdatedBy);
            return (result == DBNull.Value) ? 0 : int.Parse(result.ToString());
        }

        public int Save(Incentive obj)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_Incentive_Insert", obj.CompanyId, obj.OrgUnitId, obj.IncentiveName,
                                                                                                obj.IncentiveDescription, obj.PeriodId, obj.Active, obj.CreatedBy);
            return (result == DBNull.Value) ? 0 : int.Parse(result.ToString());

        }

        public int Deactivate(Incentive obj)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_Incentive_UpdateStatus", obj.IncentiveId, obj.Active, obj.UpdatedBy);
            return (result == DBNull.Value) ? 0 : int.Parse(result.ToString());
        }

        #endregion
    }
}