﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.ApplicationBlocks.Data;
using System.Data;
using HR.Incentives.Entities;

namespace HR.Incentives.DataAccess
{
    public class EmployeeSalaryDAL
    {
        #region Variables

        private string _conString;

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return _conString; }
            set { _conString = value; }
        }

        #endregion

        #region Default Constructors

        public EmployeeSalaryDAL(string conString)
        {
            _conString = conString;
        }

        #endregion

        #region Public Methods and Functions

        public int Delete(long employeeSalaryId)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_EmployeeSalary_Delete", employeeSalaryId);
            return (result == DBNull.Value) ? 0 : int.Parse(result.ToString());
        }

        public int Update(EmployeeSalary obj)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_EmployeeSalary_Update", obj.EmployeeSalaryId, obj.SalaryAmount, obj.UpdatedBy);
            return (result == DBNull.Value) ? 0 : int.Parse(result.ToString());
        }

        public int Save(EmployeeSalary obj)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_EmployeeSalary_Insert", obj.EmployeeId, obj.SalaryTypeId, obj.SalaryAmount, obj.Active, obj.CreatedBy);
            return (result == DBNull.Value) ? 0 : int.Parse(result.ToString());
        }

        public IDataReader Get(long employeeSalaryId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EmployeeSalary_GetById", employeeSalaryId);
        }

        public IDataReader Get(int companyId, int employeeId, bool active)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EmployeeSalary_GetByEmployeeId", companyId, employeeId, active);
        }
        #endregion
    }
}