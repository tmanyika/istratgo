﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.ApplicationBlocks.Data;
using System.Data;
using HR.Incentives.Entities;

namespace HR.Incentives.DataAccess
{
    public class IncentiveThresholdDAL
    {
        #region Variables

        private string _conString;

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return _conString; }
            set { _conString = value; }
        }

        #endregion

        #region Default Constructors

        public IncentiveThresholdDAL(string conString)
        {
            _conString = conString;
        }

        #endregion

        #region Public Methods and Functions

        public IDataReader Get(long thresholdId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_IncentiveThreshold_GetById", thresholdId);
        }

        public IDataReader Get(int companyId, int? orgUnitId, bool active)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_IncentiveThreshold_GetByStatus", companyId, orgUnitId, active);
        }

        public IDataReader Get(int companyId, DateTime startDate, DateTime endDate, int incentiveId, int salaryTypeId, bool active)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_IncentiveThreshold_GetForIncentiveReport", companyId, startDate, endDate, incentiveId, salaryTypeId, active);
        }

        public IDataReader Get(int companyId, int? orgUnitId, bool active, int? incentiveId, int? year)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_IncentiveThreshold_Search", companyId, orgUnitId, active, incentiveId, year);
        }

        public int Save(IncentiveThreshold obj)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_IncentiveThreshold_Insert", obj.IncentiveId, obj.CompanyId, obj.OrgUnitId, obj.SalaryTypeId,
                                                                                                            obj.StartDate, obj.EndDate, obj.MinimumRating, obj.MaximumRating,
                                                                                                            obj.RewardPercentage, obj.RewardConstant, obj.CreatedBy, obj.Active);
            return (result == DBNull.Value) ? 0 : int.Parse(result.ToString());
        }

        public int Update(IncentiveThreshold obj)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_IncentiveThreshold_Update", obj.ThresholdId, obj.IncentiveId, obj.CompanyId, obj.OrgUnitId,
                                                                                                    obj.SalaryTypeId, obj.StartDate, obj.EndDate, obj.MinimumRating,
                                                                                                    obj.MaximumRating, obj.RewardPercentage, obj.RewardConstant,
                                                                                                    obj.UpdatedBy, obj.Active);
            return (result == DBNull.Value) ? 0 : int.Parse(result.ToString());
        }

        public int Deactivate(IncentiveThreshold obj)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_IncentiveThreshold_Deactivate", obj.ThresholdId, obj.UpdatedBy, obj.Active);
            return (result == DBNull.Value) ? 0 : int.Parse(result.ToString());
        }

        #endregion
    }
}