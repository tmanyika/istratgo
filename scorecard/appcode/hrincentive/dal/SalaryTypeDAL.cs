﻿using System;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using HR.Incentives.Entities;

namespace HR.Incentives.DataAccess
{
    public class SalaryTypeDAL
    {
        #region Variables

        private string _conString;

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return _conString; }
            set { _conString = value; }
        }

        #endregion

        #region Default Constructors

        public SalaryTypeDAL(string conString)
        {
            _conString = conString;
        }

        #endregion

        #region Public Methods and Functions

        public IDataReader Get(int companyId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_SalaryType_GetByCompanyId", companyId);
        }

        public IDataReader Get(int companyId, bool active)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_SalaryType_GetActive", companyId, active);
        }

        public IDataReader GetById(int salaryTypeId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_SalaryType_GetById", salaryTypeId);
        }

        public int Save(SalaryType obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_SalaryType_Insert", obj.CompanyId, obj.SalaryTypeName,
                                                                                                obj.SalaryTypeDescription, obj.Active, obj.CreatedBy);
            return result;
        }

        public int Update(SalaryType obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_SalaryType_Update", obj.SalaryTypeId, obj.CompanyId, obj.SalaryTypeName,
                                                                                                obj.SalaryTypeDescription, obj.Active, obj.UpdatedBy);
            return result;
        }

        public int UpdateStatus(SalaryType obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_SalaryType_Deactivate", obj.SalaryTypeId, obj.Active, obj.UpdatedBy);
            return result;
        }

        #endregion
    }
}