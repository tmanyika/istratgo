﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ScoreCard.Common.Entities;
using scorecard.entities;

namespace HR.Incentives.Entities
{
    public class Incentive
    {
        public int IncentiveId { get; set; }
        public int CompanyId { get; set; }
        public int? OrgUnitId { get; set; }
        public int PeriodId { get; set; }

        public bool Active { get; set; }

        public string IncentiveName { get; set; }
        public string IncentiveDescription { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }

        public Period YearPeriod { get; set; }
        public Orgunit Company { get; set; }
    }
}