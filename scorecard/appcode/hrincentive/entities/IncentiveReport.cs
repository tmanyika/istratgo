﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.Incentives.Entities
{
    public class IncentiveReport
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }

        public int CompanyId { get; set; }
        public int OrgUnitId { get; set; }

        public decimal? SalaryAmount { get; set; }
        public decimal? Rating { get; set; }
        public decimal? PercentOfSalaryType { get; set; }
        public decimal? IncentivePayable { get; set; }
    }
}