﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ScoreCard.Common.Entities;
using scorecard.entities;

namespace HR.Incentives.Entities
{
    public class IncentiveThreshold
    {
        public long ThresholdId { get; set; }
        public int IncentiveId { get; set; }
        public int CompanyId { get; set; }
        public int? OrgUnitId { get; set; }
        public int SalaryTypeId { get; set; }
        public int? Year { get; set; }

        public decimal MinimumRating { get; set; }
        public decimal MaximumRating { get; set; }
        public decimal RewardPercentage { get; set; }
        public decimal RewardConstant { get; set; }

        public bool Active { get; set; }
        public bool IsForCompany { get; set; }

        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }

        public Incentive Reward { get; set; }
        public Orgunit Company { get; set; }
        public SalaryType Salary { get; set; }
    }
}