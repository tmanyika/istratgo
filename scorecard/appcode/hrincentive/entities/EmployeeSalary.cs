﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.Incentives.Entities
{
    public class EmployeeSalary
    {
        public long EmployeeSalaryId { get; set; }

        public int EmployeeId { get; set; }
        public int SalaryTypeId { get; set; }

        public decimal SalaryAmount { get; set; }

        public bool Active { get; set; }

        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }

        public SalaryType Salary { get; set; }
    }
}