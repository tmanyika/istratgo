﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.Incentives.Entities
{
    public class SalaryType
    {
        public int SalaryTypeId { get; set; }
        public int CompanyId { get; set; }

        public bool Active { get; set; }

        public string SalaryTypeName { get; set; }
        public string SalaryTypeDescription { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }

    }
}