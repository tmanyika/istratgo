﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using scorecard.entities;

/* @Copy Albertoncaffeine.net */

namespace scorecard.interfaces
{
    interface Iroles
    {
        int addRole(roles roleInfo);
        int deleteRole(roles roleInfo);
        int updateRole(roles roleInfo);
        List<roles> getRolesByCompany(company _company);
        roles getRole(roles roleInfo);
    }
}