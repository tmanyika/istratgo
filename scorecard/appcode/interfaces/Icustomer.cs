﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using scorecard.entities;
using Scorecard.Entities;

/* @Copy Albertoncaffeine.net */
namespace scorecard.interfaces
{
    public interface Icustomer
    {
        int updateCustomer(customer _customerInfo);
        int deleteCustomer(customer _customerInfo);
        int registerCustomer(customer _customerInfo, company _company, out string errorMessage);
        int registerCustomer(customer _customer, company _company, CompanyActivation _activation, out string errMsg);
        int registerCustomer(customer customer, company company, bankaccount account);

        customer getCustomerInfo(customer _customer);
        List<customer> getCustomersList();
    }
}