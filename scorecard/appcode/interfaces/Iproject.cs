﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using scorecard.entities;

namespace scorecard.interfaces
{
    public interface Iproject
    {
        bool addProject(project obj);
        bool updateProject(project obj);
        bool setProjectStatus(project obj);

        project getProject(int projectId);
        List<project> getProjects(int companyId, bool active);
      
    }
}