﻿using System;

namespace ThreeSixty.Evaluation
{
    public class AreaItem
    {
        public int AreaId { get; set; }
        public int AreaItemId { get; set; }

        public string AreaItemName { get; set; }
        public string AreaItemDescription { get; set; }
    }
}