﻿using System;


namespace ThreeSixty.Evaluation
{
    public class EvaluationInternalPeople : EvaluationExternalPeople
    {
        public int DepartmentId { get; set; }
        public string Department { get; set; }
        public string FullName { get; set; }

    }
}