﻿using System;
using ScoreCard.Common.Entities;

namespace ThreeSixty.Evaluation
{
    public class EvaluationCompetency
    {
        public long CompetencyId { get; set; }

        public int AreaId { get; set; }
        public int CompanyId { get; set; }

        public bool Active { get; set; }

        public string CompetencyName { get; set; }
        public string CompetencyDescription { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }

        public ScoreCardArea Area { get; set; }
    }
}