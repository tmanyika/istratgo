﻿using System;
using ScoreCard.Common.Entities;

namespace ThreeSixty.Evaluation
{
    public class Evaluation
    {
        public long EvaluationId { get; set; }
        public long CompetencyId { get; set; }
        public int AreaValueId { get; set; }

        public decimal? WeightVal { get; set; }

        public bool Active { get; set; }

        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }

        public EvaluationCompetency Competency { get; set; }
        public ScoreCardArea Area { get; set; }
    }
}