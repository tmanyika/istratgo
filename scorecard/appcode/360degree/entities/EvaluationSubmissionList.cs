﻿using System;

namespace ThreeSixty.Evaluation
{
    public class EvaluationSubmissionList
    {
        public long EvaluatorId { get; set; }
        public long SubmissionId { get; set; }

        public int ResourceId { get; set; }

        public decimal? EvaluatorWeight { get; set; }
        public decimal? AverageRatingValue { get; set; }

        public bool SubmittedScore { get; set; }
        public bool HasWeight { get; set; }

        public string OverallComment { get; set; }
        public string XmlEvalList { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public DateTime? DateCreated { get; set; }
        public DateTime? DateSubmitted { get; set; }

        public EvaluationInternalPeople Person { get; set; }

    }
}