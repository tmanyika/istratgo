﻿using System;

namespace ThreeSixty.Evaluation
{
    public class EvaluationScore
    {
        public long ScoreId { get; set; }
        public long SubmissionId { get; set; }
        public long EvaluationId { get; set; }
        public long EvaluatorId { get; set; }

        public decimal? WeightVal { get; set; }
        public decimal? WeightedRatingValue { get; set; }

        public Int16? RatingValue { get; set; }

        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }

        public EvaluationCompetency Competency { get; set; }
    }

}