﻿using System;

namespace ThreeSixty.Evaluation
{
    public class EvaluationExternalPeople
    {
        public int PersonId { get; set; }
        public int CompanyId { get; set; }

        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string XmlPeopleList { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }

    }
}