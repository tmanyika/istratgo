﻿using System;
using ScoreCard.Common.Entities;

namespace ThreeSixty.Evaluation
{
    public class EvaluationSubmission
    {
        public long SubmissionId { get; set; }

        public int OrgUnitId { get; set; }
        public int AreaId { get; set; }
        public int AreaItemId { get; set; }
        public int CapturerId { get; set; }
        public int StatusId { get; set; }

        public bool IsInternal { get; set; }

        public Int16 MaximumRateValue { get; set; }

        public decimal? OveralAverageRating { get; set; }

        public DateTime PeriodDate { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? DateSentOut { get; set; }
        public DateTime? DateUpdated { get; set; }

        public string OveralComment { get; set; }
        public string UpdatedBy { get; set; }

        public ScoreCardArea Area { get; set; }
        public AreaItem Item { get; set; }
        public ScoreCardStatus Status { get; set; }
        public EvaluationSubmissionList Evaluator { get; set; }
    }
}