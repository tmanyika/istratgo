﻿using System;

namespace ThreeSixty.Evaluation
{
    public class EvaluationRating
    {
        public int RatingId { get; set; }
        public int CompanyId { get; set; }

        public Int16 RatingValue { get; set; }

        public bool Active { get; set; }

        public string RatingDescription { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }

    }
}