﻿using System;

namespace ThreeSixty.Evaluation
{
    public class EvaluationReport : AreaItem
    {
        public string FullName { get; set; }
        public string CompetencyName { get; set; }
        public string OverallComment { get; set; }

        public long EvaluatorId { get; set; }

        public decimal? SelfRating { get; set; }
        public decimal? ManagerRating { get; set; }
        public decimal? PeerRating { get; set; }
        public decimal? DirectReportRating { get; set; }
        public decimal? WeightedAvgRating { get; set; }
        public decimal? TargetWeightRating { get; set; }
        public decimal? AverageExternalRating { get; set; }
        public decimal? AverageInternalRating { get; set; }
        public decimal? AverageRating { get; set; }
        public decimal? RatingValue { get; set; }
        public decimal? WeightSelfRating { get; set; }
        public decimal? WeightInternalRating { get; set; }
        public decimal? WeightExternalRating { get; set; }

        public DateTime PeriodDate { get; set; }
    }

    public class Evaluator
    {
        public string FullName { get; set; }
        public long EvaluatorId { get; set; }
    }

}