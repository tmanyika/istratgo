﻿using System;
using System.Data;
using System.Collections.Generic;
using scorecard.implementations;
using ScoreCard.Common.Entities;

namespace ThreeSixty.Evaluation
{
    public class EvaluationBL : IEvaluation
    {
        public EvaluationDAL Dal
        {
            get { return new EvaluationDAL(Util.GetconnectionString()); }
        }

        public bool AddEvaluation(Evaluation obj)
        {
            int result = Dal.Save(obj);
            return (result > 0) ? true : false;
        }

        public bool UpdateEvaluation(Evaluation obj)
        {
            int result = Dal.Update(obj);
            return (result > 0) ? true : false;
        }

        public bool DeactivateEvaluation(Evaluation obj)
        {
            int result = Dal.Deactivate(obj);
            return (result > 0) ? true : false;
        }

        public Evaluation GetRow(IDataRecord rw)
        {
            DateTime? defDate = null;
            decimal? defDec = null;

            return new Evaluation
            {
                Active = bool.Parse(rw["Active"].ToString()),
                Area = new ScoreCardArea
                {
                    AreaAliasName = (rw["AreaAliasName"] != DBNull.Value) ? rw["AreaAliasName"].ToString() : string.Empty,
                    AreaId = (rw["AreaId"] != DBNull.Value) ? int.Parse(rw["AreaId"].ToString()) : 0,
                    AreaName = (rw["AreaName"] != DBNull.Value) ? rw["AreaName"].ToString() : string.Empty,
                },
                AreaValueId = int.Parse(rw["AreaValueId"].ToString()),
                Competency = new EvaluationCompetency
                {
                    CompetencyName = (rw["CompetencyName"] != DBNull.Value) ? rw["CompetencyName"].ToString() : string.Empty,
                    CompanyId = (rw["CompanyId"] != DBNull.Value) ? int.Parse(rw["CompanyId"].ToString()) : 0
                },
                CompetencyId = (rw["CompetencyId"] != DBNull.Value) ? long.Parse(rw["CompetencyId"].ToString()) : 0,
                CreatedBy = rw["CreatedBy"].ToString(),
                DateCreated = DateTime.Parse(rw["DateCreated"].ToString()),
                DateUpdated = (rw["DateUpdated"] != DBNull.Value) ? DateTime.Parse(rw["DateUpdated"].ToString()) : defDate,
                EvaluationId = (rw["EvaluationId"] != DBNull.Value) ? long.Parse(rw["EvaluationId"].ToString()) : 0,
                UpdatedBy = (rw["UpdatedBy"] != DBNull.Value) ? rw["UpdatedBy"].ToString() : string.Empty,
                WeightVal = (rw["WeightVal"] != DBNull.Value) ? decimal.Parse(rw["WeightVal"].ToString()) : defDec
            };
        }

        public List<Evaluation> GetByStatus(int companyId, int areaId, int areaValueId, bool active)
        {
            List<Evaluation> obj = new List<Evaluation>();
            using (IDataReader data = Dal.Get(companyId, areaId, areaValueId, active))
            {
                while (data.Read())
                    obj.Add(GetRow(data));
            }
            return obj;
        }
    }
}