﻿using System;
using System.Data;
using System.Collections.Generic;
using scorecard.implementations;

namespace ThreeSixty.Evaluation
{
    public class EvaluationExternalPeopleBL : IEvaluationExternalPeople
    {
        private IEvaluationSubmissionList External;

        public EvaluationExternalPeopleBL()
            : this(new EvaluationSubmissionListBL())  { }

        public EvaluationExternalPeopleBL(IEvaluationSubmissionList sList)
        {
            External = sList;
        }

        public EvaluationExternalPeopleDAL Dal
        {
            get { return new EvaluationExternalPeopleDAL(Util.GetconnectionString()); }
        }

        public DataTable GetSchema(int companyId)
        {
            DataTable dT = new DataTable();
            DataColumn col = new DataColumn("CompanyId", typeof(int));
            col.DefaultValue = companyId;

            dT.Columns.Add(col);
            dT.Columns.Add("PersonId", typeof(int));
            dT.Columns.Add("FirstName", typeof(string));
            dT.Columns.Add("MiddleName", typeof(string));
            dT.Columns.Add("LastName", typeof(string));
            dT.Columns.Add("Email", typeof(string));
            dT.Columns.Add("CreatedBy", typeof(string));
            dT.Columns.Add("UpdatedBy", typeof(string));

            dT.AcceptChanges();
            return dT;
        }

        public bool AddEvaluators(EvaluationExternalPeople obj)
        {
            int result = Dal.SaveBulk(obj);
            return (result > 0);
        }

        public List<EvaluationInternalPeople> GetExternalSentOutPeopleById(int companyId, string emailList)
        {
            List<EvaluationInternalPeople> obj = new List<EvaluationInternalPeople>();
            using (IDataReader data = Dal.GetExternalSentOutPeopleList(companyId, emailList))
            {
                while (data.Read())
                    obj.Add(External.GetInternalRecipient(data));
            }
            return obj;
        }
    }
}