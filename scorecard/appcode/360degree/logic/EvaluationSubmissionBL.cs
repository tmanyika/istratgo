﻿using System;
using System.Data;
using System.Collections.Generic;
using scorecard.implementations;
using ScoreCard.Common.Entities;

namespace ThreeSixty.Evaluation
{
    public class EvaluationSubmissionBL : IEvaluationSubmission
    {
        public EvaluationSubmissionDAL Dal
        {
            get { return new EvaluationSubmissionDAL(Util.GetconnectionString()); }
        }

        public long AddSubmission(EvaluationSubmission obj)
        {
            long result = Dal.Save(obj);
            return result;
        }

        public bool UpdateSubmissionPeriod(EvaluationSubmission obj)
        {
            int result = Dal.UpdatePeriod(obj);
            return (result > 0) ? true : false;
        }

        public bool SetStatus(EvaluationSubmission obj)
        {
            int result = Dal.UpdateStatus(obj);
            return (result > 0) ? true : false;
        }

        public bool DeleteSubmission(long submissionId)
        {
            int result = Dal.Delete(submissionId);
            return (result > 0) ? true : false;
        }

        public bool EvaluationExist(EvaluationSubmission obj)
        {
            object result = Dal.Check(obj);
            return (result == DBNull.Value) ? false : bool.Parse(result.ToString());
        }

        public EvaluationSubmission GetSubmissionRow(IDataRecord rw)
        {
            DateTime? defDate = null;
            return new EvaluationSubmission
            {
                AreaId = int.Parse(rw["AreaId"].ToString()),
                AreaItemId = int.Parse(rw["AreaItemId"].ToString()),
                CapturerId = int.Parse(rw["CapturerId"].ToString()),
                DateCreated = DateTime.Parse(rw["DateCreated"].ToString()),
                DateSentOut = (rw["DateSentOut"] != DBNull.Value) ? DateTime.Parse(rw["DateSentOut"].ToString()) : defDate,
                DateUpdated = (rw["DateUpdated"] != DBNull.Value) ? DateTime.Parse(rw["DateUpdated"].ToString()) : defDate,
                IsInternal = bool.Parse(rw["IsInternal"].ToString()),
                MaximumRateValue = Int16.Parse(rw["MaximumRateValue"].ToString()),
                OrgUnitId = int.Parse(rw["OrgUnitId"].ToString()),
                OveralComment = (rw["OveralComment"] != DBNull.Value) ? rw["OveralComment"].ToString() : string.Empty,
                PeriodDate = DateTime.Parse(rw["PeriodDate"].ToString()),
                StatusId = int.Parse(rw["StatusId"].ToString()),
                SubmissionId = int.Parse(rw["SubmissionId"].ToString()),
                UpdatedBy = (rw["UpdatedBy"] != DBNull.Value) ? rw["UpdatedBy"].ToString() : string.Empty
            };
        }

        public EvaluationSubmission GetRow(IDataRecord rw)
        {
            DateTime? defDate = null;
            return new EvaluationSubmission
            {
                Area = new ScoreCardArea
                {
                    AreaAliasName = (rw["AreaAliasName"] != DBNull.Value) ? rw["AreaAliasName"].ToString() : string.Empty,
                    AreaId = (rw["AreaId"] != DBNull.Value) ? int.Parse(rw["AreaId"].ToString()) : 0,
                    AreaName = (rw["AreaName"] != DBNull.Value) ? rw["AreaName"].ToString() : string.Empty,
                },
                AreaId = int.Parse(rw["AreaId"].ToString()),
                AreaItemId = int.Parse(rw["AreaItemId"].ToString()),
                CapturerId = int.Parse(rw["CapturerId"].ToString()),
                DateCreated = DateTime.Parse(rw["DateCreated"].ToString()),
                DateSentOut = (rw["DateSentOut"] != DBNull.Value) ? DateTime.Parse(rw["DateSentOut"].ToString()) : defDate,
                DateUpdated = (rw["DateUpdated"] != DBNull.Value) ? DateTime.Parse(rw["DateUpdated"].ToString()) : defDate,
                IsInternal = bool.Parse(rw["IsInternal"].ToString()),
                Item = new AreaItem
                {
                    AreaItemId = int.Parse(rw["AreaItemId"].ToString()),
                    AreaItemName = (rw["AreaItemName"] != DBNull.Value) ? rw["AreaItemName"].ToString() : string.Empty
                },
                MaximumRateValue = Int16.Parse(rw["MaximumRateValue"].ToString()),
                OrgUnitId = int.Parse(rw["OrgUnitId"].ToString()),
                OveralComment = (rw["OveralComment"] != DBNull.Value) ? rw["OveralComment"].ToString() : string.Empty,
                PeriodDate = DateTime.Parse(rw["PeriodDate"].ToString()),
                StatusId = int.Parse(rw["StatusId"].ToString()),
                Status = new ScoreCardStatus
                {
                    StatusId = int.Parse(rw["StatusId"].ToString()),
                    StatusName = (rw["StatusName"] != DBNull.Value) ? rw["StatusName"].ToString() : string.Empty
                },
                SubmissionId = int.Parse(rw["SubmissionId"].ToString()),
                UpdatedBy = (rw["UpdatedBy"] != DBNull.Value) ? rw["UpdatedBy"].ToString() : string.Empty
            };
        }

        public AreaItem GetItemRow(IDataRecord rw)
        {
            return new AreaItem
            {
                AreaItemId = int.Parse(rw["AreaItemId"].ToString()),
                AreaItemName = rw["AreaItemName"].ToString()
            };
        }

        public EvaluationSubmission GetEvaluatorRow(IDataRecord rw)
        {
            DateTime? defDate = null;
            return new EvaluationSubmission
            {
                Area = new ScoreCardArea
                {
                    AreaAliasName = (rw["AreaAliasName"] != DBNull.Value) ? rw["AreaAliasName"].ToString() : string.Empty,
                    AreaId = (rw["AreaId"] != DBNull.Value) ? int.Parse(rw["AreaId"].ToString()) : 0,
                    AreaName = (rw["AreaName"] != DBNull.Value) ? rw["AreaName"].ToString() : string.Empty,
                },
                AreaId = int.Parse(rw["AreaId"].ToString()),
                AreaItemId = int.Parse(rw["AreaItemId"].ToString()),
                CapturerId = int.Parse(rw["CapturerId"].ToString()),
                DateCreated = DateTime.Parse(rw["DateCreated"].ToString()),
                DateSentOut = (rw["DateSentOut"] != DBNull.Value) ? DateTime.Parse(rw["DateSentOut"].ToString()) : defDate,
                DateUpdated = (rw["DateUpdated"] != DBNull.Value) ? DateTime.Parse(rw["DateUpdated"].ToString()) : defDate,
                Evaluator = new EvaluationSubmissionList
                {
                    ResourceId = int.Parse(rw["ResourceId"].ToString()),
                    EvaluatorId = long.Parse(rw["EvaluatorId"].ToString())
                },
                IsInternal = bool.Parse(rw["IsInternal"].ToString()),
                Item = new AreaItem
                {
                    AreaItemId = int.Parse(rw["AreaItemId"].ToString()),
                    AreaItemName = (rw["AreaItemName"] != DBNull.Value) ? rw["AreaItemName"].ToString() : string.Empty
                },
                MaximumRateValue = Int16.Parse(rw["MaximumRateValue"].ToString()),
                OrgUnitId = int.Parse(rw["OrgUnitId"].ToString()),
                OveralComment = (rw["OveralComment"] != DBNull.Value) ? rw["OveralComment"].ToString() : string.Empty,
                PeriodDate = DateTime.Parse(rw["PeriodDate"].ToString()),
                StatusId = int.Parse(rw["StatusId"].ToString()),
                Status = new ScoreCardStatus
                {
                    StatusId = int.Parse(rw["StatusId"].ToString()),
                    StatusName = (rw["StatusName"] != DBNull.Value) ? rw["StatusName"].ToString() : string.Empty
                },
                SubmissionId = int.Parse(rw["SubmissionId"].ToString()),
                UpdatedBy = (rw["UpdatedBy"] != DBNull.Value) ? rw["UpdatedBy"].ToString() : string.Empty

            };
        }

        public List<EvaluationSubmission> GetSubmissionsByStatus(int companyId, string statusIds)
        {
            List<EvaluationSubmission> obj = new List<EvaluationSubmission>();
            using (IDataReader data = Dal.GetByStatusId(companyId, statusIds))
            {
                while (data.Read())
                    obj.Add(GetRow(data));
            }
            return obj;
        }

        public List<EvaluationSubmission> GetSubmissionsByStatus(int orgUnitId, string statusIds, int capturerId)
        {
            List<EvaluationSubmission> obj = new List<EvaluationSubmission>();
            using (IDataReader data = Dal.GetByStatusId(orgUnitId, statusIds, capturerId))
            {
                while (data.Read())
                    obj.Add(GetRow(data));
            }
            return obj;
        }

        public List<EvaluationSubmission> GetAllByOrgUnitId(int orgUnitId)
        {
            List<EvaluationSubmission> obj = new List<EvaluationSubmission>();
            using (IDataReader data = Dal.GetAllByOrgUnitId(orgUnitId))
            {
                while (data.Read())
                    obj.Add(GetRow(data));
            }
            return obj;
        }

        public List<EvaluationSubmission> GetAllByCompanyId(int companyId)
        {
            List<EvaluationSubmission> obj = new List<EvaluationSubmission>();
            using (IDataReader data = Dal.GetAllByCompId(companyId))
            {
                while (data.Read())
                    obj.Add(GetRow(data));
            }
            return obj;
        }

        public List<AreaItem> GetEmployeesByOrgUnitId(int orgUnitId)
        {
            List<AreaItem> obj = new List<AreaItem>();
            using (IDataReader data = Dal.GetResourceByOrgUnitId(orgUnitId))
            {
                while (data.Read())
                    obj.Add(GetItemRow(data));
            }
            return obj;
        }

        public List<AreaItem> GetEmployeesByEmployeeId(int employeeId)
        {
            List<AreaItem> obj = new List<AreaItem>();
            using (IDataReader data = Dal.GetResourceByEmployeeId(employeeId))
            {
                while (data.Read())
                    obj.Add(GetItemRow(data));
            }
            return obj;
        }

        public List<AreaItem> GetEmployeesByCompanyId(int companyId)
        {
            List<AreaItem> obj = new List<AreaItem>();
            using (IDataReader data = Dal.GetResourceByCompanyId(companyId))
            {
                while (data.Read())
                    obj.Add(GetItemRow(data));
            }
            return obj;
        }

        public List<AreaItem> GetJobTitleEmployeesByOrgUnitId(int orgUnitId)
        {
            List<AreaItem> obj = new List<AreaItem>();
            using (IDataReader data = Dal.GetJobTitleResourceByOrgUnitId(orgUnitId))
            {
                while (data.Read())
                    obj.Add(GetItemRow(data));
            }
            return obj;
        }

        public List<AreaItem> GetJobTitleEmployeesByCompanyId(int companyId)
        {
            List<AreaItem> obj = new List<AreaItem>();
            using (IDataReader data = Dal.GetJobTitleResourceByCompanyId(companyId))
            {
                while (data.Read())
                    obj.Add(GetItemRow(data));
            }
            return obj;
        }

        public List<AreaItem> GetProjectsByCompanyId(int companyId)
        {
            List<AreaItem> obj = new List<AreaItem>();
            using (IDataReader data = Dal.GetProjectList(companyId))
            {
                while (data.Read())
                    obj.Add(GetItemRow(data));
            }
            return obj;
        }

        public List<AreaItem> GetProjectsByOwnerId(int ownerId)
        {
            List<AreaItem> obj = new List<AreaItem>();
            using (IDataReader data = Dal.GetProjectListByOwner(ownerId))
            {
                while (data.Read())
                    obj.Add(GetItemRow(data));
            }
            return obj;
        }

        public List<AreaItem> GetOrgUnitListByOwnerId(int ownerId)
        {
            List<AreaItem> obj = new List<AreaItem>();
            using (IDataReader data = Dal.GetOrgUnitListByOwner(ownerId))
            {
                while (data.Read())
                    obj.Add(GetItemRow(data));
            }
            return obj;
        }

        public List<AreaItem> GetOrgUnitsInHirachy(int orgUnitId)
        {
            List<AreaItem> obj = new List<AreaItem>();
            using (IDataReader data = Dal.GetOrgUnitList(orgUnitId))
            {
                while (data.Read())
                    obj.Add(GetItemRow(data));
            }
            return obj;
        }

        public List<EvaluationSubmission> GetPendingEvaluation(int resourceId, string statusIds)
        {
            List<EvaluationSubmission> obj = new List<EvaluationSubmission>();
            using (IDataReader data = Dal.GetPendingEvaluationList(resourceId, statusIds))
            {
                while (data.Read())
                    obj.Add(GetEvaluatorRow(data));
            }
            return obj;
        }

        public List<EvaluationSubmission> GetPendingExternalEvaluation(int resourceId, string statusIds)
        {
            List<EvaluationSubmission> obj = new List<EvaluationSubmission>();
            using (IDataReader data = Dal.GetPendingExternalEvaluationList(resourceId, statusIds))
            {
                while (data.Read())
                    obj.Add(GetEvaluatorRow(data));
            }
            return obj;
        }

        public List<EvaluationSubmission> GetPendingExternalEvaluation(string email, int companyId, string statusIds)
        {
            List<EvaluationSubmission> obj = new List<EvaluationSubmission>();
            using (IDataReader data = Dal.GetPendingExternalEvaluationList(email, companyId, statusIds))
            {
                while (data.Read())
                    obj.Add(GetEvaluatorRow(data));
            }
            return obj;
        }

        public List<AreaItem> GetEvaluationPeriods(int areaId, int areaItemId)
        {
            List<AreaItem> obj = new List<AreaItem>();
            using (IDataReader data = Dal.GetEvaluationDates(areaId, areaItemId))
            {
                while (data.Read())
                    obj.Add(new AreaItem
                    {
                        AreaItemDescription = data["PeriodDateDesc"].ToString(),
                        AreaItemName = data["PeriodDate"].ToString()
                    }
              );
            }
            return obj;
        }

        public List<AreaItem> GetEvaluationPeriods(int areaId, int areaItemId, bool isinternal)
        {
            List<AreaItem> obj = new List<AreaItem>();
            using (IDataReader data = Dal.GetEvaluationDates(areaId, areaItemId, isinternal))
            {
                while (data.Read())
                    obj.Add(new AreaItem
                    {
                        AreaItemDescription = data["PeriodDateDesc"].ToString(),
                        AreaItemName = data["PeriodDate"].ToString()
                    }
              );
            }
            return obj;
        }

        public EvaluationSubmission GetEvaluation(int areaId, int areaItemId, DateTime periodDate)
        {
            using (IDataReader data = Dal.GetEvaluation(areaId, areaItemId, periodDate))
            {
                return (data.Read()) ? GetSubmissionRow(data) : new EvaluationSubmission();
            }
        }

        public List<AreaItem> GetEmployeesByEmployeeId(int employeeId, int areaId)
        {
            List<AreaItem> obj = new List<AreaItem>();
            using (IDataReader data = Dal.GetResourceByEmployeeId(employeeId, areaId))
            {
                while (data.Read())
                    obj.Add(GetItemRow(data));
            }
            return obj;
        }

        public List<AreaItem> GetEmployeesByCompanyId(int companyId, int areaId)
        {
            List<AreaItem> obj = new List<AreaItem>();
            using (IDataReader data = Dal.GetResourceByCompanyId(companyId, areaId))
            {
                while (data.Read())
                    obj.Add(GetItemRow(data));
            }
            return obj;
        }
    }
}