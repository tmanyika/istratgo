﻿using System;
using System.Data;
using System.Collections.Generic;
using scorecard.implementations;
using ScoreCard.Common.Entities;

namespace ThreeSixty.Evaluation
{
    public class EvaluationSubmissionListBL : IEvaluationSubmissionList
    {
        public EvaluationSubmissionListDAL Dal
        {
            get { return new EvaluationSubmissionListDAL(Util.GetconnectionString()); }
        }

        public bool AddEvaluationResource(EvaluationSubmissionList obj)
        {
            int result = Dal.Save(obj);
            return (result > 0) ? true : false;
        }

        public bool AddEvaluationInternalResourceInBulk(EvaluationSubmissionList obj)
        {
            int result = Dal.SaveInternalBulk(obj);
            return (result > 0) ? true : false;
        }

        public bool DeleteResource(long evaluatorId)
        {
            int result = Dal.Delete(evaluatorId);
            return (result > 0) ? true : false;
        }

        public DataTable GetListSchema()
        {
            DataTable dT = new DataTable();

            dT.Columns.Add("EvaluatorId", typeof(Int64));
            dT.Columns.Add("SubmissionId", typeof(Int64));
            dT.Columns.Add("ResourceId", typeof(int));
            dT.Columns.Add("SubmittedScore", typeof(bool));
            dT.Columns.Add("EvaluatorWeight", typeof(decimal));
            dT.Columns.Add("CreatedBy", typeof(string));
            dT.Columns.Add("UpdatedBy", typeof(string));
            dT.AcceptChanges();
            return dT;
        }

        public EvaluationSubmissionList GetEvaulator(long evaluatorId)
        {
            using (IDataReader data = Dal.GetById(evaluatorId))
            {
                return (data.Read()) ? GetBriefRow(data) : new EvaluationSubmissionList();
            }
        }

        public EvaluationSubmissionList GetBriefRow(IDataRecord rw)
        {
            DateTime? defDate = null;
            decimal? defDec = null;

            return new EvaluationSubmissionList
            {
                AverageRatingValue = (rw["AverageRatingValue"] != DBNull.Value) ? decimal.Parse(rw["AverageRatingValue"].ToString()) : defDec,
                CreatedBy = (rw["CreatedBy"] != DBNull.Value) ? rw["CreatedBy"].ToString() : string.Empty,
                DateCreated = DateTime.Parse(rw["DateCreated"].ToString()),
                DateSubmitted = (rw["DateSubmitted"] != DBNull.Value) ? DateTime.Parse(rw["DateSubmitted"].ToString()) : defDate,
                EvaluatorId = long.Parse(rw["EvaluatorId"].ToString()),
                EvaluatorWeight = (rw["EvaluatorWeight"] != DBNull.Value) ? decimal.Parse(rw["EvaluatorWeight"].ToString()) : defDec,
                HasWeight = (rw["EvaluatorWeight"] != DBNull.Value),
                OverallComment = (rw["OverallComment"] != DBNull.Value) ? rw["OverallComment"].ToString() : string.Empty,
                ResourceId = int.Parse(rw["ResourceId"].ToString()),
                SubmissionId = long.Parse(rw["SubmissionId"].ToString()),
                SubmittedScore = bool.Parse(rw["SubmittedScore"].ToString())
            };
        }

        public EvaluationSubmissionList GetRow(IDataRecord rw)
        {
            DateTime? defDate = null;
            decimal? defDec = null;
            int defInt = 0;

            return new EvaluationSubmissionList
            {
                DateCreated = DateTime.Parse(rw["DateCreated"].ToString()),
                DateSubmitted = (rw["DateSubmitted"] != DBNull.Value) ? DateTime.Parse(rw["DateSubmitted"].ToString()) : defDate,
                EvaluatorId = long.Parse(rw["EvaluatorId"].ToString()),
                EvaluatorWeight = (rw["EvaluatorWeight"] != DBNull.Value) ? decimal.Parse(rw["EvaluatorWeight"].ToString()) : defDec,
                Person = new EvaluationInternalPeople
                {
                    CompanyId = (rw["CompanyId"] != DBNull.Value) ? int.Parse(rw["CompanyId"].ToString()) : defInt,
                    Email = (rw["Email"] != DBNull.Value) ? rw["Email"].ToString() : string.Empty,
                    FirstName = (rw["FirstName"] != DBNull.Value) ? rw["FirstName"].ToString() : string.Empty,
                    LastName = (rw["LastName"] != DBNull.Value) ? rw["LastName"].ToString() : string.Empty,
                    MiddleName = (rw["MiddleName"] != DBNull.Value) ? rw["MiddleName"].ToString() : string.Empty,
                    PersonId = int.Parse(rw["ResourceId"].ToString())
                },
                ResourceId = int.Parse(rw["ResourceId"].ToString()),
                SubmissionId = long.Parse(rw["SubmissionId"].ToString()),
                SubmittedScore = bool.Parse(rw["SubmittedScore"].ToString())
            };
        }

        public List<EvaluationSubmissionList> GetEvaulatorList(long submissionId)
        {
            List<EvaluationSubmissionList> obj = new List<EvaluationSubmissionList>();
            using (IDataReader data = Dal.Get(submissionId))
            {
                while (data.Read())
                    obj.Add(GetRow(data));
            }
            return obj;
        }

        public EvaluationInternalPeople GetInternalRecipient(IDataRecord rw)
        {
            return new EvaluationInternalPeople
            {
                CompanyId = int.Parse(rw["CompanyId"].ToString()),
                PersonId = int.Parse(rw["EmployeeId"].ToString()),
                DepartmentId = int.Parse(rw["DepartmentId"].ToString()),
                Department = rw["Department"].ToString(),
                Email = rw["Email"].ToString(),
                FullName = rw["FullName"].ToString()
            };
        }

        public List<EvaluationInternalPeople> GetInternalPeopleByCompanyId(int companyId)
        {
            List<EvaluationInternalPeople> obj = new List<EvaluationInternalPeople>();
            using (IDataReader data = Dal.GetInternaCompanyPeopleList(companyId))
            {
                while (data.Read())
                    obj.Add(GetInternalRecipient(data));
            }
            return obj;
        }

        public List<EvaluationInternalPeople> GetInternalPeopleByOrgUnitId(int orgUnitId)
        {
            List<EvaluationInternalPeople> obj = new List<EvaluationInternalPeople>();
            using (IDataReader data = Dal.GetInternalOrgUnitPeopleList(orgUnitId))
            {
                while (data.Read())
                    obj.Add(GetInternalRecipient(data));
            }
            return obj;
        }

        public List<EvaluationInternalPeople> GetInternalSentOutPeopleById(string personIds)
        {
            List<EvaluationInternalPeople> obj = new List<EvaluationInternalPeople>();
            using (IDataReader data = Dal.GetInternalSentOutPeopleList(personIds))
            {
                while (data.Read())
                    obj.Add(GetInternalRecipient(data));
            }
            return obj;
        }
    }
}