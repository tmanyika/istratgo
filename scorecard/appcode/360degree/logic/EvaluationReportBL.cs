﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using scorecard.implementations;
using ScoreCard.Common.Entities;
using System.Configuration;
using vb = Microsoft.VisualBasic;

namespace ThreeSixty.Evaluation
{
    public class EvaluationReportBL : IEvaluationReport
    {
        public EvaluationReportDAL Dal
        {
            get { return new EvaluationReportDAL(Util.GetconnectionString()); }
        }

        public EvaluationReport GetPeerRow(IDataRecord rw)
        {
            decimal? defDec = null;
            return new EvaluationReport
            {
                CompetencyName = rw["CompetencyName"].ToString(),
                DirectReportRating = (rw["DirectReportRating"] == DBNull.Value) ? defDec : Math.Round(decimal.Parse(rw["DirectReportRating"].ToString()), 2),
                ManagerRating = (rw["ManagerRating"] == DBNull.Value) ? defDec : Math.Round(decimal.Parse(rw["ManagerRating"].ToString()), 2),
                PeerRating = (rw["PeerRating"] == DBNull.Value) ? defDec : Math.Round(decimal.Parse(rw["PeerRating"].ToString()), 2),
                SelfRating = (rw["SelfRating"] == DBNull.Value) ? defDec : Math.Round(decimal.Parse(rw["SelfRating"].ToString()), 2),
                WeightedAvgRating = (rw["WeightedAvgRating"] == DBNull.Value) ? defDec : Math.Round(decimal.Parse(rw["WeightedAvgRating"].ToString()), 2),
                TargetWeightRating = (rw["TargetWeightRating"] == DBNull.Value) ? defDec : Math.Round(decimal.Parse(rw["TargetWeightRating"].ToString()), 2)
            };
        }

        public EvaluationReport GetSelfRow(IDataRecord rw)
        {
            decimal? defDec = null;
            return new EvaluationReport
            {
                CompetencyName = rw["CompetencyName"].ToString(),
                AverageExternalRating = (rw["AverageExternalRating"] == DBNull.Value) ? defDec : Math.Round(decimal.Parse(rw["AverageExternalRating"].ToString()), 2),
                AverageInternalRating = (rw["AverageInternalRating"] == DBNull.Value) ? defDec : Math.Round(decimal.Parse(rw["AverageInternalRating"].ToString()), 2),
                SelfRating = (rw["SelfRating"] == DBNull.Value) ? defDec : Math.Round(decimal.Parse(rw["SelfRating"].ToString()), 2),
                WeightedAvgRating = (rw["WeightedAvgRating"] == DBNull.Value) ? defDec : Math.Round(decimal.Parse(rw["WeightedAvgRating"].ToString()), 2),
                TargetWeightRating = (rw["TargetWeightRating"] == DBNull.Value) ? defDec : Math.Round(decimal.Parse(rw["TargetWeightRating"].ToString()), 2)
            };
        }

        public EvaluationReport GetDetailedRow(IDataRecord rw)
        {
            decimal? defDec = null;
            return new EvaluationReport
            {
                AreaId = int.Parse(rw["AreaId"].ToString()),
                AreaItemId = int.Parse(rw["AreaItemId"].ToString()),
                CompetencyName = rw["CompetencyName"] != DBNull.Value ? rw["CompetencyName"].ToString() : string.Empty,
                EvaluatorId = long.Parse(rw["EvaluatorId"].ToString()),
                FullName = vb.Strings.StrConv(string.Format("{0} {1} {2}", rw["FirstName"], rw["MiddleName"], rw["LastName"]), vb.VbStrConv.ProperCase),
                OverallComment = rw["OverallComment"] != DBNull.Value ? rw["OverallComment"].ToString() : string.Empty,
                RatingValue = (rw["RatingValue"] == DBNull.Value) ? defDec : Math.Round(decimal.Parse(rw["RatingValue"].ToString()), 2),
            };
        }

        public List<EvaluationReport> Get360SelfAverageReport(int areaId, int areaItemId, DateTime periodDate)
        {
            List<EvaluationReport> obj = new List<EvaluationReport>();
            using (IDataReader data = Dal.GetSelf(areaId, areaItemId, periodDate))
            {
                while (data.Read())
                    obj.Add(GetSelfRow(data));
            }
            return obj;
        }

        public List<EvaluationReport> Get360UserPeerReport(int areaId, int areaItemId, DateTime periodDate)
        {
            List<EvaluationReport> obj = new List<EvaluationReport>();
            using (IDataReader data = Dal.GetPeer(areaId, areaItemId, periodDate))
            {
                while (data.Read())
                    obj.Add(GetPeerRow(data));
            }
            return obj;
        }

        public List<EvaluationReport> Get360UserSelfReport(int areaId, int areaItemId, DateTime periodDate)
        {
            List<EvaluationReport> obj = new List<EvaluationReport>();
            using (IDataReader data = Dal.GetSelf(areaId, areaItemId, periodDate))
            {
                while (data.Read())
                    obj.Add(GetSelfRow(data));
            }
            return obj;
        }

        public List<EvaluationReport> Get360OrgUnitSelfReport(int areaId, int areaItemId, DateTime periodDate)
        {
            List<EvaluationReport> obj = new List<EvaluationReport>();
            using (IDataReader data = Dal.GetOrgUnitSelf(areaId, areaItemId, periodDate))
            {
                while (data.Read())
                    obj.Add(GetSelfRow(data));
            }
            return obj;
        }

        public List<EvaluationReport> Get360OrgUnitPeerReport(int areaId, int areaItemId, DateTime periodDate)
        {
            List<EvaluationReport> obj = new List<EvaluationReport>();
            using (IDataReader data = Dal.GetOrgUnitPeer(areaId, areaItemId, periodDate))
            {
                while (data.Read())
                    obj.Add(GetPeerRow(data));
            }
            return obj;
        }

        public List<EvaluationReport> Get360ProjectSelfReport(int areaId, int areaItemId, DateTime periodDate)
        {
            List<EvaluationReport> obj = new List<EvaluationReport>();
            using (IDataReader data = Dal.GetProjectSelf(areaId, areaItemId, periodDate))
            {
                while (data.Read())
                    obj.Add(GetSelfRow(data));
            }
            return obj;
        }

        public List<EvaluationReport> Get360ProjectPeerReport(int areaId, int areaItemId, DateTime periodDate)
        {
            List<EvaluationReport> obj = new List<EvaluationReport>();
            using (IDataReader data = Dal.GetProjectPeer(areaId, areaItemId, periodDate))
            {
                while (data.Read())
                    obj.Add(GetPeerRow(data));
            }
            return obj;
        }

        public string[] GetOrgUnitSeriesTitlesSelf()
        {
            string[] title = ConfigurationManager.AppSettings["OrgUnitSelfSeriesTitle"].ToString().Split(',');
            return title;
        }

        public string[] GetOrgUnitSeriesTitlePeer()
        {
            string[] title = ConfigurationManager.AppSettings["OrgUnitPeerSeriesTitle"].ToString().Split(',');
            return title;
        }

        public List<EvaluationReport> Get360DetailedReport(int areaId, int areaItemId, DateTime periodDate, bool isInternal)
        {
            List<EvaluationReport> obj = new List<EvaluationReport>();
            using (IDataReader data = Dal.GetDetailed(areaId, areaItemId, periodDate, isInternal))
            {
                while (data.Read())
                    obj.Add(GetDetailedRow(data));
            }
            return obj;
        }

        public List<EvaluationReport> Get360FullDetailedReport(int areaId, int areaItemId, DateTime periodDate)
        {
            List<EvaluationReport> obj = new List<EvaluationReport>();
            using (IDataReader data = Dal.GetDetailed(areaId, areaItemId, periodDate))
            {
                while (data.Read())
                    obj.Add(GetDetailedRow(data));
            }
            return obj;
        }

        DataTable GetDataSchema(List<EvaluationReport> data)
        {
            DataTable dT = new DataTable();
            dT.Columns.Add("Full Name", typeof(string));

            List<string> moreColumns = data.Select(e => e.CompetencyName).Distinct().ToList();
            foreach (var col in moreColumns)
            {
                dT.Columns.Add(col, typeof(string));
            }

            dT.Columns.Add("Average", typeof(string));
            dT.Columns.Add("Comment", typeof(string));
            dT.AcceptChanges();
            return dT;
        }

        public DataTable GetDetailedData(List<EvaluationReport> data)
        {
            DataTable dT = GetDataSchema(data);
            var evaluators = data.DistinctBy(e => e.EvaluatorId).ToList();

            foreach (var item in evaluators)
            {
                long evaluatorId = item.EvaluatorId;
                DataRow nRow = dT.NewRow();

                nRow["Full Name"] = item.FullName;
                var comment = string.Empty;
                var evalData = data.Where(e => e.EvaluatorId == evaluatorId).ToList();
                foreach (var evalItem in evalData)
                {
                    nRow[evalItem.CompetencyName] = evalItem.RatingValue;
                    comment = evalItem.OverallComment;
                }
                nRow["Comment"] = comment;

                var average = evalData.Where(c => c.RatingValue != null).Select(d => d.RatingValue).Average();
                nRow["Average"] = Math.Round((decimal)average, 2);
                dT.Rows.Add(nRow);
                nRow = null;
            }

            dT.AcceptChanges();

            decimal totaleValuators = evaluators.Count;
            int len = dT.Columns.Count;
            decimal[] avg = new decimal[len];
            
            DataRow lRow = dT.NewRow();
            lRow[0] = "Average";

            foreach (DataRow rw in dT.Rows)
            {
                for (int i = 1; i < len - 1; i++)
                {
                    avg[i] = avg[i] + decimal.Parse(rw[i].ToString());
                }
            }

            if (totaleValuators > 0)
            {
                for (int i = 1; i < len - 1; i++)
                {
                    decimal averageRating = Math.Round((avg[i] / totaleValuators),2);
                    lRow[i] = averageRating;
                }
            }
            dT.Rows.Add(lRow);
            lRow = null;

            dT.AcceptChanges();
            return dT;
        }
    }
}