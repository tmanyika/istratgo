﻿using System;
using System.Data;
using System.Collections.Generic;
using scorecard.implementations;
using ScoreCard.Common.Entities;

namespace ThreeSixty.Evaluation
{
    public class EvaluationCompetencyBL : IEvaluationCompetency
    {
        #region Properties

        public string ConnectionString
        {
            get { return Util.GetconnectionString(); }
        }

        public EvaluationCompetencyDAL Dal
        {
            get { return new EvaluationCompetencyDAL(ConnectionString); }
        }

        #endregion

        #region Default Constructors

        public EvaluationCompetencyBL() { }

        #endregion

        #region Public Methods & Functions

        public bool AddCompetency(EvaluationCompetency obj)
        {
            int result = Dal.Save(obj);
            return result > 0 ? true : false;
        }

        public bool UpdateCompetency(EvaluationCompetency obj)
        {
            int result = Dal.Update(obj);
            return result > 0 ? true : false;
        }

        public bool SetStatus(EvaluationCompetency obj)
        {
            int result = Dal.Deactivate(obj);
            return result > 0 ? true : false;
        }

        public List<EvaluationCompetency> GetByStatus(int companyId, bool active)
        {
            List<EvaluationCompetency> obj = new List<EvaluationCompetency>();
            using (IDataReader reader = Dal.Get(companyId, active))
            {
                while (reader.Read())
                    obj.Add(GetRow(reader));
            };
            return obj;
        }

        public List<EvaluationCompetency> GetByArea(int companyId, int areaId, bool active)
        {
            List<EvaluationCompetency> obj = new List<EvaluationCompetency>();
            using (IDataReader reader = Dal.Get(companyId, areaId, active))
            {
                while (reader.Read())
                    obj.Add(GetRow(reader));
            };
            return obj;
        }

        public List<EvaluationCompetency> GetAllCompentencies(int companyId)
        {
            List<EvaluationCompetency> obj = new List<EvaluationCompetency>();
            using (IDataReader reader = Dal.GetAll(companyId))
            {
                while (reader.Read())
                    obj.Add(GetRow(reader));
            };
            return obj;
        }

        public EvaluationCompetency GetById(int competencyId)
        {
            using (IDataReader reader = Dal.Get(competencyId))
            {
                if (reader.Read())
                    return GetRow(reader);
            }
            return new EvaluationCompetency();
        }

        public EvaluationCompetency GetRow(IDataRecord rw)
        {
            DateTime? defDate = null;
            return new EvaluationCompetency
            {
                Active = bool.Parse(rw["Active"].ToString()),
                AreaId = int.Parse(rw["AreaId"].ToString()),
                Area = new ScoreCardArea
                {
                    AreaId = int.Parse(rw["AreaId"].ToString()),
                    AreaName = rw["AreaName"].ToString()
                },
                CompanyId = int.Parse(rw["CompanyId"].ToString()),
                CompetencyDescription = (rw["CompetencyDescription"] == DBNull.Value) ?
                                        string.Empty : rw["CompetencyDescription"].ToString(),
                CompetencyId = int.Parse(rw["CompetencyId"].ToString()),
                CompetencyName = rw["CompetencyName"].ToString(),
                CreatedBy = (rw["CreatedBy"] == DBNull.Value) ?
                                        string.Empty : rw["CreatedBy"].ToString(),
                DateCreated = DateTime.Parse(rw["DateCreated"].ToString()),
                DateUpdated = (rw["DateUpdated"] == DBNull.Value) ?
                                    defDate : DateTime.Parse(rw["DateUpdated"].ToString()),
                UpdatedBy = (rw["UpdatedBy"] == DBNull.Value) ?
                                    string.Empty : rw["UpdatedBy"].ToString()
            };
        }

        public EvaluationCompetency GetBriefRow(IDataRecord rw)
        {
            return new EvaluationCompetency
            {
                CompetencyId = (rw["CompetencyId"] == DBNull.Value) ?
                                    0 : long.Parse(rw["CompetencyId"].ToString()),
                CompetencyName = (rw["CompetencyName"] == DBNull.Value) ?
                                    string.Empty : rw["CompetencyName"].ToString()
            };
        }

        #endregion
    }
}