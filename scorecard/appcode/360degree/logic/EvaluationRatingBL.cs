﻿using System;
using System.Data;
using System.Collections.Generic;
using scorecard.implementations;

namespace ThreeSixty.Evaluation
{
    public class EvaluationRatingBL : IEvaluationRating
    {
        public EvaluationRatingDAL Dal
        {
            get { return new EvaluationRatingDAL(Util.GetconnectionString()); }
        }

        public bool AddEvaluationRating(EvaluationRating obj)
        {
            int result = Dal.Save(obj);
            return (result > 0) ? true : false;
        }

        public bool UpdateEvaluationRating(EvaluationRating obj)
        {
            int result = Dal.Update(obj);
            return (result > 0) ? true : false;
        }

        public bool DeactivateEvalRating(EvaluationRating obj)
        {
            int result = Dal.Deactivate(obj);
            return (result > 0) ? true : false;
        }

        public Int16 GetMaxCompanyRating(int companyId, bool active)
        {
            object result = Dal.GetMax(companyId, active);
            return (result == DBNull.Value) ? (Int16)0 : Int16.Parse(result.ToString());
        }

        public EvaluationRating GetById(int ratingId)
        {
            using (IDataReader data = Dal.Get(ratingId))
            {
                if (data.Read())
                    return GetRow(data);
            }
            return new EvaluationRating();
        }

        public EvaluationRating GetRow(IDataRecord rw)
        {
            DateTime? defDate = null;
            return new EvaluationRating
            {
                Active = bool.Parse(rw["Active"].ToString()),
                CompanyId = int.Parse(rw["CompanyId"].ToString()),
                CreatedBy = rw["CreatedBy"].ToString(),
                DateCreated = DateTime.Parse(rw["DateCreated"].ToString()),
                DateUpdated = (rw["DateUpdated"] != DBNull.Value) ? DateTime.Parse(rw["DateUpdated"].ToString()) : defDate,
                RatingDescription = (rw["RatingDescription"] != DBNull.Value) ? rw["RatingDescription"].ToString() : string.Empty,
                RatingId = int.Parse(rw["RatingId"].ToString()),
                RatingValue = Int16.Parse(rw["RatingValue"].ToString()),
                UpdatedBy = (rw["UpdatedBy"] != DBNull.Value) ? rw["UpdatedBy"].ToString() : string.Empty,
            };
        }

        public List<EvaluationRating> GetAll(int companyId)
        {
            List<EvaluationRating> obj = new List<EvaluationRating>();
            using (IDataReader data = Dal.GetAll(companyId))
            {
                while (data.Read())
                    obj.Add(GetRow(data));
            }
            return obj;
        }

        public List<EvaluationRating> GetByStatus(int companyId, bool active)
        {
            List<EvaluationRating> obj = new List<EvaluationRating>();
            using (IDataReader data = Dal.Get(companyId, active))
            {
                while (data.Read())
                    obj.Add(GetRow(data));
            }
            return obj;
        }
    }
}