﻿using System;
using System.Data;
using System.Collections.Generic;
using scorecard.implementations;
using ScoreCard.Common.Entities;

namespace ThreeSixty.Evaluation
{
    public class EvaluationScoreBL : IEvaluationScore
    {
        public EvaluationScoreDAL Dal
        {
            get { return new EvaluationScoreDAL(Util.GetconnectionString()); }
        }

        public List<EvaluationScore> GetEmployeeScores(long submissionId, long evaluatorId)
        {
            List<EvaluationScore> obj = new List<EvaluationScore>();
            using (IDataReader data = Dal.GetEmployeeListScore(submissionId, evaluatorId))
            {
                while (data.Read())
                    obj.Add(GetRow(data));
            }
            return obj;
        }

        public List<EvaluationScore> GetJobTitleScores(long submissionId, long evaluatorId)
        {
            List<EvaluationScore> obj = new List<EvaluationScore>();
            using (IDataReader data = Dal.GetJobTitleListScore(submissionId, evaluatorId))
            {
                while (data.Read())
                    obj.Add(GetRow(data));
            }
            return obj;
        }

        public List<EvaluationScore> GetProjectScores(long submissionId, long evaluatorId)
        {
            List<EvaluationScore> obj = new List<EvaluationScore>();
            using (IDataReader data = Dal.GetProjectListScore(submissionId, evaluatorId))
            {
                while (data.Read())
                    obj.Add(GetRow(data));
            }
            return obj;
        }

        public List<EvaluationScore> GetOrgUnitScores(long submissionId, long evaluatorId)
        {
            List<EvaluationScore> obj = new List<EvaluationScore>();
            using (IDataReader data = Dal.GetOrgUnitListScore(submissionId, evaluatorId))
            {
                while (data.Read())
                    obj.Add(GetRow(data));
            }
            return obj;
        }

        public EvaluationScore GetRow(IDataRecord rw)
        {
            Int16? defShort = null;
            decimal? defDec = null;
            return new EvaluationScore
            {
                Competency = new EvaluationCompetency
                {
                    CompetencyDescription = (rw["CompetencyDescription"] != DBNull.Value) ? rw["CompetencyDescription"].ToString() : string.Empty,
                    CompetencyName = (rw["CompetencyName"] != DBNull.Value) ? rw["CompetencyName"].ToString() : string.Empty
                },
                EvaluationId = long.Parse(rw["EvaluationId"].ToString()),
                EvaluatorId = long.Parse(rw["EvaluatorId"].ToString()),
                RatingValue = (rw["RatingValue"] != DBNull.Value) ? Int16.Parse(rw["RatingValue"].ToString()) : defShort,
                ScoreId = (rw["ScoreId"] != DBNull.Value) ? long.Parse(rw["ScoreId"].ToString()) : 0,
                SubmissionId = int.Parse(rw["SubmissionId"].ToString()),
                WeightedRatingValue = (rw["WeightedRatingValue"] != DBNull.Value) ? decimal.Parse(rw["WeightedRatingValue"].ToString()) : defDec,
                WeightVal = (rw["WeightVal"] != DBNull.Value) ? decimal.Parse(rw["WeightVal"].ToString()) : defDec
            };
        }

        public DataTable GetSchema()
        {
            DataTable dT = new DataTable();

            dT.Columns.Add("ScoreId", typeof(Int64));
            dT.Columns.Add("SubmissionId", typeof(Int64));
            dT.Columns.Add("EvaluationId", typeof(Int64));
            dT.Columns.Add("EvaluatorId", typeof(Int64));
            dT.Columns.Add("WeightVal", typeof(decimal));
            dT.Columns.Add("RatingValue", typeof(Int16));
            dT.Columns.Add("WeightedRatingValue", typeof(decimal));
            dT.Columns.Add("CreatedBy", typeof(string));
            dT.Columns.Add("UpdatedBy", typeof(string));
            dT.AcceptChanges();

            return dT;
        }

        public bool SubmitScore(long submissionId, long evaluatorId, bool submittedScore, string comment, string xmlData)
        {
            int result = Dal.Save(submissionId, evaluatorId, submittedScore, comment, xmlData);
            return (result > 0);
        }
    }
}