﻿using System;
using System.Data;
using System.Collections.Generic;

namespace ThreeSixty.Evaluation
{
    public interface IEvaluationScore
    {
        List<EvaluationScore> GetJobTitleScores(long submissionId, long evaluatorId);
        List<EvaluationScore> GetEmployeeScores(long submissionId, long evaluatorId);
        List<EvaluationScore> GetProjectScores(long submissionId, long evaluatorId);
        List<EvaluationScore> GetOrgUnitScores(long submissionId, long evaluatorId);

        EvaluationScore GetRow(IDataRecord rw);
        DataTable GetSchema();

        bool SubmitScore(long submissionId, long evaluatorId, bool submittedScore, string overallComment, string xmlData);
    }
}
