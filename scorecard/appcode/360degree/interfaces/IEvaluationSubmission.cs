﻿using System;
using ScoreCard.Common.Entities;
using System.Collections.Generic;
using System.Data;

namespace ThreeSixty.Evaluation
{
    public interface IEvaluationSubmission
    {
        long AddSubmission(EvaluationSubmission obj);

        bool UpdateSubmissionPeriod(EvaluationSubmission obj);
        bool SetStatus(EvaluationSubmission obj);
        bool DeleteSubmission(long submissionId);
        bool EvaluationExist(EvaluationSubmission obj);

        AreaItem GetItemRow(IDataRecord rw);
        EvaluationSubmission GetRow(IDataRecord rw);
        EvaluationSubmission GetSubmissionRow(IDataRecord rw);
        EvaluationSubmission GetEvaluatorRow(IDataRecord rw);
        EvaluationSubmission GetEvaluation(int areaId, int areaItemId, DateTime periodDate);

        /// <summary>
        /// Get all Submissions by statuses
        /// </summary>
        /// <param name="companyId">Company Id</param>
        /// <param name="statusIds">Status Ids. If multiple sparate them with a comma</param>
        /// <returns>a List of EvaluationSubmission type</returns>
        List<EvaluationSubmission> GetSubmissionsByStatus(int companyId, string statusIds);

        /// <summary>
        /// Get all Submissions by statuses and capturer Id
        /// </summary>
        /// <param name="orgUnitId">Organisation Unit Id</param>
        /// <param name="statusIds">Status Ids. If multiple sparate them with a comma</param>
        /// <param name="capturerId">Capturer Id / User Id / Employee Id</param>
        /// <returns>a List of EvaluationSubmission type</returns>
        List<EvaluationSubmission> GetSubmissionsByStatus(int orgUnitId, string statusIds, int capturerId);

        List<EvaluationSubmission> GetAllByOrgUnitId(int orgUnitId);
        List<EvaluationSubmission> GetAllByCompanyId(int companyId);

        List<AreaItem> GetEmployeesByOrgUnitId(int orgUnitId);
        List<AreaItem> GetEmployeesByCompanyId(int companyId);
        List<AreaItem> GetEmployeesByEmployeeId(int employeeId);
        List<AreaItem> GetEmployeesByCompanyId(int companyId, int areaId);
        List<AreaItem> GetEmployeesByEmployeeId(int employeeId, int areaId);

        List<AreaItem> GetOrgUnitListByOwnerId(int ownerId);
        List<AreaItem> GetOrgUnitsInHirachy(int orgUnitId);

        List<AreaItem> GetJobTitleEmployeesByOrgUnitId(int orgUnitId);
        List<AreaItem> GetJobTitleEmployeesByCompanyId(int companyId);

        List<AreaItem> GetProjectsByCompanyId(int companyId);
        List<AreaItem> GetProjectsByOwnerId(int ownerId);


        /// <summary>
        /// Gets all pending evaluation for an evaluator based on status Ids
        /// </summary>
        /// <param name="resourceId">User Id /Person Id/ Employee Id</param>
        /// <param name="statusIds">Status Ids. If multiple sparate them with a comma</param>
        /// <returns>a List of EvaluationSubmission type</returns>
        List<EvaluationSubmission> GetPendingEvaluation(int resourceId, string statusIds);

        /// <summary>
        /// Gets all pending evaluation for an evaluator based on status Ids
        /// </summary>
        /// <param name="email">Email Address of the Evaluator</param>
        /// <param name="statusIds">Status Ids. If multiple sparate them with a comma</param>
        /// <returns>a List of EvaluationSubmission type</returns>
        List<EvaluationSubmission> GetPendingExternalEvaluation(string email, int companyId, string statusIds);
        List<EvaluationSubmission> GetPendingExternalEvaluation(int resourceId, string statusIds);

        List<AreaItem> GetEvaluationPeriods(int areaId, int areaItemId);
        List<AreaItem> GetEvaluationPeriods(int areaId, int areaItemId, bool isinternal);
    }
}
