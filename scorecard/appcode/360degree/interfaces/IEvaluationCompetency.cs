﻿using System;
using System.Data;
using System.Collections.Generic;

namespace ThreeSixty.Evaluation
{
    public interface IEvaluationCompetency
    {
        bool AddCompetency(EvaluationCompetency obj);
        bool UpdateCompetency(EvaluationCompetency obj);
        bool SetStatus(EvaluationCompetency obj);

        List<EvaluationCompetency> GetByStatus(int companyId, bool active);
        List<EvaluationCompetency> GetByArea(int companyId, int areaId, bool active);
        List<EvaluationCompetency> GetAllCompentencies(int companyId);

        EvaluationCompetency GetById(int competencyId);
        EvaluationCompetency GetRow(IDataRecord rw);
        EvaluationCompetency GetBriefRow(IDataRecord rw);
    }
}
