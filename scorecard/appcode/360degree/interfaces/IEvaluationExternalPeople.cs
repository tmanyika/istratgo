﻿using System;
using System.Data;
using System.Collections.Generic;

namespace ThreeSixty.Evaluation
{
    public interface IEvaluationExternalPeople
    {
        bool AddEvaluators(EvaluationExternalPeople obj);

        List<EvaluationInternalPeople> GetExternalSentOutPeopleById(int companyId, string emailList);
        DataTable GetSchema(int companyId);
    }
}
