﻿using System;
using System.Data;
using System.Collections.Generic;
using scorecard.entities;

namespace ThreeSixty.Evaluation
{
    public interface IEvaluationRating
    {
        bool AddEvaluationRating(EvaluationRating obj);
        bool UpdateEvaluationRating(EvaluationRating obj);
        bool DeactivateEvalRating(EvaluationRating obj);

        Int16 GetMaxCompanyRating(int companyId, bool active);

        EvaluationRating GetById(int ratingId);
        EvaluationRating GetRow(IDataRecord rw);

        List<EvaluationRating> GetAll(int companyId);
        List<EvaluationRating> GetByStatus(int companyId, bool active);
    }
}
