﻿using System;
using System.Collections.Generic;
using System.Data;

namespace ThreeSixty.Evaluation
{
    public interface IEvaluationSubmissionList
    {
        bool AddEvaluationResource(EvaluationSubmissionList obj);
        bool AddEvaluationInternalResourceInBulk(EvaluationSubmissionList obj);
        bool DeleteResource(long evaluatorId);

        DataTable GetListSchema();
        EvaluationSubmissionList GetRow(IDataRecord rw);
        EvaluationSubmissionList GetEvaulator(long evaluatorId);
        EvaluationInternalPeople GetInternalRecipient(IDataRecord rw);

        List<EvaluationSubmissionList> GetEvaulatorList(long submissionId);
        List<EvaluationInternalPeople> GetInternalPeopleByCompanyId(int companyId);
        List<EvaluationInternalPeople> GetInternalPeopleByOrgUnitId(int orgUnitId);
        List<EvaluationInternalPeople> GetInternalSentOutPeopleById(string personIds);
    }
}
