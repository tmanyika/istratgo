﻿using System;
using System.Data;
using System.Collections.Generic;
using scorecard.entities;

namespace ThreeSixty.Evaluation
{
    public interface IEvaluation
    {
        bool AddEvaluation(Evaluation obj);
        bool UpdateEvaluation(Evaluation obj);
        bool DeactivateEvaluation(Evaluation obj);

        Evaluation GetRow(IDataRecord rw);
        List<Evaluation> GetByStatus(int companyId, int areaId, int areaValueId, bool active);
    }
}
