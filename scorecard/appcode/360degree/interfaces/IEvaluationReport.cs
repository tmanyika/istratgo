﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ThreeSixty.Evaluation
{
    public interface IEvaluationReport
    {
        string[] GetOrgUnitSeriesTitlesSelf();
        string[] GetOrgUnitSeriesTitlePeer();

        EvaluationReport GetSelfRow(IDataRecord rw);
        EvaluationReport GetPeerRow(IDataRecord rw);

        List<EvaluationReport> Get360UserSelfReport(int areaId, int areaItemId, DateTime periodDate);
        List<EvaluationReport> Get360UserPeerReport(int areaId, int areaItemId, DateTime periodDate);

        List<EvaluationReport> Get360OrgUnitSelfReport(int areaId, int areaItemId, DateTime periodDate);
        List<EvaluationReport> Get360OrgUnitPeerReport(int areaId, int areaItemId, DateTime periodDate);

        List<EvaluationReport> Get360ProjectSelfReport(int areaId, int areaItemId, DateTime periodDate);
        List<EvaluationReport> Get360ProjectPeerReport(int areaId, int areaItemId, DateTime periodDate);

        List<EvaluationReport> Get360DetailedReport(int areaId, int areaItemId, DateTime periodDate, bool isInternal);
        List<EvaluationReport> Get360FullDetailedReport(int areaId, int areaItemId, DateTime periodDate);

        DataTable GetDetailedData(List<EvaluationReport> data);
    }
}
