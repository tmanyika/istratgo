﻿using System;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

namespace ThreeSixty.Evaluation
{
    public class EvaluationReportDAL
    {
        #region Variables

        private string _conString;

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return _conString; }
            set { _conString = value; }
        }

        #endregion

        #region Default Constructors

        public EvaluationReportDAL(string conString)
        {
            _conString = conString;
        }

        #endregion

        #region Public Methods and Functions

        public IDataReader GetSelf(int areaId, int areaItemId, DateTime periodDate)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EvaluationReport_GetSelf", areaId, areaItemId, periodDate);
        }

        public IDataReader GetPeer(int areaId, int areaItemId, DateTime periodDate)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EvaluationReport_GetPeer", areaId, areaItemId, periodDate);
        }

        public IDataReader GetOrgUnitSelf(int areaId, int areaItemId, DateTime periodDate)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EvaluationReport_GetOrgUnitSelf", areaId, areaItemId, periodDate);
        }

        public IDataReader GetOrgUnitPeer(int areaId, int areaItemId, DateTime periodDate)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EvaluationReport_GetOrgUnitPeer", areaId, areaItemId, periodDate);
        }

        public IDataReader GetProjectSelf(int areaId, int areaItemId, DateTime periodDate)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EvaluationReport_GetProjectSelf", areaId, areaItemId, periodDate);
        }

        public IDataReader GetProjectPeer(int areaId, int areaItemId, DateTime periodDate)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EvaluationReport_GetProjectPeer", areaId, areaItemId, periodDate);
        }

        public IDataReader GetDetailed(int areaId, int areaItemId, DateTime periodDate, bool isInternal)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EvaluationReport_GetDetailedByType", areaId, areaItemId, periodDate, isInternal);
        }

        public IDataReader GetDetailed(int areaId, int areaItemId, DateTime periodDate)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EvaluationReport_GetDetailedFull", areaId, areaItemId, periodDate);
        }

        #endregion
    }
}