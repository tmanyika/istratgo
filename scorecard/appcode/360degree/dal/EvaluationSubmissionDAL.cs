﻿using System;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

namespace ThreeSixty.Evaluation
{
    public class EvaluationSubmissionDAL
    {
        #region Variables

        private string _conString;

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return _conString; }
            set { _conString = value; }
        }

        #endregion

        #region Default Constructors

        public EvaluationSubmissionDAL(string conString)
        {
            _conString = conString;
        }

        #endregion

        #region Public Methods and Functions

        public long Save(EvaluationSubmission obj)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_EvaluationSubmission_Insert", obj.OrgUnitId, obj.AreaId, obj.AreaItemId, obj.PeriodDate, obj.MaximumRateValue,
                                                                                                obj.StatusId, obj.IsInternal, obj.OveralComment, obj.CapturerId);
            return (result == DBNull.Value) ? 0 : long.Parse(result.ToString());
        }

        public int UpdatePeriod(EvaluationSubmission obj)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_EvaluationSubmission_UpdatePeriod", obj.SubmissionId, obj.OrgUnitId, obj.AreaId,
                                                                                                                obj.AreaItemId, obj.PeriodDate, obj.UpdatedBy);
            return (result == DBNull.Value) ? 0 : int.Parse(result.ToString());
        }

        public int UpdateStatus(EvaluationSubmission obj)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_EvaluationSubmission_UpdateStatus", obj.SubmissionId, obj.StatusId, obj.UpdatedBy);
            return (result == DBNull.Value) ? 0 : int.Parse(result.ToString());
        }

        public int Delete(long submissionId)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_EvaluationSubmission_Delete", submissionId);
            return (result == DBNull.Value) ? 0 : int.Parse(result.ToString());
        }

        public IDataReader GetByStatusId(int companyId, string statusIds)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EvaluationSubmission_GetByCompIdAndStatus", companyId, statusIds);
        }

        public IDataReader GetByStatusId(int orgUnitId, string statusIds, int capturerId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EvaluationSubmission_GetByOrgUnitIdAndStatus", orgUnitId, statusIds, capturerId);
        }

        public IDataReader GetAllByCompId(int companyId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EvaluationSubmission_GetAllByCompId", companyId);
        }

        public IDataReader GetAllByOrgUnitId(int orgUnitId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EvaluationSubmission_GetAllByOrgUnitId", orgUnitId);
        }

        public IDataReader GetResourceByOrgUnitId(int orgUnitId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EvaluationSubmission_GetResourceByOrgUnitId", orgUnitId);
        }

        public IDataReader GetResourceByCompanyId(int companyId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EvaluationSubmission_GetResourceByCompId", companyId);
        }

        public IDataReader GetResourceByEmployeeId(int employeeId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EvaluationSubmission_GetResourceByEmployeeId", employeeId);
        }

        public IDataReader GetJobTitleResourceByOrgUnitId(int orgUnitId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EvaluationSubmission_GetJobTitleResourceByOrgUnitId", orgUnitId);
        }

        public IDataReader GetJobTitleResourceByCompanyId(int companyId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EvaluationSubmission_GetJobTitleResourceByCompId", companyId);
        }

        public IDataReader GetProjectList(int companyId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EvaluationSubmission_GetProjectByCompId", companyId);
        }

        public IDataReader GetProjectListByOwner(int ownerId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EvaluationSubmission_GetProjectByOwnerId", ownerId);
        }

        public IDataReader GetOrgUnitList(int orgUnitId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EvaluationSubmission_GetOrgUnits", orgUnitId);
        }

        public IDataReader GetOrgUnitListByOwner(int ownerId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EvaluationSubmission_GetOrgUnitsByOwnerId", ownerId);
        }

        public object Check(EvaluationSubmission obj)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_EvaluationSubmission_CheckIfPeriodExist", obj.SubmissionId, obj.AreaId,
                                                                                                                obj.AreaItemId, obj.PeriodDate, obj.IsInternal);
            return result;
        }

        public IDataReader GetPendingEvaluationList(int resourceId, string statusId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EvaluationSubmission_GetPendingEvaluation", resourceId, statusId);
        }

        public IDataReader GetPendingExternalEvaluationList(string email, int companyId, string statusId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EvaluationSubmission_GetPendingExtEvalByEmail", email, statusId, companyId);
        }

        public IDataReader GetPendingExternalEvaluationList(int resourceId, string statusId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EvaluationSubmission_GetPendingExternalEvaluation", resourceId, statusId);
        }

        public IDataReader GetEvaluationDates(int areaId, int areaItemId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EvaluationSubmission_GetEvalDates", areaId, areaItemId);
        }

        public IDataReader GetEvaluationDates(int areaId, int areaItemId, bool isinternal)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EvaluationSubmission_GetEvalDatesByType", areaId, areaItemId, isinternal);
        }

        public IDataReader GetEvaluation(int areaId, int areaItemId, DateTime periodDate)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EvaluationSubmission_GetByAreaItemAndPeriod", areaId, areaItemId, periodDate);
        }

        public IDataReader GetResourceByCompanyId(int companyId, int areaId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EvaluationSubmission_GetResourceByCompAndAreaId", companyId, areaId);
        }

        public IDataReader GetResourceByEmployeeId(int employeeId, int areaId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EvaluationSubmission_GetResourceByEmployeeAndAreaId", employeeId, areaId);
        }

        #endregion
    }
}