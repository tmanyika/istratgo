﻿using System;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

namespace ThreeSixty.Evaluation
{
    public class EvaluationScoreDAL
    {
        #region Variables

        private string _conString;

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return _conString; }
            set { _conString = value; }
        }

        #endregion

        #region Default Constructors

        public EvaluationScoreDAL(string conString)
        {
            _conString = conString;
        }

        #endregion

        #region Public Methods and Functions

        public IDataReader GetJobTitleListScore(long submissionId, long evaluatorId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EvaluationScore_GetJobTitleScore", submissionId, evaluatorId);
        }

        public IDataReader GetEmployeeListScore(long submissionId, long evaluatorId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EvaluationScore_GetEmployeeScore", submissionId, evaluatorId);
        }

        public IDataReader GetProjectListScore(long submissionId, long evaluatorId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EvaluationScore_GetProjectScore", submissionId, evaluatorId);
        }

        public IDataReader GetOrgUnitListScore(long submissionId, long evaluatorId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EvaluationScore_GetOrgUnitScore", submissionId, evaluatorId);
        }

        public int Save(long submissionId, long evaluatorId, bool submittedScore, string comment, string xmlData)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_EvaluationScore_InsertBulk", submissionId, evaluatorId, submittedScore, comment, xmlData);
            return (result == DBNull.Value) ? 0 : int.Parse(result.ToString());
        }

        #endregion
    }
}