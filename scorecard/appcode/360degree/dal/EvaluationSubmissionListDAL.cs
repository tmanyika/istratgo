﻿using System;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

namespace ThreeSixty.Evaluation
{
    public class EvaluationSubmissionListDAL
    {
        #region Variables

        private string _conString;

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return _conString; }
            set { _conString = value; }
        }

        #endregion

        #region Default Constructors

        public EvaluationSubmissionListDAL(string conString)
        {
            _conString = conString;
        }

        #endregion

        #region Public Methods and Functions

        public int Save(EvaluationSubmissionList obj)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_EvaluationSubmissionList_Insert", obj.SubmissionId, obj.ResourceId, obj.SubmittedScore,
                                                                                            obj.EvaluatorWeight, obj.CreatedBy);
            return (result == DBNull.Value) ? 0 : int.Parse(result.ToString());
        }

        public int SaveInternalBulk(EvaluationSubmissionList obj)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_EvaluationSubmissionList_InsertInternalBulk", obj.SubmissionId, obj.XmlEvalList, obj.HasWeight);
            return (result == DBNull.Value) ? 0 : int.Parse(result.ToString());
        }

        public int SaveExternalBulk(EvaluationSubmissionList obj)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_EvaluationSubmissionList_InsertExternalBulk", obj.SubmissionId, obj.XmlEvalList);
            return (result == DBNull.Value) ? 0 : int.Parse(result.ToString());
        }

        public int Delete(long evaluatorId)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_EvaluationSubmissionList_DeleteEvaluator", evaluatorId);
            return (result == DBNull.Value) ? 0 : int.Parse(result.ToString());
        }

        public IDataReader Get(long submissionId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EvaluationSubmissionList_GetByStatus", submissionId);
        }

        public IDataReader GetInternaCompanyPeopleList(int companyId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EvaluationSubmissionList_GetEvaluatorsByCompId", companyId);
        }

        public IDataReader GetInternalOrgUnitPeopleList(int orgunitId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EvaluationSubmissionList_GetEvaluatorsByOrgUnitId", orgunitId);
        }

        public IDataReader GetInternalSentOutPeopleList(string peopleIds)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EvaluationSubmissionList_GetEvaluatorsByIds", peopleIds);
        }

        public IDataReader GetById(long evaluatorId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EvaluationSubmissionList_GetById", evaluatorId);
        }

        #endregion
    }
}