﻿using System;
using System.Data;

using System.Collections.Generic;
using scorecard.entities;
using Microsoft.ApplicationBlocks.Data;

namespace ThreeSixty.Evaluation
{
    public class EvaluationRatingDAL
    {
        #region Variables

        private string _conString;

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return _conString; }
            set { _conString = value; }
        }

        #endregion

        #region Default Constructors

        public EvaluationRatingDAL(string conString)
        {
            _conString = conString;
        }

        #endregion

        public int Save(EvaluationRating obj)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_EvaluationRating_Insert", obj.CompanyId, obj.RatingValue,
                                                                                                obj.RatingDescription, obj.Active, obj.CreatedBy);
            return (result == DBNull.Value) ? 0 : int.Parse(result.ToString());
        }

        public int Update(EvaluationRating obj)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_EvaluationRating_Update", obj.RatingId, obj.CompanyId, obj.RatingValue,
                                                                                                obj.RatingDescription, obj.Active, obj.UpdatedBy);
            return (result == DBNull.Value) ? 0 : int.Parse(result.ToString());
        }

        public int Deactivate(EvaluationRating obj)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_EvaluationRating_Deactivate", obj.RatingId, obj.Active, obj.UpdatedBy);
            return (result == DBNull.Value) ? 0 : int.Parse(result.ToString());
        }

        public IDataReader GetAll(int companyId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EvaluationRating_GetByCompanyId", companyId);
        }

        public IDataReader Get(int ratingId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EvaluationRating_GetById", ratingId);
        }

        public object GetMax(int companyId, bool active)
        {
            return SqlHelper.ExecuteScalar(ConnectionString, "Proc_EvaluationRating_GetMaximumRating", companyId, active);
        }

        public IDataReader Get(int companyId, bool active)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EvaluationRating_GetByStatus", companyId, active);
        }
    }
}