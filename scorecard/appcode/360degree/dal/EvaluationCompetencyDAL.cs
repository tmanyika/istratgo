﻿using System;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

namespace ThreeSixty.Evaluation
{
    public class EvaluationCompetencyDAL
    {
        #region Variables

        private string _conString;

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return _conString; }
            set { _conString = value; }
        }

        #endregion

        #region Default Constructors

        public EvaluationCompetencyDAL(string conString)
        {
            _conString = conString;
        }

        #endregion

        #region Public Methods and Functions

        public int Save(EvaluationCompetency obj)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_EvaluationCompetency_Insert", obj.AreaId, obj.CompanyId,
                                                    obj.CompetencyName, obj.CompetencyDescription, obj.Active, obj.CreatedBy);
            return (result == DBNull.Value) ? 0 : int.Parse(result.ToString());
        }

        public int Update(EvaluationCompetency obj)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_EvaluationCompetency_Update", obj.CompetencyId, obj.AreaId, obj.CompanyId,
                                                    obj.CompetencyName, obj.CompetencyDescription, obj.Active, obj.UpdatedBy);
            return (result == DBNull.Value) ? 0 : int.Parse(result.ToString());
        }

        public int Deactivate(EvaluationCompetency obj)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_EvaluationCompetency_Deactivate", obj.CompetencyId, obj.Active, obj.UpdatedBy);
            return (result == DBNull.Value) ? 0 : int.Parse(result.ToString());
        }

        public IDataReader Get(int companyId, bool active)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EvaluationCompetency_GetByStatus", companyId, active);
        }

        public IDataReader Get(int companyId, int areaId, bool active)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EvaluationCompetency_GetByAreaId", companyId, areaId, active);
        }

        public IDataReader GetAll(int companyId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EvaluationCompetency_GetByCompanyId", companyId);
        }

        public IDataReader Get(int competencyId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EvaluationCompetency_GetById", competencyId);
        }

        #endregion
    }
}