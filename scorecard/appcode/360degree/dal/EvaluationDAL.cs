﻿using System;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

namespace ThreeSixty.Evaluation
{
    public class EvaluationDAL
    {
        #region Variables

        private string _conString;

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return _conString; }
            set { _conString = value; }
        }

        #endregion

        #region Default Constructors

        public EvaluationDAL(string conString)
        {
            _conString = conString;
        }

        #endregion

        #region Public Methods and Functions

        public int Save(Evaluation obj)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_Evaluation_Insert", obj.CompetencyId, obj.AreaValueId,
                                                                                                obj.WeightVal, obj.Active, obj.CreatedBy);
            return (result == DBNull.Value) ? 0 : int.Parse(result.ToString());
        }

        public int Update(Evaluation obj)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_Evaluation_Update", obj.EvaluationId, obj.CompetencyId,
                                                                                                obj.AreaValueId, obj.WeightVal, obj.Active, obj.UpdatedBy);
            return (result == DBNull.Value) ? 0 : int.Parse(result.ToString());
        }

        public int Deactivate(Evaluation obj)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_Evaluation_Deactivate", obj.EvaluationId, obj.Active, obj.UpdatedBy);
            return (result == DBNull.Value) ? 0 : int.Parse(result.ToString());
        }

        public IDataReader Get(int companyId, int areaId, int areaValueId, bool active)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_Evaluation_GetByStatus", companyId, areaId, areaValueId, active);
        }

        #endregion
    }
}