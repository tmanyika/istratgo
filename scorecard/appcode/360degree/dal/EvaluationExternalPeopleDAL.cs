﻿using System;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

namespace ThreeSixty.Evaluation
{
    public class EvaluationExternalPeopleDAL
    {
        #region Variables

        private string _conString;

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return _conString; }
            set { _conString = value; }
        }

        #endregion

        #region Default Constructors

        public EvaluationExternalPeopleDAL(string conString)
        {
            _conString = conString;
        }

        #endregion

        #region Public Methods and Functions

        public int SaveBulk(EvaluationExternalPeople obj)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_EvaluationExternalPeople_InsertBulk", obj.CompanyId, obj.XmlPeopleList);
            return (result == DBNull.Value) ? 0 : int.Parse(result.ToString());
        }

        public IDataReader GetExternalSentOutPeopleList(int companyId, string emailList)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_EvaluationExternalPeople_GetEvaluatorsByIds", companyId, emailList);
        }

        #endregion
    }
}