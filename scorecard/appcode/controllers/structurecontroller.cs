﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using scorecard.interfaces;
using scorecard.entities;
using System.Data;

/* @Copy Albertoncaffeine.net */

namespace scorecard.controllers
{
    public class structurecontroller
    {
        Iorgunit org;

        public structurecontroller(Iorgunit _org)
        {
            org = _org;
        }

        public int addOrgUnit(Orgunit _orgunit)
        {
            return org.addOrgUnit(_orgunit);
        }

        public int updateOrgUnit(Orgunit _orgunit)
        {
            return org.updateOrgUnit(_orgunit);
        }

        public int deleteOrgUnit(Orgunit _orgunit)
        {
            return org.deleteOrgUnit(_orgunit);
        }

        public DataSet getCompanyStructure(company _company)
        {
            return org.getCompanyStructure(_company);
        }

        public DataView getActiveCompanyOrgStructure(int companyId)
        {
            return org.getActiveCompanyStructure(companyId);
        }

        public DataTable getOrgStructureInHirachy(int orgUnitId)
        {
            return org.getOrgStructureInHirachy(orgUnitId);
        }

        public DataTable getOrgUnitsByLineManager(int managerId, bool active)
        {
            return org.getOrgUnitsByLineManager(managerId, active);
        }

        public DataView getActiveCompanyOrgStructure(company _company)
        {
            return org.getActiveCompanyStructure(_company);
        }

        public List<Orgunit> getOrgstructureByParentID(Orgunit _unit)
        {
            return org.getOrgstructureByParentID(_unit);
        }

        public List<Orgunit> getCompanyStructureListView(company _companyID)
        {
            return org.getCompanyStructureListView(_companyID);
        }
        public Orgunit getOrgunitDetails(Orgunit _unit)
        {
            return org.getOrgunitDetails(_unit);
        }
        public int asignJobTitle(Orgunit _unit, JobTitle _title)
        {
            return org.asignJobTitle(_unit, _title);
        }
        public int deleteAllJobTitleAsignments(Orgunit _unit)
        {
            return org.deleteAllJobTitleAsignments(_unit);
        }
        public DataSet getAllJobTitleAsignments(Orgunit _unit)
        {
            return org.getAllJobTitleAsignments(_unit);
        }
    }
}