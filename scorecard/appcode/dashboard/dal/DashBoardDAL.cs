﻿using System;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

namespace Dashboard.Reporting
{
    public class DashBoardDAL
    {
        #region Variables

        private string _conString;

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return _conString; }
            set { _conString = value; }
        }

        #endregion

        #region Default Constructors

        public DashBoardDAL(string conString)
        {
            _conString = conString;
        }

        #endregion

        #region Public Methods & Functions

        public DataTable GetObjective(int areaId, int orgUnitId, bool isCompany, string startDate, string endDate)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_DashBoard_Objectives", areaId, orgUnitId, isCompany, startDate, endDate).Tables[0];
        }

        public DataTable GetPerspective(int areaId, int orgUnitId, bool isCompany, string startDate, string endDate)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_DashBoard_Perspectives", areaId, orgUnitId, isCompany, startDate, endDate).Tables[0];
        }

        public DataTable GetFocusArea(int areaId, int orgUnitId, bool isCompany, string startDate, string endDate)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_DashBoard_FocusAreas2", areaId, orgUnitId, isCompany, startDate, endDate).Tables[0];
        }

        public DataTable GetFocusArea(int areaId, int orgUnitId, bool isCompany)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_DashBoard_FocusAreas", areaId, orgUnitId, isCompany).Tables[0];
        }

        public DataTable GetPerfomanceRating(int areaId, int orgUnitId, bool isCompany, int year)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_DashBoard_PerfomanceRating", areaId, orgUnitId, isCompany, year).Tables[0];
        }

        public DataTable GetPerfomanceRating(int areaId, int orgUnitId, bool isCompany, string startDate, string endDate)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_DashBoard_PerfomanceRatingByDates", areaId, orgUnitId, isCompany, startDate, endDate).Tables[0];
        }

        public DataTable GetTalentMatrixData(int areaId, int orgUnitId, bool isCompany, string startDate, string endDate)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_DashBoard_TalentMatrix", areaId, orgUnitId, isCompany, startDate, endDate).Tables[0];
        }

        public DataTable GetAssessmentData(int areaId, int valueId, string startDate, string endDate)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_DashBoard_AssessmentRatingByDates", areaId, valueId, startDate, endDate).Tables[0];
        }

        #endregion
    }
}