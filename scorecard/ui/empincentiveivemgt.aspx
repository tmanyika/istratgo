﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="empincentiveivemgt.aspx.cs"
    Inherits="scorecard.ui.empincentiveivemgt" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="controls/hrincentive/employeeincentivemgt.ascx" TagName="employeeincentivemgt"
    TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="css/layout.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">

        function refreshParent() {
            window.opener.location.reload(true);
            hs.close(this);
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManagerMain" runat="server" CombineScripts="True"
        EnablePartialRendering="true" EnablePageMethods="true">
        <Scripts>
            <asp:ScriptReference Path="js/jquery-1.3.2.min.js" ScriptMode="Release" NotifyScriptLoaded="false" />
            <asp:ScriptReference Path="js/easyTooltip.js" ScriptMode="Release" NotifyScriptLoaded="false" />
            <asp:ScriptReference Path="js/jquery-ui-1.7.2.custom.min.js" ScriptMode="Release"
                NotifyScriptLoaded="false" />
            <asp:ScriptReference Path="js/jquery.wysiwyg.js" ScriptMode="Release" NotifyScriptLoaded="false" />
            <asp:ScriptReference Path="js/hoverIntent.js" ScriptMode="Release" NotifyScriptLoaded="false" />
            <asp:ScriptReference Path="js/superfish.js" ScriptMode="Release" NotifyScriptLoaded="false" />
            <asp:ScriptReference Path="js/custom.js" ScriptMode="Release" NotifyScriptLoaded="false" />
            <asp:ScriptReference Path="js/jscript.js" ScriptMode="Release" NotifyScriptLoaded="false" />
        </Scripts>
    </asp:ToolkitScriptManager>
    <div>
        <uc1:employeeincentivemgt ID="employeeincentivemgt1" runat="server" />
    </div>
    </form>
</body>
</html>
