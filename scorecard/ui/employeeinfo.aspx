﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="employeeinfo.aspx.cs"
    Inherits="scorecard.ui.employeeinfo" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register src="controls/hr/employeeview.ascx" tagname="employeeview" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="css/preview.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    
    <div class="preview">
        <uc1:employeeview ID="employeeview1" runat="server" />
    </div>
    </form>
</body>
</html>
