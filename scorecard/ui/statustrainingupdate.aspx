﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ui/inside.Master" AutoEventWireup="true"
    CodeBehind="statustrainingupdate.aspx.cs" Inherits="scorecard.ui.statustrainingupdate" %>

<%@ Register Src="controls/orgstructure.ascx" TagName="orgstructure" TagPrefix="uc1" %>
<%@ Register Src="controls/training/trainingstatusupdate.ascx" TagName="trainingstatusupdate"
    TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header" runat="server">
    <link href="highslide/highslide.css" rel="stylesheet" type="text/css" />
    <script src="highslide/highslide-with-html.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <uc2:trainingstatusupdate ID="trainingstatusupdate1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="OrgStructure" runat="server">
    <uc1:orgstructure ID="orgstructure1" runat="server" />
</asp:Content>
