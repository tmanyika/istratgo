﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ui/inside.Master" AutoEventWireup="true" CodeBehind="rptdepartment.aspx.cs" Inherits="scorecard.ui.rptdepartment" %>
<%@ Register src="controls/rptdept.ascx" tagname="rptDept" tagprefix="uc1" %>
<%@ Register src="controls/orgstructure.ascx" tagname="orgstructure" tagprefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header" runat="server">
    <link href="css/ajaxcalendar.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <uc1:rptDept ID="rptDept1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="OrgStructure" runat="server">
    <uc2:orgstructure ID="orgstructure1" runat="server" />
</asp:Content>
