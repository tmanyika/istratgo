﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ui/inside.Master" AutoEventWireup="true" CodeBehind="incentivereportform.aspx.cs" Inherits="scorecard.ui.incentivereportform" %>
<%@ Register src="controls/orgstructure.ascx" tagname="orgstructure" tagprefix="uc1" %>
<%@ Register src="controls/incentiveReportForm.ascx" tagname="incentiveReportForm" tagprefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header" runat="server">
    <link href="css/ajaxcalendar.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <uc2:incentiveReportForm ID="incentiveReportForm1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="OrgStructure" runat="server">
    <uc1:orgstructure ID="orgstructure1" runat="server" />
</asp:Content>
