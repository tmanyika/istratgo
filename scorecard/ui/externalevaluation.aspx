﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="externalevaluation.aspx.cs"
    Inherits="scorecard.ui.externalevaluation" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="controls/360degree/evaluate360.ascx" TagName="evaluate360" TagPrefix="uc1" %>
<%@ Register Src="controls/footer.ascx" TagName="footer" TagPrefix="uc2" %>
<%@ Register Src="controls/headerexternal.ascx" TagName="headerexternal" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- Page title -->
    <title>iStratgo :: External 360 Degree Evaluation</title>
    <!-- End of Page title -->
    <!-- Libraries -->
    <link type="text/css" href="css/smoothness/jquery-ui-1.7.2.custom.html" rel="stylesheet" />
    <link href="css/avi.css" rel="stylesheet" type="text/css" />
    <link type="text/css" href="css/layout.css" rel="stylesheet" />
    <link href="css/threesixtyevaluation.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-34180756-1']);
        _gaq.push(['_setDomainName', 'imdconsulting.co.za']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManagerMain" runat="server" CombineScripts="True"
        EnablePartialRendering="true" EnablePageMethods="true">
        <Scripts>
            <asp:ScriptReference Path="js/jquery-1.3.2.min.js" ScriptMode="Release" NotifyScriptLoaded="false" />
            <asp:ScriptReference Path="js/easyTooltip.js" ScriptMode="Release" NotifyScriptLoaded="false" />
            <asp:ScriptReference Path="js/jquery-ui-1.7.2.custom.min.js" ScriptMode="Release"
                NotifyScriptLoaded="false" />
            <asp:ScriptReference Path="js/jquery.wysiwyg.js" ScriptMode="Release" NotifyScriptLoaded="false" />
            <asp:ScriptReference Path="js/hoverIntent.js" ScriptMode="Release" NotifyScriptLoaded="false" />
            <asp:ScriptReference Path="js/superfish.js" ScriptMode="Release" NotifyScriptLoaded="false" />
            <asp:ScriptReference Path="js/custom.js" ScriptMode="Release" NotifyScriptLoaded="false" />
            <asp:ScriptReference Path="js/jscript.js" ScriptMode="Release" NotifyScriptLoaded="false" />
        </Scripts>
    </asp:ToolkitScriptManager>
    <div id="container">
        <!-- Header -->
        <uc3:headerexternal ID="headerexternal1" runat="server" />
        <!-- Background wrapper -->
        <div id="bgwrap">
            <!-- Main Content -->
            <div id="content">
                <div id="main">
                    <div class="pad20">
                        <!-- Tabs -->
                        <div id="tabs">
                            <!-- First tab -->
                            <div id="tabs-1">
                                <!-- Form -->
                                <uc1:evaluate360 ID="evaluate3601" runat="server" IsInternal="false" />
                            </div>
                            <!-- End of First tab -->
                            <!-- Second tab -->
                            <!-- End of Second tab -->
                            <!-- Third tab -->
                            <!-- End of Third tab -->
                        </div>
                        <!-- End of Tabs -->
                    </div>
                </div>
            </div>
            <!-- End of Main Content -->
            <!-- Sidebar -->
            <!-- End of Sidebar -->
        </div>
        <!-- End of bgwrap -->
    </div>
    <!-- End of Container -->
    <!-- Footer -->
    <div id="footer">
        <p class="mid">
            <!-- Change this to your own once purchased -->
            <uc2:footer ID="footer1" runat="server" />
            <!-- -->
        </p>
    </div>
    </form>
</body>
</html>
