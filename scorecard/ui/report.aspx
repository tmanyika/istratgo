﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/ui/inside.Master"
    CodeBehind="report.aspx.cs" Inherits="scorecard.ui.report" %>

<%@ Register Src="controls/orgstructure.ascx" TagName="orgstructure" TagPrefix="uc1" %>
<%@ Register Src="controls/strategic.ascx" TagName="strategic" TagPrefix="uc2" %>
<%@ Register Src="controls/report.ascx" TagName="report" TagPrefix="uc3" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MainContent">
    <uc3:report ID="report1" runat="server" />
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="OrgStructure">
    <uc1:orgstructure ID="orgstructure1" runat="server" />
</asp:Content>
