﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="leaveAccruals.ascx.cs"
    Inherits="scorecard.ui.controls.hr.leaveAccruals" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<h1>
    Leave Accrual Management</h1>
<asp:UpdatePanel ID="UpdatePanelData" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div>
            <asp:ListView ID="lstData" runat="server" OnPagePropertiesChanged="lstData_PagePropertiesChanged"
                OnItemEditing="lstData_ItemEditing" OnItemCommand="lstData_ItemCommand" >
                <LayoutTemplate>
                    <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                        <thead>
                            <tr>
                                <td>
                                    Leave Type
                                </td>
                                <td>
                                    Working Week
                                </td>
                                <td>
                                    Accrual Time
                                </td>
                                <td align="right">
                                    Accrual Rate
                                </td>
                                <td align="right">
                                    Maximum Threshhold
                                </td>
                                <td>
                                    Active
                                </td>
                                <td>
                                    Edit
                                </td>
                                <td>
                                    Delete
                                </td>
                            </tr>
                            <tr id="itemPlaceholder" runat="server">
                            </tr>
                        </thead>
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <tbody>
                        <tr class="odd">
                            <td>
                                <asp:HiddenField ID="hdnLeaveAccrualId" runat="server" Value='<%# Eval("LeaveAccrualId")%>' />
                                <asp:Literal ID="lblLeaveName" runat="server" Text='<%# Eval("Leave.LeaveName")%>'></asp:Literal>
                            </td>
                            <td>
                                <asp:Literal ID="lblWekinWeek" runat="server" Text='<%# Eval("WorkWeek.Name")%>'></asp:Literal>
                            </td>
                            <td>
                                <asp:Literal ID="lblAccrualTime" runat="server" Text='<%# Eval("TimeAccrual.TimeName")%>'></asp:Literal>
                            </td>
                            <td align="right">
                                <%# Eval("AccrualRate")%>
                            </td>
                            <td align="right">
                                <%# Eval("MaximumThreshold")%>
                            </td>
                            <td>
                                <asp:Literal ID="lblActive" runat="server" Text='<%# GetBoolean(Eval("Active")) %>' />
                            </td>
                            <td>
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="lnkEdit" CommandName="Edit" class="ui-state-default ui-corner-all"
                                        title="Edit Record" CausesValidation="false"><span class="ui-icon ui-icon-pencil">
                                        </span>
                                    </asp:LinkButton>
                                </ul>
                            </td>
                            <td>
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="lnkDelete" CommandName="DeleteItem" class="ui-state-default ui-corner-all"
                                        title="Delete Record" OnClientClick="return confirmDelete();" CausesValidation="false"><span class="ui-icon ui-icon-trash">
                                        </span></asp:LinkButton>
                                </ul>
                            </td>
                        </tr>
                    </tbody>
                </ItemTemplate>
                <EditItemTemplate>
                    <tbody>
                        <tr class="odd">
                            <td>
                                <asp:HiddenField ID="hdnLeaveAccrualIdE" runat="server" Value='<%# Eval("LeaveAccrualId")%>' />
                                <asp:HiddenField ID="hdnLeaveId" runat="server" Value='<%# Eval("LeaveId")%>' />
                                <asp:HiddenField ID="hdnWorkingWeekId" runat="server" Value='<%# Eval("WorkingWeekId") %>' />
                                <asp:HiddenField ID="hdnAccrualTimeId" runat="server" Value='<%# Eval("AccrualTimeId") %>' />
                                <asp:DropDownList ID="ddLeave" runat="server" DataValueField="LeaveId" DataTextField="LeaveName">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddWekinWik" runat="server" DataValueField="WorkingWeekId" DataTextField="Name">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddTime" runat="server" DataValueField="AccrualTimeId" DataTextField="TimeName">
                                </asp:DropDownList>
                            </td>
                            <td align="right">
                                <asp:TextBox runat="server" ID="txtAccrualRate" Text='<%# Eval("AccrualRate")%>'
                                    ValidationGroup="Update" />
                                <asp:RequiredFieldValidator ID="RqdNum" runat="server" ErrorMessage="*" ValidationGroup="Update"
                                    ControlToValidate="txtAccrualRate" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="CvdNum" runat="server" ControlToValidate="txtAccrualRate"
                                    Display="Dynamic" ErrorMessage="numeric value required" Operator="DataTypeCheck"
                                    Type="Double" ValidationGroup="Update" ForeColor="Red"></asp:CompareValidator>
                            </td>
                            <td align="right">
                                <asp:TextBox runat="server" ID="txtMaximum" Text='<%# Eval("MaximumThreshold")%>'
                                    ValidationGroup="Update" />
                                <asp:RequiredFieldValidator ID="rqdMax" runat="server" ErrorMessage="*" ValidationGroup="Update"
                                    ControlToValidate="txtMaximum" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="cvdMaximum" runat="server" ControlToValidate="txtMaximum"
                                    Display="Dynamic" ErrorMessage="numeric value required" Operator="DataTypeCheck"
                                    Type="Double" ValidationGroup="Update" ForeColor="Red"></asp:CompareValidator>
                            </td>
                            <td>
                                <asp:CheckBox ID="chkActive" runat="server" Checked='<%# Eval("Active") %>' />
                            </td>
                            <td>
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="lnkUpdate" CommandName="UpdateItem" class="ui-state-default ui-corner-all"
                                        title="Update Record" ValidationGroup="Update"><span class="ui-icon ui-icon-disk"></span></asp:LinkButton>
                                </ul>
                            </td>
                            <td>
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="lnkCancelUpdate" CommandName="CancelUpdate" class="ui-state-default ui-corner-all"
                                        title="Cancel Update" CausesValidation="false"><span class="ui-icon ui-icon-cancel"></span></asp:LinkButton>
                                </ul>
                            </td>
                        </tr>
                    </tbody>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <tbody>
                        <tr class="odd">
                            <td>
                                <asp:DropDownList ID="ddLeavei" runat="server" DataValueField="LeaveId" DataTextField="LeaveName">
                                </asp:DropDownList>
                            </td>
                            <td align="right">
                                <asp:DropDownList ID="ddWekinWiki" runat="server" DataValueField="WorkingWeekId"
                                    DataTextField="Name">
                                </asp:DropDownList>
                            </td>
                            <td align="right">
                                <asp:DropDownList ID="ddTimei" runat="server" DataValueField="AccrualTimeId" DataTextField="TimeName">
                                </asp:DropDownList>
                            </td>
                            <td align="right">
                                <asp:TextBox runat="server" ID="txtAccrualRatei" Text='<%# Eval("AccrualRate")%>'
                                    ValidationGroup="Update" />
                                <asp:RequiredFieldValidator ID="RqdNumi" runat="server" ErrorMessage="*" ValidationGroup="Update"
                                    ControlToValidate="txtAccrualRatei" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="CvdNumi" runat="server" ControlToValidate="txtAccrualRatei"
                                    Display="Dynamic" ErrorMessage="numeric value required" Operator="DataTypeCheck"
                                    Type="Double" ValidationGroup="Update" ForeColor="Red"></asp:CompareValidator>
                            </td>
                            <td align="right">
                                <asp:TextBox runat="server" ID="txtMaximumi" Text='<%# Eval("MaximumThreshold")%>'
                                    ValidationGroup="Update" />
                                <asp:RequiredFieldValidator ID="rqdMaxi" runat="server" ErrorMessage="*" ValidationGroup="Update"
                                    ControlToValidate="txtMaximumi" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="cvdMaximumi" runat="server" ControlToValidate="txtMaximumi"
                                    Display="Dynamic" ErrorMessage="numeric value required" Operator="DataTypeCheck"
                                    Type="Double" ValidationGroup="Update" ForeColor="Red"></asp:CompareValidator>
                            </td>
                            <td>
                                <asp:CheckBox ID="chkActivei" runat="server" Checked="True" />
                            </td>
                            <td>
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="lnkSave" CommandName="AddItem" class="ui-state-default ui-corner-all"
                                        title="Update Record" ValidationGroup="Add"><span class="ui-icon ui-icon-disk"></span></asp:LinkButton>
                                </ul>
                            </td>
                            <td>
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="lnkCancelUpdate" CommandName="CancelUpdate" class="ui-state-default ui-corner-all"
                                        title="Cancel Update" CausesValidation="false"><span class="ui-icon ui-icon-cancel"></span></asp:LinkButton>
                                </ul>
                            </td>
                        </tr>
                    </tbody>
                </InsertItemTemplate>
                <EmptyDataTemplate>
                    <p class="errorMsg">
                        There are no records to display.
                    </p>
                </EmptyDataTemplate>
            </asp:ListView>
            <br />
            <asp:DataPager ID="pager" runat="server" PagedControlID="lstData">
                <Fields>
                    <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True" ShowNextPageButton="False"
                        ShowPreviousPageButton="False" />
                    <asp:NumericPagerField />
                    <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True" ShowNextPageButton="False"
                        ShowPreviousPageButton="False" />
                </Fields>
            </asp:DataPager>
            <br />
            <br />
            <p class="errorMsg">
                <asp:Literal ID="lblMsg" runat="server"></asp:Literal></p>
        </div>
        <div id="tabs-3">
            <p>
                <asp:Button ID="btnNew" runat="server" class="button" Text="Add New Leave Accrual"
                    OnClick="lnkAdd_Click" CausesValidation="False" />
                <asp:Button ID="lnkCancel0" runat="server" class="button" Text="Done" OnClick="lnkCancel_Click"
                    CausesValidation="false" />
            </p>
        </div>
        <asp:UpdateProgress ID="UpdateProgressInfo" runat="server" AssociatedUpdatePanelID="UpdatePanelData">
            <ProgressTemplate>
                <div>
                    <asp:Image ID="imgProj" runat="server" ImageUrl="~/ui/images/icons/activity.gif"
                        AlternateText="Please wait.."></asp:Image></div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
