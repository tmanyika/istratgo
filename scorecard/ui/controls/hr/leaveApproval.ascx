﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="leaveApproval.ascx.cs"
    Inherits="scorecard.ui.controls.hr.leaveApproval" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Import Namespace="scorecard.implementations" %>
<h1>
    Leave Approval
</h1>
<asp:UpdatePanel ID="UpdatePanelLeave" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:MultiView ID="mView" runat="server" ActiveViewIndex="0">
            <asp:View ID="vwData" runat="server">
                <div>
                    <div>
                        <asp:ListView ID="lstData" runat="server" OnPagePropertiesChanging="lstData_PagePropertiesChanging"
                            OnItemCommand="lstData_ItemCommand" OnItemEditing="lstData_ItemEditing">
                            <LayoutTemplate>
                                <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                                    <thead>
                                        <tr>
                                            <td>
                                                Employee
                                            </td>
                                            <td style="width: 100px;">
                                                Leave Type
                                            </td>
                                            <td>
                                                Start Date
                                            </td>
                                            <td>
                                                End Date
                                            </td>
                                            <td style="width: 100px; text-align: right">
                                                No. Of Days
                                            </td>
                                            <td>
                                                Status
                                            </td>
                                            <td>
                                                Leave Notes
                                            </td>
                                            <td>
                                                Documents
                                            </td>
                                            <td>
                                                Edit
                                            </td>
                                        </tr>
                                        <tr id="itemPlaceholder" runat="server">
                                        </tr>
                                    </thead>
                                </table>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tbody>
                                    <tr class="odd">
                                        <td>
                                            <%# Eval("Staff.FullName")%>
                                            <asp:HiddenField ID="hdnName" runat="server" Value='<%# Eval("Staff.FullName")%>' />
                                        </td>
                                        <td>
                                            <%# Eval("Leave.LeaveName")%>
                                            <asp:HiddenField ID="hdnId" runat="server" Value='<%# Eval("LeaveApplicationId") %>' />
                                        </td>
                                        <td>
                                            <%# String.Format("{0:dd/MM/yyyy}", Eval("StartDate"))%>
                                        </td>
                                        <td>
                                            <%# String.Format("{0:dd/MM/yyyy}", Eval("EndDate")) %>
                                        </td>
                                        <td style="text-align: right">
                                            <%# Eval("NoOfLeaveDays")%>
                                        </td>
                                        <td>
                                            <%# Eval("Status.StatusName")%>
                                        </td>
                                        <td>
                                            <%# Eval("LeaveNotes")%>
                                        </td>
                                        <td>
                                            <a href='leavedoc.aspx?show=false&id=<%# Eval("LeaveApplicationId") %>' onclick="return hs.htmlExpand(this, 
                                            {  objectType: 'iframe', width: '800',  creditsPosition: 'bottom left', headingText: 'APPLICATION SUPPORTING DOCUMENTATION', wrapperClassName: 'titlebar' } )">
                                                View Docs</a>
                                        </td>
                                        <td>
                                            <ul id="icons">
                                                <asp:LinkButton runat="server" ID="lnkEdit" CommandName="Edit" class="ui-state-default ui-corner-all"
                                                    title="Edit Record" CausesValidation="false" Visible='<%# GetVisibilityStatus(Eval("statusId")) %>'><span class="ui-icon ui-icon-pencil"></span>
                                                </asp:LinkButton>
                                            </ul>
                                        </td>
                                    </tr>
                                </tbody>
                            </ItemTemplate>
                            <EmptyDataTemplate>
                                <p class="errorMsg">
                                    There are no records to display.
                                </p>
                            </EmptyDataTemplate>
                        </asp:ListView>
                        <br />
                        <asp:DataPager ID="pager" runat="server" PagedControlID="lstData" PageSize="15">
                            <Fields>
                                <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True" ShowNextPageButton="False"
                                    ShowPreviousPageButton="False" />
                                <asp:NumericPagerField />
                                <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True" ShowNextPageButton="False"
                                    ShowPreviousPageButton="False" />
                            </Fields>
                        </asp:DataPager>
                    </div>
                    <div>
                        <p class="errorMsg">
                            <asp:Literal ID="lblMsg" runat="server"></asp:Literal>
                        </p>
                    </div>
                </div>
            </asp:View>
            <asp:View ID="vwForm" runat="server">
                <div>
                    <p>
                        <label for="sf">
                            Employee Name :
                        </label>
                        <span class="field_desc">
                            <asp:Literal ID="lblName" runat="server"></asp:Literal>
                        </span>
                    </p>
                    <p>
                        <label for="sf">
                            Leave Type :
                        </label>
                        <span class="field_desc">
                            <asp:Literal ID="lblLeaveType" runat="server"></asp:Literal>
                        </span>
                    </p>
                    <p>
                        <label for="lf">
                            Start Date :
                        </label>
                        <span class="field_desc">
                            <asp:Literal ID="lblStartDate" runat="server"></asp:Literal>
                        </span>
                    </p>
                    <p>
                        <label for="lf">
                            End Date :
                        </label>
                        <span class="field_desc">
                            <asp:Literal ID="lblEndDate" runat="server"></asp:Literal>
                        </span>
                    </p>
                    <p>
                        <label for="lf">
                            Number of Days :
                        </label>
                        <span class="field_desc">
                            <asp:Literal ID="lblNoOfDays" runat="server"></asp:Literal>
                        </span>
                    </p>
                    <p>
                        <div class="floatLeft">
                            <label for="lf" style="vertical-align: top;">
                                Leave Notes :
                            </label>
                        </div>
                        <div class="floatLeft">
                            <span class="field_desc">
                                <div style="width: 600px; height: 150px; border-width: 0px; overflow-y: scroll;">
                                    <asp:Literal ID="lblNotes" runat="server"></asp:Literal></div>
                            </span>
                        </div>
                        <div style="clear: both;">
                        </div>
                    </p>
                    <p>
                        <label for="lf">
                            Supporting Document(s) :
                        </label>
                        <span class="field_desc">
                            <asp:Repeater ID="rptDocs" runat="server">
                                <HeaderTemplate>
                                    <table class="fullwidth" border="0" cellpadding="0" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th align="left">
                                                    Document Title
                                                </th>
                                                <th align="left">
                                                    Document Name
                                                </th>
                                                <th style="width: 550px; text-align: left;">
                                                    Notes
                                                </th>
                                                <th align="left">
                                                    Uploaded By
                                                </th>
                                                <th align="left">
                                                    Date Uploaded
                                                </th>
                                                <th align="left">
                                                    Valid
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr class="odd">
                                        <td align="left">
                                            <%# Eval("DocumentTitle")%>
                                        </td>
                                        <td align="left">
                                            <a href='../attachments/leavedocs/<%# Eval("SupportingDoc")%>' target="_blank">
                                                <%# Eval("SupportingDoc")%></a>
                                        </td>
                                        <td align="left">
                                            <%# Eval("Comment")%>
                                        </td>
                                        <td align="left">
                                            <%# Eval("UploadedBy") %>
                                        </td>
                                        <td align="left">
                                            <%# Common.GetDescriptiveDateWithTime(Eval("DateCreated"), true)%>
                                        </td>
                                        <td align="left">
                                            <%# GetBoolean(Eval("Valid")) %>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody> </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </span>
                    </p>
                    <p>
                        <label for="sf">
                            Action :
                        </label>
                        <span class="field_desc">
                            <asp:DropDownList ID="ddStatus" runat="server">
                            </asp:DropDownList>
                        </span>
                    </p>
                    <p>
                        <div class="floatLeft">
                            <label for="sf">
                                Comment :
                            </label>
                        </div>
                        <div class="floatLeft">
                            <span class="field_desc">
                                <asp:TextBox CssClass="mf" runat="server" ID="txtComment" TextMode="MultiLine" Columns="50"
                                    Rows="8" ValidationGroup="Upload" />
                            </span>
                        </div>
                        <div style="clear: both;">
                        </div>
                    </p>
                    <p>
                        <br />
                    </p>
                    <p class="errorMsg">
                        <asp:Literal ID="lblErr" runat="server"></asp:Literal>
                        <br />
                    </p>
                </div>
                <div id="tabs-3">
                    <p>
                        <label for="lf">
                            <asp:HiddenField ID="hdnId" runat="server" />
                            &nbsp;</label>
                        <span class="field_desc">
                            <asp:Button ID="btnUpdateLeave" runat="server" CssClass="button" OnClick="btnUpdateLeave_Click"
                                Text="Update" ValidationGroup="AddLeave" />
                            <asp:Button ID="btnCancelLeave" runat="server" CssClass="button" Text="Cancel" CausesValidation="false"
                                OnClick="btnCancelLeave_Click" />
                        </span>
                    </p>
                </div>
            </asp:View>
        </asp:MultiView>
        <asp:UpdateProgress ID="UpdateProgressLeave" runat="server" AssociatedUpdatePanelID="UpdatePanelLeave">
            <ProgressTemplate>
                <div>
                    <asp:Image ID="imgProj" runat="server" ImageUrl="~/ui/images/icons/activity.gif"
                        AlternateText="Please wait.."></asp:Image></div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
