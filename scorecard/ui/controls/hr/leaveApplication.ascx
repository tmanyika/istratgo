﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="leaveApplication.ascx.cs"
    Inherits="scorecard.ui.controls.hr.leaveApplication" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<h1>
    Leave Application</h1>
<asp:UpdatePanel ID="UpdatePanelLeave" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:MultiView ID="mView" runat="server" ActiveViewIndex="0">
            <asp:View ID="vwData" runat="server">
                <div>
                    <div>
                        <asp:ListView ID="lstData" runat="server" OnPagePropertiesChanging="lstData_PagePropertiesChanging"
                            OnItemCommand="lstData_ItemCommand" OnItemEditing="lstData_ItemEditing">
                            <LayoutTemplate>
                                <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                                    <thead>
                                        <tr>
                                            <td style="width: 100px;">
                                                Leave Type
                                            </td>
                                            <td>
                                                Start Date
                                            </td>
                                            <td>
                                                End Date
                                            </td>
                                            <td style="width: 100px; text-align: right">
                                                No. Of Days
                                            </td>
                                            <td>
                                                Status
                                            </td>
                                            <td>
                                                Leave Notes
                                            </td>
                                            <td>
                                                Rejection Reason
                                            </td>
                                            <td>
                                                Documents
                                            </td>
                                            <td>
                                                Edit
                                            </td>
                                        </tr>
                                        <tr id="itemPlaceholder" runat="server">
                                        </tr>
                                    </thead>
                                </table>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tbody>
                                    <tr class="odd">
                                        <td>
                                            <%# Eval("Leave.LeaveName")%>
                                            <asp:HiddenField ID="hdnId" runat="server" Value='<%# Eval("LeaveApplicationId") %>' />
                                        </td>
                                        <td>
                                            <%# String.Format("{0:dd/MM/yyyy}", Eval("StartDate"))%>
                                        </td>
                                        <td>
                                            <%# String.Format("{0:dd/MM/yyyy}", Eval("EndDate")) %>
                                        </td>
                                        <td style="text-align: right">
                                            <%# Eval("NoOfLeaveDays")%>
                                        </td>
                                        <td>
                                            <%# Eval("Status.StatusName")%>
                                        </td>
                                        <td>
                                            <%# Eval("LeaveNotes")%>
                                        </td>
                                        <td>
                                            <%# Eval("RejectReason")%>
                                        </td>
                                        <td>
                                            <a href='leavedoc.aspx?show=false&id=<%# Eval("LeaveApplicationId") %>' onclick="return hs.htmlExpand(this, 
                                            {  objectType: 'iframe', width: '800',  creditsPosition: 'bottom left', headingText: 'APPLICATION SUPPORTING DOCUMENTATION', wrapperClassName: 'titlebar' } )">
                                                View Docs</a>
                                        </td>
                                        <td>
                                            <ul id="icons">
                                                <asp:LinkButton runat="server" ID="lnkEdit" CommandName="Edit" class="ui-state-default ui-corner-all"
                                                    title="Edit Record" CausesValidation="false" Visible='<%# GetVisibilityStatus(Eval("statusId")) %>'><span class="ui-icon ui-icon-pencil"></span>
                                                </asp:LinkButton>
                                            </ul>
                                        </td>
                                    </tr>
                                </tbody>
                            </ItemTemplate>
                            <EmptyDataTemplate>
                                <p class="errorMsg">
                                    There are no records to display.
                                </p>
                            </EmptyDataTemplate>
                        </asp:ListView>
                        <br />
                        <asp:DataPager ID="pager" runat="server" PagedControlID="lstData" PageSize="15">
                            <Fields>
                                <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True" ShowNextPageButton="False"
                                    ShowPreviousPageButton="False" />
                                <asp:NumericPagerField />
                                <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True" ShowNextPageButton="False"
                                    ShowPreviousPageButton="False" />
                            </Fields>
                        </asp:DataPager>
                    </div>
                    <div>
                        <p class="errorMsg">
                            <asp:Literal ID="lblMsg" runat="server"></asp:Literal>
                        </p>
                    </div>
                    <div id="tabs-btn">
                        <p>
                            <asp:Button ID="btnNew" runat="server" CssClass="button" Text="Apply for Leave" OnClick="lnkAdd_Click"
                                CausesValidation="False" />
                            <asp:Button ID="lnkDone" runat="server" CssClass="button" Text="Done" CausesValidation="false"
                                PostBackUrl="~/ui/index.aspx" />
                        </p>
                    </div>
                </div>
            </asp:View>
            <asp:View ID="vwForm" runat="server">
                <div>
                    <p>
                        <label for="sf">
                            Leave Type :
                        </label>
                        <span class="field_desc">
                            <asp:DropDownList ID="ddLeave" runat="server">
                            </asp:DropDownList>
                        </span>
                    </p>
                    <p>
                        <label for="lf">
                            Start Date :
                        </label>
                        <span class="field_desc">
                            <asp:TextBox CssClass="mf" runat="server" ID="txtDate" Width="120px" ValidationGroup="AddLeave" />
                            <asp:CalendarExtender ID="txtDate_CalendarExtender" runat="server" Enabled="True"
                                Format="d/MM/yyyy" TargetControlID="txtDate">
                            </asp:CalendarExtender>
                        </span>
                        <asp:RequiredFieldValidator ID="rqdFName" runat="server" ControlToValidate="txtDate"
                            Display="None" ErrorMessage="Start Date" ValidationGroup="AddLeave" ForeColor="Red"></asp:RequiredFieldValidator>
                    </p>
                    <p>
                        <label for="lf">
                            Number of Days :
                        </label>
                        <span class="field_desc">
                            <asp:TextBox CssClass="mf" runat="server" ID="txtNoOfDays" ValidationGroup="AddLeave"
                                MaxLength="3" Width="50px" />
                        </span>
                        <asp:RequiredFieldValidator ID="rqdNumDays" runat="server" ControlToValidate="txtDate"
                            Display="None" ErrorMessage="Number of days" ValidationGroup="AddLeave" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="CvDays" runat="server" ControlToValidate="txtNoOfDays"
                            Display="Dynamic" ErrorMessage="only numeric values allowed" ForeColor="Red"
                            Type="Integer" ValidationGroup="AddLeave" Operator="DataTypeCheck"></asp:CompareValidator>
                    </p>
                    <div style="clear: both;">
                        <div class="floatLeft">
                            <label for="lf" style="vertical-align: top;">
                                Leave Notes :
                            </label>
                        </div>
                        <div class="floatLeft">
                            &nbsp;&nbsp;&nbsp;&nbsp;<span class="field_desc">
                                <asp:TextBox CssClass="mf" runat="server" ID="txtNotes" ValidationGroup="AddLeave"
                                    TextMode="MultiLine" Rows="6" Columns="40" /></span>
                        </div>
                    </div>
                    <div style="clear: both;">
                        <p>
                            <br />
                            <br />
                        </p>
                        <p>
                            <label for="lf">
                                Supporting Document :
                            </label>
                            <span class="field_desc"><a id="lnkDoc" runat="server" href="~/ui/leavedoc.aspx?show=true"
                                onclick="return hs.htmlExpand(this, { objectType: 'iframe', width: 800, creditsPosition: 'bottom left', headingText: 'LEAVE SUPPORTING DOCUMENTATION', wrapperClassName: 'titlebar' } )">
                                click here</a> to upload/view documents</span>
                        </p>
                        <p>
                            <br />
                        </p>
                    </div>
                    <p class="errorMsg">
                        <asp:Literal ID="lblErr" runat="server"></asp:Literal>
                        <br />
                    </p>
                </div>
                <div id="tabs-3">
                    <p>
                        <label for="lf">
                            <asp:HiddenField ID="hdnId" runat="server" />
                            &nbsp;</label>
                        <span class="field_desc">
                            <asp:Button ID="btnAddLeave" runat="server" CssClass="button" Text="Submit" ValidationGroup="AddLeave"
                                OnClick="btnAddLeave_Click" />
                            <asp:Button ID="btnUpdateLeave" runat="server" CssClass="button" OnClick="btnUpdateLeave_Click"
                                Text="Update" ValidationGroup="AddLeave" Visible="False" />
                            <asp:Button ID="btnCancelLeave" runat="server" CssClass="button" Text="Cancel" CausesValidation="false"
                                OnClick="btnCancelLeave_Click" />
                        </span>
                    </p>
                </div>
            </asp:View>
        </asp:MultiView>
        <asp:UpdateProgress ID="UpdateProgressLeave" runat="server" AssociatedUpdatePanelID="UpdatePanelLeave">
            <ProgressTemplate>
                <div>
                    <asp:Image ID="imgProj" runat="server" ImageUrl="~/ui/images/icons/activity.gif"
                        AlternateText="Please wait.."></asp:Image></div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
