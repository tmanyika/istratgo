﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="employeeview.ascx.cs"
    Inherits="scorecard.ui.controls.employeeview" %>
<%@ Import Namespace="scorecard.implementations" %>
<%--<div style="padding-left: 5px;" >
    <h3>
        <asp:Literal ID="LblName" runat="server"></asp:Literal>
        Information</h3>
    <br />
    <br />
</div>--%>
<div>
    <asp:Repeater ID="rptData" runat="server">
        <HeaderTemplate>
            <table cellpadding="0" cellspacing="10" border="0" width="550px">
                <tbody>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td style="width: 300px">
                    <b>Full Name</b>
                </td>
                <td>
                    <%# Eval("FullName") %>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Race</b>
                </td>
                <td>
                    <%# Eval("Creed.RaceName") %>
                </td>
            </tr>
            <tr>
                <td>
                    <b>ID Number</b>
                </td>
                <td>
                    <%# Eval("IDNumber")%>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Passport Number</b>
                </td>
                <td>
                    <%# Eval("PassportNo")%>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Date Of Birth</b>
                </td>
                <td>
                    <%# Common.GetDescriptiveDate(Eval("DateOfBirth")) %>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Citizenship</b>
                </td>
                <td>
                    <%# Eval("Citizenship")%>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Employee No.</b>
                </td>
                <td>
                    <%# Eval("EmployeeNo")%>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Job Title</b>
                </td>
                <td>
                    <%# Eval("Position.Name")%>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Date Of Appointment</b>
                </td>
                <td>
                    <%# Eval("DateOfAppointment")%>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Email Address</b>
                </td>
                <td>
                    <%# Eval("EmailAddress")%>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Cellphone Number</b>
                </td>
                <td>
                    <%# Eval("Cellphone")%>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Business Telephone</b>
                </td>
                <td>
                    <%# Eval("BusTelephone")%>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Company</b>
                </td>
                <td>
                    <%# Eval("Organisation.ORGUNIT_NAME")%>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Status</b>
                </td>
                <td>
                    <%# Eval("Status.StatusName")%>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Working Week</b>
                </td>
                <td>
                    <%# Eval("WekinWeek.Name")%>
                </td>
            </tr>
            <tr>
                <td>
                    <b>System User Name</b>
                </td>
                <td>
                    <%# Eval("UserName")%>
                </td>
            </tr>
            <tr>
                <td>
                    <b>System Assigned Role</b>
                </td>
                <td>
                    <%# Eval("UserRole.RoleName")%>
                </td>
            </tr>
            </tbody>
        </ItemTemplate>
        <FooterTemplate>
            </tbody></table>
        </FooterTemplate>
    </asp:Repeater>
</div>
<div>
    <p class="errorMsg">
        <asp:Literal ID="lblMsg" runat="server"></asp:Literal>
    </p>
</div>
