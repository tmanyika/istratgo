﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.implementations;
using HR.Leave;
using HR.Leave.Documents;
using System.Data;

namespace scorecard.ui.controls.hr
{
    public partial class leaveDocs : System.Web.UI.UserControl
    {
        #region Property

        bool Show
        {
            get
            {
                return (Request.QueryString["show"] != null) ? bool.Parse(Request.QueryString["show"].ToString()) : false;
            }
        }

        int AppId
        {
            get { return (Request.QueryString["id"] != null) ? int.Parse(Request.QueryString["id"].ToString()) : 0; }
        }

        #endregion

        #region Page Event Handling Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                pnlDocs.Visible = Show;
                FillDocuments();
            }
        }

        #endregion

        #region Databinding Methods

        void FillDocuments()
        {

            object data = null;
            if (AppId > 0)
            {
                ILeaveApplicationComment obj = new LeaveApplicationCommentBL();
                data = obj.GetComments(AppId);
            }
            else data = Session["Docs"] as DataTable;
            BindControl.BindListView(lstData, data);
        }

        void ClearForm()
        {
            txtDocName.Text = "";
            txtNotes.Text = "";
        }

        void UploadFile()
        {
            try
            {
                if (FileUploadDoc.HasFile)
                {
                    string fileName = FileUploadDoc.FileName;
                    string folderPath = Util.getConfigurationSettingLeaveUploadFolder();
                    string newFileName = fileName.Replace(" ", "").Replace("'", "").Replace(",", "");
                    string filePath = string.Format(@"{0}\{1}", folderPath, newFileName);
                    string docTitle = txtDocName.Text;
                    string notes = txtNotes.Text;
                    string uploadedBy = Util.user.LoginId;

                    int applicationId = AppId;
                    bool valid = true;
                    FileUploadDoc.SaveAs(filePath);

                    DateTime todayDate = DateTime.Now;
                    ILeaveApplicationComment obj = new LeaveApplicationCommentBL();

                    if (applicationId > 0)
                    {
                        LeaveApplicationComment com = new LeaveApplicationComment
                        {
                            Comment = notes,
                            DateCreated = todayDate,
                            DateUpdated = todayDate,
                            DocumentTitle = docTitle,
                            LeaveApplicationId = applicationId,
                            SupportingDoc = newFileName,
                            UpdatedBy = uploadedBy,
                            UploadedBy = uploadedBy,
                            Valid = valid
                        };

                        bool saved = obj.AddComment(com);
                        if (saved)
                        {
                            ClearForm();
                            BindControl.BindLiteral(lblMsg, Messages.GetSaveMessage());
                        }
                        else BindControl.BindLiteral(lblMsg, Messages.GetSaveFailedMessage());
                    }
                    else
                    {
                        DataTable dT = (Session["Docs"] == null) ? obj.GetSchema() : Session["Docs"] as DataTable;
                        DataRow rw = dT.NewRow();
                        rw["ApplicationId"] = applicationId;
                        rw["DocumentTitle"] = docTitle;
                        rw["Comment"] = notes;
                        rw["SupportingDoc"] = newFileName;
                        rw["Valid"] = valid;
                        rw["UploadedBy"] = uploadedBy;
                        rw["DateCreated"] = DateTime.Now;
                        dT.Rows.Add(rw);
                        dT.AcceptChanges();
                        Session["Docs"] = dT;
                    }
                    FillDocuments();
                }
            }
            catch (Exception e)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(e);
            }
        }

        #endregion

        #region Checkbox Event Handling

        bool DeletedRow(int commentId)
        {
            DataTable dT = Session["Docs"] as DataTable;
            foreach (DataRow rw in dT.Rows)
                if (int.Parse(rw["commentId"].ToString()) == commentId)
                    rw.Delete();
            dT.AcceptChanges();
            Session["Docs"] = dT;
            return true;
        }

        void UpdateDocStatus(int commentId, string updatedBy)
        {
            try
            {
                ILeaveApplicationComment obj = new LeaveApplicationCommentBL();
                bool deleted = (AppId > 0) ? obj.DeleteComments(commentId, updatedBy) : DeletedRow(commentId);
                BindControl.BindLiteral(lblMsg, deleted ? Messages.GetDeleteMessage() : Messages.GetDeleteFailedMessage());
            }
            catch (Exception e)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(e);
            }
        }

        protected void chkValid_OnCheckedChanged(object sender, EventArgs e)
        {
            if (!Show)
            {
                return;
            }
            CheckBox chk = sender as CheckBox;
            string updatedBy = Util.user.LoginId;
            int commentId = int.Parse(chk.ValidationGroup);
            UpdateDocStatus(commentId, updatedBy);
            FillDocuments();
        }

        #endregion

        #region Button Event Handling Methods

        protected void lnkUpload_Click(object sender, EventArgs e)
        {
            UploadFile();
        }

        #endregion
    }
}