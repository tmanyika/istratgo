﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="leaveDocs.ascx.cs" Inherits="scorecard.ui.controls.hr.leaveDocs" %>
<div style="height: 20px;">
    &nbsp;
</div>
<div>
    <asp:ListView ID="lstData" runat="server">
        <LayoutTemplate>
            <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                <thead>
                    <tr>
                        <td>
                            Document Title
                        </td>
                        <td>
                            Document Name
                        </td>
                        <td>
                            Notes
                        </td>
                        <td>
                            Uploaded By
                        </td>
                        <td>
                            Date Uploaded
                        </td>
                        <td>
                            Valid
                        </td>
                    </tr>
                    <tr id="itemPlaceholder" runat="server">
                    </tr>
                </thead>
            </table>
        </LayoutTemplate>
        <ItemTemplate>
            <tbody>
                <tr class="odd">
                    <td>
                        <%# Eval("DocumentTitle")%>
                    </td>
                    <td>
                        <a href='../attachments/leavedocs/<%# Eval("SupportingDoc")%>' target="_blank">
                            <%# Eval("SupportingDoc")%></a>
                    </td>
                    <td>
                        <%# Eval("Comment")%>
                    </td>
                    <td>
                        <%# Eval("UploadedBy") %>
                    </td>
                    <td>
                        <%# Eval("DateCreated")%>
                    </td>
                    <td>
                        <asp:CheckBox ID="chkValid" runat="server" Checked='<%# Eval("Valid") %>' ValidationGroup='<%# Eval("CommentId") %>'
                            AutoPostBack="true" OnCheckedChanged="chkValid_OnCheckedChanged" ToolTip="Click to make the document valid / invalid." />
                    </td>
                </tr>
            </tbody>
        </ItemTemplate>
        <EmptyDataTemplate>
            <p class="errorMsg">
                There are no documents for this leave application to display.
            </p>
        </EmptyDataTemplate>
    </asp:ListView>
</div>
<div>
    <asp:Panel ID="pnlDocs" runat="server" GroupingText="Supporting Documents" Width="30%">
        <p>
            <br />
        </p>
        <p>
            <label for="sf" style="vertical-align: top">
                Document Title
            </label>
            <br />
            <span class="field_desc">
                <asp:TextBox CssClass="mf" runat="server" ID="txtDocName" ValidationGroup="Upload"
                    Width="500px" />
                <asp:RequiredFieldValidator ID="rqdDocName" runat="server" ControlToValidate="txtDocName"
                    Display="Dynamic" ErrorMessage="Document title is required" Font-Size="Small"
                    ForeColor="Red" ValidationGroup="Upload"></asp:RequiredFieldValidator>
            </span>
        </p>
        <p>
            <label for="sf" style="vertical-align: top">
                Notes / Comment
            </label>
            &nbsp;<span class="field_desc">
                <asp:TextBox CssClass="mf" runat="server" ID="txtNotes" TextMode="MultiLine" Columns="50"
                    Rows="8" ValidationGroup="Upload" />
            </span>
        </p>
        <p>
            <label for="sf">
                Upload Document
            </label>
            &nbsp;<span class="field_desc"><asp:FileUpload ID="FileUploadDoc" runat="server" />
            </span>
        </p>
        <p class="errorMsg">
            <br />
            <asp:Literal ID="lblMsg" runat="server"></asp:Literal>
            <br />
        </p>
        <p>
            <asp:Button ID="lnkUpload" runat="server" CssClass="button" Text="Save" ValidationGroup="Upload"
                OnClick="lnkUpload_Click" />
        </p>
    </asp:Panel>
</div>
