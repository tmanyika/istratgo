﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HR.Human.Resources;
using scorecard.implementations;

namespace scorecard.ui.controls.hr
{
    public partial class raceMgt : System.Web.UI.UserControl
    {
        IRace creed;

        public raceMgt()
        {
            creed = new RaceBL();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ResetLabels();
            if (!IsPostBack)
            {
                LoadData();
            }
        }

        private void ResetLabels()
        {
            LblError.Text = "";
        }

        void LoadData()
        {
            try
            {
                var data = creed.GetByStatus(true);
                dtPagerlvComps.Visible = (data.Count > dtPagerlvComps.PageSize) ? true : false;
                BindControl.BindListView(lvData, data);
            }
            catch (Exception e)
            {
                BindControl.BindLiteral(LblError, Messages.GetErrorMessage());
                Util.LogErrors(e);
            }
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            lvData.InsertItemPosition = InsertItemPosition.LastItem;
            LoadData();
        }

        void CreateNewRace(ListViewCommandEventArgs e)
        {
            try
            {
                string description = (e.Item.FindControl("txtDescription") as TextBox).Text;
                string name = (e.Item.FindControl("txtName") as TextBox).Text;
                bool added = creed.Add(new Race
                {
                    Active = true,
                    RaceDescription = description,
                    RaceName = name,
                    CreatedBy = Util.user.LoginId
                });

                if (added)
                {
                    LoadReadOnly();
                    LoadData();
                }
                BindControl.BindLiteral(LblError, added ? Messages.GetSaveMessage() : Messages.GetAlreadyExistMessage());
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(LblError, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        void LoadReadOnly()
        {
            lvData.EditIndex = -1;
            lvData.InsertItemPosition = InsertItemPosition.None;
        }

        void UpdateRace(ListViewDataItem e)
        {
            try
            {
                string description = (e.FindControl("txtEditDescription") as TextBox).Text;
                string name = (e.FindControl("txtEditName") as TextBox).Text;
                bool active = (e.FindControl("chkActive") as CheckBox).Checked;
                var hdnRaceId = e.FindControl("hdnRaceId") as HiddenField;

                if (string.IsNullOrEmpty(hdnRaceId.Value))
                {
                    BindControl.BindLiteral(LblError, Messages.GetUpdateFailedMessage());
                    return;
                }

                if (string.IsNullOrEmpty(name))
                {
                    BindControl.BindLiteral(LblError, Messages.GetRequiredFieldsMessage(true, "Name"));
                    return;
                }

                int raceId = int.Parse(hdnRaceId.Value);
                bool updated = creed.Update(new Race
                {
                    Active = true,
                    RaceDescription = description,
                    RaceId = raceId,
                    RaceName = name,
                    UpdatedBy = Util.user.LoginId
                });

                if (updated)
                {
                    LoadReadOnly();
                    LoadData();
                }
                BindControl.BindLiteral(LblError, updated ? Messages.GetUpdateMessage() : Messages.GetAlreadyExistMessage());
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(LblError, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        void UpdateStatus(ListViewDataItem e)
        {
            try
            {
                var hdnRaceId = e.FindControl("hdnRaceId") as HiddenField;
                if (string.IsNullOrEmpty(hdnRaceId.Value))
                {
                    BindControl.BindLiteral(LblError, Messages.GetDeleteFailedMessage());
                    return;
                }

                int raceId = int.Parse(hdnRaceId.Value);
                bool deactivated = creed.SetStatus(new Race
                {
                    Active = false,
                    RaceId = raceId,
                    UpdatedBy = Util.user.LoginId
                });

                if (deactivated)
                {
                    LoadReadOnly();
                    LoadData();
                }
                BindControl.BindLiteral(LblError, deactivated ? Messages.GetDeleteMessage() : Messages.GetDeleteFailedMessage());
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(LblError, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        void CancelUpdate()
        {
            LoadReadOnly();
            LoadData();
        }

        protected void lvData_ItemCanceling(object sender, ListViewCancelEventArgs e)
        {
            CancelUpdate();
        }

        protected void lvData_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            LblError.Text = "";
            if (e.Item.ItemType == ListViewItemType.InsertItem ||
                e.Item.ItemType == ListViewItemType.DataItem)
            {
                switch (e.CommandName)
                {
                    case "add":
                        CreateNewRace(e);
                        break;
                }
            }
        }

        protected void lvData_ItemDeleting(object sender, ListViewDeleteEventArgs e)
        {
            ListViewDataItem el = lvData.Items[e.ItemIndex];
            UpdateStatus(el);
        }

        protected void lvData_ItemUpdating(object sender, ListViewUpdateEventArgs e)
        {
            ListViewDataItem el = lvData.Items[e.ItemIndex];
            UpdateRace(el);
        }

        protected void lvData_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            lvData.EditIndex = e.NewEditIndex;
            lvData.InsertItemPosition = InsertItemPosition.None;
            LoadData();
        }

        protected void lvData_PagePropertiesChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            lvData.InsertItemPosition = InsertItemPosition.None;
            LoadData();
        }

        protected void lnkCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("index.aspx", true);
        }
    }
}