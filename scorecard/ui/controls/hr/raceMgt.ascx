﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="raceMgt.ascx.cs" Inherits="scorecard.ui.controls.hr.raceMgt" %>
<%@ Import Namespace="scorecard.implementations" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:UpdatePanel ID="UpdatePanelData" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <h1>
            Race Management</h1>
        <asp:ListView ID="lvData" runat="server" OnItemCommand="lvData_ItemCommand" OnItemEditing="lvData_ItemEditing"
            OnPagePropertiesChanged="lvData_PagePropertiesChanged" OnItemCanceling="lvData_ItemCanceling"
            OnItemDeleting="lvData_ItemDeleting" OnItemUpdating="lvData_ItemUpdating">
            <LayoutTemplate>
                <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                    <thead>
                        <tr id="trFirst" runat="server">
                            <td style="width: 300px">
                                Race
                            </td>
                            <td align="left" style="width: 400px">
                                Description
                            </td>
                            <td style="width: 90px">
                                Active
                            </td>
                            <td width="80px">
                                Edit
                            </td>
                            <td width="80px">
                                Delete
                            </td>
                        </tr>
                        <tr id="ItemPlaceHolder" runat="server">
                        </tr>
                    </thead>
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tbody>
                    <tr class="odd">
                        <td>
                            <%# Eval("RaceName")%>
                            <asp:HiddenField ID="hdnRaceId" runat="server" Value='<%# Bind("RaceId")%>' />
                        </td>
                        <td>
                            <%# Eval("RaceDescription")%>
                        </td>
                        <td>
                            <%# Common.GetBooleanYesNo(Eval("Active"))%>
                        </td>
                        <td>
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkEdit" CommandName="edit" class="ui-state-default ui-corner-all"
                                    title="Edit Record"><span class="ui-icon ui-icon-pencil">
                        </span></asp:LinkButton>
                            </ul>
                        </td>
                        <td>
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkDelete" CommandName="delete" class="ui-state-default ui-corner-all"
                                    title="Delete Record" OnClientClick="return confirmDelete();"><span class="ui-icon ui-icon-trash">
                                    </span>
                                </asp:LinkButton>
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </ItemTemplate>
            <EditItemTemplate>
                <tbody>
                    <tr class="odd">
                        <td>
                            <asp:TextBox runat="server" ID="txtEditName" Text='<%# Eval("RaceName")%>' ValidationGroup="Edit"
                                Width="250px" /><asp:HiddenField ID="hdnRaceId" runat="server" Value='<%# Bind("RaceId")%>' />
                            <asp:RequiredFieldValidator ID="RqdName" runat="server" ErrorMessage="*" ValidationGroup="Edit"
                                ControlToValidate="txtEditName" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtEditDescription" Text='<%# Eval("RaceDescription")%>'
                                Width="300px" />
                        </td>
                        <td>
                            <asp:CheckBox ID="chkActive" runat="server" Checked='<%# Eval("Active")%>' />
                        </td>
                        <td>
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkSave" CommandName="update" class="ui-state-default ui-corner-all"
                                    title="Update Record" ValidationGroup="Edit"><span class="ui-icon ui-icon-disk">
                                     </span>
                                </asp:LinkButton>
                            </ul>
                        </td>
                        <td>
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkCancelUpdate" CommandName="cancel" class="ui-state-default ui-corner-all"
                                    title="Cancel Update" CausesValidation="false"><span class="ui-icon ui-icon-cancel">
                                    </span>
                                </asp:LinkButton>
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </EditItemTemplate>
            <InsertItemTemplate>
                <tbody>
                    <tr>
                        <td align="left" valign="top">
                            <asp:TextBox runat="server" ID="txtName" ValidationGroup="Insert" Width="250px" />
                            <asp:RequiredFieldValidator ID="RqdInsertName" runat="server" ErrorMessage="*" ValidationGroup="Insert"
                                ControlToValidate="txtName" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" valign="top">
                            <asp:TextBox runat="server" ID="txtDescription" Width="300px" />
                        </td>
                        <td align="left" valign="top">
                            <asp:CheckBox ID="chkActive" runat="server" Checked="true" Enabled="false" />
                        </td>
                        <td valign="top">
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkSave" class="ui-state-default ui-corner-all"
                                    title="Save this Record" CommandName="add" ValidationGroup="Insert"><span class="ui-icon ui-icon-disk">
                        </span></asp:LinkButton>
                            </ul>
                        </td>
                        <td valign="top">
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkCancel" class="ui-state-default ui-corner-all"
                                    title="Cancel Save " CommandName="cancel"><span class="ui-icon ui-icon-cancel">
                        </span></asp:LinkButton>
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </InsertItemTemplate>
            <EmptyDataTemplate>
                <span style="color: Red">There are no records to display.</span>
            </EmptyDataTemplate>
        </asp:ListView>
        <br />
        <asp:DataPager ID="dtPagerlvComps" runat="server" PagedControlID="lvData" PageSize="15">
            <Fields>
                <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True" ShowNextPageButton="False"
                    ShowPreviousPageButton="False" />
                <asp:NumericPagerField />
                <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True" ShowNextPageButton="False"
                    ShowPreviousPageButton="False" />
            </Fields>
        </asp:DataPager>
        <div>
            <br />
            <span style="color: Red">
                <asp:Literal ID="LblError" runat="server" /></span>
            <br />
            <br />
        </div>
        <div id="tabs-3">
            <p>
                <asp:Button ID="btnNew" runat="server" CssClass="button" Text="Add New Race" OnClick="btnNew_Click"
                    CausesValidation="false" />
                <asp:Button ID="lnkCancel0" runat="server" CssClass="button" Text="Done" CausesValidation="false"
                    PostBackUrl="~/ui/index.aspx" Visible="False" />
            </p>
        </div>
        <asp:UpdateProgress ID="UpdateProgressData" runat="server" AssociatedUpdatePanelID="UpdatePanelData">
            <ProgressTemplate>
                <div>
                    <asp:Image ID="imgOrg" runat="server" ImageUrl="~/ui/images/icons/activity.gif" AlternateText="Please wait..">
                    </asp:Image></div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
