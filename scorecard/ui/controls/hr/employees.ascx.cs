﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.implementations;
using scorecard.controllers;
using HR.Human.Resources;
using scorecard.entities;
using System.Web.Security;
using HR.Leave;
using System.Net.Mail;
using System.Email.Communication;

namespace scorecard.ui.controls
{
    public partial class employees : System.Web.UI.UserControl
    {
        #region Member Variables

        useradmincontroller userAdmin;
        structurecontroller orgunits;
        jobtitlesdefinitions impl;

        #endregion

        #region Properties

        bool IsAdministrator
        {
            get
            {
                int? roleId = Util.user.UserTypeId;
                return (roleId == Util.getTypeDefinitionsUserTypeAdministrator()) ? true : false;
            }
        }

        #endregion

        #region Default Constructors

        public employees()
        {
            orgunits = new structurecontroller(new structureImpl());
            userAdmin = new useradmincontroller(new useradminImpl());
            impl = new jobtitlesdefinitions(new jobtitleImpl());

        }

        #endregion

        #region Page Event Handling Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            ClearLabels();
            if (!IsPostBack)
            {
                FillEmployees();
            }
        }

        #endregion

        #region Databinding Methods

        void FillEmployees()
        {
            try
            {
                IEmployee obj = new EmployeeBL();
                List<Employee> data = IsAdministrator ? obj.GetByCompanyId(true, Util.user.CompanyId) : // obj.GetByOrgStructure(Util.user.OrgId, true) :
                                                        obj.GetAll(Util.user.OrgId);
                BindControl.BindListView(lvUsers, data);
                PagerData.Visible = (data.Count > PagerData.PageSize);
                mainView.SetActiveView(vwData);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void BindLineManagers(int empId, int orgUnitId)
        {
            IEmployee obj = new EmployeeBL();
            var data = obj.GetByOrgStructureUp(orgUnitId, true);//obj.GetByOrgStructure(Util.user.OrgId, true);//obj.GetAll(Util.user.OrgId);
            BindControl.BindDropdown(ddlUsersReportsTo, "FullName", "EmployeeId", data);
            ListItem user = ddlUsersReportsTo.Items.FindByValue(empId.ToString());
            if (user != null) ddlUsersReportsTo.Items.Remove(user);
        }

        void BindUserRoles()
        {
            var data = userAdmin.appsettings.getChildSettings(new applicationsettings { PARENT_ID = Util.getTypeDefinitionsSystemUserTypesParentId() }).ToList();
            BindControl.BindDropdown(ddlUserType, "NAME", "TYPE_ID", data);
        }

        void BindOrgUnits()
        {
            var data = orgunits.getActiveCompanyOrgStructure(Util.user.CompanyId);
            BindControl.BindDropdown(ddlOrgUnit, "ORGUNIT_NAME", "ID", data);
        }

        void BindPositions()
        {
            var data = impl.getAllCompanyJobTitles(new company { COMPANY_ID = Util.user.CompanyId });
            BindControl.BindDropdown(ddlJobTitle, "NAME", "ID", data);
        }

        void BindStatus()
        {
            IEmployeeStatus obj = new EmployeeStatusBL();
            var data = obj.GetByStatus(true);
            BindControl.BindDropdown(ddStatus, "StatusName", "StatusId", data);
        }

        void BindWorkingWeeks()
        {
            IWorkingWeek obj = new WorkingWeekBL();
            var data = obj.GetByStatus(Util.user.CompanyId, true);
            BindControl.BindDropdown(ddWeek, "Name", "WorkingWeekId", data);
        }

        void BindRaces()
        {
            IRace obj = new RaceBL();
            var data = obj.GetByStatus(true);
            BindControl.BindDropdown(ddRace, "RaceName", "RaceId", "- select race -", "-1", data);
        }

        void FillDropDowns(int empId, int orgUnitId, bool isEdit)
        {
            BindOrgUnits();
            BindPositions();
            BindStatus();
            BindUserRoles();
            BindWorkingWeeks();
            BindRaces();
            BindLineManagers(empId, isEdit ? orgUnitId : int.Parse(ddlOrgUnit.SelectedValue));
        }

        #endregion

        #region Utility Methods

        void HideControls(bool editing)
        {
            //Buttons
            lnkUpdateUser.Visible = editing ? true : false;
            lnkSaveAddUser.Visible = !editing ? true : false;

            //Text Boxes
            txtLoginName.Disabled = editing ? true : false;
            txtConfirmPassword.Disabled = editing ? true : false;
            txtPassword.Disabled = editing ? true : false;

            //Panel
            pnlPassword.Visible = !editing ? true : false;

            //Validators
            rqdPass.Enabled = !editing ? true : false;
            rqdPin.Enabled = !editing ? true : false;
        }

        void ClearLabels()
        {
            lblError.Text = string.Empty;
            lblMsg.Text = string.Empty;
        }

        void ClearText()
        {
            txtConfirmPassword.Value = "";
            txtEmployeeNo.Value = "";
            txtFirstName.Value = "";
            txtMiddleName.Value = "";
            txtSurname.Value = "";
            txtEmailAddress.Value = "";
            txtLoginName.Value = "";
            txtCellNo.Value = "";
            txtTel.Value = "";
        }

        void ShowMessage(Literal lbl, string msg)
        {
            lbl.Text = msg;
        }

        public string StrConvToUpper(object fullname)
        {
            return fullname.ToString().ToUpper();
        }

        public string GetUrl(object employeeId, object fullName)
        {
            return string.Format("employeeinfo.aspx?Id={0}&FullName={1}", employeeId, HttpUtility.HtmlEncode(fullName));
        }

        void AddEmployeeInfo(int empId)
        {
            try
            {
                string password = txtPassword.Value.Trim();
                string userName = txtLoginName.Value;
                string firstName = txtFirstName.Value;
                string middleName = txtMiddleName.Value;
                string surname = txtSurname.Value;
                string cellphone = txtCellNo.Value;
                string telephone = txtTel.Value;
                string email = txtEmailAddress.Value;
                string createdBy = Util.user.LoginId;
                string employeeNo = txtEmployeeNo.Value;
                string idNumber = txtIdNumber.Value;
                string citizenship = txtCitizenship.Value;
                string passportNo = txtPassportNo.Value;

                string pass = FormsAuthentication.HashPasswordForStoringInConfigFile(password, "sha1");

                DateTime? defDate = null;
                if (!string.IsNullOrEmpty(txtStartDate.Text))
                    defDate = DateTime.Parse(txtStartDate.Text.Trim());

                DateTime? dateOfBirth = null;
                if (!string.IsNullOrEmpty(txtDateOfBirth.Text))
                    dateOfBirth = DateTime.Parse(txtDateOfBirth.Text.Trim());

                int? defInt = null;
                int? raceId = (ddRace.SelectedIndex != 0) ? int.Parse(ddRace.SelectedValue) : defInt;
                int? workWeekId = (!string.IsNullOrEmpty(ddWeek.SelectedValue)) ? int.Parse(ddWeek.SelectedValue) : defInt;
                int? statusId = (!string.IsNullOrEmpty(ddStatus.SelectedValue)) ? int.Parse(ddStatus.SelectedValue) : defInt;
                int companyId = int.Parse(ddlOrgUnit.SelectedValue);
                int? positionId = (!string.IsNullOrEmpty(ddlJobTitle.SelectedValue)) ? int.Parse(ddlJobTitle.SelectedValue) : defInt;
                int? lineManagerId = (!string.IsNullOrEmpty(ddlUsersReportsTo.SelectedValue)) ? int.Parse(ddlUsersReportsTo.SelectedValue) : defInt;
                int roleId = int.Parse(ddlUserType.SelectedValue);
                bool active = chkActive.Checked;

                if (empId <= 0 && !active)
                {
                    active = true;
                }

                Employee emp = new Employee
                {
                    Active = active,
                    BusTelephone = telephone,
                    Cellphone = cellphone,
                    CreatedBy = createdBy,
                    DateCreated = DateTime.Now,
                    DateUpdated = DateTime.Now,
                    EmailAddress = email,
                    EmployeeId = empId,
                    EmployeeNo = employeeNo,
                    FirstName = firstName,
                    FullName = string.Format("{0} {1} {2}", firstName, middleName, surname),
                    JobTitleId = positionId,
                    LastName = surname,
                    LineManagerEmployeeId = lineManagerId,
                    MiddleName = middleName,
                    OrgUnitId = companyId,
                    Pass = pass,
                    RoleId = roleId,
                    StatusId = statusId,
                    UpdatedBy = createdBy,
                    UserName = userName,
                    WorkingWeekId = workWeekId,
                    DateOfAppointment = defDate,
                    RaceId = raceId,
                    PassportNo = passportNo,
                    IDNumber = idNumber,
                    DateOfBirth = dateOfBirth,
                    Citizenship = citizenship
                };

                IEmployee obj = new EmployeeBL();
                bool saved = (empId <= 0) ? obj.AddEmployee(emp) : obj.UpdateEmployee(emp);
                if (saved)
                {
                    FillEmployees();
                    ShowMessage(lblMsg, Messages.GetUpdateMessage());
                    if (empId <= 0)
                    {
                        SendMail(emp, password);
                    }
                }
                else
                {
                    ShowMessage(lblError, Messages.GetSaveFailedMessage("employee"));
                }
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void EditUser(int empId)
        {
            try
            {
                IEmployee obj = new EmployeeBL();
                Employee obje = obj.GetById(empId);

                DateTime defDate = DateTime.Now;
                DateTime dateDobVal = obje.DateOfBirth.HasValue ?
                                (DateTime)obje.DateOfBirth : defDate;

                DateTime dateVal = obje.DateOfAppointment.HasValue ?
                               (DateTime)obje.DateOfAppointment : defDate;

                ClearText();
                FillDropDowns(empId, obje.OrgUnitId, true);
                HideControls(true);

                hdnEmpId.Value = empId.ToString();

                txtConfirmPassword.Value = "";
                txtEmployeeNo.Value = obje.EmployeeNo;
                txtFirstName.Value = obje.FirstName;
                txtMiddleName.Value = obje.MiddleName;
                txtSurname.Value = obje.LastName;
                txtEmailAddress.Value = obje.EmailAddress;
                txtLoginName.Value = obje.UserName;
                txtCellNo.Value = obje.Cellphone;
                txtTel.Value = obje.BusTelephone;
                txtPassportNo.Value = obje.PassportNo;
                txtIdNumber.Value = obje.IDNumber;
                txtCitizenship.Value = obje.Citizenship;
                txtDateOfBirth.Text = obje.DateOfBirth.HasValue ?
                                    string.Format("{0}/{1}/{2}", dateDobVal.Month, dateDobVal.Day, dateDobVal.Year)
                                    : string.Empty;
                txtStartDate.Text = obje.DateOfAppointment.HasValue ?
                                    string.Format("{0}/{1}/{2}", dateVal.Month, dateVal.Day, dateVal.Year)
                                    : string.Empty;
                chkActive.Checked = obje.Active;

                if (ddlJobTitle.Items.FindByValue(obje.JobTitleId.ToString()) != null)
                    ddlJobTitle.SelectedValue = obje.JobTitleId.ToString();
                if (ddlOrgUnit.Items.FindByValue(obje.OrgUnitId.ToString()) != null)
                    ddlOrgUnit.SelectedValue = obje.OrgUnitId.ToString();

                if (ddlUsersReportsTo.Items.FindByValue(obje.LineManagerEmployeeId.ToString()) == null)
                {
                    if (obje.LineManagerEmployeeId != null)
                    {
                        Employee objl = obj.GetById((int)obje.LineManagerEmployeeId);
                        if (objl.FirstName == string.Empty)
                            ddlJobTitle.Items.Insert(0, new ListItem
                            {
                                Selected = true,
                                Text = objl.FullName,
                                Value = objl.EmployeeId.ToString()
                            });
                    }
                }
                else if (ddlUsersReportsTo.Items.FindByValue(obje.LineManagerEmployeeId.ToString()) != null)
                    ddlUsersReportsTo.SelectedValue = obje.LineManagerEmployeeId.ToString();

                if (ddlUserType.Items.FindByValue(obje.RoleId.ToString()) != null)
                    ddlUserType.SelectedValue = obje.RoleId.ToString();
                if (ddStatus.Items.FindByValue(obje.StatusId.ToString()) != null)
                    ddStatus.SelectedValue = obje.StatusId.ToString();
                if (ddWeek.Items.FindByValue(obje.WorkingWeekId.ToString()) != null)
                    ddWeek.SelectedValue = obje.WorkingWeekId.ToString();
                if (ddRace.Items.FindByValue(obje.RaceId.ToString()) != null)
                    ddRace.SelectedValue = obje.RaceId.ToString();

                mainView.SetActiveView(vwForm);
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                ShowMessage(lblMsg, Messages.GetErrorMessage());
            }
        }

        void SendMail(Employee user, string password)
        {
            try
            {
                string result = string.Empty;
                int emailId = Util.getNewUserEmailId();

                MailAddressCollection addy = new MailAddressCollection();
                addy.Add(new MailAddress(user.EmailAddress, user.FullName));

                object[] mydata = { user.UserName, password };
                IMailRelay objm = new MailRelayBL();
                MailRelay obj = new MailRelay
                {
                    MailId = emailId,
                    Recipient = addy,
                    Data = mydata
                };
                objm.SendMail(obj, out result);
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, string.Format("{0} {1}", lblMsg.Text, Messages.GetEmailFailedMessage()));
                Util.LogErrors(ex);
            }
        }

        void InitialiseForm()
        {
            chkActive.Checked = true;
            ClearText();
            HideControls(false);

            FillDropDowns(0, Util.user.OrgId, false);
        }

        #endregion

        #region Button Event Handling Methods

        protected void btnNew_Click(object sender, EventArgs e)
        {
            InitialiseForm();
            mainView.SetActiveView(vwForm);
        }

        protected void lnkSaveAddUser_Click(object sender, EventArgs e)
        {
            AddEmployeeInfo(0);
        }

        protected void lnkCancelAddUser_Click(object sender, EventArgs e)
        {
            ClearText();
            mainView.SetActiveView(vwData);
        }

        protected void lnkUpdateUser_Click(object sender, EventArgs e)
        {
            int empId = (!string.IsNullOrEmpty(hdnEmpId.Value)) ? int.Parse(hdnEmpId.Value) : 0;
            AddEmployeeInfo(empId);
        }

        protected void lnkCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("index.aspx");
        }

        #endregion

        #region ListView Event Handling Methods

        protected void lvUsers_PagePropertiesChanged(object sender, EventArgs e)
        {
            FillEmployees();
        }

        protected void lvUsers_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            try
            {
                var empId = lvUsers.Items[e.NewEditIndex].FindControl("hdnId") as HiddenField;
                if (empId != null && empId.Value != "")
                    EditUser(int.Parse(empId.Value));
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                ShowMessage(lblMsg, Messages.GetErrorMessage());
            }
        }

        #endregion

        #region Dropdown Event Handling

        protected void ddlOrgUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            int orgUnitId = int.Parse(ddlOrgUnit.SelectedValue);
            BindLineManagers(0, orgUnitId);
        }

        #endregion
    }
}