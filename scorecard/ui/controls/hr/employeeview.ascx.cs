﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.implementations;
using scorecard.controllers;
using HR.Human.Resources;
using scorecard.entities;
using System.Web.Security;
using HR.Leave;
using System.Net.Mail;
using System.Email.Communication;

namespace scorecard.ui.controls
{
    public partial class employeeview : System.Web.UI.UserControl
    {
        #region Members

        IEmployee obj;

        #endregion

        #region Properties

        int EmployeeId
        {
            get
            {
                return int.Parse(Request.QueryString["Id"].ToString());
            }
        }

        string EmployeeName
        {
            get
            {
                return HttpUtility.HtmlDecode(Request.QueryString["FullName"].ToString());
            }
        }

        #endregion

        #region Default Constructors

        public employeeview()
        {
            obj = new EmployeeBL();
        }

        #endregion

        #region Page Event Handling Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillData();
            }
        }

        #endregion

        #region Databinding Methods

        void FillData()
        {
            try
            {
                int employeeId = EmployeeId;

                List<Employee> data = new List<Employee>();
                data.Add(obj.GetById(employeeId));

                BindControl.BindRepeater(rptData, data);
                //BindControl.BindLiteral(LblName, EmployeeName);
                BindControl.BindLiteral(lblMsg, data.Count <= 0 ? Messages.GetRecordNotFoundMessage() : string.Empty);
            }
            catch (Exception e)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(e);
            }
        }

        #endregion
    }
}