﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.implementations;
using scorecard.controllers;
using scorecard.entities;
using HR.Human.Resources;
using ScoreCard.Common.Interfaces;
using ScoreCard.Common.Logic;
using ThreeSixty.Evaluation;
using ScoreCard.Common.Config;

namespace scorecard.ui.controls
{
    public partial class score360Form : System.Web.UI.UserControl
    {
        #region  Variables

        IScoreCardArea area;
        IEvaluationSubmission sub;

        bool isAdmin;
        #endregion

        #region Default Class Constructors

        public score360Form()
        {
            area = new ScoreCardAreaBL();
            sub = new EvaluationSubmissionBL();
            isAdmin = (Util.user == null) ? false : Util.user.UserTypeId == Util.getTypeDefinitionsUserTypeAdministrator();
        }

        #endregion

        #region Page Events Handling Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadAreas(ddlArea);
            }
        }

        #endregion

        #region Databinding Methods

        void ClearDropDowns()
        {
            ddDate.Items.Clear();
            ddlValueTypes.Items.Clear();
        }

        void LoadAreas(DropDownList drpdwn)
        {
            var data = area.GetByStatus(true);
            BindControl.BindDropdown(drpdwn, "AreaAliasName", "AreaId", "-select all -", "0", data);
        }

        void LoadAreaValues(int areaId)
        {
            int orgUnitId = Util.user.OrgId;
            int compId = Util.user.CompanyId;
            int employeeId = Util.user.UserId;

            var data = new object();

            string dataValueField = "AreaItemId";
            string dataTextField = "AreaItemName";

            if (areaId == ScoreCardAreaConfig.EmployeeAreaId ||
                areaId == ScoreCardAreaConfig.JobTitleAreaId
                )
            {
                data = isAdmin ? sub.GetEmployeesByCompanyId(compId, areaId) :
                                sub.GetEmployeesByEmployeeId(employeeId, areaId);
            }
            else if (areaId == ScoreCardAreaConfig.OrgUnitAreaId)
            {
                data = isAdmin ? sub.GetOrgUnitsInHirachy(orgUnitId) :
                                sub.GetOrgUnitListByOwnerId(employeeId);
            }
            else if (areaId == ScoreCardAreaConfig.ProjectAreaId)
            {
                data = isAdmin ? sub.GetProjectsByCompanyId(compId) :
                                 sub.GetProjectsByOwnerId(employeeId);
            }

            BindControl.BindDropdown(ddlValueTypes, dataTextField, dataValueField, "-select value -", "0", data);
        }

        void LoadReportDates()
        {
            int areaId = int.Parse(ddlArea.SelectedValue);
            int areaItemId = int.Parse(ddlValueTypes.SelectedValue);
            bool isDetailed = (ddRepName.SelectedIndex == 3 || ddRepName.SelectedIndex == 4);
            bool isInternal = (ddRepName.SelectedIndex == 3) ? true : (ddRepName.SelectedIndex == 4) ? false : true;

            var dates = isDetailed ? sub.GetEvaluationPeriods(areaId, areaItemId, isInternal) : 
                                     sub.GetEvaluationPeriods(areaId, areaItemId);
            BindControl.BindDropdown(ddDate, "AreaItemDescription", "AreaItemName", "- select date -", "0", dates);

            DownloadReport(true);
        }

        void DownloadReport(bool addEventToButton)
        {
            bool isInternal = (ddRepName.SelectedIndex == 3) ? true : (ddRepName.SelectedIndex == 4) ? false : true;
            string pageUrl = ddRepName.SelectedValue.Replace("_","");
            string script = addEventToButton ? string.Format("return openNewWindow('{3}?aid={0}&idv={1}&dVal={2}&isIn={4}');", ddlArea.SelectedValue, ddlValueTypes.SelectedValue, ddDate.SelectedValue, pageUrl, isInternal) :
                                               string.Format("openNewWindow('{3}?aid={0}&idv={1}&dVal={2}&isIn={4}');", ddlArea.SelectedValue, ddlValueTypes.SelectedValue, ddDate.SelectedValue, pageUrl, isInternal);

           
            if (addEventToButton) btnViewReport.OnClientClick = script;
            else AjaxScriptManager.RegisterStartupScript(Page, typeof(Page), "myReport", script, true);
        }

        #endregion

        #region Dropdown Event Handling Methods

        protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            int areaId = int.Parse(ddlArea.SelectedValue);
            ClearDropDowns();
            LoadAreaValues(areaId);
        }

        protected void ddlValueTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadReportDates();
        }

        protected void ddDate_SelectedIndexChanged(object sender, EventArgs e)
        {
            DownloadReport(true);
        }

        protected void ddRepName_SelectedIndexChanged(object sender, EventArgs e)
        {
            DownloadReport(true);
        }

        #endregion

        #region Button Event Handling Methods

        protected void btnViewReport_Click(object sender, EventArgs e)
        {
            DownloadReport(false);
        }

        #endregion

    }
}