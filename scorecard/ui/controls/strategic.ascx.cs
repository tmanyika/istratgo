﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.controllers;
using scorecard.implementations;
using scorecard.entities;

namespace scorecard.ui.controls
{
    public partial class strategic : System.Web.UI.UserControl
    {
        goaldefinition strategicGoals;

        public strategic()
        {
            strategicGoals = new goaldefinition(new goalsImpl());
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                loadStrategicObjectives();
        }

        void loadSetupTypes(DropDownList ddlPerspective)
        {
            try
            {
                int companyId = Util.user.CompanyId;
                ddlPerspective.DataTextField = "PERSPECTIVE_NAME";
                ddlPerspective.DataValueField = "PERSPECTIVE_ID";
                BindControl.BindDropdown(ddlPerspective, new perspectivemanagement(new perspectiveImpl()).get(companyId, true));
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        public string getTypeName(int id)
        {
            return new perspectivemanagement(new perspectiveImpl()).get(id).PERSPECTIVE_NAME;
        }

        void loadStrategicObjectives()
        {
            try
            {
                var objectives = strategicGoals.listStrategicGoals(new company { COMPANY_ID = Util.user.CompanyId });

                //if (objectives.Count <= 0)
                //{
                //    lvStategicObjectives.InsertItemPosition = InsertItemPosition.LastItem;
                //    loadInitialPerspectives();
                //}

                dtPagerStategicObjectives.Visible = (objectives.Count < dtPagerStategicObjectives.PageSize) ? false : true;

                lvStategicObjectives.DataSource = objectives;
                lvStategicObjectives.DataBind();
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        bool isDataValid(string name)
        {
            return string.IsNullOrEmpty(name) ? false : true;
        }

        void createNewObjective(ListViewCommandEventArgs e)
        {
            try
            {
                var ddlPerspective = e.Item.FindControl("ddlPerspective") as DropDownList;
                var txtObjective = e.Item.FindControl("txtObjectiveName") as TextBox;
                var txtRationale = e.Item.FindControl("txtRationale") as TextBox;

                if (!isDataValid(txtObjective.Text))
                {
                    lblMsg.Text = "Goal is required.";
                    return;
                }

                strategicGoals.defineStrategicGoal(new scorecard.entities.strategic
                {
                    COMPANY_ID = Util.user.CompanyId,
                    PERSPECTIVE = int.Parse(ddlPerspective.SelectedValue),
                    OBJECTIVE = txtObjective.Text.Trim(),
                    RATIONALE = txtRationale.Text,
                    UPDATE_BY = Util.user.LoginId,
                    CREATED_BY = Util.user.LoginId
                });
                lvStategicObjectives.InsertItemPosition = InsertItemPosition.None;
                loadStrategicObjectives();
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void updateObjectives(ListViewCommandEventArgs e)
        {
            try
            {
                var id = e.Item.FindControl("lblID") as Label;
                var ddlEditPerspective = e.Item.FindControl("ddlEditPerspective") as DropDownList;
                var txtObjective = e.Item.FindControl("txtEditObjectiveName") as TextBox;
                var txtRationale = e.Item.FindControl("txtEditRationale") as TextBox;

                strategicGoals.updateStrategicGoal(new scorecard.entities.strategic
                {
                    ID = int.Parse(id.Text),
                    COMPANY_ID = Util.user.CompanyId,
                    PERSPECTIVE = int.Parse(ddlEditPerspective.SelectedValue),
                    OBJECTIVE = txtObjective.Text.Trim(),
                    RATIONALE = txtRationale.Text,
                    UPDATE_BY = Util.user.LoginId,
                    CREATED_BY = Util.user.LoginId
                });
                lvStategicObjectives.InsertItemPosition = InsertItemPosition.None;
                loadStrategicObjectives();
                lvStategicObjectives.EditIndex = -1;
                loadStrategicObjectives();
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void loadInitialPerspectives()
        {
            var ddlPerspective = lvStategicObjectives.InsertItem.FindControl("ddlPerspective") as DropDownList;
            if (ddlPerspective != null) loadSetupTypes(ddlPerspective);

        }

        void deleteStategicObjective(ListViewCommandEventArgs e)
        {
            try
            {
                var lblID = e.Item.FindControl("lblID") as Label;
                //var associatedGoals = strategicGoals.listGeneralGoals(new company { COMPANY_ID = Util.user.CompanyId }).Where(t => t.STRATEGIC_ID == int.Parse(lblID.Text)).ToList();
                //if (associatedGoals.Count == 0)
                //{
                strategicGoals.deleteStrategicGoal(new scorecard.entities.strategic
                {
                    ID = int.Parse(lblID.Text),
                    UPDATE_BY = Util.user.LoginId,
                    CREATED_BY = Util.user.LoginId
                });
                lblMsg.Text = "";
                //}
                //else
                //{
                //    lblMsg.Text = "This record cannot be deleted because it has sub records associated with it";
                //}
                loadStrategicObjectives();
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void cancelUpdate()
        {
            lvStategicObjectives.EditIndex = -1;
            lvStategicObjectives.InsertItemPosition = InsertItemPosition.None;
            loadStrategicObjectives();
        }

        protected void lnkCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("index.aspx");
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            lvStategicObjectives.InsertItemPosition = InsertItemPosition.LastItem;
            loadStrategicObjectives();
            loadInitialPerspectives();
        }

        protected void lvStategicObjectives_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.InsertItem || e.Item.ItemType == ListViewItemType.DataItem)
            {

                switch (e.CommandName)
                {
                    case "addNewStategicObjective":
                        createNewObjective(e);
                        break;
                    case "updateStategicObjective":
                        updateObjectives(e);
                        break;
                    case "deleteStategicObjective":
                        deleteStategicObjective(e);
                        break;
                    case "cancelUpdate":
                        cancelUpdate();
                        break;
                    case "Edit":

                        break;
                }
            }
        }

        protected void lvStategicObjectives_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            try
            {
                lvStategicObjectives.EditIndex = e.NewEditIndex;
                lvStategicObjectives.InsertItemPosition = InsertItemPosition.None;
                loadStrategicObjectives();
                var ddlPerspective = lvStategicObjectives.Items[e.NewEditIndex].FindControl("ddlEditPerspective") as DropDownList;
                loadSetupTypes(ddlPerspective);
                var perspectiveId = (lvStategicObjectives.Items[e.NewEditIndex].FindControl("hdnPerspectiveId") as HiddenField).Value;
                ddlPerspective.SelectedValue = perspectiveId;
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        protected void lvStategicObjectives_PagePropertiesChanged(object sender, EventArgs e)
        {
            loadStrategicObjectives();
        }

        protected void dtPagerStategicObjectives_PreRender(object sender, EventArgs e)
        {
            if (lvStategicObjectives.EditIndex == -1 &&
                 lvStategicObjectives.InsertItemPosition != InsertItemPosition.LastItem)
                loadStrategicObjectives();
        }
    }
}