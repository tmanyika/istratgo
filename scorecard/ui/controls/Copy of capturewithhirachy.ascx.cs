﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.controllers;
using scorecard.entities;
using scorecard.interfaces;
using scorecard.implementations;
using HR.Human.Resources;
using HierarchicalData;
using System.Data;
using System.Collections;
using System.Text;
using Scorecard.Business.Rules;
using vb = Microsoft.VisualBasic;
using System.Net.Mail;
using System.Email.Communication;
using Telerik.Web.UI;
using System.IO;
using System.Security.Permissions;

namespace scorecard.ui.controls
{
    public partial class capturewithhirachy : System.Web.UI.UserControl
    {
        #region Member Variables, Properties
        goaldefinition goalDef;
        useradmincontroller usersDef;
        scoresmanager scoresManager;
        structurecontroller orgunits;
        IBusinessRule bR;
        IScoreRating iR;

        #endregion

        #region Default Constructors

        public capturewithhirachy()
        {
            orgunits = new structurecontroller(new structureImpl());
            goalDef = new goaldefinition(new goalsImpl());
            usersDef = new useradmincontroller(new useradminImpl());
            scoresManager = new scoresmanager(new scoresImpl());
            bR = new BusinessRuleBL();
            iR = new ScoreRatingBL();
        }

        #endregion

        #region Page Load Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            ClearLabels();
            if (!IsPostBack)
            {
                AssociateActionToButton();
                InitialiseObjects();
                LoadSubmissions();
            }
        }

        void AssociateActionToButton()
        {
            Page.Form.DefaultButton = btnSaveLater.UniqueID;
        }

        void ClearLabels()
        {
            lblDateUsed.Text = string.Empty;
            LblDelete.Text = string.Empty;
            lblMsg.Text = string.Empty;
            LblNoData.Text = string.Empty;
            LblOutCome.Text = string.Empty;
        }

        void LoadSubmissions()
        {
            hdnUpdate.Value = "0";
            MainView.SetActiveView(vwData);
            DataTable dT = scoresManager.getPendingSubmissions(Util.user.UserId);
            BindControl.BindListView(lvSubmittedWorkflows, dT);
            lvSubmittedWorkflows.Visible = (dT.Rows.Count <= 0) ? false : true;
            LblNoData.Text = dT.Rows.Count > 0 ? string.Empty : Messages.GetRecordNotFoundMessage();
        }

        void InitialiseObjects()
        {
            Session["Files"] = new List<attachmentDocuments>();
            string fullPath = string.Format(@"{0}", Server.MapPath(Util.getConfigurationSettingUploadFolder()));
            RdAsyncUpd.TargetFolder = fullPath;
        }

        void initialiseForm()
        {
            try
            {
                txtDate.Text = string.Empty;
                MainView.SetActiveView(vwForm);
                getGoalsCriteriaTypes();
                BindControl.BindRepeater(rptFile, new List<attachmentDocuments>());
                SetVisibilityOfButtons(false);
                rptFile.Visible = rptFile.Items.Count > 0 ? true : false;
                string fullPath = Server.MapPath(Util.getConfigurationSettingUploadFolder());
                RdAsyncUpd.TargetFolder = fullPath;
                RdAsyncUpd.TemporaryFolder = fullPath;
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(LblNoData, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        void SetVisibilityOfButtons(bool visibul)
        {
            docFieldSet.Visible = visibul;
            btnNew.Visible = visibul;
            btnSaveLater.Visible = visibul;
        }

        #endregion

        #region Databinding Methods

        public string getObjectName(int id, int criteria)
        {
            try
            {
                if (id > 0)
                {
                    if (criteria == Util.getTypeDefinitionsRolesTypeID())
                    {
                        IEmployee obj = new EmployeeBL();
                        return obj.GetById(id).FullName;
                    }
                    else if (criteria == Util.getTypeDefinitionsProjectID()) return new projectmanagement(new projectImpl()).get(id).PROJECT_NAME;
                    else return orgunits.getOrgunitDetails(new Orgunit { ID = id }).ORGUNIT_NAME;
                }
            }
            catch (Exception e) { Util.LogErrors(e); }
            return string.Empty;
        }

        public string getTypeName(object id)
        {
            return (id == DBNull.Value ||
                id == null) ? string.Empty :
                goalDef.appsettings.getApplicationSetting(new entities.applicationsettings
                {
                    TYPE_ID = int.Parse(id.ToString())
                }).NAME;
        }

        int getUserAccountInfo(int ID)
        {
            try
            {
                if (ID == Util.getTypeDefinitionsRolesTypeID())
                {
                    IEmployee obj = new EmployeeBL();
                    var user = obj.GetById(int.Parse(ddlValueTypes.SelectedValue));
                    return user.JobTitleId == null ? 0 : (int)user.JobTitleId;
                }
                else return int.Parse(ddlValueTypes.SelectedValue);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return 0;
            }
        }

        DataTable GetData(bool reload, int companyId, int areaTypeId, int areaValueId)
        {
            if (Cache["Data"] == null) reload = true;
            if (reload) Cache["Data"] = goalDef.listGeneralGoals(Util.user.CompanyId, areaTypeId, areaValueId);
            return Cache["Data"] as DataTable;
        }

        DataTable GetData(bool reload, submissionWorkflow subflow)
        {
            if (Cache["Data"] == null) reload = true;
            if (reload) Cache["Data"] = scoresManager.getSubmittedScoresData(subflow);
            return Cache["Data"] as DataTable;
        }

        DataTable GetParentData(DataTable dT)
        {
            if (dT == null) return dT;
            DataTable dS = dT.Clone();
            DataRow[] rows = dT.Select("PARENT_ID IS NULL");

            foreach (DataRow rw in rows) dS.ImportRow(rw);
            dS.AcceptChanges();
            return dS;
        }

        DataTable GetChildrenData(DataTable dT, int parentId)
        {
            DataRow[] rows = dT.Select(string.Format("PARENT_ID = {0}", parentId));
            DataTable dS = dT.Clone();
            foreach (DataRow rw in rows)
                dS.ImportRow(rw);
            dS.AcceptChanges();
            return dS;
        }

        void loadGoals(bool reload)
        {
            try
            {
                if (ddlValueTypes.SelectedIndex == 0)
                {
                    pnlNoData.Visible = false;
                    BindControl.BindListView(lvGoals, new List<goals>());
                    return;
                }

                int areaTypeId = int.Parse(ddlArea.SelectedValue);
                int areaValueId = int.Parse(ddlValueTypes.SelectedValue);

                if (areaTypeId == Util.getTypeDefinitionsRolesTypeID())
                {
                    IEmployee obj = new EmployeeBL();
                    areaValueId = (int)obj.GetById(areaValueId).JobTitleId;
                }

                var objectives = GetParentData(GetData(reload, Util.user.CompanyId, areaTypeId, areaValueId));
                int numofrec = objectives.Rows.Count;
                pnlNoData.Visible = (numofrec <= 0);
                SetVisibilityOfButtons(numofrec > 0);
                BindControl.BindListView(lvGoals, objectives);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void loadGoals(bool reload, int selectedId, int criteria, int submittedId)
        {
            try
            {
                var objectives = GetParentData(GetData(reload, new submissionWorkflow
                {
                    ID = submittedId,
                    CRITERIA_TYPE_ID = criteria,
                    SELECTED_ITEM_VALUE = selectedId
                }));

                var totalCount = objectives.Rows.Count;
                SetVisibilityOfButtons(totalCount > 0);

                BindControl.BindListView(lvGoals, objectives);
                getAttachments(submittedId);
                if (totalCount > 0)
                {
                    string date = String.Format("{0:dd/MM/yyyy}", objectives.Rows[0]["PERIOD_DATE"]);
                    txtDate.Text = date;
                }
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void getGoalsCriteriaTypes()
        {
            var areaList = goalDef.appsettings.getChildSettings(new applicationsettings { PARENT_ID = Util.getTypeDefinitionsGoalAreaTypes() }).ToList();
            BindControl.BindDropdown(ddlArea, "DESCRIPTION", "TYPE_ID", "- select area -", "-1", areaList);
            ResetValueDropdown();

        }
        void ResetValueDropdown()
        {
            if (ddlArea.SelectedIndex == 0)
            {
                ddlValueTypes.Items.Clear();
                ddlValueTypes.Items.Add(new ListItem { Text = "- select option -", Value = "-1" });
            }
        }

        void loadSetupTypes(DropDownList ddlPerspective)
        {
            try
            {
                var setuptypes = goalDef.appsettings.getChildSettings(new entities.applicationsettings { PARENT_ID = Util.getTypeDefinitionsPerspectivesParentID() });
                BindControl.BindDropdown(ddlValueTypes, "NAME", "TYPE_ID", setuptypes);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void loadJobTitles()
        {
            try
            {
                IEmployee obj = new EmployeeBL();
                List<Employee> titles = obj.GetByOrgStructure(Util.user.OrgId, true);

                if (Util.user.UserTypeId != Util.getTypeDefinitionsUserTypeAdministrator())
                    titles = titles.Where(t => t.LineManagerEmployeeId == Util.user.UserId || t.EmployeeId == Util.user.UserId).ToList();
                BindControl.BindDropdown(ddlValueTypes, "FullName", "EmployeeId", "- select user -", "-1", titles);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void loadStrategicGoals()
        {
            try
            {
                var strategic = goalDef.listStrategicGoals(new company { COMPANY_ID = Util.user.CompanyId });
                BindControl.BindDropdown(ddlValueTypes, "OBJECTIVE", "ID", "- select project -", "-1", strategic);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        public string getCurrentStore(int ID)
        {
            return string.Empty;
        }

        public string getFinalScore(int ID)
        {
            return string.Empty;
        }

        void loadAreas()
        {
            try
            {
                var userRole = Util.user.UserTypeId;
                var userID = Util.user.UserId;
                var dV = orgunits.getActiveCompanyOrgStructure(Util.user.CompanyId);

                if (userRole != Util.getTypeDefinitionsUserTypeAdministrator())
                    dV.RowFilter = string.Format("OWNER_ID = {0}", userID);
                BindControl.BindDropdown(ddlValueTypes, "ORGUNIT_NAME", "ID", "- select org unit -", "-1", dV);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void loadProjects(int companyId)
        {
            projectmanagement proj = new projectmanagement(new projectImpl());

            int adminRoleId = Util.getTypeDefinitionsUserTypeAdministrator();
            int roleId = (int)Util.user.UserTypeId;

            IEnumerable<project> projL = proj.get(companyId, true);

            if (adminRoleId != Util.user.UserTypeId) projL = projL.Where(c => c.RESPONSIBLE_PERSON_ID == Util.user.UserId);

            BindControl.BindDropdown(ddlValueTypes, "PROJECT_NAME", "PROJECT_ID", "- select project -", "-1", projL);
        }

        void setCriteriaSelected()
        {
            try
            {
                int areaId = int.Parse(ddlArea.SelectedValue);
                if (areaId == Util.getTypeDefinitionsRolesTypeID()) loadJobTitles();
                else if (areaId == Util.getTypeDefinitionsProjectID()) loadProjects(Util.user.CompanyId);
                else loadAreas();
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        private entities.useradmin getApproverID(int id, int criteria)
        {
            try
            {
                if (criteria == Util.getTypeDefinitionsRolesTypeID())
                {
                    var approver_id = usersDef.getUserAccountInformation(new entities.useradmin { ID = (int)Util.user.ReportToUserId });
                    return approver_id;
                }
                else if (criteria == Util.getTypeDefinitionsProjectID())
                {
                    var proj = new projectmanagement(new projectImpl()).get(id);
                    var approver_id = new scorecard.entities.useradmin
                    {
                        ID = proj.RESPONSIBLE_PERSON_ID == null ? (int)Util.user.ReportToUserId : (int)proj.RESPONSIBLE_PERSON_ID,
                        NAME = proj.RESPONSIBLE_NAME,
                        EMAIL_ADDRESS = proj.RESPONSIBLE_EMAIL
                    };
                    return approver_id;
                }
                else
                {
                    var userId = goalDef.orgstructure.getOrgunitDetails(new Orgunit { ID = id }).OWNER_ID;
                    var approver_id = usersDef.getUserAccountInformation(new entities.useradmin { ID = userId });
                    return approver_id;
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return new entities.useradmin();
            }
        }

        bool dateAlreadyCaptured(string periodDate, int criteriaId, int itemValId)
        {
            List<scores> scoreLst = scoresManager.getUsedItemDates(criteriaId, itemValId);
            string tmpDate = periodDate.Replace('/', '-');
            foreach (scores obj in scoreLst)
            {
                DateTime dbdate = DateTime.Parse(obj.PERIOD_DATE_VAL);
                DateTime mydate = DateTime.Parse(periodDate);
                if (dbdate.Subtract(mydate).TotalDays == 0)
                    return true;
            }
            return false;
        }

        bool dateAlreadyCaptured(int submissionId, string periodDate, int criteriaId, int itemValId)
        {
            bool captured = scoresManager.scoreDateAlreadyExist(criteriaId, itemValId, submissionId, periodDate);
            return captured;
        }

        bool informationValid(bool saveForLater)
        {
            bool valid = true;
            foreach (ListViewItem item in lvGoals.Items)
            {
                HiddenField hdnHasSubGoals = item.FindControl("hdnHasSubGoals") as HiddenField;
                bool hassubgoals = string.IsNullOrEmpty(hdnHasSubGoals.Value) ? false : bool.Parse(hdnHasSubGoals.Value);
                if (!hassubgoals)
                {
                    Label lblUnitMeasureID = item.FindControl("lblUnitMeasureID") as Label;
                    Literal lblError = item.FindControl("lblError") as Literal;
                    TextBox txtScore = item.FindControl("txtScore") as TextBox;

                    lblError.Text = "";
                    int unitOfMeasure = int.Parse(lblUnitMeasureID.Text);
                    string val = txtScore.Text;

                    if (!string.IsNullOrEmpty(val))
                    {
                        if (bR.UnitOfMeasureRequireNumericValue(unitOfMeasure))
                        {
                            bool valNumeric = Util.IsNumeric(val); //vb.Information.IsNumeric(val);
                            if (!valNumeric)
                            {
                                valid = false;
                                ShowMessage(lblError, Messages.NumericValueRequired);
                            }
                        }
                        else if (bR.UnitOfMeasureRequireYesNoValue(unitOfMeasure))
                        {
                            bool yesNoValid = bR.UnitOfMeasureRequireYesNoValueValid(val);
                            if (!yesNoValid)
                            {
                                valid = false;
                                ShowMessage(lblError, Messages.YesNoValueRequired);
                            }
                        }
                    }
                    else if (!saveForLater)
                    {
                        valid = false;
                        lblError.Text = "*";
                    }
                }
                else
                {
                    Repeater rptData = item.FindControl("rptMeasureData") as Repeater;
                    foreach (RepeaterItem ritem in rptData.Items)
                    {
                        HiddenField hdnUnitMeasureId = ritem.FindControl("hdnUnitMeasureId") as HiddenField;
                        Literal lblError = ritem.FindControl("lblError") as Literal;
                        TextBox txtScore = ritem.FindControl("txtScore") as TextBox;

                        lblError.Text = string.Empty;

                        int unitOfMeasure = int.Parse(hdnUnitMeasureId.Value);
                        string val = txtScore.Text;

                        if (!string.IsNullOrEmpty(val))
                        {
                            if (bR.UnitOfMeasureRequireNumericValue(unitOfMeasure))
                            {
                                bool valNumeric = Util.IsNumeric(val); //vb.Information.IsNumeric(val);
                                if (!valNumeric)
                                {
                                    valid = false;
                                    ShowMessage(lblError, Messages.NumericValueRequired);
                                }
                            }
                            else if (bR.UnitOfMeasureRequireYesNoValue(unitOfMeasure))
                            {
                                bool yesNoValid = bR.UnitOfMeasureRequireYesNoValueValid(val);
                                if (!yesNoValid)
                                {
                                    valid = false;
                                    ShowMessage(lblError, Messages.YesNoValueRequired);
                                }
                            }
                        }
                        else if (!saveForLater)
                        {
                            valid = false;
                            lblError.Text = "*";
                        }
                    }
                }
            }
            return valid;
        }

        void ShowMessage(Literal Lbl, string message)
        {
            Lbl.Text = message;
        }

        void calculateValues(bool saveForLater)
        {
            var requestID = 0;
            bool updateMode = false;
            try
            {
                updateMode = string.Compare(hdnUpdate.Value, "0", true) == 0 ?
                                            false : true;
                var selectedItem = int.Parse(ddlValueTypes.SelectedValue);
                var selectedArea = int.Parse(ddlArea.SelectedValue);

                bool allupdated = true;
                bool changetosavelater = false;
                bool valid = informationValid(saveForLater);

                if (selectedItem <= 0 || selectedArea <= 0)
                {
                    lblDateUsed.Text = "area and value are required fields.";
                    return;
                }

                if (!valid)
                {
                    lblMsg.Text = "Please correct all invalid information before submitting.";
                    return;
                }

                var prevStatusId = updateMode ? int.Parse(hdnStatusId.Value) : 0;
                var submissionId = updateMode ? int.Parse(hdnSubmissionId.Value) : 0;
                var periodDate = Common.GetInternationalDateFormat(txtDate.Text, DateFormat.dayMonthYear);

                bool onceApproved = string.IsNullOrEmpty(hdnWasOnceApproved.Value) ?
                                      false : bool.Parse(hdnWasOnceApproved.Value);
                bool updateMngScore = !onceApproved;

                ClearLabels();
                //bool dateCaptured = updateMode ? false : dateAlreadyCaptured(submissionId, periodDate, selectedArea, selectedItem);
                bool dateCaptured = dateAlreadyCaptured(submissionId, periodDate, selectedArea, selectedItem);

                if (dateCaptured || !valid)
                {
                    lblDateUsed.Text = dateCaptured ? "You cannot capture information twice for the same date." : "Correct errors below.";
                    if (dateCaptured) { valid = false; }
                    uploadFiles(updateMode, valid, submissionId);
                    if (submissionId > 0) { getAttachments(submissionId); }
                    else { bindAttachments(Session["Files"] as List<attachmentDocuments>); }
                    return;
                }

                if (updateMode && !dateCaptured)
                { //in case the date has been updated
                    scoresManager.updateScoreDate(submissionId, periodDate);
                }

                var approver = new useradmincontroller(new useradminImpl()).getLineManagerUserAccount(selectedArea, selectedItem);
                var setarea = string.Format("The resource for Approving the captured scores could not be found. Please make sure this is set in the {0} section.",
                                                ddlValueTypes.SelectedItem.Text);

                if (Util.BlockCapturingIfApproveIdIsEmpty && !saveForLater)
                {
                    if (string.IsNullOrEmpty(approver.EMAIL_ADDRESS) || approver.ID <= 0)
                    {
                        lblMsg.Text = setarea;
                        return;
                    }
                }
                else
                {
                    if (!saveForLater)
                    {
                        changetosavelater = true;
                    }
                    saveForLater = true;
                }

                var newRequest = new submissionWorkflow
                {
                    CRITERIA_TYPE_ID = int.Parse(ddlArea.SelectedValue),
                    SELECTED_ITEM_VALUE = int.Parse(ddlValueTypes.SelectedValue),
                    STATUS = (saveForLater || approver.ID <= 0) ? Util.getTypeDefinitionsWorkFlowSaveForLaterId() :
                                            Util.getTypeDefinitionsWorkFlowNewRequestId(),
                    APPROVER_ID = approver.ID,
                    CAPTURER_ID = Util.user.UserId,
                    ID = submissionId
                };

                if (updateMode)
                {
                    scoresManager.updateWorkFlow(newRequest);
                    requestID = submissionId;
                }
                else
                {
                    requestID = scoresManager.createWorkFlow(newRequest);
                }

                if (requestID > 0)
                {
                    decimal ratingVal = iR.GetCompanyRating(Util.user.CompanyId, true);
                    for (int i = 0; i < lvGoals.Items.Count; i++)
                    {
                        var lblUnitMeasureID = lvGoals.Items[i].FindControl("lblUnitMeasureID") as Label;
                        var lblTotal = lvGoals.Items[i].FindControl("lblTotal") as Label;
                        var lblWeight = lvGoals.Items[i].FindControl("lblWeight") as Label;
                        var lblTarget = lvGoals.Items[i].FindControl("lblTarget") as Label;
                        var txtScore = lvGoals.Items[i].FindControl("txtScore") as TextBox;
                        var lblID = lvGoals.Items[i].FindControl("lblID") as Label;
                        var txtComment = lvGoals.Items[i].FindControl("txtComment") as TextBox;
                        var txtEvidence = lvGoals.Items[i].FindControl("txtEvidence") as TextBox;
                        var hassubgoals = bool.Parse((lvGoals.Items[i].FindControl("hdnHasSubGoals") as HiddenField).Value);

                        int? parentId = null;
                        if (lvGoals.Items[i].ItemType == ListViewItemType.DataItem)
                        {
                            if (!hassubgoals)
                            {
                                var comment = txtComment.Text;
                                var evidence = txtEvidence.Text;
                                var unitMeasure = int.Parse(lblUnitMeasureID.Text);
                                var weight = decimal.Parse(lblWeight.Text);
                                var item_id = int.Parse(lblID.Text);

                                decimal answer = 0;
                                if (string.IsNullOrEmpty(txtScore.Text))
                                {
                                    answer = 0;
                                }
                                else
                                {
                                    if (bR.UnitOfMeasureRequireNumericValue(unitMeasure))
                                    {
                                        var target = decimal.Parse(lblTarget.Text);
                                        var score = decimal.Parse(txtScore.Text);
                                        answer = bR.FinalScore(unitMeasure, weight, score, target);
                                        lblTotal.Text = answer.ToString();
                                    }
                                    else
                                    {
                                        if (txtScore.Text.Trim().ToLower() != "yes")
                                        {
                                            lblTotal.Text = "0";
                                        }
                                        else
                                        {
                                            lblTotal.Text = weight.ToString();
                                            answer = weight;
                                        }
                                    }
                                }

                                decimal myscore = decimal.Parse(answer.ToString());
                                decimal finalAgreedScore = bR.GetDefinedWeightedScore(myscore, ratingVal);

                                scores _scoreDef = new scores
                                {
                                    EVIDENCE = evidence,
                                    JUSTIFICATION_COMMENT = comment,
                                    CRITERIA_TYPE_ID = int.Parse(ddlArea.SelectedValue),
                                    SELECTED_ITEM_VALUE = int.Parse(ddlValueTypes.SelectedValue),
                                    SCORES = txtScore.Text.Trim(),
                                    GOAL_ID = int.Parse(lblID.Text),
                                    FINAL_SCORE = double.Parse(answer.ToString()),
                                    SUBMISSION_ID = requestID,
                                    PERIOD_DATE = DateTime.Parse(periodDate),
                                    RATINGVALUE = ratingVal,
                                    FINAL_SCORE_AGREED = finalAgreedScore,
                                    ID = item_id,
                                    STATUS_ID = prevStatusId,
                                    HAS_SUB_GOALS = false,
                                    PARENT_ID = null,
                                    UPDATE_MNG_SCORE = updateMngScore
                                };
                                if (!updateMode) scoresManager.createNewScore(_scoreDef);
                                else scoresManager.updateScore(_scoreDef);
                            }
                            else
                            {
                                Repeater rptData = lvGoals.Items[i].FindControl("rptMeasureData") as Repeater;
                                if (!updateMode) //add the parent record and get the new Id
                                {
                                    int criteriaTypeId = int.Parse(ddlArea.SelectedValue);
                                    int selectedValue = int.Parse(ddlValueTypes.SelectedValue);
                                    scores primaryScore = new scores
                                    {
                                        EVIDENCE = string.Empty,
                                        JUSTIFICATION_COMMENT = string.Empty,
                                        CRITERIA_TYPE_ID = criteriaTypeId,
                                        SELECTED_ITEM_VALUE = selectedValue,
                                        GOAL_ID = int.Parse(lblID.Text),
                                        FINAL_SCORE = 0,
                                        SUBMISSION_ID = requestID,
                                        PERIOD_DATE = DateTime.Parse(periodDate),
                                        RATINGVALUE = ratingVal,
                                        FINAL_SCORE_AGREED = null,
                                        STATUS_ID = prevStatusId,
                                        HAS_SUB_GOALS = hassubgoals,
                                        PARENT_ID = null,
                                        UPDATE_MNG_SCORE = updateMngScore
                                    };
                                    parentId = scoresManager.createNewScore(primaryScore);
                                    if (parentId <= 0)
                                    {
                                        allupdated = false;
                                        if (!updateMode)
                                            scoresManager.deleteWorkFlow(requestID);
                                        ShowMessage(lblMsg, Messages.GetSaveFailedMessage());
                                        return;
                                    }
                                }

                                if ((parentId != null && !updateMode) || updateMode)
                                {
                                    foreach (RepeaterItem item in rptData.Items)
                                    {
                                        if (updateMode) //if in update mode get parent id already set
                                        {//not really necessary
                                            var hdnIndividualScoreId = item.FindControl("hdnParentId") as HiddenField;
                                            if (!string.IsNullOrEmpty(hdnIndividualScoreId.Value) && updateMode)
                                            { parentId = int.Parse(hdnIndividualScoreId.Value); }
                                        }
                                        var slblTarget = item.FindControl("hdnTarget") as HiddenField;
                                        var stxtScore = item.FindControl("txtScore") as TextBox;
                                        var slblID = item.FindControl("hdnSubId") as HiddenField;
                                        var stxtComment = item.FindControl("txtComment") as TextBox;
                                        var stxtEvidence = item.FindControl("txtEvidence") as TextBox;
                                        var hdnScoreId = item.FindControl("hdnScoreId") as HiddenField;
                                        var hdnUnitMeasureId = item.FindControl("hdnUnitMeasureId") as HiddenField;

                                        var comment = stxtComment.Text;
                                        var evidence = stxtEvidence.Text;
                                        var item_id = int.Parse(slblID.Value);
                                        var unitMeasure = int.Parse(hdnUnitMeasureId.Value);
                                        var weight = decimal.Parse(lblWeight.Text);

                                        decimal answer = 0;
                                        if (string.IsNullOrEmpty(stxtScore.Text))
                                        { answer = 0; }
                                        else
                                        {
                                            if (bR.UnitOfMeasureRequireNumericValue(unitMeasure))
                                            {
                                                var target = decimal.Parse(slblTarget.Value);
                                                var score = decimal.Parse(stxtScore.Text);
                                                answer = bR.FinalScore(unitMeasure, weight, score, target);
                                                lblTotal.Text = answer.ToString();
                                            }
                                            else
                                            {
                                                if (txtScore.Text.Trim().ToLower() != "yes")
                                                {
                                                    answer = 0;
                                                    lblTotal.Text = "0";
                                                }
                                                else
                                                {
                                                    lblTotal.Text = weight.ToString();
                                                    answer = weight;
                                                }
                                            }
                                        }

                                        decimal myscore = decimal.Parse(answer.ToString());
                                        decimal finalAgreedScore = bR.GetDefinedWeightedScore(myscore, ratingVal);
                                        scores _scoreDef = new scores
                                        {
                                            EVIDENCE = evidence,
                                            JUSTIFICATION_COMMENT = comment,
                                            CRITERIA_TYPE_ID = int.Parse(ddlArea.SelectedValue),
                                            SELECTED_ITEM_VALUE = int.Parse(ddlValueTypes.SelectedValue),
                                            SCORES = stxtScore.Text.Trim(),
                                            GOAL_ID = item_id,
                                            FINAL_SCORE = double.Parse(answer.ToString()),
                                            SUBMISSION_ID = requestID,
                                            PERIOD_DATE = DateTime.Parse(periodDate),
                                            RATINGVALUE = ratingVal,
                                            FINAL_SCORE_AGREED = finalAgreedScore,
                                            ID = item_id,
                                            STATUS_ID = prevStatusId,
                                            PARENT_ID = parentId,
                                            HAS_SUB_GOALS = false,
                                            UPDATE_MNG_SCORE = updateMngScore
                                        };
                                        if (!updateMode) scoresManager.createNewScore(_scoreDef);
                                        else scoresManager.updateScore(_scoreDef);
                                    }
                                    if (parentId > 0 && parentId != null)
                                        scoresManager.updateParentScore((int)parentId);
                                }
                                else
                                { //There is no parent so no records should be updated
                                    if (!updateMode)
                                    {
                                        allupdated = false;
                                        scoresManager.deleteWorkFlow(requestID);
                                        //deleted = true;
                                    }
                                    ShowMessage(lblMsg, Messages.GetSaveFailedMessage());
                                    return;
                                }
                            }
                        }
                    }

                    int workflowId = updateMode ? submissionId : requestID;
                    if (workflowId > 0)
                    {
                        uploadFiles(updateMode, saveForLater, workflowId);
                        addHangingFiles(workflowId, false);
                    }
                    //if in updateMode and not allupdated and not in savelater mode reverse status of request
                    if (!allupdated && !saveForLater && updateMode)
                    {
                        newRequest.STATUS = Util.getTypeDefinitionsWorkFlowSaveForLaterId();
                        scoresManager.updateWorkFlow(newRequest);
                    }

                    pnlSubmitted.Visible = true;
                    pnlScores.Visible = false;
                    MainView.SetActiveView(vwForm);
                    if (!saveForLater
                        && newRequest.STATUS == Util.getTypeDefinitionsWorkFlowNewRequestId()
                        && allupdated) sendMail(approver);

                    ShowMessage(LblOutCome, allupdated && saveForLater ?
                        string.Format("Scores have been successfully saved for later. {0}", changetosavelater ? setarea : string.Empty) :
                        (allupdated && !saveForLater) ? "Scores submitted succesfully, email has been sent to your manager." :
                        "Some of your scores could not be saved.");
                    InitialiseObjects();
                }
            }
            catch (Exception e)
            {
                if (!updateMode && requestID > 0)
                    scoresManager.deleteWorkFlow(requestID);
                ShowMessage(lblMsg, Messages.GetSaveFailedMessage());
                Util.LogErrors(e);
            }
        }

        void addHangingFiles(int submissionId, bool reloadData)
        {
            try
            {
                if (Session["Files"] != null)
                {
                    List<attachmentDocuments> hangingData = Session["Files"] as List<attachmentDocuments>;
                    foreach (attachmentDocuments doc in hangingData)
                    {
                        doc.SUBMISSION_ID = submissionId;
                        scoresManager.createAttachment(doc);
                    }
                }
                if (reloadData) getAttachments(submissionId);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void uploadFiles(bool updateMode, bool validData, int submissionId)
        {
            string fullPath = string.Empty;
            try
            {

                List<attachmentDocuments> hangingData = Session["Files"] as List<attachmentDocuments>;
                foreach (UploadedFile myFile in RdAsyncUpd.UploadedFiles)
                {
                    string tmpName = myFile.GetName();
                    string fName = tmpName;
                    fullPath = string.Format(@"{0}{1}", Server.MapPath(Util.getConfigurationSettingUploadFolder()), fName);

                    FileIOPermission permission = new FileIOPermission(FileIOPermissionAccess.Write, fullPath);
                    permission.Demand();
                    FileInfo fInfo = new FileInfo(fullPath);
                    if (!fInfo.Exists)
                        myFile.SaveAs(fullPath);
                    fInfo = null;
                    permission = null;

                    attachmentDocuments docs = new attachmentDocuments
                    {
                        SUBMISSION_ID = submissionId,
                        ATTACHMENT_NAME = fName,
                        ID = 0
                    };

                    if (submissionId > 0)
                    {
                        scoresManager.createAttachment(docs);
                    }
                    else if (!hangingData.Contains(docs))
                    {
                        hangingData.Add(docs);
                    }
                }
                Session["Files"] = hangingData;
            }
            catch (Exception e)
            {
                Trace.Write("Path " + fullPath + " -> " + e.StackTrace);
                Util.LogErrors(e);
            }
        }

        void createAttachements(int submissionId)
        {
            try
            {
                for (int i = 0; i < 2; i++)
                {
                    var attachmentsUpload = this.FindControl("ftpAttachDocuments_" + i) as FileUpload;
                    if (attachmentsUpload != null && attachmentsUpload.PostedFile.ContentLength > 0)
                    {
                        attachmentDocuments docs = new attachmentDocuments
                        {
                            SUBMISSION_ID = submissionId,
                            ATTACHMENT_NAME = attachmentsUpload.PostedFile.FileName
                        };
                        attachmentsUpload.SaveAs(Server.MapPath(Util.getConfigurationSettingUploadFolder()) + attachmentsUpload.PostedFile.FileName);
                        scoresManager.createAttachment(docs);
                    }
                }
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void sendMail(entities.useradmin user)
        {
            try
            {
                string result = string.Empty;
                int emailId = Util.getWorkflowApprovalRequestEmailId();

                MailAddressCollection addy = new MailAddressCollection();
                addy.Add(new MailAddress(user.EMAIL_ADDRESS, user.NAME));

                IMailRelay objm = new MailRelayBL();
                MailRelay obj = new MailRelay
                {
                    MailId = emailId,
                    Recipient = addy,
                    Data = { }
                };
                objm.SendMail(obj, out result);
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        #endregion

        #region Dropdown Event Handling Methods

        protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlValueTypes.Items.Clear();
            pnlSubmitted.Visible = false;
            pnlScores.Visible = false;

            if (ddlArea.SelectedIndex == 0) return;
            else setCriteriaSelected();
        }

        protected void ddlValueTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlSubmitted.Visible = false;
            InitialiseObjects();
            bindAttachments(new List<attachmentDocuments>());
            if (ddlValueTypes.SelectedIndex == 0)
            {
                pnlScores.Visible = false;
                return;
            }
            else
            {
                pnlScores.Visible = true;
                loadGoals(true);
            }
        }

        #endregion

        #region Button Event Handling Methods

        protected void btnBack_Click(object sender, EventArgs e)
        {
            pnlSubmitted.Visible = false;
            LoadSubmissions();
            UpdatePanelCapture.Update();
        }

        protected void lnkSubmitNew_Click(object sender, EventArgs e)
        {
            EnableControl(true);
            InitialiseObjects();
            initialiseForm();
            ResetSelectedIndexes();
            pnlSubmitted.Visible = false;
            pnlScores.Visible = false;
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            calculateValues(false);
        }

        protected void btnSaveLater_Click(object sender, EventArgs e)
        {
            calculateValues(true);
        }

        void ResetSelectedIndexes()
        {
            ddlArea.SelectedIndex = 0;
            ddlValueTypes.SelectedIndex = 0;
            txtDate.Text = string.Empty;
            ClearLabels();
        }

        void EnableControl(bool enable)
        {
            ddlArea.Enabled = enable;
            ddlValueTypes.Enabled = enable;
            //txtDate.Enabled = enable;
        }

        protected void btnNewScore_Click(object sender, EventArgs e)
        {
            hdnUpdate.Value = "0";
            InitialiseObjects();
            initialiseForm();
            EnableControl(true);
        }

        #endregion

        #region ListView Event Handling Methods

        protected void rptMeasureData_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.AlternatingItem ||
                    e.Item.ItemType == ListItemType.EditItem ||
                    e.Item.ItemType == ListItemType.Item)
                {
                    HiddenField hdnUnitMeasureId = e.Item.FindControl("hdnUnitMeasureId") as HiddenField;
                    TextBox txtScore = e.Item.FindControl("txtScore") as TextBox;

                    if (hdnUnitMeasureId != null && txtScore != null)
                    {
                        if (!string.IsNullOrEmpty(hdnUnitMeasureId.Value))
                        {
                            int unitMeasure = int.Parse(hdnUnitMeasureId.Value);
                            if (bR.UnitOfMeasureRequireNumericValue(unitMeasure))
                            {
                                txtScore.Attributes.Add("onkeydown", "return jsDecimals(event);");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        protected void lvGoals_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListViewItemType.DataItem)
                {
                    int parentId = int.Parse((e.Item.FindControl("lblID") as Label).Text);
                    bool hdnHasSubGoals = bool.Parse((e.Item.FindControl("hdnHasSubGoals") as HiddenField).Value);

                    Label lblUnitOfMeasure = e.Item.FindControl("lblUnitMeasureID") as Label;
                    Repeater rptData = e.Item.FindControl("rptMeasureData") as Repeater;
                    TextBox txtScore = e.Item.FindControl("txtScore") as TextBox;

                    if (rptData != null && hdnHasSubGoals)
                    {
                        var data = GetChildrenData(Cache["Data"] as DataTable, parentId);
                        BindControl.BindRepeater(rptData, data);
                        rptData.Visible = (data.Rows.Count <= 0) ? false : true;
                    }
                    if (txtScore != null && lblUnitOfMeasure != null)
                    {
                        if (!string.IsNullOrEmpty(lblUnitOfMeasure.Text))
                        {
                            int unitMeasure = int.Parse(lblUnitOfMeasure.Text);
                            if (bR.UnitOfMeasureRequireNumericValue(unitMeasure))
                            {
                                txtScore.Attributes.Add("onkeydown", "return jsDecimals(event);");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        protected void lvSubmittedWorkflows_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                string commandName = e.CommandName.ToLower();
                var lblSubmittedId = e.Item.FindControl("lblID") as Label;
                var submitId = int.Parse(lblSubmittedId.Text);
                switch (commandName)
                {
                    case "view":
                        var lblItemSelected = e.Item.FindControl("lblItemSelected") as Label;
                        var lblCriteria = e.Item.FindControl("lblCriteria") as Label;
                        var hdnPrevStatusId = e.Item.FindControl("hdnPrevStatusId") as HiddenField;
                        var hdnWasApproved = e.Item.FindControl("hdnOnceApproved") as HiddenField;

                        hdnUpdate.Value = "1";
                        hdnAreaId.Value = lblCriteria.Text.Trim();
                        hdnValueId.Value = lblItemSelected.Text.Trim();
                        hdnSubmissionId.Value = lblSubmittedId.Text;
                        hdnWasOnceApproved.Value = hdnWasApproved.Value;
                        hdnStatusId.Value = hdnPrevStatusId.Value;

                        InitialiseObjects();
                        getGoalsCriteriaTypes();
                        if (ddlArea.Items.FindByValue(hdnAreaId.Value) != null)
                            ddlArea.SelectedValue = hdnAreaId.Value;

                        setCriteriaSelected();
                        if (ddlValueTypes.Items.FindByValue(hdnValueId.Value) != null)
                            ddlValueTypes.SelectedValue = hdnValueId.Value;

                        int valueId = int.Parse(hdnValueId.Value);
                        int areaId = int.Parse(hdnAreaId.Value);

                        loadGoals(true, valueId, areaId, submitId);
                        EnableControl(false);
                        MainView.SetActiveView(vwForm);
                        pnlScores.Visible = true;
                        pnlSubmitted.Visible = false;
                        break;
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        protected void lvSubmittedWorkflows_ItemDeleting(object sender, ListViewDeleteEventArgs e)
        {
            try
            {
                var lblSubmittedId = lvSubmittedWorkflows.Items[e.ItemIndex].FindControl("lblID") as Label;
                var submitId = int.Parse(lblSubmittedId.Text);
                DeleteCapturedScore(submitId);
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void DeleteCapturedScore(int submitId)
        {
            try
            {
                var result = scoresManager.deleteWorkFlow(submitId);
                if (result > 0)
                {
                    LoadSubmissions();
                    UpdatePanelCapture.Update();
                }
                BindControl.BindLiteral(LblNoData, result > 0 ? Messages.GetDeleteMessage() : Messages.GetDeleteFailedMessage());
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                BindControl.BindLiteral(LblNoData, Messages.GetErrorMessage());
            }
        }

        void bindAttachments(List<attachmentDocuments> docs)
        {
            BindControl.BindRepeater(rptFile, docs);
            rptFile.Visible = docs.Count > 0 ? true : false;
        }

        void getAttachments(int submittedId)
        {
            try
            {
                var docs = scoresManager.getAllAttachments(new submissionWorkflow
                {
                    ID = submittedId
                });
                bindAttachments(docs);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        public string getfilePath(object fileName)
        {
            return string.Format("{0}{1}", Util.getConfigurationSettingServerAttachmentsUrl(), fileName);
        }

        #endregion

        #region DataList Event Handling Methods

        protected void rptFile_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.ToLower().Equals("Delete".ToLower()))
                {
                    if (vb.Information.IsNumeric(e.CommandArgument))
                    {
                        int submissionId = int.Parse(hdnSubmissionId.Value);
                        int attachmentId = int.Parse(e.CommandArgument.ToString());
                        int result = scoresManager.deleteAttachment(attachmentId);
                        LblDelete.Text = (result > 0) ? Messages.GetDeleteMessage() : Messages.GetDeleteFailedMessage();
                        if (result > 0) getAttachments(submissionId);
                    }
                }
                else LblDelete.Text = Messages.GetDeleteFailedMessage();
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        #endregion
    }
}