﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HR.Incentives.Interfaces;
using HR.Incentives.Logic;
using scorecard.implementations;
using scorecard.interfaces;

namespace scorecard.ui.reports
{
    public partial class incentiveSalaryvsRatingForm : System.Web.UI.UserControl
    {
        #region  Variables

        ISalaryType sal;
        IIncentiveReport rep;
        Iorgunit org;
       
        #endregion

        #region Default Class Constructors

        public incentiveSalaryvsRatingForm()
        {
            sal = new SalaryTypeBL();
            rep = new IncentiveReportBL();
            org = new structureImpl();
        }

        #endregion

        #region Page Events Handling Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindOrgUnits();
                BindSalaryTypes();
            }
        }

        #endregion

        #region Databinding Methods

        void BindOrgUnits()
        {
            int orgUnitId = Util.user.OrgId;
            var data = org.getOrgStructureInHirachy(orgUnitId);

            bool isAdmin = Util.user.UserTypeId == Util.getTypeDefinitionsUserTypeAdministrator();
            string selectedVal = isAdmin ? string.Empty : orgUnitId.ToString();

            BindControl.BindDropdown(ddOrgUnit, "ORGUNIT_NAME", "ID", "- select all -", "-1", selectedVal, data);
            ddOrgUnit.Enabled = isAdmin;
        }

        void BindSalaryTypes()
        {
            int compId = Util.user.CompanyId;
            var data = sal.GetByStatus(compId, true);
            BindControl.BindDropdown(ddSalyType, data, "- select salary type -", "-1");
        }

        void ShowMessage(string msg)
        {
            BindControl.BindLiteral(lblMsg, msg);
        }

        bool IsValid()
        {
            bool isValid = false;
            try
            {
                if (string.IsNullOrEmpty(txtEndDate.Text) || string.IsNullOrEmpty(txtStartDate.Text))
                {
                    isValid = false;
                    ShowMessage(Messages.GetRequiredFieldsMessage(true, string.IsNullOrEmpty(txtStartDate.Text) ? "Start Date" : string.Empty,
                       string.IsNullOrEmpty(txtEndDate.Text) ? "End Date" : string.Empty, ddSalyType.SelectedIndex == 0 ? "Select Salary Type" : string.Empty));
                }
                else
                {
                    if (!string.IsNullOrEmpty(txtEndDate.Text) && !string.IsNullOrEmpty(txtStartDate.Text))
                    {
                        string tempStartDate = Common.GetInternationalDateFormat(txtStartDate.Text, DateFormat.monthDayYear);
                        string tempEndDate = Common.GetInternationalDateFormat(txtEndDate.Text, DateFormat.monthDayYear);
                        if (DateTime.Parse(tempStartDate) > DateTime.Parse(tempEndDate))
                        {
                            ShowMessage("Start Date cannot be greater than End Date.");
                            isValid = false;
                        }
                        else
                        {
                            isValid = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessage(Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }

            return isValid;
        }

        void DownloadReport()
        {
            string startDate = Common.GetInternationalDateFormat(txtStartDate.Text.Trim(), DateFormat.monthDayYear);
            string endDate = Common.GetInternationalDateFormat(txtEndDate.Text.Trim(), DateFormat.monthDayYear);

            string orgUnitId = ddOrgUnit.SelectedValue;
            string orgUnitName = (ddOrgUnit.SelectedIndex == 0) ? "All" : HttpUtility.UrlEncode(ddOrgUnit.SelectedItem.Text);

            string salaryTypeId = ddSalyType.SelectedValue;
            string salaryTypeName = HttpUtility.UrlEncode(ddSalyType.SelectedItem.Text);

            string script = string.Format("openNewWindow('salaryvsratingpreview.aspx?sid={0}&dsVal={1}&deVal={2}&orgunitId={3}&name={4}&orgunit={5}');", salaryTypeId, startDate, endDate, orgUnitId, salaryTypeName, orgUnitName);
            AjaxScriptManager.RegisterStartupScript(Page, typeof(Page), "viewReport", script, true);
        }

        #endregion

        #region Button Event Handling Methods

        protected void btnViewReport_Click(object sender, EventArgs e)
        {
            bool valid = IsValid();
            if (valid)
            {
                DownloadReport();
            }
        }

        #endregion
    }
}