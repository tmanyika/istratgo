﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.implementations;
using scorecard.controllers;
using scorecard.entities;
using Telerik.Web.UI;
using System.IO;

namespace scorecard.ui.controls
{
    public partial class jobtitles : System.Web.UI.UserControl
    {
        jobtitlesdefinitions impl;

        public jobtitles()
        {
            impl = new jobtitlesdefinitions(new jobtitleImpl());
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadJobTitlesByCompany();
            }
        }

        public bool GetVisibility(object fileName)
        {
            bool isVisible = false;
            if (fileName == DBNull.Value)
            {
                return isVisible;
            }

            if (string.IsNullOrEmpty(fileName.ToString()))
            {
                return isVisible;
            }

            return true;
        }

        public string GetFileUrl(object fileName)
        {
            string nolink = "#";
            if (fileName == DBNull.Value) return nolink;
            if (string.IsNullOrEmpty(fileName.ToString())) return nolink;
            var companyId = Util.user.CompanyId;
            return Util.GetJobContractVirtualPath(companyId, fileName.ToString());
        }

        void loadJobTitlesByCompany()
        {
            try
            {
                var obj = new company { COMPANY_ID = Util.user.CompanyId };
                var jobtitles = impl.getAllCompanyJobTitles(obj).ToList();

                if (jobtitles.Count <= 0)
                {
                    lvJobTitles.InsertItemPosition = InsertItemPosition.LastItem;
                }

                lvJobTitles.DataSource = jobtitles;
                lvJobTitles.DataBind();
                dtPagerJobTitles.Visible = (jobtitles.Count > dtPagerJobTitles.PageSize);
            }
            catch (Exception e)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(e);
            }
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            lvJobTitles.InsertItemPosition = InsertItemPosition.LastItem;
            loadJobTitlesByCompany();
        }

        void UploadFile(FileUpload radUpload, int companyId, int jobId, string name, out string contractFile)
        {
            contractFile = string.Empty;
            if (radUpload.HasFile)
            {
                string physicalfolder = Util.GetJobContractPhysicalPath(companyId);
                try
                {
                    if (!Directory.Exists(physicalfolder))
                    {
                        Directory.CreateDirectory(physicalfolder);
                    }
                }
                catch (Exception ex)
                {
                    BindControl.BindLiteral(lblMsg, "The system failed to locate the upload folder");
                    Util.LogErrors(ex);
                    return;
                }

                string fileExtension = Path.GetExtension(radUpload.PostedFile.FileName);
                string newFileName = string.Format("{0}{1}{2}", name.Replace(" ", "").Replace(",", ""), jobId, fileExtension);
                string fullPathNewFileName = string.Format(@"{0}\{1}", physicalfolder, newFileName);

                radUpload.PostedFile.SaveAs(fullPathNewFileName);
                contractFile = newFileName;
            }
        }

        void createNewJobTitle(ListViewCommandEventArgs e)
        {
            try
            {
                var txtName = e.Item.FindControl("txtName") as TextBox;
                var radUpload = e.Item.FindControl("FileUpload1") as FileUpload;
                var companyId = Util.user.CompanyId;
                var name = txtName.Text.Trim();
                var active = true;
                var createdBy = Util.user.LoginId;

                string contractFile;
                UploadFile(radUpload, companyId, 0, name, out contractFile);

                impl.addJobTitle(new JobTitle
                {
                    COMPANY_ID = companyId,
                    NAME = name,
                    CONTRACT_FILE = contractFile,
                    ACTIVE = active,
                    CREATEDBY = createdBy
                });
                lvJobTitles.InsertItemPosition = InsertItemPosition.None;
                loadJobTitlesByCompany();
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void updateJobtitle(ListViewCommandEventArgs e)
        {
            try
            {
                var txtName = e.Item.FindControl("txtName") as TextBox;
                var lblTitleID = e.Item.FindControl("lblID") as Label;
                var radUpload = e.Item.FindControl("FileUpload1") as FileUpload;

                var companyId = Util.user.CompanyId;
                var name = txtName.Text.Trim();
                var active = true;
                var createdBy = Util.user.LoginId;
                var jobTitleId = int.Parse(lblTitleID.Text);
                var contractFile = string.Empty;

                UploadFile(radUpload, companyId, jobTitleId, name, out contractFile);

                impl.updateJobTitle(new JobTitle
                {
                    ID = jobTitleId,
                    COMPANY_ID = companyId,
                    NAME = name,
                    CONTRACT_FILE = contractFile,
                    ACTIVE = active,
                    CREATEDBY = createdBy
                });

                lvJobTitles.InsertItemPosition = InsertItemPosition.None;
                lvJobTitles.EditIndex = -1;
                loadJobTitlesByCompany();
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        void deleteJobtitle(ListViewCommandEventArgs e)
        {
            try
            {
                var lblTitleID = e.Item.FindControl("lblID") as Label;
                impl.deleteJobTitle(new JobTitle
                {
                    ID = int.Parse(lblTitleID.Text)
                });
                loadJobTitlesByCompany();
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        void deleteJobtitleContract(ListViewCommandEventArgs e)
        {
            try
            {
                var lblTitleID = e.Item.FindControl("lblID") as Label;
                var id = int.Parse(lblTitleID.Text);

                impl.deleteContract(id);
                loadJobTitlesByCompany();
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        void cancelUpdate()
        {

            lvJobTitles.EditIndex = -1;
            lvJobTitles.InsertItemPosition = InsertItemPosition.None;
            loadJobTitlesByCompany();
        }

        protected void lvJobTitles_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.InsertItem || e.Item.ItemType == ListViewItemType.DataItem)
            {

                switch (e.CommandName)
                {
                    case "addNewJobTitle":
                        createNewJobTitle(e);
                        break;
                    case "updateJobTitle":
                        updateJobtitle(e);
                        break;
                    case "deleteJobTitle":
                        deleteJobtitle(e);
                        break;
                    case "deleteContract":
                        deleteJobtitleContract(e);
                        break;
                    case "cancelUpdate":
                        cancelUpdate();
                        break;
                }
            }
        }

        protected void lvJobTitle_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            lvJobTitles.EditIndex = e.NewEditIndex;
            lvJobTitles.InsertItemPosition = InsertItemPosition.None;
            loadJobTitlesByCompany();
        }

        protected void lvJobTitles_PagePropertiesChanged(object sender, EventArgs e)
        {
            loadJobTitlesByCompany();
        }

        protected void lvJobTitles_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListViewItemType.DataItem ||
                    e.Item.ItemType == ListViewItemType.InsertItem)
                {
                    //RadAsyncUpload radUpload = e.Item.FindControl("AsyncFileContractUpload") as RadAsyncUpload;
                    //if (radUpload != null)
                    //{
                    //    int companyId = Util.user.CompanyId;
                    //    string physicalfolder = Util.GetJobContractPhysicalPath(companyId);
                    //    try
                    //    {
                    //        if (!Directory.Exists(physicalfolder))
                    //        {
                    //            Directory.CreateDirectory(physicalfolder);
                    //        }
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        BindControl.BindLiteral(lblMsg, "The system failed to locate the upload folder");
                    //        Util.LogErrors(ex);
                    //    }
                    //    radUpload.TargetFolder = physicalfolder;
                    //    radUpload.TemporaryFolder = physicalfolder;
                    //}

                    if (e.Item.ItemType == ListViewItemType.DataItem)
                    {
                        LinkButton lnkRemove = e.Item.FindControl("lnkRemove") as LinkButton;
                        HiddenField hdnFile = e.Item.FindControl("hdnFile") as HiddenField;
                        if (hdnFile != null && lnkRemove != null)
                        {
                            lnkRemove.Visible = string.IsNullOrEmpty(hdnFile.Value) ? false : true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        protected void lnkCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("index.aspx");
        }
    }
}