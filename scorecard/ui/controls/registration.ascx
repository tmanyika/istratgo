﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="registration.ascx.cs"
    Inherits="scorecard.ui.controls.registration" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="CustomCaptcha" Namespace="CustomCaptcha" TagPrefix="cc1" %>
<%@ Register Assembly="CheckboxValidator" Namespace="TensaiLabs.Web.Controls" TagPrefix="cc2" %>
<h1>
    User Registration
</h1>
<!-- Fieldset -->
<fieldset runat="server" id="pnlContactInfo">
    <legend>Customer Information </legend>
    <p>
        <label for="sf">
            User Name:
        </label>
        <span class="field_desc">
            <input class="mf" name="mf0" type="text" runat="server" id="txtUserName" /></span>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtUserName"
            Display="None" ErrorMessage="User Name"></asp:RequiredFieldValidator>
    </p>
    <p>
        <label for="sf">
            Password:
        </label>
        <span class="field_desc">
            <input class="mf" name="mf0" type="password" runat="server" id="txtPassword" /></span>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtPassword"
            Display="None" ErrorMessage="Password"></asp:RequiredFieldValidator>
    </p>
    <p>
        <label for="sf">
            Confirm Password:
        </label>
        <span class="field_desc">
            <input class="mf" name="mf0" type="password" runat="server" id="txtConfirmPassword" /></span>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtConfirmPassword"
            Display="None" ErrorMessage="Confirm Password "></asp:RequiredFieldValidator>
        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtConfirmPassword"
            ControlToValidate="txtPassword" Display="None" ErrorMessage="Passwords do not match"></asp:CompareValidator>
    </p>
    <p>
        <label for="mf">
            First Name:
        </label>
        <input class="mf" name="mf" type="text" runat="server" id="txtFirstName" />
        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtFirstName"
            Display="None" ErrorMessage="First Name"></asp:RequiredFieldValidator>
    </p>
    <p>
        <label for="lf">
            Last Name :
        </label>
        <input class="mf" name="lf1" type="text" runat="server" id="txtLastName" />
        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtLastName"
            Display="None" ErrorMessage="Last Name"></asp:RequiredFieldValidator>
    </p>
    <p>
        <label for="lf">
            Email :
        </label>
        <input class="mf" name="lf1" type="text" runat="server" id="txtEmailAddress" />
        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtEmailAddress"
            Display="None" ErrorMessage="Email Address"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmailAddress"
            Display="None" ErrorMessage="Valid Email Address" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
    </p>
    <p>
        <label for="lf">
            Company Name :
        </label>
        <input class="mf" name="lf1" type="text" runat="server" id="txtCompany" />
        <asp:RequiredFieldValidator ID="rqdCompany" runat="server" ControlToValidate="txtCompany"
            Display="None" ErrorMessage="Company Name"> </asp:RequiredFieldValidator>
    </p>
    <p>
        <label for="lf">
            Contact Telephone :
        </label>
        <input class="mf" name="lf0" type="text" runat="server" id="txtTelephone" />
    </p>
    <p>
        <label for="dropdown">
            Country :
        </label>
        <asp:DropDownList name="dropdown" class="dropdown" runat="server" ID="ddCountry" Width="250px"
            DataTextField="NAME" DataValueField="TYPE_ID" />
    </p>
    <p>
        <div class="floatLeft">
            <label for="dropdown">
                Prove you are not a robot :
            </label>
        </div>
        <div class="floatLeft">
            <telerik:RadCaptcha ID="RegRadCaptcha" runat="server" 
                ErrorMessage="The code you entered is not valid." 
                CaptchaLinkButtonText="Generate new code" CaptchaTextBoxLabel="" 
                Display="Dynamic" EnableRefreshImage="True" ForeColor="Red" Width="250px">
            </telerik:RadCaptcha>
        </div>
        <div class="clr">
            &nbsp;
        </div>
    </p>
    <p>
        <label for="chkTerms">
            I agree to the
            <br />
            <br />
            <a href='http://www.imdconsulting.co.za/terms/iStratgoBalancedScorecardTermsandConditions.pdf'
                target='_blank'>Terms and Conditions</a>
        </label>
        <asp:CheckBox ID="chkTerms" runat="server" TextAlign="Right" Text="" />
        <cc2:CheckBoxValidator ID="vldChk" runat="server" ControlToValidate="chkTerms" Display="None"
            ErrorMessage="Agree to Terms &amp; Conditions">
        </cc2:CheckBoxValidator>
    </p>
    <p class="errorMsg">
        <label for="lblError">
            &nbsp;
        </label>
        <br />
        <asp:Literal ID="lblError" runat="server"></asp:Literal><br />
    </p>
    <p>
        <br />
    </p>
    <p>
        <label>
            &nbsp;
        </label>
        <asp:Button ID="btnNext" runat="server" class="button" Text="Submit" OnClick="btnComplete_Click" />
        <asp:Button ID="btnCancel" runat="server" class="button" Text="Cancel" CausesValidation="False"
            PostBackUrl="~/ui/login.aspx" />
    </p>
    <p>
        <asp:ValidationSummary ID="vdSumarry" runat="server" HeaderText="Please provide the following fields:"
            ShowMessageBox="True" ShowSummary="False" />
    </p>
</fieldset>
