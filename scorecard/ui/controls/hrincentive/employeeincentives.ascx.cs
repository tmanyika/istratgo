﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HR.Human.Resources;
using scorecard.implementations;

namespace scorecard.ui.controls.hrincentive
{
    public partial class employyeincentives : System.Web.UI.UserControl
    {
        #region Member Variables

        #endregion

        #region Properties

        bool IsAdministrator
        {
            get
            {
                int? roleId = null;
                if (Util.user != null)
                {
                    roleId = Util.user.UserTypeId;
                }
                return (roleId == Util.getTypeDefinitionsUserTypeAdministrator());
            }
        }

        #endregion

        #region Default Constructors

        public employyeincentives()
        {

        }

        #endregion

        #region Page Event Handling Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillEmployees();
            }
        }

        #endregion

        #region Databinding Methods

        public string StrConvToUpper(object fullname)
        {
            return fullname.ToString().ToUpper();
        }

        public string GetUrl(object employeeId, object fullName)
        {
            //return string.Format("empincentiveivemgt.aspx?Id={0}&FullName={1}", employeeId, HttpUtility.HtmlEncode(fullName));
            return string.Format("empincentivesmgt.aspx?Id={0}&FullName={1}", employeeId, HttpUtility.HtmlEncode(fullName));
        }

        void FillEmployees()
        {
            try
            {
                IEmployee obj = new EmployeeBL();
                List<Employee> data = IsAdministrator ? obj.GetByOrgStructure(Util.user.OrgId, true) :
                                                        obj.GetAll(Util.user.OrgId);
                BindControl.BindListView(lvData, data);
                PagerData.Visible = (data.Count > PagerData.PageSize);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        #endregion

        #region Utility Methods

        void ShowMessage(Literal lbl, string msg)
        {
            lbl.Text = msg;
        }

        #endregion

        #region ListView Event Handling Methods

        protected void lvData_PagePropertiesChanged(object sender, EventArgs e)
        {
            FillEmployees();
        }

        #endregion
    }
}