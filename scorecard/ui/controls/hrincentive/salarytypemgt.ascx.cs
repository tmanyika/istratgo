﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HR.Incentives.Interfaces;
using HR.Incentives.Logic;
using scorecard.implementations;
using HR.Incentives.Entities;

namespace scorecard.ui.controls.hrincentive
{
    public partial class salarytypemgt : System.Web.UI.UserControl
    {
        ISalaryType sal;

        public salarytypemgt()
        {
            sal = new SalaryTypeBL();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ResetLabels();
            if (!IsPostBack)
            {
                LoadSalaryTypes();
            }
        }

        private void ResetLabels()
        {
            lblMsg.Text = "";
            LblError.Text = "";
        }

        void LoadSalaryTypes()
        {
            try
            {
                int compId = Util.user.CompanyId;
                var data = sal.GetByStatus(compId, true);
                dtPagerlvData.Visible = (data.Count > dtPagerlvData.PageSize);
                BindControl.BindListView(lvData, data);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            lvData.InsertItemPosition = InsertItemPosition.LastItem;
            LoadSalaryTypes();
        }

        void AddNewSalaryType(ListViewCommandEventArgs e)
        {
            try
            {
                string description = (e.Item.FindControl("txtDescription") as TextBox).Text;
                string name = (e.Item.FindControl("txtName") as TextBox).Text;
                string username = Util.user.LoginId;

                int companyId = Util.user.CompanyId;
                if (string.IsNullOrEmpty(name))
                {
                    BindControl.BindLiteral(LblError, Messages.GetRequiredFieldsMessage(true, "Name"));
                    return;
                }

                bool added = sal.Add(new SalaryType
                {
                    Active = true,
                    CompanyId = companyId,
                    SalaryTypeName = name,
                    SalaryTypeDescription = description,
                    CreatedBy = Util.user.LoginId
                });

                if (added)
                {
                    loadReadOnly();
                    LoadSalaryTypes();
                }
                BindControl.BindLiteral(LblError, added ? Messages.GetSaveMessage() : Messages.GetAlreadyExistMessage());
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void loadReadOnly()
        {
            lvData.EditIndex = -1;
            lvData.InsertItemPosition = InsertItemPosition.None;
        }

        void UpdateSalaryType(ListViewDataItem e)
        {
            try
            {
                var hdnSalaryTypeId = e.FindControl("hdnSalaryTypeId") as HiddenField;
                var hdnCompanyId = e.FindControl("hdnCompanyId") as HiddenField;
                var chkActive = e.FindControl("chkActive") as CheckBox;

                string description = (e.FindControl("txtEditDescription") as TextBox).Text;
                string name = (e.FindControl("txtEditName") as TextBox).Text;

                bool active = chkActive.Checked;

                int companyId = int.Parse(hdnCompanyId.Value);
                int salaryTypeId = int.Parse(hdnSalaryTypeId.Value);

                if (string.IsNullOrEmpty(name))
                {
                    BindControl.BindLiteral(LblError, Messages.GetRequiredFieldsMessage(true, "Name", "Alias"));
                    return;
                }

                bool updated = sal.Update(new SalaryType
                {
                    Active = true,
                    CompanyId = companyId,
                    SalaryTypeName = name,
                    SalaryTypeDescription = description,
                    SalaryTypeId = salaryTypeId,
                    UpdatedBy = Util.user.LoginId
                });

                if (updated)
                {
                    loadReadOnly();
                    LoadSalaryTypes();
                }
                BindControl.BindLiteral(LblError, updated ? Messages.GetUpdateMessage() : Messages.GetAlreadyExistMessage());
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void RemoveSalaryType(ListViewDataItem e)
        {
            try
            {
                var hdnSalaryTypeId = e.FindControl("hdnSalaryTypeId") as HiddenField;
                if (string.IsNullOrEmpty(hdnSalaryTypeId.Value))
                {
                    BindControl.BindLiteral(LblError, Messages.GetDeleteFailedMessage());
                    return;
                }

                int salaryTypeId = int.Parse(hdnSalaryTypeId.Value);
                bool deactivated = sal.SetStatus(new SalaryType
                {
                    Active = false,
                    SalaryTypeId = salaryTypeId,
                    UpdatedBy = Util.user.LoginId
                });

                if (deactivated)
                {
                    loadReadOnly();
                    LoadSalaryTypes();
                }
                BindControl.BindLiteral(LblError, deactivated ? Messages.GetDeleteMessage() : Messages.GetDeleteFailedMessage());
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void CancelUpdate()
        {
            loadReadOnly();
            LoadSalaryTypes();
        }

        protected void lvData_ItemCanceling(object sender, ListViewCancelEventArgs e)
        {
            CancelUpdate();
        }

        protected void lvData_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            LblError.Text = "";
            if (e.Item.ItemType == ListViewItemType.InsertItem ||
                e.Item.ItemType == ListViewItemType.DataItem)
            {
                switch (e.CommandName)
                {
                    case "add":
                        AddNewSalaryType(e);
                        break;
                }
            }
        }

        protected void lvData_ItemDeleting(object sender, ListViewDeleteEventArgs e)
        {
            ListViewDataItem el = lvData.Items[e.ItemIndex];
            RemoveSalaryType(el);
        }

        protected void lvData_ItemUpdating(object sender, ListViewUpdateEventArgs e)
        {
            ListViewDataItem el = lvData.Items[e.ItemIndex];
            UpdateSalaryType(el);
        }

        protected void lvData_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            lvData.EditIndex = e.NewEditIndex;
            lvData.InsertItemPosition = InsertItemPosition.None;
            LoadSalaryTypes();
        }

        protected void lvData_PagePropertiesChanged(object sender, EventArgs e)
        {
            LoadSalaryTypes();
        }

        protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadSalaryTypes();
        }

        protected void lnkCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("index.aspx", true);
        }
    }
}