﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HR.Incentives.Interfaces;
using scorecard.implementations;
using HR.Incentives.Logic;
using HR.Incentives.Entities;

namespace scorecard.ui.controls.hrincentive
{
    public partial class employeeincentivemgt : System.Web.UI.UserControl
    {
        IEmployeeSalary sal;

        int EmployeeId
        {
            get
            {
                return int.Parse(Request.QueryString["Id"].ToString());
            }
        }

        string EmployeeName
        {
            get
            {
                return HttpUtility.HtmlDecode(Request.QueryString["FullName"].ToString());
            }
        }

        public employeeincentivemgt()
        {
            sal = new EmployeeSalaryBL();
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            CheckSessionState();
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            ResetLabels();
            if (!IsPostBack)
            {
                lblName.Text = EmployeeName;
                LoadSalaryInfo();
            }
        }

        private void CheckSessionState()
        {
            if (Util.user == null)
            {
                BindControl.BindLiteral(LblError, Messages.SessionExpired);
                AjaxScriptManager.RegisterStartupScript(Page, typeof(Page), "closeWin", "closeHighSlideWindowAndRefresh();", true);
            }
        }

        void ResetLabels()
        {
            LblError.Text = "";
        }

        public string GetAmount(object active, object amount)
        {
            bool updated = bool.Parse(active.ToString());
            return (updated) ? amount.ToString() : string.Empty;
        }

        void LoadSalaryInfo()
        {
            try
            {
                int empId = EmployeeId;
                int compId = Util.user.CompanyId;
                var data = sal.GetByEmployeeId(compId, empId, true);
                BindControl.BindListView(lvData, data);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void LoadReadOnly()
        {
            lvData.EditIndex = -1;
            lvData.InsertItemPosition = InsertItemPosition.None;
        }

        void UpdateIncentive(ListViewDataItem e)
        {
            try
            {
                HiddenField hdnEmployeeSalaryId = e.FindControl("hdnEmployeeSalaryId") as HiddenField;
                HiddenField hdnSalaryTypeId = e.FindControl("hdnSalaryTypeId") as HiddenField;
                TextBox txtAmount = e.FindControl("txtAmount") as TextBox;

                if (string.IsNullOrEmpty(txtAmount.Text))
                {

                    BindControl.BindLiteral(LblError, Messages.GetRequiredFieldsMessage(true, "Amount"));
                    return;
                }

                decimal amount = decimal.Parse(txtAmount.Text);
                long employeeSalaryId = string.IsNullOrEmpty(hdnEmployeeSalaryId.Value) ? 0 : long.Parse(hdnEmployeeSalaryId.Value);

                int salaryTypeId = string.IsNullOrEmpty(hdnSalaryTypeId.Value) ? 0 : int.Parse(hdnSalaryTypeId.Value);
                int employeeId = EmployeeId;

                string userName = Util.user.LoginId;

                bool updated = false;
                bool active = true;

                var data = new EmployeeSalary
                {
                    Active = active,
                    CreatedBy = userName,
                    EmployeeId = employeeId,
                    EmployeeSalaryId = employeeSalaryId,
                    SalaryAmount = amount,
                    SalaryTypeId = salaryTypeId,
                    UpdatedBy = userName
                };

                if (employeeSalaryId > 0)
                {
                    updated = sal.UpdateSalaryInfo(data);
                }
                else if (employeeSalaryId == 0 && salaryTypeId > 0)
                {
                    updated = sal.AddSalaryInfo(data);
                }

                if (updated)
                {
                    LoadReadOnly();
                    LoadSalaryInfo();
                }

                BindControl.BindLiteral(LblError, updated ? Messages.GetUpdateMessage() : Messages.GetAlreadyExistMessage());
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void DeactivateIncentive(ListViewDataItem e)
        {
            try
            {
                HiddenField hdnEmployeeSalaryId = e.FindControl("hdnEmployeeSalaryId") as HiddenField;
                if (hdnEmployeeSalaryId != null)
                {
                    if (!string.IsNullOrEmpty(hdnEmployeeSalaryId.Value))
                    {
                        long employeeSalaryId = long.Parse(hdnEmployeeSalaryId.Value);
                        bool deleted = sal.DeleteSalaryInfo(employeeSalaryId);

                        if (deleted)
                        {
                            LoadSalaryInfo();
                        }
                        BindControl.BindLiteral(LblError, deleted ? Messages.GetDeleteMessage() : Messages.GetDeleteFailedMessage());
                    }
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void CancelUpdate()
        {
            LoadReadOnly();
            LoadSalaryInfo();
        }

        protected void lvDatas_ItemCanceling(object sender, ListViewCancelEventArgs e)
        {
            CancelUpdate();
        }

        protected void lvDatas_ItemDeleting(object sender, ListViewDeleteEventArgs e)
        {
            ListViewDataItem el = lvData.Items[e.ItemIndex];
            DeactivateIncentive(el);
        }

        protected void lvDatas_ItemUpdating(object sender, ListViewUpdateEventArgs e)
        {
            ListViewDataItem el = lvData.Items[e.ItemIndex];
            UpdateIncentive(el);
        }

        protected void lvData_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            lvData.EditIndex = e.NewEditIndex;
            lvData.InsertItemPosition = InsertItemPosition.None;
            LoadSalaryInfo();
        }

        protected void lvData_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                HiddenField hdnEmployeeSalaryId = e.Item.FindControl("hdnEmployeeSalaryId") as HiddenField;
                LinkButton lnkDelete = e.Item.FindControl("lnkDelete") as LinkButton;
                if (hdnEmployeeSalaryId != null && lnkDelete != null)
                {
                    if (!string.IsNullOrEmpty(hdnEmployeeSalaryId.Value))
                    {
                        lnkDelete.Visible = (hdnEmployeeSalaryId.Value.Equals("0")) ? false : true;
                    }
                    else
                    {
                        lnkDelete.Visible = false;
                    }
                }
            }
        }

        protected void lvData_PagePropertiesChanged(object sender, EventArgs e)
        {
            LoadSalaryInfo();
        }

    }
}