﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="employeeincentivemgt.ascx.cs"
    Inherits="scorecard.ui.controls.hrincentive.employeeincentivemgt" %>
<%@ Import Namespace="scorecard.implementations" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:UpdatePanel ID="UpdatePanelData" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <h1>
            <br />
            <asp:Literal ID="lblName" runat="server"></asp:Literal>
            Incentives
            <br />
        </h1>
        <asp:ListView ID="lvData" runat="server" OnItemEditing="lvData_ItemEditing" OnItemCanceling="lvDatas_ItemCanceling"
            OnItemDeleting="lvDatas_ItemDeleting" OnItemUpdating="lvDatas_ItemUpdating" OnItemDataBound="lvData_ItemDataBound">
            <LayoutTemplate>
                <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                    <thead>
                        <tr id="trFirst" runat="server">
                            <td style="width: 300px">
                                Salary Type
                            </td>
                            <td align="left" style="width: 420px">
                                Amount
                            </td>
                            <td style="width: 90px">
                                Value Updated
                            </td>
                            <td width="80px">
                                Edit
                            </td>
                            <td width="80px">
                                Delete
                            </td>
                        </tr>
                        <tr id="ItemPlaceHolder" runat="server">
                        </tr>
                    </thead>
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tbody>
                    <tr class="odd">
                        <td>
                            <%# Eval("Salary.SalaryTypeName")%>
                            <asp:HiddenField ID="hdnEmployeeSalaryId" runat="server" Value='<%# Bind("EmployeeSalaryId")%>' />
                            <asp:HiddenField ID="hdnSalaryTypeId" runat="server" Value='<%# Bind("SalaryTypeId")%>' />
                        </td>
                        <td>
                            <%# GetAmount(Eval("Active"),Common.FormatMoney(Eval("SalaryAmount")))%>
                        </td>
                        <td>
                            <%# Common.GetBooleanYesNo(Eval("Active")) %>
                        </td>
                        <td>
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkEdit" CommandName="edit" class="ui-state-default ui-corner-all"
                                    title="Edit Record"><span class="ui-icon ui-icon-pencil">
                        </span></asp:LinkButton>
                            </ul>
                        </td>
                        <td>
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkDelete" CommandName="delete" class="ui-state-default ui-corner-all"
                                    title="Delete Record" OnClientClick="return confirmDelete();"><span class="ui-icon ui-icon-trash">
                                    </span>
                                </asp:LinkButton>
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </ItemTemplate>
            <EditItemTemplate>
                <tbody>
                    <tr class="odd">
                        <td>
                            <%# Eval("Salary.SalaryTypeName")%>
                            <asp:HiddenField ID="hdnEmployeeSalaryId" runat="server" Value='<%# Bind("EmployeeSalaryId")%>' />
                            <asp:HiddenField ID="hdnSalaryTypeId" runat="server" Value='<%# Bind("SalaryTypeId")%>' />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtAmount" Text='<%# Eval("SalaryAmount")%>' Width="150px"
                                ValidationGroup="Edit" CssClass="mf" onkeydown="return jsDecimals(event);" />
                            <asp:RequiredFieldValidator ID="rqdMin" runat="server" ErrorMessage="*" ValidationGroup="Edit"
                                ControlToValidate="txtAmount" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="cvdMinV" runat="server" ErrorMessage="only numeric value is allowed without spaces or commas"
                                ControlToValidate="txtAmount" ValidationGroup="Edit" Display="Dynamic" Operator="DataTypeCheck"
                                Type="Double"></asp:CompareValidator>
                        </td>
                        <td>
                            <%# Common.GetBooleanYesNo(Eval("Active")) %>
                        </td>
                        <td>
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkSave" CommandName="update" class="ui-state-default ui-corner-all"
                                    title="Update Record" ValidationGroup="Edit"><span class="ui-icon ui-icon-disk">
                                     </span>
                                </asp:LinkButton>
                            </ul>
                        </td>
                        <td>
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkCancelUpdate" CommandName="cancel" class="ui-state-default ui-corner-all"
                                    title="Cancel Update" CausesValidation="false"><span class="ui-icon ui-icon-cancel">
                                    </span>
                                </asp:LinkButton>
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </EditItemTemplate>
            <EmptyDataTemplate>
                <span style="color: Red">There are no records to display.</span>
            </EmptyDataTemplate>
        </asp:ListView>
        <br />
        <div>
            <span style="color: Red">
                <asp:Literal ID="LblError" runat="server" /></span>
        </div>
        <asp:UpdateProgress ID="UpdateProgressComp" runat="server" AssociatedUpdatePanelID="UpdatePanelData">
            <ProgressTemplate>
                <div>
                    <asp:Image ID="imgOrg" runat="server" ImageUrl="~/ui/images/icons/activity.gif" AlternateText="Please wait..">
                    </asp:Image></div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
