﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HR.Incentives.Interfaces;
using HR.Incentives.Logic;
using ScoreCard.Common.Interfaces;
using ScoreCard.Common.Logic;
using scorecard.implementations;
using HR.Incentives.Entities;

namespace scorecard.ui.controls.hrincentive
{
    public partial class incentivesmgt : System.Web.UI.UserControl
    {
        IIncentive inc;
        IPeriod prd;

        int orgUnitId;
        int compId;

        public incentivesmgt()
        {
            inc = new IncentiveBL();
            prd = new PeriodBL();

            if (Util.user != null)
            {
                orgUnitId = Util.user.OrgId;
                compId = Util.user.CompanyId;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ResetLabels();
            if (!IsPostBack)
            {
                LoadIncentives();
            }
        }

        void ResetLabels()
        {
            lblMsg.Text = "";
            LblError.Text = "";
        }

        void LoadPeriods(DropDownList drpdwn, string selectedVal)
        {
            var data = prd.GetByStatus(true);
            BindControl.BindDropdown(drpdwn, "PeriodName", "PeriodId", selectedVal, data);
        }

        void LoadIncentives()
        {
            try
            {
                var data = inc.GetByStatus(compId, orgUnitId, true);
                dtPagerlvDatas.Visible = (data.Count > dtPagerlvDatas.PageSize);
                BindControl.BindListView(lvData, data);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            lvData.InsertItemPosition = InsertItemPosition.LastItem;
            LoadIncentives();

            var ddlPeriods = lvData.InsertItem.FindControl("ddlPeriod") as DropDownList;
            LoadPeriods(ddlPeriods, string.Empty);
        }

        void CreateNewIncentive(ListViewCommandEventArgs e)
        {
            try
            {
                string description = (e.Item.FindControl("txtDescription") as TextBox).Text;
                string name = (e.Item.FindControl("txtName") as TextBox).Text;
                var ddlPeriod = e.Item.FindControl("ddlPeriod") as DropDownList;

                if (string.IsNullOrEmpty(name))
                {
                    BindControl.BindLiteral(LblError, Messages.GetRequiredFieldsMessage(true, "Name", "Frequency of Payment"));
                    return;
                }

                int periodId = int.Parse(ddlPeriod.SelectedValue);
                bool added = inc.Add(new Incentive
                {
                    Active = true,
                    CreatedBy = Util.user.LoginId,
                    CompanyId = compId,
                    IncentiveDescription = description,
                    IncentiveName = name,
                    PeriodId = periodId
                });

                if (added)
                {
                    LoadReadOnly();
                    LoadIncentives();
                }
                BindControl.BindLiteral(LblError, added ? Messages.GetSaveMessage() : Messages.GetAlreadyExistMessage());
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void LoadReadOnly()
        {
            lvData.EditIndex = -1;
            lvData.InsertItemPosition = InsertItemPosition.None;
        }

        void UpdateIncentive(ListViewDataItem e)
        {
            try
            {
                string description = (e.FindControl("txtEditDescription") as TextBox).Text;
                string name = (e.FindControl("txtEditName") as TextBox).Text;

                var hdnIncentiveId = e.FindControl("hdnIncentiveId") as HiddenField;
                var ddlPeriod = e.FindControl("ddlPeriod") as DropDownList;

                if (string.IsNullOrEmpty(hdnIncentiveId.Value))
                {
                    BindControl.BindLiteral(LblError, Messages.GetUpdateFailedMessage());
                    return;
                }

                if (string.IsNullOrEmpty(name))
                {
                    BindControl.BindLiteral(LblError, Messages.GetRequiredFieldsMessage(true, "Name", "Frequency of Payment"));
                    return;
                }

                int periodId = int.Parse(ddlPeriod.SelectedValue);
                int incentiveId = int.Parse(hdnIncentiveId.Value);
                bool updated = inc.Update(new Incentive
                {
                    Active = true,
                    CompanyId = compId,
                    IncentiveDescription = description,
                    IncentiveId = incentiveId,
                    IncentiveName = name,
                    PeriodId = periodId,
                    UpdatedBy = Util.user.LoginId
                });

                if (updated)
                {
                    LoadReadOnly();
                    LoadIncentives();
                }
                BindControl.BindLiteral(LblError, updated ? Messages.GetUpdateMessage() : Messages.GetAlreadyExistMessage());
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void DeactivateIncentive(ListViewDataItem e)
        {
            try
            {
                var hdnIncentiveId = e.FindControl("hdnIncentiveId") as HiddenField;
                if (string.IsNullOrEmpty(hdnIncentiveId.Value))
                {
                    BindControl.BindLiteral(LblError, Messages.GetDeleteFailedMessage());
                    return;
                }

                int incentiveId = int.Parse(hdnIncentiveId.Value);
                bool deactivated = inc.SetStatus(new Incentive
                {
                    Active = false,
                    IncentiveId = incentiveId,
                    UpdatedBy = Util.user.LoginId
                });

                if (deactivated)
                {
                    LoadReadOnly();
                    LoadIncentives();
                }
                BindControl.BindLiteral(LblError, deactivated ? Messages.GetDeleteMessage() : Messages.GetDeleteFailedMessage());
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void CancelUpdate()
        {
            LoadReadOnly();
            LoadIncentives();
        }

        protected void lvDatas_ItemCanceling(object sender, ListViewCancelEventArgs e)
        {
            CancelUpdate();
        }

        protected void lvData_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            LblError.Text = "";
            if (e.Item.ItemType == ListViewItemType.InsertItem ||
                e.Item.ItemType == ListViewItemType.DataItem)
            {
                switch (e.CommandName)
                {
                    case "add":
                        CreateNewIncentive(e);
                        break;
                }
            }
        }

        protected void lvDatas_ItemDeleting(object sender, ListViewDeleteEventArgs e)
        {
            ListViewDataItem el = lvData.Items[e.ItemIndex];
            DeactivateIncentive(el);
        }

        protected void lvDatas_ItemUpdating(object sender, ListViewUpdateEventArgs e)
        {
            ListViewDataItem el = lvData.Items[e.ItemIndex];
            UpdateIncentive(el);
        }

        protected void lvData_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            lvData.EditIndex = e.NewEditIndex;
            lvData.InsertItemPosition = InsertItemPosition.None;
            LoadIncentives();
        }

        protected void lvData_PagePropertiesChanged(object sender, EventArgs e)
        {
            LoadIncentives();
        }

        protected void lvData_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            try
            {
                if (lvData.EditIndex != -1 && e.Item.ItemType == ListViewItemType.DataItem)
                {
                    var ddlPeriods = e.Item.FindControl("ddlPeriod") as DropDownList;
                    var period = (e.Item.FindControl("hdnPeriodId") as HiddenField).Value;
                    if (ddlPeriods != null)
                    {
                        LoadPeriods(ddlPeriods, period);
                    }
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            lvData.InsertItemPosition = InsertItemPosition.None;
            LoadIncentives();
        }

        protected void lnkCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("index.aspx", true);
        }

    }
}