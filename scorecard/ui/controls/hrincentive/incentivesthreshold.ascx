﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="incentivesthreshold.ascx.cs"
    Inherits="scorecard.ui.controls.hrincentive.incentivesthreshold" %>
<%@ Import Namespace="scorecard.implementations" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:UpdatePanel ID="UpdatePanelData" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <h1>
            Incentive Threshold Management</h1>
        <div>
            <asp:MultiView ID="mainView" runat="server">
                <asp:View ID="vwData" runat="server">
                    <div>
                        <fieldset runat="server" id="searchPanel">
                            <legend>Filter Threshold Information </legend>
                            <table cellpadding="4" cellspacing="4">
                                <tr>
                                    <td>
                                        Incentive Type
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddIncentiveS" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        Year
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddYear" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:Button ID="btnFilter" runat="server" class="button" Text="Search" OnClick="btnFilter_Click" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </div>
                    <div>
                        <asp:ListView ID="lvData" runat="server" OnPagePropertiesChanged="lvData_PagePropertiesChanged"
                            OnItemDeleting="lvData_ItemDeleting" OnItemEditing="lvData_ItemEditing">
                            <LayoutTemplate>
                                <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                                    <thead>
                                        <tr id="trFirst" runat="server">
                                            <td style="width: 220px">
                                                Incentive Name
                                            </td>
                                            <td style="width: 220px">
                                                Salary Type
                                            </td>
                                            <td style="width: 80px; text-align: left;">
                                                Bottom Rating
                                            </td>
                                            <td style="width: 80px; text-align: left;">
                                                Upper Rating
                                            </td>
                                            <td style="width: 90px; text-align: left;">
                                                % of Salary Type
                                            </td>
                                            <td style="width: 80px; text-align: left;">
                                                Is For Company
                                            </td>
                                            <td style="width: 240px; text-align: left;">
                                                Org Unit Name
                                            </td>
                                            <td style="width: 100px">
                                                Start Date
                                            </td>
                                            <td style="width: 100px">
                                                End Date
                                            </td>
                                            <td width="70px">
                                                Edit
                                            </td>
                                            <td width="70px">
                                                Delete
                                            </td>
                                        </tr>
                                        <tr id="ItemPlaceHolder" runat="server">
                                        </tr>
                                    </thead>
                                </table>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tbody>
                                    <tr class="odd">
                                        <td>
                                            <%# Eval("Reward.IncentiveName")%>
                                            <asp:HiddenField ID="hdnThresholdId" runat="server" Value='<%# Bind("ThresholdId")%>' />
                                        </td>
                                        <td>
                                            <%# Eval("Salary.SalaryTypeName")%>
                                        </td>
                                        <td>
                                            <%# Eval("MinimumRating") %>
                                        </td>
                                        <td>
                                            <%# Eval("MaximumRating")%>
                                        </td>
                                        <td>
                                            <%# Eval("RewardPercentage")%>
                                        </td>
                                        <td>
                                            <%# Common.GetBooleanYesNo(Eval("IsForCompany")) %>
                                        </td>
                                        <td>
                                            <%# Eval("Company.ORGUNIT_NAME")%>
                                        </td>
                                        <td>
                                            <%# Common.GetDescriptiveDate(Eval("StartDate")) %>
                                        </td>
                                        <td>
                                            <%# Common.GetDescriptiveDate(Eval("EndDate")) %>
                                        </td>
                                        <td>
                                            <ul id="icons">
                                                <asp:LinkButton runat="server" ID="lnkEdit" CommandName="edit" class="ui-state-default ui-corner-all"
                                                    title="Edit Record"><span class="ui-icon ui-icon-pencil">
                                    </span></asp:LinkButton>
                                            </ul>
                                        </td>
                                        <td>
                                            <ul id="icons">
                                                <asp:LinkButton runat="server" ID="lnkDelete" CommandName="delete" class="ui-state-default ui-corner-all"
                                                    title="Delete Record" OnClientClick="return confirmDelete();"><span class="ui-icon ui-icon-trash">
                                    </span>
                                                </asp:LinkButton>
                                            </ul>
                                        </td>
                                    </tr>
                                </tbody>
                            </ItemTemplate>
                            <EmptyDataTemplate>
                                <span style="color: Red">There are no records to display.</span>
                            </EmptyDataTemplate>
                        </asp:ListView>
                        <br />
                    </div>
                    <asp:DataPager ID="dtPagerlvData" runat="server" PagedControlID="lvData" PageSize="15">
                        <Fields>
                            <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True" ShowNextPageButton="False"
                                ShowPreviousPageButton="False" />
                            <asp:NumericPagerField />
                            <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True" ShowNextPageButton="False"
                                ShowPreviousPageButton="False" />
                        </Fields>
                    </asp:DataPager>
                    <div>
                        <span style="color: Red">
                            <asp:Literal ID="LblError" runat="server" /></span>
                    </div>
                    <div>
                        <p>
                            <br />
                            <asp:Label ID="lblMsg" runat="server" Style="color: #FF3300" />
                            <br />
                        </p>
                    </div>
                    <div id="tabs-3">
                        <p>
                            <asp:Button ID="btnNew" runat="server" CssClass="button" Text="Add New Threshold"
                                OnClick="btnNew_Click" CausesValidation="false" />
                            <asp:Button ID="lnkCancel0" runat="server" CssClass="button" Text="Done" CausesValidation="false"
                                PostBackUrl="~/ui/index.aspx" />
                        </p>
                    </div>
                </asp:View>
                <asp:View ID="vwForm" runat="server">
                    <fieldset runat="server" id="pnlContactInfo">
                        <legend>Threshold Information </legend>
                        <p>
                            <label for="lf">
                                Incentive Name:
                            </label>
                            <span class="field_desc">
                                <asp:DropDownList name="dropdown" CssClass="dropdown" runat="server" ID="ddIncentiveF" />
                            </span>
                        </p>
                        <p>
                            <label for="sf">
                                Apply Start Date:</label><span class="field_desc">
                                    <asp:TextBox ID="txtStartDate" runat="server" Width="120px" ValidationGroup="Form"></asp:TextBox></span>
                            <asp:CalendarExtender ID="txtDate_CalendarExtender" runat="server" Enabled="True"
                                Format="d/MM/yyyy" TargetControlID="txtStartDate">
                            </asp:CalendarExtender>
                            <asp:RequiredFieldValidator ID="rqSDate" runat="server" ErrorMessage="Start Date is required" Display="Dynamic"
                                ControlToValidate="txtStartDate" ForeColor="Red" Font-Bold="false" ValidationGroup="Form"></asp:RequiredFieldValidator>
                        </p>
                        <p>
                            <label for="sf">
                              Apply End Date:</label><span class="field_desc">
                                    <asp:TextBox ID="txtEndDate" runat="server" Width="120px" ValidationGroup="Form"></asp:TextBox></span>
                            <asp:CalendarExtender ID="txtEndDate_CalendarExtender" runat="server" Enabled="True"
                                Format="d/MM/yyyy" TargetControlID="txtEndDate">
                            </asp:CalendarExtender>
                            <asp:RequiredFieldValidator ID="rqEDate" runat="server" ErrorMessage="End Date is required" Display="Dynamic"
                                ControlToValidate="txtEndDate" ForeColor="Red" Font-Bold="false" ValidationGroup="Form"></asp:RequiredFieldValidator>
                        </p>
                        <p>
                            <label for="lf">
                                Salary Type:
                            </label>
                            <span class="field_desc">
                                <asp:DropDownList name="dropdown" CssClass="dropdown" runat="server" ID="ddSalaryTypeF" />
                            </span>
                        </p>
                        <p>
                            <label for="lf">
                                Bottom Rating:
                            </label>
                            <span class="field_desc">
                                <asp:TextBox runat="server" ID="txtMininum" Text='<%# Eval("MinimumRating")%>' Width="25px"
                                    ValidationGroup="Form" onkeydown="return jsDecimals(event);" CssClass="mf" />
                                <asp:RequiredFieldValidator ID="rqdMin" runat="server" ErrorMessage="Bottom Rating is required" ValidationGroup="Form"
                                    ControlToValidate="txtMininum" ForeColor="Red" Font-Bold="false"></asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="cvdMinV" runat="server" ErrorMessage="only numeric value is allowed"
                                    ControlToValidate="txtMininum" ValidationGroup="Form" Display="Dynamic" Operator="DataTypeCheck"
                                    Type="Double" ForeColor="Red" Font-Bold="false"></asp:CompareValidator>
                            </span>
                        </p>
                        <p>
                            <label for="lf">
                                Upper Rating:
                            </label>
                            <span class="field_desc">
                                <asp:TextBox runat="server" ID="txtMaximum" Text='<%# Eval("MaximumRating")%>' Width="25px"
                                    ValidationGroup="Form" />
                                <asp:RequiredFieldValidator ID="rdqMaxV" runat="server" ErrorMessage="Upper Rating is required" ValidationGroup="Form"
                                    ControlToValidate="txtMaximum" ForeColor="Red" Font-Bold="false"></asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="cvdMaxV" runat="server" ErrorMessage="only numeric value is allowed"
                                    ControlToValidate="txtMaximum" ValidationGroup="Form" Display="Dynamic" Operator="DataTypeCheck"
                                    Type="Double" ForeColor="Red" Font-Bold="false"></asp:CompareValidator>
                            </span>
                        </p>
                        <p>
                            <label for="lf">
                                % Of Salary Type:
                            </label>
                            <span class="field_desc">
                                <asp:TextBox runat="server" ID="txtPercentage" Width="25px" ValidationGroup="Form" />
                                <asp:RequiredFieldValidator ID="rqdPercentV" runat="server" ErrorMessage="% Of Salary Type is required" ValidationGroup="Form"
                                    ControlToValidate="txtPercentage" ForeColor="Red" Font-Bold="false"></asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="cvdPercentV" runat="server" ErrorMessage="only numeric value is allowed"
                                    ControlToValidate="txtPercentage" ValidationGroup="Form" Display="Dynamic" Operator="DataTypeCheck"
                                    Type="Double" ForeColor="Red" Font-Bold="false"></asp:CompareValidator>
                            </span>
                        </p>
                        <p>
                            <label for="lf">
                                Set Threshold at Company Level:
                            </label>
                            <span class="field_desc">
                                <asp:DropDownList name="dropdown" class="dropdown" runat="server" ID="ddIsForCompany">
                                    <asp:ListItem Selected="True" Value="true">Yes</asp:ListItem>
                                    <asp:ListItem Value="false">No</asp:ListItem>
                                </asp:DropDownList>
                                <span class="errorMsg">
                                    <asp:Literal ID="lblIsForCompany" runat="server"></asp:Literal></span> </span>
                        </p>
                        <p>
                            <label for="lf">
                                Org Unit / Department:
                            </label>
                            <span class="field_desc">
                                <asp:DropDownList name="dropdown" CssClass="dropdown" runat="server" ID="ddOrgUnitF" />
                            </span>
                        </p>
                        <p class="errorMsg">
                            <br />
                            <br />
                            <asp:Literal ID="LblMsgForm" runat="server"></asp:Literal><br />
                            <br />
                        </p>
                        <p>
                            <label>
                                <asp:HiddenField ID="hdnThreshold" runat="server" />
                                &nbsp;</label>
                            <asp:Button ID="btnSave" runat="server" class="button" Text="Submit" OnClick="btnSave_Click" ValidationGroup="Form"/>
                            <asp:Button ID="btnCancelComplete" runat="server" class="button" Text="Cancel" CausesValidation="False"
                                OnClick="btnCancelComplete_Click" />
                        </p>
                    </fieldset>
                </asp:View>
            </asp:MultiView>
            <asp:UpdateProgress ID="UpdateProgressData" runat="server" AssociatedUpdatePanelID="UpdatePanelData">
                <ProgressTemplate>
                    <div>
                        <asp:Image ID="imgOrg" runat="server" ImageUrl="~/ui/images/icons/activity.gif" AlternateText="Please wait..">
                        </asp:Image></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
