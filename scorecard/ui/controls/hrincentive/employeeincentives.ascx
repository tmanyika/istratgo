﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="employeeincentives.ascx.cs"
    Inherits="scorecard.ui.controls.hrincentive.employyeincentives" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<h1>
    Employee Incentives Management</h1>
<asp:UpdatePanel ID="UpdatePanelUser" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div>
            <asp:ListView ID="lvData" runat="server" OnPagePropertiesChanged="lvData_PagePropertiesChanged">
                <LayoutTemplate>
                    <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                        <thead>
                            <tr>
                                <td>
                                    Full Name
                                </td>
                                <td>
                                    Employee No.
                                </td>
                                <td>
                                    Email Address
                                </td>
                                <td>
                                    Job Title
                                </td>
                                <td>
                                    Company
                                </td>
                                <td align="center">
                                    Manage Incentives
                                </td>
                            </tr>
                            <tr id="ItemPlaceHolder" runat="server">
                            </tr>
                        </thead>
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <tbody>
                        <tr class="odd">
                            <td>
                                <%# Eval("FullName")%>
                                <asp:HiddenField ID="hdnId" runat="server" Value='<%# Eval("EmployeeId")%>' />
                            </td>
                            <td>
                                <%# Eval("EmployeeNo")%>
                            </td>
                            <td>
                                <%# Eval("EmailAddress")%>
                            </td>
                            <td>
                                <%# Eval("Position.Name")%>
                            </td>
                            <td>
                                <%# Eval("Organisation.ORGUNIT_NAME")%>
                            </td>
                            <td align="center">
                                <div style="width: 100%; text-align: center">
                                    <a href='<%# GetUrl(Eval("EmployeeId"), Eval("FullName"))  %>' onclick="return hs.htmlExpand(this, { objectType: 'iframe', width: 900, height: 450, creditsPosition: 'bottom left', headingText: '<%# StrConvToUpper(Eval("FullName")) %> INCENTIVES', wrapperClassName: 'titlebar' } )">
                                        <span class="links">Manage</span></a>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </ItemTemplate>
                <EmptyItemTemplate>
                    <p class="errorMsg">
                        There are no records to display.</p>
                </EmptyItemTemplate>
            </asp:ListView>
            <br />
            <asp:DataPager ID="PagerData" runat="server" PagedControlID="lvData" PageSize="10">
                <Fields>
                    <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True" ShowNextPageButton="False"
                        ShowPreviousPageButton="False" />
                    <asp:NumericPagerField />
                    <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True" ShowNextPageButton="False"
                        ShowPreviousPageButton="False" />
                </Fields>
            </asp:DataPager>
            <br />
            <asp:UpdateProgress ID="UpdateProgressUser" runat="server" AssociatedUpdatePanelID="UpdatePanelUser">
                <ProgressTemplate>
                    <div>
                        <asp:Image ID="imgOrg" runat="server" ImageUrl="~/ui/images/icons/activity.gif" AlternateText="Please wait..">
                        </asp:Image>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <br />
            <div>
                <p class="errorMsg">
                    <br />
                    <asp:Literal ID="lblMsg" runat="server"></asp:Literal>
                    <br />
                </p>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
