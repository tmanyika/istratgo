﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HR.Incentives.Interfaces;
using HR.Incentives.Logic;
using scorecard.implementations;
using scorecard.interfaces;
using HR.Incentives.Entities;
using System.Text;

namespace scorecard.ui.controls.hrincentive
{
    public partial class incentivesthreshold : System.Web.UI.UserControl
    {
        IIncentiveThreshold thresh;
        IIncentive inc;
        Iorgunit org;
        ISalaryType salary;

        readonly int orgUnitId;
        readonly int compId;

        public incentivesthreshold()
        {
            thresh = new IncentiveThresholdBL();
            inc = new IncentiveBL();
            org = new structureImpl();
            salary = new SalaryTypeBL();

            orgUnitId = (Util.user != null) ? Util.user.OrgId : 0;
            compId = (Util.user != null) ? Util.user.CompanyId : 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ResetLabels();
            if (!IsPostBack)
            {
                LoadFilterForm();
                LoadThresholds();
            }
        }

        void ResetLabels()
        {
            lblMsg.Text = "";
            LblError.Text = "";
            lblIsForCompany.Text = "";
        }

        void LoadFilterForm()
        {
            LoadIncentives(ddIncentiveS, string.Empty);
            ddIncentiveS.Items.Insert(0, new ListItem
            {
                Text = "- select all -",
                Value = "-1"
            });
            LoadYears(ddYear, string.Empty);
        }

        void LoadYears(DropDownList dd, string selectedVal)
        {
            int maxYears = Util.MaxYearsForSearchDropDown;
            int currentYear = DateTime.Now.Year;
            for (int i = maxYears; i >= 1; i--)
            {
                bool selected = currentYear.ToString().Equals(selectedVal);
                int yearVal = currentYear - i;

                dd.Items.Add(new ListItem
                {
                    Text = yearVal.ToString(),
                    Value = yearVal.ToString(),
                    Selected = selected
                });

            }

            dd.Items.Insert(0, new ListItem
            {
                Text = "- select all -",
                Value = "-1"
            });
        }

        void LoadThresholds()
        {
            try
            {
                int? year = null;
                int? incentiveId = null;

                if (ddIncentiveS.SelectedIndex != 0)
                {
                    incentiveId = int.Parse(ddIncentiveS.SelectedValue);
                }

                if (ddYear.SelectedIndex != 0)
                {
                    year = int.Parse(ddYear.SelectedValue);
                }

                mainView.SetActiveView(vwData);
                var data = thresh.Search(compId, orgUnitId, true, incentiveId, year); //thresh.GetByStatus(compId, orgUnitId, true);
                dtPagerlvData.Visible = (data.Count > dtPagerlvData.PageSize);
                BindControl.BindListView(lvData, data);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void LoadIncentives(DropDownList dd, string defaultValue)
        {
            if (dd == null) return;
            var data = inc.GetByStatus(compId, orgUnitId, true);
            BindControl.BindDropdown(dd, "IncentiveName", "IncentiveId", defaultValue, data);
        }

        void LoadOrgUnits(DropDownList dd, string defaultValue)
        {
            if (dd == null) return;
            var data = org.getOrgStructureInHirachy(orgUnitId);
            BindControl.BindDropdown(dd, "ORGUNIT_NAME", "ID", "- set threshold at company level -", "-1", defaultValue, data);
        }

        void LoadSalaryTypes(DropDownList dd, string defaultValue)
        {
            if (dd == null) return;
            var data = salary.GetByStatus(compId, true);
            BindControl.BindDropdown(dd, "SalaryTypeName", "SalaryTypeId", defaultValue, data);
        }

        void ClearAllLabels()
        {
            LblError.Text = string.Empty;
            lblMsg.Text = string.Empty;
            LblMsgForm.Text = string.Empty;
        }

        void ClearForm()
        {
            txtMaximum.Text = string.Empty;
            txtMininum.Text = string.Empty;
            txtPercentage.Text = string.Empty;
            hdnThreshold.Value = string.Empty;
        }

        void LoadListViewFooterForm()
        {
            lvData.InsertItemPosition = InsertItemPosition.LastItem;
            LoadThresholds();
            DropDownList ddIncentive = lvData.InsertItem.FindControl("ddIncentive") as DropDownList;
            DropDownList ddOrgUnit = lvData.InsertItem.FindControl("ddOrgUnit") as DropDownList;
            DropDownList ddPeriod = lvData.InsertItem.FindControl("ddPeriod") as DropDownList;
            DropDownList ddSalary = lvData.InsertItem.FindControl("ddSalary") as DropDownList;
        }

        void LoadForm()
        {
            try
            {
                mainView.SetActiveView(vwForm);

                ClearAllLabels();
                ClearForm();

                LoadOrgUnits(ddOrgUnitF, string.Empty);
                LoadIncentives(ddIncentiveF, string.Empty);
                LoadSalaryTypes(ddSalaryTypeF, string.Empty);
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(LblError, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        void SaveThreshold(bool update)
        {
            try
            {
                int companyId = compId;
                int? organisationUnitId = null;

                int incentiveId = int.Parse(ddIncentiveF.SelectedValue);
                int salaryTypeId = int.Parse(ddSalaryTypeF.SelectedValue);
                long thresholdId = update ? long.Parse(hdnThreshold.Value) : 0;

                decimal maximumrate = decimal.Parse(txtMaximum.Text);
                decimal minimumrate = decimal.Parse(txtMininum.Text);
                decimal rewardPercentage = decimal.Parse(txtPercentage.Text);

                if (ddOrgUnitF.SelectedIndex != 0)
                {
                    organisationUnitId = int.Parse(ddOrgUnitF.SelectedValue);
                }

                bool isForCompany = bool.Parse(ddIsForCompany.SelectedValue);
                //bool isForCompany = ddOrgUnitF.SelectedIndex == 0 ? true : false;

                string createdBy = Util.user.LoginId;
                string sDate = Common.GetInternationalDateFormat(txtStartDate.Text, DateFormat.dayMonthYear);
                string eDate = Common.GetInternationalDateFormat(txtEndDate.Text, DateFormat.dayMonthYear);

                DateTime startDate = DateTime.Parse(sDate);
                DateTime endDate = DateTime.Parse(eDate);

                if (isForCompany && organisationUnitId != null)
                {
                    BindControl.BindLiteral(LblMsgForm, "You cannot select organisation unit if the threshold is set at company level.");
                    BindControl.BindLiteral(lblIsForCompany, "select <b>No</b> to set threshold at organisation unit level.");
                    return;
                }
                else if (!isForCompany && organisationUnitId == null)
                {
                    BindControl.BindLiteral(LblMsgForm, "Organisation unit is required.");
                    return;
                }

                if (startDate >= endDate)
                {
                    BindControl.BindLiteral(LblMsgForm, "Start Date cannot be equal or greater than the End Date");
                    return;
                }

                if (minimumrate >= maximumrate)
                {
                    BindControl.BindLiteral(LblMsgForm, "Minimum Rating cannot be equal or greater than the Maximum Rating");
                    return;
                }

                var obj = new IncentiveThreshold
                {
                    Active = true,
                    CompanyId = companyId,
                    CreatedBy = createdBy,
                    EndDate = endDate,
                    IncentiveId = incentiveId,
                    IsForCompany = isForCompany,
                    MaximumRating = maximumrate,
                    MinimumRating = minimumrate,
                    OrgUnitId = organisationUnitId,
                    RewardConstant = 0,
                    RewardPercentage = rewardPercentage,
                    SalaryTypeId = salaryTypeId,
                    StartDate = startDate,
                    ThresholdId = thresholdId,
                    UpdatedBy = createdBy
                };

                bool added = update ? thresh.Update(obj) : thresh.Add(obj);
                if (added)
                {
                    LoadThresholds();
                }

                StringBuilder strB = new StringBuilder("The reason for not saving could be any of the following:<br/><br/><ol style='padding-left:20px;'>");
                if (!update)
                {
                    strB.Append("<li>the threshold already exists</li>");
                }

                strB.Append("<li>your minimum & maximum rating may be overlapping with the existing threshold</li>");
                strB.Append("<li>start & end date may be overlapping with the existing threshold</li>");
                strB.Append("</ol>");

                BindControl.BindLiteral(LblMsgForm, added ? Messages.GetSaveMessage() : string.Format("{0} {1}", Messages.GetSaveFailedMessage(), strB.ToString()));
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(LblMsgForm, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        void EditThreshold(string dataKeyId)
        {
            try
            {
                if (string.IsNullOrEmpty(dataKeyId))
                {
                    BindControl.BindLiteral(LblError, Messages.GetRecordNotFound);
                    return;
                }

                long thresholdId = long.Parse(dataKeyId);
                var data = thresh.GetById(thresholdId);

                if (data.ThresholdId != 0)
                {
                    LoadForm();

                    hdnThreshold.Value = data.ThresholdId.ToString();
                    txtMaximum.Text = data.MaximumRating.ToString();
                    txtMininum.Text = data.MinimumRating.ToString();
                    txtPercentage.Text = data.RewardPercentage.ToString();
                    txtStartDate.Text = Common.GetCustomDateFormat(data.StartDate, "/");
                    txtEndDate.Text = Common.GetCustomDateFormat(data.EndDate.Date, "/");

                    if (ddIncentiveF.Items.FindByValue(data.IncentiveId.ToString()) != null)
                    {
                        ddIncentiveF.SelectedValue = data.IncentiveId.ToString();
                    }
                    if (ddSalaryTypeF.Items.FindByValue(data.SalaryTypeId.ToString()) != null)
                    {
                        ddSalaryTypeF.SelectedValue = data.SalaryTypeId.ToString();
                    }
                    if (data.OrgUnitId != null && ddOrgUnitF.Items.FindByValue(data.OrgUnitId.ToString()) != null)
                    {
                        ddOrgUnitF.SelectedValue = data.OrgUnitId.ToString();
                    }
                    if (ddIsForCompany.Items.FindByValue(data.IsForCompany.ToString().ToLower()) != null)
                    {
                        ddIsForCompany.SelectedValue = data.IsForCompany.ToString().ToLower();
                    }
                }
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(LblError, Messages.GetErrorMessage());
                mainView.SetActiveView(vwData);
                Util.LogErrors(ex);
            }
        }

        void RemoveThreshold(ListViewDataItem e)
        {
            try
            {
                var hdnThresholdId = e.FindControl("hdnThresholdId") as HiddenField;
                if (string.IsNullOrEmpty(hdnThresholdId.Value))
                {
                    BindControl.BindLiteral(LblError, Messages.GetDeleteFailedMessage());
                    return;
                }

                int thresholdId = int.Parse(hdnThresholdId.Value);
                bool deactivated = thresh.SetStatus(new IncentiveThreshold
                {
                    Active = false,
                    ThresholdId = thresholdId,
                    UpdatedBy = Util.user.LoginId
                });

                if (deactivated)
                {
                    LoadThresholds();
                }
                BindControl.BindLiteral(LblError, deactivated ? Messages.GetDeleteMessage() : Messages.GetDeleteFailedMessage());
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        protected void lvData_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            var thresholdId = lvData.Items[e.NewEditIndex].FindControl("hdnThresholdId") as HiddenField;
            EditThreshold(thresholdId.Value);
        }

        protected void lvData_ItemDeleting(object sender, ListViewDeleteEventArgs e)
        {
            ListViewDataItem el = lvData.Items[e.ItemIndex];
            RemoveThreshold(el);
        }

        protected void lvData_PagePropertiesChanged(object sender, EventArgs e)
        {
            LoadThresholds();
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            LoadForm();
        }

        protected void btnCancelComplete_Click(object sender, EventArgs e)
        {
            mainView.SetActiveView(vwData);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            bool update = string.IsNullOrEmpty(hdnThreshold.Value) ? false : true;
            SaveThreshold(update);
        }

        protected void btnFilter_Click(object sender, EventArgs e)
        {
            LoadThresholds();
        }
    }
}