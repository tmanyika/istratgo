﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="incentivesmgt.ascx.cs"
    Inherits="scorecard.ui.controls.hrincentive.incentivesmgt" %>
<%@ Import Namespace="scorecard.implementations" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:UpdatePanel ID="UpdatePanelComp" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <h1>
            Incentive Types Management
        </h1>
        <asp:ListView ID="lvData" runat="server" OnItemCommand="lvData_ItemCommand" OnItemEditing="lvData_ItemEditing"
            OnPagePropertiesChanged="lvData_PagePropertiesChanged" OnItemCanceling="lvDatas_ItemCanceling"
            OnItemDeleting="lvDatas_ItemDeleting" OnItemUpdating="lvDatas_ItemUpdating" OnItemDataBound="lvData_ItemDataBound">
            <LayoutTemplate>
                <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                    <thead>
                        <tr id="trFirst" runat="server">
                            <td style="width: 300px">
                                Incentive
                            </td>
                            <td align="left" style="width: 320px">
                                Description
                            </td>
                            <td style="width: 200px">
                                Paid
                            </td>
                            <td width="80px">
                                Edit
                            </td>
                            <td width="80px">
                                Delete
                            </td>
                        </tr>
                        <tr id="ItemPlaceHolder" runat="server">
                        </tr>
                    </thead>
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tbody>
                    <tr class="odd">
                        <td>
                            <%# Eval("IncentiveName")%>
                            <asp:HiddenField ID="hdnIncentiveId" runat="server" Value='<%# Bind("IncentiveId")%>' />
                            <asp:HiddenField ID="hdnPeriodId" runat="server" Value='<%# Bind("PeriodId")%>' />
                        </td>
                        <td>
                            <%# Eval("IncentiveDescription")%>
                        </td>
                        <td>
                            <%# Eval("YearPeriod.PeriodName")%>
                        </td>
                        <td>
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkEdit" CommandName="edit" class="ui-state-default ui-corner-all"
                                    title="Edit Record"><span class="ui-icon ui-icon-pencil">
                        </span></asp:LinkButton>
                            </ul>
                        </td>
                        <td>
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkDelete" CommandName="delete" class="ui-state-default ui-corner-all"
                                    title="Delete Record" OnClientClick="return confirmDelete();"><span class="ui-icon ui-icon-trash">
                                    </span>
                                </asp:LinkButton>
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </ItemTemplate>
            <EditItemTemplate>
                <tbody>
                    <tr class="odd">
                        <td>
                            <asp:TextBox runat="server" ID="txtEditName" Text='<%# Eval("IncentiveName")%>' ValidationGroup="Edit"
                                Width="250px" />
                            <asp:HiddenField ID="hdnIncentiveId" runat="server" Value='<%# Bind("IncentiveId")%>' />
                            <asp:HiddenField ID="hdnPeriodId" runat="server" Value='<%# Bind("PeriodId")%>' />
                            <asp:RequiredFieldValidator ID="RqdName" runat="server" ErrorMessage="*" ValidationGroup="Edit"
                                ControlToValidate="txtEditName" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtEditDescription" Text='<%# Eval("IncentiveDescription")%>'
                                Width="250px" />
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlPeriod" ValidationGroup="Insert" Width="170px" />
                        </td>
                        <td>
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkSave" CommandName="update" class="ui-state-default ui-corner-all"
                                    title="Update Record" ValidationGroup="Edit"><span class="ui-icon ui-icon-disk">
                                     </span>
                                </asp:LinkButton>
                            </ul>
                        </td>
                        <td>
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkCancelUpdate" CommandName="cancel" class="ui-state-default ui-corner-all"
                                    title="Cancel Update" CausesValidation="false"><span class="ui-icon ui-icon-cancel">
                                    </span>
                                </asp:LinkButton>
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </EditItemTemplate>
            <InsertItemTemplate>
                <tbody>
                    <tr>
                        <td align="left" valign="top">
                            <asp:TextBox runat="server" ID="txtName" ValidationGroup="Insert" Width="250px" />
                            <asp:RequiredFieldValidator ID="RqdInsertName" runat="server" ErrorMessage="*" ValidationGroup="Insert"
                                ControlToValidate="txtName" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" valign="top">
                            <asp:TextBox runat="server" ID="txtDescription" Width="250px" />
                        </td>
                        <td align="left" valign="top">
                            <asp:DropDownList runat="server" ID="ddlPeriod" ValidationGroup="Insert" Width="170px" />
                        </td>
                        <td valign="top">
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkSave" class="ui-state-default ui-corner-all"
                                    title="Save this Record" CommandName="add" ValidationGroup="Insert"><span class="ui-icon ui-icon-disk">
                        </span></asp:LinkButton>
                            </ul>
                        </td>
                        <td valign="top">
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkCancel" class="ui-state-default ui-corner-all"
                                    title="Cancel Save " CommandName="cancel"><span class="ui-icon ui-icon-cancel">
                        </span></asp:LinkButton>
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </InsertItemTemplate>
            <EmptyDataTemplate>
                <span style="color: Red">There are no records to display.</span>
            </EmptyDataTemplate>
        </asp:ListView>
        <br />
        <asp:DataPager ID="dtPagerlvDatas" runat="server" PagedControlID="lvData" PageSize="15">
            <Fields>
                <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True" ShowNextPageButton="False"
                    ShowPreviousPageButton="False" />
                <asp:NumericPagerField />
                <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True" ShowNextPageButton="False"
                    ShowPreviousPageButton="False" />
            </Fields>
        </asp:DataPager>
        <div>
            <span style="color: Red">
                <asp:Literal ID="LblError" runat="server" /></span>
        </div>
        <div>
            <p>
                <br />
                <br />
                <asp:Label ID="lblMsg" runat="server" Style="color: #FF3300" />
                <br />
                <br />
            </p>
        </div>
        <div id="tabs-3">
            <p>
                <asp:Button ID="btnNew" runat="server" CssClass="button" Text="Add New Incentive"
                    OnClick="btnNew_Click" CausesValidation="false" />
                <asp:Button ID="lnkCancel0" runat="server" CssClass="button" Text="Done" CausesValidation="false"
                    PostBackUrl="~/ui/index.aspx" />
            </p>
        </div>
        <asp:UpdateProgress ID="UpdateProgressComp" runat="server" AssociatedUpdatePanelID="UpdatePanelComp">
            <ProgressTemplate>
                <div>
                    <asp:Image ID="imgOrg" runat="server" ImageUrl="~/ui/images/icons/activity.gif" AlternateText="Please wait..">
                    </asp:Image></div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
