﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="roleaccess.ascx.cs"
    Inherits="scorecard.ui.controls.roleaccess" %>
<h1>
    Role Based Access
</h1>
<asp:UpdatePanel ID="UpdatePanelRole" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <fieldset>
            <legend>Select User Roles </legend>
            <p>
                <asp:DropDownList ID="ddlUserRoles" runat="server" AutoPostBack="true" Width="140px"
                    OnSelectedIndexChanged="ddlUserRoles_SelectedIndexChanged">
                </asp:DropDownList>
            </p>
        </fieldset>
        <fieldset>
            <legend>Select Menu Access </legend>
            <p>
                <asp:ListBox ID="lstMenus" runat="server" SelectionMode="Multiple"></asp:ListBox>
            </p>
        </fieldset>
        <asp:Panel ID="pnlNoData" runat="server" Visible="false">
            <div class="message information close">
                <h2>
                    Information
                </h2>
                <p>
                    Changes saved succesfully.
                </p>
            </div>
        </asp:Panel>
        <hr />
        <asp:Button runat="server" ID="btnUpdate" CssClass="button" Text="Apply changes"
            OnClick="btnUpdate_Click" />
        <br />
        <br />
        <asp:UpdateProgress ID="UpdateProgressRole" runat="server" AssociatedUpdatePanelID="UpdatePanelRole">
            <ProgressTemplate>
                <div>
                    <asp:Image ID="imgOrg" runat="server" ImageUrl="~/ui/images/icons/activity.gif" AlternateText="Please wait..">
                    </asp:Image></div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
