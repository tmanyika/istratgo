﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="headerexternal.ascx.cs"
    Inherits="scorecard.ui.controls.extheader" %>
<%@ Register Src="mainmenu.ascx" TagName="mainmenu" TagPrefix="uc1" %>
<div id="header">
    <!-- Top -->
    <div id="top">
        <!-- Logo -->
        <div class="logo">
            <a href="#" title="iStratgo" class="tooltip">
                <img src="assets/logo.png" alt="iStratgo" id="mylogo" /></a>
        </div>
        <!-- End of Logo -->
        <!-- Meta information -->
        <div class="meta">
            <p>
                <asp:Label ID="lblloggedInText" runat="server" />
            </p>
        </div>
        <!-- End of Meta information -->
    </div>
    <!-- End of Top-->
    <!-- The navigation bar -->
    <div id="navbar">
        &nbsp;s
    </div>
    <!-- End of navigation bar" -->
    <!-- Search bar -->
    <!-- End of Search bar -->
</div>
<!-- End of Header -->
