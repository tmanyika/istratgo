﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="capture.ascx.cs" Inherits="scorecard.ui.controls.capture" %>
<%@ Import Namespace="scorecard.implementations" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:UpdatePanel ID="UpdatePanelCapture" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div>
            <h1>
                Capture Scores
            </h1>
        </div>
        <asp:MultiView ID="MainView" runat="server" ActiveViewIndex="0">
            <asp:View ID="vwData" runat="server">
                <div>
                    <asp:ListView ID="lvSubmittedWorkflows" runat="server" OnItemCommand="lvSubmittedWorkflows_ItemCommand"
                        OnItemDeleting="lvSubmittedWorkflows_ItemDeleting">
                        <LayoutTemplate>
                            <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                                <thead>
                                    <tr id="Tr1" runat="server">
                                        <td>
                                            <input type="checkbox" class="checkall" />
                                        </td>
                                        <td>
                                            Criteria
                                        </td>
                                        <td>
                                            Name
                                        </td>
                                        <td>
                                            Approver
                                        </td>
                                        <td>
                                            Status
                                        </td>
                                        <td>
                                            Date
                                        </td>
                                        <td>
                                            Comment
                                        </td>
                                        <td>
                                            Update
                                        </td>
                                        <td>
                                            Delete
                                        </td>
                                    </tr>
                                    <tr id="ItemPlaceHolder" runat="server">
                                    </tr>
                                </thead>
                            </table>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tbody>
                                <tr class="odd">
                                    <td>
                                        <input type="checkbox" />
                                        <asp:Label ID="lblID" runat="server" Visible="false" Text='<%# Eval("ID") %>' />
                                        <asp:Label ID="lblCriteria" runat="server" Visible="false" Text='<%# Eval("CRITERIA_TYPE_ID") %>' />
                                        <asp:Label ID="lblItemSelected" runat="server" Visible="false" Text='<%# Eval("SELECTED_ITEM_VALUE") %>' />
                                        <asp:HiddenField ID="hdnOverallComment" runat="server" Value='<%# Eval("OVERALL_COMMENT") %>' />
                                        <asp:HiddenField ID="hdnPrevStatusId" runat="server" Value='<%# Eval("STATUS") %>' />
                                    </td>
                                    <td>
                                        <%# Eval("CriteriaDescription")%>
                                    </td>
                                    <td>
                                        <%# getObjectName(int.Parse(Eval("SELECTED_ITEM_VALUE").ToString()), int.Parse(Eval("CRITERIA_TYPE_ID").ToString()))%>
                                    </td>
                                    <td>
                                        <%#Eval("FirstName")%>
                                        <%#Eval("MiddleName")%>
                                        <%#Eval("LastName")%>
                                    </td>
                                    <td>
                                        <%# Eval("StatusDescription") %>
                                    </td>
                                    <td>
                                        <%# String.Format("{0:dd-MM-yyyy}", Eval("DATE_SUBMITTED"))%>
                                    </td>
                                    <td>
                                        <%# Eval("OVERALL_COMMENT")%>
                                    </td>
                                    <td>
                                        <ul id="icons">
                                            <asp:LinkButton runat="server" ID="lnkView" CommandName="View" class="ui-state-default ui-corner-all"
                                                title="View Details" CausesValidation="false"><span class="ui-icon ui-icon-pencil">
                        </span></asp:LinkButton>
                                        </ul>
                                    </td>
                                    <td>
                                        <ul id="Ul1">
                                            <asp:LinkButton runat="server" ID="lnkDelete" CommandName="Delete" class="ui-state-default ui-corner-all"
                                                title="Delete Record" OnClientClick="return confirmDelete();"><span class="ui-icon ui-icon-trash">
                        </span></asp:LinkButton>
                                        </ul>
                                    </td>
                                </tr>
                            </tbody>
                        </ItemTemplate>
                        <EmptyItemTemplate>
                            There are no records to display.</EmptyItemTemplate>
                    </asp:ListView>
                    <p style="color: #FF3300">
                        <asp:Literal ID="LblNoData" runat="server"></asp:Literal></p>
                </div>
                <div style="width: 100%; text-align: left">
                    <br />
                    <asp:Button ID="btnNewScore" runat="server" CssClass="button" CausesValidation="false"
                        Text="Capture New Scores" OnClick="btnNewScore_Click" />
                </div>
            </asp:View>
            <asp:View ID="vwForm" runat="server">
                <div>
                    <fieldset runat="server" id="pnlContactInfo">
                        <legend>Capture your scores</legend>
                        <p>
                            <label for="sf">
                                Select Area:
                            </label>
                            &nbsp;<span class="field_desc"><asp:DropDownList runat="server" ID="ddlArea" AutoPostBack="True"
                                OnSelectedIndexChanged="ddlArea_SelectedIndexChanged" ValidationGroup="Form" />
                            </span>
                            <asp:RequiredFieldValidator ID="rqdArea" runat="server" ControlToValidate="ddlArea"
                                Display="Dynamic" ErrorMessage="area is required" InitialValue="-1" ValidationGroup="Form"
                                ForeColor="Red"></asp:RequiredFieldValidator>
                        </p>
                        <p>
                            <label for="sf">
                                Select Value:
                            </label>
                            &nbsp;<span class="field_desc"><asp:DropDownList runat="server" ID="ddlValueTypes"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlValueTypes_SelectedIndexChanged"
                                ValidationGroup="Form" />
                            </span>
                            <asp:RequiredFieldValidator ID="rqdVal" runat="server" ControlToValidate="ddlValueTypes"
                                Display="Dynamic" ErrorMessage="value is required" InitialValue="-1" ValidationGroup="Form"
                                ForeColor="Red"></asp:RequiredFieldValidator>
                        </p>
                        <p>
                            <label for="sf">
                                Date of Score:</label>&nbsp; <span class="field_desc">
                                    <asp:TextBox ID="txtDate" runat="server" Width="120px" ValidationGroup="Form"></asp:TextBox></span>
                            <asp:CalendarExtender ID="txtDate_CalendarExtender" runat="server" Enabled="True"
                                Format="d/MM/yyyy" TargetControlID="txtDate">
                            </asp:CalendarExtender>
                            <asp:RequiredFieldValidator ID="rqDate" runat="server" ErrorMessage="Date of Score"
                                Display="None" ControlToValidate="txtDate" ForeColor="Red" ValidationGroup="Form"></asp:RequiredFieldValidator>
                            <span style="color: Red">
                                <asp:Literal ID="lblDateUsed" runat="server"></asp:Literal></span>
                        </p>
                    </fieldset>
                    <asp:Panel runat="server" ID="pnlScores" Width="100%">
                        <asp:ListView ID="lvGoals" runat="server">
                            <LayoutTemplate>
                                <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                                    <thead>
                                        <tr id="trHeader" runat="server">
                                            <td>
                                                Performance Measure
                                            </td>
                                            <td>
                                                Score
                                            </td>
                                            <td style="width: 170px">
                                                Justification Comments
                                            </td>
                                            <td style="width: 170px">
                                                Evidence
                                            </td>
                                            <td>
                                                Target
                                            </td>
                                            <td>
                                                Unit Of Measure
                                            </td>
                                            <td align="right">
                                                Weight
                                            </td>
                                            <td align="right">
                                                Manager Score
                                            </td>
                                            <td style="width: 170px">
                                                Manager's Comment
                                            </td>
                                        </tr>
                                        <tr id="ItemPlaceHolder" runat="server">
                                        </tr>
                                    </thead>
                                </table>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tbody>
                                    <tr class="odd">
                                        <td>
                                            <%#  Eval("GOAL_DESCRIPTION")%><asp:Label ID="lblTotal" runat="server" Visible="false"
                                                Text=""></asp:Label>
                                            <asp:Label ID="lblID" runat="server" Visible="false" Text='<%# Eval("ID") %>' />
                                            <asp:Label ID="lblUnitMeasureID" runat="server" Text='<%# Eval("UNIT_MEASURE") %>'
                                                Visible="false" />
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtScore" Width="50px" Text='<%# Eval("SCORES") %>' />
                                            <span style="color: Red">
                                                <asp:Literal ID="lblError" runat="server" Text=""></asp:Literal></span>
                                            <asp:HiddenField ID="hdnUnitMeasureId" runat="server" Value='<%# Bind("UNIT_MEASURE") %>' />
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtComment" Width="220px" Text='<%# Bind("JUSTIFICATION_COMMENT") %>' />
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtEvidence" Width="220px" Text='<%# Bind("EVIDENCE") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblTarget" runat="server" Text='<%# Eval("TARGET")%>' />
                                        </td>
                                        <td>
                                            <%# getTypeName(int.Parse(Eval("UNIT_MEASURE").ToString()))%>
                                        </td>
                                        <td align="right">
                                            <asp:Label ID="lblWeight" runat="server" Text='<%# Eval("WEIGHT")%>'></asp:Label>
                                            %
                                        </td>
                                        <td align="right">
                                            <%# Eval("MANAGER_SCORE")%>
                                        </td>
                                        <td>
                                            <%# Eval("MANAGER_COMMENT") %>
                                        </td>
                                    </tr>
                                </tbody>
                            </ItemTemplate>
                        </asp:ListView>
                        <br />
                        <p>
                            <br />
                            <span style="color: Red">
                                <asp:Literal ID="lblMsg" runat="server"></asp:Literal></span><br />
                        </p>
                        <asp:Panel ID="pnlNoData" runat="server" Visible="false">
                            <div class="message information close">
                                <h2>
                                    Information
                                </h2>
                                <p>
                                    You do not have any goals defined yet.
                                </p>
                            </div>
                        </asp:Panel>
                        <fieldset runat="server" id="docFieldSet">
                            <legend>Supporting Documents </legend>
                            <div>
                                <asp:Repeater ID="rptFile" runat="server" OnItemCommand="rptFile_ItemCommand">
                                    <HeaderTemplate>
                                        <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                                            <thead>
                                                <tr>
                                                    <td>
                                                        <b>File Name</b>
                                                    </td>
                                                    <td>
                                                        <b>Remove</b>
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <asp:HiddenField ID="HdnId" runat="server" Value='<%# Bind("ID") %>' />
                                                <a href='<%# getfilePath(Eval("ATTACHMENT_NAME")) %>' id="lnk" runat="server" target="_blank">
                                                    <asp:Image ID="IcnAttachment" runat="server" ImageUrl="~/ui/assets/1334961966_attachment.png" />
                                                    <%# Eval("ATTACHMENT_NAME")%>
                                                </a>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="LnkRemove" runat="server" CommandName="Delete" CommandArgument='<%# Bind("ID") %>'
                                                    CausesValidation="false" ForeColor="Blue">Delete</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody></table>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <br />
                                <br />
                                <span style="color: Red">
                                    <asp:Literal ID="LblDelete" runat="server"></asp:Literal></span>
                            </div>
                            <div>
                                <p>
                                    Locate Document (<span style="color: Red">Maximum 10MB</span>)
                                </p>
                                <br />
                                <telerik:RadAsyncUpload ID="RdAsyncUpd" runat="server" MultipleFileSelection="Automatic"
                                    OverwriteExistingFiles="True" Skin="Web20" MaxFileSize="10485760" />
                            </div>
                            <div id="exFileList" class="file-list">
                                <%-- <strong>Uploaded files in the target folder:</strong>--%>
                            </div>
                        </fieldset>
                        <br />
                        <p>
                            <asp:Button ID="btnNew" runat="server" CssClass="button tooltip" Text="Submit to Manager"
                                OnClick="btnNew_Click" ValidationGroup="Form" />&nbsp;<asp:Button ID="btnSaveLater"
                                    runat="server" CssClass="button tooltip" Text="Save For Later" CommandArgument="330"
                                    OnClick="btnSaveLater_Click" ValidationGroup="Form" />&nbsp;
                            <asp:Button ID="lnkCancelToManager" runat="server" CssClass="button tooltip" CausesValidation="false"
                                Text="Done" PostBackUrl="~/ui/index.aspx" />
                        </p>
                    </asp:Panel>
                    <asp:Panel ID="pnlSubmitted" runat="server" Visible="false" Width="100%">
                        <fieldset runat="server" id="Fieldset2">
                            <legend>Confirmation </legend>
                            <p style="color: #FF3300">
                                <asp:Literal ID="LblOutCome" runat="server"></asp:Literal>
                            </p>
                            <p>
                                <asp:Button ID="btnBack" runat="server" CssClass="button tooltip" Text="&lt;&lt; Back"
                                    CausesValidation="false" OnClick="btnBack_Click" />
                                <asp:Button ID="lnkSubmitNew" runat="server" CssClass="button" OnClick="lnkSubmitNew_Click"
                                    Text="Submit New Scores" />
                                <asp:Button ID="lnkCancel" runat="server" CssClass="button tooltip" Text="Done" CausesValidation="false"
                                    PostBackUrl="~/ui/index.aspx" />
                            </p>
                        </fieldset>
                    </asp:Panel>
                </div>
                <div>
                    <asp:HiddenField ID="hdnUpdate" runat="server" Value="0" />
                    <asp:HiddenField ID="hdnSubmissionId" runat="server" />
                    <asp:HiddenField ID="hdnAreaId" runat="server" />
                    <asp:HiddenField ID="hdnValueId" runat="server" />
                    <asp:HiddenField ID="hdnStatusId" runat="server" />
                    <asp:ValidationSummary ID="vdS" runat="server" HeaderText="The following fields are required:"
                        ShowMessageBox="True" ShowSummary="False" ValidationGroup="Form" />
                </div>
            </asp:View>
        </asp:MultiView>
        <asp:UpdateProgress ID="UpdateProgressCapture" runat="server" AssociatedUpdatePanelID="UpdatePanelCapture">
            <ProgressTemplate>
                <img runat="server" src="~/ui/images/icons/activity.gif" alt="Please wait..." id="imgPrg" />
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
<telerik:RadScriptBlock ID="RadScriptBlockRadUpload" runat="server">
    <script type="text/javascript">
          //<![CDATA[

        var fileList = null,
            fileListUL = null;

        function fileUploaded(sender, args) {
            var name = args.get_fileName(),
                         li = document.createElement("li");

            if (fileList == null) {
                fileList = document.getElementById("exFileList");
                fileListUL = document.createElement("ul");
                fileList.appendChild(fileListUL);

                fileList.style.display = "block";
            }

            li.innerHTML = name;
            fileListUL.appendChild(li);
        }
          //]]>
    </script>
</telerik:RadScriptBlock>
