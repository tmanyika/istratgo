﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="scorecardarea.ascx.cs"
    Inherits="scorecard.ui.controls.scorecardarea" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:UpdatePanel ID="UpdatePanelArea" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <h1>
            Manage Score Card Areas of Measure
        </h1>
        <div>
            <asp:ListView ID="lvArea" runat="server" OnItemCommand="lvArea_ItemCommand" OnItemEditing="lvArea_ItemEditing"
                OnPagePropertiesChanged="lvArea_PagePropertiesChanged" OnItemCanceling="lvAreas_ItemCanceling"
                OnItemDeleting="lvAreas_ItemDeleting" OnItemUpdating="lvAreas_ItemUpdating">
                <LayoutTemplate>
                    <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                        <thead>
                            <tr id="trFirst" runat="server">
                                <td style="width: 200px">
                                    Area Name
                                </td>
                                <td style="width: 280px; text-align: left;">
                                    Area Name Alias
                                </td>
                                <td style="width: 300px; text-align: left;">
                                    Description
                                </td>
                                <td style="width: 90px">
                                    Active
                                </td>
                                <td width="80px">
                                    Edit
                                </td>
                                <td width="80px">
                                    Delete
                                </td>
                            </tr>
                            <tr id="ItemPlaceHolder" runat="server">
                            </tr>
                        </thead>
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <tbody>
                        <tr class="odd">
                            <td>
                                <%# Eval("AreaName")%>
                                <asp:HiddenField ID="HdnAreaId" runat="server" Value='<%# Bind("AreaId")%>' />
                            </td>
                            <td>
                                <%# Eval("AreaAliasName")%>
                            </td>
                            <td>
                                <%# Eval("AreaDescription")%>
                            </td>
                            <td>
                                <asp:CheckBox ID="chkActive" runat="server" Checked='<%# Eval("Active")%>' />
                            </td>
                            <td>
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="lnkEdit" CommandName="edit" class="ui-state-default ui-corner-all"
                                        title="Edit Record"><span class="ui-icon ui-icon-pencil">
                                    </span></asp:LinkButton>
                                </ul>
                            </td>
                            <td>
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="lnkDelete" CommandName="delete" class="ui-state-default ui-corner-all"
                                        title="Delete Record" OnClientClick="return confirmDelete();"><span class="ui-icon ui-icon-trash">
                                    </span>
                                    </asp:LinkButton>
                                </ul>
                            </td>
                        </tr>
                    </tbody>
                </ItemTemplate>
                <EditItemTemplate>
                    <tbody>
                        <tr class="odd">
                            <td align="left" valign="top">
                                <asp:TextBox runat="server" ID="txtEditName" Text='<%# Eval("AreaName")%>' ValidationGroup="Edit"
                                    Width="180px" />
                                <asp:RequiredFieldValidator ID="RqdArea" runat="server" ErrorMessage="*" ValidationGroup="Edit"
                                    ControlToValidate="txtEditName" ForeColor="Red"></asp:RequiredFieldValidator>
                                <asp:HiddenField ID="HdnAreaId" runat="server" Value='<%# Bind("AreaId")%>' />
                            </td>
                            <td align="left" valign="top">
                                <asp:TextBox runat="server" ID="txtEditAliasName" Text='<%# Eval("AreaAliasName")%>'
                                    ValidationGroup="Edit" Width="260px" />
                                <asp:RequiredFieldValidator ID="RqdAliasName" runat="server" ErrorMessage="*" ValidationGroup="Edit"
                                    ControlToValidate="txtEditAliasName" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                            <td align="left" valign="top">
                                <asp:TextBox runat="server" ID="txtEditDescription" Text='<%# Eval("AreaDescription")%>'
                                    Width="260px" />
                            </td>
                            <td align="left" valign="top">
                                <asp:CheckBox ID="chkActive" runat="server" Checked='<%# Eval("Active")%>' />
                            </td>
                            <td valign="top">
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="lnkSave" CommandName="update" class="ui-state-default ui-corner-all"
                                        title="Update Record" ValidationGroup="Edit"><span class="ui-icon ui-icon-disk">
                                     </span>
                                    </asp:LinkButton>
                                </ul>
                            </td>
                            <td valign="top">
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="lnkCancelUpdate" CommandName="cancel" class="ui-state-default ui-corner-all"
                                        title="Cancel Update" CausesValidation="false"><span class="ui-icon ui-icon-cancel">
                                    </span>
                                    </asp:LinkButton>
                                </ul>
                            </td>
                        </tr>
                    </tbody>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <tbody>
                        <tr>
                            <td align="left" valign="top">
                                <asp:TextBox runat="server" ID="txtName" Text='<%# Eval("AreaName")%>' ValidationGroup="Insert"
                                    Width="180px" />
                                <asp:RequiredFieldValidator ID="RqdArea" runat="server" ErrorMessage="*" ValidationGroup="Insert"
                                    ControlToValidate="txtName" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                            <td align="left" valign="top">
                                <asp:TextBox runat="server" ID="txtAliasName" Text='<%# Eval("AreaAliasName")%>'
                                    ValidationGroup="Insert" Width="260px" />
                                <asp:RequiredFieldValidator ID="RqdAlias" runat="server" ErrorMessage="*" ValidationGroup="Insert"
                                    ControlToValidate="txtAliasName" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                            <td align="left" valign="top">
                                <asp:TextBox runat="server" ID="txtDescription" Text='<%# Eval("AreaDescription")%>'
                                    ValidationGroup="Insert" Width="260px" />
                            </td>
                            <td valign="top">
                                <asp:CheckBox ID="chkActive" runat="server" Checked="true" Enabled="false" />
                            </td>
                            <td valign="top">
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="lnkSave" class="ui-state-default ui-corner-all"
                                        title="Save this Record" CommandName="add" ValidationGroup="Insert"><span class="ui-icon ui-icon-disk">
                        </span></asp:LinkButton>
                                </ul>
                            </td>
                            <td valign="top">
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="lnkCancel" class="ui-state-default ui-corner-all"
                                        title="Cancel Save " CommandName="cancel" CausesValidation="false"><span class="ui-icon ui-icon-cancel">
                        </span></asp:LinkButton>
                                </ul>
                            </td>
                        </tr>
                    </tbody>
                </InsertItemTemplate>
                <EmptyDataTemplate>
                    <span style="color: Red">There are no records to display.</span>
                </EmptyDataTemplate>
            </asp:ListView>
            <br />
            <asp:DataPager ID="dtPagerlvAreas" runat="server" PagedControlID="lvArea" PageSize="15">
                <Fields>
                    <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True" ShowNextPageButton="False"
                        ShowPreviousPageButton="False" />
                    <asp:NumericPagerField />
                    <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True" ShowNextPageButton="False"
                        ShowPreviousPageButton="False" />
                </Fields>
            </asp:DataPager>
            <div>
                <span style="color: Red">
                    <asp:Literal ID="LblError" runat="server" /></span>
            </div>
            <div>
                <p>
                    <br />
                    <br />
                    <asp:Label ID="lblMsg" runat="server" Style="color: #FF3300" />
                    <br />
                    <br />
                </p>
            </div>
            <div id="tabs-3">
                <p>
                    <asp:Button ID="btnNew" runat="server" CssClass="button" Text="Add New Competency"
                        OnClick="btnNew_Click" CausesValidation="false" />
                    <asp:Button ID="lnkCancel0" runat="server" CssClass="button" Text="Done" CausesValidation="false"
                        PostBackUrl="~/ui/index.aspx" />
                </p>
            </div>
            <asp:UpdateProgress ID="UpdateProgressArea" runat="server" AssociatedUpdatePanelID="UpdatePanelArea">
                <ProgressTemplate>
                    <div>
                        <asp:Image ID="imgOrg" runat="server" ImageUrl="~/ui/images/icons/activity.gif" AlternateText="Please wait..">
                        </asp:Image></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
