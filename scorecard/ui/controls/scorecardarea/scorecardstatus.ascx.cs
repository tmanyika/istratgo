﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.implementations;
using ScoreCard.Common.Interfaces;
using ScoreCard.Common.Logic;
using ScoreCard.Common.Config;
using ScoreCard.Common.Entities;

namespace scorecard.ui.controls
{
    public partial class scorecardstatus : System.Web.UI.UserControl
    {
        #region Properties

        int CompId
        {
            get { return Util.user.CompanyId; }
        }

        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) InitialisePage();
        }

        #endregion

        #region Utility Methods

        void InitialisePage()
        {
            LoadScoreCardStatus();
        }

        void LoadScoreCardStatus()
        {
            IScoreCardStatus obj = new ScoreCardStatusBL();
            BindControl.BindListView(lstData, obj.GetByStatus(true));
            pager.Visible = (lstData.Items.Count <= pager.PageSize) ? false : true;
        }

        #endregion

        #region Button Event Handling

        protected void lnkAdd_Click(object sender, EventArgs e)
        {
            BindControl.BindLiteral(lblMsg, "");
            lstData.InsertItemPosition = InsertItemPosition.LastItem;
            LoadScoreCardStatus();
        }

        protected void lnkCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("index.aspx", false);
        }

        #endregion

        #region Utility

        public string GetScript(object statusId)
        {
            bool enabled = GetEnabledStatus(statusId);
            return enabled ? "return confirmDelete();" : "return cannotEditOrDelete();";
        }

        public bool GetEnabledStatus(object statusId)
        {
            int idVal = int.Parse(statusId.ToString());
            return (idVal == ScoreCardAreaConfig.ApprovalInProgressId ||
                idVal == ScoreCardAreaConfig.ApprovedId ||
                idVal == ScoreCardAreaConfig.SavedForLaterId ||
                idVal == ScoreCardAreaConfig.SubmittedId) ? false : true;
        }

        public string GetBoolean(object boolVal)
        {
            if (boolVal == DBNull.Value) return "Yes";
            return bool.Parse(boolVal.ToString()) ? "Yes" : "No";
        }

        private void CancelUpdate()
        {
            lstData.EditIndex = -1;
            lstData.InsertItemPosition = InsertItemPosition.None;
            LoadScoreCardStatus();
        }

        void DeleteItem(ListViewCommandEventArgs e)
        {
            int statusId = int.Parse((e.Item.FindControl("hdnStatusId") as HiddenField).Value);
            string updatedBy = Util.user.LoginId;

            IScoreCardStatus obj = new ScoreCardStatusBL();
            bool deleted = obj.Delete(statusId, updatedBy);
            if (deleted) LoadScoreCardStatus();
            string msg = deleted ? Messages.GetDeleteMessage() : Messages.GetDeleteFailedMessage();
            BindControl.BindLiteral(lblMsg, msg);
        }

        void UpdateItem(ListViewCommandEventArgs e)
        {
            try
            {
                int statusId = int.Parse((e.Item.FindControl("hdnStatusIdE") as HiddenField).Value);

                bool active = (e.Item.FindControl("chkActive") as CheckBox).Checked;

                string name = (e.Item.FindControl("txtName") as TextBox).Text;
                string updatedBy = Util.user.LoginId;

                ScoreCardStatus obj = new ScoreCardStatus
                {
                    StatusName = name,
                    StatusId = statusId,
                    CreatedBy = updatedBy,
                    UpdatedBy = updatedBy,
                    Active = active
                };

                IScoreCardStatus objl = new ScoreCardStatusBL();

                bool saved = objl.UpdateScoreCardStatus(obj);
                string msg = saved ? Messages.GetUpdateMessage() : Messages.GetUpdateFailedMessage();

                BindControl.BindLiteral(lblMsg, msg);
                if (saved)
                {
                    lstData.EditIndex = -1;
                    LoadScoreCardStatus();
                }

            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        void AddItem(ListViewCommandEventArgs e)
        {
            try
            {
                bool active = (e.Item.FindControl("chkActivei") as CheckBox).Checked;
                string name = (e.Item.FindControl("txtNamei") as TextBox).Text;
                string updatedBy = Util.user.LoginId;

                ScoreCardStatus obj = new ScoreCardStatus
                {
                    StatusName = name,
                    StatusId = 0,
                    CreatedBy = updatedBy,
                    UpdatedBy = updatedBy,
                    Active = active
                };

                IScoreCardStatus objl = new ScoreCardStatusBL();
                bool saved = objl.AddScoreCardStatus(obj);
                string msg = saved ? Messages.GetUpdateMessage() : Messages.GetUpdateFailedMessage();
                BindControl.BindLiteral(lblMsg, msg);
                if (saved)
                {
                    lstData.InsertItemPosition = InsertItemPosition.None;
                    LoadScoreCardStatus();
                }
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        #endregion

        #region ListView Event Handling Methods including Slide Paging

        protected void lstData_PagePropertiesChanged(object sender, EventArgs e)
        {
            LoadScoreCardStatus();
        }

        protected void lstData_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            try
            {
                lstData.EditIndex = e.NewEditIndex;
                lstData.InsertItemPosition = InsertItemPosition.None;
                LoadScoreCardStatus();
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        protected void lstData_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                BindControl.BindLiteral(lblMsg, "");
                if (e.Item.ItemType == ListViewItemType.InsertItem ||
               e.Item.ItemType == ListViewItemType.DataItem)
                {
                    switch (e.CommandName)
                    {
                        case "AddItem":
                            AddItem(e);
                            break;
                        case "UpdateItem":
                            UpdateItem(e);
                            break;
                        case "DeleteItem":
                            DeleteItem(e);
                            break;
                        case "CancelUpdate":
                            CancelUpdate();
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        #endregion
    }
}