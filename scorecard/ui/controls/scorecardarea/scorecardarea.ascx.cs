﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.controllers;
using scorecard.implementations;
using scorecard.entities;
using Scorecard.Business.Rules;
using System.Text;
using vb = Microsoft.VisualBasic;
using ScoreCard.Common.Interfaces;
using ScoreCard.Common.Logic;
using ThreeSixty.Evaluation;
using ScoreCard.Common.Config;
using ScoreCard.Common.Entities;

namespace scorecard.ui.controls
{
    public partial class scorecardarea : System.Web.UI.UserControl
    {
        IScoreCardArea area;

        public scorecardarea()
        {
            area = new ScoreCardAreaBL();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ResetLabels();
            if (!IsPostBack)
                loadAreas();
        }

        private void ResetLabels()
        {
            lblMsg.Text = "";
            LblError.Text = "";
        }

        void loadAreas()
        {
            try
            {
                var data = area.GetByStatus(true);
                dtPagerlvAreas.Visible = (data.Count > dtPagerlvAreas.PageSize) ? true : false;
                BindControl.BindListView(lvArea, data);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            lvArea.InsertItemPosition = InsertItemPosition.LastItem;
            loadAreas();
        }

        void createNewArea(ListViewCommandEventArgs e)
        {
            try
            {
                string description = (e.Item.FindControl("txtDescription") as TextBox).Text;
                string name = (e.Item.FindControl("txtName") as TextBox).Text;
                string alias = (e.Item.FindControl("txtAliasName") as TextBox).Text;

                if (string.IsNullOrEmpty(alias) || string.IsNullOrEmpty(name))
                {
                    BindControl.BindLiteral(LblError, Messages.GetRequiredFieldsMessage(true, "Name", "Alias"));
                    return;
                }

                bool added = area.AddScoreCardArea(new ScoreCardArea
                {
                    Active = true,
                    AreaAliasName = alias,
                    AreaDescription = description,
                    CreatedBy = Util.user.LoginId,
                    AreaName = name
                });

                if (added)
                {
                    loadReadOnly();
                    loadAreas();
                }
                BindControl.BindLiteral(LblError, added ? Messages.GetSaveMessage() : Messages.GetSaveFailedMessage());
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void loadReadOnly()
        {
            lvArea.EditIndex = -1;
            lvArea.InsertItemPosition = InsertItemPosition.None;
        }

        void updateScoreCardArea(ListViewDataItem e)
        {
            try
            {
                var hdnAreaId = e.FindControl("HdnAreaId") as HiddenField;

                string description = (e.FindControl("txtEditDescription") as TextBox).Text;
                string name = (e.FindControl("txtEditName") as TextBox).Text;
                string alias = (e.FindControl("txtEditAliasName") as TextBox).Text;

                int areaId = int.Parse(hdnAreaId.Value);

                if (string.IsNullOrEmpty(alias) || string.IsNullOrEmpty(name))
                {
                    BindControl.BindLiteral(LblError, Messages.GetRequiredFieldsMessage(true, "Name", "Alias"));
                    return;
                }

                bool updated = area.UpdateScoreCardArea(new ScoreCardArea
                {
                    Active = true,
                    AreaAliasName = alias,
                    AreaDescription = description,
                    AreaId = areaId,
                    AreaName = name,
                    UpdatedBy = Util.user.LoginId
                });

                if (updated)
                {
                    loadReadOnly();
                    loadAreas();
                }
                BindControl.BindLiteral(LblError, updated ? Messages.GetUpdateMessage() : Messages.GetUpdateFailedMessage());
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void deactivateScoreCardArea(ListViewDataItem e)
        {
            try
            {
                var hdnAreaId = e.FindControl("HdnAreaId") as HiddenField;
                if (string.IsNullOrEmpty(hdnAreaId.Value))
                {
                    BindControl.BindLiteral(LblError, Messages.GetDeleteFailedMessage());
                    return;
                }

                int areaId = int.Parse(hdnAreaId.Value);
                bool deactivated = area.SetStatus(new ScoreCardArea
                {
                    Active = false,
                    AreaId = areaId,
                    UpdatedBy = Util.user.LoginId
                });

                if (deactivated)
                {
                    loadReadOnly();
                    loadAreas();
                }
                BindControl.BindLiteral(LblError, deactivated ? Messages.GetDeleteMessage() : Messages.GetDeleteFailedMessage());
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void cancelUpdate()
        {
            loadReadOnly();
            loadAreas();
        }

        protected void lvAreas_ItemCanceling(object sender, ListViewCancelEventArgs e)
        {
            cancelUpdate();
        }

        protected void lvArea_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            LblError.Text = "";
            if (e.Item.ItemType == ListViewItemType.InsertItem ||
                e.Item.ItemType == ListViewItemType.DataItem)
            {
                switch (e.CommandName)
                {
                    case "add":
                        createNewArea(e);
                        break;
                }
            }
        }

        protected void lvAreas_ItemDeleting(object sender, ListViewDeleteEventArgs e)
        {
            ListViewDataItem el = lvArea.Items[e.ItemIndex];
            deactivateScoreCardArea(el);
        }

        protected void lvAreas_ItemUpdating(object sender, ListViewUpdateEventArgs e)
        {
            ListViewDataItem el = lvArea.Items[e.ItemIndex];
            updateScoreCardArea(el);
        }

        protected void lvArea_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            lvArea.EditIndex = e.NewEditIndex;
            lvArea.InsertItemPosition = InsertItemPosition.None;
            loadAreas();
        }

        protected void lvArea_PagePropertiesChanged(object sender, EventArgs e)
        {
            loadAreas();
        }

        protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadAreas();
        }

        protected void lnkCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("index.aspx", true);
        }
    }
}