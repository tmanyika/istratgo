﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.implementations;
using Scorecard.Interfaces;
using Scorecard.Logic;
using Scorecard.Entities;

namespace scorecard.ui.controls
{
    public partial class verifyactivationcode : System.Web.UI.UserControl
    {
        #region Properties

        private readonly ICompanyActivation act;

        public int CompId
        {
            get
            {
                if (Request.QueryString["compId"] != null) 
                {
                    string companyId = Request.QueryString["compId"].ToString().Replace(' ', '+');
                    string decryptedCompId = Util.Decrypt(companyId);
                    return int.Parse(decryptedCompId);
                }
                return 0;
            }
        }

        public string Code
        {
            get
            {
                return (Request.QueryString["code"] != null) ? Request.QueryString["code"].ToString().Trim() : string.Empty;
            }
        }

        #endregion

        #region Default Constructor

        public verifyactivationcode()
        {
            act = new CompanyActivationBL();
        }

        #endregion

        #region Page Load Even Handling Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ActivateAccount();
            }
        }

        #endregion

        #region Databinding Methods

        void ActivateAccount()
        {
            try
            {
                var obj = new CompanyActivation
                {
                    ActivationCode = Code,
                    CompanyId = CompId
                };
                var actInfo = act.GetDetails(obj);
                if (actInfo.Activated && actInfo.CompanyId > 0)
                {
                    mainView.SetActiveView(vwAlreadyActive);
                }
                else if (!actInfo.Activated && actInfo.CompanyId > 0)
                {
                    var activated = act.ActivateAccount(obj);
                    mainView.SetActiveView(activated ? vwSuccess : vwFail);
                }
                else
                {
                    mainView.SetActiveView(vwFail);
                }
            }
            catch (Exception ex)
            {
                mainView.SetActiveView(vwFail);
                Util.LogErrors(ex);
            }
        }

        #endregion
    }
}