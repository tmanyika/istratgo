﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="settings.ascx.cs" Inherits="scorecard.ui.controls.settings" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<h1>
    System Settings
</h1>
<asp:UpdatePanel ID="UpdatePanelBank" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <fieldset runat="server" id="pnlSettings">
            <legend>Application Configuration Settings </legend>
            <asp:ListView ID="lvSettings" runat="server" OnItemCommand="lvSettings_ItemCommand"
                OnPagePropertiesChanged="lvSettings_PagePropertiesChanged">
                <LayoutTemplate>
                    <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                        <thead>
                            <tr id="tblMain" runat="server">
                                <td>
                                    <input type="checkbox" class="checkall" />
                                </td>
                                <td>
                                    ID
                                </td>
                                <td>
                                    Parent Setting
                                </td>
                                <td>
                                    Description
                                </td>
                                <td>
                                    Edit
                                </td>
                                <td>
                                    sub setting
                                </td>
                                <td>
                                    Delete
                                </td>
                            </tr>
                            <tr id="ItemPlaceHolder" runat="server">
                            </tr>
                        </thead>
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <tbody>
                        <tr class="odd">
                            <td>
                                <input type="checkbox" />
                            </td>
                            <td>
                                <asp:Label ID="lblId" runat="server" Text='<%# Eval("TYPE_ID")%>' />
                            </td>
                            <td>
                                <asp:Label ID="lblName" runat="server" Text='<%# Eval("NAME")%>' />
                            </td>
                            <td>
                                <%# Eval("DESCRIPTION")%>
                            </td>
                            <td>
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="imgEditDetails" CommandName="editRecord" AlternateText="Edit Details"
                                        class="ui-state-default ui-corner-all">
                        <span class="ui-icon ui-icon-pencil">
                        </span></asp:LinkButton>
                                </ul>
                            </td>
                            <td>
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="imgViewDetails" CommandName="viewRecord" AlternateText="View Details"
                                        class="ui-state-default ui-corner-all">
                                  <span class="ui-icon ui-icon-pencil">
                        </span>
                                    </asp:LinkButton>
                                </ul>
                            </td>
                            <td>
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="imgDeleteItem" CommandName="deleteRecord" AlternateText="Delete Item"
                                        OnClientClick="return confirmDelete();" class="ui-state-default ui-corner-all"><span class="ui-icon ui-icon-trash">
                        </span></asp:LinkButton>
                                </ul>
                            </td>
                        </tr>
                    </tbody>
                </ItemTemplate>
                <EditItemTemplate>
                    <tbody>
                        <tr class="odd">
                            <td>
                                <input type="checkbox" />
                            </td>
                            <td>
                                <asp:Label ID="lblId" runat="server" Text='<%# Eval("TYPE_ID")%>' />
                            </td>
                            <td>
                                <asp:TextBox ID="txtEditName" runat="server" Text='<%# Eval("NAME")%>' SkinID="smallText" />
                            </td>
                            <td>
                                <asp:TextBox ID="txtEditDescription" runat="server" Text='<%# Eval("DESCRIPTION")%>'
                                    SkinID="smallText" />
                            </td>
                            <td>
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="imgSaveDetails" CommandName="updateRecord" AlternateText="Save Changes"
                                        class="ui-state-default ui-corner-all">
                        <span class="ui-icon ui-icon-disk"></span>
                                    </asp:LinkButton>
                                </ul>
                            </td>
                            <td>
                            </td>
                            <td>
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="imgCancelUpdate" CommandName="cancelUpdate" AlternateText="Cancel Update"
                                        class="ui-state-default ui-corner-all">
                   <span class="ui-icon ui-icon-cancel"></span>
                                    </asp:LinkButton>
                                </ul>
                            </td>
                        </tr>
                    </tbody>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <tbody>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                <asp:TextBox ID="txtName" runat="server" SkinID="smallText"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="txtDescription" runat="server" SkinID="smallText"></asp:TextBox>
                            </td>
                            <td>
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="imgAddDetails" CommandName="addRecord" AlternateText="Add New Record">
                       <span class="ui-icon ui-icon-disk"></span>
                                    </asp:LinkButton>
                                </ul>
                            </td>
                            <td>
                            </td>
                            <td>
                                <ul id="icons">
                                    <asp:LinkButton ID="imgCancelAddRecord" runat="server" AlternateText="Cancel Add New Record"
                                        CommandName="cancelAdd" class="ui-state-default ui-corner-all">
                                  <span class="ui-icon ui-icon-cancel"></span>
                                    </asp:LinkButton>
                                </ul>
                            </td>
                        </tr>
                    </tbody>
                </InsertItemTemplate>
            </asp:ListView>
            <br />
            <asp:DataPager ID="pgerLvSettings" runat="server" PagedControlID="lvSettings">
                <Fields>
                    <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True" ShowNextPageButton="False"
                        ShowPreviousPageButton="False" />
                    <asp:NumericPagerField />
                    <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True" ShowNextPageButton="False"
                        ShowPreviousPageButton="False" />
                </Fields>
            </asp:DataPager>
            <br />
            <br />
            <asp:Button ID="btnNew" runat="server" class="button" Text="Add New Setting" OnClick="btnAddRecord_Click" />
            &nbsp;<asp:LinkButton ID="lnkCancel0" runat="server" class="button tooltip" title="Save Scores"
                OnClick="lnkCancel_Click" CausesValidation="false"> Done </asp:LinkButton>
        </fieldset>
        <asp:Panel ID="pnlChildSettings" runat="server" CssClass="modalPopup">
            <fieldset id="fchildSettings">
                <legend id="childSettingHeaderText" runat="server">Child Settings </legend>
                <asp:Label ID="lblParentID" runat="server" Visible="false" />
                <asp:ListView ID="lvChildSettings" runat="server" OnItemCommand="lvChildSettings_ItemCommand"
                    OnPagePropertiesChanged="lvChildSettings_PagePropertiesChanged">
                    <LayoutTemplate>
                        <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                            <thead>
                                <tr id="tblMain" runat="server">
                                    <td>
                                        <input type="checkbox" class="checkall" />
                                    </td>
                                    <td>
                                        ID
                                    </td>
                                    <td>
                                        Name
                                    </td>
                                    <td>
                                        Description
                                    </td>
                                    <td>
                                        Edit
                                    </td>
                                    <td>
                                        Delete
                                    </td>
                                </tr>
                                <tr id="ItemPlaceHolder" runat="server">
                                </tr>
                            </thead>
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tbody>
                            <tr class="odd">
                                <td>
                                    <input type="checkbox" />
                                </td>
                                <td>
                                    <asp:Label ID="lblId" runat="server" Text='<%# Eval("TYPE_ID")%>' />
                                </td>
                                <td>
                                    <%# Eval("NAME")%>
                                </td>
                                <td>
                                    <%# Eval("DESCRIPTION")%>
                                </td>
                                <td>
                                    <ul id="icons">
                                        <asp:LinkButton runat="server" ID="imgEditDetails" CommandName="editRecord" AlternateText="Edit Details"
                                            class="ui-state-default ui-corner-all">
                             <span class="ui-icon ui-icon-pencil">
                        </span>
                                        </asp:LinkButton>
                                    </ul>
                                </td>
                                <td>
                                    <ul id="icons">
                                        <asp:LinkButton runat="server" ID="imgDeleteItem" CommandName="deleteRecord" AlternateText="Delete Item"
                                            OnClientClick="return confirmDelete();" class="ui-state-default ui-corner-all">
<span class="ui-icon ui-icon-trash">
                        </span>
                                        </asp:LinkButton>
                                    </ul>
                                </td>
                            </tr>
                        </tbody>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <tbody>
                            <tr class="odd">
                                <td>
                                    <input type="checkbox" />
                                </td>
                                <td>
                                    <asp:Label ID="lblId" runat="server" Text='<%# Eval("TYPE_ID")%>' />
                                </td>
                                <td>
                                    <asp:TextBox ID="txtEditName" runat="server" Text='<%# Eval("NAME")%>' SkinID="smallText" />
                                </td>
                                <td>
                                    <asp:TextBox ID="txtEditDescription" runat="server" Text='<%# Eval("DESCRIPTION")%>'
                                        SkinID="smallText" />
                                </td>
                                <td>
                                    <ul id="icons">
                                        <asp:LinkButton runat="server" ID="imgSaveDetails" CommandName="updateRecord" AlternateText="Save Changes"
                                            class="ui-state-default ui-corner-all">
                            <span class="ui-icon ui-icon-disk"></span>
                                        </asp:LinkButton>
                                    </ul>
                                </td>
                                <td>
                                    <ul id="icons">
                                        <asp:LinkButton runat="server" ID="imgCancelUpdate" CommandName="cancelUpdate" AlternateText="Cancel Update"
                                            class="ui-state-default ui-corner-all">
                         <span class="ui-icon ui-icon-cancel"></span>
                                        </asp:LinkButton>
                                    </ul>
                                </td>
                            </tr>
                        </tbody>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <tbody>
                            <tr>
                                <td>
                                </td>
                                 <td>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtName" runat="server" SkinID="smallText"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtDescription" runat="server" SkinID="smallText"></asp:TextBox>
                                </td>
                                <td>
                                    <ul id="icons">
                                        <asp:LinkButton runat="server" ID="imgAddDetails" CommandName="addRecord" AlternateText="Add New Record"
                                            class="ui-state-default ui-corner-all">
                             <span class="ui-icon ui-icon-disk"></span>
                                        </asp:LinkButton>
                                    </ul>
                                </td>
                                <td>
                                </td>
                            </tr>
                        </tbody>
                    </InsertItemTemplate>
                </asp:ListView>
                <br />
                <asp:DataPager ID="pgrLvChildSettings" runat="server" PagedControlID="lvChildSettings">
                    <Fields>
                        <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True" ShowNextPageButton="False"
                            ShowPreviousPageButton="False" />
                        <asp:NumericPagerField />
                        <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True" ShowNextPageButton="False"
                            ShowPreviousPageButton="False" />
                    </Fields>
                </asp:DataPager>
                <br />
                <br />
                <asp:Button ID="btnAddChildRecord" runat="server" class="button" Text="Add New Record"
                    OnClick="btnAddChildRecord_Click" />
                <asp:Button ID="btnClose" runat="server" class="button" Text="Close" OnClick="btnClose_Click" />
            </fieldset>
        </asp:Panel>
        <asp:Button ID="btnChildSettings" runat="server" Style="display: none" />
        <ajaxToolkit:ModalPopupExtender ID="mdlShowChildSettings" runat="server" TargetControlID="btnChildSettings"
            PopupControlID="pnlChildSettings" BackgroundCssClass="modalBackground" />
        <asp:UpdateProgress ID="UpdateProgressBank" runat="server" AssociatedUpdatePanelID="UpdatePanelBank">
            <ProgressTemplate>
                <img runat="server" src="~/ui/images/icons/activity.gif" alt="Please wait..." id="imgPrg" />
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
