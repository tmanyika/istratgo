﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HR.Incentives.Interfaces;
using HR.Incentives.Logic;
using scorecard.implementations;
using scorecard.interfaces;

namespace scorecard.ui.reports
{
    public partial class incentiveReportForm : System.Web.UI.UserControl
    {
        #region  Variables

        IIncentive inc;
        ISalaryType sal;
        IIncentiveReport rep;
        Iorgunit org;

        #endregion

        #region Default Class Constructors

        public incentiveReportForm()
        {
            inc = new IncentiveBL();
            sal = new SalaryTypeBL();
            rep = new IncentiveReportBL();
            org = new structureImpl();           
        }

        #endregion

        #region Page Events Handling Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindOrgUnits();
                BindSalaryTypes();
                BindIncentives(null);
            }
        }

        #endregion

        #region Databinding Methods

        void BindOrgUnits()
        {
            int orgUnitId = Util.user.OrgId;
            var data = org.getOrgStructureInHirachy(orgUnitId);

            bool isAdmin = Util.user.UserTypeId == Util.getTypeDefinitionsUserTypeAdministrator();
            string selectedVal = isAdmin ? string.Empty : orgUnitId.ToString();

            BindControl.BindDropdown(ddOrgUnit, "ORGUNIT_NAME", "ID", "- Company Level -", "-1", selectedVal, data);
            ddOrgUnit.Enabled = isAdmin;
        }

        void BindSalaryTypes()
        {
            int compId = Util.user.CompanyId;
            var data = sal.GetByStatus(compId, true);
            BindControl.BindDropdown(ddSalyType, data, "- select salary type -", "-1");
        }

        void BindIncentives(int? orgUnitId)
        {
            int compId = Util.user.CompanyId;
            var data = (orgUnitId != null) ? inc.GetByOrgUnitId(true, compId, (int)orgUnitId) :
                                            inc.GetByStatus(compId, orgUnitId, true);
            if (data.Count <= 0)
            {
                BindControl.BindLiteral(lblMsg, "You need to setup Incentives for the selected organisation unit.");
            }

            BindControl.BindDropdown(ddIncentive, data, "- select incentive type -", "-1");
        }

        void ShowMessage(string msg)
        {
            BindControl.BindLiteral(lblMsg, msg);
        }

        bool IsValid()
        {
            bool isValid = false;
            try
            {
                if (string.IsNullOrEmpty(txtEndDate.Text) || string.IsNullOrEmpty(txtStartDate.Text))
                {
                    isValid = false;
                    ShowMessage(Messages.GetRequiredFieldsMessage(true, string.IsNullOrEmpty(txtStartDate.Text) ? "Start Date" : string.Empty,
                       string.IsNullOrEmpty(txtEndDate.Text) ? "End Date" : string.Empty, ddSalyType.SelectedIndex == 0 ? "Select Salary Type" : string.Empty));
                }
                else
                {
                    if (!string.IsNullOrEmpty(txtEndDate.Text) && !string.IsNullOrEmpty(txtStartDate.Text))
                    {
                        string tempStartDate = Common.GetInternationalDateFormat(txtStartDate.Text, DateFormat.monthDayYear);
                        string tempEndDate = Common.GetInternationalDateFormat(txtEndDate.Text, DateFormat.monthDayYear);
                        if (DateTime.Parse(tempStartDate) > DateTime.Parse(tempEndDate))
                        {
                            ShowMessage("Start Date cannot be greater than End Date.");
                            isValid = false;
                        }
                        else
                        {
                            isValid = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessage(Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }

            return isValid;
        }

        void DownloadReport()
        {
            int compId = Util.user.CompanyId;

            string startDate = Common.GetInternationalDateFormat(txtStartDate.Text.Trim(), DateFormat.monthDayYear);
            string endDate = Common.GetInternationalDateFormat(txtEndDate.Text.Trim(), DateFormat.monthDayYear);

            string orgUnitId = ddOrgUnit.SelectedValue;
            string orgUnitName = (ddOrgUnit.SelectedIndex == 0) ? "Company Level" : HttpUtility.UrlEncode(ddOrgUnit.SelectedItem.Text);

            string salaryTypeId = ddSalyType.SelectedValue;
            string salaryTypeName = HttpUtility.UrlEncode(ddSalyType.SelectedItem.Text);
            string incentiveId = ddIncentive.SelectedValue;
            string incentiveName = HttpUtility.UrlEncode(ddIncentive.SelectedItem.Text);

            string script = string.Format("openNewWindow('incentivereportpreview.aspx?sid={0}&iid={1}&dsVal={2}&deVal={3}&orgunitId={4}&compId={5}&salname={6}&orgunitname={7}&incname={8}');", salaryTypeId,
                                            incentiveId, startDate, endDate, orgUnitId, compId, salaryTypeName, orgUnitName, incentiveName);
            AjaxScriptManager.RegisterStartupScript(Page, typeof(Page), "viewIncReport", script, true);
        }

        #endregion

        #region Button Event Handling Methods

        protected void btnViewReport_Click(object sender, EventArgs e)
        {
            bool valid = IsValid();
            if (valid)
            {
                DownloadReport();
            }
        }

        #endregion

        #region Dropdown Event Handling Methods

        protected void ddOrgUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            int? orgUnitId = null;
            if (ddOrgUnit.SelectedIndex != 0)
            {
                orgUnitId = int.Parse(ddOrgUnit.SelectedValue);
            }

            BindIncentives(orgUnitId);
        }

        #endregion
    }
}