﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.implementations;
using scorecard.controllers;
using HR.Human.Resources;
using System.Net.Mail;
using System.Email.Communication;

namespace scorecard.ui.controls
{
    public partial class changePassword : System.Web.UI.UserControl
    {
        #region Property

        public bool IsAccountActivation
        {
            get;
            set;
        }

        #endregion

        #region Page Event Handling Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (IsAccountActivation)
                {
                    Util.CheckSessionState();
                }

                mView.SetActiveView(vwForm);
            }
        }

        #endregion

        #region Utility Methods

        private void updateUserPassword()
        {
            try
            {
                string oldPassword = Util.HashPassword(txtOldPassword.Value.Trim());
                string newPass = Util.HashPassword(txtPassword.Value.Trim());
                string userName = Util.user.LoginId;

                IEmployee obj = new EmployeeBL();
                Employee user = obj.GetByUserName(userName);

                bool oldPinRight = !string.IsNullOrEmpty(user.UserName) ? string.CompareOrdinal(user.Pass, oldPassword) == 0 ? true : false : false;
                if (oldPinRight)
                {
                    user.Pass = newPass;
                    user.UpdatedBy = user.UserName;
                    bool updated = obj.UpdateLogin(user);
                    if (updated)
                    {
                        lblMsg.Text = "Password was updated successfully.";
                        mView.SetActiveView(vwMsg);
                        if (IsAccountActivation)
                        {
                            SendNewRegistrationEmail(user);
                            Response.Redirect("updatecompanyinfo.aspx", true);
                        }
                    }
                    else
                    {
                        mView.SetActiveView(vwForm);
                        lblError.Text = "The system failed to update the password. Please try again.";
                    }
                }
                else lblError.Text = "The old password you supplied is incorrect.";
            }
            catch (Exception e)
            {
                lblError.Text = Messages.GetErrorMessage();
                Util.LogErrors(e);
            }
        }

        void SendNewRegistrationEmail(Employee user)
        {
            try
            {
                string result = string.Empty;
                int emailId = Util.getRegistrationSuccessfulEmailId;

                MailAddressCollection addy = Util.RegistrationEmail;

                object[] mydata = { user.FullName, user.EmailAddress, user.Organisation.ORGUNIT_NAME };

                IMailRelay objm = new MailRelayBL();
                MailRelay obj = new MailRelay
                {
                    MailId = emailId,
                    Recipient = addy,
                    Data = mydata
                };
                objm.SendMail(obj, out result);
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                BindControl.BindLiteral(lblError, Messages.GetErrorMessage());
            }
        }

        #endregion

        #region Button Event Handling Methods

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            updateUserPassword();
        }

        #endregion
    }
}