﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="perspectives.ascx.cs"
    Inherits="scorecard.ui.controls.perspectives" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<h1>
    Key Focus Areas Management</h1>
<asp:UpdatePanel ID="UpdatePanelPers" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div>
            <asp:ListView ID="lstPerspective" runat="server" OnPagePropertiesChanged="lstPerspective_PagePropertiesChanged"
                OnItemEditing="lstPerspective_ItemEditing" OnItemCommand="lstPerspective_ItemCommand">
                <LayoutTemplate>
                    <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                        <thead>
                            <tr>
                                <td>
                                    Key Focus Area
                                </td>
                                <td>
                                    Description
                                </td>
                                <td>
                                    Active
                                </td>
                                <td>
                                    Edit
                                </td>
                                <td>
                                    Delete
                                </td>
                            </tr>
                            <tr id="itemPlaceholder" runat="server">
                            </tr>
                        </thead>
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <tbody>
                        <tr class="odd">
                            <td valign="top">
                                <asp:HiddenField ID="hdnperspectiveId" runat="server" Value='<%# Eval("perspective_Id") %>' />
                                <asp:Literal ID="lblName" runat="server" Text='<%# Eval("perspective_name")%>'></asp:Literal>
                            </td>
                            <td valign="top">
                                <%# Eval("perspective_desc") %>
                            </td>
                            <td>
                                <asp:Literal ID="lblActive" runat="server" Text='<%# GetBoolean(Eval("active")) %>' />
                            </td>
                            <td>
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="lnkEdit" CommandName="Edit" class="ui-state-default ui-corner-all"
                                        title="Edit Record" CausesValidation="false"><span class="ui-icon ui-icon-pencil">
                        </span></asp:LinkButton></ul>
                            </td>
                            <td>
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="lnkDelete" CommandName="deleteItem" class="ui-state-default ui-corner-all"
                                        title="Delete Record" OnClientClick="return confirmDelete();" CausesValidation="false"><span class="ui-icon ui-icon-trash">
                        </span></asp:LinkButton></ul>
                            </td>
                        </tr>
                    </tbody>
                </ItemTemplate>
                <EditItemTemplate>
                    <tbody>
                        <tr class="odd">
                            <td valign="top">
                                <asp:TextBox runat="server" ID="txtName" Text='<%# Eval("perspective_name")%>' ValidationGroup="Edit" />
                                <asp:RequiredFieldValidator ID="RqdNameE" runat="server" ErrorMessage="* - required"
                                    ValidationGroup="Edit" ControlToValidate="txtName" ForeColor="Red"></asp:RequiredFieldValidator>
                                <asp:HiddenField ID="hdnperspectiveIdE" runat="server" Value='<%# Eval("perspective_Id") %>' />
                            </td>
                            <td valign="top">
                                <asp:TextBox runat="server" ID="txtDesc" Text='<%# Eval("perspective_desc")%>' Width="500px"/>
                            </td>
                            <td>
                                <asp:CheckBox ID="chkActive" runat="server" Checked='<%# Eval("active") %>' />
                            </td>
                            <td>
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="lnkUpdate" CommandName="updatePerspective" class="ui-state-default ui-corner-all"
                                        title="Update Record" ValidationGroup="Edit"><span class="ui-icon ui-icon-disk"></span></asp:LinkButton>
                                </ul>
                            </td>
                            <td>
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="lnkCancelUpdate" CommandName="cancelUpdate" class="ui-state-default ui-corner-all"
                                        title="Cancel Update"><span class="ui-icon ui-icon-cancel"></span></asp:LinkButton>
                                </ul>
                            </td>
                        </tr>
                    </tbody>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <tbody>
                        <tr class="odd">
                            <td valign="top">
                                <asp:TextBox runat="server" ID="txtNamei" ValidationGroup="Insert" />
                                <asp:HiddenField ID="hdnperspectiveIdI" runat="server" />
                                <asp:RequiredFieldValidator ID="RqdNamei" runat="server" ErrorMessage="* - required"
                                    ValidationGroup="Insert" ControlToValidate="txtNamei" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                            <td valign="top">
                                <asp:TextBox runat="server" ID="txtDesci" Width="500px" />
                            </td>
                            <td>
                                <asp:CheckBox ID="chkActivei" runat="server" Checked="True" />
                            </td>
                            <td>
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="lnkSave" CommandName="addPerspective" ValidationGroup="Insert"
                                        class="ui-state-default ui-corner-all" title="Update Record"><span class="ui-icon ui-icon-disk"></span></asp:LinkButton>
                                </ul>
                            </td>
                            <td>
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="lnkCancelUpdate" CommandName="cancelUpdate" class="ui-state-default ui-corner-all"
                                        title="Cancel Update"><span class="ui-icon ui-icon-cancel"></span></asp:LinkButton>
                                </ul>
                            </td>
                        </tr>
                    </tbody>
                </InsertItemTemplate>
                <EmptyDataTemplate>
                    <p class="errorMsg">
                        There are no records to display.
                    </p>
                </EmptyDataTemplate>
            </asp:ListView>
            <br />
            <asp:DataPager ID="pager" runat="server" PagedControlID="lstPerspective" PageSize="10"
                OnPreRender="pager_PreRender">
                <Fields>
                    <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True" ShowNextPageButton="False"
                        ShowPreviousPageButton="False" />
                    <asp:NumericPagerField />
                    <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True" ShowNextPageButton="False"
                        ShowPreviousPageButton="False" />
                </Fields>
            </asp:DataPager>
            <br />
            <br />
            <p class="errorMsg">
                <asp:Literal ID="lblMsg" runat="server"></asp:Literal></p>
        </div>
        <div id="tabs-3">
            <p>
                <asp:Button ID="btnNew" runat="server" class="button" Text="Add New Key Focus Area"
                    OnClick="lnkAdd_Click" CausesValidation="False" />
                <asp:Button ID="lnkCancel0" runat="server" class="button" Text="Done" OnClick="lnkCancel_Click"
                    CausesValidation="false" />
            </p>
        </div>
        <asp:UpdateProgress ID="UpdateProgressPers" runat="server" AssociatedUpdatePanelID="UpdatePanelPers">
            <ProgressTemplate>
                <div>
                    <asp:Image ID="imgProj" runat="server" ImageUrl="~/ui/images/icons/activity.gif"
                        AlternateText="Please wait.."></asp:Image></div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
