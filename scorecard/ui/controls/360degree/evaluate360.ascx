﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="evaluate360.ascx.cs"
    Inherits="scorecard.ui.controls._360degree.evaluate360" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:UpdatePanel ID="UpdatePanelEvaluate" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div>
            <h1>
                Evaluate a 360<sup>0</sup> Evaluation</h1>
        </div>
        <asp:MultiView ID="MainView" runat="server">
            <asp:View ID="VwData" runat="server">
                <asp:ListView ID="lvData" runat="server" OnItemCommand="lvData_ItemCommand">
                    <LayoutTemplate>
                        <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                            <thead>
                                <tr id="trhead" runat="server">
                                    <td>
                                        Criteria
                                    </td>
                                    <td>
                                        Name
                                    </td>
                                    <td>
                                        Period
                                    </td>
                                    <td>
                                        Date Submitted
                                    </td>
                                    <td>
                                        Evaluate
                                    </td>
                                </tr>
                                <tr id="ItemPlaceHolder" runat="server">
                                </tr>
                            </thead>
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tbody>
                            <tr class="odd">
                                <td>
                                    <asp:Literal ID="lblAreaName" runat="server" Text='<%# Eval("Area.AreaAliasName") %>' />
                                    <asp:HiddenField ID="hdnAreaId" runat="server" Value='<%# Bind("AreaId") %>' />
                                    <asp:HiddenField ID="hdnAreaItemId" runat="server" Value='<%# Bind("AreaItemId") %>' />
                                    <asp:HiddenField ID="hdnSubmissionId" runat="server" Value='<%# Bind("SubmissionId") %>' />
                                    <asp:HiddenField ID="hdnMaximumRateValue" runat="server" Value='<%# Bind("MaximumRateValue") %>' />
                                    <asp:HiddenField ID="hdnEvaluatorId" runat="server" Value='<%# Bind("Evaluator.EvaluatorId") %>' />
                                </td>
                                <td>
                                    <asp:Literal ID="lblAreaItemName" runat="server" Text='<%# Eval("Item.AreaItemName")%>' />
                                </td>
                                <td>
                                    <asp:Literal ID="lblPeriodDate" runat="server" Text='<%# String.Format("{0:dd/MM/yyyy}", Eval("PeriodDate"))%>' />
                                </td>
                                <td>
                                    <asp:Literal ID="lblDateSentOut" runat="server" Text='<%# String.Format("{0:dd/MM/yyyy}", Eval("DateSentOut"))%>' />
                                </td>
                                <td>
                                    <ul id="icons">
                                        <asp:LinkButton runat="server" ID="lnkView" CommandName="Evaluate" class="ui-state-default ui-corner-all"
                                            title="Evaluate" CausesValidation="false"><span class="ui-icon ui-icon-pencil">
                        </span></asp:LinkButton>
                                    </ul>
                                </td>
                            </tr>
                        </tbody>
                    </ItemTemplate>
                    <EmptyItemTemplate>
                        <p style="color: Red">
                            There are no pending evaluations to display.</p>
                    </EmptyItemTemplate>
                </asp:ListView>
                <div>
                    <p style="color: Red">
                        <asp:Literal ID="lblMsg" runat="server"></asp:Literal></p>
                </div>
            </asp:View>
            <asp:View ID="VwEvaluate" runat="server">
                <div>
                    <fieldset runat="server" id="pnlScoresInfo">
                        <legend>360 Evaluation For</legend>
                        <p>
                            <label for="sf">
                                Area :
                            </label>
                            &nbsp;<span class="field_desc" style="font-style: normal;"><b><asp:Literal ID="lblAreaName"
                                runat="server"></asp:Literal></b>
                                <asp:HiddenField ID="hdnAreaId" runat="server" />
                                <asp:HiddenField ID="hdnAreaItemId" runat="server" />
                                <asp:HiddenField ID="hdnSubmissionId" runat="server" />
                                <asp:HiddenField ID="hdnEvaluatorId" runat="server" />
                                <asp:HiddenField ID="hdnMaximumRateValue" runat="server" />
                            </span>
                        </p>
                        <br />
                        <br />
                        <p>
                            <label for="sf">
                                Name :
                            </label>
                            &nbsp;<span class="field_desc" style="font-style: normal;"><b><asp:Literal ID="lblAreaItemName"
                                runat="server"></asp:Literal></b> </span>
                        </p>
                        <br />
                        <br />
                        <p>
                            <label for="sf">
                                Period :
                            </label>
                            &nbsp;<span class="field_desc" style="font-style: normal;"><b><asp:Literal ID="lblPeriod"
                                runat="server"></asp:Literal></b> </span>
                        </p>
                    </fieldset>
                </div>
                <div>
                    <br />
                    <p>
                        Please evaluate <strong style="color: Black">
                            <asp:Literal ID="lblName" runat="server"></asp:Literal></strong> based on the
                        following competencies (<strong style="color: Black">1</strong> being <strong style="color: Black">
                            <asp:Literal ID="lblLowRatingHeader" runat="server" /></strong> and <strong style="color: Black">
                                <asp:Literal ID="lblMaxRating" runat="server"></asp:Literal></strong> being
                        <strong style="color: Black">
                            <asp:Literal ID="lblMaxRatingHeader" runat="server"></asp:Literal></strong>)
                    </p>
                    <br />
                </div>
                <div>
                    <asp:GridView ID="gdvData" runat="server" CssClass="fullwidth" CellPadding="0" CellSpacing="0"
                        AutoGenerateColumns="False" AllowPaging="false" OnRowDataBound="gdvData_RowDataBound"
                        DataKeyNames="ScoreId,EvaluationId,RatingValue,WeightVal">
                        <Columns>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <div class="competency">
                                        Competency
                                    </div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%# Eval("Competency.CompetencyName")%>
                                    <asp:HiddenField ID="hdnRatingValue" runat="server" Value='<%# Bind("RatingValue")%>' />
                                    <asp:HiddenField ID="hdnWeightValue" runat="server" Value='<%# Bind("WeightVal")%>' />
                                    <asp:HiddenField ID="hdnEvaluationId" runat="server" Value='<%# Bind("EvaluationId")%>' />
                                    <asp:HiddenField ID="hdnScoreId" runat="server" Value='<%# Bind("ScoreId")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <div class="competency">
                                        Description
                                    </div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%# Eval("Competency.CompetencyDescription")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <div class="rating">
                                        1
                                    </div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:RadioButton ID="Rd1" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <div class="rating">
                                        2
                                    </div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:RadioButton ID="Rd2" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <div class="rating">
                                        3
                                    </div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:RadioButton ID="Rd3" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <div class="rating">
                                        4
                                    </div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:RadioButton ID="Rd4" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <div class="rating">
                                        5
                                    </div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:RadioButton ID="Rd5" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <div class="rating">
                                        6
                                    </div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:RadioButton ID="Rd6" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <div class="rating">
                                        7
                                    </div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:RadioButton ID="Rd7" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <div class="rating">
                                        8
                                    </div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:RadioButton ID="Rd8" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <div class="rating">
                                        9
                                    </div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:RadioButton ID="Rd9" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <div class="rating">
                                        10
                                    </div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:RadioButton ID="Rd10" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                <div>
                    <p>
                        <label for="sf">
                            <b>Overall Comment</b> :
                        </label>
                        &nbsp;<span class="field_desc"><asp:TextBox ID="txtOveralComment" 
                            runat="server" Width="70%"></asp:TextBox></span>
                    </p>
                </div>
                <div>
                    <p style="color: Red">
                        <br />
                        <asp:Literal ID="lblError" runat="server"></asp:Literal><br />
                        <br />
                    </p>
                </div>
                <div>
                    <asp:Button ID="btnSubmit" runat="server" class="button" Text="Submit Evaluation"
                        OnClientClick="return confirmSubmit();" OnClick="btnSubmit_Click" CausesValidation="false" />&nbsp;
                    <asp:Button ID="btnSaveForLater" runat="server" class="button" Text="Save For Later"
                        OnClick="btnSaveForLater_Click" OnClientClick="return confirmSubmit();" CausesValidation="false" />&nbsp;
                    <asp:Button ID="lnkCancel" runat="server" class="button" Text="Cancel" OnClick="lnkCancel_Click"
                        CausesValidation="false" />
                </div>
            </asp:View>
        </asp:MultiView>
        <asp:UpdateProgress ID="UpdateProgressEvaluate" runat="server" AssociatedUpdatePanelID="UpdatePanelEvaluate">
            <ProgressTemplate>
                <div>
                    <asp:Image ID="imgOrg" runat="server" ImageUrl="~/ui/images/icons/activity.gif" AlternateText="Please wait..">
                    </asp:Image></div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
