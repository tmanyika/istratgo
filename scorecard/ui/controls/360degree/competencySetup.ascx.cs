﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.controllers;
using scorecard.implementations;
using scorecard.entities;
using Scorecard.Business.Rules;
using System.Text;
using vb = Microsoft.VisualBasic;
using ScoreCard.Common.Interfaces;
using ScoreCard.Common.Logic;
using ThreeSixty.Evaluation;
using ScoreCard.Common.Config;

namespace scorecard.ui.controls
{
    public partial class competencySetup : System.Web.UI.UserControl
    {
        IScoreCardArea area;
        IEvaluationCompetency comp;

        public competencySetup()
        {
            area = new ScoreCardAreaBL();
            comp = new EvaluationCompetencyBL();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ResetLabels();
            if (!IsPostBack)
            {
                LoadAreas(ddlArea, string.Empty);
                LoadCompetencies();
            }
        }

        private void ResetLabels()
        {
            lblMsg.Text = "";
            LblError.Text = "";
        }

        void LoadAreas(DropDownList drpdwn, string selectedVal)
        {
            var data = area.GetByStatus(true);//.Where(r => r.AreaId != ScoreCardAreaConfig.EmployeeAreaId).ToList();

            BindControl.BindDropdown(drpdwn, "AreaName", "AreaId", "-select all -", "-1", data);
            if (drpdwn.Items.FindByValue(selectedVal) != null)
                drpdwn.SelectedValue = selectedVal;
        }

        void LoadCompetencies()
        {
            try
            {
                int areaId = int.Parse(ddlArea.SelectedValue);
                int companyId = Util.user.CompanyId;
                var data = (ddlArea.SelectedIndex == 0) ? comp.GetByStatus(companyId, true)
                                                      : comp.GetByArea(companyId, areaId, true);
                dtPagerlvComps.Visible = (data.Count > dtPagerlvComps.PageSize) ? true : false;
                pnlContactInfo.Visible = (data.Count > 0) ? true : false;
                BindControl.BindListView(lvComps, data);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            lvComps.InsertItemPosition = InsertItemPosition.LastItem;
            LoadCompetencies();

            var ddlAreas = lvComps.InsertItem.FindControl("ddlArea") as DropDownList;
            LoadAreas(ddlAreas, string.Empty);
        }

        void CreateNewCompetency(ListViewCommandEventArgs e)
        {
            try
            {
                string description = (e.Item.FindControl("txtDescription") as TextBox).Text;
                string name = (e.Item.FindControl("txtName") as TextBox).Text;
                var ddlArea = e.Item.FindControl("ddlArea") as DropDownList;

                if (ddlArea.SelectedIndex == 0 || string.IsNullOrEmpty(name))
                {
                    BindControl.BindLiteral(LblError, Messages.GetRequiredFieldsMessage(true, "Name", "Area"));
                    return;
                }

                int areaId = int.Parse(ddlArea.SelectedValue);
                bool added = comp.AddCompetency(new EvaluationCompetency
                {
                    Active = true,
                    AreaId = areaId,
                    CompanyId = Util.user.CompanyId,
                    CompetencyDescription = description,
                    CompetencyName = name,
                    CreatedBy = Util.user.LoginId
                });

                if (added)
                {
                    LoadReadOnly();
                    LoadCompetencies();
                }
                BindControl.BindLiteral(LblError, added ? Messages.GetSaveMessage() : Messages.GetAlreadyExistMessage());
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void LoadReadOnly()
        {
            lvComps.EditIndex = -1;
            lvComps.InsertItemPosition = InsertItemPosition.None;
        }

        void UpdateCompetency(ListViewDataItem e)
        {
            try
            {
                string description = (e.FindControl("txtEditDescription") as TextBox).Text;
                string name = (e.FindControl("txtEditName") as TextBox).Text;

                bool active = (e.FindControl("chkActive") as CheckBox).Checked;

                var hdnAreaId = e.FindControl("HdnAreaId") as HiddenField;
                var hdnCompetencyId = e.FindControl("HdnCompetencyId") as HiddenField;

                if (string.IsNullOrEmpty(hdnCompetencyId.Value))
                {
                    BindControl.BindLiteral(LblError, Messages.GetUpdateFailedMessage());
                    return;
                }

                if (string.IsNullOrEmpty(name))
                {
                    BindControl.BindLiteral(LblError, Messages.GetRequiredFieldsMessage(true, "Name", "Area"));
                    return;
                }

                int competencyId = int.Parse(hdnCompetencyId.Value);
                int areaId = int.Parse(hdnAreaId.Value);
                bool updated = comp.UpdateCompetency(new EvaluationCompetency
                {
                    Active = true,
                    AreaId = areaId,
                    CompanyId = Util.user.CompanyId,
                    CompetencyDescription = description,
                    CompetencyId = competencyId,
                    CompetencyName = name,
                    UpdatedBy = Util.user.LoginId
                });

                if (updated)
                {
                    LoadReadOnly();
                    LoadCompetencies();
                }
                BindControl.BindLiteral(LblError, updated ? Messages.GetUpdateMessage() : Messages.GetAlreadyExistMessage());
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void DeactivateCompetency(ListViewDataItem e)
        {
            try
            {
                var hdnCompetencyId = e.FindControl("HdnCompetencyId") as HiddenField;
                if (string.IsNullOrEmpty(hdnCompetencyId.Value))
                {
                    BindControl.BindLiteral(LblError, Messages.GetDeleteFailedMessage());
                    return;
                }

                int competencyId = int.Parse(hdnCompetencyId.Value);
                bool deactivated = comp.SetStatus(new EvaluationCompetency
                {
                    Active = false,
                    CompetencyId = competencyId,
                    UpdatedBy = Util.user.LoginId
                });

                if (deactivated)
                {
                    LoadReadOnly();
                    LoadCompetencies();
                }
                BindControl.BindLiteral(LblError, deactivated ? Messages.GetDeleteMessage() : Messages.GetDeleteFailedMessage());
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void CancelUpdate()
        {
            LoadReadOnly();
            LoadCompetencies();
        }

        protected void lvComps_ItemCanceling(object sender, ListViewCancelEventArgs e)
        {
            CancelUpdate();
        }

        protected void lvComp_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            LblError.Text = "";
            if (e.Item.ItemType == ListViewItemType.InsertItem ||
                e.Item.ItemType == ListViewItemType.DataItem)
            {
                switch (e.CommandName)
                {
                    case "add":
                        CreateNewCompetency(e);
                        break;
                }
            }
        }

        protected void lvComps_ItemDeleting(object sender, ListViewDeleteEventArgs e)
        {
            ListViewDataItem el = lvComps.Items[e.ItemIndex];
            DeactivateCompetency(el);
        }

        protected void lvComps_ItemUpdating(object sender, ListViewUpdateEventArgs e)
        {
            ListViewDataItem el = lvComps.Items[e.ItemIndex];
            UpdateCompetency(el);
        }

        protected void lvComp_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            lvComps.EditIndex = e.NewEditIndex;
            lvComps.InsertItemPosition = InsertItemPosition.None;
            LoadCompetencies();
        }

        protected void lvComp_PagePropertiesChanged(object sender, EventArgs e)
        {
            LoadCompetencies();
        }

        protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            lvComps.InsertItemPosition = InsertItemPosition.None;
            LoadCompetencies();
        }

        protected void lnkCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("index.aspx", true);
        }
    }
}