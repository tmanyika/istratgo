﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Net.Mail;
using vb = Microsoft.VisualBasic;

using System.Email.Communication;
using ScoreCard.Common.Interfaces;
using ThreeSixty.Evaluation;
using scorecard.controllers;
using ScoreCard.Common.Logic;
using HR.Human.Resources;
using scorecard.implementations;
using ScoreCard.Common.Config;
using scorecard.entities;
using Telerik.Web.UI;
using Bulk.Import;
using System.Text.RegularExpressions;
using System.Collections;

namespace scorecard.ui.controls._360degree
{
    public partial class evalSentOut : System.Web.UI.UserControl
    {
        #region Member Variables

        IScoreCardArea area;
        IEmployee emp;
        IEvaluationSubmission sub;
        IEvaluationSubmissionList subList;
        IEvaluationRating rate;
        IMailRelay objm;
        IBulkImport excel;
        IBulkSheet bsh;
        IEvaluationExternalPeople extp;

        projectmanagement proj;
        structurecontroller org;
        jobtitlesdefinitions job;

        bool isInternal = true;
        bool isAdmin = false;

        public string DropDownTree
        {
            get { return this.RadUserList.ClientID; }
        }

        #endregion

        #region Default Constructors

        public evalSentOut()
        {
            area = new ScoreCardAreaBL();
            emp = new EmployeeBL();
            sub = new EvaluationSubmissionBL();
            subList = new EvaluationSubmissionListBL();
            rate = new EvaluationRatingBL();
            excel = new BulkImportBL();
            bsh = new BulkSheetBL();
            extp = new EvaluationExternalPeopleBL();

            org = new structurecontroller(new structureImpl());
            proj = new projectmanagement(new projectImpl());
            job = new jobtitlesdefinitions(new jobtitleImpl());

            objm = new MailRelayBL();

            isAdmin = Util.user.UserTypeId == Util.getTypeDefinitionsUserTypeAdministrator();
        }

        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            ClearLabels();
            if (!IsPostBack)
            {
                InitialisePage();
            }
        }

        #endregion

        #region Databinding Methods

        void ClearLabels()
        {
            LblError.Text = string.Empty;
            lblImportMsg.Text = string.Empty;
            LblMsg.Text = string.Empty;
        }

        void InitialisePage()
        {
            ClearForm();
            MainView.SetActiveView(vwForm);
            LoadAreas(ddlArea, string.Empty);
        }

        void InitialisePage(string message, bool showList)
        {
            ClearForm();

            var data = sub.GetSubmissionsByStatus(Util.user.OrgId, ScoreCardAreaConfig.SavedForLaterId.ToString(), Util.user.UserId);
            if (showList)
            {
                MainView.SetActiveView(vwList);
                BindControl.BindListView(lvSaved, data.Where(s => s.IsInternal == isInternal).ToList());
                BindControl.BindLiteral(LblMsg, data.Count > 0 ? string.Empty : string.IsNullOrEmpty(message) ? Messages.NoRecordsToDisplay : message);
            }
            else
            {
                MainView.SetActiveView(vwForm);
                LoadAreas(ddlArea, string.Empty);
            }
        }

        DataTable GetEmptyData()
        {
            DataTable dT = new DataTable();
            dT.Columns.Add("ID");
            dT.Columns.Add("ParentID");
            dT.Columns.Add("Value");
            dT.Columns.Add("Text");

            dT.AcceptChanges();
            return dT;
        }

        void ClearUserListDropDown()
        {
            RadUserList.DataFieldID = "ID";
            RadUserList.DataFieldParentID = "ParentID";
            RadUserList.DataValueField = "Value";
            RadUserList.DataTextField = "Text";
            RadUserList.DataSource = GetEmptyData();
            RadUserList.DataBind();
        }

        void ClearForm()
        {
            ddEvalType.SelectedIndex = 0;
            ddlArea.Items.Clear();
            ddlAreaValue.Items.Clear();

            lblImportMsg.Text = string.Empty;
            LblError.Text = string.Empty;
            LblMsg.Text = string.Empty;

            txtDate.Text = string.Empty;

            divInternal.Visible = false;
            divExternal.Visible = false;

            var dataSrc = new List<EvaluationInternalPeople>();
            BindControl.BindRepeater(RptWeighting, dataSrc);
            AssignWeight.Visible = false;

            ClearUserListDropDown();
        }

        void LoadAreas(DropDownList drpdwn, string selectedVal)
        {
            var data = area.GetByStatus(true);

            BindControl.BindDropdown(drpdwn, "AreaAliasName", "AreaId", "-select area -", "-1", data);
            if (drpdwn.Items.FindByValue(selectedVal) != null)
                drpdwn.SelectedValue = selectedVal;
        }

        void LoadAreaValues(int areaId)
        {
            int orgUnitId = Util.user.OrgId;
            int compId = Util.user.CompanyId;
            int employeeId = Util.user.UserId;

            var data = new object();

            string dataValueField = "AreaItemId";
            string dataTextField = "AreaItemName";

            if (areaId == ScoreCardAreaConfig.EmployeeAreaId)
            {
                data = isAdmin ? sub.GetEmployeesByCompanyId(compId) : sub.GetEmployeesByEmployeeId(employeeId);
            }
            else if (areaId == ScoreCardAreaConfig.JobTitleAreaId)
            {
                data = isAdmin ? sub.GetJobTitleEmployeesByCompanyId(compId) : sub.GetJobTitleEmployeesByOrgUnitId(orgUnitId);
            }
            else if (areaId == ScoreCardAreaConfig.OrgUnitAreaId)
            {
                data = sub.GetOrgUnitsInHirachy(orgUnitId);
            }
            else if (areaId == ScoreCardAreaConfig.ProjectAreaId)
            {
                data = sub.GetProjectsByCompanyId(compId);
            }

            BindControl.BindDropdown(ddlAreaValue, dataTextField, dataValueField, "-select value -", "-1", data);
        }

        DataTable GetData(List<EvaluationInternalPeople> data)
        {
            DataTable dT = new DataTable();
            dT.Columns.Add("ID");
            dT.Columns.Add("ParentID");
            dT.Columns.Add("Value");
            dT.Columns.Add("Text");

            var dataP = data.GroupBy(p => new { p.DepartmentId, p.Department }).Select(g => g.First()).ToList();
            foreach (var item in dataP)
            {
                DataRow rw = dT.NewRow();
                rw["ID"] = string.Format("{0}-{1}", item.DepartmentId, item.Department);
                rw["ParentID"] = null;
                rw["Value"] = item.DepartmentId;
                rw["Text"] = item.Department;
                dT.Rows.Add(rw);
                rw = null;
            }

            foreach (var item in data)
            {
                DataRow rw = dT.NewRow();
                rw["ID"] = item.PersonId;
                rw["ParentID"] = string.Format("{0}-{1}", item.DepartmentId, item.Department);
                rw["Value"] = item.PersonId;
                rw["Text"] = item.FullName;
                dT.Rows.Add(rw);
                rw = null;
            }

            dT.AcceptChanges();
            return dT;
        }

        void FillInternalPeople()
        {
            int compId = Util.user.CompanyId;
            int orgUnitd = Util.user.OrgId;
            int employeeId = Util.user.UserId;

            var data = subList.GetInternalPeopleByCompanyId(compId);
            //isAdmin ? subList.GetInternalPeopleByCompanyId(compId) :
            //subList.GetInternalPeopleByOrgUnitId(orgUnitd);

            DataTable dT = GetData(data);

            RadUserList.DataFieldID = "ID";
            RadUserList.DataFieldParentID = "ParentID";
            RadUserList.DataValueField = "Value";
            RadUserList.DataTextField = "Text";
            RadUserList.DataSource = dT;
            RadUserList.DataBind();
        }

        void LoadEvaluators(bool isInternal, string data)
        {
            var dataSrc = isInternal ? subList.GetInternalSentOutPeopleById(data) :
                                       extp.GetExternalSentOutPeopleById(Util.user.CompanyId, data);

            BindControl.BindRepeater(RptWeighting, dataSrc);
        }

        decimal CalculateTotalWeight()
        {
            decimal total = 0;
            foreach (RepeaterItem item in RptWeighting.Items)
            {
                string weightVal = (item.FindControl("TxtWeight") as TextBox).Text;
                if (!string.IsNullOrEmpty(weightVal) && vb.Information.IsNumeric(weightVal))
                {
                    decimal itemWeight = decimal.Parse(weightVal);
                    total += itemWeight;
                }
            }
            return total;
        }

        bool HasWeightAssigned()
        {
            foreach (RepeaterItem item in RptWeighting.Items)
            {
                string weightVal = (item.FindControl("TxtWeight") as TextBox).Text;
                if (!string.IsNullOrEmpty(weightVal))
                    return true;
            }
            return false;
        }

        bool ValidInternalEvaluatorsList(long submissionId, bool hasWeight, MailAddressCollection recip, DataTable dT)
        {
            decimal maxWeight = 100;
            decimal totalWieght = 0;

            int itemCount = RptWeighting.Items.Count;

            string createdBy = Util.user.LoginId;
            foreach (RepeaterItem item in RptWeighting.Items)
            {
                HiddenField hdnValid = item.FindControl("HdnValid") as HiddenField;
                if (bool.Parse(hdnValid.Value))
                {
                    Literal lblItemError = item.FindControl("lblItemError") as Literal;
                    string weightVal = (item.FindControl("TxtWeight") as TextBox).Text;
                    string fullName = (item.FindControl("LblFullName") as Literal).Text;
                    string email = (item.FindControl("LblEmail") as Literal).Text;

                    int resourceId = int.Parse((item.FindControl("HdnPersonId") as HiddenField).Value);
                    object weight = DBNull.Value;

                    lblItemError.Text = string.Empty;

                    if (!string.IsNullOrEmpty(weightVal))
                    {
                        if (vb.Information.IsNumeric(weightVal))
                        {
                            weight = decimal.Parse(weightVal);
                            totalWieght += (decimal)weight;
                        }
                        else lblItemError.Text = Messages.NumericValueRequired;
                    }

                    recip.Add(new MailAddress(email, fullName));

                    DataRow nRow = dT.NewRow();
                    nRow["SubmissionId"] = submissionId;
                    nRow["ResourceId"] = resourceId;
                    nRow["SubmittedScore"] = false;
                    nRow["EvaluatorWeight"] = weight;
                    nRow["CreatedBy"] = createdBy;
                    nRow["UpdatedBy"] = createdBy;
                    dT.Rows.Add(nRow);

                    nRow = null;
                }
            }

            dT.AcceptChanges();
            if (maxWeight != totalWieght && hasWeight)
            {
                BindControl.BindLiteral(LblError, (maxWeight < totalWieght) ? Messages.TotalWeightCannotExceedHundred :
                                                             Messages.TotalWeightCannotBeLessHundred);
                return false;
            }
            else if (dT.Rows.Count <= 0)
            {
                BindControl.BindLiteral(LblError, "You have not yet added evaluators.");
                return false;
            }

            return true;
        }

        bool AddInternalEvaluators(long submissionId, bool hasWeight, string dataXml)
        {
            bool added = subList.AddEvaluationInternalResourceInBulk(new EvaluationSubmissionList
                                    {
                                        SubmissionId = submissionId,
                                        XmlEvalList = dataXml,
                                        HasWeight = hasWeight
                                    });
            return added;
        }

        bool RowValid(DataRow rw)
        {
            if (vb.Information.IsNothing(rw["FirstName"]) ||
                vb.Information.IsDBNull(rw["FirstName"]) ||
                vb.Information.IsNothing(rw["Surname"]) ||
                vb.Information.IsDBNull(rw["Surname"]) ||
                  vb.Information.IsNothing(rw["Email"]) ||
                vb.Information.IsDBNull(rw["Email"]))
                return false;
            return true;
        }

        bool AddExternalEvaluators(DataTable dS, int companyId, out string emaillist)
        {
            DataTable dT = extp.GetSchema(companyId);
            emaillist = string.Empty;

            StringBuilder strEml = new StringBuilder();
            StringBuilder strB = new StringBuilder();

            bool invalidRows = false;
            string emailRegEx = @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
            string createdBy = Util.user.LoginId;

            ArrayList data = new ArrayList();
            Regex regex = new Regex(emailRegEx, RegexOptions.IgnoreCase);
            foreach (DataRow rw in dS.Rows)
            {
                if (RowValid(rw))
                {
                    string name = rw["FirstName"].ToString();
                    string surname = rw["Surname"].ToString();
                    string email = rw["Email"].ToString();

                    if (regex.IsMatch(email) && !data.Contains(email))
                    {
                        DataRow nRow = dT.NewRow();

                        nRow["FirstName"] = name;
                        nRow["LastName"] = surname;
                        nRow["Email"] = email;
                        nRow["CompanyId"] = companyId;
                        nRow["CreatedBy"] = createdBy;

                        dT.Rows.Add(nRow);
                        data.Add(email);
                        strEml.Append(string.Format("'{0}',", email));
                        nRow = null;
                    }
                    else if (!regex.IsMatch(email))
                    {
                        invalidRows = true;
                        strB.Append(string.Format("<li>{0}</li>", email));
                    }
                }
                else invalidRows = true;
            }

            dT.AcceptChanges();
            if (dT.Rows.Count <= 0)
            {
                if (invalidRows)
                    BindControl.BindLiteral(lblImportMsg, string.IsNullOrEmpty(strB.ToString()) ?
                                                            "The data you uploaded is not valid" :
                                                            string.Format("<p>The following email addresses are invalid:</p><ul>{0}</ul>", strB.ToString()));
                else BindControl.BindLiteral(lblImportMsg, "There are no valid Evaluator records in that data you uploaded.");
                return false;
            }

            string dataXml = Common.GetXML(dT, "Data", "Record");
            bool added = extp.AddEvaluators(new EvaluationExternalPeople
                                                {
                                                    CompanyId = companyId,
                                                    XmlPeopleList = dataXml
                                                }
                                           );

            string mylist = strEml.ToString();
            int len = mylist.Length - 1;
            emaillist = (len > 0) ? mylist.Substring(0, len) : string.Empty;
            return added;
        }

        bool SendEmail(MailAddressCollection addy, long submissionId, bool isInternal, out string result)
        {
            bool sent = false;
            result = string.Empty;

            int idx = 0;
            int emailId = isInternal ? Util.InternalEmailId : Util.ExternalEmailId;

            object[][] mydata = new object[addy.Count][];
            string baseUrl = Common.GetBaseUrl;
            string evalFor = ddlAreaValue.SelectedItem.Text;

            foreach (MailAddress add in addy)
            {
                if (!isInternal)
                {
                    string compId = Util.user.CompanyId.ToString();
                    string customUrl = string.Format("{0}{1}?Id={2}&CompId={3}&UserId={4}&UserN={5}", baseUrl, Util.ExternalLandingPage,
                        Util.Encrypt(submissionId.ToString()), Util.Encrypt(compId), Util.Encrypt(add.Address), Util.Encrypt(add.DisplayName));
                    object[] data = { evalFor, customUrl };
                    mydata[idx] = data;
                }
                else
                {
                    object[] data = { evalFor };
                    mydata[idx] = data;
                }
                idx += 1;
            }


            object[] sub = { evalFor };

            MailRelay obj = new MailRelay
            {
                MailId = emailId,
                Recipient = addy,
                UData = mydata,
                SubjectData = sub
            };
            objm.SendMailWithRecipientData(obj, out result);
            return sent;
        }

        public void AddEvaluation()
        {
            long submissionId = 0;
            try
            {
                if (ddEvalType.SelectedIndex == 0 || ddlArea.SelectedIndex == 0 ||
                    ddlAreaValue.SelectedIndex == 0 || RptWeighting.Items.Count <= 0)
                {
                    string msg = Messages.GetRequiredFieldsMessage(true, ddEvalType.SelectedIndex == 0 ? "Evaluation Type" : string.Empty,
                        ddlArea.SelectedIndex == 0 ? "Area" : string.Empty, ddlAreaValue.SelectedIndex == 0 ? "Area Value" : string.Empty,
                        RptWeighting.Items.Count <= 0 ? "Evaluators List" : string.Empty);
                    BindControl.BindLiteral(LblError, msg);
                    return;
                }

                //Check for weight list of evaluators
                bool hasWeight = HasWeightAssigned();
                decimal maxWeight = 100;
                decimal totalWeight = CalculateTotalWeight();
                if (hasWeight && (totalWeight > maxWeight || totalWeight < maxWeight))
                {
                    string errorMessage = string.Empty;
                    if (totalWeight > maxWeight)
                    {
                        errorMessage = Messages.TotalWeightCannotExceedHundred; //string.Format("Total weight cannot exceed {0}. Your current total weight is {1}", maxWeight, totalWeight);
                    }
                    else if (totalWeight < maxWeight)
                    {
                        errorMessage = Messages.TotalWeightCannotBeLessHundred; //string.Format("Total weight should be equal to {0}. Your current total weight is {1}", maxWeight, totalWeight);
                    }

                    BindControl.BindLiteral(LblError, errorMessage);
                    return;
                }

                int orgUnitId = Util.user.OrgId;
                int capturerId = Util.user.UserId;
                int companyId = Util.user.CompanyId;
                int areaId = int.Parse(ddlArea.SelectedValue);
                int areaItemId = int.Parse(ddlAreaValue.SelectedValue);
                int statusId = ScoreCardAreaConfig.SubmittedId;
                Int16 rateValId = rate.GetMaxCompanyRating(companyId, true);

                string date = Common.GetInternationalDateFormat(txtDate.Text, DateFormat.dayMonthYear);
                bool isInternal = bool.Parse(ddEvalType.SelectedValue);
                bool deleteEvaluation = false;

                DateTime periodDate = DateTime.Parse(date);

                submissionId = sub.AddSubmission(new EvaluationSubmission
                                            {
                                                OrgUnitId = orgUnitId,
                                                AreaId = areaId,
                                                AreaItemId = areaItemId,
                                                PeriodDate = periodDate,
                                                MaximumRateValue = rateValId,
                                                StatusId = statusId,
                                                IsInternal = isInternal,
                                                CapturerId = capturerId
                                            });
                if (submissionId > 0)
                {
                    MailAddressCollection recip = new MailAddressCollection();
                    DataTable dT = subList.GetListSchema();

                    bool validList = ValidInternalEvaluatorsList(submissionId, hasWeight, recip, dT);
                    if (validList)
                    {
                        string dataXml = Common.GetXML(dT, "Data", "Record");
                        bool added = AddInternalEvaluators(submissionId, hasWeight, dataXml);
                        if (added)
                        {
                            string result = string.Empty;
                            bool sent = SendEmail(recip, submissionId, isInternal, out result);
                            MainView.SetActiveView(vwList);
                            BindControl.BindLiteral(LblMsg, sent ? result : Messages.EvaluationSaved);
                        }
                        else
                        {
                            sub.DeleteSubmission(submissionId);
                            BindControl.BindLiteral(LblError, Messages.GetSaveFailedGeneralMessage("Evaluators"));
                            return;
                        }
                    }
                    else deleteEvaluation = true;
                }
                else BindControl.BindLiteral(LblError, Messages.PeriodExistMessage);

                //Check if the evaluation should be deleted because of failure
                if (deleteEvaluation && submissionId > 0)
                {
                    bool deleted = sub.DeleteSubmission(submissionId);
                    BindControl.BindLiteral(LblError, string.Format("{0}{1}", LblError.Text, Messages.RectifyErrors));
                }
            }
            catch (Exception ex)
            {
                Regex reg = new Regex("duplicate", RegexOptions.IgnoreCase);
                BindControl.BindLiteral(LblError, reg.IsMatch(ex.StackTrace) ? string.Format("{0} {1}", LblError.Text, Messages.PeriodExistMessage) : Messages.GetErrorMessage());

                if (submissionId > 0)
                    sub.DeleteSubmission(submissionId);
                Util.LogErrors(ex);
            }
        }

        DataSet UploadFile()
        {
            try
            {
                if (RdUpload.UploadedFiles.Count > 0)
                {
                    UploadedFile myFile = RdUpload.UploadedFiles[0];
                    var enumType = excel.GetFileTypeEnum(myFile.GetExtension());
                    //check validity of the file here
                    if (enumType == FileEnum.None)
                    {
                        BindControl.BindLiteral(LblError, "Wrong file format. Only Excel <b>{0}</b> are allowed.");
                        return new DataSet();
                    }

                    DataSet dS = excel.GetData(myFile.InputStream, enumType);
                    return dS;
                }
                else
                {
                    BindControl.BindLiteral(LblError, "Please upload a file.");
                }
            }
            catch (Exception e)
            {
                BindControl.BindLiteral(LblError, Messages.GetErrorMessage());
                Util.LogErrors(e);
            }
            return new DataSet();
        }

        bool ValidateData(DataSet dS)
        {
            string outcome = string.Empty;
            var sheetList = bsh.GetByAreaIdAndStatus(BulkSheetAreaConfig.ThreeSixtyDegreeEvaluationAreaId, true).OrderBy(s => s.OrderNumber).ToList();
            bool valid = bsh.ValidateSheets(dS, sheetList, out  outcome);
            BindControl.BindLiteral(lblImportMsg, outcome);
            return valid;
        }

        #endregion

        #region ListView Event Handling Methods

        protected void lvSaved_ItemCommand(object sender, ListViewCommandEventArgs e)
        {

        }

        protected void lvSaved_ItemDeleting(object sender, ListViewDeleteEventArgs e)
        {

        }

        #endregion

        #region Drop Down Event Handling Methods

        protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            int areaId = ddlArea.SelectedIndex == 0 ? -1 : int.Parse(ddlArea.SelectedValue);
            if (areaId == -1)
            {
                ddlAreaValue.Items.Clear();
                BindControl.BindLiteral(LblError, "Select area to start setting up evaluation.");
                return;
            }

            LoadAreaValues(areaId);
            if (ddlAreaValue.Items.Count <= 0)
            {
                BindControl.BindLiteral(LblError, "There are no evaluation measures set for values in this area.");
            }
        }

        protected void ddEvalType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (bool.Parse(ddEvalType.SelectedValue))
            {
                divInternal.Visible = true;
                divExternal.Visible = false;
                FillInternalPeople();
            }
            else
            {
                divInternal.Visible = false;
                divExternal.Visible = true;
                ClearUserListDropDown();
            }
        }

        #endregion

        #region Button Event Handling Methods

        protected void lnkAssignExtUserWeight_Click(object sender, EventArgs e)
        {
            string emaillist;
            int companyId = Util.user.CompanyId;

            AssignWeight.Visible = true;
            DataTable dT = UploadFile().Tables[0];
            bool added = AddExternalEvaluators(dT, companyId, out emaillist);
            if (added)
            {
                LoadEvaluators(false, emaillist);
            }
            else
            {
                BindControl.BindLiteral(LblError, Messages.GetSaveFailedGeneralMessage("Evaluators'"));
            }
        }

        protected void lnkAssignWeight_Click(object sender, EventArgs e)
        {
            string data = RadUserList.SelectedValue;
            if (!string.IsNullOrEmpty(data))
            {
                AssignWeight.Visible = true;
                LoadEvaluators(true, data);
            }
            else
            {
                BindControl.BindLiteral(LblError, "Please select recipients.");
            }
        }

        protected void btnNewSentOut_Click(object sender, EventArgs e)
        {
            AddEvaluation();
        }

        protected void btnSentOutNewEval_Click(object sender, EventArgs e)
        {
            MainView.SetActiveView(vwForm);
            ClearForm();
            LoadAreas(ddlArea, string.Empty);
        }

        #endregion

        #region Repeater Event Handling Methods

        protected void RptWeighting_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName.Equals("delete"))
            {
                HiddenField HdnValid = e.Item.FindControl("HdnValid") as HiddenField;
                if (HdnValid != null)
                    HdnValid.Value = false.ToString();
                e.Item.Visible = false;
            }
        }

        #endregion

    }
}