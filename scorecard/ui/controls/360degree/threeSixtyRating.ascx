﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="threeSixtyRating.ascx.cs"
    Inherits="scorecard.ui.controls.scoring.threeSixtyRating" %>
<%@ Import Namespace="scorecard.implementations" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<h1>
    Setup Evaluation Ratings
</h1>
<asp:UpdatePanel ID="UpdatePanelRating" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:ListView ID="lstRating" runat="server" OnPagePropertiesChanged="lstRating_PagePropertiesChanged"
            OnItemEditing="lstRating_ItemEditing" OnItemCommand="lstRating_ItemCommand">
            <LayoutTemplate>
                <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                    <thead>
                        <tr>
                            <td style="width: 90px">
                                Rating
                            </td>
                            <td style="width: 550px">
                                Description
                            </td>
                            <td style="width: 90px">
                                Active
                            </td>
                            <td style="width: 80px">
                                Edit
                            </td>
                            <td style="width: 80px">
                                Delete
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr id="itemPlaceholder" runat="server" />
                    </tbody>
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tr class="odd">
                    <td align="left">
                        <%# Eval("RatingValue")%>
                        <asp:HiddenField ID="hdnRatingId" runat="server" Value='<%# Eval("RatingId") %>' />
                    </td>
                    <td align="left">
                        <asp:Literal ID="LblDesc" runat="server" Text='<%# Eval("RatingDescription")%>'></asp:Literal>
                    </td>
                    <td align="left">
                        <%# Common.GetBooleanYesNo(Eval("Active"))%>
                    </td>
                    <td>
                        <ul id="icons">
                            <asp:LinkButton runat="server" ID="lnkEdit" CommandName="Edit" class="ui-state-default ui-corner-all"
                                title="Edit Record" CausesValidation="false"><span class="ui-icon ui-icon-pencil">
                            
                            </span></asp:LinkButton></ul>
                    </td>
                    <td>
                        <ul id="icons">
                            <asp:LinkButton runat="server" ID="lnkDelete" CommandName="deleteItem" class="ui-state-default ui-corner-all"
                                title="Delete Record" OnClientClick="return confirmDelete();" CausesValidation="false"><span class="ui-icon ui-icon-trash">
                        </span></asp:LinkButton>
                        </ul>
                    </td>
                </tr>
            </ItemTemplate>
            <EditItemTemplate>
                <tbody>
                    <tr class="odd">
                        <td align="left" valign="top">
                            <asp:TextBox runat="server" ID="txtRatingValueE" Text='<%# Eval("RatingValue")%>'
                                ValidationGroup="Edit" Width="40px" />
                            <asp:RequiredFieldValidator ID="RqdInfoE" runat="server" ErrorMessage="rating value is required" ValidationGroup="Edit"
                                ControlToValidate="txtRatingValueE" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="CmpValid" runat="server" ErrorMessage="only numeric values are allowed"
                                ValidationGroup="Edit" ControlToValidate="txtRatingValueE" ForeColor="Red" Operator="DataTypeCheck"
                                Type="Integer"></asp:CompareValidator>
                            <asp:HiddenField ID="hdnRatingId" runat="server" Value='<%# Eval("RatingId") %>' />
                        </td>
                        <td align="left" valign="top">
                            <asp:TextBox runat="server" ID="txtDescE" Text='<%# Eval("RatingDescription")%>'
                                Width="400px" />
                            <asp:RequiredFieldValidator ID="rqdVald" runat="server" ErrorMessage="*" ValidationGroup="Edit"
                                ControlToValidate="txtDescE" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" valign="top">
                            <asp:CheckBox ID="chkActiveE" runat="server" Checked='<%# Eval("Active") %>' />
                        </td>
                        <td valign="top">
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkUpdate" CommandName="updateItem" ValidationGroup="Edit"
                                    class="ui-state-default ui-corner-all" title="Update Record"><span class="ui-icon ui-icon-disk"></span></asp:LinkButton>
                            </ul>
                        </td>
                        <td valign="top">
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkCancelUpdate" CommandName="cancelUpdate" CssClass="ui-state-default ui-corner-all"
                                    title="Cancel Update" CausesValidation="false"><span class="ui-icon ui-icon-cancel"></span></asp:LinkButton>
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </EditItemTemplate>
            <InsertItemTemplate>
                <tbody>
                    <tr class="odd">
                        <td align="left" valign="top">
                            <asp:TextBox runat="server" ID="txtRatingValueI" ValidationGroup="Insert" Width="40px" />
                            <asp:RequiredFieldValidator ID="RqdInfoI" runat="server" ErrorMessage="rating value is required" ValidationGroup="Insert"
                                ControlToValidate="txtRatingValueI" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="CmpValidI" runat="server" ErrorMessage="only numeric values are allowed"
                                ValidationGroup="Insert" ControlToValidate="txtRatingValueI" ForeColor="Red"
                                Operator="DataTypeCheck" Type="Integer"></asp:CompareValidator>

                        </td>
                        <td align="left" valign="top">
                            <asp:TextBox runat="server" ID="txtDescI" Width="400px" />
                        </td>
                        <td align="left" valign="top">
                            <asp:CheckBox ID="chkActiveI" runat="server" Checked="true" />
                        </td>
                        <td valign="top">
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkSave" CommandName="insertItem" ValidationGroup="Insert"
                                    class="ui-state-default ui-corner-all" title="Add New Rating"><span class="ui-icon ui-icon-disk"></span></asp:LinkButton>
                            </ul>
                        </td>
                        <td valign="top">
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkCancelUpdate" CommandName="cancelUpdate" CssClass="ui-state-default ui-corner-all"
                                    title="Cancel Update" CausesValidation="false"><span class="ui-icon ui-icon-cancel"></span></asp:LinkButton>
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </InsertItemTemplate>
            <EmptyDataTemplate>
                <p class="errorMsg">
                    There are no records to display.
                </p>
            </EmptyDataTemplate>
        </asp:ListView>
        <br />
        <asp:DataPager ID="pager" runat="server" PagedControlID="lstRating" PageSize="15">
            <Fields>
                <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True" ShowNextPageButton="False"
                    ShowPreviousPageButton="False" />
                <asp:NumericPagerField />
                <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True" ShowNextPageButton="False"
                    ShowPreviousPageButton="False" />
            </Fields>
        </asp:DataPager>
        <br />
        <br />
        <p class="errorMsg">
            <asp:Literal ID="lblMsg" runat="server"></asp:Literal></p>
        <div id="tabs-3">
            <p>
                <asp:Button ID="btnNew" runat="server" class="button" Text="Add New Rating" OnClick="lnkAdd_Click"
                    CausesValidation="False" />
                &nbsp;<asp:Button ID="lnkCancel0" runat="server" class="button" Text="Cancel" OnClick="lnkCancel_Click"
                    CausesValidation="false" />
            </p>
        </div>
        <asp:UpdateProgress ID="UpdateProgressRating" runat="server" AssociatedUpdatePanelID="UpdatePanelRating">
            <ProgressTemplate>
                <div>
                    <asp:Image ID="imgRating" runat="server" ImageUrl="~/ui/images/icons/activity.gif"
                        AlternateText="Please wait.."></asp:Image></div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
