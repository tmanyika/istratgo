﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="evalSentOut.ascx.cs"
    Inherits="scorecard.ui.controls._360degree.evalSentOut" %>
<%@ Import Namespace="scorecard.implementations" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:UpdatePanel ID="UpdatePanelSentOut" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div>
            <h1>
                Send 360<sup>0</sup> Evaluation</h1>
        </div>
        <asp:MultiView ID="MainView" runat="server" ActiveViewIndex="-1">
            <asp:View ID="vwList" runat="server">
                <div>
                    <asp:ListView ID="lvSaved" runat="server" OnItemCommand="lvSaved_ItemCommand" OnItemDeleting="lvSaved_ItemDeleting">
                        <LayoutTemplate>
                            <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                                <thead>
                                    <tr id="tr1" runat="server">
                                        <td>
                                            <input type="checkbox" class="checkall" />
                                        </td>
                                        <td>
                                            Criteria
                                        </td>
                                        <td>
                                            Name
                                        </td>
                                        <td>
                                            Status
                                        </td>
                                        <td>
                                            Period Date
                                        </td>
                                        <td>
                                            Comment
                                        </td>
                                        <td>
                                            Update
                                        </td>
                                        <td>
                                            Delete
                                        </td>
                                    </tr>
                                    <tr id="ItemPlaceHolder" runat="server">
                                    </tr>
                                </thead>
                            </table>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tbody>
                            </tbody>
                        </ItemTemplate>
                        <EmptyItemTemplate>
                            There are no record to display.</EmptyItemTemplate>
                    </asp:ListView>
                </div>
                <div>
                    <p style="color: Red">
                        <asp:Literal ID="LblMsg" runat="server"></asp:Literal></p>
                </div>
                <div style="width: 100%; text-align: left">
                    <br />
                    <asp:Button ID="btnSentOutNewEval" runat="server" CssClass="button" CausesValidation="false"
                        Text="Send New Evaluation" OnClick="btnSentOutNewEval_Click" OnClientClicked="clearDropdownTree" />
                </div>
            </asp:View>
            <asp:View ID="vwForm" runat="server">
                <fieldset runat="server" id="pnlContactInfo">
                    <legend>Send 360<sup>0</sup> Evaluation For</legend>
                    <p>
                        <label for="sf">
                            Select Area:
                        </label>
                        <span class="field_desc">
                            <asp:DropDownList runat="server" ID="ddlArea" AutoPostBack="True" Width="130px" CausesValidation="false"
                                OnSelectedIndexChanged="ddlArea_SelectedIndexChanged" />
                            <asp:RequiredFieldValidator ID="rqdArea" runat="server" ControlToValidate="ddlArea"
                                Display="Dynamic" ErrorMessage="area is required" InitialValue="-1" ValidationGroup="Form"
                                ForeColor="Red"></asp:RequiredFieldValidator>
                            <br />
                        </span>
                    </p>
                    <p>
                        <label for="sf">
                            Select Value:
                        </label>
                        <span class="field_desc">
                            <asp:DropDownList runat="server" ID="ddlAreaValue" ValidationGroup="Form" />
                            <asp:RequiredFieldValidator ID="rqdVal" runat="server" ControlToValidate="ddlAreaValue"
                                Display="Dynamic" ErrorMessage="value is required" InitialValue="-1" ValidationGroup="Form"
                                ForeColor="Red"></asp:RequiredFieldValidator>
                        </span>
                    </p>
                    <p>
                        <label for="sf">
                            Date:</label>
                        <span class="field_desc">
                            <asp:TextBox ID="txtDate" runat="server" Width="120px" ValidationGroup="Form"></asp:TextBox>
                            <asp:CalendarExtender ID="txtDate_CalendarExtender" runat="server" Enabled="True"
                                Format="d/MM/yyyy" TargetControlID="txtDate">
                            </asp:CalendarExtender>
                            <asp:RequiredFieldValidator ID="rqDate" runat="server" ErrorMessage="Date of Score"
                                Display="None" ControlToValidate="txtDate" ForeColor="Red" ValidationGroup="Form"></asp:RequiredFieldValidator>
                            <span style="color: Red">
                                <asp:Literal ID="lblDateUsed" runat="server"></asp:Literal></span> </span>
                    </p>
                    <p>
                        <label for="sf">
                            Evaluation Type:
                        </label>
                        <span class="field_desc">
                            <asp:DropDownList runat="server" ID="ddEvalType" AutoPostBack="True" ValidationGroup="Form"
                                OnSelectedIndexChanged="ddEvalType_SelectedIndexChanged">
                                <asp:ListItem Selected="True" Text="- select option -" Value="-1" />
                                <asp:ListItem Text="Internal" Value="true" />
                                <asp:ListItem Text="External" Value="false" />
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RqdEvalT" runat="server" ErrorMessage="evaluation type is required"
                                Display="None" ControlToValidate="ddEvalType" InitialValue="-1" ForeColor="Red"
                                ValidationGroup="Form"></asp:RequiredFieldValidator>
                        </span>
                    </p>
                    <p>
                        <span style="color: Red">
                            <asp:Literal ID="LblError" runat="server"></asp:Literal></span></p>
                </fieldset>
                <fieldset runat="server" id="InternalEvalList">
                    <legend>Evaluation Recipients</legend>
                    <div style="clear: both" runat="server" id="divInternal" visible="false">
                        <div style="float: left">
                            <label for="sf">
                                Recipients:
                            </label>
                        </div>
                        <div style="float: left; width: 320px;">
                            <telerik:RadDropDownTree ID="RadUserList" runat="server" DefaultMessage="- select recipients -"
                                ExpandNodeOnSingleClick="true" enablefiltering="true" CheckBoxes="SingleCheck"
                                filtersettings-highlight="Matches" Skin="Windows7" Width="300px" OnClientEntryAdding="OnClientEntryAdding">
                                <DropDownSettings OpenDropDownOnLoad="true" />
                            </telerik:RadDropDownTree>
                        </div>
                        <div style="float: left; padding-left: 15px;">
                            <asp:LinkButton ID="lnkAssignWeight" runat="server" CssClass="button" ValidationGroup="Form"
                                ToolTip="Confirm recipients" OnClick="lnkAssignWeight_Click">Confirm Recipients</asp:LinkButton>
                        </div>
                    </div>
                    <div style="clear: both" runat="server" id="divExternal" visible="false">
                        <div>
                            <p>
                                Import spreadsheet with external evaluators information in the following in order:
                                :</span><br />
                                <br />
                            </p>
                            <p>
                                <b>Name</b> | <b>Surname</b>| <b>E-mail</b>&nbsp;<a href="templates/ExternalEvaluationTemplate.xlsx"
                                    target="_blank"><span style="color: Blue"> download template here</a><span style="color: Blue"><br />
                                        <br />
                            </p>
                        </div>
                        <div>
                            <span style="color: Red">
                                <asp:Literal ID="lblImportMsg" runat="server"></asp:Literal></span>
                            <div id="exFileList" class="file-list">
                            </div>
                        </div>
                        <div>
                            <div style="float: left">
                                <label for="sf">
                                    Recipients:
                                </label>
                            </div>
                            <div style="float: left; width: 320px;">
                                <telerik:RadAsyncUpload ID="RdUpload" runat="server" AllowedFileExtensions="xls,xlsx"
                                    OnClientFileUploadFailed="OnClientFileUploadFailed" AutoAddFileInputs="False"
                                    MultipleFileSelection="Disabled" overwriteexistingfiles="True" Skin="Web20" MaxFileSize="10485760"
                                    MaxFileInputsCount="1">
                                </telerik:RadAsyncUpload>
                            </div>
                            <div style="float: left; padding-left: 15px;">
                                <asp:LinkButton ID="lnkAssignExtUserWeight" runat="server" CssClass="button" ValidationGroup="Form"
                                    ToolTip="Confirm recipients" OnClick="lnkAssignExtUserWeight_Click">Confirm Recipients</asp:LinkButton>
                            </div>
                        </div>
                    </div>
                    </p> </span> </span>
                </fieldset>
                <fieldset runat="server" id="AssignWeight" visible="false">
                    <legend>Evaluation Recipients Weighting</legend>
                    <asp:Repeater ID="RptWeighting" runat="server" OnItemCommand="RptWeighting_ItemCommand">
                        <HeaderTemplate>
                            <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                                <thead>
                                    <tr>
                                        <td>
                                            Full Name
                                        </td>
                                        <td>
                                            Department
                                        </td>
                                        <td>
                                            Email
                                        </td>
                                        <td>
                                            Weight
                                        </td>
                                        <td>
                                            Remove
                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:Literal ID="LblFullName" runat="server" Text='<%# Eval("FullName")%>' />
                                    <asp:HiddenField ID="HdnPersonId" runat="server" Value='<%# Bind("PersonId") %>' />
                                    <asp:HiddenField ID="HdnValid" runat="server" Value="true" />
                                </td>
                                <td>
                                    <%# Eval("Department")%>
                                </td>
                                <td>
                                    <asp:Literal ID="LblEmail" runat="server" Text='<%# Eval("Email")%>' />
                                </td>
                                <td>
                                    <asp:TextBox ID="TxtWeight" runat="server" Width="60px" ValidationGroup="Form"></asp:TextBox><asp:CompareValidator
                                        ID="Cvd" ValidationGroup="Form" Display="Dynamic" ControlToValidate="TxtWeight"
                                        runat="server" ErrorMessage="only numeric value is allowed" Operator="DataTypeCheck"
                                        Type="Double" ForeColor="Red"></asp:CompareValidator>
                                    <span style="color: Red">
                                        <asp:Literal ID="lblItemError" runat="server"></asp:Literal></span>
                                </td>
                                <td>
                                    <ul id="icons">
                                        <asp:LinkButton runat="server" ID="lnkDelete" CommandName="delete" class="ui-state-default ui-corner-all"
                                            title="Remove recipient" OnClientClick="return confirmDelete();"><span class="ui-icon ui-icon-trash">
                                    </span>
                                        </asp:LinkButton>
                                    </ul>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody></table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <p>
                        <u><b style="color: Black">Notes<br />
                            <br />
                        </b></u>
                    </p>
                    <p>
                        <span style="color: Red">1. * = required field
                            <br />
                        </span>
                        <br />
                        <span style="color: Red">2. For numeric values remove any commas. Use a <strong>full
                            stop</strong> for decimal numbers. Only a maximum of 2 decimal places is allowed.</span><br />
                    </p>
                </fieldset>
                <div id="tabs-3">
                    <p>
                        <asp:Button ID="btnNewSentOut" runat="server" CssClass="button" Text="Send Evaluation"
                            OnClick="btnNewSentOut_Click" CausesValidation="false" OnClientClick="return confirmSubmit();"
                            ValidationGroup="Form" />&nbsp;
                        <asp:Button ID="lnkCancel0" runat="server" CssClass="button" Text="Done" CausesValidation="false"
                            PostBackUrl="~/ui/index.aspx" Visible="False" />
                    </p>
                </div>
            </asp:View>
        </asp:MultiView>
        <asp:UpdateProgress ID="UpdateProgressSentOut" runat="server" AssociatedUpdatePanelID="UpdatePanelSentOut">
            <ProgressTemplate>
                <div>
                    <asp:Image ID="imgOrg" runat="server" ImageUrl="~/ui/images/icons/activity.gif" AlternateText="Please wait..">
                    </asp:Image></div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="RdUpload" />
    </Triggers>
</asp:UpdatePanel>
<script type="text/javascript">
    function OnClientEntryAdding(sender, args) {
        var path = args.get_entry().get_fullPath();
        var s = path.split("/");
        if (s.length == 1) {
            // if the selected entry is parent then it will not allow to select
            args.set_cancel(true);
        }
        else {
            // close the dropdown explicitly
            //sender.closeDropDown();
        }
    }

    function clearDropdownTree(sender, args) {
        var dropTree = document.getElementById('<% = DropDownTree %>');
        dropTree.control.get_entries().clear();
    }

    function OnClientFileUploadFailed(sender, args) {
        if (args.get_message() == "error") {

            var fileList = document.getElementById("exFileList");
            fileList.innerHTML = 'File upload failed';
            args.set_handled(true);
        }
    }
</script>
