﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="competencySetup.ascx.cs"
    Inherits="scorecard.ui.controls.competencySetup" %>
<%@ Import Namespace="scorecard.implementations" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:UpdatePanel ID="UpdatePanelComp" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <h1>
            360<sup>0</sup> Evaluation Competencies</h1>
        <fieldset runat="server" id="pnlContactInfo">
            <legend>360<sup>0</sup> Evaluation Competencies Filter</legend>
            <p>
                <label for="lf">
                    Select Area To Display:
                </label>
                &nbsp;<span class="field_desc"><asp:DropDownList runat="server" ID="ddlArea" AutoPostBack="True"
                    OnSelectedIndexChanged="ddlArea_SelectedIndexChanged" Width="130px" CausesValidation="false" />
                    <br />
                </span>
            </p>
        </fieldset>
        <asp:ListView ID="lvComps" runat="server" OnItemCommand="lvComp_ItemCommand" OnItemEditing="lvComp_ItemEditing"
            OnPagePropertiesChanged="lvComp_PagePropertiesChanged" OnItemCanceling="lvComps_ItemCanceling"
            OnItemDeleting="lvComps_ItemDeleting" OnItemUpdating="lvComps_ItemUpdating">
            <LayoutTemplate>
                <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                    <thead>
                        <tr id="trFirst" runat="server">
                            <td style="width: 60px">
                                <input type="checkbox" class="checkall" />
                            </td>
                            <td style="width: 210px">
                                Area
                            </td>
                            <td align="left" style="width: 210px">
                                Competency
                            </td>
                            <td align="left" style="width: 210px">
                                Description
                            </td>
                            <td style="width: 90px">
                                Active
                            </td>
                            <td width="80px">
                                Edit
                            </td>
                            <td width="80px">
                                Delete
                            </td>
                        </tr>
                        <tr id="ItemPlaceHolder" runat="server">
                        </tr>
                    </thead>
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tbody>
                    <tr class="odd">
                        <td>
                            <input type="checkbox" />
                            <asp:HiddenField ID="HdnCompetencyId" runat="server" Value='<%# Bind("CompetencyId")%>' />
                            <asp:HiddenField ID="HdnAreaId" runat="server" Value='<%# Bind("Area.AreaId")%>' />
                        </td>
                        <td>
                            <%# Eval("Area.AreaName")%>
                        </td>
                        <td>
                            <%# Eval("CompetencyName")%>
                        </td>
                        <td>
                            <%# Eval("CompetencyDescription")%>
                        </td>
                        <td>
                            <%# Common.GetBooleanYesNo(Eval("Active"))%>
                        </td>
                        <td>
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkEdit" CommandName="edit" class="ui-state-default ui-corner-all"
                                    title="Edit Record"><span class="ui-icon ui-icon-pencil">
                        </span></asp:LinkButton>
                            </ul>
                        </td>
                        <td>
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkDelete" CommandName="delete" class="ui-state-default ui-corner-all"
                                    title="Delete Record" OnClientClick="return confirmDelete();"><span class="ui-icon ui-icon-trash">
                                    </span>
                                </asp:LinkButton>
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </ItemTemplate>
            <EditItemTemplate>
                <tbody>
                    <tr class="odd">
                        <td>
                            <input type="checkbox" />
                            <asp:HiddenField ID="HdnCompetencyId" runat="server" Value='<%# Bind("CompetencyId")%>' />
                            <asp:HiddenField ID="HdnAreaId" runat="server" Value='<%# Bind("Area.AreaId")%>' />
                        </td>
                        <td>
                            <%# Eval("Area.AreaName")%>
                        </td>
                        <td valign="top">
                            <asp:TextBox runat="server" ID="txtEditName" Text='<%# Eval("CompetencyName")%>'
                                ValidationGroup="Edit" Width="200px" />
                            <asp:RequiredFieldValidator ID="RqdName" runat="server" ErrorMessage="*" ValidationGroup="Edit"
                                ControlToValidate="txtEditName" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                        <td valign="top">
                            <asp:TextBox runat="server" ID="txtEditDescription" Text='<%# Eval("CompetencyDescription")%>'
                                Width="200px" />
                        </td>
                       <td valign="top">
                            <asp:CheckBox ID="chkActive" runat="server" Checked='<%# Eval("Active")%>' />
                        </td>
                       <td valign="top">
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkSave" CommandName="update" class="ui-state-default ui-corner-all"
                                    title="Update Record" ValidationGroup="Edit"><span class="ui-icon ui-icon-disk">
                                     </span>
                                </asp:LinkButton>
                            </ul>
                        </td>
                        <td valign="top">
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkCancelUpdate" CommandName="cancel" class="ui-state-default ui-corner-all"
                                    title="Cancel Update" CausesValidation="false"><span class="ui-icon ui-icon-cancel">
                                    </span>
                                </asp:LinkButton>
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </EditItemTemplate>
            <InsertItemTemplate>
                <tbody>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td align="left" valign="top">
                            <asp:DropDownList runat="server" ID="ddlArea" ValidationGroup="Insert" Width="200px" />
                            <asp:RequiredFieldValidator ID="RqdArea" runat="server" ErrorMessage="area of competence is required"
                                ValidationGroup="Insert" ControlToValidate="ddlArea" InitialValue="-1" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" valign="top">
                            <asp:TextBox runat="server" ID="txtName" ValidationGroup="Insert" Width="200px" />
                            <asp:RequiredFieldValidator ID="RqdInsertName" runat="server" ErrorMessage="*" ValidationGroup="Insert"
                                ControlToValidate="txtName" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" valign="top">
                            <asp:TextBox runat="server" ID="txtDescription" Width="200px" />
                        </td>
                        <td align="left" valign="top">
                            &nbsp;
                            <asp:CheckBox ID="chkActive" runat="server" Checked="true" Enabled="false" />
                        </td>
                        <td valign="top">
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkSave" class="ui-state-default ui-corner-all"
                                    title="Save this Record" CommandName="add" ValidationGroup="Insert"><span class="ui-icon ui-icon-disk">
                        </span></asp:LinkButton>
                            </ul>
                        </td>
                        <td valign="top">
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkCancel" class="ui-state-default ui-corner-all"
                                    title="Cancel Save " CommandName="cancel"><span class="ui-icon ui-icon-cancel">
                        </span></asp:LinkButton>
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </InsertItemTemplate>
            <EmptyDataTemplate>
                <span style="color: Red">There are no records to display.</span>
            </EmptyDataTemplate>
        </asp:ListView>
        <br />
        <asp:DataPager ID="dtPagerlvComps" runat="server" PagedControlID="lvComps" PageSize="15">
            <Fields>
                <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True" ShowNextPageButton="False"
                    ShowPreviousPageButton="False" />
                <asp:NumericPagerField />
                <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True" ShowNextPageButton="False"
                    ShowPreviousPageButton="False" />
            </Fields>
        </asp:DataPager>
        <div>
            <span style="color: Red">
                <asp:Literal ID="LblError" runat="server" /></span>
        </div>
        <div>
            <p>
                <br />
                <br />
                <asp:Label ID="lblMsg" runat="server" Style="color: #FF3300" />
                <br />
                <br />
            </p>
        </div>
        <div id="tabs-3">
            <p>
                <asp:Button ID="btnNew" runat="server" CssClass="button" Text="Add New Competency"
                    OnClick="btnNew_Click" CausesValidation="false" />
                <asp:Button ID="lnkCancel0" runat="server" CssClass="button" Text="Done" CausesValidation="false"
                    PostBackUrl="~/ui/index.aspx" Visible="False" />
            </p>
        </div>
        <asp:UpdateProgress ID="UpdateProgressComp" runat="server" AssociatedUpdatePanelID="UpdatePanelComp">
            <ProgressTemplate>
                <div>
                    <asp:Image ID="imgOrg" runat="server" ImageUrl="~/ui/images/icons/activity.gif" AlternateText="Please wait..">
                    </asp:Image></div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
