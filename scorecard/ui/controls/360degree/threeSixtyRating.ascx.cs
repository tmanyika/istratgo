﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.implementations;
using scorecard.interfaces;
using scorecard.entities;
using ms = Microsoft.VisualBasic;
using ThreeSixty.Evaluation;

namespace scorecard.ui.controls.scoring
{
    public partial class threeSixtyRating : System.Web.UI.UserControl
    {
        #region Properties

        int CompId
        {
            get { return Util.user.CompanyId; }
        }

        int maximumRatingValueAllowed = Util.MaximumEvaluationRatingValue;

        #endregion

        #region Private Variables

        IEvaluationRating objR;

        #endregion

        #region Default Constructor

        public threeSixtyRating()
        {
            objR = new EvaluationRatingBL();
        }

        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            ClearLabels();
            if (!IsPostBack)
            {
                InitialisePage();
            }
        }

        void ClearLabels()
        {
            lblMsg.Text = "";
        }

        #endregion

        #region Utility Methods

        void InitialisePage()
        {
            LoadEvaluationRating();
        }

        void LoadEvaluationRating()
        {
            try
            {
                var data = objR.GetByStatus(CompId, true);
                BindControl.BindListView(lstRating, data);
                if (data.Count <= pager.PageSize) pager.Visible = false;
                if (data.Count <= 0) lstRating.InsertItemPosition = InsertItemPosition.LastItem;
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        public string GetBoolean(object boolVal)
        {
            if (boolVal == DBNull.Value) return string.Empty;
            return bool.Parse(boolVal.ToString()) ? "Yes" : "No";
        }

        void CancelUpdate()
        {
            lstRating.EditIndex = -1;
            lstRating.InsertItemPosition = InsertItemPosition.None;
            LoadEvaluationRating();
        }

        void DeleteRating(int ratingId)
        {
            try
            {
                string updatedBy = Util.user.FullName;
                EvaluationRating obj = new EvaluationRating
                {
                    Active = false,
                    RatingId = ratingId,
                    UpdatedBy = updatedBy
                };

                bool deleted = objR.DeactivateEvalRating(obj);
                if (deleted) LoadEvaluationRating();
                BindControl.BindLiteral(lblMsg, deleted ? Messages.GetDeleteMessage() : Messages.GetDeleteFailedMessage());
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        void AddNewRating(ListViewCommandEventArgs e)
        {
            try
            {
                TextBox txtDesc = e.Item.FindControl("txtDescI") as TextBox;
                TextBox txtRatingValueI = e.Item.FindControl("txtRatingValueI") as TextBox;
                CheckBox chkActiveE = e.Item.FindControl("chkActiveI") as CheckBox;

                if (!ms.Information.IsNumeric(txtRatingValueI.Text))
                {
                    BindControl.BindLiteral(lblMsg, "Rating should be a numeric");
                    return;
                }

                Int16 rateVal = Int16.Parse(txtRatingValueI.Text);
                if (rateVal > maximumRatingValueAllowed)
                {
                    BindControl.BindLiteral(lblMsg, string.Format("Rating value cannot exceed {0}", maximumRatingValueAllowed));
                    return;
                }

                string userName = Util.user.FullName;
                string desc = txtDesc.Text.Trim();

                EvaluationRating obj = new EvaluationRating
                {
                    Active = true,
                    CompanyId = CompId,
                    CreatedBy = userName,
                    DateCreated = DateTime.Now,
                    DateUpdated = DateTime.Now,
                    RatingDescription = desc,
                    RatingId = 0,
                    RatingValue = rateVal,
                    UpdatedBy = userName
                };

                bool saved = objR.AddEvaluationRating(obj);
                if (saved)
                {
                    lstRating.InsertItemPosition = InsertItemPosition.None;
                    LoadEvaluationRating();
                    BindControl.BindLiteral(lblMsg, Messages.GetSaveMessage());
                }
                else BindControl.BindLiteral(lblMsg, Messages.GetAlreadyExistMessage());
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
            }
        }

        void UpdateRating(ListViewCommandEventArgs e)
        {
            try
            {
                TextBox txtDesc = e.Item.FindControl("txtDescE") as TextBox;
                TextBox txtVal = e.Item.FindControl("txtRatingValueE") as TextBox;

                HiddenField hdnRatingId = e.Item.FindControl("hdnRatingId") as HiddenField;
                CheckBox chkActiveE = e.Item.FindControl("chkActiveE") as CheckBox;

                if (!ms.Information.IsNumeric(txtVal.Text))
                {
                    BindControl.BindLiteral(lblMsg, "Rating should be a numeric");
                    return;
                }

                string desc = txtDesc.Text.Trim();
                string userName = Util.user.FullName;

                int ratingId = int.Parse(hdnRatingId.Value);
                Int16 rateVal = Int16.Parse(txtVal.Text);
                bool isActive = chkActiveE.Checked;

                if (rateVal > maximumRatingValueAllowed)
                {
                    BindControl.BindLiteral(lblMsg, string.Format("Rating value cannot exceed {0}", maximumRatingValueAllowed));
                    return;
                }

                EvaluationRating obj = new EvaluationRating
                {
                    Active = isActive,
                    CompanyId = CompId,
                    CreatedBy = userName,
                    DateCreated = DateTime.Now,
                    DateUpdated = DateTime.Now,
                    RatingDescription = desc,
                    RatingId = ratingId,
                    RatingValue = rateVal,
                    UpdatedBy = userName
                };

                bool saved = objR.UpdateEvaluationRating(obj);
                if (saved)
                {
                    lstRating.EditIndex = -1;
                    lstRating.InsertItemPosition = InsertItemPosition.None;
                    LoadEvaluationRating();
                    BindControl.BindLiteral(lblMsg, Messages.GetUpdateMessage());
                }
                else BindControl.BindLiteral(lblMsg, Messages.GetAlreadyExistMessage());
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
            }
        }

        #endregion

        #region ListView Event Handling Methods including Slide Paging

        protected void lstRating_PagePropertiesChanged(object sender, EventArgs e)
        {
            LoadEvaluationRating();
        }

        protected void lstRating_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            try
            {
                lstRating.EditIndex = e.NewEditIndex;
                lstRating.InsertItemPosition = InsertItemPosition.None;
                LoadEvaluationRating();
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        protected void lstRating_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListViewItemType.InsertItem ||
                    e.Item.ItemType == ListViewItemType.DataItem)
                {
                    string commandname = e.CommandName.ToLower();
                    switch (commandname)
                    {
                        case "insertitem":
                            AddNewRating(e);
                            break;
                        case "updateitem":
                            UpdateRating(e);
                            break;
                        case "deleteitem":
                            int rId = int.Parse((e.Item.FindControl("hdnRatingId") as HiddenField).Value);
                            DeleteRating(rId);
                            break;
                        case "cancelupdate":
                            CancelUpdate();
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        #endregion

        #region Button Event Handling Methods

        protected void lnkCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("index.aspx");
        }

        protected void lnkAdd_Click(object sender, EventArgs e)
        {
            lstRating.InsertItemPosition = InsertItemPosition.LastItem;
            LoadEvaluationRating();
        }

        #endregion
    }
}