﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.controllers;
using scorecard.implementations;
using scorecard.entities;
using Scorecard.Business.Rules;
using System.Text;
using vb = Microsoft.VisualBasic;
using ScoreCard.Common.Interfaces;
using ScoreCard.Common.Logic;
using ThreeSixty.Evaluation;
using ScoreCard.Common.Config;
using HR.Human.Resources;
using scorecard.interfaces;
using System.Collections;
using System.Text.RegularExpressions;

namespace scorecard.ui.controls
{
    public partial class evaluationSetup : System.Web.UI.UserControl
    {
        #region Member Variables

        IScoreCardArea area;
        IEvaluation evalc;
        IEmployee emp;
        IEvaluationCompetency evalcomp;

        projectmanagement proj;
        structurecontroller org;
        jobtitlesdefinitions job;

        #endregion

        #region Default Constructors

        public evaluationSetup()
        {
            area = new ScoreCardAreaBL();
            evalc = new EvaluationBL();
            evalcomp = new EvaluationCompetencyBL();
            emp = new EmployeeBL();

            org = new structurecontroller(new structureImpl());
            proj = new projectmanagement(new projectImpl());
            job = new jobtitlesdefinitions(new jobtitleImpl());
        }

        #endregion

        #region Page Load Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            ResetLabels();
            if (!IsPostBack)
            {
                LoadAreas();
            }
        }

        #endregion

        #region Databinding methods

        private void ResetLabels()
        {
            lblMsg.Text = "";
            LblError.Text = "";
        }

        void LoadAreas()
        {
            var data = area.GetByStatus(true);
            BindControl.BindDropdown(ddlArea, "AreaName", "AreaId", "-select area -", "-1", data);
        }

        void LoadAreaValues(int areaId)
        {
            int orgUnitId = Util.user.OrgId;
            int compId = Util.user.CompanyId;

            var data = new object();
            string dataValueField = string.Empty;
            string dataTextField = string.Empty;

            if (areaId == ScoreCardAreaConfig.EmployeeAreaId)
            {
                data = emp.GetActive(orgUnitId);
                dataValueField = "EmployeeId";
                dataTextField = "FullName";
            }
            else if (areaId == ScoreCardAreaConfig.JobTitleAreaId)
            {
                data = job.getAllCompanyJobTitles(new company
                {
                    COMPANY_ID = compId
                }).Where(a => a.ACTIVE == true).ToList();

                dataValueField = "ID";
                dataTextField = "NAME";
            }
            else if (areaId == ScoreCardAreaConfig.OrgUnitAreaId)
            {
                data = org.getActiveCompanyOrgStructure(Util.user.CompanyId);
                dataValueField = "ID";
                dataTextField = "ORGUNIT_NAME";
            }
            else if (areaId == ScoreCardAreaConfig.ProjectAreaId)
            {
                data = proj.get(compId, true);
                dataValueField = "PROJECT_ID";
                dataTextField = "PROJECT_NAME";
            }

            BindControl.BindDropdown(ddlAreaValue, dataTextField, dataValueField, "-select value -", "-1", data);
        }

        void LoadEvaluation()
        {
            try
            {
                int areaId = int.Parse(ddlArea.SelectedValue);
                int areaValueId = int.Parse(ddlAreaValue.SelectedValue);
                int companyId = Util.user.CompanyId;

                var data = evalc.GetByStatus(companyId, areaId, areaValueId, true);
                dtPagerlvEvals.Visible = (data.Count > dtPagerlvEvals.PageSize) ? true : false;
                BindControl.BindListView(lvEval, data);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void LoadCompetencies(DropDownList ddlComp, int areaId)
        {
            int companyId = Util.user.CompanyId;
            var data = evalcomp.GetByArea(companyId, areaId, true);
            BindControl.BindDropdown(ddlComp, "CompetencyName", "CompetencyId", "- select competency -", "-1", data);
        }

        public string GetWeight(object weightVal)
        {
            return (weightVal == DBNull.Value || weightVal == null) ? string.Empty :
                string.IsNullOrEmpty(weightVal.ToString()) ?
                string.Empty : string.Format("{0}%", weightVal);
        }

        bool AllHaveWeight(long evaluationId)
        {
            if (lvEval.Items.Count == 0) return false;
            int itemCount = 0;

            foreach (ListViewItem item in lvEval.Items)
            {
                string tempWeight = (item.FindControl("HdnWeightVal") as HiddenField).Value;
                long evalId = long.Parse((item.FindControl("HdnEvaluationId") as HiddenField).Value);
                if (evalId != evaluationId)
                {
                    itemCount += 1;
                    if (string.IsNullOrEmpty(tempWeight))
                        return false;
                }
            }

            return (itemCount == 0) ? false : true;
        }

        bool CanAdd(long evaluationId, decimal weightVal)
        {
            int MAX = 100;
            decimal exclWeight = 0;
            decimal totalWeight = 0;
            decimal subTotalWeight = 0;

            foreach (ListViewItem item in lvEval.Items)
            {
                Literal lblItemError = item.FindControl("LblItemError") as Literal;

                string tempWeight = (item.FindControl("HdnWeightVal") as HiddenField).Value;
                long evalId = long.Parse((item.FindControl("HdnEvaluationId") as HiddenField).Value);
                bool valid = vb.Information.IsNumeric(tempWeight);

                if (!string.IsNullOrEmpty(tempWeight))
                {
                    if (lblItemError != null)
                        lblItemError.Text = valid ? string.Empty : "only numeric value is allowed";

                    if (valid)
                    {
                        decimal weight = decimal.Parse(tempWeight);
                        subTotalWeight += weight;
                    }
                    else if (lblItemError != null)
                        lblItemError.Text = string.Empty;
                }
                else if (lblItemError != null)
                {
                    lblItemError.Text = string.Empty;
                }

                if (valid && evaluationId == evalId)
                    exclWeight = decimal.Parse(tempWeight);
            }

            totalWeight = subTotalWeight - exclWeight + weightVal;
            if (totalWeight > MAX)
            {
                BindControl.BindLiteral(LblError, Messages.TotalWeightCannotExceedHundred);
                return false;
            }

            return true;
        }

        void AddNewEvaluationMeasure(ListViewCommandEventArgs e)
        {
            try
            {
                string strWeight = (e.Item.FindControl("txtWeight") as TextBox).Text;
                var ddlComp = e.Item.FindControl("ddlComp") as DropDownList;

                if (ddlComp.SelectedIndex == 0)
                {
                    BindControl.BindLiteral(LblError, Messages.GetRequiredFieldsMessage(false, "Competency"));
                    return;
                }

                decimal? weight = null;
                if (vb.Information.IsNumeric(strWeight))
                {
                    weight = decimal.Parse(strWeight);
                    if (!CanAdd(0, (decimal)weight))
                    {
                        return;
                    }
                }

                int itemCount = lvEval.Items.Count;
                bool allHaveWeight = AllHaveWeight(0);
                if (!allHaveWeight && weight != null && itemCount > 0)
                {
                    BindControl.BindLiteral(LblError, Messages.NoWeightAllowed);
                    return;
                }
                else if (allHaveWeight && weight == null)
                {
                    BindControl.BindLiteral(LblError, Messages.GetRequiredFieldsMessage(false, "Weight"));
                    return;
                }

                int areaId = int.Parse(ddlArea.SelectedValue);
                int areaValueId = int.Parse(ddlAreaValue.SelectedValue);
                long competencyId = int.Parse(ddlComp.SelectedValue);
                string createdBy = Util.user.LoginId;

                bool added = evalc.AddEvaluation(new Evaluation
                {
                    Active = true,
                    //AreaId = areaId,
                    AreaValueId = areaValueId,
                    CompetencyId = competencyId,
                    CreatedBy = createdBy,
                    WeightVal = weight
                });

                if (added)
                {
                    LoadReadOnly();
                    LoadEvaluation();
                }
                BindControl.BindLiteral(LblError, added ? Messages.GetSaveMessage() : Messages.GetAlreadyExistMessage());
            }
            catch (Exception ex)
            {
                Regex reg = new Regex("duplicate", RegexOptions.IgnoreCase);
                BindControl.BindLiteral(LblError, reg.IsMatch(ex.StackTrace) ? Messages.GetAlreadyExistMessage() : string.Format("{0} {1}", LblError.Text, Messages.GetErrorMessage()));
                Util.LogErrors(ex);
            }
        }

        void LoadReadOnly()
        {
            lvEval.EditIndex = -1;
            lvEval.InsertItemPosition = InsertItemPosition.None;
        }

        void UpdateEvaluation(ListViewDataItem e)
        {
            try
            {
                string strWeight = (e.FindControl("txtWeightVal") as TextBox).Text;
                string evalId = (e.FindControl("HdnEvaluationId") as HiddenField).Value;
                string competenzyId = (e.FindControl("HdnCompetencyId") as HiddenField).Value;
                string areavalueId = (e.FindControl("HdnValueId") as HiddenField).Value;
                //string areaStrId = (e.FindControl("HdnAreaId") as HiddenField).Value;
                bool active = (e.FindControl("chkActive") as CheckBox).Checked;

                if (string.IsNullOrEmpty(evalId))
                {
                    BindControl.BindLiteral(LblError, "The system cannot update this record.");
                    return;
                }

                decimal? weight = null;
                bool isnumeric = vb.Information.IsNumeric(strWeight);
                bool isnull = string.IsNullOrEmpty(strWeight);


                if (isnumeric && !isnull)
                {
                    weight = decimal.Parse(strWeight);
                    if (!CanAdd(long.Parse(evalId), (decimal)weight))
                        return;
                }
                else if (!isnumeric && !isnull)
                {
                    BindControl.BindLiteral(LblError, "Only numeric values are allowed for Weight.");
                    return;
                }

                int itemCount = lvEval.Items.Count;
                bool allHaveWeight = AllHaveWeight(long.Parse(evalId));
                if (!allHaveWeight && weight != null && itemCount > 0)
                {
                    BindControl.BindLiteral(LblError, Messages.NoWeightAllowed);
                    return;
                }
                else if (allHaveWeight && weight == null)
                {
                    BindControl.BindLiteral(LblError, Messages.GetRequiredFieldsMessage(false, "Weight"));
                    return;
                }

                //int areaId = int.Parse(areaStrId);
                int areaValueId = int.Parse(areavalueId);
                long evaluationId = long.Parse(evalId);
                long competencyId = int.Parse(competenzyId);
                string updatedBy = Util.user.LoginId;

                bool updated = evalc.UpdateEvaluation(new Evaluation
                {
                    Active = active,
                    //AreaId = areaId,
                    AreaValueId = areaValueId,
                    CompetencyId = competencyId,
                    EvaluationId = evaluationId,
                    UpdatedBy = updatedBy,
                    WeightVal = weight
                });

                if (updated)
                {
                    LoadReadOnly();
                    LoadEvaluation();
                }
                BindControl.BindLiteral(LblError, updated ? Messages.GetUpdateMessage() : Messages.GetAlreadyExistMessage());
            }
            catch (Exception ex)
            {
                Regex reg = new Regex("duplicate", RegexOptions.IgnoreCase);
                BindControl.BindLiteral(LblError, reg.IsMatch(ex.StackTrace) ? Messages.GetAlreadyExistMessage() : string.Format("{0} {1}", LblError.Text, Messages.GetErrorMessage()));
                Util.LogErrors(ex);
            }
        }

        void DeleteEvaluation(ListViewDataItem e)
        {
            try
            {
                string evalId = (e.FindControl("HdnEvaluationId") as HiddenField).Value;

                if (string.IsNullOrEmpty(evalId))
                {
                    BindControl.BindLiteral(LblError, "The system cannot delete this record.");
                    return;
                }

                long evaluationId = long.Parse(evalId);
                bool deactivated = evalc.DeactivateEvaluation(new Evaluation
                {
                    Active = false,
                    EvaluationId = evaluationId,
                    UpdatedBy = Util.user.LoginId
                });

                if (deactivated)
                {
                    LoadReadOnly();
                    LoadEvaluation();
                }
                BindControl.BindLiteral(LblError, deactivated ? Messages.GetDeleteMessage() : Messages.GetDeleteFailedMessage());
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(LblError, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        void CancelUpdate()
        {
            LoadReadOnly();
            LoadEvaluation();
        }

        void AreaValueNotSelected()
        {
            lvEval.Visible = false;
            BindControl.BindLiteral(LblError, "Select area to start setting up evaluation.");
        }

        #endregion

        #region DropDown Event Handling Methods

        protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            int areaId = ddlArea.SelectedIndex == 0 ? -1 : int.Parse(ddlArea.SelectedValue);
            if (areaId == -1)
            {
                lvEval.Visible = false;
                ddlAreaValue.Items.Clear();
                BindControl.BindLiteral(LblError, "Select area to start setting up evaluation.");
                return;
            }

            LoadAreaValues(areaId);
            if (ddlAreaValue.SelectedIndex == 0)
                lvEval.InsertItemPosition = InsertItemPosition.None;

            LoadEvaluation();           
        }

        protected void ddlAreaValue_SelectedIndexChanged(object sender, EventArgs e)
        {
            int areaId = ddlAreaValue.SelectedIndex == 0 ? -1 : int.Parse(ddlArea.SelectedValue);
            if (areaId == -1)
            {
                lvEval.Visible = false;
                BindControl.BindLiteral(LblError, "Select value to start setting up evaluation.");
                return;
            }

            lvEval.InsertItemPosition = InsertItemPosition.None;
            LoadEvaluation();
        }

        #endregion

        #region Button Event Handling Methods

        protected void btnNew_Click(object sender, EventArgs e)
        {
            if (ddlArea.SelectedIndex == 0 ||
                ddlAreaValue.SelectedIndex == 0)
            {
                BindControl.BindLiteral(LblError, "Please select Area and Area Value first");
                return;
            }

            lvEval.InsertItemPosition = InsertItemPosition.LastItem;
            LoadEvaluation();

            int areaId = int.Parse(ddlArea.SelectedValue);
            var ddlComp = lvEval.InsertItem.FindControl("ddlComp") as DropDownList;
            LoadCompetencies(ddlComp, areaId);
        }

        #endregion

        #region ListView Event Handling Methods

        protected void lvEvals_ItemCanceling(object sender, ListViewCancelEventArgs e)
        {
            CancelUpdate();
        }

        protected void lvEval_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            LblError.Text = "";
            if (e.Item.ItemType == ListViewItemType.InsertItem ||
                e.Item.ItemType == ListViewItemType.DataItem)
            {
                switch (e.CommandName)
                {
                    case "add":
                        AddNewEvaluationMeasure(e);
                        break;
                }
            }
        }

        protected void lvEval_ItemDeleting(object sender, ListViewDeleteEventArgs e)
        {
            ListViewDataItem el = lvEval.Items[e.ItemIndex];
            DeleteEvaluation(el);
        }

        protected void lvEval_ItemUpdating(object sender, ListViewUpdateEventArgs e)
        {
            ListViewDataItem el = lvEval.Items[e.ItemIndex];
            UpdateEvaluation(el);
        }

        protected void lvEval_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            lvEval.EditIndex = e.NewEditIndex;
            lvEval.InsertItemPosition = InsertItemPosition.None;
            LoadEvaluation();
        }

        protected void lvEval_PagePropertiesChanged(object sender, EventArgs e)
        {
            LoadEvaluation();
        }

        #endregion
    }
}