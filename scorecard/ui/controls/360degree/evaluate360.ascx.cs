﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScoreCard.Common.Interfaces;
using HR.Human.Resources;
using ThreeSixty.Evaluation;
using System.Email.Communication;
using scorecard.implementations;
using ScoreCard.Common.Config;
using Scorecard.Business.Rules;
using System.Data;
using Telerik.Web.UI;

namespace scorecard.ui.controls._360degree
{
    public partial class evaluate360 : System.Web.UI.UserControl
    {
        #region Member Variables

        IEvaluationSubmissionList evalList;
        IEvaluationSubmission sub;
        IEvaluationRating rate;
        IEvaluationScore score;
        IBusinessRule busr;

        string radioButtonPrefix = "Rd";
        string groupNamePrefix = "Grp";

        private string Email
        {
            get
            {
                return IsInternal ? string.Empty :
                                    Util.Decrypt(Request.QueryString["UserId"].ToString().Replace(" ", "+"));
            }
        }

        private long SubmissionId
        {
            get
            {
                return IsInternal ? 0 :
                                    long.Parse(Util.Decrypt(Request.QueryString["Id"].ToString().Replace(" ", "+")));
            }
        }

        private int CompanyId
        {
            get
            {
                return IsInternal ? Util.user.CompanyId :
                                    int.Parse(Util.Decrypt(Request.QueryString["CompId"].ToString().Replace(" ", "+")));
            }
        }

        public bool IsInternal { get; set; }

        #endregion

        #region Default Constructors

        public evaluate360()
        {
            evalList = new EvaluationSubmissionListBL();
            sub = new EvaluationSubmissionBL();
            rate = new EvaluationRatingBL();
            score = new EvaluationScoreBL();
            busr = new BusinessRuleBL();
        }

        #endregion

        #region Page Event Handling Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            ClearLabels();
            if (!IsPostBack)
            {
                InitialisePage();
            }
        }

        #endregion

        #region Databinding Methods

        void ClearLabels()
        {
            lblMsg.Text = string.Empty;
            lblError.Text = string.Empty;
        }

        void ClearForm()
        {
            txtOveralComment.Text = string.Empty;
        }

        void InitialisePage()
        {
            int userId = Util.user.UserId;
            string statusIds = ScoreCardAreaConfig.SubmittedId.ToString();

            var data = IsInternal ? sub.GetPendingEvaluation(userId, statusIds) :
                                    sub.GetPendingExternalEvaluation(Email, CompanyId, statusIds);
            MainView.SetActiveView(VwData);
            BindControl.BindListView(lvData, data);
            BindControl.BindLiteral(lblMsg, data.Count > 0 ? string.Empty : Messages.NoRecordsToDisplay);
        }

        private void HeaderRatingNames(int companyId)
        {
            try
            {
                var data = rate.GetByStatus(companyId, true).OrderBy(r => r.RatingValue).ToList();
                if (data.Count > 0)
                {
                    var lastIndex = data.Count - 1;
                    var lowRatingHeader = data[0].RatingDescription;
                    var maxRatingHeader = data[lastIndex].RatingDescription;

                    lblLowRatingHeader.Text = lowRatingHeader;
                    lblMaxRatingHeader.Text = maxRatingHeader;
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void ManageGridColums(Int16 maxRating)
        {
            int columnCount = gdvData.Columns.Count;
            int columnsToRemove = columnCount - maxRating - 2;

            for (int i = 1; i <= columnsToRemove; i++)
            {
                gdvData.Columns[gdvData.Columns.Count - i].Visible = false;
            }
        }

        void FillComment(long evaluataorId)
        {
            try
            {
               var obj = evalList.GetEvaulator(evaluataorId);
               txtOveralComment.Text = obj.OverallComment;
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblError, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        void FillEvaluation(ListViewCommandEventArgs e)
        {
            try
            {
                //getting hiden values
                long submissionId = long.Parse((e.Item.FindControl("hdnSubmissionId") as HiddenField).Value);
                long evaluatorId = long.Parse((e.Item.FindControl("hdnEvaluatorId") as HiddenField).Value);

                Int16 maxRating = Int16.Parse((e.Item.FindControl("hdnMaximumRateValue") as HiddenField).Value);
                int areaId = int.Parse((e.Item.FindControl("hdnAreaId") as HiddenField).Value);
                int areaItemId = int.Parse((e.Item.FindControl("hdnAreaItemId") as HiddenField).Value);

                //assign values to hidden controls
                hdnAreaId.Value = areaId.ToString();
                hdnAreaItemId.Value = areaItemId.ToString();
                hdnEvaluatorId.Value = evaluatorId.ToString();
                hdnSubmissionId.Value = submissionId.ToString();
                hdnMaximumRateValue.Value = maxRating.ToString();

                //assign labels
                lblAreaName.Text = (e.Item.FindControl("lblAreaName") as Literal).Text;
                lblAreaItemName.Text = (e.Item.FindControl("lblAreaItemName") as Literal).Text;
                lblPeriod.Text = (e.Item.FindControl("lblPeriodDate") as Literal).Text;
                lblName.Text = (e.Item.FindControl("lblAreaItemName") as Literal).Text;
                lblMaxRating.Text = maxRating.ToString();

                var data = (areaId == ScoreCardAreaConfig.JobTitleAreaId) ? score.GetJobTitleScores(submissionId, evaluatorId) :
                            (areaId == ScoreCardAreaConfig.EmployeeAreaId) ? score.GetEmployeeScores(submissionId, evaluatorId) :
                            (areaId == ScoreCardAreaConfig.ProjectAreaId) ? score.GetProjectScores(submissionId, evaluatorId) :
                            (areaId == ScoreCardAreaConfig.OrgUnitAreaId) ? score.GetOrgUnitScores(submissionId, evaluatorId) :
                            new List<EvaluationScore>();

                MainView.SetActiveView(VwEvaluate);
                
                BindControl.BindGridView(gdvData, data);
               
                HeaderRatingNames(CompanyId);
                ManageGridColums(maxRating);
                FillComment(evaluatorId);
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblError, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        Int16? GetRating(GridViewRow row, short maxRatingValue)
        {
            Int16? scoreValue = null;
            for (Int16 i = 1; i <= maxRatingValue; i++)
            {
                string rdName = radioButtonPrefix + i.ToString();
                RadioButton rdBtn = row.FindControl(rdName) as RadioButton;
                if (rdBtn != null)
                {
                    if (rdBtn.Checked)
                    {
                        scoreValue = i;
                        return scoreValue;
                    }
                }
            }
            return scoreValue;
        }

        bool EvaluationIsValid()
        {
            int ratingSuppliedCount = 0;
            int numberOfItems = gdvData.Rows.Count;

            Int16 cols = Int16.Parse(hdnMaximumRateValue.Value);
            foreach (GridViewRow gvdrow in gdvData.Rows)
            {
                if (gvdrow.RowType == DataControlRowType.DataRow)
                {
                    for (Int16 i = 1; i <= cols; i++)
                    {
                        string rdName = radioButtonPrefix + i.ToString();
                        RadioButton rdBtn = gvdrow.FindControl(rdName) as RadioButton;
                        if (rdBtn != null)
                        {
                            if (rdBtn.Checked)
                            {
                                ratingSuppliedCount += 1;
                            }
                        }
                    }
                }
            }
            return (ratingSuppliedCount >= numberOfItems);
        }

        DataTable GetDataToSubmit()
        {
            DataTable dT = score.GetSchema();

            object defDbNull = DBNull.Value;
            decimal? decimalNullValue = null;

            long submissionId = long.Parse(hdnSubmissionId.Value);
            long evaluatorId = long.Parse(hdnEvaluatorId.Value);

            Int16 maxRatingValue = Int16.Parse(hdnMaximumRateValue.Value);
            string createdBy = Util.user.LoginId;
            foreach (GridViewRow row in gdvData.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    HiddenField hdnScoreId = row.Cells[0].FindControl("hdnScoreId") as HiddenField;
                    HiddenField hdnEvaluationId = row.Cells[0].FindControl("hdnEvaluationId") as HiddenField;

                    decimal? weightVal = decimalNullValue;
                    decimal? weightedRatingValue = decimalNullValue;

                    long scoreId = long.Parse(hdnScoreId.Value);
                    long evaluationId = long.Parse(hdnEvaluationId.Value);

                    HiddenField hdnWeight = row.FindControl("hdnWeightValue") as HiddenField;
                    if (!string.IsNullOrEmpty(hdnWeight.Value))
                        weightVal = decimal.Parse(hdnWeight.Value);

                    Int16? ratingValue = GetRating(row, maxRatingValue);
                    if (ratingValue != null && weightVal != null)
                    {
                        weightedRatingValue = busr.GetEvaluationWeightedRating((decimal)ratingValue, (decimal)maxRatingValue, (decimal)weightVal);
                    }

                    DataRow nRow = dT.NewRow();
                    nRow["ScoreId"] = scoreId;
                    nRow["SubmissionId"] = submissionId;
                    nRow["EvaluationId"] = evaluationId;
                    nRow["EvaluatorId"] = evaluatorId;

                    if (null == weightVal) nRow["WeightVal"] = defDbNull;
                    else nRow["WeightVal"] = weightVal;

                    if (null == weightedRatingValue) nRow["WeightedRatingValue"] = defDbNull;
                    else nRow["WeightedRatingValue"] = weightedRatingValue;

                    if (ratingValue == null) nRow["RatingValue"] = defDbNull;
                    else nRow["RatingValue"] = ratingValue;

                    nRow["CreatedBy"] = createdBy;
                    nRow["UpdatedBy"] = createdBy;
                    dT.Rows.Add(nRow);
                    nRow = null;
                }
            }

            dT.AcceptChanges();
            return dT;
        }

        void SubmitEvaluation(bool saveForLater)
        {
            try
            {
                bool isValid = saveForLater ? true :
                                              EvaluationIsValid();

                if (!isValid)
                {
                    BindControl.BindLiteral(lblError, "Please supply rating on all competencies for you to proceed.");
                    return;
                }

                bool submittedScore = saveForLater ? false : true;
                long evaluatorId = long.Parse(hdnEvaluatorId.Value);
                long submissionId = long.Parse(hdnSubmissionId.Value);

                DataTable dT = GetDataToSubmit();

                string comment = txtOveralComment.Text;
                string xmlData = Common.GetXML(dT, "Data", "Record");
                bool saved = score.SubmitScore(submissionId, evaluatorId, submittedScore, comment, xmlData);
                if (saved)
                {
                    if (!saveForLater) InitialisePage();
                    MainView.SetActiveView(VwData);
                    BindControl.BindLiteral(lblMsg, Messages.GetSaveMessage());
                }
                else
                {
                    BindControl.BindLiteral(lblError, Messages.GetSaveFailedGeneralMessage("360<sup>0</sup> Evaluation"));
                }
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblError, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        #endregion

        #region ListView Event Handling Methods

        protected void lvData_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Evaluate":
                    ClearForm();
                    FillEvaluation(e);
                    break;
            }
        }

        #endregion

        #region GridView Event Handling Methods

        protected void gdvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string itemIndex = e.Row.DataItemIndex.ToString();
                    string groupName = groupNamePrefix + itemIndex;

                    Int16 maxRating = Int16.Parse(lblMaxRating.Text);
                    Int16? ratingValue = null;

                    HiddenField hdnRatingValue = e.Row.FindControl("hdnRatingValue") as HiddenField;

                    if (hdnRatingValue != null)
                        if (!string.IsNullOrEmpty(hdnRatingValue.Value))
                            ratingValue = Int16.Parse(hdnRatingValue.Value);

                    for (Int16 i = 1; i <= maxRating; i++)
                    {
                        string rdName = radioButtonPrefix + i.ToString();
                        RadioButton rdBtn = e.Row.FindControl(rdName) as RadioButton;
                        if (rdBtn != null)
                        {
                            rdBtn.GroupName = groupName;
                            if (ratingValue == i)
                                rdBtn.Checked = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblError, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        #endregion

        #region Button Event Handling Methods

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            SubmitEvaluation(false);
        }

        protected void btnSaveForLater_Click(object sender, EventArgs e)
        {
            SubmitEvaluation(true);
        }

        protected void lnkCancel_Click(object sender, EventArgs e)
        {
            MainView.SetActiveView(VwData);
        }

        #endregion
    }
}