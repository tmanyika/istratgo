﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="evaluationSetup.ascx.cs"
    Inherits="scorecard.ui.controls.evaluationSetup" %>
<%@ Import Namespace="scorecard.implementations" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:UpdatePanel ID="UpdatePanelEval" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <h1>
            360<sup>0</sup> Evaluation Measures
        </h1>
        <fieldset runat="server" id="pnlContactInfo">
            <legend>Setup 360<sup>0</sup> Evaluation Measures</legend>
            <p>
                <label for="sf">
                    Select Area :
                </label>
                &nbsp;<span class="field_desc"><asp:DropDownList runat="server" ID="ddlArea" AutoPostBack="True"
                    OnSelectedIndexChanged="ddlArea_SelectedIndexChanged" Width="130px" CausesValidation="false" />
                    <br />
                </span>
            </p>
            <p>
                <label for="sf">
                    Select Value:
                </label>
                &nbsp;<span class="field_desc"><asp:DropDownList runat="server" ID="ddlAreaValue"
                    AutoPostBack="True" Width="130px" OnSelectedIndexChanged="ddlAreaValue_SelectedIndexChanged" />
                </span>
            </p>
        </fieldset>
        <asp:ListView ID="lvEval" runat="server" OnItemCommand="lvEval_ItemCommand" OnItemEditing="lvEval_ItemEditing"
            OnPagePropertiesChanged="lvEval_PagePropertiesChanged" OnItemCanceling="lvEvals_ItemCanceling"
            OnItemDeleting="lvEval_ItemDeleting" OnItemUpdating="lvEval_ItemUpdating">
            <LayoutTemplate>
                <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                    <thead>
                        <tr id="trFirst" runat="server">
                            <td style="width: 200px; text-align: left">
                                Area
                            </td>
                            <td style="width: 250px; text-align: left">
                                Competency
                            </td>
                            <td align="left" style="width: 110px; text-align: left">
                                Weight
                            </td>
                            <td style="width: 90px">
                                Active
                            </td>
                            <td width="80px">
                                Edit
                            </td>
                            <td width="80px">
                                Delete
                            </td>
                        </tr>
                        <tr id="ItemPlaceHolder" runat="server">
                        </tr>
                    </thead>
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tbody>
                    <tr class="odd">
                        <td>
                            <%# Eval("Area.AreaName")%><asp:HiddenField ID="HdnEvaluationId" runat="server" Value='<%# Bind("EvaluationId")%>' />
                            <asp:HiddenField ID="HdnAreaId" runat="server" Value='<%# Bind("Area.AreaId")%>' />
                            <asp:HiddenField ID="HdnValueId" runat="server" Value='<%# Bind("AreaValueId")%>' />
                            <asp:HiddenField ID="HdnCompetencyId" runat="server" Value='<%# Bind("CompetencyId")%>' />
                            <asp:HiddenField ID="HdnWeightVal" runat="server" Value='<%# Bind("WeightVal")%>' />
                        </td>
                        <td>
                            <%# Eval("Competency.CompetencyName")%>
                        </td>
                        <td>
                            <%# GetWeight(Eval("WeightVal"))%>
                        </td>
                        <td>
                            <%# Common.GetBooleanYesNo(Eval("Active"))%>
                        </td>
                        <td>
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkEdit" CommandName="edit" class="ui-state-default ui-corner-all"
                                    title="Edit Record"><span class="ui-icon ui-icon-pencil">
                        </span></asp:LinkButton>
                            </ul>
                        </td>
                        <td>
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkDelete" CommandName="delete" class="ui-state-default ui-corner-all"
                                    title="Delete Record" OnClientClick="return confirmDelete();"><span class="ui-icon ui-icon-trash">
                                    </span>
                                </asp:LinkButton>
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </ItemTemplate>
            <EditItemTemplate>
                <tbody>
                    <tr class="odd">
                        <td>
                            <%# Eval("Area.AreaName")%>
                            <asp:HiddenField ID="HdnEvaluationId" runat="server" Value='<%# Bind("EvaluationId")%>' />
                            <asp:HiddenField ID="HdnAreaId" runat="server" Value='<%# Bind("Area.AreaId")%>' />
                            <asp:HiddenField ID="HdnValueId" runat="server" Value='<%# Bind("AreaValueId")%>' />
                            <asp:HiddenField ID="HdnCompetencyId" runat="server" Value='<%# Bind("CompetencyId")%>' />
                            <asp:HiddenField ID="HdnWeightVal" runat="server" Value='<%# Bind("WeightVal")%>' />
                        </td>
                        <td>
                            <%# Eval("Competency.CompetencyName")%>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtWeightVal" Text='<%# Eval("WeightVal")%>' ValidationGroup="Edit" />
                            <asp:CompareValidator ID="CVD" runat="server" ErrorMessage="only numeric value allowed."
                                ValidationGroup="Edit" Display="Dynamic" ForeColor="Red" ControlToValidate="txtWeightVal"
                                Operator="DataTypeCheck" Type="Double"></asp:CompareValidator>
                            <asp:Literal ID="LblItemError" runat="server"></asp:Literal>
                        </td>
                        <td>
                            <asp:CheckBox ID="chkActive" runat="server" Checked='<%# Eval("Active")%>' />
                        </td>
                        <td>
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkSave" CommandName="update" class="ui-state-default ui-corner-all"
                                    title="Update Record" ValidationGroup="Edit"><span class="ui-icon ui-icon-disk">
                                     </span>
                                </asp:LinkButton>
                            </ul>
                        </td>
                        <td>
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkCancelUpdate" CommandName="cancel" class="ui-state-default ui-corner-all"
                                    title="Cancel Update" CausesValidation="false"><span class="ui-icon ui-icon-cancel">
                                    </span>
                                </asp:LinkButton>
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </EditItemTemplate>
            <InsertItemTemplate>
                <tbody>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td align="left" valign="top">
                            <asp:DropDownList runat="server" ID="ddlComp" ValidationGroup="Insert" Width="200px" />
                            <asp:RequiredFieldValidator ID="RqdArea" runat="server" ErrorMessage="competency is required"
                                ValidationGroup="Insert" ControlToValidate="ddlComp" InitialValue="-1" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" valign="top">
                            <asp:TextBox runat="server" ID="txtWeight" ValidationGroup="Insert" Width="100px" />
                            <asp:CompareValidator ID="CVD" runat="server" ErrorMessage="only numeric value allowed."
                                ValidationGroup="Insert" Display="Dynamic" ForeColor="Red" ControlToValidate="txtWeight"
                                Operator="DataTypeCheck" Type="Double"></asp:CompareValidator>
                        </td>
                        <td align="left" valign="top">
                            <asp:CheckBox ID="chkActive" runat="server" Checked="true" Enabled="false" />
                        </td>
                        <td valign="top">
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkSave" class="ui-state-default ui-corner-all"
                                    title="Save this Record" CommandName="add" ValidationGroup="Insert"><span class="ui-icon ui-icon-disk">
                        </span></asp:LinkButton>
                            </ul>
                        </td>
                        <td valign="top">
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkCancel" class="ui-state-default ui-corner-all"
                                    title="Cancel Save " CommandName="cancel"><span class="ui-icon ui-icon-cancel">
                        </span></asp:LinkButton>
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </InsertItemTemplate>
            <EmptyDataTemplate>
                <span style="color: Red">
                    <asp:Literal ID="lblNoData" runat="server" Text="There are no competencies to display for the selected area
                    and selected value."></asp:Literal></span>
            </EmptyDataTemplate>
        </asp:ListView>
        <br />
        <asp:DataPager ID="dtPagerlvEvals" runat="server" PagedControlID="lvEval" PageSize="1000">
            <Fields>
                <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True" ShowNextPageButton="False"
                    ShowPreviousPageButton="False" />
                <asp:NumericPagerField />
                <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True" ShowNextPageButton="False"
                    ShowPreviousPageButton="False" />
            </Fields>
        </asp:DataPager>
        <div>
            <span style="color: Red">
                <asp:Literal ID="LblError" runat="server" /></span>
        </div>
        <div>
            <p>
                <br />
                <br />
                <asp:Label ID="lblMsg" runat="server" Style="color: #FF3300" />
                <br />
                <br />
            </p>
        </div>
        <div id="tabs-3">
            <p>
                <asp:Button ID="btnNew" runat="server" CssClass="button" Text="Add New Evaluation"
                    OnClick="btnNew_Click" CausesValidation="false" />
                <asp:Button ID="lnkCancel0" runat="server" CssClass="button" Text="Done" CausesValidation="false"
                    PostBackUrl="~/ui/index.aspx" />
            </p>
        </div>
        <asp:UpdateProgress ID="UpdateProgressEval" runat="server" AssociatedUpdatePanelID="UpdatePanelEval">
            <ProgressTemplate>
                <div>
                    <asp:Image ID="imgOrg" runat="server" ImageUrl="~/ui/images/icons/activity.gif" AlternateText="Please wait..">
                    </asp:Image></div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
