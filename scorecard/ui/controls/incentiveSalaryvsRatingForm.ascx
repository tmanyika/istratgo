﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="incentiveSalaryvsRatingForm.ascx.cs"
    Inherits="scorecard.ui.reports.incentiveSalaryvsRatingForm" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:UpdatePanel ID="UpdatePanelData" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div>
            <h1>
                Salary vs Rating Report
            </h1>
        </div>
        <div>
            <fieldset id="pnlInfo">
                <legend>Report Criteria </legend>
                <p>
                    <label for="sf">
                        Organisation Unit:
                    </label>
                    <span class="field_desc">&nbsp;<asp:DropDownList runat="server" ID="ddOrgUnit" ValidationGroup="Form"
                        DataTextField="SalaryTypeName" DataValueField="SalaryTypeId" />
                    </span>
                </p>
                <p>
                    <label for="sf">
                        Salary Type:
                    </label>
                    <span class="field_desc">&nbsp;<asp:DropDownList runat="server" ID="ddSalyType" ValidationGroup="Form"
                        DataTextField="SalaryTypeName" DataValueField="SalaryTypeId" />
                        <asp:RequiredFieldValidator ID="rqdSalType" runat="server" ControlToValidate="ddSalyType"
                            Display="Dynamic" ErrorMessage="salary type is required" InitialValue="-1" ValidationGroup="Form"
                            ForeColor="Red"></asp:RequiredFieldValidator>
                    </span>
                </p>
                <p>
                    <label for="sf">
                        Start Date:
                    </label>
                    <span class="field_desc">&nbsp;<asp:TextBox runat="server" ID="txtStartDate" Text=""
                        ValidationGroup="Form" />
                        <asp:CalendarExtender ID="txtStartDate_CalendarExtender" runat="server" Enabled="True"
                            CssClass="calendar" Format="MM/dd/yyyy" TargetControlID="txtStartDate">
                        </asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="rqdStartDate" runat="server" ControlToValidate="txtStartDate"
                            Display="Dynamic" ErrorMessage="start date is required" ValidationGroup="Form"
                            ForeColor="Red"></asp:RequiredFieldValidator>
                    </span>
                </p>
                <p>
                    <label for="sf">
                        End Date:
                    </label>
                    <span class="field_desc">&nbsp;<asp:TextBox runat="server" ID="txtEndDate" Text=""
                        ValidationGroup="Form" />
                        <asp:CalendarExtender ID="txtEndDate_CalendarExtender" runat="server" Enabled="True"
                            CssClass="calendar" Format="MM/dd/yyyy" TargetControlID="txtEndDate">
                        </asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="rqdEndDate" runat="server" ControlToValidate="txtEndDate"
                            Display="Dynamic" ErrorMessage="end date is required" ValidationGroup="Form"
                            ForeColor="Red"></asp:RequiredFieldValidator>
                    </span>
                </p>
                <p class="errorMsg">
                    <label for="lblMsg">
                    </label>
                    <asp:Literal ID="lblMsg" runat="server"></asp:Literal>
                </p>
                <p>
                    <label>
                        &nbsp;
                    </label>
                    <asp:Button ID="btnViewReport" runat="server" CssClass="button tooltip" Text="View Report"
                        ValidationGroup="Form" OnClick="btnViewReport_Click" CausesValidation="true" />
                </p>
            </fieldset>
        </div>
        <asp:UpdateProgress ID="UpdateProgressData" runat="server" AssociatedUpdatePanelID="UpdatePanelData">
            <ProgressTemplate>
                <img runat="server" src="~/ui/images/icons/activity.gif" alt="Please wait..." id="imgPrg" />
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
