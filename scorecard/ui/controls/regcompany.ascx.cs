﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.implementations;
using scorecard.entities;
using scorecard.controllers;

namespace scorecard.ui.controls
{
    public partial class regcompany : System.Web.UI.UserControl
    {
        #region Properties

        public bool IsAccountActivation
        {
            get;
            set;
        }

        #endregion

        #region Private Variables

        scorecard.controllers.registration _registration;

        #endregion

        #region Default Constructor

        public regcompany()
        {
            _registration = new scorecard.controllers.registration(new customerImpl());
        }

        #endregion

        #region Page Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                initialisePage();
            }
        }

        #endregion

        #region Databinding Methods

        void initialisePage()
        {
            try
            {
                loadIndustryTypes();
                loadCountryTypes();
                clearTextBoxes();
                loadCompanyInfo();
                setButtonCaption();
            }
            catch (Exception e)
            {
                BindControl.BindLiteral(lblError, string.Format("<span class='error'>{0}</span>", Messages.GetErrorMessage()));
                Util.LogErrors(e);
            }
        }

        private void setButtonCaption()
        {
            btnCancelComplete.Text = IsAccountActivation ? "Skip >>" : "Cancel";
        }

        void loadIndustryTypes()
        {
            var indTypes = _registration.appsettings.getChildSettings(new entities.applicationsettings { PARENT_ID = Util.getTypeDefinitionsIndustryTypeParentID() });
            BindControl.BindDropdown(ddlIndustry, indTypes);
        }

        void loadCountryTypes()
        {
            var country = _registration.appsettings.getChildSettings(new entities.applicationsettings { PARENT_ID = Util.getTypeDefinitionsCountryTypeParentID() });
            BindControl.BindDropdown(ddlCountry, country);
        }

        #endregion

        #region Utility Methods

        void clearTextBoxes()
        {
            txtCompanyName.Value = "";
            txtContactTelephone.Value = "";
            txtPostalAddress.Value = "";
            txtRegistrationNo.Value = "";
            txtVatNo.Value = "";
        }

        void loadCompanyInfo()
        {
            int companyId = Util.user.CompanyId;
            company obj = new companyregistration(new companyImpl()).getRegisteredInfo(new company { COMPANY_ID = companyId });

            txtCompanyName.Value = obj.COMPANY_NAME;
            txtContactTelephone.Value = obj.CONTACT_NO;
            txtPostalAddress.Value = obj.ADDRESS;
            txtRegistrationNo.Value = obj.REGISTRATION_NO;
            txtVatNo.Value = obj.VAT_NO;
            if (obj.COUNTRY != null && ddlCountry.Items.FindByValue(obj.COUNTRY.ToString()) != null)
                ddlCountry.SelectedValue = obj.COUNTRY.ToString();
            if (obj.INDUSTRY != null && ddlIndustry.Items.FindByValue(obj.INDUSTRY.ToString()) != null)
                ddlIndustry.SelectedValue = obj.INDUSTRY.ToString();
        }

        void updateCompany()
        {
            try
            {
                company comp = new company
                {
                    COMPANY_ID = Util.user.CompanyId,
                    REGISTRATION_NO = txtRegistrationNo.Value.Trim(),
                    COMPANY_NAME = txtCompanyName.Value.Trim(),
                    COUNTRY = int.Parse(ddlCountry.SelectedValue),
                    INDUSTRY = int.Parse(ddlIndustry.SelectedValue),
                    ADDRESS = txtPostalAddress.Value.Trim(),
                    CONTACT_NO = txtContactTelephone.Value,
                    VAT_NO = txtVatNo.Value,
                    ACTIVE = true
                };

                int result = new companyregistration(new companyImpl()).updateRegisteredCompany(comp);
                string msg = result > 0 ? Messages.GetUpdateMessage() : Messages.GetUpdateFailedMessage();
                if (IsAccountActivation)
                {
                    Response.Redirect("index.aspx", true);
                }
                else
                {
                    BindControl.BindLiteral(lblError, msg);
                }
            }
            catch (Exception e)
            {
                BindControl.BindLiteral(lblError, Messages.GetErrorMessage());
                Util.LogErrors(e);
            }
        }

        #endregion

        #region Update Company Details

        protected void btnComplete_Click(object sender, EventArgs e)
        {
            updateCompany();
        }

        #endregion

    }
}