﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.implementations;
using scorecard.interfaces;
using scorecard.entities;
using ms = Microsoft.VisualBasic;

namespace scorecard.ui.controls.scoring
{
    public partial class companyScoreRating : System.Web.UI.UserControl
    {
        #region Properties

        int CompId
        {
            get { return Util.user.CompanyId; }
        }

        #endregion

        #region Private Variables

        IScoreRating objR;

        #endregion

        #region Default Constructor

        public companyScoreRating()
        {
            objR = new ScoreRatingBL();
        }

        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMsg.Text = "";
            if (!IsPostBack)
            {
                InitialisePage();
            }
        }

        #endregion

        #region Utility Methods

        void InitialisePage()
        {
            LoadRating();
        }

        void LoadRating()
        {
            try
            {
                var data = objR.GetByStatus(CompId, true);
                BindControl.BindListView(lstRating, data);
                if (data.Count <= pager.PageSize) pager.Visible = false;
                if (data.Count <= 0) lstRating.InsertItemPosition = InsertItemPosition.LastItem;
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        public string GetBoolean(object boolVal)
        {
            if (boolVal == DBNull.Value) return "Unknown";
            return bool.Parse(boolVal.ToString()) ? "Yes" : "No";
        }

        void CancelUpdate()
        {
            lstRating.EditIndex = -1;
            lstRating.InsertItemPosition = InsertItemPosition.None;
            LoadRating();
        }

        void DeleteRating(int ratingId)
        {
            try
            {
                string updatedBy = Util.user.FullName;
                ScoreRating obj = new ScoreRating
                {
                    Active = false,
                    RatingId = ratingId,
                    UpdatedBy = updatedBy
                };

                bool deleted = objR.DeactivateRating(obj);
                if (deleted) LoadRating();
                BindControl.BindLiteral(lblMsg, deleted ? Messages.GetDeleteMessage() : Messages.GetDeleteFailedMessage());
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        void AddNewRating(ListViewCommandEventArgs e)
        {
            try
            {
                TextBox txtDesc = e.Item.FindControl("txtDescI") as TextBox;
                TextBox txtRatingValueI = e.Item.FindControl("txtRatingValueI") as TextBox;
                //TextBox txtMinValueI = e.Item.FindControl("txtMinimumI") as TextBox;
                //TextBox txtMaxValueI = e.Item.FindControl("txtMaximumI") as TextBox;
                CheckBox chkActiveE = e.Item.FindControl("chkActiveI") as CheckBox;

                if (!ms.Information.IsNumeric(txtRatingValueI.Text) //||
                    //   !ms.Information.IsNumeric(txtMinValueI.Text) ||
                    //   !ms.Information.IsNumeric(txtMaxValueI.Text)
                    )
                {
                    BindControl.BindLiteral(lblMsg, "Minimum, Maximum and Rating should be a numeric");
                    return;
                }

                decimal? minValue = null;// decimal.Parse(txtMinValueI.Text);
                decimal? maxValue = null;// decimal.Parse(txtMaxValueI.Text);
                decimal rateVal = decimal.Parse(txtRatingValueI.Text);
                string userName = Util.user.FullName;
                string desc = txtDesc.Text.Trim();

                if (rateVal > Util.MaximumScoreRatingValues)
                {
                    BindControl.BindLiteral(lblMsg, Messages.YouCannotExceed(string.Format("{0} rating.", Util.MaximumScoreRatingValues)));
                    return;
                }

                ScoreRating obj = new ScoreRating
                {
                    Active = true,
                    CompanyId = CompId,
                    CreatedBy = userName,
                    DateCreated = DateTime.Now,
                    DateUpdated = DateTime.Now,
                    MinimumValue = minValue,
                    MaximumValue = maxValue,
                    RatingDescription = desc,
                    RatingId = 0,
                    RatingValue = rateVal,
                    UpdatedBy = userName
                };

                bool saved = objR.AddRating(obj);
                if (saved)
                {
                    lstRating.InsertItemPosition = InsertItemPosition.None;
                    LoadRating();
                    BindControl.BindLiteral(lblMsg, Messages.GetSaveMessage());
                }
                else BindControl.BindLiteral(lblMsg, Messages.GetAlreadyExistMessage());
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
            }
        }

        void UpdateRating(ListViewCommandEventArgs e)
        {
            try
            {
                TextBox txtDesc = e.Item.FindControl("txtDescE") as TextBox;
                TextBox txtVal = e.Item.FindControl("txtRatingValueE") as TextBox;
                //TextBox txtMinValue = e.Item.FindControl("txtMinimumE") as TextBox;
                //TextBox txtMaxValue = e.Item.FindControl("txtMaximumE") as TextBox;
                HiddenField hdnRatingId = e.Item.FindControl("hdnRatingId") as HiddenField;
                CheckBox chkActiveE = e.Item.FindControl("chkActiveE") as CheckBox;

                if (!ms.Information.IsNumeric(txtVal.Text)
                    //||
                    // !ms.Information.IsNumeric(txtMinValue.Text) ||
                    // !ms.Information.IsNumeric(txtMaxValue.Text)
                    )
                {
                    BindControl.BindLiteral(lblMsg, "Minimum, Maximum and Rating should be a numeric");
                    return;
                }

                string desc = txtDesc.Text.Trim();
                string userName = Util.user.FullName;
                int ratingId = int.Parse(hdnRatingId.Value);
                decimal rateVal = decimal.Parse(txtVal.Text);
                decimal? minValue = null;// decimal.Parse(txtMinValue.Text);
                decimal? maxValue = null;// decimal.Parse(txtMaxValue.Text);

                if (rateVal > Util.MaximumScoreRatingValues)
                {
                    BindControl.BindLiteral(lblMsg, Messages.YouCannotExceed(string.Format("{0} rating.", Util.MaximumScoreRatingValues)));
                    return;
                }

                ScoreRating obj = new ScoreRating
                {
                    Active = true,
                    CompanyId = CompId,
                    CreatedBy = userName,
                    DateCreated = DateTime.Now,
                    DateUpdated = DateTime.Now,
                    MinimumValue = minValue,
                    MaximumValue = maxValue,
                    RatingDescription = desc,
                    RatingId = ratingId,
                    RatingValue = rateVal,
                    UpdatedBy = userName
                };

                bool saved = objR.UpdateRating(obj);
                if (saved)
                {
                    lstRating.EditIndex = -1;
                    lstRating.InsertItemPosition = InsertItemPosition.None;
                    LoadRating();
                    BindControl.BindLiteral(lblMsg, Messages.GetUpdateMessage());
                }
                else BindControl.BindLiteral(lblMsg, Messages.GetAlreadyExistMessage());
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
            }
        }

        #endregion

        #region ListView Event Handling Methods including Slide Paging

        protected void PagerCommand(object sender, DataPagerCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Next":
                    //guard against going off the end of the list
                    e.NewStartRowIndex = Math.Min(e.Item.Pager.StartRowIndex + e.Item.Pager.MaximumRows, e.Item.Pager.TotalRowCount - e.Item.Pager.MaximumRows);
                    e.NewMaximumRows = e.Item.Pager.MaximumRows;
                    break;
                case "Previous":
                    //guard against going off the begining of the list
                    e.NewStartRowIndex = Math.Max(0, e.Item.Pager.StartRowIndex - e.Item.Pager.MaximumRows);
                    e.NewMaximumRows = e.Item.Pager.MaximumRows;
                    break;
                case "Last":
                    //the
                    e.NewStartRowIndex = e.Item.Pager.TotalRowCount - e.Item.Pager.MaximumRows;
                    e.NewMaximumRows = e.Item.Pager.MaximumRows;
                    break;
                case "First":
                default:
                    e.NewStartRowIndex = 0;
                    e.NewMaximumRows = e.Item.Pager.MaximumRows;
                    break;
            }
        }

        protected void CurrentPageChanged(object sender, EventArgs e)
        {
            TextBox txtCurrentPage = sender as TextBox;
            DataPager pager = this.lstRating.FindControl("pager") as DataPager;
            int startRowIndex = Math.Min((int.Parse(txtCurrentPage.Text) - 1) * pager.MaximumRows, pager.TotalRowCount - pager.MaximumRows);
            pager.SetPageProperties(startRowIndex, pager.MaximumRows, true);
        }

        protected void lstRating_PagePropertiesChanged(object sender, EventArgs e)
        {
            LoadRating();
        }

        protected void lstRating_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            try
            {
                lstRating.EditIndex = e.NewEditIndex;
                lstRating.InsertItemPosition = InsertItemPosition.None;
                LoadRating();
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        protected void lstRating_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListViewItemType.InsertItem ||
                    e.Item.ItemType == ListViewItemType.DataItem)
                {
                    string commandname = e.CommandName.ToLower();
                    switch (commandname)
                    {
                        case "insertitem":
                            AddNewRating(e);
                            break;
                        case "updateitem":
                            UpdateRating(e);
                            break;
                        case "deleteitem":
                            int rId = int.Parse((e.Item.FindControl("hdnRatingId") as HiddenField).Value);
                            DeleteRating(rId);
                            break;
                        case "cancelupdate":
                            CancelUpdate();
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        #endregion

        #region Button Event Handling Methods

        protected void lnkCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("index.aspx");
        }

        protected void lnkAdd_Click(object sender, EventArgs e)
        {
            lstRating.InsertItemPosition = InsertItemPosition.LastItem;
            LoadRating();
        }

        #endregion
    }
}