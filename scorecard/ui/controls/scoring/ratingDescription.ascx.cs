﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.implementations;
using scorecard.interfaces;

namespace scorecard.ui.controls.scoring
{
    public partial class ratingDescription : System.Web.UI.UserControl
    {
        #region Properties

        int CompId
        {
            get { return Util.user.CompanyId; }
        }

        #endregion

        #region Private Variables

        IScoreRating objR;

        #endregion

        #region Default Constructor

        public ratingDescription()
        {
            objR = new ScoreRatingBL();
        }

        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMsg.Text = "";
            if (!IsPostBack)
            {
                LoadRating();
            }
        }

        #endregion

        #region Databinding Methods

        void LoadRating()
        {
            try
            {
                var data = objR.GetByStatus(CompId, true);
                BindControl.BindRepeater(rptData, data);
                rptData.Visible = (data.Count > 0);
                BindControl.BindLiteral(lblMsg, (data.Count <= 0) ? Messages.NoScoreRatingSetup : string.Empty);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        #endregion
    }
}