﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ratingDescription.ascx.cs"
    Inherits="scorecard.ui.controls.scoring.ratingDescription" %>
<%@ Import Namespace="scorecard.implementations" %>
<div>
    <asp:Repeater ID="rptData" runat="server">
        <HeaderTemplate>
            <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                <thead>
                    <tr>
                        <td style="width: 20%">
                            Rating
                        </td>
                        <td style="width: 80%">
                            Description
                        </td>
                    </tr>
                </thead>
                <tbody>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td align="left">
                    <%# Eval("RatingValue")%>
                </td>
                <td align="left">
                    <%# Eval("RatingDescription")%>
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </tbody> </table>
        </FooterTemplate>
    </asp:Repeater>
    <p class="errorMsg">
        <asp:Literal ID="lblMsg" runat="server"></asp:Literal>
    </p>
</div>
