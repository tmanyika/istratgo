﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.implementations;
using scorecard.entities;

namespace scorecard.ui.controls
{
    public partial class extheader : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            UserInfo user = Session["UserInfo"] as UserInfo;
            lblloggedInText.Text = string.Format("Welcome, <b>{0}</b> ({1})", user.FullName, user.Email);
        }
    }
}