﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.implementations;
using ScoreCard.Common.Interfaces;
using ScoreCard.Common.Logic;
using ScoreCard.Common.Entities;

namespace scorecard.ui.controls.sysmanagement
{
    public partial class periods : System.Web.UI.UserControl
    {
        IPeriod prd;

        int orgUnitId = Util.user.OrgId;
        int compId = Util.user.CompanyId;

        public periods()
        {
            prd = new PeriodBL();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ResetLabels();
            if (!IsPostBack)
            {
                LoadPeriods();
            }
        }

        void ResetLabels()
        {
            lblMsg.Text = "";
            LblError.Text = "";
        }

        void LoadPeriods()
        {
            try
            {
                var data = prd.GetByStatus(true);
                dtPagerlvData.Visible = (data.Count > dtPagerlvData.PageSize);
                BindControl.BindListView(lvData, data);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            lvData.InsertItemPosition = InsertItemPosition.LastItem;
            LoadPeriods();
        }

        void CreateNewIncentive(ListViewCommandEventArgs e)
        {
            try
            {
                string description = (e.Item.FindControl("txtDescription") as TextBox).Text;
                string name = (e.Item.FindControl("txtName") as TextBox).Text;
                string numofMonths = (e.Item.FindControl("txtNumOfMonths") as TextBox).Text;

                if (string.IsNullOrEmpty(name))
                {
                    BindControl.BindLiteral(LblError, Messages.GetRequiredFieldsMessage(true, "Name"));
                    return;
                }

                int? defInt = null;
                int? months = string.IsNullOrEmpty(numofMonths) ? defInt : int.Parse(numofMonths);
                bool added = prd.Add(new Period
                {
                    Active = true,
                    CreatedBy = Util.user.LoginId,
                    NumberOfMonths = months,
                    PeriodDescription = description,
                    PeriodName = name
                });

                if (added)
                {
                    LoadReadOnly();
                    LoadPeriods();
                }
                BindControl.BindLiteral(LblError, added ? Messages.GetSaveMessage() : Messages.GetAlreadyExistMessage());
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void LoadReadOnly()
        {
            lvData.EditIndex = -1;
            lvData.InsertItemPosition = InsertItemPosition.None;
        }

        void UpdateIncentive(ListViewDataItem e)
        {
            try
            {
                string description = (e.FindControl("txtEditDescription") as TextBox).Text;
                string name = (e.FindControl("txtEditName") as TextBox).Text;
                string numofMonths = (e.FindControl("txtEditNumOfMonths") as TextBox).Text;

                var hdnPeriodId = e.FindControl("hdnPeriodId") as HiddenField;

                if (string.IsNullOrEmpty(hdnPeriodId.Value))
                {
                    BindControl.BindLiteral(LblError, Messages.GetUpdateFailedMessage());
                    return;
                }

                if (string.IsNullOrEmpty(name))
                {
                    BindControl.BindLiteral(LblError, Messages.GetRequiredFieldsMessage(true, "Name"));
                    return;
                }

                int? defInt = null;
                int periodId = int.Parse(hdnPeriodId.Value);
                int? months = string.IsNullOrEmpty(numofMonths) ? defInt : int.Parse(numofMonths);
                bool updated = prd.Update(new Period
                {
                    Active = true,
                    NumberOfMonths = months,
                    PeriodDescription = description,
                    PeriodName = name,
                    PeriodId = periodId,
                    UpdatedBy = Util.user.LoginId
                });

                if (updated)
                {
                    LoadReadOnly();
                    LoadPeriods();
                }
                BindControl.BindLiteral(LblError, updated ? Messages.GetUpdateMessage() : Messages.GetAlreadyExistMessage());
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void DeactivateIncentive(ListViewDataItem e)
        {
            try
            {
                var hdnPeriodId = e.FindControl("hdnPeriodId") as HiddenField;
                if (string.IsNullOrEmpty(hdnPeriodId.Value))
                {
                    BindControl.BindLiteral(LblError, Messages.GetDeleteFailedMessage());
                    return;
                }

                int periodId = int.Parse(hdnPeriodId.Value);
                bool deactivated = prd.Deactivate(new Period
                {
                    Active = false,
                    PeriodId = periodId,
                    UpdatedBy = Util.user.LoginId
                });

                if (deactivated)
                {
                    LoadReadOnly();
                    LoadPeriods();
                }
                BindControl.BindLiteral(LblError, deactivated ? Messages.GetDeleteMessage() : Messages.GetDeleteFailedMessage());
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void CancelUpdate()
        {
            LoadReadOnly();
            LoadPeriods();
        }

        protected void lvData_ItemCanceling(object sender, ListViewCancelEventArgs e)
        {
            CancelUpdate();
        }

        protected void lvData_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            LblError.Text = "";
            if (e.Item.ItemType == ListViewItemType.InsertItem ||
                e.Item.ItemType == ListViewItemType.DataItem)
            {
                switch (e.CommandName)
                {
                    case "add":
                        CreateNewIncentive(e);
                        break;
                }
            }
        }

        protected void lvData_ItemDeleting(object sender, ListViewDeleteEventArgs e)
        {
            ListViewDataItem el = lvData.Items[e.ItemIndex];
            DeactivateIncentive(el);
        }

        protected void lvData_ItemUpdating(object sender, ListViewUpdateEventArgs e)
        {
            ListViewDataItem el = lvData.Items[e.ItemIndex];
            UpdateIncentive(el);
        }

        protected void lvData_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            lvData.EditIndex = e.NewEditIndex;
            lvData.InsertItemPosition = InsertItemPosition.None;
            LoadPeriods();
        }

        protected void lvData_PagePropertiesChanged(object sender, EventArgs e)
        {
            LoadPeriods();
        }

        protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            lvData.InsertItemPosition = InsertItemPosition.None;
            LoadPeriods();
        }

        protected void lnkCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("index.aspx", true);
        }

    }
}