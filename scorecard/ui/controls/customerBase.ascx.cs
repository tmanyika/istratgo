﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.implementations;
using scorecard.controllers;
using RKLib.ExportData;
using System.Data;

namespace scorecard.ui.reports
{
    public partial class customerReport : System.Web.UI.UserControl
    {
        #region Page Events Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Cache["dT"] = new reportcontroller(new reportsImpl()).getCustomerReport();
                bindCustomers();
            }
        }

        #endregion

        #region Databinding Methods

        void bindCustomers()
        {
            DataTable dT = Cache["dT"] as DataTable;
            BindControl.BindListView(lvCustomer, dT);
            dtPagerUserAccounts.Visible = (dT.Rows.Count <= dtPagerUserAccounts.PageSize) ? false : true;
        }

        #endregion

        #region Button Event Handling Methods

        protected void lnkExcel_Click(object sender, EventArgs e)
        {
            Export exp = new Export("Web");
            DataTable dT = Cache["dT"] as DataTable;
            DateTime today = DateTime.Now;
            string fileName = string.Format("{0}-{1}-{2}_CustomerBase.xls", today.Year, today.Month, today.Day);
            exp.ExportDetails(dT, Export.ExportFormat.Excel, fileName);
        }

        #endregion

        #region Data Pager

        protected void DataPagerCustomers_PreRender(object sender, EventArgs e)
        {
            bindCustomers();
        }

        #endregion
    }
}