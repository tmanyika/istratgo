﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="approvalwithhirachynosaveall.ascx.cs"
    Inherits="scorecard.ui.controls.approvalwithhirachynosaveall" %>
<%@ Import Namespace="scorecard.implementations" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:UpdatePanel ID="UpdatePanelApproval" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <h1>
            &nbsp; Workflow Approval
        </h1>
        <asp:ListView ID="lvSubmittedWorkflows" runat="server" OnItemCommand="lvSubmittedWorkflows_ItemCommand">
            <LayoutTemplate>
                <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                    <thead>
                        <tr>
                            <td>
                                <input type="checkbox" class="checkall" />
                            </td>
                            <td>
                                Name
                            </td>
                            <td>
                                Criteria
                            </td>
                            <td>
                                Approver
                            </td>
                            <td>
                                Status
                            </td>
                            <td>
                                Date Submitted
                            </td>
                            <td>
                                Details
                            </td>
                        </tr>
                        <tr id="ItemPlaceHolder" runat="server">
                        </tr>
                    </thead>
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tbody>
                    <tr class="odd">
                        <td>
                            <input type="checkbox" />
                            <asp:Label ID="lblID" runat="server" Visible="false" Text='<%# Eval("ID") %>' />
                            <asp:Label ID="lblCriteria" runat="server" Visible="false" Text='<%# Eval("CRITERIA_TYPE_ID") %>' />
                            <asp:Label ID="lblItemSelected" runat="server" Visible="false" Text='<%# Eval("SELECTED_ITEM_VALUE") %>' />
                            <asp:HiddenField ID="hdnOverallComment" runat="server" Value='<%# Eval("OVERALL_COMMENT") %>' />
                        </td>
                        <td>
                            <%# getObjectName(int.Parse(Eval("SELECTED_ITEM_VALUE").ToString()), int.Parse(Eval("CRITERIA_TYPE_ID").ToString()))%>
                        </td>
                        <td>
                            <%# Eval("CriteriaDescription") %>
                        </td>
                        <td>
                            <%# Eval("FirstName") %>
                            <%# Eval("MiddleName") %>
                            <%# Eval("LastName") %>
                        </td>
                        <td>
                            <%# Eval("StatusDescription") %>
                        </td>
                        <td>
                            <%# String.Format("{0:dd/MM/yyyy}", Eval("DATE_SUBMITTED"))%>
                        </td>
                        <td>
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkView" CommandName="View" class="ui-state-default ui-corner-all"
                                    title="View Details " CausesValidation="false"><span class="ui-icon ui-icon-pencil">
                        </span></asp:LinkButton>
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </ItemTemplate>
            <EmptyItemTemplate>
                There are no records to display.
            </EmptyItemTemplate>
        </asp:ListView>
        <div>
            <p>
                <span style="color: Red;">
                    <asp:Literal ID="lblResult" runat="server"></asp:Literal></span>
            </p>
        </div>
        <asp:Panel ID="pnlNoData" runat="server" Visible="false">
            <div class="message information close">
                <h2>
                    Information
                </h2>
                <p>
                    <asp:Literal ID="lblMsg" runat="server"></asp:Literal>
                </p>
            </div>
        </asp:Panel>
        <br />
        <asp:DataPager ID="dtPagerSubmittedWorkflows" runat="server" PagedControlID="lvSubmittedWorkflows"
            PageSize="1000">
            <Fields>
                <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True" ShowNextPageButton="False"
                    ShowPreviousPageButton="False" />
                <asp:NumericPagerField />
                <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True" ShowNextPageButton="False"
                    ShowPreviousPageButton="False" />
            </Fields>
        </asp:DataPager>
        <br />
        <br />
        <asp:Button ID="lnkDone" runat="server" class="button tooltip" title="Save Scores"
            Text="Go To Home Screen" OnClick="lnkDone_Click" CausesValidation="false"></asp:Button>
        <br />
        <br />
        <asp:Panel ID="mdlPanelShowScores" runat="server" CssClass="modalPopup" Height="768px"
            Width="1280px" ScrollBars="Both">
            <br />
            <table style="width: 100%">
                <tr>
                    <td style="float: left; width: 50%; text-align: left;">
                        <h1>
                            Scores
                        </h1>
                    </td>
                    <td style="float: left; width: 50%; text-align: right;">
                        <asp:Button ID="btnClose" runat="server" class="button" OnClick="lnkCancel_Click"
                            Text="Close" />
                    </td>
                </tr>
            </table>
            <fieldset runat="server" id="pnlScoresInfo">
                <legend>Approve Scores</legend>
                <p>
                    <label for="sf">
                        Criteria :
                    </label>
                    &nbsp;<span class="field_desc" style="font-style: normal;"><b><asp:Literal ID="lblArea"
                        runat="server"></asp:Literal></b>
                        <asp:HiddenField ID="hdnAreaId" runat="server" />
                        <asp:HiddenField ID="hdnValueId" runat="server" />
                        <asp:HiddenField ID="hdnSubmissionId" runat="server" />
                        <asp:HiddenField ID="hdnSubWeightId" runat="server" />
                    </span>
                    <asp:Label ID="lblID" runat="server" Visible="False"></asp:Label>
                </p>
                <p>
                    <label for="sf">
                        Name :
                    </label>
                    &nbsp;<span class="field_desc" style="font-style: normal;"><b><asp:Literal ID="lblName"
                        runat="server"></asp:Literal></b> </span>
                </p>
            </fieldset>
            <asp:ListView ID="lvGoals" runat="server" OnItemCommand="lvGoals_ItemCommand" OnItemDataBound="lvGoals_ItemDataBound">
                <LayoutTemplate>
                    <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                        <thead>
                            <tr>
                                <td style="width: 160px">
                                    Performance Measure
                                </td>
                                <td style="width: 150px">
                                    Justification Comment
                                </td>
                                <td style="width: 200px">
                                    Manager Comment
                                </td>
                                <td>
                                    Score
                                </td>
                                <td>
                                    Manager Score
                                </td>
                                <td>
                                    Target
                                </td>
                                <td align="right" style="width: 50px">
                                    Weight
                                </td>
                                <td>
                                    Agreed Score
                                </td>
                                <td align="right" style="width: 80px">
                                    Final Score %
                                </td>
                                <td align="right" style="width: 100px">
                                    Defined Target Score
                                </td>
                                <td align="right" style="width: 100px">
                                    Defined Weighted Score
                                </td>
                                <td align="center">
                                    Update
                                </td>
                            </tr>
                            <tr id="ItemPlaceHolder" runat="server">
                            </tr>
                        </thead>
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <tbody>
                        <tr class="odd">
                            <td>
                                <%# Eval("GOAL_DESCRIPTION")%>
                                <asp:Label ID="lblID" runat="server" Visible="false" Text='<%# Eval("ID") %>' />
                                <asp:HiddenField ID="hdnUnitMeasureId" runat="server" Value='<%# Eval("UNIT_MEASURE") %>' />
                                <asp:HiddenField ID="hdnOrgScoreValue" runat="server" Value='<%# Eval("SCORES") %>' />
                                <asp:HiddenField ID="hdnHasSubGoals" runat="server" Value=' <%# Eval("HAS_SUB_GOALS")%>' />
                                <asp:HiddenField ID="hdnParentId" runat="server" Value=' <%# Eval("PARENT_ID")%>' />
                            </td>
                            <td>
                                <%# Eval("JUSTIFICATION_COMMENT") %>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtMngComment" Text='<%# Eval("MANAGER_COMMENT")%>'
                                    Width="170px" Visible='<%# Util.GetParentVisibility(Eval("HAS_SUB_GOALS")) %>' />
                            </td>
                            <td>
                                <asp:Literal ID="lblScores" runat="server" Text='<%# Eval("SCORES") %>' Visible='<%# Util.GetParentVisibility(Eval("HAS_SUB_GOALS")) %>' />
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtManagerScore" Text='<%# Eval("MANAGER_SCORE")%>'
                                    Width="38px" Visible='<%# Util.GetParentVisibility(Eval("HAS_SUB_GOALS")) %>' />
                                <span style="color: Red">
                                    <asp:Literal ID="lblError" runat="server" Text=""></asp:Literal></span>
                            </td>
                            <td>
                                <asp:Label ID="lblTarget" runat="server" Text='<%# Eval("TARGET")%>' Visible='<%# Util.GetParentVisibility(Eval("HAS_SUB_GOALS")) %>' />
                            </td>
                            <td align="right">
                                <b>
                                    <asp:Label ID="lblWeight" runat="server" Text='<%# Eval("WEIGHT") %>' />
                                    %</b>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtAgreedScore" Text='<%# Eval("AGREED_SCORE")%>'
                                    Width="38px" Visible='<%# Util.GetParentVisibility(Eval("HAS_SUB_GOALS")) %>' />
                                <span style="color: Red">
                                    <asp:Literal ID="lblError2" runat="server" Text=""></asp:Literal></span>
                            </td>
                            <td align="right">
                                <b>
                                    <asp:Label ID="lblTotal" runat="server" Text='<%# Eval("FINAL_SCORE") %>' />
                                    %</b>
                            </td>
                            <td align="right">
                                <b>
                                    <asp:Label ID="lblRatingValue" runat="server" Text='<%# Eval("RATINGVALUE") %>' Visible='<%# Util.GetParentVisibility(Eval("HAS_SUB_GOALS")) %>' />
                                </b>
                            </td>
                            <td align="right">
                                <b>
                                    <asp:Label ID="lblFinalAgreedTotal" runat="server" Text='<%# Eval("FINAL_SCORE_AGREED") %>' />
                                </b>
                            </td>
                            <td align="center">
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="lnkUpdate" CommandName="updateItem" ValidationGroup="Edit"
                                        Visible='<%# Util.GetParentVisibility(Eval("HAS_SUB_GOALS")) %>' class="ui-state-default ui-corner-all"
                                        title="Update Record"><span class="ui-icon ui-icon-disk"></span></asp:LinkButton>
                                </ul>
                            </td>
                        </tr>
                        <asp:Repeater ID="rptMeasureData" runat="server" Visible="false" OnItemCommand="rptMeasureData_OnItemCommand">
                            <ItemTemplate>
                                <tr>
                                    <td style="padding-left: 25px;">
                                        <%#  Eval("GOAL_DESCRIPTION")%>
                                        <asp:HiddenField ID="hdnSubId" runat="server" Value='<%# Eval("ID") %>' />
                                        <asp:HiddenField ID="hdnTarget" runat="server" Value='<%# Eval("TARGET") %>' />
                                        <asp:HiddenField ID="hdnParentId" runat="server" Value='<%# Eval("PARENT_ID") %>' />
                                        <asp:HiddenField ID="hdnUnitMeasure" runat="server" Value='<%# Eval("UNIT_MEASURE") %>' />
                                    </td>
                                    <td>
                                        <%# Eval("JUSTIFICATION_COMMENT") %>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtMngComment" Text='<%# Eval("MANAGER_COMMENT")%>'
                                            Width="170px" />
                                    </td>
                                    <td>
                                        <%# Eval("SCORES") %>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtManagerScore" Text='<%# Eval("MANAGER_SCORE")%>'
                                            Width="38px" />
                                        <span style="color: Red">
                                            <asp:Literal ID="lblError" runat="server" Text=""></asp:Literal>
                                        </span>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblTarget" runat="server" Text='<%# Eval("TARGET")%>' Visible='<%# Util.GetParentVisibility(Eval("HAS_SUB_GOALS")) %>' />
                                    </td>
                                    <td align="right">
                                        &nbsp;
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtAgreedScore" Text='<%# Eval("AGREED_SCORE")%>'
                                            Width="38px" Visible='<%# Util.GetParentVisibility(Eval("HAS_SUB_GOALS")) %>' />
                                        <span style="color: Red">
                                            <asp:Literal ID="lblError2" runat="server" Text=""></asp:Literal></span>
                                    </td>
                                    <td align="right">
                                        <b>
                                            <asp:Label ID="lblTotal" runat="server" Text='<%# Eval("FINAL_SCORE") %>' />
                                            %</b>
                                    </td>
                                    <td align="right">
                                        <b>
                                            <asp:Label ID="lblRatingValue" runat="server" Text='<%# Eval("RATINGVALUE") %>' />
                                        </b>
                                    </td>
                                    <td align="right">
                                        <b>
                                            <asp:Label ID="lblFinalAgreedTotal" runat="server" Text='<%# Eval("FINAL_SCORE_AGREED") %>' />
                                        </b>
                                    </td>
                                    <td align="center">
                                        <ul id="icons">
                                            <asp:LinkButton runat="server" ID="lnkUpdateItem" CommandName="update" ValidationGroup="EditItem"
                                                class="ui-state-default ui-corner-all" title="Update Record"><span class="ui-icon ui-icon-disk"></span></asp:LinkButton>
                                        </ul>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </ItemTemplate>
            </asp:ListView>
            <p>
                <span style="color: Red; font-size: small;">
                    <asp:Literal ID="lblLstView" runat="server"></asp:Literal><br />
                    <br />
            </p>
            <p>
                <u><b style="color: Black">Notes</b></u><br />
                <br />
            </p>
            <p>
                <span style="color: Red">1. * = required field<br />
                </span>
                <br />
                <span style="color: Red">2. For numeric values remove any commas. Use a <strong>full
                    stop</strong> for decimal numbers. Only a maximum of 2 decimal places is allowed.</span><br />
            </p>
            </span>
            <fieldset runat="server" id="Fieldset1">
                <legend>Supporting Documents </legend>
                <asp:DataList ID="dtlstAttachments" runat="server" Width="100%">
                    <ItemTemplate>
                        <a href='<%# getfilePath(Eval("ATTACHMENT_NAME").ToString())%>' id="lnk" runat="server"
                            target="_blank">
                            <asp:Image ID="IcnAttachment" runat="server" ImageUrl="~/ui/assets/1334961966_attachment.png" />
                            <%# Eval("ATTACHMENT_NAME")%></a>
                    </ItemTemplate>
                </asp:DataList>
                <br />
            </fieldset>
            <fieldset runat="server" id="Fieldset3">
                <legend>Overall Manager's Comment(s)</legend>
                <div class="field_desc">
                    <div style="float: left; width: 85%">
                        <asp:TextBox runat="server" ID="txtComment" TextMode="MultiLine" Width="95%" Height="20px" />
                    </div>
                    <div style="float: left">
                        <asp:Button ID="btnUpdateComment" runat="server" class="button" OnClick="btnUpdateComment_Click"
                            Text="Save" /></div>
                    <div style="clear: both" />
                </div>
                <p>
                    <br />
                    <span style="color: Red">
                        <asp:Literal ID="lblPopupMsg" runat="server"></asp:Literal></span>
                </p>
            </fieldset>
            <fieldset runat="server" id="Fieldset2">
                <legend>Approve / Decline Request </legend>
                <p>
                    <label for="sf">
                        Status:
                    </label>
                    &nbsp;<span class="field_desc"><asp:DropDownList ID="ddlStatus" runat="server" Width="130px" />
                    </span>
                    <asp:Button ID="btnNew" runat="server" class="button" OnClick="btnNew_Click" Text="Submit" />
                    <asp:Button ID="lnkCancel" runat="server" class="button" OnClick="lnkCancel_Click"
                        Text="Cancel" />
                </p>
            </fieldset>
        </asp:Panel>
        <asp:Button ID="btnScoreDetails" runat="server" Style="display: none" />
        <ajaxToolkit:ModalPopupExtender ID="mdlShowScoreDetails" runat="server" TargetControlID="btnScoreDetails"
            PopupControlID="mdlPanelShowScores" BackgroundCssClass="modalBackground" />
        <asp:UpdateProgress ID="UpdateProgressApproval" runat="server" AssociatedUpdatePanelID="UpdatePanelApproval">
            <ProgressTemplate>
                <img runat="server" src="~/ui/images/icons/activity.gif" alt="Please wait..." id="imgPrg" />
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
