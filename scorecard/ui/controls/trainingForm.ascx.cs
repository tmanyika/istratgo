﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.interfaces;
using scorecard.implementations;
using HR.Human.Resources;
using scorecard.entities;
using System.Data;

namespace scorecard.ui.controls
{
    public partial class trainingForm : System.Web.UI.UserControl
    {
        #region  Variables

        Iorgunit org;
        IEmployee emp;

        #endregion

        #region Default Class Constructors

        public trainingForm()
        {
            org = new structureImpl();
            emp = new EmployeeBL();
        }

        #endregion

        #region Page Events Handling Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindOrgUnits();
                BindEmployees();
            }
        }

        #endregion

        #region Databinding Methods

        void BindNormalUserOrgUnit()
        {
            int orgUnitId = Util.user.OrgId;
            var data = new List<Orgunit>();
            data.Add(org.getOrgunitDetails(new Orgunit { ID = orgUnitId }));
            BindControl.BindDropdown(ddOrgUnit, "ORGUNIT_NAME", "ID", "- select all -", "-1", orgUnitId.ToString(), data);
        }

        void BindOrgUnits()
        {
            int orgUnitId = Util.user.OrgId;
            int companyId = Util.user.CompanyId;
            int userId = Util.user.UserId;
            int? roleId = Util.user.UserTypeId;

            bool isAdmin = (roleId == Util.getTypeDefinitionsUserTypeAdministrator());
            bool isManager = (roleId == Util.getTypeDefinitionsUserTypeManager());

            rqdDropD.Enabled = isAdmin ? false : true;
            rqdDropEmp.Enabled = (isAdmin || isManager) ? false : true;

            var data = new DataView();
            if (isManager || isAdmin)
            {
                data = isManager ? org.getOrgUnitsByLineManager(userId, true).DefaultView :
                                   org.getActiveCompanyStructure(new company { COMPANY_ID = companyId });
            }
            else
            {
                BindNormalUserOrgUnit();
                return;
            }

            string selectedVal = isAdmin ? string.Empty : orgUnitId.ToString();
            string selectedText = isAdmin ? "- select all -" : "- select org unit -";

            BindControl.BindDropdown(ddOrgUnit, "ORGUNIT_NAME", "ID", "- select all -", "-1", selectedVal, data);
        }

        void BindEmployees()
        {
            List<Employee> data = new List<Employee>();

            int? roleId = Util.user.UserTypeId;
            int managerId = Util.user.UserId;

            bool isAdmin = (roleId == Util.getTypeDefinitionsUserTypeAdministrator());
            bool isManager = (roleId == Util.getTypeDefinitionsUserTypeManager());

            string selectedText = "- select all -";
            string fullName = Util.user.FullName;

            if (ddOrgUnit.SelectedIndex == 0)
            {
                if (!(isAdmin || isManager))
                {
                    ddOrgUnit.Items.Add(new ListItem
                    {
                        Selected = true,
                        Text = Util.user.FullName,
                        Value = Util.user.UserId.ToString()
                    });
                }
                else
                {
                    BindControl.BindDropdown(ddEmployee, "FullName", "EmployeeId", selectedText, "-1", string.Empty, data);
                }
                return;
            }

            int orgUnitId = int.Parse(ddOrgUnit.SelectedValue);
            data.Add(new Employee { EmployeeId = managerId, FullName = fullName });

            if (isAdmin || isManager)
            {
                data = isAdmin ? emp.GetActive(orgUnitId) : emp.GetByManager(managerId, true);
            }

            BindControl.BindDropdown(ddEmployee, "FullName", "EmployeeId", selectedText, "-1", string.Empty, data);
        }

        void ShowMessage(string msg)
        {
            BindControl.BindLiteral(lblMsg, msg);
        }

        bool IsValid()
        {
            bool isValid = false;
            try
            {
                if (string.IsNullOrEmpty(txtEndDate.Text) || string.IsNullOrEmpty(txtStartDate.Text))
                {
                    isValid = false;
                    ShowMessage(Messages.GetRequiredFieldsMessage(true, string.IsNullOrEmpty(txtStartDate.Text) ? "Start Date" : string.Empty,
                       string.IsNullOrEmpty(txtEndDate.Text) ? "End Date" : string.Empty));
                }
                else
                {
                    if (!string.IsNullOrEmpty(txtEndDate.Text) && !string.IsNullOrEmpty(txtStartDate.Text))
                    {
                        string tempStartDate = Common.GetInternationalDateFormat(txtStartDate.Text, DateFormat.monthDayYear);
                        string tempEndDate = Common.GetInternationalDateFormat(txtEndDate.Text, DateFormat.monthDayYear);
                        if (DateTime.Parse(tempStartDate) > DateTime.Parse(tempEndDate))
                        {
                            ShowMessage("Start Date cannot be greater than End Date.");
                            isValid = false;
                        }
                        else
                        {
                            isValid = true;
                        }
                    }
                }

                bool isAdmin = Util.user.UserTypeId == Util.getTypeDefinitionsUserTypeAdministrator();
                bool isManager = Util.user.UserTypeId == Util.getTypeDefinitionsUserTypeManager();

                if (!isAdmin && ddOrgUnit.SelectedIndex == 0)
                {
                    isValid = false;
                    ShowMessage("Organisation Unit is required");
                }
            }
            catch (Exception ex)
            {
                ShowMessage(Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }

            return isValid;
        }

        void DownloadReport()
        {
            bool isAdmin = Util.user.UserTypeId == Util.getTypeDefinitionsUserTypeAdministrator();
            bool isComp = (isAdmin && ddOrgUnit.SelectedIndex == 0) ? true : false;

            string startDate = Common.GetInternationalDateFormat(txtStartDate.Text.Trim(), DateFormat.monthDayYear);
            string endDate = Common.GetInternationalDateFormat(txtEndDate.Text.Trim(), DateFormat.monthDayYear);

            string orgUnitId = (ddOrgUnit.SelectedIndex == 0 && isAdmin) ? Util.user.CompanyId.ToString() : ddOrgUnit.SelectedValue;
            string orgUnitName = (ddOrgUnit.SelectedIndex == 0) ? "All" : HttpUtility.UrlEncode(ddOrgUnit.SelectedItem.Text);

            string employeeId = ddEmployee.SelectedValue;
            string employeeName = (ddEmployee.SelectedIndex == 0) ? "All" : HttpUtility.UrlEncode(ddEmployee.SelectedItem.Text);

            string script = string.Format("openNewWindow('trainingpreview.aspx?sid={0}&isComp={6}&dsVal={1}&deVal={2}&orgunitId={3}&name={4}&orgunit={5}');", employeeId, startDate, endDate, orgUnitId, employeeName, orgUnitName, isComp);
            AjaxScriptManager.RegisterStartupScript(Page, typeof(Page), "viewEmpReport", script, true);
        }

        #endregion

        #region Button Event Handling Methods

        protected void btnViewReport_Click(object sender, EventArgs e)
        {
            bool valid = IsValid();
            if (valid)
            {
                DownloadReport();
            }
        }

        #endregion

        #region Dropdown Event Handling Methods

        protected void ddOrgUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindEmployees();
        }

        #endregion
    }
}