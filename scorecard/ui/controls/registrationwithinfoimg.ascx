﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="registrationwithinfoimg.ascx.cs"
    Inherits="scorecard.ui.controls.registrationwithinfoimg" %>
<div class="imageregdiv">
    <div>
        <h2>
            &nbsp;</h2>
        <h2>
            &nbsp;</h2>
    </div>
    <div class="leftreginfo">
        <img src="~/ui/assets/registration/perfomanceappraisal.jpg" alt="Performance Appraisal"
            width="350" runat="server" id="imgperform" />
    </div>
    <div class="leftreginfo">
        <ul>
            <li class="informationText">Easy to use performance appraisal system which allows your staff to rate themselves, get rated by the manager and then agree on the final rating</li>
        </ul>
    </div>
    <div class="leftreginfo">
        <img src="~/ui/assets/registration/360feedback.jpg" alt="360 Degree Feedback" width="350"
            runat="server" id="imgreg" />
    </div>
    <div class="leftreginfo">
        <ul>
            <li class="informationText">Send out internal and external 360<sup>0</sup> surveys at
                a click of a button </li>
        </ul>
    </div>
    <div class="leftreginfo">
        <img src="~/ui/assets/registration/incentiveandtraining.jpg" alt="Incentives and Training"
            width="350" runat="server" id="imgincentive" />
    </div>
    <div class="leftreginfo">
        <ul>
            <li class="informationText">Link bonuses, annual salary increases and training courses
                to performance </li>
        </ul>
    </div>
</div>
