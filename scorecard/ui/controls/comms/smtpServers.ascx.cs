﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.implementations;
using System.Email.Communication;

namespace scorecard.ui.controls.comms
{
    public partial class smtpServers : System.Web.UI.UserControl
    {
        #region Properties

        int CompId
        {
            get { return Util.user.CompanyId; }
        }

        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) InitialisePage();
        }

        #endregion

        #region Utility Methods

        void InitialisePage()
        {
            LoadMailServers();
        }

        void LoadMailServers()
        {
            IMailServer obj = new MailServerBL();
            BindControl.BindListView(lstData, obj.GetByStatus(true));
            pager.Visible = (lstData.Items.Count <= pager.PageSize) ? false : true;
        }

        #endregion

        #region Button Event Handling

        protected void lnkAdd_Click(object sender, EventArgs e)
        {
            BindControl.BindLiteral(lblMsg, "");
            lstData.InsertItemPosition = InsertItemPosition.LastItem;
            LoadMailServers();
        }

        protected void lnkCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("index.aspx", false);
        }

        #endregion

        #region Utility

        public string GetBoolean(object boolVal)
        {
            if (boolVal == DBNull.Value) return "Yes";
            return bool.Parse(boolVal.ToString()) ? "Yes" : "No";
        }

        private void CancelUpdate()
        {
            lstData.EditIndex = -1;
            lstData.InsertItemPosition = InsertItemPosition.None;
            LoadMailServers();
        }

        void DeleteItem(ListViewCommandEventArgs e)
        {
            int accountId = int.Parse((e.Item.FindControl("hdnAccountId") as HiddenField).Value);
            string updatedBy = Util.user.LoginId;

            IMailServer obj = new MailServerBL();
            bool deleted = obj.Delete(accountId, updatedBy);
            if (deleted) LoadMailServers();
            BindControl.BindLiteral(lblMsg, deleted ? Messages.GetDeleteMessage() : Messages.GetDeleteFailedMessage());
        }

        void UpdateItem(ListViewCommandEventArgs e)
        {
            try
            {
                int accountId = int.Parse((e.Item.FindControl("hdnAccountIdE") as HiddenField).Value);
                int port = int.Parse((e.Item.FindControl("txtPort") as TextBox).Text.Trim());

                bool active = (e.Item.FindControl("chkActive") as CheckBox).Checked;
                string sqlProfile = (e.Item.FindControl("txtSqlProfile") as TextBox).Text;
                string smtpServer = (e.Item.FindControl("txtSmtpServer") as TextBox).Text;
                string name = (e.Item.FindControl("txtAccountName") as TextBox).Text;
                string userId = (e.Item.FindControl("txtUserName") as TextBox).Text;
                string pin = (e.Item.FindControl("txtPin") as TextBox).Text;
                string updatedBy = Util.user.LoginId;

                MailServer obj = new MailServer
                {
                    Port = port,
                    AccountName = name,
                    SqlProfileName = sqlProfile,
                    UserName = userId,
                    PIN = pin.Trim(),
                    SmtpServer = smtpServer,
                    AccountMailId = accountId,
                    CreatedBy = updatedBy,
                    UpdatedBy = updatedBy,
                    Active = active
                };

                IMailServer objl = new MailServerBL();
                bool saved = objl.AddMailServer(obj);
                BindControl.BindLiteral(lblMsg, saved ? Messages.GetUpdateMessage() : Messages.GetUpdateFailedMessage());
                if (saved)
                {
                    lstData.EditIndex = -1;
                    LoadMailServers();
                }
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        void AddItem(ListViewCommandEventArgs e)
        {
            try
            {
                bool active = (e.Item.FindControl("chkActivei") as CheckBox).Checked;
                int port = int.Parse((e.Item.FindControl("txtPorti") as TextBox).Text);

                string sqlProfile = (e.Item.FindControl("txtSqlProfilei") as TextBox).Text;
                string smtpServer = (e.Item.FindControl("txtSmtpServeri") as TextBox).Text;
                string name = (e.Item.FindControl("txtAccountNamei") as TextBox).Text;
                string userId = (e.Item.FindControl("txtUserNamei") as TextBox).Text;
                string pin = (e.Item.FindControl("txtPini") as TextBox).Text;
                string updatedBy = Util.user.LoginId;

                MailServer obj = new MailServer
                {
                    DateCreated = DateTime.Now,
                    DateUpdated = DateTime.Now,
                    Port = port,
                    AccountName = name,
                    SqlProfileName = sqlProfile,
                    UserName = userId,
                    PIN = pin.Trim(),
                    SmtpServer = smtpServer,
                    AccountMailId = 0,
                    CreatedBy = updatedBy,
                    UpdatedBy = updatedBy,
                    Active = active
                };

                IMailServer objl = new MailServerBL();
                bool saved = objl.AddMailServer(obj);
                BindControl.BindLiteral(lblMsg, saved ? Messages.GetUpdateMessage() : Messages.GetUpdateFailedMessage());
                if (saved)
                {
                    lstData.InsertItemPosition = InsertItemPosition.None;
                    LoadMailServers();
                }
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        #endregion

        #region ListView Event Handling Methods including Slide Paging

        protected void lstData_PagePropertiesChanged(object sender, EventArgs e)
        {
            LoadMailServers();
        }

        protected void lstData_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            try
            {
                lstData.EditIndex = e.NewEditIndex;
                lstData.InsertItemPosition = InsertItemPosition.None;
                LoadMailServers();
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        protected void lstData_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                BindControl.BindLiteral(lblMsg, "");
                if (e.Item.ItemType == ListViewItemType.InsertItem ||
               e.Item.ItemType == ListViewItemType.DataItem)
                {
                    switch (e.CommandName)
                    {
                        case "AddItem":
                            AddItem(e);
                            break;
                        case "UpdateItem":
                            UpdateItem(e);
                            break;
                        case "DeleteItem":
                            DeleteItem(e);
                            break;
                        case "CancelUpdate":
                            CancelUpdate();
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        #endregion
    }
}