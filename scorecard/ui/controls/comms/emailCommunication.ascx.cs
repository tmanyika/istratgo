﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.entities;
using scorecard.controllers;
using scorecard.implementations;
using System.Email.Communication;
using System.Text;

namespace scorecard.ui.controls
{
    public partial class emailCommunication : System.Web.UI.UserControl
    {
        #region Properties

        public settingsmanager Utility
        {
            get { return new settingsmanager(new applicationsettingsImpl()); }
        }

        #endregion

        #region Page Load Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                FillBatches();
        }

        #endregion

        #region Drop Downlist

        void BindRoles()
        {
            var roles = Utility.getChildSettings(new applicationsettings { PARENT_ID = Util.getTypeDefinitionsSystemUserTypesParentId() }).ToList();
            BindControl.BindChecklist(chkRoles, "NAME", "TYPE_ID", roles);
        }

        void BindMailAccount()
        {
            IMailServer obj = new MailServerBL();
            List<MailServer> data = obj.GetByStatus(true);
            BindControl.BindDropdown(ddAccount, "AccountName", "AccountMailId", data);
        }

        void BindMailTemplates()
        {
            IMailTemplate obj = new MailTemplateBL();
            List<MailTemplate> data = obj.GetMasterTemplates(true, true);
            BindControl.BindDropdown(ddMail, "MailName", "MailId", data);
        }

        #endregion

        #region Utilities

        void ClearTextBoxes()
        {
            rdContent.Content = "";
            txtBatchName.Text = "";
            txtDate.Text = "";
            txtSubject.Text = "";
        }

        void BindDropDowns()
        {
            BindRoles();
            BindMailAccount();
            BindMailTemplates();
        }

        void FillBatches()
        {
            IMailBatch obj = new MailBatchBL();
            List<MailBatch> data = obj.GetAll();
            BindControl.BindListView(lstData, data);
            DtPager.Visible = (data.Count <= DtPager.PageSize) ? false : true;
            vMain.SetActiveView(vwData);
        }

        public string GetBoolean(object boolVal)
        {
            if (boolVal == DBNull.Value) return "Yes";
            return bool.Parse(boolVal.ToString()) ? "Yes" : "No";
        }

        string GetValueList()
        {
            StringBuilder strB = new StringBuilder("");
            bool selected = false;
            foreach (ListItem item in chkRoles.Items)
                if (item.Selected)
                {
                    selected = true;
                    strB.Append(string.Format("{0},", item.Value));
                }
            if (!selected) return "";
            string strVals = strB.ToString();
            int lstIdx = strVals.LastIndexOf(',');
            return lstIdx > 0 ? strVals.Substring(0, lstIdx) : strVals;
        }

        void Save()
        {
            try
            {
                string mailContent = rdContent.Content;
                string recipientRoles = GetValueList();
                if (recipientRoles.Length <= 0)
                {
                    ShowMessage(lblError, "Please select recipient role(s).");
                    return;
                }

                if (mailContent == string.Empty)
                {
                    ShowMessage(lblError, "Please supply mail content.");
                    return;
                }

                DateTime tmpDate = DateTime.Now;
                DateTime todayDate = new DateTime(tmpDate.Year, tmpDate.Month, tmpDate.Day);
                DateTime dateToSend = DateTime.Parse(Common.GetInternationalDateFormat(txtDate.Text.Trim(), DateFormat.dayMonthYear));
                if (dateToSend.Subtract(todayDate).TotalDays < 0)
                {
                    ShowMessage(lblError, "The date you supplied is invalid as it is in the past.");
                    return;
                }

                bool send = false;// dateToSend.Subtract(todayDate).TotalDays == 0 ? true : false;

                int templateId = int.Parse(ddMail.SelectedValue);
                int mailAccountId = int.Parse(ddAccount.SelectedValue);

                string createdBy = Util.user.LoginId;
                string subject = txtSubject.Text.Trim();
                string batchName = txtBatchName.Text.Trim();

                string roleIds = Common.GetXML(recipientRoles.Split(','), "Role", "Data", "Id");

                MailBatch obj = new MailBatch
                {
                    AccountMailId = mailAccountId,
                    BatchId = 0,
                    CreatedBy = createdBy,
                    DateCreated = DateTime.Now,
                    DateToBeSent = dateToSend,
                    IsSent = false,
                    MailContent = mailContent,
                    MailId = templateId,
                    MailSubject = subject,
                    MainTemplate = string.Empty,
                    BatchName = batchName,
                    NoOfUsers = 0,
                    SendNow = send,
                    UserRoles = roleIds
                };

                IMailRelay mail = new MailRelayBL();
                IMailBatch objm = new MailBatchBL();

                int batchId = objm.AddMailBatch(obj);
                bool added = (batchId > 0) ? true : false;
                ShowMessage(lblError, (added) ? Messages.GetMailedQueueMessage() : Messages.GetSaveFailedMessage());
                if (added)
                {
                    if (send)
                    {
                        string result;
                        bool sent = mail.SendMail(batchId, true, out result);
                        ShowMessage(lblError, string.Format("{0} {1}", Messages.GetSaveMessage(), result));
                    }
                    vMain.SetActiveView(vwData);
                    FillBatches();
                }

            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                ShowMessage(lblError, e.ToString());
            }
        }

        void ShowMessage(Literal lbl, string msg)
        {
            lbl.Text = msg;
        }

        #endregion

        #region Button Event Handling

        protected void lnkSaveMail_Click(object sender, EventArgs e)
        {
            Save();
        }

        protected void lnkAddNew_Click(object sender, EventArgs e)
        {
            vMain.SetActiveView(vForm);
            BindDropDowns();
            ClearTextBoxes();
            ShowMessage(lblError, "");
        }

        protected void lnkCancel_Click(object sender, EventArgs e)
        {
            FillBatches();
        }

        #endregion

        #region ListView Event Handling Methods

        protected void lstData_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            DtPager.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            FillBatches();
        }

        #endregion

    }
}