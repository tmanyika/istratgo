﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using scorecard.implementations;
using Bulk.Import;
using System.Data;
using System.Text;

namespace scorecard.ui.controls.bulkupload
{
    public partial class uploadFile : System.Web.UI.UserControl
    {
        #region Member Variables

        IBulkImport Imp;
        IBulkSheet Ish;
        IBulkSheetField Ifld;

        #endregion

        #region Default Constructor

        public uploadFile()
        {
            Imp = new BulkImportBL();
            Ish = new BulkSheetBL();
            Ifld = new BulkSheetFieldBL();
        }

        #endregion

        #region Page Load Event Handling Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            ManageControls();
            if (!IsPostBack)
                LoadSheets();
        }

        void ManageControls()
        {
            rptInvalidObjectives.Visible = false;
        }

        #endregion

        #region Databinding Methods

        void LoadSheets()
        {
            try
            {
                var dataSheets = Ish.GetByStatus(true).OrderBy(o => o.OrderNumber).ToList();
                BindControl.BindRepeater(rptSheets, dataSheets);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void ShowMessage(Literal lbl, string message)
        {
            lbl.Text = message;
        }

        DataSet UploadFile()
        {
            try
            {
                UploadedFile myFile = RdUpload.UploadedFiles[0];
                var enumType = Imp.GetFileTypeEnum(myFile.GetExtension());
                //check validity of the file here
                if (enumType == FileEnum.None)
                {
                    ShowMessage(lblError, "Wrong file format. Only Excel <b>.xlsx</b> are allowed.");
                    return new DataSet();
                }
                DataSet dS = Imp.GetData(myFile.InputStream, enumType);
                return dS;
            }
            catch (Exception e)
            {
                ShowMessage(lblError, Messages.GetErrorMessage());
                Util.LogErrors(e);
            }
            return new DataSet();
        }

        void ImportData()
        {
            try
            {
                DataSet dS = UploadFile();

                StringBuilder strB = new StringBuilder();
                StringBuilder strP = new StringBuilder();

                string outcome = string.Empty;
                var sheetList = Ish.GetByAreaIdAndStatus(BulkSheetAreaConfig.PerformanceManagementAreaId, true).OrderBy(s => s.OrderNumber).ToList();
                bool valid = Ish.ValidateSheets(dS, sheetList, out  outcome);

                if (valid)
                {
                    string username = Util.user.LoginId;
                    string additionalFieldName = "CompanyId";

                    int companyId = Util.user.CompanyId;

                    var fieldList = Ifld.GetAll(true);
                    bool imported = Imp.ImportData(dS, sheetList, fieldList, additionalFieldName, companyId, out outcome);
                    strB.Append(imported ? "- Data was imported successfully." + Environment.NewLine : "- " + outcome + Environment.NewLine);
                    if (imported)
                    {
                        int savedsome = 0;
                        foreach (BulkSheet sht in sheetList)
                        {
                            if (dS.Tables.Contains(sht.SheetName))
                            {
                                bool saved = false;
                                int orderNumber = sht.OrderNumber;
                                switch (orderNumber)
                                {
                                    case 1:
                                        saved = Imp.UpdateJobTitles(companyId, username);
                                        strP.Append("Job Titles" + Environment.NewLine);
                                        if (saved) strB.Append("- Job Titles were saved succesfully" + Environment.NewLine);
                                        else strB.Append("- Job Titles were not saved" + Environment.NewLine);
                                        break;
                                    case 2:
                                        saved = Imp.UpdateOrgUnits(companyId, username);
                                        strP.Append("Organisational Units" + Environment.NewLine);
                                        if (saved)
                                        {
                                            savedsome++;
                                            strB.Append("- Organisational Units were saved succesfully" + Environment.NewLine);
                                        }
                                        else strB.Append("- Organisational Units were not saved" + Environment.NewLine);
                                        break;
                                    case 3:
                                        saved = Imp.UpdateProjects(companyId, username);
                                        strP.Append("Projects" + Environment.NewLine);
                                        if (saved)
                                        {
                                            savedsome++;
                                            strB.Append("- Projects were saved succesfully" + Environment.NewLine);
                                        }
                                        else strB.Append("- Projects were saved succesfully" + Environment.NewLine);
                                        break;
                                    case 4:
                                        saved = Imp.UpdateEmployees(companyId, username);
                                        strP.Append("Users");
                                        if (saved) strB.Append("- Employees information was saved succesfully" + Environment.NewLine);
                                        else strB.Append("- Employees information was not saved" + Environment.NewLine);
                                        break;
                                    case 5:
                                        DataTable dT = Imp.ValidationResultsOfObjectives(companyId);
                                        if (dT.Rows.Count <= 0)
                                        {
                                            saved = Imp.UpdatePerformanceMeasures(companyId, username, out outcome);
                                            if (saved) strB.Append(Environment.NewLine + "Performance Measures Results:" + Environment.NewLine + outcome);
                                        }
                                        else
                                        {
                                            strB.Append("- Performance Measures data sheet has some invalid information." + Environment.NewLine + Environment.NewLine + "See incorrectly linked Strategic Objectives below:" + Environment.NewLine);
                                            rptInvalidObjectives.Visible = true;
                                            BindControl.BindRepeater(rptInvalidObjectives, dT);
                                        }
                                        break;
                                }
                            }
                        }
                        if (savedsome > 0)
                        {
                            bool linked = Imp.LinkInformation(companyId, username);
                            strB.Append(linked ? string.Format("- {0} have been linked." + Environment.NewLine, strP.ToString().Replace(Environment.NewLine, ",")) :
                               string.Format("- {0} could not be linked." + Environment.NewLine, strP.ToString().Replace(Environment.NewLine, ",")));
                        }
                    }
                    ShowMessage(lblError, string.Format("The results of your Data Import are as follows:<br/>{0}", strB.ToString().Replace(Environment.NewLine, "<br/>")));
                    Imp.DeleteImportedData(companyId);
                }
                else
                {
                    string validationFail = string.Format("Validation results are as follows:<br/>{0}", outcome.Replace(Environment.NewLine, "<br/>"));
                    ShowMessage(lblError, validationFail);
                }
            }
            catch (Exception e)
            {
                ShowMessage(lblError, Messages.GetErrorMessage());
                Util.LogErrors(e);
            }

        }
        #endregion

        #region Button Event Handling Methods

        protected void btnNew_Click(object sender, EventArgs e)
        {
            ImportData();
        }
        #endregion
    }
}