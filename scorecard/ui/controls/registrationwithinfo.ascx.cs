﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.controllers;
using scorecard.implementations;
using scorecard.entities;
using System.Web.Security;
using HR.Human.Resources;
using System.Email.Communication;
using System.Net.Mail;
using System.IO;
using Scorecard.Entities;

namespace scorecard.ui.controls
{

    public partial class registrationwithinfo : System.Web.UI.UserControl
    {
        #region variables or members

        useradmincontroller _user;
        menucontroller accessManager;
        scorecard.controllers.registration _registration;

        #endregion

        #region class constructor

        public registrationwithinfo()
        {
            _registration = new scorecard.controllers.registration(new customerImpl());
            _user = new useradmincontroller(new useradminImpl());
            accessManager = new menucontroller(new rolebasedImpl());
        }

        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                clearForm();
                loadCountries();
            }
        }


        #endregion

        #region utility methods

        void clearForm()
        {
            txtEmailAddress.Value = "";
            txtFirstName.Value = "";
            txtLastName.Value = "";
            mainView.SetActiveView(vwForm);
        }

        void ShowMessage(string msg)
        {
            lblError.Text = msg;
        }

        void loadCountries()
        {
            try
            {
                var setuptypes = new settingsmanager(new applicationsettingsImpl()).getChildSettings(new entities.applicationsettings { PARENT_ID = Util.getTypeDefinitionsCountryTypeParentID() });
                BindControl.BindDropdown(ddCountry, setuptypes);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                BindControl.BindLiteral(lblError, string.Format("<span class='error'>{0}</span>", Messages.GetErrorMessage()));
            }
        }

        private void submitInfo()
        {
            try
            {
                string cellNo = string.Empty;
                string email = txtEmailAddress.Value.Trim(); ;
                string firstname = txtFirstName.Value.Trim();
                string jobTitle = "Administrator";
                string lastname = txtLastName.Value.Trim();
                string companyname = txtCompany.Value.Trim();

                string pin = Util.getRandomString(10);
                string pass = Util.HashPassword(pin);
                string activationCode = Util.getRandomString(30);

                string username = email.ToLower();

                int countryId = int.Parse(ddCountry.SelectedValue);
                int setupTypeId = Util.getOrganisationalTypeGroupId();
                int adminTypeId = Util.getTypeDefinitionsUserTypeAdministrator();

                DateTime? dob = null;

                var comp = new company
                {
                    COMPANY_NAME = companyname

                };
                var activation = new CompanyActivation
                    {
                        ActivationCode = activationCode,
                        UserName = username
                    };

                customer cust = new customer
                {
                    USER_NAME = username,
                    SETUP_TYPE = setupTypeId,
                    USER_TYPE = adminTypeId,
                    PASSWORD = pass,
                    EMAIL_ADDRESS = email,
                    FIRST_NAME = firstname,
                    LAST_NAME = lastname,
                    CELL_NUMBER = cellNo,
                    DATE_OF_BIRTH = dob,
                    JOB_TITLE_NAME = jobTitle,
                    CountryId = countryId
                };

                string errMsg;
                int compNo = _registration.registerCustomer(cust, comp, activation, out errMsg);
                if (compNo > 0)
                {
                    IEmployee obj = new EmployeeBL();
                    Employee user = obj.GetByUserName(username);
                    if (!string.IsNullOrEmpty(user.UserName))
                    {
                        lblEmail.Text = email;
                        SendWelcomeMail(user, pin, activationCode);
                        CopyJobDescriptions(user.CompanyId);
                        mainView.SetActiveView(vwMessage);
                    }
                    else
                    {
                        BindControl.BindLiteral(lblError, Messages.GetSaveFailedMessage());
                    }
                }
                else
                {
                    BindControl.BindLiteral(lblError, errMsg);
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                BindControl.BindLiteral(lblError, Messages.GetErrorMessage());
            }
        }

        void CopyJobDescriptions(int companyId)
        {
            try
            {
                string defaultCompanyFolder = Util.GetJobContractPhysicalPath(Util.DefaultCompanyId);
                if (Directory.Exists(defaultCompanyFolder))
                {
                    string newCompanyFolder = Util.GetJobContractPhysicalPath(companyId);
                    if (!Directory.Exists(newCompanyFolder))
                    {
                        Directory.CreateDirectory(newCompanyFolder);
                    }

                    if (Directory.Exists(newCompanyFolder))
                    {
                        DirectoryInfo source = new DirectoryInfo(defaultCompanyFolder);
                        foreach (FileInfo file in source.GetFiles())
                        {
                            string destFileName = string.Format(@"{0}\{1}", newCompanyFolder, file.Name);
                            File.Copy(file.FullName, destFileName, true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void SendWelcomeMail(Employee user, string password, string activationCode)
        {
            try
            {
                string result = string.Empty;
                string companyId = Util.Encrypt(user.CompanyId.ToString());
                string activationUrl = string.Format("{0}{1}?compId={2}&code={3}", Util.BaseUrl, Util.ActivationPage, companyId, activationCode);
                int emailId = Util.getRegWelcomeEmailId();

                MailAddressCollection addy = new MailAddressCollection();
                addy.Add(new MailAddress(user.EmailAddress, user.FullName));

                object[] mydata = { user.UserName, password, activationUrl };

                IMailRelay objm = new MailRelayBL();
                MailRelay obj = new MailRelay
                {
                    MailId = emailId,
                    Recipient = addy,
                    Data = mydata
                };
                objm.SendMail(obj, out result);
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                BindControl.BindLiteral(lblError, Messages.GetErrorMessage());
            }
        }

        #endregion

        #region button event handling methods

        protected void btnComplete_Click(object sender, EventArgs e)
        {
            if (chkTerms.Checked)
            {
                if (Page.IsValid)
                {
                    submitInfo();
                }
                else
                {
                    ShowMessage("Please supply a valid code.");
                }
            }
            else
            {
                ShowMessage("You have to agree to the Terms & Conditions to continue.");
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            Response.Redirect("~/ui/login.aspx");
        }

        #endregion

    }
}