﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.implementations;
using HR.EmployeeTraining.Interfaces;
using HR.EmployeeTraining.Logic;
using HR.EmployeeTraining.Entities;

namespace scorecard.ui.controls.training
{
    public partial class trainingstatusmgt : System.Web.UI.UserControl
    {
        ITrainingStatus stat;

        public trainingstatusmgt()
        {
            stat = new TrainingStatusBL();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ResetLabels();
            if (!IsPostBack)
            {
                LoadTrainingStatus();
            }
        }

        private void ResetLabels()
        {
            lblMsg.Text = "";
            LblError.Text = "";
        }

        void LoadTrainingStatus()
        {
            try
            {
                int compId = Util.user.CompanyId;
                var data = stat.GetByStatus(compId, true);
                dtPagerlvData.Visible = (data.Count > dtPagerlvData.PageSize);
                BindControl.BindListView(lvData, data);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            lvData.InsertItemPosition = InsertItemPosition.LastItem;
            LoadTrainingStatus();
        }

        void AddNewTrainingStatus(ListViewCommandEventArgs e)
        {
            try
            {
                string name = (e.Item.FindControl("txtName") as TextBox).Text;
                string description = (e.Item.FindControl("txtDescription") as TextBox).Text;
                string username = Util.user.LoginId;

                int companyId = Util.user.CompanyId;
                if (string.IsNullOrEmpty(name))
                {
                    BindControl.BindLiteral(LblError, Messages.GetRequiredFieldsMessage(true, "Status Name"));
                    return;
                }

                bool added = stat.Add(new TrainingStatus
                {
                    Active = true,
                    CompanyId = companyId,
                    StatusName = name,
                    StatusDescription = description,
                    CreatedBy = Util.user.LoginId
                });

                if (added)
                {
                    loadReadOnly();
                    LoadTrainingStatus();
                }
                BindControl.BindLiteral(LblError, added ? Messages.GetSaveMessage() : Messages.GetAlreadyExistMessage());
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void loadReadOnly()
        {
            lvData.EditIndex = -1;
            lvData.InsertItemPosition = InsertItemPosition.None;
        }

        void UpdateTrainingStatus(ListViewDataItem e)
        {
            try
            {
                var hdnTrainingStatusId = e.FindControl("hdnTrainingStatusId") as HiddenField;
                var hdnCompanyId = e.FindControl("hdnCompanyId") as HiddenField;
                var chkActive = e.FindControl("chkActive") as CheckBox;
                string name = (e.FindControl("txtEditName") as TextBox).Text;
                string description = (e.FindControl("txtEditDescription") as TextBox).Text;

                bool active = chkActive.Checked;

                int companyId = int.Parse(hdnCompanyId.Value);
                int statusId = int.Parse(hdnTrainingStatusId.Value);

                if (string.IsNullOrEmpty(name))
                {
                    BindControl.BindLiteral(LblError, Messages.GetRequiredFieldsMessage(true, "StatusName"));
                    return;
                }

                bool updated = stat.Update(new TrainingStatus
                {
                    Active = true,
                    CompanyId = companyId,
                    StatusName = name,
                    StatusDescription = description,
                    StatusId = statusId,
                    UpdatedBy = Util.user.LoginId
                });

                if (updated)
                {
                    loadReadOnly();
                    LoadTrainingStatus();
                }
                BindControl.BindLiteral(LblError, updated ? Messages.GetUpdateMessage() : Messages.GetAlreadyExistMessage());
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void RemoveTrainingStatus(ListViewDataItem e)
        {
            try
            {
                var hdnTrainingStatusId = e.FindControl("hdnTrainingStatusId") as HiddenField;
                if (string.IsNullOrEmpty(hdnTrainingStatusId.Value))
                {
                    BindControl.BindLiteral(LblError, Messages.GetDeleteFailedMessage());
                    return;
                }

                int statusId = int.Parse(hdnTrainingStatusId.Value);
                bool deactivated = stat.SetStatus(new TrainingStatus
                {
                    Active = false,
                    StatusId = statusId,
                    UpdatedBy = Util.user.LoginId
                });

                if (deactivated)
                {
                    loadReadOnly();
                    LoadTrainingStatus();
                }
                BindControl.BindLiteral(LblError, deactivated ? Messages.GetDeleteMessage() : Messages.GetDeleteFailedMessage());
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void CancelUpdate()
        {
            loadReadOnly();
            LoadTrainingStatus();
        }

        protected void lvData_ItemCanceling(object sender, ListViewCancelEventArgs e)
        {
            CancelUpdate();
        }

        protected void lvData_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            LblError.Text = "";
            if (e.Item.ItemType == ListViewItemType.InsertItem ||
                e.Item.ItemType == ListViewItemType.DataItem)
            {
                switch (e.CommandName)
                {
                    case "add":
                        AddNewTrainingStatus(e);
                        break;
                }
            }
        }

        protected void lvData_ItemDeleting(object sender, ListViewDeleteEventArgs e)
        {
            ListViewDataItem el = lvData.Items[e.ItemIndex];
            RemoveTrainingStatus(el);
        }

        protected void lvData_ItemUpdating(object sender, ListViewUpdateEventArgs e)
        {
            ListViewDataItem el = lvData.Items[e.ItemIndex];
            UpdateTrainingStatus(el);
        }

        protected void lvData_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            lvData.EditIndex = e.NewEditIndex;
            lvData.InsertItemPosition = InsertItemPosition.None;
            LoadTrainingStatus();
        }

        protected void lvData_PagePropertiesChanged(object sender, EventArgs e)
        {
            LoadTrainingStatus();
        }

        protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadTrainingStatus();
        }

        protected void lnkCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("index.aspx", true);
        }
    }
}