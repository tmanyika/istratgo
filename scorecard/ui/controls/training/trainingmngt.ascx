﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="trainingmngt.ascx.cs"
    Inherits="scorecard.ui.controls.training.trainingmngt" %>
<%@ Import Namespace="scorecard.implementations" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<h1>
    Assign Training</h1>
<asp:UpdatePanel ID="UpdatePanelUser" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:MultiView ID="vwMain" runat="server">
            <asp:View ID="vwData" runat="server">
                <div runat="server" id="searchPanel">
                    <fieldset>
                        <legend>Search</legend>
                        <table cellpadding="4" cellspacing="4" width="60%">
                            <tr>
                                <td>
                                    Category
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddCategory" runat="server">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    Employee Name
                                </td>
                                <td>
                                    <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:Button ID="btnFilter" runat="server" class="button" Text="Search" 
                                        OnClick="btnFilter_Click" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </div>
                <div>
                    <asp:ListView ID="lvData" runat="server" OnPagePropertiesChanged="lvData_PagePropertiesChanged"
                        OnItemCommand="lvData_ItemCommand" OnItemEditing="lvData_ItemEditing">
                        <LayoutTemplate>
                            <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                                <thead>
                                    <tr>
                                        <td style="width: 320px">
                                            Full Name
                                        </td>
                                        <td style="width: 220px">
                                            Training Category
                                        </td>
                                        <td style="width: 350px">
                                            Training Course
                                        </td>
                                        <td style="width: 100px">
                                            Start Date
                                        </td>
                                        <td style="width: 100px">
                                            End Date
                                        </td>
                                        <td style="width: 350px">
                                            Related Performance Measure
                                        </td>
                                        <td style="width: 250px">
                                            Organisation Unit
                                        </td>
                                        <td style="width: 200px">
                                            Status
                                        </td>
                                        <td align="center" style="width: 200px">
                                            Update Status
                                        </td>
                                        <td style="width: 70px">
                                            Edit
                                        </td>
                                    </tr>
                                    <tr id="ItemPlaceHolder" runat="server">
                                    </tr>
                                </thead>
                            </table>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tbody>
                                <tr class="odd">
                                    <td>
                                        <asp:Literal ID="lblEmployeeName" runat="server" Text='<%# Eval("Emp.FullName")%>'></asp:Literal>
                                        <asp:HiddenField ID="hdnId" runat="server" Value='<%# Eval("TrainingId")%>' />
                                    </td>
                                    <td>
                                        <%# Eval("Cat.CategoryName")%>
                                    </td>
                                    <td>
                                        <%# Eval("Course.TrainingName")%>
                                    </td>
                                    <td>
                                        <%# Common.GetDescriptiveDate(Eval("StartDate")) %>
                                    </td>
                                    <td>
                                        <%# Common.GetDescriptiveDate(Eval("EndDate")) %>
                                    </td>
                                    <td>
                                        <%# Eval("Measure.PerformanceMeasureName")%>
                                    </td>
                                    <td>
                                         <asp:Literal ID="lblOrgUnitName" runat="server" Text='<%# Eval("OrgUnit.OrganisationName")%>'></asp:Literal>
                                    </td>
                                    <td>
                                        <%# Eval("Status.StatusName")%>
                                    </td>
                                    <td align="center">
                                        <div style="width: 100%; text-align: center">
                                            <a href='<%# GetUrl(Eval("TrainingId"), Eval("Course.TrainingName"), Eval("StartDate"))  %>' onclick="return hs.htmlExpand(this, { objectType: 'iframe', width: 900, height: 500, creditsPosition: 'bottom left', headingText: '<%# StrConvToUpper(Eval("Emp.FullName")) %> - <%# StrConvToUpper(Eval("Course.TrainingName")) %> TRAINING', wrapperClassName: 'titlebar' } )">
                                                <span class="links">Update Status</span></a>
                                        </div>
                                    </td>
                                    <td>
                                        <ul id="icons">
                                            <asp:LinkButton runat="server" ID="lnkEdit" CommandName="edit" class="ui-state-default ui-corner-all"
                                                title="Edit Record"><span class="ui-icon ui-icon-pencil">
                                    </span></asp:LinkButton>
                                        </ul>
                                    </td>
                                </tr>
                            </tbody>
                        </ItemTemplate>
                        <EmptyItemTemplate>
                            <p class="errorMsg">
                                There are no records to display.</p>
                        </EmptyItemTemplate>
                    </asp:ListView>
                    <br />
                    <asp:DataPager ID="PagerData" runat="server" PagedControlID="lvData" PageSize="10">
                        <Fields>
                            <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True" ShowNextPageButton="False"
                                ShowPreviousPageButton="False" />
                            <asp:NumericPagerField />
                            <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True" ShowNextPageButton="False"
                                ShowPreviousPageButton="False" />
                        </Fields>
                    </asp:DataPager>
                    <br />
                    <br />
                    <div>
                        <p class="errorMsg">
                            <br />
                            <asp:Literal ID="lblMsg" runat="server"></asp:Literal>
                            <br />
                            <br />
                        </p>
                    </div>
                    <div id="tabs-3">
                        <p>
                            <asp:Button ID="btnNew" runat="server" CssClass="button" Text="Assign New Training To Individual"
                                OnClick="btnNew_Click" CausesValidation="false" />
                            <asp:Button ID="lnkBack" runat="server" CssClass="button" Text="Done" CausesValidation="false"
                                PostBackUrl="~/ui/index.aspx" />
                        </p>
                    </div>
                </div>
            </asp:View>
            <asp:View ID="vwForm" runat="server">
                <fieldset runat="server" id="pnlContactInfo">
                    <legend>Assign New Training To Individual</legend>
                    <p>
                        <label for="lf">
                            Training Category:
                        </label>
                        <span class="field_desc">&nbsp;&nbsp;<asp:DropDownList CssClass="dropdown" runat="server"
                            ID="ddCat" AutoPostBack="True" OnSelectedIndexChanged="ddCat_SelectedIndexChanged" />
                        </span>
                    </p>
                    <p>
                        <label for="lf">
                            Training Course:
                        </label>
                        <span class="field_desc">&nbsp;&nbsp;<asp:DropDownList CssClass="dropdown" runat="server"
                            ID="ddCourse" />
                        </span>
                    </p>
                    <p>
                        <label for="lf">
                            Organisation Unit:
                        </label>
                        <span class="field_desc">&nbsp;&nbsp;<asp:DropDownList CssClass="dropdown" runat="server"
                            ID="ddOrgUnit" AutoPostBack="True" OnSelectedIndexChanged="ddOrgUnit_SelectedIndexChanged" />
                        </span>
                    </p>
                    <p>
                        <label for="lf">
                            Employee:
                        </label>
                        <span class="field_desc">&nbsp;&nbsp;<asp:DropDownList CssClass="dropdown" runat="server"
                            ID="ddEmployee" AutoPostBack="True" OnSelectedIndexChanged="ddEmployee_SelectedIndexChanged" />
                        </span>
                    </p>
                    <p>
                        <label for="sf">
                            Start Date:</label><span class="field_desc">&nbsp;&nbsp;
                                <asp:TextBox ID="txtStartDate" runat="server" Width="120px" ValidationGroup="Form"></asp:TextBox></span>
                        <asp:CalendarExtender ID="txtDate_CalendarExtender" runat="server" Enabled="True"
                            Format="d/MM/yyyy" TargetControlID="txtStartDate">
                        </asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="rqSDate" runat="server" ErrorMessage="Start Date is required"
                            Display="Dynamic" ControlToValidate="txtStartDate" ForeColor="Red" Font-Bold="false"
                            ValidationGroup="Form"></asp:RequiredFieldValidator>
                    </p>
                    <p>
                        <label for="sf">
                            End Date:</label><span class="field_desc">&nbsp;&nbsp;
                                <asp:TextBox ID="txtEndDate" runat="server" Width="120px" ValidationGroup="Form"></asp:TextBox></span>
                        <asp:CalendarExtender ID="txtEndDate_CalendarExtender" runat="server" Enabled="True"
                            Format="d/MM/yyyy" TargetControlID="txtEndDate">
                        </asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="rqEDate" runat="server" ErrorMessage="End Date is required"
                            Display="Dynamic" ControlToValidate="txtEndDate" ForeColor="Red" Font-Bold="false"
                            ValidationGroup="Form"></asp:RequiredFieldValidator>
                    </p>
                    <p>
                        <label for="lf">
                            Related Performance Measure:
                        </label>
                        <span class="field_desc">&nbsp;&nbsp;<asp:DropDownList CssClass="dropdown" runat="server"
                            ID="ddMeasure" />
                        </span>
                    </p>
                    <p>
                        <label for="lf">
                            Reason For Training Intervention:
                        </label>
                        <span class="field_desc">&nbsp;
                            <asp:TextBox ID="txtReason" runat="server" Width="500px"></asp:TextBox>
                        </span>
                    </p>
                    <p class="errorMsg">
                        <br />
                        <br />
                        <asp:Literal ID="LblMsgForm" runat="server"></asp:Literal><br />
                        <br />
                        <br />
                    </p>
                    <p>
                        <label>
                            <asp:HiddenField ID="hdnTrainingId" runat="server" />
                            &nbsp;</label>
                        <asp:Button ID="btnSave" runat="server" class="button" Text="Submit" OnClick="btnSave_Click"
                            ValidationGroup="Form" />
                        <asp:Button ID="btnCancelComplete" runat="server" class="button" Text="Cancel" CausesValidation="False"
                            OnClick="btnCancelComplete_Click" />
                    </p>
                </fieldset>
            </asp:View>
        </asp:MultiView>
        <asp:UpdateProgress ID="UpdateProgressUser" runat="server" AssociatedUpdatePanelID="UpdatePanelUser">
            <ProgressTemplate>
                <div>
                    <asp:Image ID="imgOrg" runat="server" ImageUrl="~/ui/images/icons/activity.gif" AlternateText="Please wait..">
                    </asp:Image>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
