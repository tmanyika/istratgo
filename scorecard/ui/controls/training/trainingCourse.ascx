﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="trainingCourse.ascx.cs"
    Inherits="scorecard.ui.controls.training.trainingCourse" %>
<%@ Import Namespace="scorecard.implementations" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:UpdatePanel ID="UpdatePanelData" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <h1>
            Training Course Management</h1>
        <div>
            <asp:MultiView ID="mainView" runat="server">
                <asp:View ID="vwData" runat="server">
                    <div>
                        <fieldset runat="server" id="searchPanel">
                            <legend>Filter Training Courses</legend>
                            <table cellpadding="4" cellspacing="4" width="40%">
                                <tr>
                                    <td>
                                        Select Category
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddCategory" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:Button ID="btnFilter" runat="server" class="button" Text="Search Courses" OnClick="btnFilter_Click" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </div>
                    <div>
                        <asp:ListView ID="lvData" runat="server" OnPagePropertiesChanged="lvData_PagePropertiesChanged"
                            OnItemDeleting="lvData_ItemDeleting" OnItemCommand="lvData_ItemCommand" 
                            onitemediting="lvData_ItemEditing">
                            <LayoutTemplate>
                                <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                                    <thead>
                                        <tr id="trFirst" runat="server">
                                            <td style="width: 180px">
                                                Category
                                            </td>
                                            <td style="width: 280px">
                                                Training Name
                                            </td>
                                            <td style="width: 380px; text-align: left;">
                                                Training Description
                                            </td>
                                            <td style="width: 160px; text-align: left;">
                                                NQF Level
                                            </td>
                                            <td style="width: 120px; text-align: left;">
                                                Exam Required
                                            </td>
                                            <td width="70px">
                                                Edit
                                            </td>
                                            <td width="70px">
                                                Delete
                                            </td>
                                        </tr>
                                        <tr id="ItemPlaceHolder" runat="server">
                                        </tr>
                                    </thead>
                                </table>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tbody>
                                    <tr class="odd">
                                        <td>
                                            <%# Eval("Cat.CategoryName") %>
                                            <asp:HiddenField ID="hdnCourseId" runat="server" Value='<%# Eval("CourseId") %>' />
                                        </td>
                                        <td>
                                            <%# Eval("TrainingName")%>
                                        </td>
                                        <td>
                                            <%# Eval("TrainingDescription")%>
                                        </td>
                                        <td>
                                            <%# Eval("Nqf.NqfLevelName") %>
                                        </td>
                                        <td>
                                            <%# Common.GetBooleanYesNo(Eval("ExamRequired"))%>
                                        </td>
                                        <td>
                                            <ul id="icons">
                                                <asp:LinkButton runat="server" ID="lnkEdit" CommandName="edit" class="ui-state-default ui-corner-all"
                                                    title="Edit Record"><span class="ui-icon ui-icon-pencil">
                                    </span></asp:LinkButton>
                                            </ul>
                                        </td>
                                        <td>
                                            <ul id="icons">
                                                <asp:LinkButton runat="server" ID="lnkDelete" CommandName="delete" class="ui-state-default ui-corner-all"
                                                    title="Delete Record" OnClientClick="return confirmDelete();"><span class="ui-icon ui-icon-trash">
                                    </span>
                                                </asp:LinkButton>
                                            </ul>
                                        </td>
                                    </tr>
                                </tbody>
                            </ItemTemplate>
                            <EmptyDataTemplate>
                                <span style="color: Red">There are no records to display.</span>
                            </EmptyDataTemplate>
                        </asp:ListView>
                        <br />
                    </div>
                    <asp:DataPager ID="dtPagerlvData" runat="server" PagedControlID="lvData" PageSize="15">
                        <Fields>
                            <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True" ShowNextPageButton="False"
                                ShowPreviousPageButton="False" />
                            <asp:NumericPagerField />
                            <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True" ShowNextPageButton="False"
                                ShowPreviousPageButton="False" />
                        </Fields>
                    </asp:DataPager>
                    <div>
                        <span style="color: Red">
                            <asp:Literal ID="LblError" runat="server" /></span>
                    </div>
                    <div>
                        <p>
                            <br />
                            <asp:Label ID="lblMsg" runat="server" Style="color: #FF3300" />
                            <br />
                        </p>
                    </div>
                    <div id="tabs-3">
                        <p>
                            <asp:Button ID="btnNew" runat="server" CssClass="button" Text="Add New Training Course"
                                OnClick="btnNew_Click" CausesValidation="false" />
                            <asp:Button ID="lnkCancel0" runat="server" CssClass="button" Text="Done" CausesValidation="false"
                                PostBackUrl="~/ui/index.aspx" />
                        </p>
                    </div>
                </asp:View>
                <asp:View ID="vwForm" runat="server">
                    <fieldset runat="server" id="pnlContactInfo">
                        <legend>Training Course Information </legend>
                        <p>
                            <label for="lf">
                                Category Name:
                            </label>
                            <span class="field_desc">&nbsp;&nbsp;<asp:DropDownList CssClass="dropdown" runat="server"
                                ID="ddCat" />
                            </span>
                        </p>
                        <p>
                            <label for="lf">
                                NQF Level:
                            </label>
                            <span class="field_desc">&nbsp;&nbsp;<asp:DropDownList CssClass="dropdown" runat="server"
                                ID="ddNqf" />
                            </span>
                        </p>
                        <p>
                            <label for="sf">
                                Training Name:</label>
                            <span class="field_desc">&nbsp;&nbsp;<asp:TextBox ID="txtTrainingName" runat="server"
                                ValidationGroup="Form" Width="600px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rqTrainName" runat="server" ErrorMessage="Training name is required"
                                    Display="Dynamic" ControlToValidate="txtTrainingName" ForeColor="Red" Font-Bold="false"
                                    ValidationGroup="Form"></asp:RequiredFieldValidator></span>
                        </p>
                        <p>
                            <label for="sf">
                                Training Description:</label><span class="field_desc"> &nbsp;&nbsp;<asp:TextBox ID="txtDescription"
                                    runat="server" ValidationGroup="Form" Width="600px"></asp:TextBox></span>
                        </p>
                        <p>
                            <label for="lf">
                                Exam Is Required:
                            </label>
                            <span class="field_desc">
                                <asp:CheckBox ID="chkExamReq" runat="server" />
                            </span>
                        </p>
                        <p>
                            <label for="lf">
                                Active:
                            </label>
                            <span class="field_desc">
                                <asp:CheckBox ID="chkActive" runat="server" />
                            </span>
                        </p>
                        <p class="errorMsg">
                            <br />
                            <br />
                            <asp:Literal ID="LblMsgForm" runat="server"></asp:Literal><br />
                            <br />
                        </p>
                        <p>
                            <label>
                                <asp:HiddenField ID="hdnCourseId" runat="server" />
                                &nbsp;</label>
                            <asp:Button ID="btnSave" runat="server" class="button" Text="Submit" OnClick="btnSave_Click"
                                ValidationGroup="Form" />
                            <asp:Button ID="btnCancelComplete" runat="server" class="button" Text="Cancel" CausesValidation="False"
                                OnClick="btnCancelComplete_Click" />
                        </p>
                    </fieldset>
                </asp:View>
            </asp:MultiView>
            <asp:UpdateProgress ID="UpdateProgressData" runat="server" AssociatedUpdatePanelID="UpdatePanelData">
                <ProgressTemplate>
                    <div>
                        <asp:Image ID="imgOrg" runat="server" ImageUrl="~/ui/images/icons/activity.gif" AlternateText="Please wait..">
                        </asp:Image></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
