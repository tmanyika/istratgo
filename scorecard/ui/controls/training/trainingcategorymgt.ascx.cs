﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.implementations;
using HR.EmployeeTraining.Interfaces;
using HR.EmployeeTraining.Entities;
using HR.EmployeeTraining.Logic;

namespace scorecard.ui.controls.training
{
    public partial class trainingcategorymgt : System.Web.UI.UserControl
    {
        ITrainingCategory cat;

        public trainingcategorymgt()
        {
            cat = new TrainingCategoryBL();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ResetLabels();
            if (!IsPostBack)
            {
                LoadTrainingCategory();
            }
        }

        private void ResetLabels()
        {
            lblMsg.Text = "";
            LblError.Text = "";
        }

        void LoadTrainingCategory()
        {
            try
            {
                int compId = Util.user.CompanyId;
                var data = cat.GetByStatus(compId, true);
                dtPagerlvData.Visible = (data.Count > dtPagerlvData.PageSize);
                BindControl.BindListView(lvData, data);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            lvData.InsertItemPosition = InsertItemPosition.LastItem;
            LoadTrainingCategory();
        }

        void AddNewTrainingCategory(ListViewCommandEventArgs e)
        {
            try
            {
                string description = (e.Item.FindControl("txtDescription") as TextBox).Text;
                string name = (e.Item.FindControl("txtName") as TextBox).Text;
                string username = Util.user.LoginId;

                int companyId = Util.user.CompanyId;
                if (string.IsNullOrEmpty(name))
                {
                    BindControl.BindLiteral(LblError, Messages.GetRequiredFieldsMessage(true, "Name"));
                    return;
                }

                bool added = cat.Add(new TrainingCategory
                {
                    Active = true,
                    CompanyId = companyId,
                    CategoryName = name,
                    CategoryDescription = description,
                    CreatedBy = Util.user.LoginId
                });

                if (added)
                {
                    loadReadOnly();
                    LoadTrainingCategory();
                }
                BindControl.BindLiteral(LblError, added ? Messages.GetSaveMessage() : Messages.GetAlreadyExistMessage());
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void loadReadOnly()
        {
            lvData.EditIndex = -1;
            lvData.InsertItemPosition = InsertItemPosition.None;
        }

        void UpdateTrainingCategory(ListViewDataItem e)
        {
            try
            {
                var hdnCategoryId = e.FindControl("hdnCategoryId") as HiddenField;
                var hdnCompanyId = e.FindControl("hdnCompanyId") as HiddenField;
                var chkActive = e.FindControl("chkActive") as CheckBox;

                string description = (e.FindControl("txtEditDescription") as TextBox).Text;
                string name = (e.FindControl("txtEditName") as TextBox).Text;

                bool active = chkActive.Checked;

                int companyId = int.Parse(hdnCompanyId.Value);
                int categoryId = int.Parse(hdnCategoryId.Value);

                if (string.IsNullOrEmpty(name))
                {
                    BindControl.BindLiteral(LblError, Messages.GetRequiredFieldsMessage(true, "Name"));
                    return;
                }

                bool updated = cat.Update(new TrainingCategory
                {
                    Active = true,
                    CompanyId = companyId,
                    CategoryName = name,
                    CategoryDescription = description,
                    CategoryId = categoryId,
                    UpdatedBy = Util.user.LoginId
                });

                if (updated)
                {
                    loadReadOnly();
                    LoadTrainingCategory();
                }
                BindControl.BindLiteral(LblError, updated ? Messages.GetUpdateMessage() : Messages.GetAlreadyExistMessage());
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void RemoveTrainingCategory(ListViewDataItem e)
        {
            try
            {
                var hdnCategoryId = e.FindControl("hdnCategoryId") as HiddenField;
                if (string.IsNullOrEmpty(hdnCategoryId.Value))
                {
                    BindControl.BindLiteral(LblError, Messages.GetDeleteFailedMessage());
                    return;
                }

                int categoryId = int.Parse(hdnCategoryId.Value);
                bool deactivated = cat.SetStatus(new TrainingCategory
                {
                    Active = false,
                    CategoryId = categoryId,
                    UpdatedBy = Util.user.LoginId
                });

                if (deactivated)
                {
                    loadReadOnly();
                    LoadTrainingCategory();
                }
                BindControl.BindLiteral(LblError, deactivated ? Messages.GetDeleteMessage() : Messages.GetDeleteFailedMessage());
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void CancelUpdate()
        {
            loadReadOnly();
            LoadTrainingCategory();
        }

        protected void lvData_ItemCanceling(object sender, ListViewCancelEventArgs e)
        {
            CancelUpdate();
        }

        protected void lvData_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            LblError.Text = "";
            if (e.Item.ItemType == ListViewItemType.InsertItem ||
                e.Item.ItemType == ListViewItemType.DataItem)
            {
                switch (e.CommandName)
                {
                    case "add":
                        AddNewTrainingCategory(e);
                        break;
                }
            }
        }

        protected void lvData_ItemDeleting(object sender, ListViewDeleteEventArgs e)
        {
            ListViewDataItem el = lvData.Items[e.ItemIndex];
            RemoveTrainingCategory(el);
        }

        protected void lvData_ItemUpdating(object sender, ListViewUpdateEventArgs e)
        {
            ListViewDataItem el = lvData.Items[e.ItemIndex];
            UpdateTrainingCategory(el);
        }

        protected void lvData_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            lvData.EditIndex = e.NewEditIndex;
            lvData.InsertItemPosition = InsertItemPosition.None;
            LoadTrainingCategory();
        }

        protected void lvData_PagePropertiesChanged(object sender, EventArgs e)
        {
            LoadTrainingCategory();
        }

        protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadTrainingCategory();
        }

        protected void lnkCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("index.aspx", true);
        }
    }
}