﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.implementations;
using HR.Human.Resources;
using HR.EmployeeTraining.Interfaces;
using HR.EmployeeTraining.Logic;
using scorecard.interfaces;
using Scorecard.Interfaces;
using Scorecard.Logic;
using HR.EmployeeTraining.Entities;
using System.Text;

namespace scorecard.ui.controls.training
{
    public partial class trainingstatusupdate : System.Web.UI.UserControl
    {
        #region Member Variables

        readonly IPerformanceMeasure perform;
        readonly ITrainingCategory cat;
        readonly ITrainingCourse cos;
        readonly ITraining train;
        readonly IEmployee emp;
        readonly Iorgunit org;

        readonly int orgUnitId;
        readonly int compId;

        #endregion

        #region Properties

        bool IsAdministrator
        {
            get
            {
                int? roleId = null;
                if (Util.user != null)
                {
                    roleId = Util.user.UserTypeId;
                }
                return (roleId == Util.getTypeDefinitionsUserTypeAdministrator());
            }
        }

        #endregion

        #region Default Constructors

        public trainingstatusupdate()
        {
            orgUnitId = (Util.user != null) ? Util.user.OrgId : 0;
            compId = (Util.user != null) ? Util.user.CompanyId : 0;

            cat = new TrainingCategoryBL();
            org = new structureImpl();
            cos = new TrainingCourseBL();
            emp = new EmployeeBL();
            train = new TrainingBL();
            perform = new PerformanceMeasureBL();
        }

        #endregion

        #region Page Event Handling Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            ClearLabels();
            if (!IsPostBack)
            {
                LoadFilterForm();
                FillTraining();
            }
        }

        #endregion

        #region Databinding Methods

         void FillTraining()
        {
            try
            {
                ITraining obj = new TrainingBL();

                var employeeId = Util.user.UserId;
                var data =obj.GetByEmployeeId(employeeId) ;

                //search parameters
                var name = string.IsNullOrEmpty(txtName.Text) ? string.Empty : txtName.Text.ToString().Replace(" ", "").ToLower();
                var categoryId = int.Parse(ddCategory.SelectedValue);
                if (ddCategory.SelectedIndex != 0 || !string.IsNullOrEmpty(name))
                {
                    if (ddCategory.SelectedIndex != 0 && !string.IsNullOrEmpty(name))
                    {
                        data = data.Where(c => c.Cat.CategoryId == categoryId && c.Course.TrainingName.ToLower().Replace(" ", "").Contains(name)).ToList();
                    }
                    else if (ddCategory.SelectedIndex == 0 && !string.IsNullOrEmpty(name))
                    {
                        data = data.Where(c => c.Course.TrainingName.ToLower().Replace(" ", "").Contains(name)).ToList();
                    }
                    else if (ddCategory.SelectedIndex != 0 && string.IsNullOrEmpty(name))
                    {
                        data = data.Where(c => c.Cat.CategoryId == categoryId).ToList();
                    }
                }

                PagerData.Visible = (data.Count > PagerData.PageSize);
                ShowMessage(lblMsg, (data.Count > 0) ? string.Empty : Messages.NoRecordsToDisplay);

                BindControl.BindListView(lvData, data);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void LoadFilterForm()
        {
            LoadCategories(ddCategory, string.Empty);
        }

        void DropDownNothing(DropDownList dd, string selectMessage)
        {
            dd.Items.Clear();
            dd.Items.Add(new ListItem
            {
                Text = selectMessage,
                Value = "-1"
            });
        }

        void LoadCategories(DropDownList dd, string defaultValue)
        {
            if (dd == null) return;
            var data = cat.GetByStatus(compId, true);
            BindControl.BindDropdown(dd, "CategoryName", "CategoryId", "- select all -", "-1", defaultValue, data);
        }

        #endregion

        #region Utility Methods

        void ShowMessage(Literal lbl, string msg)
        {
            lbl.Text = msg;
        }

        public string StrConvToUpper(object fullname)
        {
            return fullname.ToString().ToUpper();
        }

        public string GetUrl(object trainingId, object fullName, object startdate)
        {
            string date = Common.GetInternationalDateFormat(DateTime.Parse(startdate.ToString()));
            return string.Format("trainingstateupdate.aspx?Id={0}&Cos={1}&Sdate={2}", trainingId, HttpUtility.HtmlEncode(fullName), date);
        }

        void ClearLabels()
        {
            lblMsg.Text = string.Empty;
        }
        
        #endregion

        #region ListView Event Handling Methods

        protected void lvData_PagePropertiesChanged(object sender, EventArgs e)
        {
            FillTraining();
        }

        #endregion

        #region Button Event Handling Methods

        protected void btnFilter_Click(object sender, EventArgs e)
        {
            FillTraining();
        }

        #endregion
    }
}