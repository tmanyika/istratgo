﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="trainingstatusdetail.ascx.cs"
    Inherits="scorecard.ui.controls.training.trainingstatusdetail" %>
<%@ Import Namespace="scorecard.implementations" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<div>
    <br />
    <br />
    <%-- <asp:UpdatePanel ID="UpdatePanelData" runat="server">
        <ContentTemplate>--%>
    <asp:MultiView ID="mainView" runat="server">
        <asp:View ID="vwData" runat="server">
            <div>
                <asp:ListView ID="lvData" runat="server" OnItemDeleting="lvData_ItemDeleting" OnItemCommand="lvData_ItemCommand">
                    <LayoutTemplate>
                        <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                            <thead>
                                <tr id="trFirst">
                                    <td style="width: 280px">
                                        Training Name
                                    </td>
                                    <td style="width: 220px; text-align: left;">
                                        Status
                                    </td>
                                    <td style="width: 120px; text-align: left;">
                                        Date Achieved
                                    </td>
                                    <td style="width: 400px; text-align: left;">
                                        Comment
                                    </td>
                                    <td style="width: 120px; text-align: center;">
                                        Download
                                    </td>
                                    <td width="70px">
                                        Delete
                                    </td>
                                </tr>
                                <tr id="ItemPlaceHolder" runat="server">
                                </tr>
                            </thead>
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tbody>
                            <tr class="odd">
                                <td>
                                    <%# Eval("Course.TrainingName")%>
                                    <asp:HiddenField ID="hdnTrailId" runat="server" Value='<%# Eval("TrailId") %>' />
                                    <asp:HiddenField ID="hdnTrainingId" runat="server" Value='<%# Eval("TrainingId") %>' />
                                </td>
                                <td>
                                    <%# Eval("Status.StatusName")%>
                                </td>
                                <td>
                                    <%# Common.GetDescriptiveDate(Eval("DateAchieved")) %>
                                </td>
                                <td>
                                    <%# Eval("Comments")%>
                                </td>
                                <td style="width: 120px; text-align: center;">
                                    <asp:HyperLink ID="lnkFile" runat="server" Target="_blank" NavigateUrl='<%# GetFileUrl(Eval("DocumentName")) %>'>Download</asp:HyperLink>
                                </td>
                                <td>
                                    <ul id="icons">
                                        <asp:LinkButton runat="server" ID="lnkDelete" CommandName="delete" class="ui-state-default ui-corner-all"
                                            title="Delete Record" OnClientClick="return confirmDelete();"><span class="ui-icon ui-icon-trash">
                                    </span>
                                        </asp:LinkButton>
                                    </ul>
                                </td>
                            </tr>
                        </tbody>
                    </ItemTemplate>
                </asp:ListView>
                <br />
            </div>
            <div>
                <span style="color: Red; font-size: small;">
                    <asp:Literal ID="lblMsg" runat="server" /></span><br />
                <br />
            </div>
            <div id="tabs-3">
                <p>
                    <asp:Button ID="btnNew" runat="server" CssClass="button" Text="Update Training Status"
                        OnClick="btnNew_Click" CausesValidation="false" />
                   
                </p>
            </div>
        </asp:View>
        <asp:View ID="vwFormStatusUpdate" runat="server">
            <fieldset runat="server" id="pnlForm">
                <legend>Training Status Update </legend>
                <p>
                    <label for="sf">
                        Training Name:</label>
                    <span class="field_desc" style="font-style: normal">
                        <asp:Literal ID="lblTrainingName" runat="server"></asp:Literal></span>
                </p>
                <p>
                    <br />
                </p>
                <p>
                    <label for="sf">
                        Training Start Date:</label>
                    <span class="field_desc" style="font-style: normal">
                        <asp:Literal ID="lblStartDate" runat="server"></asp:Literal></span>
                </p>
                <p>
                    <br />
                </p>
                <p>
                    <label for="lf">
                        Training Status:
                    </label>
                    <span class="field_desc">
                        <asp:DropDownList CssClass="dropdown" runat="server" ID="ddStatus" DataTextField="StatusName"
                            DataValueField="StatusId" />
                    </span>
                </p>
                <p>
                    <label for="sf">
                        Date Achieved:</label><span class="field_desc">
                            <asp:TextBox ID="txtDateAchieved" runat="server" Width="120px" ValidationGroup="Form"></asp:TextBox></span>
                    <asp:CalendarExtender ID="txtDateAchieved_CalendarExtender" runat="server" Enabled="True"
                        Format="d/MM/yyyy" TargetControlID="txtDateAchieved">
                    </asp:CalendarExtender>
                    <asp:RequiredFieldValidator ID="rqSDate" runat="server" ErrorMessage="Date Achieved is required"
                        Display="Dynamic" ControlToValidate="txtDateAchieved" ForeColor="Red" Font-Bold="false"
                        ValidationGroup="Form"></asp:RequiredFieldValidator>
                </p>
                <p>
                    <label for="lf">
                        Attach Proof:
                    </label>
                    <span class="field_desc">
                        <asp:FileUpload ID="FileUpload1" runat="server" Width="220px" /></span>
                </p>
                <p>
                    <label for="sf">
                        Comment:</label>
                    <span class="field_desc">
                        <asp:TextBox ID="txtComment" runat="server" ValidationGroup="Form" Width="600px"></asp:TextBox>
                    </span>
                </p>
                <p class="errorMsg">
                    <br />
                    <br />
                    <asp:Literal ID="lblFormMsg" runat="server"></asp:Literal><br />
                    <br />
                </p>
                <p>
                    <label>
                        &nbsp;</label>
                    <asp:Button ID="btnSave" runat="server" class="button" Text="Submit" OnClick="btnSave_Click"
                        ValidationGroup="Form" />
                    <asp:Button ID="btnCancelComplete" runat="server" class="button" Text="Cancel" CausesValidation="False"
                        OnClick="btnCancelComplete_Click" />
                </p>
            </fieldset>
        </asp:View>
        <asp:View ID="vwDelete" runat="server">
            <p>
                <label for="sf">
                    Reason For Deletion:</label>
                <span class="field_desc">&nbsp;&nbsp;<asp:TextBox ID="txtReason" runat="server" ValidationGroup="Delete"
                    Width="600px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rqdDel" runat="server" ErrorMessage="Reason for deletion is required"
                        Display="Dynamic" ControlToValidate="txtReason" ForeColor="Red" Font-Bold="false"
                        ValidationGroup="Delete"></asp:RequiredFieldValidator></span>
            </p>
            <p class="errorMsg">
                <br />
                <br />
                <asp:Literal ID="lblDelMsg" runat="server"></asp:Literal><br />
                <br />
                <asp:HiddenField ID="hdnTrailId" runat="server" />
                <asp:HiddenField ID="hdnTrainingId" runat="server" />
            </p>
            <p>
                <label>
                    &nbsp;</label>
                <asp:Button ID="btnDelete" runat="server" class="button" Text="Confirm Deletion"
                    ValidationGroup="Delete" OnClick="btnDelete_Click" />
                <asp:Button ID="btnCancelDeletion" runat="server" class="button" Text="Cancel" CausesValidation="False"
                    OnClick="btnCancelDeletion_Click" />
            </p>
        </asp:View>
    </asp:MultiView>
    <%-- <asp:UpdateProgress ID="UpdateProgressData" runat="server" AssociatedUpdatePanelID="UpdatePanelData">
                <ProgressTemplate>
                    <div>
                        <asp:Image ID="imgOrg" runat="server" ImageUrl="~/ui/images/icons/activity.gif" AlternateText="Please wait..">
                        </asp:Image></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lvData" />
            <asp:PostBackTrigger ControlID="btnSave" />
        </Triggers>
    </asp:UpdatePanel>--%>
</div>
