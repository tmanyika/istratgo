﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="nqflevelmgt.ascx.cs"
    Inherits="scorecard.ui.controls.education.nqflevelmgt" %>
<%@ Import Namespace="scorecard.implementations" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:UpdatePanel ID="UpdatePanelArea" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <h1>
            NQF Level Management
        </h1>
        <div>
            <fieldset runat="server" id="searchPanel">
                <legend>Filter NQF Levels</legend>
                <table cellpadding="4" cellspacing="4" width="60%">
                    <tr>
                        <td>
                            Select Country
                        </td>
                        <td>
                            <asp:DropDownList ID="ddCountryS" runat="server" class="dropdown" DataValueField="TYPE_ID"
                                    DataTextField="NAME">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:Button ID="btnFilter" runat="server" class="button" Text="Search NQF Levels" OnClick="btnFilter_Click" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </div>
        <div>
            <asp:ListView ID="lvData" runat="server" OnItemCommand="lvData_ItemCommand" OnItemEditing="lvData_ItemEditing"
                OnPagePropertiesChanged="lvData_PagePropertiesChanged" OnItemCanceling="lvData_ItemCanceling"
                OnItemDeleting="lvData_ItemDeleting" OnItemUpdating="lvData_ItemUpdating">
                <LayoutTemplate>
                    <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                        <thead>
                            <tr id="trFirst" runat="server">
                                <td style="width: 250px">
                                    Country
                                </td>
                                <td style="width: 300px">
                                    NQF Level Name
                                </td>
                                <td style="width: 280px; text-align: left;">
                                    Description
                                </td>
                                <td style="width: 100px">
                                    NQF Code
                                </td>
                                <td style="width: 90px">
                                    Active
                                </td>
                                <td width="80px">
                                    Edit
                                </td>
                                <td width="80px">
                                    Delete
                                </td>
                            </tr>
                            <tr id="ItemPlaceHolder" runat="server">
                            </tr>
                        </thead>
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <tbody>
                        <tr class="odd">
                            <td>
                                <%# Eval("Nation.CountryName")%>
                            </td>
                            <td>
                                <%# Eval("NqfLevelName")%>
                                <asp:HiddenField ID="hdnNqfLevelId" runat="server" Value='<%# Bind("NqfLevelId")%>' />
                                <asp:HiddenField ID="hdnCountryId" runat="server" Value='<%# Bind("CountryId")%>' />
                            </td>
                            <td>
                                <%# Eval("NqfLevelDescription")%>
                            </td>
                            <td>
                                <%# Eval("NqfLevelCode")%>
                            </td>
                            <td>
                                <%# Common.GetBooleanYesNo(Eval("Active"))%>
                            </td>
                            <td>
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="lnkEdit" CommandName="edit" class="ui-state-default ui-corner-all"
                                        title="Edit Record"><span class="ui-icon ui-icon-pencil">
                                    </span></asp:LinkButton>
                                </ul>
                            </td>
                            <td>
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="lnkDelete" CommandName="delete" class="ui-state-default ui-corner-all"
                                        title="Delete Record" OnClientClick="return confirmDelete();"><span class="ui-icon ui-icon-trash">
                                    </span>
                                    </asp:LinkButton>
                                </ul>
                            </td>
                        </tr>
                    </tbody>
                </ItemTemplate>
                <EditItemTemplate>
                    <tbody>
                        <tr class="odd">
                            <td align="left" valign="top">
                                <asp:DropDownList ID="ddCountryE" runat="server" Width="200px" class="dropdown" DataValueField="TYPE_ID"
                                    DataTextField="NAME">
                                </asp:DropDownList>
                                <asp:HiddenField ID="hdnNqfLevelId" runat="server" Value='<%# Bind("NqfLevelId")%>' />
                                <asp:HiddenField ID="hdnCountryId" runat="server" Value='<%# Bind("CountryId")%>' />
                            </td>
                            <td align="left" valign="top">
                                <asp:TextBox runat="server" ID="txtEditName" Text='<%# Eval("NqfLevelName")%>' ValidationGroup="Edit"
                                    Width="250px" />
                                <asp:RequiredFieldValidator ID="RqdArea" runat="server" ErrorMessage="*" ValidationGroup="Edit"
                                    ControlToValidate="txtEditName" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                            <td align="left" valign="top">
                                <asp:TextBox runat="server" ID="txtEditDescription" Text='<%# Eval("NqfLevelDescription")%>'
                                    Width="220px" />
                            </td>
                            <td align="left" valign="top">
                                <asp:TextBox runat="server" ID="txtEditCode" Text='<%# Eval("NqfLevelCode")%>' Width="130px" />
                            </td>
                            <td align="left" valign="top">
                                <asp:CheckBox ID="chkActive" runat="server" Checked='<%# Eval("Active")%>' />
                            </td>
                            <td valign="top">
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="lnkSave" CommandName="update" class="ui-state-default ui-corner-all"
                                        title="Update Record" ValidationGroup="Edit"><span class="ui-icon ui-icon-disk">
                                     </span>
                                    </asp:LinkButton>
                                </ul>
                            </td>
                            <td valign="top">
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="lnkCancelUpdate" CommandName="cancel" class="ui-state-default ui-corner-all"
                                        title="Cancel Update" CausesValidation="false"><span class="ui-icon ui-icon-cancel">
                                    </span>
                                    </asp:LinkButton>
                                </ul>
                            </td>
                        </tr>
                    </tbody>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <tbody>
                        <tr>
                            <td align="left" valign="top">
                                <asp:DropDownList ID="ddCountry" runat="server" Width="200px" class="dropdown" DataValueField="TYPE_ID"
                                    DataTextField="NAME">
                                </asp:DropDownList>
                            </td>
                            <td align="left" valign="top">
                                <asp:TextBox runat="server" ID="txtName" ValidationGroup="Insert" Width="250px" />
                                <asp:RequiredFieldValidator ID="RqdArea" runat="server" ErrorMessage="*" ValidationGroup="Insert"
                                    ControlToValidate="txtName" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                            <td align="left" valign="top">
                                <asp:TextBox runat="server" ID="txtDescription" ValidationGroup="Insert" Width="280px" />
                            </td>
                            <td align="left" valign="top">
                                <asp:TextBox runat="server" ID="txtCode" Width="130px" />
                            </td>
                            <td valign="top">
                                <asp:CheckBox ID="chkActive" runat="server" Checked="true" Enabled="false" />
                            </td>
                            <td valign="top">
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="lnkSave" class="ui-state-default ui-corner-all"
                                        title="Save this Record" CommandName="add" ValidationGroup="Insert"><span class="ui-icon ui-icon-disk">
                        </span></asp:LinkButton>
                                </ul>
                            </td>
                            <td valign="top">
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="lnkCancel" class="ui-state-default ui-corner-all"
                                        title="Cancel Save " CommandName="cancel" CausesValidation="false"><span class="ui-icon ui-icon-cancel">
                        </span></asp:LinkButton>
                                </ul>
                            </td>
                        </tr>
                    </tbody>
                </InsertItemTemplate>
                <EmptyDataTemplate>
                    <span style="color: Red">There are no records to display.</span>
                </EmptyDataTemplate>
            </asp:ListView>
            <br />
            <asp:DataPager ID="dtPagerlvData" runat="server" PagedControlID="lvData" PageSize="15">
                <Fields>
                    <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True" ShowNextPageButton="False"
                        ShowPreviousPageButton="False" />
                    <asp:NumericPagerField />
                    <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True" ShowNextPageButton="False"
                        ShowPreviousPageButton="False" />
                </Fields>
            </asp:DataPager>
            <div>
                <span style="color: Red">
                    <asp:Literal ID="LblError" runat="server" /></span>
            </div>
            <div>
                <p>
                    <br />
                    <asp:Label ID="lblMsg" runat="server" Style="color: #FF3300" />
                    <br />
                </p>
            </div>
            <div id="tabs-3">
                <p>
                    <asp:Button ID="btnNew" runat="server" CssClass="button" Text="Add New NQF Level"
                        OnClick="btnNew_Click" CausesValidation="false" />
                    <asp:Button ID="lnkCancel0" runat="server" CssClass="button" Text="Done" CausesValidation="false"
                        PostBackUrl="~/ui/index.aspx" />
                </p>
            </div>
            <asp:UpdateProgress ID="UpdateProgressArea" runat="server" AssociatedUpdatePanelID="UpdatePanelArea">
                <ProgressTemplate>
                    <div>
                        <asp:Image ID="imgOrg" runat="server" ImageUrl="~/ui/images/icons/activity.gif" AlternateText="Please wait..">
                        </asp:Image></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
