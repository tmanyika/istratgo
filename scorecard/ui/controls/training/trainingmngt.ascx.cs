﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.implementations;
using HR.Human.Resources;
using HR.EmployeeTraining.Interfaces;
using HR.EmployeeTraining.Logic;
using scorecard.interfaces;
using Scorecard.Interfaces;
using Scorecard.Logic;
using HR.EmployeeTraining.Entities;
using System.Text;
using scorecard.entities;

namespace scorecard.ui.controls.training
{
    public partial class trainingmngt : System.Web.UI.UserControl
    {
        #region Member Variables

        readonly IPerformanceMeasure perform;
        readonly ITrainingCategory cat;
        readonly ITrainingCourse cos;
        readonly ITraining train;
        readonly IEmployee emp;
        readonly Iorgunit org;

        readonly int orgUnitId;
        readonly int compId;

        #endregion

        #region Properties

        bool IsAdministrator
        {
            get
            {
                int? roleId = null;
                if (Util.user != null)
                {
                    roleId = Util.user.UserTypeId;
                }
                return (roleId == Util.getTypeDefinitionsUserTypeAdministrator());
            }
        }

        #endregion

        #region Default Constructors

        public trainingmngt()
        {
            orgUnitId = (Util.user != null) ? Util.user.OrgId : 0;
            compId = (Util.user != null) ? Util.user.CompanyId : 0;

            cat = new TrainingCategoryBL();
            org = new structureImpl();
            cos = new TrainingCourseBL();
            emp = new EmployeeBL();
            train = new TrainingBL();
            perform = new PerformanceMeasureBL();
        }

        #endregion

        #region Page Event Handling Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            ClearLabels();
            if (!IsPostBack)
            {
                LoadFilterForm();
                FillTraining();
            }
        }

        #endregion

        #region Databinding Methods

        void FillTraining()
        {
            try
            {
                vwMain.SetActiveView(vwData);
                ITraining obj = new TrainingBL();
                var data = IsAdministrator ? obj.GetByCompanyId(compId) : obj.GetByOrgUnitId(orgUnitId);

                //search parameters
                var name = string.IsNullOrEmpty(txtName.Text) ? string.Empty : txtName.Text.ToString().Replace(" ", "").ToLower();
                var categoryId = int.Parse(ddCategory.SelectedValue);
                if (ddCategory.SelectedIndex != 0 || !string.IsNullOrEmpty(name))
                {

                    if (ddCategory.SelectedIndex != 0 && !string.IsNullOrEmpty(name))
                    {
                        data = data.Where(c => c.Cat.CategoryId == categoryId && c.Emp.FullName.ToLower().Replace(" ", "").Contains(name)).ToList();
                    }
                    else if (ddCategory.SelectedIndex == 0 && !string.IsNullOrEmpty(name))
                    {
                        data = data.Where(c => c.Emp.FullName.ToLower().Replace(" ", "").Contains(name)).ToList();
                    }
                    else if (ddCategory.SelectedIndex != 0 && string.IsNullOrEmpty(name))
                    {
                        data = data.Where(c => c.Cat.CategoryId == categoryId).ToList();
                    }
                }

                PagerData.Visible = (data.Count > PagerData.PageSize);
                ShowMessage(lblMsg, (data.Count > 0) ? string.Empty : Messages.NoRecordsToDisplay);

                BindControl.BindListView(lvData, data);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void LoadFilterForm()
        {
            txtName.Text = string.Empty;
            LoadCategories(ddCategory, string.Empty);
        }

        void DropDownNothing(DropDownList dd, string selectMessage)
        {
            dd.Items.Clear();
            dd.Items.Add(new ListItem
            {
                Text = selectMessage,
                Value = "-1"
            });
        }

        void LoadCategories(DropDownList dd, string defaultValue)
        {
            if (dd == null) return;
            var data = cat.GetByStatus(compId, true);
            BindControl.BindDropdown(dd, "CategoryName", "CategoryId", "- select all -", "-1", defaultValue, data);
        }

        void LoadTrainingCourses(DropDownList dd, string defaultValue)
        {
            if (dd == null) return;
            if (ddCat.SelectedIndex == 0)
            {
                DropDownNothing(dd, "- please select category first -");
                return;
            }

            var categoryId = int.Parse(ddCat.SelectedValue);
            var data = cos.GetByCategoryId(categoryId, true);
            BindControl.BindDropdown(dd, "TrainingName", "CourseId", defaultValue, data);
        }

        void LoadMeasures(DropDownList dd, string defaultValue)
        {
            if (dd == null) return;
            if (string.IsNullOrEmpty(ddEmployee.SelectedValue))
            {
                DropDownNothing(dd, "- please select employee first -");
                return;
            }

            var areaId = Util.getTypeDefinitionsRolesTypeID();
            var employeeId = int.Parse(ddEmployee.SelectedValue);
            var data = perform.GetByAreaEmployeeId(areaId, employeeId, true);
            BindControl.BindDropdown(dd, "PerformanceMeasureName", "MeasureId", "- select related measure -", "-1", defaultValue, data);
        }

        void LoadEmployees(DropDownList dd, string defaultValue)
        {
            if (dd == null) return;
            if (ddOrgUnit.SelectedIndex == 0)
            {
                DropDownNothing(dd, "- please select organisation unit first -");
                return;
            }

            var managerId = Util.user.UserId;
            var departmentId = int.Parse(ddOrgUnit.SelectedValue);
            var data = emp.GetActive(departmentId).Where(l => l.LineManagerEmployeeId == managerId).OrderBy(f => f.FullName).ToList();
            BindControl.BindDropdown(dd, "FullName", "EmployeeId", "- select employee -", "-1", defaultValue, data);
        }

        void LoadOrgUnit(DropDownList dd, int orgUnitId)
        {
            var data = org.getOrgunitDetails(new Orgunit { ID = orgUnitId });

        }

        void LoadOrgUnits(DropDownList dd, string defaultValue)
        {
            if (dd == null) return;

            int userId = Util.user.UserId;
            int? roleId = Util.user.UserTypeId;

            bool isAdmin = (roleId == Util.getTypeDefinitionsUserTypeAdministrator());

            var comp = new company { COMPANY_ID = compId };
            var data = isAdmin ? org.getActiveCompanyStructure(comp) :
                                 org.getOrgUnitsByLineManager(orgUnitId, true).DefaultView;
            BindControl.BindDropdown(dd, "ORGUNIT_NAME", "ID", "- select org unit -", "-1", defaultValue, data);
        }

        #endregion

        #region Utility Methods

        void ShowMessage(Literal lbl, string msg)
        {
            lbl.Text = msg;
        }

        public string StrConvToUpper(object fullname)
        {
            return fullname.ToString().ToUpper();
        }

        public string GetUrl(object trainingId, object fullName, object startdate)
        {
            string date = Common.GetInternationalDateFormat(DateTime.Parse(startdate.ToString()));
            return string.Format("trainingstateupdate.aspx?Id={0}&Cos={1}&Sdate={2}", trainingId, HttpUtility.HtmlEncode(fullName), date);
        }

        void ClearTextBox()
        {
            txtReason.Text = string.Empty;
        }

        void ClearLabels()
        {
            lblMsg.Text = string.Empty;
            LblMsgForm.Text = string.Empty;
        }

        void FillForm()
        {
            try
            {
                ClearTextBox();
                ClearForm();

                SetAutoPostBackDropDownProperty(true);
                SetEnabledDropDownProperty(true);

                ClearDropDowns();

                LoadCategories(ddCat, string.Empty);
                LoadOrgUnits(ddOrgUnit, string.Empty);

                vwMain.SetActiveView(vwForm);
            }
            catch (Exception e)
            {
                BindControl.BindLiteral(LblMsgForm, Messages.GetErrorMessage());
                Util.LogErrors(e);
            }
        }

        void SaveTraining()
        {
            try
            {
                string tempSDate = txtStartDate.Text;
                string tempEDate = txtEndDate.Text;

                bool trainingIdIsNull = string.IsNullOrEmpty(hdnTrainingId.Value);
                bool employeeNull = (ddEmployee.SelectedValue == "-1" || string.IsNullOrEmpty(ddEmployee.SelectedValue));
                bool courseNull = (ddCourse.SelectedValue == "-1" || string.IsNullOrEmpty(ddCourse.SelectedValue));
                bool measureIsNull = (ddMeasure.SelectedIndex == 0 || string.IsNullOrEmpty(ddMeasure.SelectedValue));

                if (employeeNull || courseNull || string.IsNullOrEmpty(tempSDate) || string.IsNullOrEmpty(tempEDate))
                {
                    BindControl.BindLiteral(LblMsgForm, Messages.GetRequiredFieldsMessage(true, employeeNull ? "Employee" : string.Empty,
                                                                                                courseNull ? "Training Course" : string.Empty,
                                                                                                string.IsNullOrEmpty(tempSDate) ? "Start Date" : string.Empty,
                                                                                                string.IsNullOrEmpty(tempEDate) ? "End Date" : string.Empty));
                    return;
                }

                DateTime startDate = DateTime.Parse(Common.GetInternationalDateFormat(tempSDate, DateFormat.dayMonthYear));
                DateTime endDate = DateTime.Parse(Common.GetInternationalDateFormat(tempEDate, DateFormat.dayMonthYear));

                if (startDate > endDate)
                {
                    BindControl.BindLiteral(LblMsgForm, "Start Date cannot be greater than End Date.");
                    return;
                }

                int? defInt = null;
                int employeeId = int.Parse(ddEmployee.SelectedValue);
                int courseId = int.Parse(ddCourse.SelectedValue);
                int? relateGoalId = measureIsNull ? defInt : int.Parse(ddMeasure.SelectedValue);
                int trainingId = trainingIdIsNull ? 0 : int.Parse(hdnTrainingId.Value);

                string loggedUserId = Util.user.LoginId;
                string reason = txtReason.Text;

                bool updateMode = (trainingId > 0);

                var info = new Training
                {
                    TrainingId = trainingId,
                    CourseId = courseId,
                    EmployeeId = employeeId,
                    RelatedGoalId = relateGoalId,
                    ReasonForTraining = reason,
                    StartDate = startDate,
                    StatusId = defInt,
                    EndDate = endDate,
                    UpdatedBy = loggedUserId,
                    CreatedBy = loggedUserId
                };

                bool saved = !updateMode ? train.Add(info) : train.Update(info);
                if (saved)
                {
                    FillTraining();
                    BindControl.BindLiteral(lblMsg, Messages.GetSaveMessage());
                }
                else
                {
                    StringBuilder strB = new StringBuilder("The reason for not saving could be:<br/><br/><ol style='padding-left:20px;'>");
                    if (!updateMode)
                    {
                        strB.Append("<li>the training for the employee specified already exists or </li>");
                    }

                    strB.Append("<li>start & end date may be overlapping with the existing traing for the employee selected</li>");
                    strB.Append("</ol>");

                    BindControl.BindLiteral(LblMsgForm, saved ? Messages.GetSaveMessage() : string.Format("{0} {1}", Messages.GetSaveFailedMessage(), strB.ToString()));
                }
            }
            catch (Exception e)
            {
                BindControl.BindLiteral(LblMsgForm, Messages.GetErrorMessage());
                Util.LogErrors(e);
            }
        }

        void SetAutoPostBackDropDownProperty(bool autopostback)
        {
            ddCat.AutoPostBack = autopostback;
            ddOrgUnit.AutoPostBack = autopostback;
            ddEmployee.AutoPostBack = autopostback;
        }

        void SetEnabledDropDownProperty(bool enabled)
        {
            ddCat.Enabled = enabled;
            ddOrgUnit.Enabled = enabled;
            ddEmployee.Enabled = enabled;
        }

        void ClearForm()
        {
            txtEndDate.Text = string.Empty;
            txtStartDate.Text = string.Empty;
            txtReason.Text = string.Empty;
        }

        void ClearDropDowns()
        {
            ddCat.Items.Clear();
            ddCourse.Items.Clear();
            ddEmployee.Items.Clear();
            ddMeasure.Items.Clear();
            ddOrgUnit.Items.Clear();
        }

        void LoadStaticEditDropDown(DropDownList dd, string textValue, string idValue)
        {
            if (dd == null) { return; }
            dd.Items.Clear();
            dd.Items.Add(new ListItem
            {
                Selected = true,
                Text = textValue,
                Value = idValue
            });
        }

        void EditEmployeeTraining(long trainingId, string employeeName, string orgUnitName)
        {
            try
            {
                var data = train.GetById(trainingId);
                if (data.TrainingId != 0)
                {
                    string employeeId = data.EmployeeId.ToString();
                    string orgUnitId = data.OrgUnit.OrganisationId.ToString();
                    SetAutoPostBackDropDownProperty(false);

                    ClearTextBox();
                    ClearDropDowns();

                    LoadCategories(ddCat, data.Cat.CategoryId.ToString());
                    LoadTrainingCourses(ddCourse, data.CourseId.ToString());
                    LoadStaticEditDropDown(ddOrgUnit, orgUnitName, orgUnitId);
                    LoadStaticEditDropDown(ddEmployee,employeeName, employeeId);
                    LoadMeasures(ddMeasure, data.RelatedGoalId.HasValue ? data.RelatedGoalId.ToString() : string.Empty);

                    txtReason.Text = data.ReasonForTraining;
                    txtStartDate.Text = Common.GetCustomDateFormat(data.StartDate, "/");
                    txtEndDate.Text = Common.GetCustomDateFormat(data.EndDate.Date, "/");
                    SetEnabledDropDownProperty(false);
                    vwMain.SetActiveView(vwForm);
                }
            }
            catch (Exception e)
            {
                hdnTrainingId.Value = string.Empty;
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(e);
            }
        }

        #endregion

        #region ListView Event Handling Methods

        protected void lvData_PagePropertiesChanged(object sender, EventArgs e)
        {
            FillTraining();
        }

        protected void lvData_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (string.Compare(e.CommandName, "Edit", true) == 0)
                {
                    var hdnId = e.Item.FindControl("hdnId") as HiddenField;
                    var lblEmployeeName = e.Item.FindControl("lblEmployeeName") as Literal;
                    var lblOrgUnitName = e.Item.FindControl("lblOrgUnitName") as Literal;

                    string employeeName = lblEmployeeName.Text;
                    string orgUnitName = lblOrgUnitName.Text;
                    long trainingId = long.Parse(hdnId.Value);

                    hdnTrainingId.Value = hdnId.Value;
                    EditEmployeeTraining(trainingId, employeeName, orgUnitName);
                }
            }
            catch (Exception ex)
            {
                hdnTrainingId.Value = string.Empty;
                BindControl.BindLiteral(LblMsgForm, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        protected void lvData_ItemEditing(object sender, ListViewEditEventArgs e)
        {

        }

        #endregion

        #region Button Event Handling Methods

        protected void btnNew_Click(object sender, EventArgs e)
        {
            FillForm();
        }

        protected void btnFilter_Click(object sender, EventArgs e)
        {
            FillTraining();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            SaveTraining();
        }

        protected void btnCancelComplete_Click(object sender, EventArgs e)
        {
            vwMain.SetActiveView(vwData);
        }

        #endregion

        #region Dropdown Event Handling Methods

        protected void ddCat_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadTrainingCourses(ddCourse, string.Empty);
        }

        protected void ddOrgUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadEmployees(ddEmployee, string.Empty);
        }

        protected void ddEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadMeasures(ddMeasure, string.Empty);
        }

        #endregion

    }
}