﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.implementations;
using HR.EmployeeTraining.Logic;
using HR.EmployeeTraining.Interfaces;
using HR.EmployeeTraining.Entities;
using System.Data.SqlTypes;
using System.IO;

namespace scorecard.ui.controls.training
{
    public partial class trainingstatusdetail : System.Web.UI.UserControl
    {
        #region Variables & Properties

        ITrainingHistory hist;
        ITrainingStatus stat;

        private int TrainingId
        {
            get
            {
                return int.Parse(Request.QueryString["Id"].ToString());
            }
        }

        private string TrainingCourseName
        {
            get
            {
                return HttpUtility.UrlDecode(Request.QueryString["Cos"].ToString());
            }
        }

        private DateTime StartDate
        {
            get
            {
                return DateTime.Parse(Request.QueryString["Sdate"].ToString());
            }
        }

        #endregion

        #region Default Constructors

        public trainingstatusdetail()
        {
            hist = new TrainingHistoryBL();
            stat = new TrainingStatusBL();
        }

        #endregion

        #region Page Event Handling Methods

        protected void Page_Init(object sender, EventArgs e)
        {
            CheckSessionState();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ClearLabels();
            if (!IsPostBack)
            {
                FillHistory(TrainingId);
            }
        }

        #endregion

        #region Databinding Methods

        private void CheckSessionState()
        {
            if (Util.user == null)
            {
                BindControl.BindLiteral(lblMsg, Messages.SessionExpired);
                AjaxScriptManager.RegisterStartupScript(Page, typeof(Page), "closeWin", "closeHighSlideWindowAndRefresh();", true);
            }
        }

        void ClearTextBoxes()
        {
            txtComment.Text = string.Empty;
            txtDateAchieved.Text = string.Empty;
            txtReason.Text = string.Empty;
        }

        void ClearLabels()
        {
            lblDelMsg.Text = string.Empty;
            lblFormMsg.Text = string.Empty;
            lblMsg.Text = string.Empty;
        }

        public string GetFileUrl(object fileName)
        {
            string nolink = "#";
            if (fileName == DBNull.Value) return nolink;
            if (string.IsNullOrEmpty(fileName.ToString())) return nolink;
            var companyId = Util.user.CompanyId;
            return Util.GetTrainingVirtualPath(companyId, fileName.ToString());
        }

        void FillHistory(int trainingId)
        {
            mainView.SetActiveView(vwData);

            var data = hist.GetByStatus(trainingId, true);

            BindControl.BindListView(lvData, data);
            BindControl.BindLiteral(lblMsg, data.Count > 0 ? string.Empty : Messages.NoRecordsToDisplay);
        }

        void BindTrainingStatus(DropDownList ddwn)
        {
            int companyId = (Util.user != null) ? Util.user.CompanyId : 0;
            var data = stat.GetByStatus(companyId, true);
            BindControl.BindDropdown(ddwn, data);
        }

        void DeleteTrainingStatus()
        {
            try
            {
                if (!string.IsNullOrEmpty(txtReason.Text))
                {
                    bool deactivate = false;

                    int trainingId = int.Parse(hdnTrainingId.Value);

                    string loggedUser = Util.user.LoginId;
                    string reason = txtReason.Text;

                    Guid trailId = new Guid(hdnTrailId.Value);

                    var obj = new TrainingHistory
                    {
                        TrailId = trailId,
                        TrainingId = trainingId,
                        ReasonForDeletion = reason,
                        ValidDocument = deactivate,
                        UpdatedBy = loggedUser
                    };

                    bool deleted = hist.SetValidStatus(obj);
                    if (deleted)
                    {
                        FillHistory(trainingId);
                    }
                    BindControl.BindLiteral(lblMsg, deleted ? Messages.GetSaveMessage() : Messages.GetSaveFailedMessage());
                }
                else
                {
                    BindControl.BindLiteral(lblDelMsg, "The reason for deletion is required.");
                }
            }
            catch (Exception e)
            {
                BindControl.BindLiteral(lblDelMsg, Messages.GetErrorMessage());
                Util.LogErrors(e);
            }
        }

        void UpdateStatus()
        {
            try
            {
                if (FileUpload1.HasFile)
                {
                    int trainingId = TrainingId;
                    int statusId = int.Parse(ddStatus.SelectedValue);
                    int companyId = Util.user.CompanyId;
                    int fileId = new Random().Next();

                    string loggedUser = Util.user.LoginId;
                    string comment = txtComment.Text;

                    DateTime dateAchieved = DateTime.Parse(Common.GetInternationalDateFormat(txtDateAchieved.Text, DateFormat.dayMonthYear));
                    bool isFutureDate = Common.IsFutureDate(dateAchieved);

                    if (dateAchieved < StartDate || isFutureDate)
                    {
                        BindControl.BindLiteral(lblFormMsg, !isFutureDate ? "Date Achieved cannot be less than the Start Date of the training." :
                                                                            "Date Achieved cannot be a Future Date.");
                        return;
                    }

                    long fileSize = FileUpload1.FileContent.Length;

                    string newFileName;
                    string filePath = FileUpload1.PostedFile.FileName;
                    string fileName = FileUpload1.FileName;
                    string extension = Path.GetExtension(fileName);
                    string mimetype = Common.GetMimeType(extension);

                    bool uploaded = UploadFile(FileUpload1, companyId, fileId, fileName, out newFileName);
                    if (uploaded)
                    {
                        var obj = new TrainingHistory
                        {
                            Comments = comment,
                            CreatedBy = loggedUser,
                            DateAchieved = dateAchieved,
                            StatusId = statusId,
                            TrainingId = trainingId,
                            ValidDocument = true,
                            FileExtension = extension,
                            DocumentName = newFileName,
                            FileSize = fileSize,
                            MimeType = mimetype
                        };

                        bool saved = hist.AddProgressStatus(obj);
                        if (saved)
                        {
                            FillHistory(trainingId);
                        }
                        BindControl.BindLiteral(lblMsg, saved ? Messages.GetSaveMessage() : Messages.GetSaveFailedMessage());
                    }
                    else
                    {
                        BindControl.BindLiteral(lblFormMsg, "The system failed to upload your document.");
                    }
                }
                else
                {
                    BindControl.BindLiteral(lblFormMsg, "It is mandatory to attach proof of training status update.");
                }
            }
            catch (Exception e)
            {
                BindControl.BindLiteral(lblFormMsg, Messages.GetErrorMessage());
                Util.LogErrors(e);
            }
        }

        bool UploadFile(FileUpload radUpload, int companyId, int fileId, string name, out string trainingFile)
        {
            trainingFile = string.Empty;
            if (radUpload.HasFile)
            {
                string physicalfolder = Util.GetTrainingPhysicalPath(companyId);
                try
                {
                    if (!Directory.Exists(physicalfolder))
                    {
                        Directory.CreateDirectory(physicalfolder);
                    }
                }
                catch (Exception ex)
                {
                    trainingFile = string.Empty;
                    BindControl.BindLiteral(lblFormMsg, "The system failed to locate the upload folder");
                    Util.LogErrors(ex);
                    return false;
                }

                string fileExtension = Path.GetExtension(radUpload.PostedFile.FileName);
                string newFileName = string.Format("{0}{1}{2}", name.Replace(" ", ""), fileId, fileExtension);
                string fullPathNewFileName = string.Format(@"{0}\{1}", physicalfolder, newFileName);

                radUpload.PostedFile.SaveAs(fullPathNewFileName);
                trainingFile = newFileName;
            }

            return string.IsNullOrEmpty(trainingFile) ? false : true;
        }

        #endregion

        #region ListView Event Handling Methods

        protected void lvData_ItemDeleting(object sender, ListViewDeleteEventArgs e)
        {
            try
            {
                hdnTrailId.Value = (lvData.Items[e.ItemIndex].FindControl("hdnTrailId") as HiddenField).Value;
                hdnTrainingId.Value = (lvData.Items[e.ItemIndex].FindControl("hdnTrainingId") as HiddenField).Value;
                mainView.SetActiveView(vwDelete);
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        protected void lvData_ItemCommand(object sender, ListViewCommandEventArgs e)
        {

        }

        #endregion

        #region Button Event Handling Methods

        protected void btnNew_Click(object sender, EventArgs e)
        {
            mainView.SetActiveView(vwFormStatusUpdate);
            ClearTextBoxes();

            BindControl.BindLiteral(lblStartDate, Common.GetDescriptiveDate(StartDate, false));
            BindControl.BindLiteral(lblTrainingName, TrainingCourseName);
            BindTrainingStatus(ddStatus);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            UpdateStatus();
        }

        protected void btnCancelDeletion_Click(object sender, EventArgs e)
        {
            FillHistory(TrainingId);
        }

        protected void btnCancelComplete_Click(object sender, EventArgs e)
        {
            FillHistory(TrainingId);
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteTrainingStatus();
        }

        #endregion
    }
}