﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="trainingstatusupdate.ascx.cs"
    Inherits="scorecard.ui.controls.training.trainingstatusupdate" %>
<%@ Import Namespace="scorecard.implementations" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<h1>
    Update Training Status</h1>
<asp:UpdatePanel ID="UpdatePanelUser" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div runat="server" id="searchPanel">
            <fieldset>
                <legend>Search</legend>
                <table cellpadding="4" cellspacing="4" width="65%">
                    <tr>
                        <td>
                            Category
                        </td>
                        <td>
                            <asp:DropDownList ID="ddCategory" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td>
                            Training Course
                        </td>
                        <td>
                            <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Button ID="btnFilter" runat="server" class="button" Text="Search" OnClick="btnFilter_Click" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </div>
        <div>
            <asp:ListView ID="lvData" runat="server" OnPagePropertiesChanged="lvData_PagePropertiesChanged">
                <LayoutTemplate>
                    <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                        <thead>
                            <tr>
                                <td style="width: 320px">
                                    Full Name
                                </td>
                                <td style="width: 220px">
                                    Training Category
                                </td>
                                <td style="width: 350px">
                                    Training Course
                                </td>
                                <td style="width: 100px">
                                    Start Date
                                </td>
                                <td style="width: 100px">
                                    End Date
                                </td>
                                <td style="width: 350px">
                                    Related Performance Measure
                                </td>
                                <td style="width: 250px">
                                    Organisation Unit
                                </td>
                                <td style="width: 200px">
                                    Status
                                </td>
                                <td align="center" style="width: 200px">
                                    Update Status
                                </td>
                            </tr>
                            <tr id="ItemPlaceHolder" runat="server">
                            </tr>
                        </thead>
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <tbody>
                        <tr class="odd">
                            <td>
                                <%# Eval("Emp.FullName")%>
                                <asp:HiddenField ID="hdnId" runat="server" Value='<%# Eval("TrainingId")%>' />
                            </td>
                            <td>
                                <%# Eval("Cat.CategoryName")%>
                            </td>
                            <td>
                                <%# Eval("Course.TrainingName")%>
                            </td>
                            <td>
                                <%# Common.GetDescriptiveDate(Eval("StartDate")) %>
                            </td>
                            <td>
                                <%# Common.GetDescriptiveDate(Eval("EndDate")) %>
                            </td>
                            <td>
                                <%# Eval("Measure.PerformanceMeasureName")%>
                            </td>
                            <td>
                                <%# Eval("OrgUnit.OrganisationName")%>
                            </td>
                            <td>
                                <%# Eval("Status.StatusName")%>
                            </td>
                            <td align="center">
                                <div style="width: 100%; text-align: center">
                                    <a href='<%# GetUrl(Eval("TrainingId"), Eval("Course.TrainingName"), Eval("StartDate"))  %>'
                                        onclick="return hs.htmlExpand(this, { objectType: 'iframe', width: 900, height: 500, creditsPosition: 'bottom left', headingText: '<%# StrConvToUpper(Eval("Emp.FullName")) %> - <%# StrConvToUpper(Eval("Course.TrainingName")) %> TRAINING', wrapperClassName: 'titlebar' } )">
                                        <span class="links">Update Status</span></a>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </ItemTemplate>
                <EmptyItemTemplate>
                    <p class="errorMsg">
                        There are no records to display.</p>
                </EmptyItemTemplate>
            </asp:ListView>
            <br />
            <asp:DataPager ID="PagerData" runat="server" PagedControlID="lvData" PageSize="10">
                <Fields>
                    <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True" ShowNextPageButton="False"
                        ShowPreviousPageButton="False" />
                    <asp:NumericPagerField />
                    <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True" ShowNextPageButton="False"
                        ShowPreviousPageButton="False" />
                </Fields>
            </asp:DataPager>
            <br />
            <br />
            <div>
                <p class="errorMsg">
                    <br />
                    <asp:Literal ID="lblMsg" runat="server"></asp:Literal>
                    <br />
                    <br />
                </p>
            </div>
            <div id="tabs-3">
                <p>
                    <asp:Button ID="lnkBack" runat="server" CssClass="button" Text="Done" CausesValidation="false"
                        PostBackUrl="~/ui/index.aspx" />
                </p>
            </div>
        </div>
        <asp:UpdateProgress ID="UpdateProgressUser" runat="server" AssociatedUpdatePanelID="UpdatePanelUser">
            <ProgressTemplate>
                <div>
                    <asp:Image ID="imgOrg" runat="server" ImageUrl="~/ui/images/icons/activity.gif" AlternateText="Please wait..">
                    </asp:Image>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
