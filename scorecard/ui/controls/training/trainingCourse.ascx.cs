﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HR.EmployeeTraining.Interfaces;
using Education.Accreditation;
using scorecard.implementations;
using HR.EmployeeTraining.Logic;
using HR.EmployeeTraining.Entities;

namespace scorecard.ui.controls.training
{
    public partial class trainingCourse : System.Web.UI.UserControl
    {
        ITrainingCourse cos;
        ITrainingCategory cat;
        INQFLevel nqf;

        readonly int orgUnitId;
        readonly int compId;

        public trainingCourse()
        {
            orgUnitId = (Util.user != null) ? Util.user.OrgId : 0;
            compId = (Util.user != null) ? Util.user.CompanyId : 0;

            cos = new TrainingCourseBL();
            cat = new TrainingCategoryBL();
            nqf = new NQFLevelBL();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ResetLabels();
            if (!IsPostBack)
            {
                LoadFilterForm();
                LoadTrainingCourses();
            }
        }

        void ResetLabels()
        {
            lblMsg.Text = "";
            LblError.Text = "";
        }

        void LoadFilterForm()
        {
            LoadCategories(ddCategory, string.Empty);
            ddCategory.Items.Insert(0, new ListItem
            {
                Text = "- select all -",
                Value = "-1"
            });
        }

        void LoadCategories(DropDownList dd, string defaultValue)
        {
            if (dd == null) return;
            var data = cat.GetByStatus(compId, true);
            BindControl.BindDropdown(dd, "CategoryName", "CategoryId", defaultValue, data);
        }

        void LoadTrainingCourses()
        {
            try
            {
                int? categoryId = null;

                if (ddCategory.SelectedIndex != 0)
                {
                    categoryId = int.Parse(ddCategory.SelectedValue);
                }

                mainView.SetActiveView(vwData);
                var data = cos.GetByStatus(compId, true);
                if (categoryId != null)
                {
                    data = data.Where(c => c.CategoryId == categoryId).ToList();
                }

                dtPagerlvData.Visible = (data.Count > dtPagerlvData.PageSize);
                BindControl.BindListView(lvData, data);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void LoadNqfLevels(DropDownList dd, string defaultValue)
        {
            if (dd == null) return;

            var countryId = Util.user.CountryId;
            var data = nqf.GetByCountryId(countryId, true);
            BindControl.BindDropdown(dd, "NqfLevelName", "NqfLevelId", "- select NQF Level -", "-1", defaultValue, data);
        }

        void ClearAllLabels()
        {
            LblError.Text = string.Empty;
            lblMsg.Text = string.Empty;
            LblMsgForm.Text = string.Empty;
        }

        void ClearForm()
        {
            txtDescription.Text = string.Empty;
            txtTrainingName.Text = string.Empty;
            hdnCourseId.Value = string.Empty;
        }

        void LoadForm()
        {
            try
            {
                mainView.SetActiveView(vwForm);

                ClearAllLabels();
                ClearForm();

                chkExamReq.Checked = false;
                chkActive.Checked = true;
                LoadCategories(ddCat, string.Empty);
                LoadNqfLevels(ddNqf, string.Empty);
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(LblError, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        void SaveTrainingCourse(bool update)
        {
            try
            {
                int courseId = update ? int.Parse(hdnCourseId.Value) : 0;
                int categoryId = int.Parse(ddCat.SelectedValue);
                int? nqflevelId = null;

                if (ddNqf.SelectedIndex != 0)
                {
                    nqflevelId = int.Parse(ddNqf.SelectedValue);
                }

                bool examrequired = chkExamReq.Checked;
                bool active = chkActive.Checked;

                string createdBy = Util.user.LoginId;
                string trainingName = txtTrainingName.Text;
                string description = txtDescription.Text;

                if (string.IsNullOrEmpty(txtTrainingName.Text))
                {
                    BindControl.BindLiteral(LblMsgForm, Messages.GetRequiredFieldsMessage(true, "Training name"));
                    return;
                }

                var obj = new TrainingCourse
                {
                    Active = active,
                    CategoryId = categoryId,
                    CompanyId = compId,
                    CourseId = courseId,
                    CreatedBy = createdBy,
                    ExamRequired = examrequired,
                    NqfLevelId = nqflevelId,
                    TrainingDescription = description,
                    TrainingName = trainingName,
                    UpdatedBy = createdBy
                };

                bool added = update ? cos.Update(obj) : cos.Add(obj);
                if (added)
                {
                    LoadTrainingCourses();
                }


                BindControl.BindLiteral(LblMsgForm, added ? Messages.GetSaveMessage() : Messages.GetAlreadyExistMessage());
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(LblMsgForm, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        void EditTrainingCourse(string dataKeyId)
        {
            try
            {
                if (string.IsNullOrEmpty(dataKeyId))
                {
                    BindControl.BindLiteral(LblError, Messages.GetRecordNotFound);
                    return;
                }

                int courseId = int.Parse(dataKeyId);
                var data = cos.GetById(courseId);

                if (data.CourseId != 0)
                {
                    LoadForm();

                    hdnCourseId.Value = data.CourseId.ToString();

                    chkActive.Checked = data.Active;
                    chkExamReq.Checked = data.ExamRequired;
                    txtDescription.Text = data.TrainingDescription;
                    txtTrainingName.Text = data.TrainingName;

                    if (ddCat.Items.FindByValue(data.CategoryId.ToString()) != null)
                    {
                        ddCat.SelectedValue = data.CategoryId.ToString();
                    }

                    if (data.NqfLevelId != null && ddNqf.Items.FindByValue(data.NqfLevelId.ToString()) != null)
                    {
                        ddNqf.SelectedValue = data.NqfLevelId.ToString();
                    }
                }
                else
                {
                    BindControl.BindLiteral(LblError, Messages.GetRecordNotFound);
                }
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(LblError, Messages.GetErrorMessage());
                mainView.SetActiveView(vwData);
                Util.LogErrors(ex);
            }
        }

        void RemoveCourse(ListViewDataItem e)
        {
            try
            {
                var hdnCourse = e.FindControl("hdnCourseId") as HiddenField;
                if (string.IsNullOrEmpty(hdnCourse.Value))
                {
                    BindControl.BindLiteral(LblError, Messages.GetDeleteFailedMessage());
                    return;
                }

                int courseId = int.Parse(hdnCourse.Value);
                string loggedUser = Util.user.LoginId;
                bool deactivated = cos.SetStatus(new TrainingCourse
                {
                    Active = false,
                    CourseId = courseId,
                    UpdatedBy = loggedUser
                });

                if (deactivated)
                {
                    LoadTrainingCourses();
                }
                BindControl.BindLiteral(LblError, deactivated ? Messages.GetDeleteMessage() : Messages.GetDeleteFailedMessage());
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(LblError, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        protected void lvData_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            var courseId = lvData.Items[e.NewEditIndex].FindControl("hdnCourseId") as HiddenField;
            EditTrainingCourse(courseId.Value);
        }

        protected void lvData_ItemDeleting(object sender, ListViewDeleteEventArgs e)
        {
            ListViewDataItem el = lvData.Items[e.ItemIndex];
            RemoveCourse(el);
        }

        protected void lvData_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            //try
            //{
            //    if (e.CommandName.ToLower().Equals("edit"))
            //    {
            //        var courseId = e.Item.FindControl("hdnCourseId") as HiddenField;
            //        EditTrainingCourse(courseId.Value);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    BindControl.BindLiteral(LblError, Messages.GetErrorMessage());
            //    Util.LogErrors(ex);
            //}
        }

        protected void lvData_PagePropertiesChanged(object sender, EventArgs e)
        {
            LoadTrainingCourses();
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            LoadForm();
        }

        protected void btnCancelComplete_Click(object sender, EventArgs e)
        {
            mainView.SetActiveView(vwData);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            bool update = string.IsNullOrEmpty(hdnCourseId.Value) ? false : true;
            SaveTrainingCourse(update);
        }

        protected void btnFilter_Click(object sender, EventArgs e)
        {
            LoadTrainingCourses();
        }


    }
}