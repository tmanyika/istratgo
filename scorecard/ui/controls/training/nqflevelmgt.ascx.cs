﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.implementations;
using HR.EmployeeTraining.Interfaces;
using HR.Incentives.Logic;
using HR.EmployeeTraining.Entities;
using Education.Accreditation;
using scorecard.controllers;
using scorecard.interfaces;

namespace scorecard.ui.controls.education
{
    public partial class nqflevelmgt : System.Web.UI.UserControl
    {
        INQFLevel edu;
        Isettings settings;

        public nqflevelmgt()
        {
            edu = new NQFLevelBL();
            settings = new applicationsettingsImpl();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ResetLabels();
            if (!IsPostBack)
            {
                LoadSearchForm();
                LoadNqlLevels();
            }
        }

        private void LoadSearchForm()
        {
            LoadCountries(ddCountryS, string.Empty);
            ddCountryS.Items.Insert(0, new ListItem
            {
                Selected = true,
                Text = "- select all countries -",
                Value = "-1"
            });
        }

        private void ResetLabels()
        {
            lblMsg.Text = "";
            LblError.Text = "";
        }

        void LoadNqlLevels()
        {
            try
            {
                var countryId = int.Parse(ddCountryS.SelectedValue);
                var data = (ddCountryS.SelectedIndex == 0) ? edu.GetByStatus(true) :
                                                            edu.GetByCountryId(countryId, true);
                dtPagerlvData.Visible = (data.Count > dtPagerlvData.PageSize);
                BindControl.BindListView(lvData, data);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void LoadCountries(DropDownList dd, string defaultValue)
        {
            var parent = new entities.applicationsettings
            {
                PARENT_ID = Util.getTypeDefinitionsCountryTypeParentID()
            };

            var data = settings.getChildSettings(parent);
            BindControl.BindDropdown(dd, data, defaultValue);
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
                lvData.InsertItemPosition = InsertItemPosition.LastItem;
                LoadNqlLevels();

                var dd = lvData.InsertItem.FindControl("ddCountry") as DropDownList;
                LoadCountries(dd, string.Empty);
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(LblError, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        void AddNewNqfLevel(ListViewCommandEventArgs e)
        {
            try
            {
                string description = (e.Item.FindControl("txtDescription") as TextBox).Text;
                string name = (e.Item.FindControl("txtName") as TextBox).Text;
                string code = (e.Item.FindControl("txtCode") as TextBox).Text;
                string username = Util.user.LoginId;

                int countryId = int.Parse((e.Item.FindControl("ddCountry") as DropDownList).SelectedValue);

                if (string.IsNullOrEmpty(name))
                {
                    BindControl.BindLiteral(LblError, Messages.GetRequiredFieldsMessage(true, "Name"));
                    return;
                }

                bool added = edu.Add(new NQFLevel
                {
                    Active = true,
                    CountryId = countryId,
                    NqfLevelCode = code,
                    NqfLevelName = name,
                    NqfLevelDescription = description,
                    CreatedBy = Util.user.LoginId
                });

                if (added)
                {
                    loadReadOnly();
                    LoadNqlLevels();
                }
                BindControl.BindLiteral(LblError, added ? Messages.GetSaveMessage() : Messages.GetAlreadyExistMessage());
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void loadReadOnly()
        {
            lvData.EditIndex = -1;
            lvData.InsertItemPosition = InsertItemPosition.None;
        }

        void UpdateNqfLevel(ListViewDataItem e)
        {
            try
            {
                var hdnNqfLevelId = e.FindControl("hdnNqfLevelId") as HiddenField;
                var chkActive = e.FindControl("chkActive") as CheckBox;

                string description = (e.FindControl("txtEditDescription") as TextBox).Text;
                string name = (e.FindControl("txtEditName") as TextBox).Text;
                string code = (e.FindControl("txtEditCode") as TextBox).Text;

                bool active = chkActive.Checked;

                int nqfLevelId = int.Parse(hdnNqfLevelId.Value);
                int countryId = int.Parse((e.FindControl("ddCountryE") as DropDownList).SelectedValue);

                if (string.IsNullOrEmpty(name))
                {
                    BindControl.BindLiteral(LblError, Messages.GetRequiredFieldsMessage(true, "Name"));
                    return;
                }

                bool updated = edu.Update(new NQFLevel
                {
                    Active = true,
                    CountryId = countryId,
                    NqfLevelCode = code,
                    NqfLevelName = name,
                    NqfLevelDescription = description,
                    NqfLevelId = nqfLevelId,
                    UpdatedBy = Util.user.LoginId
                });

                if (updated)
                {
                    loadReadOnly();
                    LoadNqlLevels();
                }
                BindControl.BindLiteral(LblError, updated ? Messages.GetUpdateMessage() : Messages.GetAlreadyExistMessage());
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void RemoveNqfLevel(ListViewDataItem e)
        {
            try
            {
                var hdnNqfLevelId = e.FindControl("hdnNqfLevelId") as HiddenField;
                if (string.IsNullOrEmpty(hdnNqfLevelId.Value))
                {
                    BindControl.BindLiteral(LblError, Messages.GetDeleteFailedMessage());
                    return;
                }

                int nqfLevelId = int.Parse(hdnNqfLevelId.Value);
                bool deactivated = edu.SetStatus(new NQFLevel
                {
                    Active = false,
                    NqfLevelId = nqfLevelId,
                    UpdatedBy = Util.user.LoginId
                });

                if (deactivated)
                {
                    loadReadOnly();
                    LoadNqlLevels();
                }
                BindControl.BindLiteral(LblError, deactivated ? Messages.GetDeleteMessage() : Messages.GetDeleteFailedMessage());
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void CancelUpdate()
        {
            loadReadOnly();
            LoadNqlLevels();
        }

        protected void lvData_ItemCanceling(object sender, ListViewCancelEventArgs e)
        {
            CancelUpdate();
        }

        protected void lvData_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            LblError.Text = "";
            if (e.Item.ItemType == ListViewItemType.InsertItem ||
                e.Item.ItemType == ListViewItemType.DataItem)
            {
                switch (e.CommandName)
                {
                    case "add":
                        AddNewNqfLevel(e);
                        break;
                }
            }
        }

        protected void lvData_ItemDeleting(object sender, ListViewDeleteEventArgs e)
        {
            ListViewDataItem el = lvData.Items[e.ItemIndex];
            RemoveNqfLevel(el);
        }

        protected void lvData_ItemUpdating(object sender, ListViewUpdateEventArgs e)
        {
            ListViewDataItem el = lvData.Items[e.ItemIndex];
            UpdateNqfLevel(el);
        }

        protected void lvData_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            lvData.EditIndex = e.NewEditIndex;
            lvData.InsertItemPosition = InsertItemPosition.None;
            LoadNqlLevels();

            var dd = lvData.Items[e.NewEditIndex].FindControl("ddCountryE") as DropDownList;
            var defValue = (lvData.Items[e.NewEditIndex].FindControl("hdnCountryId") as HiddenField).Value;
            LoadCountries(dd, defValue);

        }

        protected void lvData_PagePropertiesChanged(object sender, EventArgs e)
        {
            LoadNqlLevels();
        }

        protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadNqlLevels();
        }

        protected void lnkCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("index.aspx", true);
        }

        protected void btnFilter_Click(object sender, EventArgs e)
        {
            LoadNqlLevels();
        }
    }
}