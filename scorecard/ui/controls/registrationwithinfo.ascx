﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="registrationwithinfo.ascx.cs"
    Inherits="scorecard.ui.controls.registrationwithinfo" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="CustomCaptcha" Namespace="CustomCaptcha" TagPrefix="cc1" %>
<%@ Register Assembly="CheckboxValidator" Namespace="TensaiLabs.Web.Controls" TagPrefix="cc2" %>
<div class="regformheadings">
    <h2>
        iStratgo - All in one Performance Management and Appraisal Solution
    </h2>
    <h2 class="Black">
        More than just software
    </h2>
    <h2 class="Orange">
        Register and start your 30 Day Free Trial Today!
    </h2>
</div>
<asp:MultiView ID="mainView" runat="server" ActiveViewIndex="0">
    <asp:View ID="vwForm" runat="server">
        <div>
            <!-- Fieldset -->
            <fieldset runat="server" id="pnlContactInfo">
                <legend>Customer Information</legend>
                <p>
                    <label for="mf">
                        First Name:
                    </label>
                    <input class="mf" name="mf" type="text" runat="server" id="txtFirstName" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtFirstName"
                        Display="None" ErrorMessage="First Name"></asp:RequiredFieldValidator>
                </p>
                <p>
                    <label for="lf">
                        Last Name :
                    </label>
                    <input class="mf" name="lf1" type="text" runat="server" id="txtLastName" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtLastName"
                        Display="None" ErrorMessage="Last Name"></asp:RequiredFieldValidator>
                </p>
                <p>
                    <label for="lf">
                        Email :
                    </label>
                    <input class="mf" name="lf1" type="text" runat="server" id="txtEmailAddress" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtEmailAddress"
                        Display="None" ErrorMessage="Email Address"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmailAddress"
                        Display="None" ErrorMessage="Valid Email Address" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                </p>
                <p>
                    <label for="lf">
                        Company Name :
                    </label>
                    <input class="mf" name="lf1" type="text" runat="server" id="txtCompany" />
                    <asp:RequiredFieldValidator ID="rqdCompany" runat="server" ControlToValidate="txtCompany"
                        Display="None" ErrorMessage="Company Name"> </asp:RequiredFieldValidator>
                </p>
                <p>
                    <label for="dropdown">
                        Country :
                    </label>
                    <asp:DropDownList name="dropdown" class="dropdown" runat="server" ID="ddCountry" Width="250"
                        DataTextField="NAME" DataValueField="TYPE_ID" />
                </p>
                <p>
                    <div class="floatLeft">
                        <label for="dropdown">
                            Prove you are not a robot :
                        </label>
                    </div>
                    <div class="floatLeft">
                        <telerik:RadCaptcha ID="RegRadCaptcha" runat="server" ErrorMessage="The code you entered is not valid."
                            CaptchaLinkButtonText="Generate new code" CaptchaTextBoxLabel="" Display="Dynamic"
                            EnableRefreshImage="True" ForeColor="Red" Width="250px">
                        </telerik:RadCaptcha>
                    </div>
                    <div class="clr">
                        &nbsp;
                    </div>
                    <p>
                    </p>
                    <p>
                        <label for="chkTerms">
                        I agree to the
                        <a href="http://www.imdconsulting.co.za/terms/iStratgoBalancedScorecardTermsandConditions.pdf" 
                            target="_blank">Terms and Conditions</a>
                        </label>
                        <asp:CheckBox ID="chkTerms" runat="server" Text="" TextAlign="Right" />
                        <cc2:CheckBoxValidator ID="vldChk" runat="server" ControlToValidate="chkTerms" 
                            Display="None" ErrorMessage="Agree to Terms &amp; Conditions">
                    </cc2:CheckBoxValidator>
                    </p>
                    <p class="errorMsg">
                        <label for="lblError">
                        &nbsp;
                        </label>
                        <br />
                        <asp:Literal ID="lblError" runat="server"></asp:Literal>
                        <br />
                    </p>
                    <p>
                        <br />
                    </p>
                    <p>
                        <label>
                        &nbsp;
                        </label>
                        <asp:Button ID="btnNext" runat="server" class="button" 
                            OnClick="btnComplete_Click" Text="Submit" />
                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" 
                            class="button" PostBackUrl="~/ui/login.aspx" Text="Cancel" />
                    </p>
                    <p>
                        &nbsp;<asp:ValidationSummary ID="vdSumarry" runat="server" 
                            HeaderText="Please provide the following fields:" ShowMessageBox="True" 
                            ShowSummary="False" />
                    </p>
                </p>
            </fieldset>
        </div>
    </asp:View>
    <asp:View ID="vwMessage" runat="server">
        <div class="message information">
            <h2>
                Information
            </h2>
            <p class="linespacing">
                Your profile was created successfully. An email has been sent to
                <b>
                    <asp:Literal ID="lblEmail" runat="server"></asp:Literal></b> with a link to
                activate your profile.
            </p>
        </div>
    </asp:View>
</asp:MultiView>
