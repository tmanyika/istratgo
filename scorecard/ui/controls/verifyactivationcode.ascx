﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="verifyactivationcode.ascx.cs"
    Inherits="scorecard.ui.controls.verifyactivationcode" %>
<div>
    <asp:MultiView ID="mainView" runat="server">
        <asp:View ID="vwSuccess" runat="server">
            <div class="message information">
                <h2>
                    Information
                </h2>
                <p>
                    Your account was activated successfully. Please click <asp:HyperLink ID="lnkLogin" runat="server" ForeColor="Blue" NavigateUrl="~/ui/login.aspx">Here</asp:HyperLink> to login.
                </p>
            </div>
        </asp:View>
        <asp:View ID="vwFail" runat="server">
            <p class="errorMsg">
                You account account could not activated. The activation code supplied is invalid.</p>
        </asp:View>
         <asp:View ID="vwAlreadyActive" runat="server">
            <p class="errorMsg">
                You account account is already activated. Please click <asp:HyperLink ID="HyperLink1" runat="server" ForeColor="Blue" NavigateUrl="~/ui/login.aspx">Here</asp:HyperLink> to login.</p>
        </asp:View>
    </asp:MultiView>
</div>
