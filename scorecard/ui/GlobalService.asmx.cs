﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using HR.Human.Resources;
using HR.EmployeeTraining.Interfaces;
using HR.EmployeeTraining.Logic;
using System.Collections.Specialized;
using AjaxControlToolkit;

namespace scorecard.ui
{
    /// <summary>
    /// Summary description for GlobalService
    /// </summary>
    [WebService(Namespace = "http://scorecard.istratgo.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class GlobalService : System.Web.Services.WebService
    {
        readonly IEmployee emp;
        readonly ITrainingCourse cos;

        public GlobalService()
        {
            emp = new EmployeeBL();
            cos = new TrainingCourseBL();
        }

        [WebMethod]
        public CascadingDropDownNameValue[] GetEmployeesForOrgUnit(string knownCategoryValues, string category)
        {
            int orgUnitId;

            List<CascadingDropDownNameValue> empList = new List<CascadingDropDownNameValue>();
            StringDictionary kv = CascadingDropDown.ParseKnownCategoryValuesString(knownCategoryValues);
            if (!kv.ContainsKey("OrgUnit") || !Int32.TryParse(kv["OrgUnit"], out orgUnitId))
            {
                return null;
            };

            var data = emp.GetActive(orgUnitId);

            foreach (var item in data)
            {
                empList.Add(new CascadingDropDownNameValue
                {
                    name = item.FullName,
                    isDefaultValue = false,
                    value = item.EmployeeId.ToString()
                });
            }
            return empList.ToArray();
        }

        [WebMethod]
        public CascadingDropDownNameValue[] GetTrainingCourseForCategory(string knownCategoryValues, string category)
        {
            int categoryId;

            List<CascadingDropDownNameValue> empList = new List<CascadingDropDownNameValue>();
            StringDictionary kv = CascadingDropDown.ParseKnownCategoryValuesString(knownCategoryValues);
            if (!kv.ContainsKey("Category") || !Int32.TryParse(kv["Category"], out categoryId))
            {
                return null;
            };

            var data = cos.GetByCategoryId(categoryId, true);
            foreach (var item in data)
            {
                empList.Add(new CascadingDropDownNameValue
                {
                    name = item.TrainingName,
                    isDefaultValue = false,
                    value = item.CourseId.ToString()
                });
            }
            return empList.ToArray();
        }
    }
}
