﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ui/inside.Master" AutoEventWireup="true"
    CodeBehind="bankdetails.aspx.cs" Inherits="scorecard.ui.bankdetails" %>

<%@ Register Src="controls/orgstructure.ascx" TagName="orgstructure" TagPrefix="uc2" %>
<%@ Register Src="controls/bankaccountdetails.ascx" TagName="bankaccountdetails"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <uc1:bankaccountdetails ID="bankaccountdetails1" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="OrgStructure" runat="server">
    <uc2:orgstructure ID="orgstructure1" runat="server" />
</asp:Content>
