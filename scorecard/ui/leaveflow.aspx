﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ui/inside.Master" AutoEventWireup="true"
    CodeBehind="leaveflow.aspx.cs" Inherits="scorecard.ui.leaveflow" %>

<%@ Register Src="controls/orgstructure.ascx" TagName="orgstructure" TagPrefix="uc1" %>
<%@ Register Src="controls/hr/leaveApproval.ascx" TagName="leaveApproval" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header" runat="server">
    <style type="text/css">
        label
        {
            width: 220px !important;
        }
        .field_desc
        {
            font-style: normal !important;
        }
        p
        {
            padding-bottom: 10px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <uc2:leaveApproval ID="leaveApproval1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="OrgStructure" runat="server">
    <uc1:orgstructure ID="orgstructure1" runat="server" />
</asp:Content>
