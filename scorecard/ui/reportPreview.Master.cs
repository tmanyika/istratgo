﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.implementations;

namespace scorecard.ui
{
    public partial class reportPreview : System.Web.UI.MasterPage
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (Util.user == null)
            {
                AjaxScriptManager.RegisterStartupScript(Page, typeof(Page), "closeWin", "closeWindowAndRefresh();", true);
            }
        }
    }
}