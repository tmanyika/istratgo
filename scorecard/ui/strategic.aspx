﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="strategic.aspx.cs" MasterPageFile="~/ui/inside.Master" Inherits="scorecard.ui.strategic" %>
<%@ Register src="controls/orgstructure.ascx" tagname="orgstructure" tagprefix="uc1" %>
<%@ Register src="controls/strategic.ascx" tagname="strategic" tagprefix="uc2" %>

<asp:Content ID="Content1" runat="server" contentplaceholderid="OrgStructure">
    <uc1:orgstructure ID="orgstructure1" runat="server" />
</asp:Content>
<asp:Content ID="Content2" runat="server" contentplaceholderid="MainContent">
    <uc2:strategic ID="strategic1" runat="server" />
</asp:Content>

