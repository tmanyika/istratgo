<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="registration.aspx.cs" Inherits="scorecard.ui.registration" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="controls/login.ascx" TagName="login" TagPrefix="uc1" %>
<%@ Register Src="controls/registrationwithinfoimg.ascx" TagName="registrationwithinfoimg"
    TagPrefix="uc2" %>
<%@ Register Src="controls/registrationwithinfo.ascx" TagName="registrationwithinfo"
    TagPrefix="uc3" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!-- Page title -->
    <title>iStratgo </title>
    <!-- End of Page title -->
    <!-- Libraries -->
    <link type="text/css" href="css/layout.css" rel="stylesheet" />
    <link href="css/avi.css" rel="stylesheet" type="text/css" />
    <link href="css/registration.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
    <script type="text/javascript" src="js/easyTooltip.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.7.2.custom.min.js"></script>
    <script type="text/javascript" src="js/jquery.wysiwyg.js"></script>
    <script type="text/javascript" src="js/hoverIntent.js"></script>
    <script type="text/javascript" src="js/superfish.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <!-- End of Libraries -->
</head>
<body>
    <form id="form1" runat="server">
    <asp:ToolkitScriptManager ID="ToolkitScripRegisterMe" runat="server" CombineScripts="false"
        LoadScriptsBeforeUI="true">
    </asp:ToolkitScriptManager>
    <div id="container">
        <div class="logo">
            <a href="#">
                <img src="assets/logo.png" alt="" id="mylogo" /></a>
        </div>
        <div id="regform">
            <table cellpadding="0" cellspacing="20" width="100%">
                <tr>
                    <td valign="top" style="width: 360px">
                        <uc2:registrationwithinfoimg ID="registrationwithinfoimg1" runat="server" />
                    </td>
                    <td valign="top">
                        <uc3:registrationwithinfo ID="registrationwithinfo1" runat="server" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </form>
</body>
</html>
