﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ui/inside.Master" AutoEventWireup="true"
    CodeBehind="salaryvsratingform.aspx.cs" Inherits="scorecard.ui.salaryvsratingform" %>

<%@ Register Src="controls/incentiveSalaryvsRatingForm.ascx" TagName="incentiveSalaryvsRatingForm"
    TagPrefix="uc1" %>
<%@ Register Src="controls/orgstructure.ascx" TagName="orgstructure" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header" runat="server">
    <link href="css/ajaxcalendar.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <uc1:incentiveSalaryvsRatingForm ID="incentiveSalaryvsRatingForm1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="OrgStructure" runat="server">
    <uc2:orgstructure ID="orgstructure1" runat="server" />
</asp:Content>
