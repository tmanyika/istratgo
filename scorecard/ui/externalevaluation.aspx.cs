﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.entities;
using scorecard.implementations;

namespace scorecard.ui
{
    public partial class externalevaluation : System.Web.UI.Page
    {
        private string EmailAddress
        {
            get { return Util.Decrypt(Request.QueryString["UserId"].ToString().Replace(" ", "+")); }
        }

        private string FName
        {
            get { return Util.Decrypt(Request.QueryString["UserN"].ToString().Replace(" ", "+")); }
        }

        private int Company
        {
            get { return int.Parse(Util.Decrypt(Request.QueryString["CompId"].ToString().Replace(" ", "+"))); }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["UserInfo"] = new UserInfo
                {
                    Email = EmailAddress,
                    CompanyId = Company,
                    LoginId = EmailAddress,
                    FullName = FName,
                    CountryId = 0,
                    OrgId = 0,
                    ReportToUserId = 0,
                    ShowAgain = false,
                    UserId = 0,
                    UserTypeId = 46
                };
            }
        }

    }
}