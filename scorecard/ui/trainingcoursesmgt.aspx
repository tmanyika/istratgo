﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ui/inside.Master" AutoEventWireup="true"
    CodeBehind="trainingcoursesmgt.aspx.cs" Inherits="scorecard.ui.trainingcoursesmgt" %>

<%@ Register Src="controls/training/trainingCourse.ascx" TagName="trainingCourse"
    TagPrefix="uc1" %>
<%@ Register Src="controls/orgstructure.ascx" TagName="orgstructure" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <uc1:trainingCourse ID="trainingCourse1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="OrgStructure" runat="server">
    <uc2:orgstructure ID="orgstructure1" runat="server" />
</asp:Content>
