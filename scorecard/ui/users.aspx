﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ui/inside.Master" AutoEventWireup="true"
    CodeBehind="users.aspx.cs" Inherits="scorecard.ui.users" %>

<%@ Register Src="controls/orgstructure.ascx" TagName="orgstructure" TagPrefix="uc2" %>
<%@ Register Src="controls/hr/employees.ascx" TagName="employees" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header" runat="server">
    <link href="highslide/highslide.css" rel="stylesheet" type="text/css" />
    <link href="css/ajaxcalendar.css" rel="stylesheet" type="text/css" />
    <script src="highslide/highslide-with-html.min.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <uc1:employees ID="employees1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="OrgStructure" runat="server">
    <uc2:orgstructure ID="orgstructure1" runat="server" />
</asp:Content>
