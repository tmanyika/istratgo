﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ThreeSixty.Evaluation;
using ScoreCard.Common.Config;
using scorecard.implementations;

namespace scorecard.ui.reports
{
    public partial class graph360PeerEvaluation : System.Web.UI.UserControl
    {
        #region Properties

        string Date
        {
            get
            {
                return Request["dVal"].ToString();
            }
        }

        int AreaId
        {
            get
            {
                return int.Parse(Request["aid"].ToString());
            }
        }

        int AreaValueId
        {
            get
            {
                return int.Parse(Request["idv"].ToString());
            }
        }

        DateTime PeriodDate
        {
            get
            {
                return DateTime.Parse(Request["dVal"].ToString());
            }
        }

        #endregion

        #region Variables

        IEvaluationReport report;

        #endregion

        #region Default Constructors

        public graph360PeerEvaluation()
        {
            report = new EvaluationReportBL();
        }

        #endregion

        #region Page Event Handling Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindData();
            }
        }

        #endregion

        #region Databinding

        void BindData()
        {
            try
            {
                List<EvaluationReport> data = null;

                if (AreaId == ScoreCardAreaConfig.EmployeeAreaId ||
                    AreaId == ScoreCardAreaConfig.JobTitleAreaId)
                {
                    data = report.Get360UserPeerReport(AreaId, AreaValueId, PeriodDate);
                }
                else if (AreaId == ScoreCardAreaConfig.OrgUnitAreaId)
                {
                    data = report.Get360OrgUnitPeerReport(AreaId, AreaValueId, PeriodDate);
                }
                else if (AreaId == ScoreCardAreaConfig.ProjectAreaId)
                {
                    data = report.Get360ProjectPeerReport(AreaId, AreaValueId, PeriodDate);
                }

                int numberOfWeightedRecords = data.Where(w => w.WeightedAvgRating == null || w.TargetWeightRating == null).Count();
               
                if (data.Count >= Util.getGraphMinItems())
                {
                    radChart.Height = new Unit(Util.getGraphWidth());
                    radChart.Width = new Unit(Util.getGraphHeight());
                }

                radChart.DataSource = data;
                radChart.DataBind();

                if (numberOfWeightedRecords > 0)
                {
                  
                }

                BindControl.BindLiteral(LblNoDataMsg, data.Count > 0 ? string.Empty : Messages.NoRecordsToDisplay);
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(LblNoDataMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        #endregion
    }
}