﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using scorecard.implementations;
using scorecard.controllers;
using scorecard.entities;
using vb = Microsoft.VisualBasic;
using HR.Human.Resources;
using System.Collections;
using Dashboard.Reporting;

namespace scorecard.ui.reports
{
    public partial class dashFocusAreaWeighting : System.Web.UI.UserControl
    {
        #region Variables

        IDashboard dash;

        #endregion

        #region Default Class Constructors

        public dashFocusAreaWeighting()
        {
            dash = new DashboardBL();
        }

        #endregion

        #region Delegates Methods

        public void FillFocusAreas(int areaId, int orgUnitId, bool isCompany)
        {
            try
            {
                DataTable data = dash.GetFocusAreas(areaId, orgUnitId, isCompany);
                LblNoDataMsg.Visible = data.Rows.Count > 0 ? false : true;
                orgChart.Visible = data.Rows.Count > 0 ? true : false;

                if (data.Rows.Count <= 0) return;
                if (data.Rows.Count > Util.getDashBoardGraphMinItems())
                {
                    orgChart.Height = new Unit(Util.getDashBoardGraphHeight());
                    orgChart.Width = new Unit(Util.getDashBoardGraphWidth());
                }

                orgChart.Series[0].ToolTip = "#VALX - #VALY%";
                orgChart.Series[0].XValueMember = "Perspective";
                orgChart.Series[0].YValueMembers = "PerspectiveValue";
                orgChart.DataSource = data;
                orgChart.DataBind();
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        #endregion
    }
}