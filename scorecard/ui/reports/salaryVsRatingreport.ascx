﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="salaryVsRatingreport.ascx.cs"
    Inherits="scorecard.ui.reports.salaryVsRatingreport" %>
<%@ Import Namespace="scorecard.implementations" %>
<div style="background-color: White;">
    <table cellpadding="0" cellspacing="10" border="0" width="90%">
        <tr>
            <td style="width: 250px;">
                <h2>
                    Report Name</h2>
            </td>
            <td style="width: 250px;">
                <h2>
                    <span style="color: Black">Salary vs Rating</span></h2>
            </td>
            <td align="right">
                <span style="color: Red">
                    <asp:Literal ID="lblError" runat="server"></asp:Literal></span>
                <asp:LinkButton ID="lnkPrint" runat="server" CausesValidation="False" OnClientClick="window.print();">
                    <img runat="server" src="~/ui/assets/icons/print.png" id="imgPrint" alt="Click to Print" /></asp:LinkButton>&nbsp;&nbsp;<asp:LinkButton
                        ID="lnkExcel" runat="server" CausesValidation="False" OnClick="lnkExcel_Click">
                        <img src="~/ui/assets/icons/Excel.png" runat="server" id="imgExcel" alt="Export to Excel" /></asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td>
                <h2>
                    Organisation Unit</h2>
            </td>
            <td colspan="2">
                <h2>
                    <span style="color: Black">
                        <asp:Literal ID="lblOrgUnit" runat="server"></asp:Literal>
                    </span>
                </h2>
            </td>
        </tr>
        <tr>
            <td>
                <h2>
                    Salary Type</h2>
            </td>
            <td colspan="2">
                <h2>
                    <span style="color: Black">
                        <asp:Literal ID="lblSalaryType" runat="server"></asp:Literal>
                    </span>
                </h2>
            </td>
        </tr>
        <tr>
            <td>
                <h2>
                    Start Date</h2>
            </td>
            <td colspan="2">
                <h2>
                    <span style="color: Black">
                        <asp:Literal ID="lblStartDate" runat="server"></asp:Literal>
                    </span>
                </h2>
            </td>
        </tr>
        <tr>
            <td>
                <h2>
                    End Date</h2>
            </td>
            <td colspan="2">
                <h2>
                    <span style="color: Black">
                        <asp:Literal ID="lblEndDate" runat="server"></asp:Literal>
                    </span>
                </h2>
            </td>
        </tr>
    </table>
</div>
<div>
    <asp:ListView ID="lstData" runat="server">
        <LayoutTemplate>
            <table class="fullwidth" cellpadding="0" cellspacing="0" border="0" width="90%">
                <thead>
                    <tr>
                        <th align="left">
                            Surname
                        </th>
                        <th align="left">
                            Middle Name
                        </th>
                        <th align="left">
                            First Name
                        </th>
                        <th align="right">
                            Salary
                        </th>
                        <th align="right">
                            Rating
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr id="ItemPlaceHolder" runat="server">
                    </tr>
                </tbody>
            </table>
        </LayoutTemplate>
        <ItemTemplate>
            <tr class="odd">
                <td align="left">
                    <asp:Literal ID="lblSurname" runat="server" Text='<%#  Eval("LastName") %>'></asp:Literal>
                </td>
                <td align="left">
                    <asp:Literal ID="lblMiddleName" runat="server" Text='<%#  Eval("MiddleName") %>'></asp:Literal>
                </td>
                <td align="left">
                    <asp:Literal ID="lblFirstName" runat="server" Text='<%#  Eval("FirstName") %>' />
                </td>
                <td align="right">
                    <asp:Literal ID="lblSalary" runat="server" Text='<%# Eval("SalaryAmount")%>' />
                </td>
                <td align="right">
                    <asp:Literal ID="lblRating" runat="server" Text='<%# Eval("Rating") %>' />
                </td>
            </tr>
        </ItemTemplate>
        <EmptyDataTemplate>
            There are no records to display for the criteria you specified.
        </EmptyDataTemplate>
    </asp:ListView>
</div>
