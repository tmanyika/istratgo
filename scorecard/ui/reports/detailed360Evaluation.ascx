﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="detailed360Evaluation.ascx.cs"
    Inherits="scorecard.ui.reports.detailed360Evaluation" %>
<style type="text/css">
    th
    {
        text-align: left !important;
    }
    
    td
    {
        padding-left: 12px !important;
        padding-right: 12px !important;
    }
</style>
<div style="background-color: White;">
    <table cellpadding="0" cellspacing="10" border="0">
        <tr>
            <td style="width: 250px;">
                <h2>
                    Area</h2>
            </td>
            <td style="width: 250px;">
                <h2>
                    <span style="color: Black">
                        <asp:Literal ID="lblCriteria" runat="server"></asp:Literal></span></h2>
            </td>
            <td align="right">
                <span style="color: Red">
                    <asp:Literal ID="lblError" runat="server"></asp:Literal></span><asp:LinkButton ID="lnkPrint"
                        runat="server" CausesValidation="False" OnClientClick="window.print();">
                        <img runat="server" src="~/ui/assets/icons/print.png" id="imgPrint" alt="Click to Print" /></asp:LinkButton>&nbsp;&nbsp;<asp:LinkButton
                            ID="lnkExcel" runat="server" CausesValidation="False" OnClick="lnkExcel_Click">
                            <img src="~/ui/assets/icons/Excel.png" runat="server" id="imgExcel" alt="Export to Excel" /></asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td>
                <h2>
                    Name</h2>
            </td>
            <td colspan="2">
                <h2>
                    <span style="color: Black">
                        <asp:Literal ID="lblName" runat="server"></asp:Literal>
                    </span>
                </h2>
            </td>
        </tr>
        <tr>
            <td>
                <h2>
                    Report Date</h2>
            </td>
            <td colspan="2">
                <h2>
                    <span style="color: Black">
                        <asp:Literal ID="lblDate" runat="server"></asp:Literal>
                    </span>
                </h2>
            </td>
        </tr>
        <tr>
            <td>
                <h2>
                    Report Type</h2>
            </td>
            <td colspan="2">
                <h2>
                    <span style="color: Black">
                        <asp:Literal ID="lblReportType" runat="server"></asp:Literal>
                    </span>
                </h2>
            </td>
        </tr>
    </table>
</div>
<div>
    <asp:GridView ID="gdvData" runat="server" CellPadding="0" CellSpacing="0" CssClass="fullwidth"
        HeaderStyle-HorizontalAlign="Left" RowStyle-HorizontalAlign="Left">
    </asp:GridView>
</div>
<div>
    <asp:Literal ID="LblNoDataMsg" runat="server"></asp:Literal>
</div>
