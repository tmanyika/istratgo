﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="dashPerformanceRatingVsNumStaff.ascx.cs"
    Inherits="scorecard.ui.reports.dashPerformanceRatingVsNumStaff" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Charting" TagPrefix="telerik" %>

<div>
    <table border="0" cellpadding="2" cellspacing="0">
        <tr>
            <td class="box_rotate">
                <b>No. Of Employees</b>
            </td>
            <td>
                <telerik:RadChart ID="MyRadChart" runat="server" AutoTextWrap="True" Skin="Mac" AutoLayout="True"
                    Height="350px" Width="400px" SeriesOrientation="Vertical" OnBeforeLayout="RadChart_BeforeLayout">
                    <Appearance Corners="Round, Round, Round, Round, 6">
                        <FillStyle FillType="Image">
                            <FillSettings BackgroundImage="{chart}" ImageDrawMode="Flip" ImageFlip="FlipX">
                            </FillSettings>
                        </FillStyle>
                        <Border Color="138, 138, 138" />
                    </Appearance>
                    <Series>
                        <telerik:ChartSeries Name="Current Year" DataYColumn="CurrentYear">
                            <Appearance>
                                <FillStyle MainColor="55, 167, 226" SecondColor="22, 85, 161">
                                    <FillSettings GradientMode="Vertical">
                                    </FillSettings>
                                </FillStyle>
                                <TextAppearance TextProperties-Color="Black">
                                </TextAppearance>
                            </Appearance>
                        </telerik:ChartSeries>
                        <telerik:ChartSeries Name="Previous Year" DataYColumn="LastYear">
                            <Appearance>
                                <FillStyle MainColor="223, 87, 60" SecondColor="200, 38, 37">
                                    <FillSettings GradientMode="Vertical">
                                    </FillSettings>
                                </FillStyle>
                                <TextAppearance TextProperties-Color="Black">
                                </TextAppearance>
                            </Appearance>
                        </telerik:ChartSeries>
                    </Series>
                    <Legend>
                        <Appearance Dimensions-Margins="15.4%, 3%, 1px, 1px" Position-AlignedPosition="TopRight">
                            <ItemMarkerAppearance Figure="Square">
                                <Border Color="134, 134, 134" />
                            </ItemMarkerAppearance>
                            <FillStyle MainColor="">
                            </FillStyle>
                            <Border Color="Transparent" />
                        </Appearance>
                    </Legend>
                    <PlotArea>
                        <XAxis DataLabelsColumn="RatingValue">
                            <Appearance Color="134, 134, 134" MajorTick-Color="134, 134, 134">
                                <MajorGridLines Color="209, 222, 227" PenStyle="Solid" />
                                <TextAppearance TextProperties-Color="51, 51, 51">
                                </TextAppearance>
                            </Appearance>
                            <AxisLabel>
                                <Appearance RotationAngle="270">
                                </Appearance>
                                <TextBlock>
                                    <Appearance TextProperties-Color="51, 51, 51">
                                    </Appearance>
                                </TextBlock>
                            </AxisLabel>
                        </XAxis>
                        <YAxis>
                            <Appearance Color="134, 134, 134" MajorTick-Color="134, 134, 134" MinorTick-Color="134, 134, 134"
                                MinorTick-Width="0">
                                <MajorGridLines Color="209, 222, 227" />
                                <MinorGridLines Color="233, 239, 241" />
                                <TextAppearance TextProperties-Color="51, 51, 51">
                                </TextAppearance>
                            </Appearance>
                            <AxisLabel TextBlock-Text="Rating">
                                <Appearance RotationAngle="0">
                                </Appearance>
                                <TextBlock>
                                    <Appearance TextProperties-Color="51, 51, 51">
                                    </Appearance>
                                </TextBlock>
                            </AxisLabel>
                        </YAxis>
                        <YAxis2>
                            <AxisLabel>
                                <Appearance RotationAngle="0">
                                </Appearance>
                            </AxisLabel>
                        </YAxis2>
                        <Appearance>
                            <FillStyle FillType="Solid" MainColor="White">
                            </FillStyle>
                            <Border Color="134, 134, 134" />
                        </Appearance>
                    </PlotArea>
                    <ChartTitle>
                        <Appearance Position-AlignedPosition="Top">
                            <FillStyle MainColor="">
                            </FillStyle>
                        </Appearance>
                        <TextBlock Appearance-AutoTextWrap="False" Text="Performance Rating Vs Number Of Staff"
                            Appearance-TextProperties-Color="Blue">
                            <Appearance AutoTextWrap="False" TextProperties-Font="Tahoma, 13pt">
                            </Appearance>
                        </TextBlock>
                    </ChartTitle>
                </telerik:RadChart>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center"><br />
                <span class="matrixaxislabels"><b>Rating</b></span>
            </td>
        </tr>
    </table>
</div>
<div class="dashboardred" style="padding-left: 115px">
    <br />
    <asp:Literal ID="LblData" runat="server" Text="" />
    <asp:HiddenField ID="hdnTable" runat="server" Value="<table cellpadding='6' cellspacing='4' border='1' width='400px' class='dashtable'>" />
    <br />
</div>
