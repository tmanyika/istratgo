﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HR.Incentives.Interfaces;
using HR.Incentives.Logic;
using scorecard.implementations;
using RKLib.ExportData;
using System.Data;
using HR.Incentives.Entities;
using scorecard.interfaces;

namespace scorecard.ui.reports
{
    public partial class incentiveReport : System.Web.UI.UserControl
    {
        #region Properties

        private DateTime StartDate
        {
            get
            {
                return DateTime.Parse(Request.QueryString["dsVal"].ToString());
            }
        }


        private DateTime EndDate
        {
            get
            {
                return DateTime.Parse(Request.QueryString["deVal"].ToString());
            }
        }

        private int SalaryTypeId
        {
            get
            {
                return int.Parse(Request.QueryString["sid"].ToString());
            }
        }

        private int IncentiveId
        {
            get
            {
                return int.Parse(Request.QueryString["iid"].ToString());
            }
        }

        private int CompanyId
        {
            get
            {
                return int.Parse(Request.QueryString["compId"].ToString());
            }
        }

        private int? OrgUnitId
        {
            get
            {
                int? defInt = null;
                int orgunitid = int.Parse(Request.QueryString["orgunitId"].ToString());
                return orgunitid > 0 ? orgunitid : defInt;
            }
        }

        private string SalaryTypeName
        {
            get
            {
                return HttpUtility.UrlDecode(Request.QueryString["salname"].ToString());
            }
        }

        private string OrgUnitName
        {
            get
            {
                return HttpUtility.UrlDecode(Request.QueryString["orgunitname"].ToString());
            }
        }

        private string IncentiveName
        {
            get
            {
                return HttpUtility.UrlDecode(Request.QueryString["incname"].ToString());
            }
        }

        #endregion

        #region Default Constructors

        IIncentiveReport rep;

        public incentiveReport()
        {
            rep = new IncentiveReportBL();
        }

        #endregion

        #region Page Event Handling Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillInfo();
                BindData();
            }
        }

        #endregion

        #region Databinding Methods

        void FillInfo()
        {
            try
            {
                lblEndDate.Text = Common.GetDescriptiveDate(EndDate);
                lblSalaryType.Text = SalaryTypeName;
                lblStartDate.Text = Common.GetDescriptiveDate(StartDate);
                lblOrgUnit.Text = OrgUnitName;
                lblIncentive.Text = IncentiveName;
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void BindData()
        {
            try
            {
                var data = rep.GetIncentiveReport(OrgUnitId, CompanyId, IncentiveId, SalaryTypeId, StartDate, EndDate);
                if (data.Count > 0)
                {
                    var totalCaption = "<b>Totals</b>";
                    var totalSalary = data.Sum(s => s.SalaryAmount);
                    var totalIncentives = data.Sum(s => s.IncentivePayable);
                    var totals = new IncentiveReport
                    {
                        LastName = totalCaption,
                        SalaryAmount = totalSalary,
                        IncentivePayable = totalIncentives
                    };

                    data.Add(totals);
                }

                BindControl.BindListView(lstData, data);
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblError, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        DataTable GetExportSchema()
        {
            DataTable dT = new DataTable();
            dT.Columns.Add("Surname", typeof(string));
            dT.Columns.Add("FirstName", typeof(string));
            dT.Columns.Add("MiddleName", typeof(string));
            dT.Columns.Add("Salary", typeof(string));
            dT.Columns.Add("Rating", typeof(string));
            dT.Columns.Add("PercentOfSalaryType", typeof(string));
            dT.Columns.Add("IncentivePayable", typeof(string));

            dT.AcceptChanges();
            return dT;
        }

        private void Download()
        {
            if (lstData.Items.Count <= 0)
            {
                lblError.Text = "There are no records to export to excel.";
                return;
            }

            DataTable dT = GetExportSchema();
            int[] cols = { 0, 1, 2, 3, 4, 5, 6 };
            string[] headers = { "", "", "", "", "", "", "" };

            DataRow nRow = dT.NewRow();
            nRow["Surname"] = "Report Name";
            nRow["FirstName"] = "Incentive Report";
            dT.Rows.Add(nRow);
            dT.Rows.Add(dT.NewRow());//add blank row
            nRow = null;

            nRow = dT.NewRow();
            nRow["Surname"] = "Organisation Unit";
            nRow["FirstName"] = lblOrgUnit.Text;
            dT.Rows.Add(nRow);
            dT.Rows.Add(dT.NewRow());//add blank row
            nRow = null;

            nRow = dT.NewRow();
            nRow["Surname"] = "Incentive";
            nRow["FirstName"] = lblSalaryType.Text;
            dT.Rows.Add(nRow);
            dT.Rows.Add(dT.NewRow());//add blank row
            nRow = null;

            nRow = dT.NewRow();
            nRow["Surname"] = "Salary Type";
            nRow["FirstName"] = lblSalaryType.Text;
            dT.Rows.Add(nRow);
            dT.Rows.Add(dT.NewRow());//add blank row
            nRow = null;

            nRow = dT.NewRow();
            nRow["Surname"] = "Start Date";
            nRow["FirstName"] = lblStartDate.Text;
            dT.Rows.Add(nRow);
            dT.Rows.Add(dT.NewRow());//add blank row
            nRow = null;

            nRow = dT.NewRow();
            nRow["Surname"] = "End Date";
            nRow["FirstName"] = lblEndDate.Text;
            dT.Rows.Add(nRow);
            dT.Rows.Add(dT.NewRow());//add blank row
            nRow = null;

            nRow = dT.NewRow();
            nRow["Surname"] = "Surname";
            nRow["FirstName"] = "First Name";
            nRow["MiddleName"] = "Middle Name";
            nRow["Salary"] = "Salary";
            nRow["Rating"] = "Rating";
            nRow["PercentOfSalaryType"] = "% of Salary Type";
            nRow["IncentivePayable"] = "Recommended Incentive Payable";
            dT.Rows.Add(nRow);
            nRow = null;

            foreach (ListViewItem item in lstData.Items)
            {
                string rating = (item.FindControl("lblRating") as Literal).Text;
                string surname = (item.FindControl("lblSurname") as Literal).Text;
                string firstname = (item.FindControl("lblFirstName") as Literal).Text;
                string middlename = (item.FindControl("lblMiddleName") as Literal).Text;
                string salary = (item.FindControl("hdnSalaryAmount") as HiddenField).Value; //(item.FindControl("lblSalary") as Literal).Text;
                string percentage = (item.FindControl("lblPercentage") as Literal).Text;
                string payable = (item.FindControl("hdnIncentivePayable") as HiddenField).Value; //(item.FindControl("lblPayable") as Literal).Text;


                nRow = dT.NewRow();
                nRow["Surname"] = surname.Replace("<b>", "").Replace("</b>", "");
                nRow["FirstName"] = firstname;
                nRow["MiddleName"] = middlename;
                nRow["Salary"] = salary;
                nRow["Rating"] = rating;
                nRow["PercentOfSalaryType"] = percentage;
                nRow["IncentivePayable"] = payable;
                dT.Rows.Add(nRow);
                nRow = null;
            }

            dT.AcceptChanges();
            Export exp = new Export("Web");
            exp.ExportDetails(dT, cols, headers, Export.ExportFormat.Excel, Export.ExportAction.DownloadOnly, string.Format("{0}.xls", SalaryTypeName.Replace(" ","")));
        }

        #endregion

        #region Button Event Handling Methods

        protected void lnkExcel_Click(object sender, EventArgs e)
        {
            Download();
        }

        #endregion

    }
}