﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RKLib.ExportData;
using System.Data;
using scorecard.implementations;
using HR.EmployeeTraining.Interfaces;
using HR.EmployeeTraining.Logic;

namespace scorecard.ui.reports
{
    public partial class trainingemployeereport : System.Web.UI.UserControl
    {
        #region Properties

        private DateTime StartDate
        {
            get
            {
                return DateTime.Parse(Request.QueryString["dsVal"].ToString());
            }
        }


        private DateTime EndDate
        {
            get
            {
                return DateTime.Parse(Request.QueryString["deVal"].ToString());
            }
        }

        private int? EmployeeId
        {
            get
            {
                int? defInt = null;
                int empId = int.Parse(Request.QueryString["sid"].ToString());
                return empId > 0 ? empId : defInt;
            }
        }

        private int? LineManagerId
        {
            get
            {
                int? defInt = null;
                int? roleId = Util.user.UserTypeId;
                return (roleId == Util.getTypeDefinitionsUserTypeAdministrator() ||
                        roleId == Util.getTypeDefinitionsUserTypeNormalUser()) ? defInt : Util.user.UserId;
            }
        }

        private int CompanyId
        {
            get
            {
                return Util.user.CompanyId;
            }
        }

        private bool IsCompReport
        {
            get
            {
                return bool.Parse(Request.QueryString["isComp"].ToString());
            }
        }

        private int OrgUnitId
        {
            get
            {
                int orgunitid = int.Parse(Request.QueryString["orgunitId"].ToString());
                return orgunitid;
            }
        }

        private string EmployeeName
        {
            get
            {
                return HttpUtility.UrlDecode(Request.QueryString["name"].ToString());
            }
        }

        private string OrgUnitName
        {
            get
            {
                return HttpUtility.UrlDecode(Request.QueryString["orgunit"].ToString());
            }
        }

        #endregion

        #region Default Constructors

        ITrainingReport rep;

        public trainingemployeereport()
        {
            rep = new TrainingReportBL();
        }

        #endregion

        #region Page Event Handling Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillInfo();
                BindData();
            }
        }

        #endregion

        #region Databinding Methods

        void FillInfo()
        {
            try
            {
                lblEndDate.Text = Common.GetDescriptiveDate(EndDate);
                lblEmployeeName.Text = EmployeeName;
                lblStartDate.Text = Common.GetDescriptiveDate(StartDate);
                lblOrgUnit.Text = OrgUnitName;
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void BindData()
        {
            try
            {
                var data = IsCompReport ? rep.GetTrainingCompanyReport(CompanyId, EmployeeId, LineManagerId, StartDate, EndDate) :
                                          rep.GetTrainingOrgUnitReport(OrgUnitId, EmployeeId, LineManagerId, StartDate, EndDate);
                BindControl.BindListView(lstData, data);
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblError, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        DataTable GetExportSchema()
        {
            DataTable dT = new DataTable();

            dT.Columns.Add("FirstName", typeof(string));
            dT.Columns.Add("MiddleName", typeof(string));
            dT.Columns.Add("Surname", typeof(string));
            dT.Columns.Add("OrgUnitName", typeof(string));
            dT.Columns.Add("TrainingName", typeof(string));
            dT.Columns.Add("StartDate", typeof(string));
            dT.Columns.Add("EndDate", typeof(string));
            dT.Columns.Add("StatusName", typeof(string));
            dT.Columns.Add("PerfomanceMeasureName", typeof(string));
            dT.Columns.Add("LatestRating", typeof(string));
            dT.AcceptChanges();
            return dT;
        }

        private void Download()
        {
            if (lstData.Items.Count <= 0)
            {
                lblError.Text = "There are no records to export to excel.";
                return;
            }

            DataTable dT = GetExportSchema();
            int numOfCols = dT.Columns.Count;

            int[] cols = new int[numOfCols];
            string[] headers = new string[numOfCols];

            for (int i = 0; i < numOfCols; i++)
            {
                cols[i] = i;
                headers[i] = string.Empty;
            }

            DataRow nRow = dT.NewRow();
            nRow["FirstName"] = "Report Name";
            nRow["MiddleName"] = "Training Report";
            dT.Rows.Add(nRow);
            dT.Rows.Add(dT.NewRow());//add blank row
            nRow = null;

            nRow = dT.NewRow();
            nRow["FirstName"] = "Organisation Unit";
            nRow["MiddleName"] = lblOrgUnit.Text;
            dT.Rows.Add(nRow);
            dT.Rows.Add(dT.NewRow());//add blank row
            nRow = null;

            nRow = dT.NewRow();
            nRow["FirstName"] = "Employee Name";
            nRow["MiddleName"] = lblEmployeeName.Text;
            dT.Rows.Add(nRow);
            dT.Rows.Add(dT.NewRow());//add blank row
            nRow = null;

            nRow = dT.NewRow();
            nRow["FirstName"] = "Start Date";
            nRow["MiddleName"] = lblStartDate.Text;
            dT.Rows.Add(nRow);
            dT.Rows.Add(dT.NewRow());//add blank row
            nRow = null;

            nRow = dT.NewRow();
            nRow["FirstName"] = "End Date";
            nRow["MiddleName"] = lblEndDate.Text;
            dT.Rows.Add(nRow);
            dT.Rows.Add(dT.NewRow());//add blank row
            nRow = null;

            nRow = dT.NewRow();
            nRow["FirstName"] = "First Name";
            nRow["MiddleName"] = "Middle Name";
            nRow["Surname"] = "Surname";
            nRow["OrgUnitName"] = "Organisation Unit";
            nRow["TrainingName"] = "Training";
            nRow["StartDate"] = "Start Date";
            nRow["EndDate"] = "End Date";
            nRow["StatusName"] = "Status";
            nRow["PerfomanceMeasureName"] = "Perfomance MeasureName";
            nRow["LatestRating"] = "Latest Rating";

            dT.Rows.Add(nRow);
            nRow = null;

            foreach (ListViewItem item in lstData.Items)
            {
                string latestrating = (item.FindControl("lblLatestRating") as Literal).Text;
                string surname = (item.FindControl("lblSurname") as Literal).Text;
                string firstname = (item.FindControl("lblFirstName") as Literal).Text;
                string middlename = (item.FindControl("lblMiddleName") as Literal).Text;
                string orgunitname = (item.FindControl("lblOrgUnitName") as Literal).Text;
                string trainingname = (item.FindControl("lblTrainingName") as Literal).Text;
                string startdate = (item.FindControl("lblStartDate") as Literal).Text;
                string enddate = (item.FindControl("lblEndDate") as Literal).Text;
                string statusName = (item.FindControl("lblStatusName") as Literal).Text;
                string performancemeasure = (item.FindControl("lblPerfomanceMeasureName") as Literal).Text;


                nRow = dT.NewRow();
                nRow["FirstName"] = firstname;
                nRow["MiddleName"] = middlename;
                nRow["Surname"] = surname;
                nRow["OrgUnitName"] = orgunitname;
                nRow["TrainingName"] = trainingname;
                nRow["StartDate"] = startdate;
                nRow["EndDate"] = enddate;
                nRow["StatusName"] = statusName;
                nRow["PerfomanceMeasureName"] = performancemeasure;
                nRow["LatestRating"] = latestrating;
                dT.Rows.Add(nRow);
                nRow = null;
            }

            dT.AcceptChanges();
            Export exp = new Export("Web");
            exp.ExportDetails(dT, cols, headers, Export.ExportFormat.Excel, Export.ExportAction.DownloadOnly, string.Format("{0}.xls", EmployeeName));
        }

        #endregion

        #region Button Event Handling Methods

        protected void lnkExcel_Click(object sender, EventArgs e)
        {
            Download();
        }

        #endregion
    }
}