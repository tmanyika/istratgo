﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ThreeSixty.Evaluation;

namespace scorecard.ui.reports
{
    public partial class evaluationType : System.Web.UI.UserControl
    {
        #region Properties

        string Date
        {
            get
            {
                return Request["dVal"].ToString();
            }
        }

        int AreaId
        {
            get
            {
                return int.Parse(Request["aid"].ToString());
            }
        }

        int AreaValueId
        {
            get
            {
                return int.Parse(Request["idv"].ToString());
            }
        }

        #endregion

        #region Member Variables

        IEvaluationSubmission sub;

        #endregion

        #region Default Constructors

        public evaluationType()
        {
            sub = new EvaluationSubmissionBL();
        }

        #endregion

        #region Page Load Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillInfo();
            }
        }

        #endregion

        #region Databinding Methods

        string GetEvalualtionType(int areaId, int areaValueId, string date)
        {
            DateTime periodDate = DateTime.Parse(date);
            var data = sub.GetEvaluation(areaId, areaValueId, periodDate);
            string evaluationType = data.IsInternal ? "Internal" : "External";

            return evaluationType;
        }

        void FillInfo()
        {
            string evalType = GetEvalualtionType(AreaId, AreaValueId, Date);
            lblEvalType.Text = evalType;
        }

        #endregion
    }
}