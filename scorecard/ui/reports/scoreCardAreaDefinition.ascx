﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="scoreCardAreaDefinition.ascx.cs"
    Inherits="scorecard.ui.reports.scoreCardAreaDefinition" %>
<table cellpadding="0" cellspacing="10" border="0">
    <tr>
        <td style="width: 250px;">
            <h2>
                Area</h2>
        </td>
        <td style="width: 250px;">
            <h2>
                <span style="color: Black">
                    <asp:Literal ID="lblCriteria" runat="server"></asp:Literal></span></h2>
        </td>
        <td align="right">
            <span style="color: Red">
                <asp:Literal ID="lblError" runat="server"></asp:Literal></span><asp:LinkButton ID="lnkPrint"
                    runat="server" CausesValidation="False" OnClientClick="window.print();">
                    <img runat="server" src="~/ui/assets/icons/print.png" id="imgPrint" alt="Click to Print" /></asp:LinkButton>
        </td>
    </tr>
    <tr>
        <td>
            <h2>
                Name</h2>
        </td>
        <td colspan="2">
            <h2>
                <span style="color: Black">
                    <asp:Literal ID="lblName" runat="server"></asp:Literal>
                </span>
            </h2>
        </td>
    </tr>
    <tr>
        <td>
            <h2>
                Report Date</h2>
        </td>
        <td colspan="2">
            <h2>
                <span style="color: Black">
                    <asp:Literal ID="lblDate" runat="server"></asp:Literal>
                </span>
            </h2>
        </td>
    </tr>
</table>
