﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.controllers;
using scorecard.implementations;
using scorecard.entities;
using System.Data;
using RKLib.ExportData;
using HR.Human.Resources;

namespace scorecard.ui.reports
{
    public partial class scoreReportView : System.Web.UI.UserControl
    {
        #region Variables

        useradmincontroller usersDef;
        goaldefinition goalDef;
        scoresmanager scoresManager;
        structurecontroller orgunits;

        #endregion

        #region Properties

        string Date
        {
            get
            {
                return Request["dVal"].ToString();
            }
        }

        int AreaId
        {
            get
            {
                return int.Parse(Request["aid"].ToString());
            }
        }

        int AreaValueId
        {
            get
            {
                return int.Parse(Request["idv"].ToString());
            }
        }

        #endregion

        #region Default Class Constructors

        public scoreReportView()
        {
            goalDef = new goaldefinition(new goalsImpl());
            orgunits = new structurecontroller(new structureImpl());
            usersDef = new useradmincontroller(new useradminImpl());
            scoresManager = new scoresmanager(new scorecard.implementations.scoresImpl());
        }

        #endregion

        #region Page Event Handling Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                fillInfo();
                loadData();
            }
        }

        #endregion

        #region Databinding Methods

        public object GetFinalScore(object finalScore, object finalAgreedScore)
        {
            return (finalAgreedScore == DBNull.Value ||
                finalAgreedScore == null) ? finalScore : finalAgreedScore;
        }

        public object GetValue(object dataVal)
        {
            return (dataVal == null) ? string.Empty : dataVal.ToString();
        }

        private void fillInfo()
        {
            try
            {
                lblCriteria.Text = usersDef.appsettings.getApplicationSetting(new entities.applicationsettings { TYPE_ID = AreaId }).DESCRIPTION;
                lblName.Text = getName(AreaValueId, AreaId);
                lblDate.Text = Common.GetDescriptiveDate(Date, false);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        string getName(int id, int criteria)
        {
            try
            {
                if (id > 0)
                    if (criteria == Util.getTypeDefinitionsRolesTypeID())
                    {
                        IEmployee emp = new EmployeeBL();
                        return emp.GetById(id).FullName;
                    }
                    else if (criteria == Util.getTypeDefinitionsProjectID()) return new projectmanagement(new projectImpl()).get(id).PROJECT_NAME;
                    else return orgunits.getOrgunitDetails(new Orgunit { ID = id }).ORGUNIT_NAME;
            }
            catch (Exception e) { Util.LogErrors(e); }
            return "";
        }

        void trimData(List<scores> objectives)
        {
            try
            {
                string prevPerspective = "";
                decimal totalPerspective = 0;

                foreach (ListViewDataItem item in lstData.Items)
                {
                    HiddenField hdnPid = item.FindControl("hdnPid") as HiddenField;
                    Literal lblWeight = item.FindControl("lblWeight") as Literal;
                    Literal lblPerspective = item.FindControl("lblPerspective") as Literal;
                    string nxtPerspective = (item.FindControl("lblPerspective") as Literal).Text.Trim();
                    if (string.Compare(nxtPerspective, prevPerspective) == 0) lblPerspective.Text = "";
                    else
                    {
                        int pid = int.Parse(hdnPid.Value);
                        if (item.DataItemIndex != lstData.Items.Count - 1)
                        {
                            decimal total = objectives.Where(c => c.PERSPECTIVE_ID == pid).Sum(u => u.WEIGHT);
                            lblWeight.Text = string.Format("{0}%", Math.Round(total, 2).ToString());
                            totalPerspective += total;
                        }
                    }
                    prevPerspective = nxtPerspective;
                }
                Literal lblTotalPWeight = lstData.Items[lstData.Items.Count - 1].FindControl("lblWeight") as Literal;
                lblTotalPWeight.Text = string.Format("{0}%", Math.Round(totalPerspective, 2));
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void loadData()
        {
            try
            {
                var objectives = scoresManager.getScoresReport(AreaId, AreaValueId, Date);
                var finalScore = objectives.Sum(u => u.FINAL_SCORE);
                var finalWeightTotal = objectives.Sum(u => u.WEIGHT);
                //var finalPercentage = (finalScore / finalWeightTotal) * 100;

                decimal agreedFinalScore = (decimal)objectives.Sum(u => u.FINAL_SCORE_AGREED);

                scores totalDetails = new scores
                {
                    PERSPECTIVE_NAME = "<b>Totals</b> ",
                    WEIGHT = finalWeightTotal,
                    FINAL_SCORE = finalScore,
                    FINAL_SCORE_AGREED = Math.Round(agreedFinalScore, 2, MidpointRounding.AwayFromZero),
                    VISIBLE = false
                };
                objectives.Add(totalDetails);
                BindControl.BindListView(lstData, objectives);
                trimData(objectives);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        #endregion

        #region Button Event Handling Methods

        protected void lnkExcel_Click(object sender, EventArgs e)
        {
            Download();
        }

        DataTable GetExportSchema()
        {
            DataTable dT = new DataTable();
            dT.Columns.Add("Perspective", typeof(string));
            dT.Columns.Add("Weight", typeof(string));
            dT.Columns.Add("Objective", typeof(string));
            if (!Util.IsStandardVersion) dT.Columns.Add("Rationale", typeof(string));
            dT.Columns.Add("Goal", typeof(string));
            dT.Columns.Add("Unit", typeof(string));
            dT.Columns.Add("GoalWeight", typeof(string));
            dT.Columns.Add("ActualScore", typeof(string));
            dT.Columns.Add("ManagerScore", typeof(string));
            dT.Columns.Add("AgreedScore", typeof(string));
            dT.Columns.Add("Target", typeof(string));
            dT.Columns.Add("FinalScore", typeof(string));
            dT.Columns.Add("CompanyRating", typeof(string));
            dT.Columns.Add("WeightedDefinedScore", typeof(string));
            dT.Columns.Add("JustificationComment", typeof(string));
            if (!Util.IsStandardVersion) dT.Columns.Add("Evidence", typeof(string));
            dT.Columns.Add("ManagerComment", typeof(string));
            dT.AcceptChanges();
            return dT;
        }

        private void Download()
        {
            if (lstData.Items.Count <= 0)
            {
                lblError.Text = "There are no records to export to excel.";
                return;
            }

            DataTable dT = GetExportSchema();
            int[] cols = !Util.IsStandardVersion ? new int[17] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 } :
                                                   new int[15] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 };
            string[] headers = !Util.IsStandardVersion ? new string[17] { "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "" } :
                                                        new string[15] { "", "", "", "", "", "", "", "", "", "", "", "", "", "", "" };

            DataRow nRow = dT.NewRow();
            nRow["Perspective"] = "Area".ToUpper();
            nRow["Weight"] = lblCriteria.Text;
            dT.Rows.Add(nRow);
            dT.Rows.Add(dT.NewRow());//add blank row
            nRow = null;

            nRow = dT.NewRow();
            nRow["Perspective"] = "Name".ToUpper();
            nRow["Weight"] = lblName.Text;
            dT.Rows.Add(nRow);
            dT.Rows.Add(dT.NewRow());//add blank row
            nRow = null;

            nRow = dT.NewRow();
            nRow["Perspective"] = "Report Date".ToUpper();
            nRow["Weight"] = lblDate.Text;
            dT.Rows.Add(nRow);
            dT.Rows.Add(dT.NewRow());//add blank row
            nRow = null;

            nRow = dT.NewRow();
            nRow["Perspective"] = "Key Focus Area".ToUpper();
            nRow["Weight"] = "Key Focus Area Weight".ToUpper();
            nRow["Objective"] = "Strategic Objective".ToUpper();
            nRow["Goal"] = "Performance Measure".ToUpper();
            nRow["Unit"] = "Unit of Measure".ToUpper();
            nRow["GoalWeight"] = "Goal Weight".ToUpper();
            nRow["ActualScore"] = "Employee Score".ToUpper();
            nRow["ManagerScore"] = "Manager Score".ToUpper();
            nRow["AgreedScore"] = "Final Score".ToUpper();
            nRow["Target"] = "Target".ToUpper();
            nRow["FinalScore"] = "Final Score (%)".ToUpper();
            nRow["CompanyRating"] = "Company Defined Target".ToUpper();
            nRow["WeightedDefinedScore"] = "Company Defined Weighted Score".ToUpper();
            nRow["JustificationComment"] = "Justification Comment".ToUpper();
            nRow["ManagerComment"] = "Manager Comment".ToUpper();

            if (!Util.IsStandardVersion) nRow["Evidence"] = "Evidence".ToUpper();
            if (!Util.IsStandardVersion) nRow["Rationale"] = "Rationale".ToUpper();

            dT.Rows.Add(nRow);
            nRow = null;

            foreach (ListViewItem item in lstData.Items)
            {
                string weight = (item.FindControl("lblWeight") as Literal).Text;
                string strPerspective = (item.FindControl("lblPerspective") as Literal).Text;
                strPerspective = strPerspective.Replace("</b>", "").Replace("<b>", "");

                nRow = dT.NewRow();
                nRow["Perspective"] = strPerspective;
                nRow["Weight"] = weight;
                nRow["Objective"] = (item.FindControl("lblObjective") as Literal).Text;

                if (!Util.IsStandardVersion) nRow["Rationale"] = (item.FindControl("hdnRationale") as HiddenField).Value;
                if (!Util.IsStandardVersion) nRow["Evidence"] = (item.FindControl("hdnEvidence") as HiddenField).Value;

                nRow["Goal"] = (item.FindControl("lblGoalDesc") as Literal).Text;
                nRow["Unit"] = (item.FindControl("lblUnitMeasure") as Literal).Text;
                nRow["GoalWeight"] = string.Format("{0}%", (item.FindControl("lblGoalWeight") as Literal).Text);
                nRow["ActualScore"] = (item.FindControl("lblGoalScore") as Literal).Text;
                nRow["Target"] = (item.FindControl("lblTarget") as Literal).Text;
                nRow["FinalScore"] = string.Format("{0}%", (item.FindControl("lblFinalScore") as Literal).Text);
                nRow["ManagerScore"] = (item.FindControl("lblManagerScore") as Literal).Text;
                nRow["AgreedScore"] = (item.FindControl("lblAgreedScore") as Literal).Text;
                nRow["JustificationComment"] = (item.FindControl("hdnJustComment") as HiddenField).Value;
                nRow["ManagerComment"] = (item.FindControl("hdnMngComment") as HiddenField).Value;
                nRow["CompanyRating"] = (item.FindControl("lblRatingValue") as Literal).Text;
                nRow["WeightedDefinedScore"] = (item.FindControl("lblDefinedWeightedScore") as Literal).Text;
                dT.Rows.Add(nRow);
                nRow = null;
            }

            dT.AcceptChanges();
            Export exp = new Export("Web");
            exp.ExportDetails(dT, cols, headers, Export.ExportFormat.Excel, Export.ExportAction.DownloadOnly, string.Format("{0}.xls", lblName.Text));
        }

        #endregion
    }
}