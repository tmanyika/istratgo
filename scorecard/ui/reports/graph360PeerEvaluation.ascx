﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="graph360PeerEvaluation.ascx.cs"
    Inherits="scorecard.ui.reports.graph360PeerEvaluation" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Charting" TagPrefix="telerik" %>
<%@ Register Src="scoreCardAreaDefinition.ascx" TagName="scoreCardAreaDefinition"
    TagPrefix="uc1" %>
<div style="background-color: White; padding-left: 200px;">
    <uc1:scoreCardAreaDefinition ID="scoreCardAreaDefinition1" runat="server" />
</div>
<div>
    <br />
</div>
<div style="padding-left: 50px;">
    <telerik:RadChart ID="radChart" runat="server" AutoTextWrap="True" Skin="Mac" AutoLayout="True"
        Height="500px" Width="800px">
        <Appearance Corners="Round, Round, Round, Round, 6">
            <FillStyle FillType="Image">
                <FillSettings BackgroundImage="{chart}" ImageDrawMode="Flip" ImageFlip="FlipX">
                </FillSettings>
            </FillStyle>
            <Border Color="138, 138, 138" />
        </Appearance>
        <Series>
            <telerik:ChartSeries Name="Self" DataYColumn="SelfRating">
                <Appearance>
                    <FillStyle MainColor="55, 167, 226" SecondColor="22, 85, 161">
                        <FillSettings GradientMode="Vertical">
                        </FillSettings>
                    </FillStyle>
                    <TextAppearance TextProperties-Color="Black">
                    </TextAppearance>
                </Appearance>
            </telerik:ChartSeries>
            <telerik:ChartSeries Name="Manager" DataYColumn="ManagerRating">
                <Appearance>
                    <FillStyle MainColor="Orange" SecondColor="200, 38, 37">
                        <FillSettings GradientMode="Vertical">
                        </FillSettings>
                    </FillStyle>
                    <TextAppearance TextProperties-Color="Black">
                    </TextAppearance>
                </Appearance>
            </telerik:ChartSeries>
            <telerik:ChartSeries Name="Peer Rating" DataYColumn="PeerRating">
                <Appearance>
                    <FillStyle MainColor="Green" SecondColor="223, 87, 60">
                        <FillSettings GradientMode="Vertical">
                        </FillSettings>
                    </FillStyle>
                    <TextAppearance TextProperties-Color="Black">
                    </TextAppearance>
                </Appearance>
            </telerik:ChartSeries>
            <telerik:ChartSeries Name="Direct Report" DataYColumn="DirectReportRating">
                <Appearance>
                    <FillStyle MainColor="Navy" SecondColor="200, 38, 37">
                        <FillSettings GradientMode="Vertical">
                        </FillSettings>
                    </FillStyle>
                    <TextAppearance TextProperties-Color="Black">
                    </TextAppearance>
                </Appearance>
                <Appearance>
                    <FillStyle MainColor="Red" SecondColor="200, 38, 37">
                        <FillSettings GradientMode="Vertical">
                        </FillSettings>
                    </FillStyle>
                    <TextAppearance TextProperties-Color="Black">
                    </TextAppearance>
                </Appearance>
            </telerik:ChartSeries>
        </Series>
        <Legend>
            <Appearance Dimensions-Margins="15.4%, 3%, 1px, 1px" Position-AlignedPosition="TopRight">
                <ItemMarkerAppearance Figure="Square">
                    <Border Color="134, 134, 134" />
                </ItemMarkerAppearance>
                <FillStyle MainColor="">
                </FillStyle>
                <Border Color="Transparent" />
            </Appearance>
        </Legend>
        <PlotArea>
            <XAxis DataLabelsColumn="CompetencyName" Appearance-LabelAppearance-RotationAngle="-90">
                <Appearance Color="134, 134, 134" MajorTick-Color="134, 134, 134">
                    <MajorGridLines Color="209, 222, 227" PenStyle="Solid" />
                    <LabelAppearance RotationAngle="-90">
                    </LabelAppearance>
                    <TextAppearance TextProperties-Color="51, 51, 51">
                    </TextAppearance>
                </Appearance>
                <AxisLabel>
                    <TextBlock>
                        <Appearance TextProperties-Color="51, 51, 51">
                        </Appearance>
                    </TextBlock>
                </AxisLabel>
                <Items>
                    <telerik:ChartAxisItem>
                    </telerik:ChartAxisItem>
                    <telerik:ChartAxisItem Value="1">
                    </telerik:ChartAxisItem>
                    <telerik:ChartAxisItem Value="2">
                    </telerik:ChartAxisItem>
                    <telerik:ChartAxisItem Value="3">
                    </telerik:ChartAxisItem>
                    <telerik:ChartAxisItem Value="4">
                    </telerik:ChartAxisItem>
                    <telerik:ChartAxisItem Value="5">
                    </telerik:ChartAxisItem>
                    <telerik:ChartAxisItem Value="6">
                    </telerik:ChartAxisItem>
                    <telerik:ChartAxisItem Value="7">
                    </telerik:ChartAxisItem>
                </Items>
            </XAxis>
            <YAxis AxisMode="Extended" AutoScale="true">
                <Appearance Color="134, 134, 134" MajorTick-Color="134, 134, 134" MinorTick-Color="134, 134, 134"
                    MinorTick-Width="0">
                    <MajorGridLines Color="209, 222, 227" />
                    <MinorGridLines Color="233, 239, 241" />
                    <TextAppearance TextProperties-Color="51, 51, 51">
                    </TextAppearance>
                </Appearance>
                <AxisLabel>
                    <TextBlock>
                        <Appearance TextProperties-Color="51, 51, 51">
                        </Appearance>
                    </TextBlock>
                </AxisLabel>
            </YAxis>
            <Appearance>
                <FillStyle FillType="Solid" MainColor="White">
                </FillStyle>
                <Border Color="134, 134, 134" />
            </Appearance>
        </PlotArea>
        <ChartTitle>
            <Appearance Position-AlignedPosition="Top">
                <FillStyle MainColor="">
                </FillStyle>
            </Appearance>
            <TextBlock Appearance-AutoTextWrap="False" Text="360 Degree Evaluation" Appearance-TextProperties-Color="Blue">
                <Appearance AutoTextWrap="False" TextProperties-Font="Tahoma, 13pt">
                </Appearance>
            </TextBlock>
        </ChartTitle>
    </telerik:RadChart>
    <b class="dashboardred">
        <asp:Literal ID="LblNoDataMsg" runat="server" Text="There are no records for the criteria specified."></asp:Literal></b>
</div>
