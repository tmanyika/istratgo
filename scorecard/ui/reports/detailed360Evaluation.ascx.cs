﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ThreeSixty.Evaluation;
using ScoreCard.Common.Config;
using scorecard.implementations;
using HR.Human.Resources;
using ScoreCard.Common.Interfaces;
using scorecard.controllers;
using ScoreCard.Common.Logic;
using vbs = Microsoft.VisualBasic;
using vb = Microsoft.VisualBasic.Information;
using scorecard.entities;
using System.Data;
using RKLib.ExportData;

namespace scorecard.ui.reports
{
    public partial class detailed360Evaluation : System.Web.UI.UserControl
    {

        #region Properties

        string Date
        {
            get
            {
                return Request["dVal"].ToString();
            }
        }

        int AreaId
        {
            get
            {
                return int.Parse(Request["aid"].ToString());
            }
        }

        int AreaValueId
        {
            get
            {
                return int.Parse(Request["idv"].ToString());
            }
        }

        DateTime PeriodDate
        {
            get
            {
                return DateTime.Parse(Request["dVal"].ToString());
            }
        }

        bool IsInternal
        {
            get
            {
                return bool.Parse(Request["isIn"].ToString());
            }
        }

        #endregion

        #region Member Variables

        IEmployee emp;
        IScoreCardArea scoreArea;

        projectmanagement proj;
        structurecontroller orgunit;
        IEvaluationReport report;

        #endregion

        #region Default Constructors

        public detailed360Evaluation()
        {
            report = new EvaluationReportBL();
            emp = new EmployeeBL();
            proj = new projectmanagement(new projectImpl());
            orgunit = new structurecontroller(new structureImpl());
            scoreArea = new ScoreCardAreaBL();
        }

        #endregion

        #region Page Event Handling Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillAreaInfo();
                BindData();
            }
        }

        #endregion

        #region Databinding

        void BindData()
        {
            try
            {
                List<EvaluationReport> data = report.Get360DetailedReport(AreaId, AreaValueId, PeriodDate, IsInternal);
                var repData = report.GetDetailedData(data);
                BindControl.BindGridViewNormal(gdvData, repData);
                BindControl.BindLiteral(LblNoDataMsg, repData.Rows.Count > 0 ? string.Empty : Messages.NoRecordsToDisplay);
                lnkExcel.Visible = (repData.Rows.Count > 0);
                Cache["Data"] = repData;
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(LblNoDataMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        string GetItemName(int areaValueId, int criteriaId)
        {
            string itemName = string.Empty;

            if (criteriaId > 0)
            {
                if (criteriaId == ScoreCardAreaConfig.JobTitleAreaId ||
                    criteriaId == ScoreCardAreaConfig.EmployeeAreaId)
                {
                    var data = emp.GetById(areaValueId);
                    itemName = data.FullName;
                }
                else if (criteriaId == ScoreCardAreaConfig.ProjectAreaId)
                {
                    var project = proj.get(areaValueId);
                    itemName = project.PROJECT_NAME;
                }
                else if (criteriaId == ScoreCardAreaConfig.OrgUnitAreaId)
                {
                    var orgData = orgunit.getOrgunitDetails(new Orgunit { ID = areaValueId });
                    itemName = orgData.ORGUNIT_NAME;
                }
            }
            return itemName;
        }

        string GetAreaName(int areaId)
        {
            var data = scoreArea.GetById(areaId);
            return data.AreaAliasName;
        }

        void FillAreaInfo()
        {
            try
            {
                string areaName = GetAreaName(AreaId);
                string itemName = GetItemName(AreaValueId, AreaId);
                string fullDateName = vb.IsDate(Date) ? Common.GetDescriptiveDate(Date, false) : Date;

                lblCriteria.Text = areaName;
                lblName.Text = itemName;
                lblDate.Text = fullDateName;
                lblReportType.Text = string.Format("Detailed {0} Report", IsInternal ? "Internal" : "External");
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                BindControl.BindLiteral(lblError, Messages.GetErrorMessage());
            }
        }

        void ExportToExcel()
        {
            var dS = Cache["Data"] as DataTable;
            DataTable dT = dS.Clone();

            DataRow nRow = dT.NewRow();
            nRow[0] = "Area".ToUpper();
            nRow[1] = lblCriteria.Text;
            dT.Rows.Add(nRow);
            dT.Rows.Add(dT.NewRow());//add blank row
            nRow = null;

            nRow = dT.NewRow();
            nRow[0] = "Name".ToUpper();
            nRow[1] = lblName.Text;
            dT.Rows.Add(nRow);
            dT.Rows.Add(dT.NewRow());//add blank row
            nRow = null;

            nRow = dT.NewRow();
            nRow[0] = "Report Date".ToUpper();
            nRow[1] = lblDate.Text;
            dT.Rows.Add(nRow);
            dT.Rows.Add(dT.NewRow());//add blank row
            nRow = null;

            nRow = dT.NewRow();
            nRow[0] = "Report Type".ToUpper();
            nRow[1] = lblReportType.Text;
            dT.Rows.Add(nRow);
            dT.Rows.Add(dT.NewRow());//add blank row
            nRow = null;

            //Add headers to the report
            int colIdx = 0;
            nRow = dT.NewRow();
            foreach (DataColumn col in dT.Columns)
            {
                nRow[colIdx] = col.ColumnName;
                colIdx += 1;
            }
            dT.Rows.Add(nRow);
            nRow = null;

            foreach (DataRow rw in dS.Rows)
            {
                dT.ImportRow(rw);
            }

            dT.AcceptChanges();

            int len = dT.Columns.Count;
            int[] cols = new int[len];
            string[] headers = new string[len];

            for (int i = 0; i < len; i++)
            {
                cols[i] = i;
                headers[i] = string.Empty;
            }

            Export exp = new Export("Web");
            exp.ExportDetails(dT, cols, headers, Export.ExportFormat.Excel, string.Format("{0}-{1}.xls", lblName.Text.Replace(" ", ""), lblDate.Text));
        }
        #endregion

        #region Button Event Handling Methods

        protected void lnkExcel_Click(object sender, EventArgs e)
        {
            ExportToExcel();
        }


        #endregion

    }
}