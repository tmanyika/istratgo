﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.implementations;
using ScoreCard.Common.Config;
using HR.Human.Resources;
using scorecard.controllers;
using scorecard.entities;
using ScoreCard.Common.Interfaces;
using ScoreCard.Common.Logic;
using vb = Microsoft.VisualBasic.Information;

namespace scorecard.ui.reports
{
    public partial class scoreCardAreaDefinition : System.Web.UI.UserControl
    {
        #region Properties

        string Date
        {
            get
            {
                return Request["dVal"].ToString();
            }
        }

        int AreaId
        {
            get
            {
                return int.Parse(Request["aid"].ToString());
            }
        }

        int AreaValueId
        {
            get
            {
                return int.Parse(Request["idv"].ToString());
            }
        }

        #endregion

        #region Member Variables

        IEmployee emp;
        IScoreCardArea scoreArea;

        projectmanagement proj;
        structurecontroller orgunit;

        #endregion

        #region Default Constructors

        public scoreCardAreaDefinition()
        {
            emp = new EmployeeBL();
            proj = new projectmanagement(new projectImpl());
            orgunit = new structurecontroller(new structureImpl());
            scoreArea = new ScoreCardAreaBL();
        }

        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillAreaInfo();
            }
        }

        #endregion

        #region Databinding Methods

        string GetItemName(int areaValueId, int criteriaId)
        {
            string itemName = string.Empty;

            if (criteriaId > 0)
            {
                if (criteriaId == ScoreCardAreaConfig.JobTitleAreaId ||
                    criteriaId == ScoreCardAreaConfig.EmployeeAreaId)
                {
                    var data = emp.GetById(areaValueId);
                    itemName = data.FullName;
                }
                else if (criteriaId == ScoreCardAreaConfig.ProjectAreaId)
                {
                    var project = proj.get(areaValueId);
                    itemName = project.PROJECT_NAME;
                }
                else if (criteriaId == ScoreCardAreaConfig.OrgUnitAreaId)
                {
                    var orgData = orgunit.getOrgunitDetails(new Orgunit
                                                                {
                                                                    ID = areaValueId
                                                                }
                                                           );
                    itemName = orgData.ORGUNIT_NAME;
                }
            }
            return itemName;
        }

        string GetAreaName(int areaId)
        {
            var data = scoreArea.GetById(areaId);
            return data.AreaAliasName;
        }

        void FillAreaInfo()
        {
            try
            {
                string areaName = GetAreaName(AreaId);
                string itemName = GetItemName(AreaValueId, AreaId);
                string fullDateName = vb.IsDate(Date) ? Common.GetDescriptiveDate(Date, false) : Date;

                lblCriteria.Text = areaName;
                lblName.Text = itemName;
                lblDate.Text = fullDateName;
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                BindControl.BindLiteral(lblError, Messages.GetErrorMessage());
            }
        }

        #endregion
    }
}