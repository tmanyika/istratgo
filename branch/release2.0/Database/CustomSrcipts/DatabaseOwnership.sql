-- Query to get the user associated Database Role
--select DBPrincipal_2.name as role, DBPrincipal_1.name as owner 
--from sys.database_principals as DBPrincipal_1 inner join sys.database_principals as DBPrincipal_2 
--on DBPrincipal_1.principal_id = DBPrincipal_2.owning_principal_id 
--where DBPrincipal_1.name = 'imd_user_db'

use ScoreCard;

go
ALTER AUTHORIZATION ON ROLE::[db_owner] TO [dbo]
ALTER AUTHORIZATION ON SCHEMA::[imd_user_db] TO [dbo];
GO
go