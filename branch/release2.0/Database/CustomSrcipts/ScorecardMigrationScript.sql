use ScoreCard;

set nocount on;
set xact_abort on;
set transaction isolation level read uncommitted;

declare @i int, @rec int, @iL int, @recL int,
		@name varchar(50), 
		@empId int, @fName varchar(25),
		@mName varchar(25),
		@lName varchar(25);

begin try
	begin transaction
		set identity_insert imd_user_db.Employee on

		insert into imd_user_db.Employee(EmployeeId, LineManagerEmployeeId, OrgUnitId, EmailAddress, JobTitleId, DateCreated)
		select	ID AS EmployeeId, 
				REPORTS_TO AS LineManagerEmployeeId, 
				ORG_ID AS OrgUnitId,
				EMAIL_ADDRESS AS emailAddress, 
				JOB_TITLE_ID AS JobTitleId, 
				DATE_CREATED AS datecreated
		from imd_user_db.USER_ADMIN

		insert into imd_user_db.EmployeeLogin(EmployeeId, UserName, Pass, RoleId, Active)
		select ID,LOGIN_ID,[PASSWORD],USER_TYPE,ACTIVE from imd_user_db.USER_ADMIN

		set identity_insert imd_user_db.Employee off

		select id, name, row_number() over (order by id asc) as recId
		into #data
		from imd_user_db.USER_ADMIN

		set @i = 1;
		select @rec=count(id) from imd_user_db.USER_ADMIN

		while(@i <= @rec)
			begin
				select @name=name,@empId=id from #data where @i = recId
				select dataVal, Id, orderId
				into #name
				from imd_user_db.[fxSplit](cast(@empId as varchar(12)),@name,' ')
				
				set @iL = 1;
				set @lName = ''
				set @mName = ''
				set @fName = ''
				
				select @recL=count(Id) from #name
				while(@iL <= @recL)
					begin
						if (@iL = 1)
							select @fName=dataVal from #name where orderId = 1				
						if(@iL = 2)
							select @mName=dataVal from #name where orderId = 2
						if(@iL = 3)
							select @lName=dataVal from #name where orderId = 3					
						set @iL = @iL + 1;
					end		
					if (@recL < 3)
					   begin
							set @lName = @mName
							set @mName = ''
					   end
				
				update imd_user_db.Employee
				set FirstName=@fName, MiddleName = @mName, LastName = @lName
				where @empId = EmployeeId
				
				drop table #name
				set @i = @i + 1;
			end

		alter table imd_user_db.USER_ADMIN
		alter column ID int not null
	commit transaction
	print 'Done'
end try	
begin catch
	if(@@trancount > 0)
		rollback transaction
	insert into imd_user_db(ErrorNo, ErrorSeverity, ErrorState, ErrorProcedure, ErrorLine, ErrorMessage)
	select ERROR_NUMBER() AS ErrorNumber,
		ERROR_SEVERITY() AS ErrorSeverity,
		ERROR_STATE() AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE() AS ErrorLine,
		ERROR_MESSAGE() AS ErrorMessage
end catch 
print 'Finished Execution'

--select * from imd_user_db.[fxSplit](cast(12 as varchar(12)),'Shungu G. Mkhonza',' ')
--order by orderId asc

--select * from employee

--select * from employeelogin