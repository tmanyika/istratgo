-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_MailBatch_Insert]
	@accountMailId int, 
	@mailSubject varchar(800),
    @mailContent nvarchar(max), 
    @mailId int, 
    @noOfUsers int = 0, 
    @dateToBeSent datetime, 
    @sendNow bit,
    @Xml xml, 
    @createdBy varchar(20)
AS
begin
	set nocount on;
	set transaction isolation level read uncommitted;
	
	declare @mainTemplate nvarchar(max), @sent bit;
	declare @todayDate datetime, @batchId int;
	
	set @sent = 0;
	set @todayDate = getdate();
	
	select nref.value('Id[1]', 'int') as roleId
	into #Rxml
	from  @Xml.nodes('//Role/Data') AS R(nref)
	
	select @mainTemplate=MainTemplate from 	imd_user_db.MailTemplate where MailId = @mailId
	
	begin try
		begin transaction		
			insert into imd_user_db.MailBatch(AccountMailId, MailSubject, MailContent, MainTemplate, NoOfUsers, IsSent, DateToBeSent, CreatedBy, DateCreated)
			values(@batchId, roleId, @accountMailId, @mailSubject, @mailContent, @mainTemplate, @noOfUsers, 0, @dateToBeSent, @createdBy, @todayDate)
			select @batchId = scope_identity()
			
			insert into imd_user_db.MailBatchUserRole(BatchId, RoleId)
			select @batchId, roleId from #Rxml
			
			set @sent = 1			
		commit transaction
	end try
	begin catch
		rollback transaction
		set @sent = 0
	end catch
	
	
	if (@sendNow = 1)
		begin
			declare @fullName varchar(50), @email varchar(50), @subject varchar(800), @emailBody nvarchar(max);
			declare @sqlProfile varchar(100), 
			
			select l.UserName, e.FirstName + ' ' + isnull(e.MiddleName,'') + ' ' + e.LastName as fullName, EmailAddress from imd_user_db.Employee as e inner join imd_user_db.EmployeeLogin as l on e.EmployeeId = l.EmployeeId 
			into #UList
			where l.RoleId in (select roleId from #Rxml)
			
			select from imd_user_db.MailBatch as m cross join #UList as u where BatchId = @batchId
			
			drop table #UList
		end	
end
