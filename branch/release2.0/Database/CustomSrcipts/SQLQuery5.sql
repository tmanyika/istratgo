-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE imd_user_db.Proc_Leave_GetLeaveBalances
	@orgUnitId int,
	@LeaveXml xml
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	select	nref.value('Id[1]', 'int') as leaveTypeId
	into #lType
	from   @LeaveXml.nodes('//Data/Leave') AS R(nref)
	
	select   e.FirstName + ' ' + isnull(e.MiddleName,'') + ' ' + e.LastName as FullName, lt.LeaveName, lb.Accumulated, lb.DaysTaken, lb.Balance, e.FirstName, e.MiddleName, e.LastName,  e.EmailAddress, e.EmployeeNo, lb.LeaveId,   
                lb.LimitValue
	from        imd_user_db.Employee AS e LEFT OUTER JOIN
				imd_user_db.LeaveBalance AS lb ON e.EmployeeId = lb.EmployeeId INNER JOIN
				imd_user_db.LeaveType AS lt ON lb.LeaveId = lt.LeaveId
	where     (e.OrgUnitId = @orgUnitId) AND (lb.LeaveId IN (select leaveTypeId from #lType))
	order by e.LastName, e.FirstName, lt.LeaveName
	
	drop table #lType
END
GO
