USE msdb; 

GRANT EXECUTE ON OBJECT::dbo.sysmail_add_principalprofile_sp TO imd_user_db;
GRANT EXECUTE ON OBJECT::dbo.sysmail_add_profileaccount_sp TO imd_user_db;
GRANT EXECUTE ON OBJECT::dbo.sysmail_add_profile_sp TO imd_user_db;
GRANT EXECUTE ON OBJECT::dbo.sysmail_add_account_sp TO imd_user_db;
GRANT EXECUTE ON OBJECT::dbo.sysmail_update_profile_sp TO imd_user_db;  
GRANT EXECUTE ON OBJECT::dbo.sysmail_update_account_sp TO imd_user_db;  
GRANT EXECUTE ON OBJECT::dbo.sysmail_update_principalprofile_sp TO imd_user_db;  
GRANT EXECUTE ON OBJECT::dbo.sysmail_update_profileaccount_sp TO imd_user_db;  

GRANT SELECT, UPDATE, INSERT ON dbo.sysmail_profile TO imd_user_db;  
GRANT SELECT, UPDATE, INSERT ON dbo.sysmail_account TO imd_user_db; 
GRANT SELECT, UPDATE, INSERT ON dbo.sysmail_profileaccount TO imd_user_db; 
GRANT SELECT, UPDATE, INSERT ON dbo.sysmail_principalprofile TO imd_user_db; 
GRANT SELECT, UPDATE, INSERT ON dbo.sysmail_attachments TO imd_user_db; 


GO 