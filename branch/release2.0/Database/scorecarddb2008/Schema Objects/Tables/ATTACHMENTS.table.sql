﻿CREATE TABLE [imd_user_db].[ATTACHMENTS] (
    [ID]              INT           IDENTITY (1, 1) NOT NULL,
    [ATTACHMENT_NAME] VARCHAR (450) COLLATE Latin1_General_CI_AS NULL,
    [SUBMISSION_ID]   INT           NULL
);

