﻿CREATE TABLE [imd_user_db].[Employee] (
    [EmployeeId]            INT          IDENTITY (1, 1) NOT NULL,
    [LineManagerEmployeeId] INT          NULL,
    [OrgUnitId]             INT          NOT NULL,
    [FirstName]             VARCHAR (25) NULL,
    [MiddleName]            VARCHAR (25) NULL,
    [LastName]              VARCHAR (25) NULL,
    [EmailAddress]          VARCHAR (80) NOT NULL,
    [JobTitleId]            INT          NULL,
    [StatusId]              INT          NULL,
    [EmployeeNo]            VARCHAR (15) NULL,
    [WorkingWeekId]         INT          NULL,
    [Cellphone]             VARCHAR (15) NULL,
    [BusTelephone]          VARCHAR (15) NULL,
    [CreatedBy]             VARCHAR (20) NULL,
    [DateCreated]           DATETIME     NOT NULL,
    [DateUpdated]           DATETIME     NULL,
    [UpdatedBy]             VARCHAR (20) NULL
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Keeps all employee records', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'Employee';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Assist in getting leave accrual rate.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'Employee', @level2type = N'COLUMN', @level2name = N'WorkingWeekId';

