﻿CREATE TABLE [imd_user_db].[Package] (
    [PackageId]   INT             IDENTITY (1, 1) NOT NULL,
    [PackageName] VARCHAR (200)   NOT NULL,
    [Price]       DECIMAL (18, 2) NOT NULL,
    [Active]      BIT             NOT NULL,
    [CreatedBy]   VARCHAR (50)    NOT NULL,
    [DateCreated] DATETIME        NOT NULL,
    [UpdatedBy]   VARCHAR (50)    NULL,
    [DateUpdated] DATETIME        NULL
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Stores Packages offered by the system', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'Package';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Uniquely identifies a package.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'Package', @level2type = N'COLUMN', @level2name = N'PackageId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Package name', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'Package', @level2type = N'COLUMN', @level2name = N'PackageName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Indicates if a package is still active or not. 1= Active, 0 = Inactive', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'Package', @level2type = N'COLUMN', @level2name = N'Active';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The user name of the person who created the record.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'Package', @level2type = N'COLUMN', @level2name = N'CreatedBy';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date when the record was created.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'Package', @level2type = N'COLUMN', @level2name = N'DateCreated';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The user name of the person who last updated the record.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'Package', @level2type = N'COLUMN', @level2name = N'UpdatedBy';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date when the record was last updated.', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'TABLE', @level1name = N'Package', @level2type = N'COLUMN', @level2name = N'DateUpdated';

