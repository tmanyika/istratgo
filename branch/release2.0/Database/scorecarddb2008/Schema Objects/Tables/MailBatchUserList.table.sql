﻿CREATE TABLE [imd_user_db].[MailBatchUserList] (
    [BatchId]    INT          NOT NULL,
    [EmployeeId] INT          NOT NULL,
    [Email]      VARCHAR (80) NOT NULL,
    [Exported]   BIT          NOT NULL
);

