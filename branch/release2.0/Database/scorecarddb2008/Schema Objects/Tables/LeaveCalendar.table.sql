﻿CREATE TABLE [imd_user_db].[LeaveCalendar] (
    [HolidayDateId] BIGINT        IDENTITY (1, 1) NOT NULL,
    [CountryId]     INT           NOT NULL,
    [HolidayDate]   DATETIME      NOT NULL,
    [Detail]        VARCHAR (100) NOT NULL,
    [CalendarYear]  INT           NOT NULL,
    [Active]        BIT           NOT NULL,
    [CreatedBy]     VARCHAR (20)  NOT NULL,
    [DateCreated]   DATETIME      NOT NULL,
    [DateUpdated]   DATETIME      NULL,
    [UpdatedBy]     VARCHAR (20)  NULL
);

