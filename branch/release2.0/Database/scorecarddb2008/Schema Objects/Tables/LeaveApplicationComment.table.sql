﻿CREATE TABLE [imd_user_db].[LeaveApplicationComment] (
    [CommentId]     BIGINT         IDENTITY (1, 1) NOT NULL,
    [ApplicationId] INT            NOT NULL,
    [DocumentTitle] VARCHAR (200)  NULL,
    [Comment]       VARCHAR (1000) NOT NULL,
    [SupportingDoc] VARCHAR (200)  NULL,
    [Valid]         BIT            NOT NULL,
    [UploadedBy]    VARCHAR (20)   NOT NULL,
    [DateCreated]   DATETIME       NOT NULL,
    [UpdatedBy]     VARCHAR (20)   NULL,
    [DateUpdated]   DATETIME       NULL
);

