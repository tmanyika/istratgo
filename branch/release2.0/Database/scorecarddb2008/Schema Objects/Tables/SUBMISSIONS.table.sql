﻿CREATE TABLE [imd_user_db].[SUBMISSIONS] (
    [ID]                  INT      IDENTITY (1, 1) NOT NULL,
    [SELECTED_ITEM_VALUE] INT      NULL,
    [CRITERIA_TYPE_ID]    INT      NULL,
    [APPROVER_ID]         INT      NULL,
    [STATUS]              INT      NOT NULL,
    [DATE_SUBMITTED]      DATETIME NULL
);

