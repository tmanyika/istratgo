﻿CREATE TABLE [imd_user_db].[EmployeeSession] (
    [SessionId]       UNIQUEIDENTIFIER NOT NULL,
    [EmployeeId]      INT              NULL,
    [IPAddress]       VARCHAR (25)     NULL,
    [Browser]         VARCHAR (25)     NULL,
    [Platform]        VARCHAR (25)     NULL,
    [StartDate]       DATETIME         NOT NULL,
    [AppVersion]      VARCHAR (10)     NULL,
    [AssemblyVersion] VARCHAR (10)     NULL,
    [EndDate]         DATETIME         NULL
);

