﻿CREATE TABLE [imd_user_db].[MENU_ACCESS] (
    [ID]           INT      IDENTITY (1, 1) NOT NULL,
    [MENU_ID]      INT      NOT NULL,
    [USER_TYPE_ID] INT      NOT NULL,
    [COMPANY_ID]   INT      NOT NULL,
    [DATE_CREATED] DATETIME NULL
);

