﻿CREATE TABLE [imd_user_db].[EmployeeStatus] (
    [StatusId]    INT          IDENTITY (1, 1) NOT NULL,
    [StatusName]  VARCHAR (80) NOT NULL,
    [Active]      BIT          NOT NULL,
    [CreatedBy]   VARCHAR (20) NOT NULL,
    [DateCreated] DATETIME     NOT NULL,
    [DateUpdated] DATETIME     NULL,
    [UpdatedBy]   VARCHAR (20) NULL
);

