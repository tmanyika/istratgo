﻿ALTER TABLE [imd_user_db].[Package]
    ADD CONSTRAINT [PK_Package] PRIMARY KEY CLUSTERED ([PackageId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);

