﻿ALTER TABLE [imd_user_db].[LeaveAccrual]
    ADD CONSTRAINT [FK_LeaveAccrual_LeaveAccrualTime] FOREIGN KEY ([AccrualTimeId]) REFERENCES [imd_user_db].[LeaveAccrualTime] ([AccrualTimeId]) ON DELETE NO ACTION ON UPDATE NO ACTION;

