﻿ALTER TABLE [imd_user_db].[PackageFeature]
    ADD CONSTRAINT [FK_PackageFeature_Package] FOREIGN KEY ([PackageId]) REFERENCES [imd_user_db].[Package] ([PackageId]) ON DELETE NO ACTION ON UPDATE NO ACTION;

