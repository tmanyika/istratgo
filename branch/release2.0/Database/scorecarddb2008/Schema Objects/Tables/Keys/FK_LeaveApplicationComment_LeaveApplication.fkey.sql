﻿ALTER TABLE [imd_user_db].[LeaveApplicationComment]
    ADD CONSTRAINT [FK_LeaveApplicationComment_LeaveApplication] FOREIGN KEY ([ApplicationId]) REFERENCES [imd_user_db].[LeaveApplication] ([ApplicationId]) ON DELETE NO ACTION ON UPDATE NO ACTION;

