﻿CREATE TABLE [imd_user_db].[PackageFeature] (
    [FeatureId]          INT             IDENTITY (1, 1) NOT NULL,
    [PackageId]          INT             NOT NULL,
    [FeatureDescription] VARCHAR (200)   NOT NULL,
    [CreatedBy]          VARCHAR (50)    NOT NULL,
    [LimitValue]         DECIMAL (18, 2) NULL,
    [UpdatedBy]          VARCHAR (50)    NULL,
    [DateCreated]        DATETIME        NOT NULL,
    [DateUpdated]        DATETIME        NOT NULL
);

