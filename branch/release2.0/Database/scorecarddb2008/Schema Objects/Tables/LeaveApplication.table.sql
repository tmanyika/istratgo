﻿CREATE TABLE [imd_user_db].[LeaveApplication] (
    [ApplicationId]        INT            IDENTITY (1, 1) NOT NULL,
    [EmployeeId]           INT            NOT NULL,
    [LeaveId]              INT            NOT NULL,
    [RejectReason]         VARCHAR (1000) NULL,
    [LeaveNotes]           VARCHAR (800)  NULL,
    [StatusId]             INT            NOT NULL,
    [StartDate]            DATETIME       NOT NULL,
    [EndDate]              DATETIME       NOT NULL,
    [NoOfLeaveDays]        INT            NOT NULL,
    [SubmittedEmployeeId]  INT            NOT NULL,
    [ApprovedByEmployeeId] INT            NULL,
    [DateCreated]          DATETIME       NOT NULL,
    [DateUpdated]          DATETIME       NULL
);

