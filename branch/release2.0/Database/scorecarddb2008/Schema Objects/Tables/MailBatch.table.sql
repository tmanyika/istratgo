﻿CREATE TABLE [imd_user_db].[MailBatch] (
    [BatchId]       INT            IDENTITY (1, 1) NOT NULL,
    [BatchName]     VARCHAR (200)  NOT NULL,
    [AccountMailId] INT            NOT NULL,
    [MailSubject]   VARCHAR (800)  NOT NULL,
    [MailContent]   NVARCHAR (MAX) NOT NULL,
    [MainTemplate]  NVARCHAR (MAX) NOT NULL,
    [NoOfUsers]     INT            NOT NULL,
    [IsSent]        BIT            NOT NULL,
    [DateToBeSent]  DATETIME       NOT NULL,
    [CreatedBy]     VARCHAR (20)   NOT NULL,
    [DateCreated]   DATETIME       NOT NULL
);

