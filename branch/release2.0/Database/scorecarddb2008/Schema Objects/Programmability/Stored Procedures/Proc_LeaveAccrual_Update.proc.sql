﻿


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_LeaveAccrual_Update]
	@leaveAccrualId int,
	@leaveId int, 
	@workingWeekId int, 
	@accrualTimeId int,
    @accrualRate decimal(18,4), 
    @maximumThreshold decimal(18,4), 
    @active bit, 
    @updatedBy varchar(20)
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	if not(exists(select leaveId from imd_user_db.LeaveAccrual where leaveAccrualId <> @leaveAccrualId and leaveId = @leaveId and workingWeekId = @workingWeekId))
		update imd_user_db.LeaveAccrual
		set   LeaveId = @leaveId, 
			  WorkingWeekId = @workingWeekId, 
			  AccrualTimeId = @accrualTimeId, 
			  AccrualRate = @accrualRate, 
			  MaximumThreshold = @maximumThreshold, 
			  Active = @active, 
			  UpdatedBy = @updatedBy, 
			  DateUpdated = GETDATE()
		where LeaveAccrualId = @leaveAccrualId
	select @@rowcount
END



