﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_Leave_GetEmployeesOnLeave]
	@orgUnitId int,
	@LeaveXml xml
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	declare @todayDate datetime;	
	set @todayDate = getdate();
	
	select	nref.value('Id[1]', 'int') as leaveTypeId
	into #eonLeave
	from   @LeaveXml.nodes('//Data/Leave') AS R(nref)
	
	select    e.LastName + ' ' + ISNULL(e.MiddleName, '') + ' ' + e.FirstName  AS FullName, e.EmployeeNo, e.EmailAddress, lt.LeaveName, la.NoOfLeaveDays, convert(varchar, la.StartDate, 106) as StartDate, convert(varchar, la.EndDate, 106) as EndDate, 
               ls.StatusName, lt.LeaveId
	from         imd_user_db.LeaveApplication AS la INNER JOIN
                      imd_user_db.LeaveType AS lt ON la.LeaveId = lt.LeaveId INNER JOIN
                      imd_user_db.Employee AS e ON la.EmployeeId = e.EmployeeId INNER JOIN
                      imd_user_db.LeaveStatus as ls ON la.StatusId = ls.StatusId
	where		(e.OrgUnitId = @orgUnitId) AND (ls.StatusId = 2) AND
				(lt.LeaveId IN (select leaveTypeId from #eonLeave)) AND
				(@todayDate BETWEEN la.StartDate AND la.EndDate)	
	order by e.LastName, e.FirstName, la.StartDate
	
	drop table #eonLeave
END


SELECT convert(varchar, getdate(), 106)