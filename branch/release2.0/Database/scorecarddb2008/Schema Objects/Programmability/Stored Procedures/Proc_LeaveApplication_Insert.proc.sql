﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_LeaveApplication_Insert]
	@EmployeeId int, 
	@LeaveId int,
    @LeaveNotes varchar(600), 
    @StatusId int, 
    @StartDate datetime, 
    @EndDate datetime, 
    @NoOfLeaveDays int,
    @SubmittedEmployeeId int,
    @SubXml xml = null
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	declare @appId int;
	set @appId = 0;
	
	begin try
		begin transaction
			insert into imd_user_db.LeaveApplication(EmployeeId, LeaveId, LeaveNotes, StatusId, StartDate, EndDate, NoOfLeaveDays, SubmittedEmployeeId, DateCreated)
			values (@EmployeeId, @LeaveId, @LeaveNotes, @StatusId, @StartDate, @EndDate, @NoOfLeaveDays, @SubmittedEmployeeId, getdate())
			select @appId = scope_identity()
			
			insert into imd_user_db.LeaveApplicationComment(ApplicationId, DocumentTitle, Comment, SupportingDoc, Valid, UploadedBy, DateCreated)
			select	@appId as applicationId, nref.value('DocumentTitle[1]', 'varchar(200)') as DocumentTitle, 
					nref.value('Comment[1]', 'varchar(1000)') as Comment,
					nref.value('SupportingDoc[1]', 'varchar(300)') as SupportingDoc,
					1 as Valid, nref.value('UploadedBy[1]', 'varchar(50)') as UploadedBy, getdate() as dateCreated
			from   @SubXml.nodes('//Data/Record') AS R(nref)
		commit transaction
	end try
	begin catch
		if(@@trancount > 0)
			rollback transaction
	end catch
	select @appId
END
