﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_WorkingWeek_Update]
	@workingWeekId int,
	@name varchar(200), 
	@noofdays int,
	@active bit, 
	@updatedBy varchar(20)
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	update  imd_user_db.WorkingWeek
	set     Name = @name, 
			NoOfDays = @noofdays,
			Active = @active, 
			UpdatedBy = @updatedBy, 
			DateUpdated = GETDATE()
	where   (WorkingWeekId = @workingWeekId)
		
	select @@rowcount
END

