﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_EmailRelay_GetByBatchId]
	@batchId int
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	open master key decryption by password = '3891@Macd'
	open symmetric key FieldSymmetricKey
	decryption by certificate FieldCertificate;
	
	declare @subject varchar(800), @mailContent nvarchar(max);
	
	select     m.MailSubject, replace(m.MainTemplate,'{1}',m.MailContent) as MailContent , s.UserName, imd_user_db.fxDecrypt(s.PIN) as PIN, s.SmtpServer, s.Port, s.AccountMailId, s.AccountName, s.SqlProfileName, s.Active, s.CreatedBy, 
                      s.DateUpdated, s.UpdatedBy, m.BatchId, m.BatchName, m.NoOfUsers, m.IsSent, m.DateToBeSent, m.DateCreated
	from         imd_user_db.MailBatch AS m inner join
                      imd_user_db.MailServer AS s ON m.AccountMailId = s.AccountMailId
	where (m.BatchId = @batchId)	
END
