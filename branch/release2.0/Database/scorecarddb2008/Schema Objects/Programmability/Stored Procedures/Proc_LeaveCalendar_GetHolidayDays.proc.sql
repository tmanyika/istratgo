﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_LeaveCalendar_GetHolidayDays] 
	@sDate datetime,
	@eDate datetime,
	@countryId int
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	declare @noofdays int;
	set @noofdays = 0;
	
	select @noofdays=count(HolidayDateId) from imd_user_db.LeaveCalendar where (CountryId = @countryId and Active = 1) and (HolidayDate between @sDate and @eDate)
	select @noofdays
END
