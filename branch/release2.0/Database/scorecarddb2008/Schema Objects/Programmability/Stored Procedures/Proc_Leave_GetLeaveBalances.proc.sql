﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_Leave_GetLeaveBalances]
	@orgUnitId int,
	@LeaveXml xml
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	select	nref.value('Id[1]', 'int') as leaveTypeId
	into #lType
	from   @LeaveXml.nodes('//Data/Leave') AS R(nref)
	
	select e.FirstName + ' ' + isnull(e.MiddleName,'') + ' ' + e.LastName as FullName, lt.LeaveName, isnull(lb.Accumulated,0) as Accumulated, isnull(lb.DaysTaken,0) as DaysTaken, isnull(lb.Balance,0) as Balance, isnull(lb.LimitValue,0) as LimitValue, e.EmailAddress, e.EmployeeNo, e.FirstName, isnull(e.MiddleName,'') as MiddleName, e.LastName, lb.LeaveId
	from   imd_user_db.Employee AS e LEFT OUTER JOIN
		   imd_user_db.LeaveBalance AS lb ON e.EmployeeId = lb.EmployeeId INNER JOIN
		   imd_user_db.LeaveType AS lt ON lb.LeaveId = lt.LeaveId
	where     (e.OrgUnitId = @orgUnitId) AND (lb.LeaveId IN (select leaveTypeId from #lType))
	order by e.LastName, e.FirstName, lt.LeaveName
	
	drop table #lType
END
