﻿




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_MailServer_GetActive]
	@active bit
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	open master key decryption by password = '3891@Macd'
	open symmetric key FieldSymmetricKey
	decryption by certificate FieldCertificate;
	
	select   AccountMailId, AccountName, SqlProfileName, UserName, imd_user_db.fxDecrypt(PIN) as PIN, SmtpServer, Port, Active, CreatedBy, DateCreated, DateUpdated, UpdatedBy
	from     imd_user_db.MailServer
	where     (Active = @active)
	order by AccountName
END





