﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_LeaveApplicationComment_Delete]
	@commentId int,
	@updatedBy varchar(20)
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	update imd_user_db.LeaveApplicationComment
	set Valid = 0,
		UpdatedBy = @updatedBy,
		DateUpdated = getdate()
	where CommentId = @commentId 
	select @@rowcount
END
