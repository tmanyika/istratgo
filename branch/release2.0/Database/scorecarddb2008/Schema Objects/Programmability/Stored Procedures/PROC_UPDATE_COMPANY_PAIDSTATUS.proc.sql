﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE imd_user_db.PROC_UPDATE_COMPANY_PAIDSTATUS
	@companyId int,
	@paid bit
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	update imd_user_db.COMPANY_DETAIL
	set PAID = @paid
	where COMPANY_ID = @companyId
	select @@rowcount
END
