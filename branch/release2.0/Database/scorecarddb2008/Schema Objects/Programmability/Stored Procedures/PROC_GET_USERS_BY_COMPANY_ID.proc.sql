﻿CREATE PROCEDURE [imd_user_db].[PROC_GET_USERS_BY_COMPANY_ID]
@COMPANY_ID INT
AS
begin

set nocount on;
set transaction isolation level read committed;
SELECT USER_ADMIN.ID
      ,[LOGIN_ID]
      ,[PASSWORD]
      ,[ORG_ID]
      ,[JOB_TITLE_ID]
      ,[NAME]
      ,[EMAIL_ADDRESS]      
      ,USER_ADMIN.[ACTIVE]
      ,[REPORTS_TO]
      ,[USER_TYPE] 
FROM  USER_ADMIN INNER JOIN
               COMPANY_STRUCTURE ON USER_ADMIN.ORG_ID = COMPANY_STRUCTURE.ID
WHERE COMPANY_ID=@COMPANY_ID

--select     e.EmployeeId AS ID, l.UserName AS LOGIN_ID, cs.ID AS ORG_ID, e.JobTitleId AS JOB_TITLE_ID, e.FirstName + ' ' + ISNULL(e.MiddleName, '') + e.LastName AS NAME,
--                       e.EmailAddress AS Email_Address, l.Active, e.LineManagerEmployeeId AS REPORTS_TO, l.RoleId AS USER_TYPE
--from         imd_user_db.COMPANY_STRUCTURE AS cs INNER JOIN
--                      imd_user_db.COMPANY_DETAIL AS cd ON cs.COMPANY_ID = cd.COMPANY_ID INNER JOIN
--                      imd_user_db.Employee AS e ON cs.ID = e.OrgUnitId INNER JOIN
--                      imd_user_db.EmployeeLogin AS l ON e.EmployeeId = l.EmployeeId
--where     (cs.ID = @COMPANY_ID)

end
