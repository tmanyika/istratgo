﻿




CREATE PROCEDURE [imd_user_db].[PROC_ADD_CUSTOMER]
@USER_NAME nchar(20)
,@SETUP_TYPE INT
,@PASSWORD varchar(250)
,@EMAIL_ADDRESS  VARCHAR(250)
,@FIRST_NAME  VARCHAR(250)
,@LAST_NAME  VARCHAR(250)
,@CELL_NUMBER NCHAR(20)
,@DATE_OF_BIRTH DATETIME

,@COMPANY_NAME  varchar(150)
,@REGISTRATION_NO nchar(20)
,@INDUSTRY int
,@COUNTRY int
,@VAT_NO nchar(20)
,@ADDRESS varchar(150)
,@CONTACT_NO nchar(20)

,@BANK_NAME varchar(50)
,@BRANCH_NAME varchar(50)
,@BRANCH_CODE varchar(50)
,@JOB_TITLE_NAME varchar(150)
,@USER_TYPE INT

AS

declare @ACCOUNT_NO int
declare @COMPANY_ID int
declare @ORG_ID int
declare @JOBTILE_ID int
declare @USER_ID int

BEGIN TRAN

INSERT INTO [CUSTOMER]
           (
           [EMAIL_ADDRESS]
           ,[FIRST_NAME]
           ,[LAST_NAME]
           ,[CELL_NUMBER]          
           ,[DATE_OF_BIRTH]
           )
     VALUES
           (          
                  @EMAIL_ADDRESS,
                  @FIRST_NAME,
                  @LAST_NAME,
                  @CELL_NUMBER,                 
                  @DATE_OF_BIRTH
            )

SELECT @ACCOUNT_NO = @@IDENTITY

INSERT INTO COMPANY_DETAIL
           ([ACCOUNT_NO]
           ,[COMPANY_NAME]
           ,[REGISTRATION_NO]
           ,[INDUSTRY]
           ,[COUNTRY]
           ,[VAT_NO]
           ,[ADDRESS]
           ,[CONTACT_NO]
           )
     VALUES
           (
            @ACCOUNT_NO
           ,@COMPANY_NAME
           ,@REGISTRATION_NO
           ,@INDUSTRY
           ,@COUNTRY
           ,@VAT_NO
           ,@ADDRESS
           ,@CONTACT_NO
             )
SELECT @COMPANY_ID = @@IDENTITY
INSERT INTO ACCOUNT_DETAILS
(
   [COMPANY_ID]
  ,[BANK_NAME]
  ,[BRANCH_NAME]
  ,[BRANCH_CODE]
)
VALUES
(
   @COMPANY_ID 
  ,@BANK_NAME 
  ,@BRANCH_NAME 
  ,@BRANCH_CODE 
)

INSERT INTO [COMPANY_STRUCTURE]
           (
            [COMPANY_ID]
           ,[ORGUNIT_NAME]
           ,[PARENT_ORG]
           ,[ORGUNT_TYPE]
           )
     VALUES
           (
            @COMPANY_ID
           ,@COMPANY_NAME
           ,0
           ,@SETUP_TYPE
           )

SELECT @ORG_ID = @@IDENTITY

INSERT INTO COMPANY_JOB_TITLES
(
  COMPANY_ID,
  [NAME]
)
VALUES
(
  @COMPANY_ID,
  @JOB_TITLE_NAME
)
SELECT @JOBTILE_ID = @@IDENTITY

INSERT INTO [USER_ADMIN]
           ([LOGIN_ID]
           ,[PASSWORD]
           ,[ORG_ID]         
           ,[JOB_TITLE_ID]
           ,[NAME]
           ,[EMAIL_ADDRESS]
           ,[USER_TYPE]               
           )
     VALUES
(
    @USER_NAME 
   ,@PASSWORD
   ,@ORG_ID
   ,@JOBTILE_ID
   ,@FIRST_NAME +' '+ @LAST_NAME
   ,@EMAIL_ADDRESS, @USER_TYPE     
)

select @USER_ID = @@IDENTITY

UPDATE USER_ADMIN SET REPORTS_TO = @USER_ID WHERE ID = @USER_ID
UPDATE [COMPANY_STRUCTURE] SET OWNER_ID = @USER_ID  WHERE ID =@ORG_ID

if @@ERROR <> 0 begin
  rollback tran
end 

select @COMPANY_ID
COMMIT TRAN





