﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_MailBatch_GetAddressByBatchId]
	@batchId int
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	select b.BatchId, e.FirstName, e.MiddleName, e.LastName, e.EmailAddress, l.UserName, e.EmployeeId
	from imd_user_db.EmployeeLogin AS l inner join
         imd_user_db.Employee AS e ON l.EmployeeId = e.EmployeeId inner join
         imd_user_db.MailBatchUserRole AS b ON l.RoleId = b.RoleId
	where     (b.BatchId = @batchId) and (not (e.EmailAddress in (select Email from imd_user_db.MailBatchUserList where BatchId = @batchId)))	
END
