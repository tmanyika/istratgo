﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_LeaveType_Update]
	@leaveId int,
	@leaveName varchar(100), 
	@leaveDescription varchar(300),
	@active bit, 
	@updatedBy varchar(20)
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	update  imd_user_db.LeaveType
	set     LeaveName = @leaveName, 
			LeaveDescription = @leaveDescription,
			Active = @active, 
			UpdatedBy = @updatedBy, 
			DateUpdated = GETDATE()
	where   (leaveId = @leaveId)
		
	select @@rowcount
END
