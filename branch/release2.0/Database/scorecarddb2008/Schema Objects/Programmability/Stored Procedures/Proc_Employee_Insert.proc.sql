﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_Employee_Insert] 
	 @LineManagerEmployeeId int = NULL,
     @OrgUnitId int, 
     @FirstName varchar(25), 
     @MiddleName varchar(25), 
     @LastName varchar(25), 
     @EmailAddress varchar(80),
     @JobTitleId int = NULL, 
     @StatusId int = NULL, 
     @EmployeeNo varchar(15), 
     @WorkingWeekId int = NULL, 
     @Cellphone varchar(15),
     @BusTelephone varchar(15), 
     @CreatedBy varchar(20), 
     @Active bit = 1, 
     @UserName varchar(20), 
     @Pass varchar(250),
     @RoleId int
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	declare @EmployeeId int, @FullName varchar(100), 
			@EmailExist bit, @UNameExist bit;
	set @EmployeeId = 0;
	set @UNameExist = 0;
	set @EmailExist = 0;
	
	set @FullName = @FirstName + ' ' + @MiddleName + ' ' + @LastName;
	
	if ((exists(select username from imd_user_db.EmployeeLogin where userName = @UserName)) or (exists(select LOGIN_ID from imd_user_db.USER_ADMIN where LOGIN_ID = @UserName)))
		set @UNameExist = 1
	if ((exists(select emailaddress from imd_user_db.Employee where EmailAddress = @EmailAddress)) or (exists(select LOGIN_ID from imd_user_db.USER_ADMIN where EMAIL_ADDRESS = @EmailAddress)))	
		set @EmailExist = 1
		
	if(@EmailExist = 0 and @UNameExist = 0)	
		begin
			begin try
				begin transaction
					insert into imd_user_db.Employee(LineManagerEmployeeId, OrgUnitId, FirstName, MiddleName, LastName, EmailAddress, JobTitleId, StatusId, EmployeeNo, 
							  WorkingWeekId, Cellphone, BusTelephone, CreatedBy, DateCreated, DateUpdated, UpdatedBy)
					values(@LineManagerEmployeeId, @OrgUnitId, @FirstName, @MiddleName, @LastName, @EmailAddress, @JobTitleId, @StatusId, @EmployeeNo, 
							@WorkingWeekId, @Cellphone, @BusTelephone, @CreatedBy, getdate(), NULL, NULL)
					select @EmployeeId = scope_identity()
		             
					insert into imd_user_db.EmployeeLogin(EmployeeId, UserName, Pass, RoleId, Active)
					values(@EmployeeId, @UserName, @Pass, @RoleId, @Active)
					 
					--insert into imd_user_db.USER_ADMIN(ID, LOGIN_ID, [PASSWORD], REPORTS_TO, ORG_ID, JOB_TITLE_ID, NAME, EMAIL_ADDRESS, DATE_CREATED, ACTIVE, USER_TYPE)
					--values(@EmployeeId, @UserName, @Pass, @LineManagerEmployeeId, @OrgUnitId, @JobTitleId, @FullName, @EmailAddress, getdate(), @Active, @RoleId)
				commit transaction
			end try
			begin catch
				if(@@trancount > 0)
					begin					 
						set @EmployeeId = -1
						rollback transaction
					end
			end catch
		end
	select @EmployeeId
END

	
