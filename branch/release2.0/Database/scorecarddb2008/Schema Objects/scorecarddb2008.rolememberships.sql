﻿EXECUTE sp_addrolemember @rolename = N'db_owner', @membername = N'imd_user_db';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'imd_user_db';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'dbuser';


GO
EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'imd_user_db';


GO
EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'dbuser';

