﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace scorecard.welcome.interfaces
{
    public interface IWelcome
    {
        Welcome GetRow(DataRow rw);
        Welcome GetUserRow(DataRow rw);
        Welcome GetById(int welcomeId);
        List<Welcome> GetShowAgainWelcome(int employeeId);

        bool SetEmployeeWelcomeStatus(Welcome obj);
        bool SetAllWelcomeStatus(int employeeId, bool status);
    }
}
