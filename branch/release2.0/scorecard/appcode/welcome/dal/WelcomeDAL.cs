﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

namespace scorecard.welcome.dal
{
    public class WelcomeDAL
    {
        #region Variables

        private string _conString;

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return _conString; }
            set { _conString = value; }
        }

        #endregion

        #region Default Constructors

        public WelcomeDAL(string conString)
        {
            _conString = conString;
        }

        #endregion

        #region Public Methods and Functions

        public DataTable Get(int welcomeId)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_Welcome_GetById", welcomeId).Tables[0];
        }

        public DataTable GetByEmployeeId(int employeeId)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_Welcome_GetByEmployeeId", employeeId).Tables[0];
        }

        public int Update(Welcome obj)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_Welcome_UpdateEmployeeSectionStatus", obj.EmployeeId, obj.WelcomeId, obj.ShowAgain);
            return (result != DBNull.Value) ? int.Parse(result.ToString()) : 0;
        }

        public int Update(int employeeId, bool status)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_Welcome_UpdateAll", employeeId, status);
            return (result != DBNull.Value) ? int.Parse(result.ToString()) : 0;
        }
        #endregion
    }
}