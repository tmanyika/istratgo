﻿using System;
using scorecard.entities;
using scorecard.interfaces;
using scorecard.implementations;

namespace Scorecard.Business.Rules
{
    public class BusinessRuleBL : IBusinessRule
    {
        Icompany comp;

        public BusinessRuleBL()
        {
            comp = new companyImpl();
        }

        public bool SubscriptionValid(int companyId)
        {
            company obj = comp.getCompanyInfo(new company { COMPANY_ID = companyId });

            int trialPeriod = Util.getTrialPeriodDays();
            double lapsedPeriod = DateTime.Now.Subtract(obj.DATE_REGISTERED).TotalDays;
            bool periodLapsed = lapsedPeriod > trialPeriod ? true : false;
            return (obj.PAID || !periodLapsed) ? true : false;
        }

        private bool TargetDivScoreTimesWeight(int unitOfMeasure)
        {
            string[] measures = Util.TargetDivScoreTimesWeight.Split(',');
            foreach (string strval in measures)
                if (strval.Equals(unitOfMeasure.ToString()))
                    return true;

            return false;
        }

        private bool ScoreDivTargetTimesWeight(int unitOfMeasure)
        {
            string[] measures = Util.TargetDivScoreTimesWeight.Split(',');
            foreach (string strval in measures)
                if (strval.Equals(unitOfMeasure.ToString()))
                    return true;

            return false;
        }

        public bool UnitOfMeasureRequireNumericValue(int unitOfMeasure)
        {
            string[] Ids = Util.UnitOfMeasureRequireNumericIDs.Split(',');
            foreach (string strval in Ids)
                if (strval.Equals(unitOfMeasure.ToString()))
                    return true;

            return false;
        }

        public bool UnitOfMeasureRequireYesNoValueValid(string score)
        {
            string[] Ids = Util.UnitOfMeasureYesNoExpectedValues.Split(',');
            foreach (string strval in Ids)
                if (string.Compare(strval.ToLower(), score.ToLower(), true) == 0)
                    return true;

            return false;
        }

        public bool UnitOfMeasureRequireYesNoValue(int unitOfMeasure)
        {
            string[] measures = Util.UnitOfMeasureRequireYesNoValues.Split(',');
            foreach (string strval in measures)
                if (strval.Equals(unitOfMeasure.ToString()))
                    return true;

            return false;
        }

        public decimal FinalScore(int unitOfMeasure, decimal weight, decimal score, decimal target)
        {
            decimal result = 0;
            if (TargetDivScoreTimesWeight(unitOfMeasure))
                result = ((target / score) * 100m); // target/score*weight
            else if (ScoreDivTargetTimesWeight(unitOfMeasure))
                result = ((score / target) * 100m); // score/target*weight
            else //existing units
                result = ((score / target) * 100m); // score/target*weight
            decimal answer = ((result / 100m) * weight);
            return Math.Round(answer, 2);
        }
    }
}