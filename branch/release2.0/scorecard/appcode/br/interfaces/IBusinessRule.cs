﻿using System;
using scorecard.entities;

namespace Scorecard.Business.Rules
{
    public interface IBusinessRule
    {
        bool SubscriptionValid(int companyId);
        bool UnitOfMeasureRequireYesNoValue(int unitOfMeasure);
        bool UnitOfMeasureRequireNumericValue(int unitOfMeasure);
        bool UnitOfMeasureRequireYesNoValueValid(string score);
        decimal FinalScore(int unitOfMeasure, decimal weight, decimal score, decimal target);
    }
}
