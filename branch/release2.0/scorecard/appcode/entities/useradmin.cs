﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/* @Copy Albertoncaffeine.net */

namespace scorecard.entities
{
    [Serializable]
    public class useradmin
    {
        public int ID { get; set; }
        public string LOGIN_ID { get; set; }
        public string PASSWD { get; set; }
        public int ORG_ID { get; set; }
        public int REPORTS_TO { get; set; }
        public int JOB_TITLE_ID { get; set; }
        public string NAME { get; set; }
        public string EMAIL_ADDRESS { get; set; }
        public DateTime DATE_CREATED { get; set; }
        public bool ACTIVE { get; set; }
        public bool IS_AUTHENTICATED { get; set; }
        public Orgunit COMPANY_INFO { get; set; }
        public int USER_TYPE { get; set; }
        public int CountryId { get; set; }
        public useradmin() { }
    }
}