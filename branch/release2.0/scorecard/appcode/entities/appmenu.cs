﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
/* @Copy Albertoncaffeine.net */
namespace scorecard.entities
{
    public class appmenu
    {
        public int MENU_ID { get; set; }
        public string MENU_NAME { get; set; }
        public string MENU_PATH { get; set; }
        public int ORDER_ID { get; set; }
        public bool ACTIVE { get; set; }
    }
}