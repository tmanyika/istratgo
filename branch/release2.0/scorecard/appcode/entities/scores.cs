﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/* @Copy Albertoncaffeine.net */

namespace scorecard.entities
{
    public class scores : goals
    {
        public int CRITERIA_TYPE_ID { get; set; }
        public int SELECTED_ITEM_VALUE { get; set; }
        public int GOAL_ID { get; set; }
        public string SCORES { get; set; }
        public double FINAL_SCORE { get; set; }
        public int SUBMISSION_ID { get; set; }
        public string PERIOD_DATE_DESC { get; set; }
        public string PERIOD_DATE_VAL { get; set; }

        public string FULL_NAME { get; set; }
        public int USER_ID { get; set; }

        public string PROJECT_NAME { get; set; }
        public string ORGUNIT_NAME { get; set; }
    }

    public class totals : scores
    {
        public double SUM_WEIGHT { get; set; }
        public double SUM_FINAL_SCORE { get; set; }
        public double PERCENTAGE { get; set; }
    }

    public class submissionWorkflow
    {
        public int ID { get; set; }
        public int CRITERIA_TYPE_ID { get; set; }
        public int SELECTED_ITEM_VALUE { get; set; }
        public DateTime DATE_SUBMITTED { get; set; }
        public int APPROVER_ID { get; set; }
        public int STATUS { get; set; }
        public int COMPANY_ID { get; set; }
    }

    public class attachmentDocuments
    {
        public int ID { get; set; }
        public string ATTACHMENT_NAME { get; set; }
        public int SUBMISSION_ID { get; set; }
    }

    public class ItemValue
    {
        public int ItemId { get; set; }
        public string ItemName { get; set; }
    }

    public class PerformanceReport : scores
    {
        public int ItemId { get; set; }
        public string ItemName { get; set; }
        public decimal AverageScore { get; set; }
        public decimal TargetScore { get; set; }
    }

    public class ReportAdjust
    {
        public int Idx { get; set; }
        public int ResetCaptionIdx { get; set; }

        public decimal Total { get; set; }
    }
}