﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using scorecard.entities;

/* @Copy Albertoncaffeine.net */
namespace scorecard.interfaces
{
    public interface Igoals
    {
        int defineStrategicGoal(strategic _goalDef);
        int updateStrategicGoal(strategic _goalDef);
        int deleteStrategicGoal(strategic _goalDef);
        List<strategic> listStrategicGoals(company _company);

        int defineGeneralGoal(goals _goalDef);
        int updateGeneralGoal(goals _goalDef);
        int deleteGeneralGoal(goals _goalDef);
        List<goals> listGeneralGoals(company _company);
        goals getGoal(int id);
    }
}