﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using scorecard.entities;

/* @Copy Albertoncaffeine.net */
namespace scorecard.interfaces
{
    public interface Icustomer
    {
        int updateCustomer(customer _customerInfo);
        int deleteCustomer(customer _customerInfo);
        int registerCustomer(customer _customerInfo, company _company, out string errorMessage);
        int registerCustomer(customer customer, company company, bankaccount account);

        customer getCustomerInfo(customer _customer);
        List<customer> getCustomersList();
    }
}