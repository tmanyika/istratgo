﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using scorecard.entities;

/* @Copy Albertoncaffeine.net */

namespace scorecard.interfaces
{
    public interface Iuser
    {
        int addUserAccount(useradmin _user, out string errorMessage);
        int updateUserAccount(useradmin _user);
        int deactivateUserAccount(useradmin _user);
        int updatePassword(useradmin user);

        List<useradmin> getAllCompanyUsers(company _company);
        List<useradmin> getAllUsers();

        useradmin getUserAccountInformation(useradmin _user);
        useradmin getUserAccountById(useradmin _user);
        useradmin getLineManagerUserAccount(int areaId, int areaValueId);
       
    }
}