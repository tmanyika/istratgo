﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using scorecard.interfaces;
using scorecard.entities;
using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;

/* @Copy Albertoncaffeine.net */

namespace scorecard.implementations
{
    public class goalsImpl : Igoals
    {
        #region Igoals Members

        public int defineStrategicGoal(strategic _goalDef)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try
            {
                return SqlHelper.ExecuteNonQuery(con, "PROC_ADD_STRATEGIC_GOALS",
                  _goalDef.COMPANY_ID,
                  _goalDef.PERSPECTIVE,
                  _goalDef.OBJECTIVE
                 );
            }
            catch (Exception e) {
                Util.LogErrors(e);
                return 0;
            }
            finally {
                con.Close();
                con.Dispose();
            }
        }

        public int updateStrategicGoal(strategic _goalDef)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try
            {
                return SqlHelper.ExecuteNonQuery(con, "PROC_UPDATE_STRATEGIC_GOAL",
                  _goalDef.ID,
                  _goalDef.COMPANY_ID,
                  _goalDef.PERSPECTIVE,
                  _goalDef.OBJECTIVE
                 );
            }
            catch (Exception e) {
                Util.LogErrors(e);
                return 0;
            }
            finally {
                con.Close();
                con.Dispose();
            }
        }

        public int deleteStrategicGoal(strategic _goalDef)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try
            {
                return SqlHelper.ExecuteNonQuery(con, "PROC_DELETE_STRATEGIC_GOAL",
                  _goalDef.ID
                 );
            }
            catch (Exception e){
                Util.LogErrors(e);
                return 0;
            }
            finally {
                con.Close();
                con.Dispose();
            }
        }

        public List<strategic> listStrategicGoals(company _company)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            SqlDataReader read = null;
            try
            {
                List<strategic> listStrategic = new List<strategic>();
                read = SqlHelper.ExecuteReader(con, "PROC_GET_ALL_STRATEGIC_COMPANY_GOALS", _company.COMPANY_ID);
                while (read.Read())
                {
                    strategic str = new strategic
                    {
                        ID = read.GetInt32(0),
                        COMPANY_ID = read.GetInt32(1),
                        PERSPECTIVE = read.GetInt32(2),
                        OBJECTIVE = read.GetString(3),
                        DATE_CREATED = read.GetDateTime(4)
                    };
                    listStrategic.Add(str);
                }
                return listStrategic;
            }
            catch (Exception ex) {
                Util.LogErrors(ex);
                return null;
            }
            finally {
                read.Dispose();
                read.Close();
                con.Dispose();
                con.Close();
            }
        }

        public int defineGeneralGoal(goals _goalDef)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try
            {
                return SqlHelper.ExecuteNonQuery(con, "PROC_ADD_COMPANY_GOALS",
                  _goalDef.STRATEGIC_ID,
                  _goalDef.AREA_TYPE_ID,
                  _goalDef.GOAL_DESCRIPTION,
                  _goalDef.UNIT_MEASURE,
                  _goalDef.TARGET,
                  _goalDef.WEIGHT,
                  _goalDef.AREA_ID
                 );
            }
            catch (Exception e){
                Util.LogErrors(e);
                return 0;
            }
            finally{
                con.Close();
                con.Dispose();
            }
        }

        public int updateGeneralGoal(goals _goalDef)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try
            {
                return SqlHelper.ExecuteNonQuery(con, "PROC_UPDATE_COMPANY_GOAL",
                  _goalDef.ID,
                  _goalDef.STRATEGIC_ID,
                  _goalDef.AREA_TYPE_ID,
                  _goalDef.GOAL_DESCRIPTION,
                  _goalDef.UNIT_MEASURE,
                  _goalDef.TARGET,
                  _goalDef.WEIGHT,
                  _goalDef.AREA_ID
                 );
            }
            catch (Exception e){
                Util.LogErrors(e);
                return 0;
            }
            finally{
                con.Close();
                con.Dispose();
            }
        }

        public int deleteGeneralGoal(goals _goalDef)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try{
                return SqlHelper.ExecuteNonQuery(con, "PROC_DELETE_COMPANY_GOAL",
                  _goalDef.ID);
            }
            catch (Exception e){
                Util.LogErrors(e);
                return 0;
            }
            finally {
                con.Close();
                con.Dispose();
            }
        }

        public List<goals> listGeneralGoals(company _company)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            SqlDataReader read = null;
            try
            {
                List<goals> listStrategic = new List<goals>();
                read = SqlHelper.ExecuteReader(con, "PROC_GET_ALL_COMPANY_GOALS", _company.COMPANY_ID);
                while (read.Read()) {
                    goals str = new goals {
                        ID = read.GetInt32(0),
                        COMPANY_ID = read.GetInt32(1),
                        STRATEGIC_ID = read.GetInt32(2),
                        AREA_TYPE_ID = read.GetInt32(3),
                        GOAL_DESCRIPTION = read.GetString(4),
                        UNIT_MEASURE = read.GetInt32(5),
                        TARGET = read.GetString(6),
                        WEIGHT = read.GetInt32(7),
                        DATE_CREATED = read.GetDateTime(8),
                        AREA_ID = read.GetInt32(9),
                        PERIOD_DATE = DateTime.Parse(read["PERIOD_DATE"].ToString())
                    };
                    listStrategic.Add(str);
                }
                return listStrategic;
            }
            catch (Exception ex){
                Util.LogErrors(ex);
                return null;
            }
            finally{
                read.Dispose();
                read.Close();
                con.Dispose();
                con.Close();
            }
        }


        public goals getGoal(int id)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            SqlDataReader read = null;
            try
            {
                read = SqlHelper.ExecuteReader(con, "PROC_GET_GOAL_BYID", id);
                if (read.Read())
                {
                   return new goals
                    {
                        ID = read.GetInt32(0),
                        COMPANY_ID = read.GetInt32(1),
                        STRATEGIC_ID = read.GetInt32(2),
                        AREA_TYPE_ID = read.GetInt32(3),
                        GOAL_DESCRIPTION = read.GetString(4),
                        UNIT_MEASURE = read.GetInt32(5),
                        TARGET = read.GetString(6),
                        WEIGHT = read.GetInt32(7),
                        DATE_CREATED = read.GetDateTime(8),
                        AREA_ID = read.GetInt32(9)
                    };
                    
                }
                return new goals();
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
            finally
            {
                read.Dispose();
                read.Close();
                con.Dispose();
                con.Close();
            }
        }
        #endregion
    }
}