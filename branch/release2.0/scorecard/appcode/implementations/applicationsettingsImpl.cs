﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using scorecard.entities;
using System.Data.SqlClient;
using scorecard.interfaces;
using Microsoft.ApplicationBlocks.Data;

/* @Copy Albertoncaffeine.net */

namespace scorecard.implementations
{
    public class applicationsettingsImpl : Isettings
    {
        #region Isettings Members

        public List<applicationsettings> getParentSettings()
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            SqlDataReader read = null;
            try
            {
                List<applicationsettings> parentSettings = new List<applicationsettings>();
                object[] objParam = new object[] { };
                read = SqlHelper.ExecuteReader(con, "PROC_GETALLSETTINGSPARENTITEMS", objParam);
                while (read.Read())
                {
                    parentSettings.Add(new applicationsettings
                    {
                        TYPE_ID = read.GetInt32(0),
                        PARENT_ID = read.GetInt32(1),
                        NAME = read.GetString(2),
                        DESCRIPTION = read.GetString(3),
                        ACTIVE = read.GetBoolean(4)
                    });
                }
                return parentSettings.Where(c => c.ACTIVE == true).ToList();
            }
            catch (SqlException sqle){
                Util.LogErrors(sqle);
                return null;
            }
            catch (Exception ex){
                Util.LogErrors(ex);
                return null;
            }
            finally{
                read.Dispose();
                read.Close();
                con.Close();
                con.Dispose();
            }
        }

        public List<applicationsettings> getChildSettings(applicationsettings appSetting)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            SqlDataReader read = null;
            try{
                List<applicationsettings> childSettings = new List<applicationsettings>();               
                read = SqlHelper.ExecuteReader(con, "PROC_GETALLSETTINGSCHILDREN", appSetting.PARENT_ID);
                while (read.Read())
                {
                    childSettings.Add(new applicationsettings
                    {
                        TYPE_ID = read.GetInt32(0),
                        PARENT_ID = read.GetInt32(1),
                        NAME = read.GetString(2),
                        DESCRIPTION = read.GetString(3),
                        ACTIVE = read.GetBoolean(4)
                    });
                }
                return childSettings.Where(c => c.ACTIVE == true).ToList();
            }
            catch (SqlException sqle){
                Util.LogErrors(sqle);
                return null;
            }
            catch (Exception ex){
                Util.LogErrors(ex);
                return null;
            }
            finally {
                read.Dispose();
                read.Close();
                con.Close();
                con.Dispose();
            }
        }

        public applicationsettings getApplicationSetting(applicationsettings appSetting)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            SqlDataReader read = null;
            try
            {
                applicationsettings setting = new applicationsettings();
                read = SqlHelper.ExecuteReader(con, "PROC_GETSETTING", appSetting.TYPE_ID);
                while (read.Read())
                {
                    setting.TYPE_ID = read.GetInt32(0);
                    setting.PARENT_ID = read.GetInt32(1);
                    setting.NAME = read.GetString(2);
                    setting.DESCRIPTION = read.GetString(3);
                    setting.ACTIVE = read.GetBoolean(4);
                }
                return setting;
            }
            catch (SqlException sqle){
                Util.LogErrors(sqle);
                return null;
            }
            catch (Exception ex){
                Util.LogErrors(ex);
                return null;
            }
            finally{
                read.Dispose();
                read.Close();
                con.Close();
                con.Dispose();
            }
        }

        public int AddNewSettings(applicationsettings appSetting)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try{
                return SqlHelper.ExecuteNonQuery(con, "PROC_ADDNEWSETTING", appSetting.PARENT_ID, appSetting.NAME, appSetting.DESCRIPTION);
            }
            catch (SqlException sqle){
                Util.LogErrors(sqle);
                return 0;
            }
            catch (Exception ex){
                Util.LogErrors(ex);
                return 0;
            }
            finally{
                con.Close();
                con.Dispose();
            }
        }

        public int DeleteSettings(applicationsettings appSetting)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try{
                return SqlHelper.ExecuteNonQuery(con, "PROC_DEACTIVATESETTING", appSetting.TYPE_ID);
            }
            catch (SqlException sqle){
                Util.LogErrors(sqle);
                return 0;
            }
            catch (Exception ex){
                Util.LogErrors(ex);
                return 0;
            }
            finally{
                con.Close();
                con.Dispose();
            }
        }

        public int UpdateSetting(applicationsettings appSetting)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try{
                return SqlHelper.ExecuteNonQuery(con, "PROC_UPDATESETTING", appSetting.TYPE_ID, appSetting.NAME, appSetting.DESCRIPTION);
            }
            catch (SqlException sqle){
                Util.LogErrors(sqle);
                return 0;
            }
            catch (Exception ex){
                Util.LogErrors(ex);
                return 0;
            }
            finally{
                con.Close();
                con.Dispose();
            }
        }

        #endregion
    }
}