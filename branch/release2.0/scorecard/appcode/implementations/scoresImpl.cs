﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using scorecard.interfaces;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using scorecard.entities;

/* @Copy Albertoncaffeine.net */

namespace scorecard.implementations
{
    public class scoresImpl : Iscores
    {
        #region Iscores Members

        public List<scores> getScoresByGoalID(goals _goal)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            SqlDataReader read = null;
            try
            {
                List<scores> company = new List<scores>();
                read = SqlHelper.ExecuteReader(con, "PROC_GET_SCORES_BY_GOALID", _goal.ID, _goal.PERSPECTIVE);
                while (read.Read())
                {
                    company.Add(new scores
                    {
                        ID = read["ID"] != DBNull.Value ? int.Parse(read["ID"].ToString()) : 0,
                        CRITERIA_TYPE_ID = read["CRITERIA_TYPE_ID"] != DBNull.Value ? int.Parse(read["CRITERIA_TYPE_ID"].ToString()) : 0,
                        SELECTED_ITEM_VALUE = read["SELECTED_ITEM_VALUE"] != DBNull.Value ? int.Parse(read["SELECTED_ITEM_VALUE"].ToString()) : 0,
                        GOAL_ID = read["GOAL_ID"] != DBNull.Value ? int.Parse(read["GOAL_ID"].ToString()) : 0,
                        SCORES = read["SCORES"] != DBNull.Value ? read["SCORES"].ToString() : string.Empty,
                        FINAL_SCORE = read["FINAL_SCORE"] != DBNull.Value ? double.Parse(read["FINAL_SCORE"].ToString()) : 0,
                        DATE_CREATED = read["DATE_CREATED"] != DBNull.Value ? DateTime.Parse(read["DATE_CREATED"].ToString()) : DateTime.Now,
                        SUBMISSION_ID = read["SUBMISSION_ID"] != DBNull.Value ? int.Parse(read["SUBMISSION_ID"].ToString()) : 0,
                        PERIOD_DATE = DateTime.Parse(read["PERIOD_DATE"].ToString())
                    });
                }
                return company;
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
            finally
            {
                read.Dispose();
                read.Close();
                con.Dispose();
                con.Close();
            }
        }

        public List<scores> getUsedScoreDates(int criteriaId, int itemValueId)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            SqlDataReader read = null;
            try
            {
                List<scores> score = new List<scores>();
                read = SqlHelper.ExecuteReader(con, "PROC_GET_SCORES_USEDDATES", criteriaId, itemValueId);
                while (read.Read())
                    score.Add(new scores { PERIOD_DATE_VAL = read["PERIOD_DATE"].ToString() });
                return score;
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
            finally
            {
                read.Dispose();
                read.Close();
                con.Dispose();
                con.Close();
            }
        }

        public List<scores> getScoreDates(int criteriaId, int itemValueId)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            SqlDataReader read = null;
            try
            {
                List<scores> score = new List<scores>();
                read = SqlHelper.ExecuteReader(con, "PROC_GET_SCORES_DATES", criteriaId, itemValueId);
                while (read.Read())
                {
                    score.Add(new scores
                    {
                        PERIOD_DATE_VAL = read["PERIOD_DATE"].ToString(),
                        PERIOD_DATE_DESC = read["PERIOD_DATE_DESC"].ToString(),
                    });
                }
                return score;
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
            finally
            {
                read.Dispose();
                read.Close();
                con.Dispose();
                con.Close();
            }
        }

        public List<scores> getReportDates(int criteriaId, string xmlData)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            SqlDataReader read = null;
            try
            {
                List<scores> score = new List<scores>();
                read = SqlHelper.ExecuteReader(con, "PROC_GET_SCORES_AREA_DATES", criteriaId, xmlData);
                while (read.Read())
                {
                    score.Add(new scores
                    {
                        PERIOD_DATE_VAL = read["PERIOD_DATE"].ToString(),
                        PERIOD_DATE_DESC = read["PERIOD_DATE_DESC"].ToString(),
                    });
                }
                return score;
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
            finally
            {
                read.Dispose();
                read.Close();
                con.Dispose();
                con.Close();
            }
        }

        public List<PerformanceReport> getDepartmentScorePerformanceReport(int criteriaId, int itemValueId, string sdate, string edate, bool isCompany)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            SqlDataReader read = null;
            try
            {
                List<PerformanceReport> score = new List<PerformanceReport>();
                read = SqlHelper.ExecuteReader(con, "PROC_GET_SCORES_REPORT_DEPARTMENT", criteriaId, itemValueId, sdate, edate, isCompany);
                while (read.Read())
                {
                    score.Add(new PerformanceReport
                    {
                        ID = int.Parse(read["ID"].ToString()),
                        CRITERIA_TYPE_ID = int.Parse(read["CRITERIA_TYPE_ID"].ToString()),
                        SELECTED_ITEM_VALUE = int.Parse(read["SELECTED_ITEM_VALUE"].ToString()),
                        GOAL_ID = int.Parse(read["GOAL_ID"].ToString()),
                        SCORES = read["SCORES"] != DBNull.Value ? read["SCORES"].ToString() : string.Empty,
                        FINAL_SCORE = double.Parse(read["FINAL_SCORE"].ToString()),
                        DATE_CREATED = DateTime.Parse(read["DATE_CREATED"].ToString()),
                        SUBMISSION_ID = int.Parse(read["SUBMISSION_ID"].ToString()),
                        WEIGHT = int.Parse(read["WEIGHT"].ToString()),
                        UNIT_MEASURE_NAME = read["UNIT_MEASURE"].ToString(),
                        TARGET = read["TARGET"] != DBNull.Value ? read["TARGET"].ToString() : string.Empty,
                        GOAL_DESCRIPTION = read["GOAL_DESCRIPTION"].ToString(),
                        PERIOD_DATE = DateTime.Parse(read["PERIOD_DATE"].ToString()),
                        PERSPECTIVE_NAME = read["PERSPECTIVE_NAME"].ToString(),
                        OBJECTIVE = read["OBJECTIVE"].ToString(),
                        PERSPECTIVE_ID = int.Parse(read["PERSPECTIVE_ID"].ToString()),
                        ItemId = (read["ItemId"] != DBNull.Value) ? int.Parse(read["ItemId"].ToString()) : 0,
                        ItemName = (read["ItemName"] != DBNull.Value) ? read["ItemName"].ToString() : string.Empty
                    });
                }
                return score;
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
            finally
            {
                read.Dispose();
                read.Close();
                con.Dispose();
                con.Close();
            }
        }

        public List<PerformanceReport> getDepartmentUsersScorePerformanceReport(int criteriaId, int itemValueId, string sdate, string edate)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            SqlDataReader read = null;
            try
            {
                List<PerformanceReport> score = new List<PerformanceReport>();
                read = SqlHelper.ExecuteReader(con, "PROC_GET_SCORES_REPORT_USERSDEPARTMENT", criteriaId, itemValueId, sdate, edate);
                while (read.Read())
                {
                    score.Add(new PerformanceReport
                    {
                        ID = int.Parse(read["ID"].ToString()),
                        CRITERIA_TYPE_ID = int.Parse(read["CRITERIA_TYPE_ID"].ToString()),
                        SELECTED_ITEM_VALUE = int.Parse(read["SELECTED_ITEM_VALUE"].ToString()),
                        GOAL_ID = int.Parse(read["GOAL_ID"].ToString()),
                        SCORES = read["SCORES"] != DBNull.Value ? read["SCORES"].ToString() : string.Empty,
                        FINAL_SCORE = double.Parse(read["FINAL_SCORE"].ToString()),
                        DATE_CREATED = DateTime.Parse(read["DATE_CREATED"].ToString()),
                        SUBMISSION_ID = int.Parse(read["SUBMISSION_ID"].ToString()),
                        WEIGHT = int.Parse(read["WEIGHT"].ToString()),
                        UNIT_MEASURE_NAME = read["UNIT_MEASURE"].ToString(),
                        TARGET = read["TARGET"] != DBNull.Value ? read["TARGET"].ToString() : string.Empty,
                        GOAL_DESCRIPTION = read["GOAL_DESCRIPTION"].ToString(),
                        PERIOD_DATE = DateTime.Parse(read["PERIOD_DATE"].ToString()),
                        PERSPECTIVE_NAME = read["PERSPECTIVE_NAME"].ToString(),
                        OBJECTIVE = read["OBJECTIVE"].ToString(),
                        PERSPECTIVE_ID = int.Parse(read["PERSPECTIVE_ID"].ToString()),
                        ItemId = (read["ItemId"] != DBNull.Value) ? int.Parse(read["ItemId"].ToString()) : 0,
                        ItemName = (read["ItemName"] != DBNull.Value) ? read["ItemName"].ToString() : string.Empty
                    });
                }
                return score;
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
            finally
            {
                read.Dispose();
                read.Close();
                con.Dispose();
                con.Close();
            }
        }

        public List<PerformanceReport> getEmployeeScorePerformanceReport(int criteriaId, int itemValueId, string sdate, string edate)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            SqlDataReader read = null;
            try
            {
                List<PerformanceReport> score = new List<PerformanceReport>();
                read = SqlHelper.ExecuteReader(con, "PROC_GET_SCORES_REPORT_EMPLOYEES", criteriaId, itemValueId, sdate, edate);
                while (read.Read())
                {
                    score.Add(new PerformanceReport
                    {
                        ID = int.Parse(read["ID"].ToString()),
                        CRITERIA_TYPE_ID = int.Parse(read["CRITERIA_TYPE_ID"].ToString()),
                        SELECTED_ITEM_VALUE = int.Parse(read["SELECTED_ITEM_VALUE"].ToString()),
                        GOAL_ID = int.Parse(read["GOAL_ID"].ToString()),
                        SCORES = read["SCORES"] != DBNull.Value ? read["SCORES"].ToString() : string.Empty,
                        FINAL_SCORE = double.Parse(read["FINAL_SCORE"].ToString()),
                        DATE_CREATED = DateTime.Parse(read["DATE_CREATED"].ToString()),
                        SUBMISSION_ID = int.Parse(read["SUBMISSION_ID"].ToString()),
                        WEIGHT = int.Parse(read["WEIGHT"].ToString()),
                        UNIT_MEASURE_NAME = read["UNIT_MEASURE"].ToString(),
                        TARGET = read["TARGET"] != DBNull.Value ? read["TARGET"].ToString() : string.Empty,
                        GOAL_DESCRIPTION = read["GOAL_DESCRIPTION"].ToString(),
                        PERIOD_DATE = DateTime.Parse(read["PERIOD_DATE"].ToString()),
                        PERSPECTIVE_NAME = read["PERSPECTIVE_NAME"].ToString(),
                        OBJECTIVE = read["OBJECTIVE"].ToString(),
                        PERSPECTIVE_ID = int.Parse(read["PERSPECTIVE_ID"].ToString()),
                        ItemId = (read["ItemId"] != DBNull.Value) ? int.Parse(read["ItemId"].ToString()) : 0,
                        ItemName = (read["ItemName"] != DBNull.Value) ? read["ItemName"].ToString() : string.Empty
                    });
                }
                return score;
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
            finally
            {
                read.Dispose();
                read.Close();
                con.Dispose();
                con.Close();
            }
        }

        public List<PerformanceReport> getJobTitleScorePerformanceReport(int criteriaId, int itemValueId, string sdate, string edate)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            SqlDataReader read = null;
            try
            {
                List<PerformanceReport> score = new List<PerformanceReport>();
                read = SqlHelper.ExecuteReader(con, "PROC_GET_SCORES_REPORT_EMPLOYEES_BYJOBTITLE", criteriaId, itemValueId, sdate, edate);
                while (read.Read())
                {
                    score.Add(new PerformanceReport
                    {
                        ID = int.Parse(read["ID"].ToString()),
                        CRITERIA_TYPE_ID = int.Parse(read["CRITERIA_TYPE_ID"].ToString()),
                        SELECTED_ITEM_VALUE = int.Parse(read["SELECTED_ITEM_VALUE"].ToString()),
                        GOAL_ID = int.Parse(read["GOAL_ID"].ToString()),
                        SCORES = read["SCORES"] != DBNull.Value ? read["SCORES"].ToString() : string.Empty,
                        FINAL_SCORE = double.Parse(read["FINAL_SCORE"].ToString()),
                        DATE_CREATED = DateTime.Parse(read["DATE_CREATED"].ToString()),
                        SUBMISSION_ID = int.Parse(read["SUBMISSION_ID"].ToString()),
                        WEIGHT = int.Parse(read["WEIGHT"].ToString()),
                        UNIT_MEASURE_NAME = read["UNIT_MEASURE"].ToString(),
                        TARGET = read["TARGET"] != DBNull.Value ? read["TARGET"].ToString() : string.Empty,
                        GOAL_DESCRIPTION = read["GOAL_DESCRIPTION"].ToString(),
                        PERIOD_DATE = DateTime.Parse(read["PERIOD_DATE"].ToString()),
                        PERSPECTIVE_NAME = read["PERSPECTIVE_NAME"].ToString(),
                        OBJECTIVE = read["OBJECTIVE"].ToString(),
                        PERSPECTIVE_ID = int.Parse(read["PERSPECTIVE_ID"].ToString()),
                        ItemId = (read["ItemId"] != DBNull.Value) ? int.Parse(read["ItemId"].ToString()) : 0,
                        ItemName = (read["ItemName"] != DBNull.Value) ? read["ItemName"].ToString() : string.Empty
                    });
                }
                return score;
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
            finally
            {
                read.Dispose();
                read.Close();
                con.Dispose();
                con.Close();
            }
        }

        public List<scores> getScoreReport(int criteriaId, int itemValueId, string dateValue)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            SqlDataReader read = null;
            try
            {
                List<scores> score = new List<scores>();
                read = SqlHelper.ExecuteReader(con, "PROC_GET_SCORES_REPORT", criteriaId, itemValueId, dateValue);
                while (read.Read())
                {
                    score.Add(new scores
                    {
                        ID = int.Parse(read["ID"].ToString()),
                        CRITERIA_TYPE_ID = int.Parse(read["CRITERIA_TYPE_ID"].ToString()),
                        SELECTED_ITEM_VALUE = int.Parse(read["SELECTED_ITEM_VALUE"].ToString()),
                        GOAL_ID = int.Parse(read["GOAL_ID"].ToString()),
                        SCORES = read["SCORES"] != DBNull.Value ? read["SCORES"].ToString() : string.Empty,
                        FINAL_SCORE = double.Parse(read["FINAL_SCORE"].ToString()),
                        DATE_CREATED = DateTime.Parse(read["DATE_CREATED"].ToString()),
                        SUBMISSION_ID = int.Parse(read["SUBMISSION_ID"].ToString()),
                        WEIGHT = int.Parse(read["WEIGHT"].ToString()),
                        UNIT_MEASURE_NAME = read["UNIT_MEASURE"].ToString(),
                        TARGET = read["TARGET"] != DBNull.Value ? read["TARGET"].ToString() : string.Empty,
                        GOAL_DESCRIPTION = read["GOAL_DESCRIPTION"].ToString(),
                        PERIOD_DATE = DateTime.Parse(read["PERIOD_DATE"].ToString()),
                        PERSPECTIVE_NAME = read["PERSPECTIVE_NAME"].ToString(),
                        OBJECTIVE = read["OBJECTIVE"].ToString(),
                        PERSPECTIVE_ID = int.Parse(read["PERSPECTIVE_ID"].ToString())
                    });
                }
                return score;
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
            finally
            {
                read.Dispose();
                read.Close();
                con.Dispose();
                con.Close();
            }
        }

        public List<scores> getScoreReport(int criteriaId, int itemValueId, string startDate, string endDate)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            SqlDataReader read = null;
            try
            {
                List<scores> score = new List<scores>();
                read = SqlHelper.ExecuteReader(con, "PROC_GET_SCORES_REPORT_BY_PERIOD", criteriaId, itemValueId, startDate, endDate);
                while (read.Read())
                {
                    score.Add(new scores
                    {
                        //ID = int.Parse(read["ID"].ToString()),
                        //GOAL_ID = int.Parse(read["GOAL_ID"].ToString()),
                        //SCORES = read["SCORES"] != DBNull.Value ? read["SCORES"].ToString() : string.Empty,
                        //DATE_CREATED = DateTime.Parse(read["DATE_CREATED"].ToString()),
                        //SUBMISSION_ID = int.Parse(read["SUBMISSION_ID"].ToString()),
                        //PERIOD_DATE = DateTime.Parse(read["PERIOD_DATE"].ToString()),
                        WEIGHT = int.Parse(read["WEIGHT"].ToString()),                       
                        CRITERIA_TYPE_ID = int.Parse(read["CRITERIA_TYPE_ID"].ToString()),
                        SELECTED_ITEM_VALUE = int.Parse(read["SELECTED_ITEM_VALUE"].ToString()),
                        FINAL_SCORE = double.Parse(read["FINAL_SCORE"].ToString()),
                        UNIT_MEASURE_NAME = read["UNIT_MEASURE"].ToString(),
                        TARGET = read["TARGET"] != DBNull.Value ? read["TARGET"].ToString() : string.Empty,
                        GOAL_DESCRIPTION = read["GOAL_DESCRIPTION"].ToString(),                       
                        PERSPECTIVE_NAME = read["PERSPECTIVE_NAME"].ToString(),
                        OBJECTIVE = read["OBJECTIVE"].ToString(),
                        PERSPECTIVE_ID = int.Parse(read["PERSPECTIVE_ID"].ToString())
                    });
                }
                return score;
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
            finally
            {
                read.Dispose();
                read.Close();
                con.Dispose();
                con.Close();
            }
        }

        public List<scores> getAggregateReport(int criteriaId, string areaValueId, string date)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            SqlDataReader read = null;
            try
            {
                List<scores> score = new List<scores>();
                read = SqlHelper.ExecuteReader(con, "PROC_GET_SCORES_REPORT_AGGREGATE", criteriaId, areaValueId, date);
                while (read.Read())
                {
                    score.Add(new scores
                    {
                        ID = int.Parse(read["ID"].ToString()),
                        CRITERIA_TYPE_ID = int.Parse(read["CRITERIA_TYPE_ID"].ToString()),
                        SELECTED_ITEM_VALUE = int.Parse(read["SELECTED_ITEM_VALUE"].ToString()),
                        GOAL_ID = int.Parse(read["GOAL_ID"].ToString()),
                        SCORES = read["SCORES"] != DBNull.Value ? read["SCORES"].ToString() : string.Empty,
                        FINAL_SCORE = double.Parse(read["FINAL_SCORE"].ToString()),
                        DATE_CREATED = DateTime.Parse(read["DATE_CREATED"].ToString()),
                        SUBMISSION_ID = int.Parse(read["SUBMISSION_ID"].ToString()),
                        WEIGHT = int.Parse(read["WEIGHT"].ToString()),
                        UNIT_MEASURE_NAME = read["UNIT_MEASURE"].ToString(),
                        TARGET = read["TARGET"] != DBNull.Value ? read["TARGET"].ToString() : string.Empty,
                        GOAL_DESCRIPTION = read["GOAL_DESCRIPTION"].ToString(),
                        PERIOD_DATE = DateTime.Parse(read["PERIOD_DATE"].ToString()),
                        PERSPECTIVE_NAME = read["PERSPECTIVE_NAME"].ToString(),
                        OBJECTIVE = read["OBJECTIVE"].ToString(),
                        PERSPECTIVE_ID = int.Parse(read["PERSPECTIVE_ID"].ToString()),
                        FULL_NAME = (criteriaId == Util.getTypeDefinitionsRolesTypeID()) ? read["NAME"].ToString() : "",
                        USER_ID = (criteriaId == Util.getTypeDefinitionsRolesTypeID()) ? int.Parse(read["USERID"].ToString()) : 0,
                        ORGUNIT_NAME = (criteriaId == Util.getTypeDefinitionsOrgUnitsTypeID()) ? read["ORGUNIT_NAME"].ToString() : "",
                        ORG_ID = (criteriaId == Util.getTypeDefinitionsOrgUnitsTypeID()) ? int.Parse(read["ORGUNIT_ID"].ToString()) : 0,
                        PROJECT_NAME = (criteriaId == Util.getTypeDefinitionsProjectID()) ? read["PROJECT_NAME"].ToString() : "",
                        PROJECT_ID = (criteriaId == Util.getTypeDefinitionsProjectID()) ? int.Parse(read["PROJECT_ID"].ToString()) : 0
                    });
                }
                return score;
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
            finally
            {
                read.Dispose();
                read.Close();
                con.Dispose();
                con.Close();
            }
        }

        public List<scores> getSubmittedScores(submissionWorkflow _flow)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            SqlDataReader read = null;
            try
            {
                List<scores> company = new List<scores>();
                read = SqlHelper.ExecuteReader(con, "PROC_GET_SCORES_BY_OBJECT", _flow.CRITERIA_TYPE_ID, _flow.SELECTED_ITEM_VALUE, _flow.ID);
                while (read.Read())
                {
                    company.Add(new scores
                    {
                        ID = read["ID"] == DBNull.Value ? 0 : int.Parse(read["ID"].ToString()),
                        CRITERIA_TYPE_ID = read["CRITERIA_TYPE_ID"] == DBNull.Value ? 0 : int.Parse(read["CRITERIA_TYPE_ID"].ToString()),
                        SELECTED_ITEM_VALUE = read["SELECTED_ITEM_VALUE"] == DBNull.Value ? 0 : int.Parse(read["SELECTED_ITEM_VALUE"].ToString()),
                        GOAL_ID = read["GOAL_ID"] == DBNull.Value ? 0 : int.Parse(read["GOAL_ID"].ToString()),
                        SCORES = read["SCORES"] == DBNull.Value ? string.Empty : read["SCORES"].ToString(),
                        FINAL_SCORE = read["FINAL_SCORE"] == DBNull.Value ? 0 : double.Parse(read["FINAL_SCORE"].ToString()),
                        DATE_CREATED = read["DATE_CREATED"] == DBNull.Value ? DateTime.Now : DateTime.Parse(read["DATE_CREATED"].ToString()),
                        SUBMISSION_ID = read["SUBMISSION_ID"] == DBNull.Value ? 0 : int.Parse(read["SUBMISSION_ID"].ToString()),
                        WEIGHT = read["WEIGHT"] == DBNull.Value ? 0 : int.Parse(read["WEIGHT"].ToString()),
                        UNIT_MEASURE = read["UNIT_MEASURE"] == DBNull.Value ? 0 : int.Parse(read["UNIT_MEASURE"].ToString()),
                        TARGET = read["TARGET"] == DBNull.Value ? string.Empty : read["TARGET"].ToString(),
                        GOAL_DESCRIPTION = read["GOAL_DESCRIPTION"] == DBNull.Value ? string.Empty : read["GOAL_DESCRIPTION"].ToString(),
                        PERIOD_DATE = DateTime.Parse(read["PERIOD_DATE"].ToString())
                    });
                }
                return company;
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
            finally
            {
                read.Dispose();
                read.Close();
                con.Dispose();
                con.Close();
            }
        }

        public int updateWorkflowStatus(submissionWorkflow _flow)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try
            {
                return (int)SqlHelper.ExecuteScalar(con, "PROC_UPDATE_SCORECARD_STATUS",
                    _flow.ID,
                    _flow.STATUS
                 );
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return 0;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public int createSubmissionWorkflow(submissionWorkflow _workflow)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try
            {
                return (int)SqlHelper.ExecuteScalar(con, "PROC_ADD_SUBMISSIONS",
                    _workflow.SELECTED_ITEM_VALUE,
                    _workflow.CRITERIA_TYPE_ID,
                    _workflow.STATUS,
                    _workflow.APPROVER_ID
                 );
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return 0;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public int createNewScore(scores _scores)
        {

            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try
            {
                return (int)SqlHelper.ExecuteScalar(con, "PROC_ADD_SCORES",
                    _scores.CRITERIA_TYPE_ID,
                    _scores.SELECTED_ITEM_VALUE,
                    _scores.GOAL_ID,
                    _scores.SCORES,
                    _scores.FINAL_SCORE,
                    _scores.SUBMISSION_ID,
                    _scores.PERIOD_DATE
                 );
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return 0;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public int deleteScoresByGoalID(goals _goal)
        {

            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try
            {
                return SqlHelper.ExecuteNonQuery(con, "PROC_DELETE_ALL_SCORES_BY_GOAL_ID",
                  _goal.ID, _goal.PERSPECTIVE
                 );
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return 0;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public List<submissionWorkflow> getApprovalWorkflowItems(submissionWorkflow _approver)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            SqlDataReader read = null;
            try
            {
                List<submissionWorkflow> attachments = new List<submissionWorkflow>();
                read = SqlHelper.ExecuteReader(con, "PROC_GET_ALL_SUBMISSIONS_BY_APPROVER_ID", _approver.ID);
                while (read.Read())
                {
                    attachments.Add(new submissionWorkflow
                    {
                        ID = read.GetInt32(0),
                        SELECTED_ITEM_VALUE = read.GetInt32(1),
                        CRITERIA_TYPE_ID = read.GetInt32(2),
                        APPROVER_ID = read.GetInt32(3),
                        STATUS = read.GetInt32(4),
                        DATE_SUBMITTED = read.GetDateTime(5),
                        COMPANY_ID = read.GetInt32(6)
                    });
                }
                return attachments;
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
            finally
            {
                read.Dispose();
                read.Close();
                con.Dispose();
                con.Close();
            }
        }

        public List<submissionWorkflow> getApprovalWorkflowItems()
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            SqlDataReader read = null;
            try
            {
                List<submissionWorkflow> attachments = new List<submissionWorkflow>();
                read = SqlHelper.ExecuteReader(con, "PROC_GET_ALL_SUBMISSIONS");
                while (read.Read())
                {
                    attachments.Add(new submissionWorkflow
                    {
                        ID = read.GetInt32(0),
                        SELECTED_ITEM_VALUE = read.GetInt32(1),
                        CRITERIA_TYPE_ID = read.GetInt32(2),
                        APPROVER_ID = read.GetInt32(3),
                        STATUS = read.GetInt32(4),
                        DATE_SUBMITTED = read.GetDateTime(5),
                        COMPANY_ID = read.GetInt32(6)
                    });
                }
                return attachments;
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
            finally
            {
                read.Dispose();
                read.Close();
                con.Dispose();
                con.Close();
            }
        }

        public int createAttachment(attachmentDocuments _attachment)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try
            {
                return SqlHelper.ExecuteNonQuery(con, "PROC_ADD_ATTACHMENTS",
                  _attachment.ATTACHMENT_NAME, _attachment.SUBMISSION_ID
                 );
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return 0;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public List<attachmentDocuments> getAllAttachments(submissionWorkflow _wrkflow)
        {

            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            SqlDataReader read = null;
            try
            {
                List<attachmentDocuments> attachments = new List<attachmentDocuments>();
                read = SqlHelper.ExecuteReader(con, "PROC_GET_ALL_ATTACHMENTS", _wrkflow.ID);
                while (read.Read())
                {
                    attachments.Add(new attachmentDocuments
                    {
                        ID = read.GetInt32(0),
                        ATTACHMENT_NAME = read.GetString(1),
                        SUBMISSION_ID = read.GetInt32(2)
                    });
                }
                return attachments;
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
            finally
            {
                read.Dispose();
                read.Close();
                con.Dispose();
                con.Close();
            }
        }

        public totals getSubmittedScoreTotals(submissionWorkflow _flow)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            SqlDataReader read = null;
            try
            {
                List<totals> totals = new List<totals>();
                read = SqlHelper.ExecuteReader(con, "PROC_GET_SCORES_BY_OBJECT_TOTALS", _flow.ID);
                while (read.Read())
                {
                    totals.Add(new totals
                    {
                        SUM_FINAL_SCORE = read.GetInt32(0),
                        SUM_WEIGHT = read.GetInt32(1),
                        PERCENTAGE = read.GetInt32(2)
                    });
                }
                return totals[0];
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
            finally
            {
                read.Dispose();
                read.Close();
                con.Dispose();
                con.Close();
            }
        }


        #endregion
    }
}