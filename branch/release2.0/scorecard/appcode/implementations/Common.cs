﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Data;
using Microsoft.VisualBasic;

namespace scorecard.implementations
{
    public static class Common
    {
        #region Date Methods

        public static string GetCustomDateFormat(object dateVal, string separator)
        {
            if (dateVal == DBNull.Value) return "";
            DateTime date = DateTime.Parse(dateVal.ToString());
            return string.Format("{0}{3}{1}{3}{2}", date.Day, date.Month, date.Year, separator);
        }

        public static bool IsFutureDate(DateTime dateVal)
        {
            return (dateVal.Subtract(DateTime.Now).TotalDays > 0) ? true : false;
        }

        public static bool IsFutureDate(DateTime customDate, DateTime currentDate)
        {
            return (customDate.Subtract(currentDate).TotalDays > 0) ? true : false;
        }

        public static bool FromDateIsGreater(DateTime fromDate, DateTime currentDate)
        {
            return (fromDate.Subtract(currentDate).TotalDays > 0) ? true : false;
        }

        public static bool IsWeekendDate(DateTime dateVal)
        {
            return (dateVal.DayOfWeek == DayOfWeek.Saturday ||
                dateVal.DayOfWeek == DayOfWeek.Sunday) ? true : false;
        }

        public static string[] GetMonthsFullNames()
        {
            string[] months = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
            return months;
        }

        public static bool IsLeapYear(int year)
        {
            bool isLeapYr = (((year % 4) == 0) && ((year % 100) != 0) || ((year % 400) == 0));
            return isLeapYr;
        }

        public static int GetMonthLastDay(int monthNumber, int year)
        {
            int[] lastDayOfTheMonth = (IsLeapYear(year)) ? new int[] { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 } : new int[] { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 }; ;
            return lastDayOfTheMonth[monthNumber - 1];
        }

        public static string[] GetMonthsShortNames()
        {
            string[] months = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
            return months;
        }

        public static string GetMonthName(int month, bool shortMonth)
        {
            string[] months = shortMonth ? GetMonthsShortNames() : GetMonthsFullNames();
            return months[month - 1];
        }

        public static string GetDescriptiveDate(int day, int month, int year, bool shortMonth)
        {
            return string.Format("{0} {1} {2}", day, GetMonthName(month, shortMonth), year);
        }

        public static string GetDescriptiveDate(object tempDate, bool shortMonth)
        {
            if (tempDate == DBNull.Value) return "";
            DateTime dateVal = DateTime.Parse(tempDate.ToString());

            return GetDescriptiveDate(dateVal.Day, dateVal.Month, dateVal.Year, shortMonth);
        }

        public static string GetDescriptiveDateWithTime(object tempDate, bool shortMonth)
        {
            if (tempDate == DBNull.Value) return "";
            DateTime dateVal = DateTime.Parse(tempDate.ToString());

            return string.Format("{0} : {1}", GetDescriptiveDate(dateVal.Day, dateVal.Month, dateVal.Year, shortMonth), dateVal.TimeOfDay);
        }

        public static string GetDateWithTime(object tempDate)
        {
            if (tempDate == DBNull.Value) return "";
            return string.Format("{0:dd/MM/yyyy HH:mm:ss tt}", tempDate);
        }

        public static string GetDescriptiveDate(object date)
        {
            if (date == DBNull.Value) return "";
            return string.Format("{0:dd/MM/yyyy}", date);
        }

        public static string GetInternationalDateFormat(DateTime date)
        {
            return string.Format("{0:yyyy/MM/dd}", date);
        }

        public static string GetInternationalDateFormat(string dateVal, DateFormat inputFormat)
        {
            string[] tempDateVal = dateVal.Split("-,/, ".ToCharArray());
            string tempDate = "";

            if (tempDateVal.Length != 3) return tempDate;
            switch (inputFormat)
            {
                case DateFormat.dayMonthYear:
                    tempDate = string.Format("{0}-{1}-{2}", tempDateVal[2], tempDateVal[1], tempDateVal[0]);
                    break;
                case DateFormat.monthDayYear:
                    tempDate = string.Format("{0}-{1}-{2}", tempDateVal[2], tempDateVal[0], tempDateVal[1]);
                    break;
                default:
                    tempDate = dateVal;
                    break;
            }

            return tempDate;
        }

        #endregion

        #region General Methods

        public static string GetBooleanYesNo(object bVal)
        {
            return (bVal == DBNull.Value) ? "No" : bool.Parse(bVal.ToString()) ? "Yes" : "No";
        }

        public static string GetXML(object[] dataVal, string rootElement, string recordElement, string dataElement)
        {
            StringBuilder strB = new StringBuilder(string.Format("<{0}>", rootElement));
            foreach (object val in dataVal)
                strB.Append(string.Format("<{0}><{1}>{2}</{1}></{0}>", recordElement, dataElement, val));
            strB.Append(string.Format("</{0}>", rootElement));
            return strB.ToString();
        }


        /// <summary>
        /// Gets custom XML format given data that is in DataTable form
        /// </summary>
        /// <param name="dT">DataTable with information that is to be converted into custom xml string</param>
        /// <param name="rootNodeName">Root node name</param>
        /// <param name="dataElement">Data Root Node name</param>
        /// <returns>string</returns>
        public static string GetXML(DataTable dT, string rootNodeName, string dataElement)
        {
            StringBuilder strB = new StringBuilder(string.Format("<{0}>", rootNodeName));
            foreach (DataRow Rw in dT.Rows)
            {
                strB.Append(string.Format("<{0}>", dataElement));
                for (int i = 0; i < dT.Columns.Count; i++)
                {
                    string fieldName = dT.Columns[i].ColumnName;
                    strB.Append(string.Format("<{0}>{1}</{0}>", fieldName, Rw[fieldName].ToString()));
                }
                strB.Append(string.Format("</{0}>", dataElement));
            }
            strB.Append(string.Format("</{0}>", rootNodeName));
            return strB.ToString();
        }

        public static string FormatNumber(object amountVal)
        {
            return Information.IsNumeric(amountVal) ? String.Format("{0:0,0.00}", amountVal) : string.Empty;
        }

        public static string FormatDecimalNumber(object amountVal)
        {
            return Information.IsNumeric(amountVal) ? Math.Round(decimal.Parse(amountVal.ToString()), 2).ToString() : string.Empty;
        }

        #endregion
    }

    public enum DateFormat
    {
        dayMonthYear,
        monthDayYear,
        yearMonthDay
    }
}