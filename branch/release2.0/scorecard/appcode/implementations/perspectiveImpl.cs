﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using scorecard.entities;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using scorecard.interfaces;

namespace scorecard.implementations
{
    public class perspectiveImpl : Iperspective
    {
        public bool addPerspective(perspective obj)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try
            {
                int result = (int)SqlHelper.ExecuteScalar(con, "PROC_ADD_PERSPECTIVE",
                 obj.COMPANY_ID,
                 obj.PERSPECTIVE_NAME,
                 obj.PERSPECTIVE_DESC,
                 obj.CREATED_BY
                 );
                return result > 0 ? true : false;
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return false;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public bool updatePerspective(perspective obj)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try
            {
                int result = (int)SqlHelper.ExecuteScalar(con, "PROC_UPDATE_PERSPECTIVE",
                  obj.PERSPECTIVE_ID,
                 obj.PERSPECTIVE_NAME,
                 obj.PERSPECTIVE_DESC,
                 obj.UPDATE_BY
                 );
                return result > 0 ? true : false;
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return false;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public bool setPerspectiveStatus(perspective obj)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try
            {
                int result = (int)SqlHelper.ExecuteScalar(con, "PROC_DEACTIVATE_PERSPECTIVE",
                    obj.PERSPECTIVE_ID, obj.UPDATE_BY, obj.ACTIVE);
                return result > 0 ? true : false;
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return false;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public perspective getPerspective(int perspectiveId)
        {

            SqlConnection con = new SqlConnection(Util.GetconnectionString());

            try
            {
                DateTime? defDate = null;
                using (SqlDataReader rw = SqlHelper.ExecuteReader(con, "PROC_GET_PERSPECTIVE", perspectiveId))
                {
                    if (rw.HasRows)
                    {
                        rw.Read();
                        return new perspective
                        {
                            ACTIVE = bool.Parse(rw["ACTIVE"].ToString()),
                            COMPANY_ID = int.Parse(rw["COMPANY_ID"].ToString()),
                            CREATED_BY = rw["CREATED_BY"].ToString(),
                            DATE_CREATED = DateTime.Parse(rw["DATE_CREATED"].ToString()),
                            DATE_UPDATED = (rw["DATE_UPDATED"] != DBNull.Value) ? DateTime.Parse(rw["DATE_UPDATED"].ToString()) : defDate,
                            PERSPECTIVE_ID = perspectiveId,
                            PERSPECTIVE_NAME = rw["PERSPECTIVE_NAME"].ToString(),
                            PERSPECTIVE_DESC = (rw["PERSPECTIVE_DESC"] != DBNull.Value) ? rw["PERSPECTIVE_DESC"].ToString() : string.Empty,
                            UPDATE_BY = (rw["UPDATE_BY"] != DBNull.Value) ? rw["UPDATE_BY"].ToString() : string.Empty
                        };
                    }
                    else return new perspective();
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return new perspective();
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public List<perspective> getPerspectives(int companyId, bool active)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try
            {
                List<perspective> proj = new List<perspective>();
                DateTime? defDate = null;

                using (SqlDataReader rw = SqlHelper.ExecuteReader(con, "PROC_GET_COMPANYPERSPECTIVES", companyId, active))
                {
                    while (rw.Read())
                        proj.Add(new perspective
                        {
                            ACTIVE = bool.Parse(rw["ACTIVE"].ToString()),
                            COMPANY_ID = int.Parse(rw["COMPANY_ID"].ToString()),
                            CREATED_BY = rw["CREATED_BY"].ToString(),
                            DATE_CREATED = DateTime.Parse(rw["DATE_CREATED"].ToString()),
                            DATE_UPDATED = (rw["DATE_UPDATED"] != DBNull.Value) ? DateTime.Parse(rw["DATE_UPDATED"].ToString()) : defDate,
                            PERSPECTIVE_ID = int.Parse(rw["PERSPECTIVE_ID"].ToString()),
                            PERSPECTIVE_NAME = rw["PERSPECTIVE_NAME"].ToString(),
                            PERSPECTIVE_DESC = (rw["PERSPECTIVE_DESC"] != DBNull.Value) ? rw["PERSPECTIVE_DESC"].ToString() : string.Empty,
                            UPDATE_BY = (rw["UPDATE_BY"] != DBNull.Value) ? rw["UPDATE_BY"].ToString() : string.Empty
                        });
                }
                return proj;
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return new List<perspective>();
            }
            finally
            {
                con.Dispose();
                con.Close();
            }
        }
    }
}