﻿using System;
using System.Collections.Generic;
using scorecard.implementations;
using System.Data;

namespace HR.Leave
{
    public class LeaveAccrualBL : ILeaveAccrual
    {
        #region Properties

        public string ConnectionString
        {
            get { return Util.GetconnectionString(); }
        }

        public LeaveAccrualDAL Dal
        {
            get { return new LeaveAccrualDAL(ConnectionString); }
        }

        public ILeaveAccrualTime BlTime
        {
            get { return new LeaveAccrualTimeBL(); }
        }

        public IWorkingWeek BlWeek
        {
            get { return new WorkingWeekBL(); }
        }

        public ILeaveType BlLeave
        {
            get { return new LeaveTypeBL(); }
        }

        #endregion

        #region Default Constructors

        public LeaveAccrualBL() { }

        #endregion

        #region Public Methods & Functions

        public bool AddLeaveAccrual(LeaveAccrual obj)
        {
            int result = Dal.Save(obj);
            return (result > 0) ? true : false;
        }

        public bool UpdateLeaveAccrual(LeaveAccrual obj)
        {
            int result = Dal.Update(obj);
            return (result > 0) ? true : false;
        }

        public bool Delete(int leaveAccrualId, string updatedBy)
        {
            int result = Dal.Deactivate(leaveAccrualId, updatedBy);
            return (result > 0) ? true : false;
        }

        LeaveAccrual GetRecord(DataRow rw)
        {
            DateTime? defDate = null;
            return new LeaveAccrual
            {
                Active = bool.Parse(rw["active"].ToString()),
                CreatedBy = rw["createdby"].ToString(),
                DateCreated = DateTime.Parse(rw["datecreated"].ToString()),
                DateUpdated = (rw["dateupdated"] != DBNull.Value) ? DateTime.Parse(rw["dateupdated"].ToString()) : defDate,
                UpdatedBy = (rw["updatedby"] != DBNull.Value) ? rw["updatedby"].ToString() : string.Empty,
                AccrualTimeId = int.Parse(rw["accrualTimeId"].ToString()),
                AccrualRate = decimal.Parse(rw["accrualRate"].ToString()),
                LeaveAccrualId = int.Parse(rw["leaveAccrualId"].ToString()),
                LeaveId = int.Parse(rw["leaveId"].ToString()),
                WorkingWeekId = int.Parse(rw["WorkingWeekId"].ToString()),
                MaximumThreshold = decimal.Parse(rw["maximumThreshold"].ToString()),
                TimeAccrual = BlTime.GetRecord(rw),
                WorkWeek = BlWeek.GetRecord(rw),
                Leave = BlLeave.GetRecord(rw)
            };
        }

        public LeaveAccrual GetById(int leaveAccrualId)
        {
            DataTable dT = Dal.GetById(leaveAccrualId);
            if (dT.Rows.Count <= 0) return new LeaveAccrual();
            DataRow rw = dT.Rows[0];
            return GetRecord(rw);
        }

        public List<LeaveAccrual> GetByStatus(int companyId, bool active)
        {
            List<LeaveAccrual> obj = new List<LeaveAccrual>();
            DataTable dT = Dal.Get(companyId, active);
            foreach (DataRow rw in dT.Rows)
                obj.Add(GetRecord(rw));
            return obj;
        }

        public List<LeaveAccrual> GetAll(int companyId)
        {
            List<LeaveAccrual> obj = new List<LeaveAccrual>();
            DataTable dT = Dal.GetAll(companyId);
            foreach (DataRow rw in dT.Rows)
                obj.Add(GetRecord(rw));
            return obj;
        }

        #endregion
    }
}