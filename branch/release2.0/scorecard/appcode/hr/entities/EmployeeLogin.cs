﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR.Human.Resources.Security
{
    public class EmployeeLogin
    {
        public int EmployeeId { get; set; }
        public int? RoleId { get; set; }

        public string UserName { get; set; }
        public string Pass { get; set; }

        public bool Active { get; set; }
        public Role UserRole { get; set; }
    }

    public class Role
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
    }
}