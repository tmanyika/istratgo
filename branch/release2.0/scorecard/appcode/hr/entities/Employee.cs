﻿using System;
using scorecard.entities;
using HR.Leave;
using HR.Human.Resources.Security;
using scorecard.welcome;

namespace HR.Human.Resources
{
    public class Employee : EmployeeLogin
    {
        public int? LineManagerEmployeeId { get; set; }
        public int OrgUnitId { get; set; }
        public int? JobTitleId { get; set; }
        public int? StatusId { get; set; }
        public int? WorkingWeekId { get; set; }
        public int? CountryId { get; set; }
        public int CompanyId { get; set; }

        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string EmailAddress { get; set; }
        public string EmployeeNo { get; set; }
        public string Cellphone { get; set; }
        public string BusTelephone { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }

        public JobTitle Position { get; set; }
        public WorkingWeek WekinWeek { get; set; }
        public Orgunit Organisation { get; set; }
        public EmployeeStatus Status { get; set; }
        public Welcome Start { get; set; }
    }
}