﻿using System;

namespace HR.Leave
{
    public class LeaveAccrualTime
    {
        public int AccrualTimeId { get; set; }
        public int TimeValue { get; set; }

        public string TimeName { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public bool Active { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
    }
}