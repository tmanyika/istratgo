﻿using System;
using System.Collections.Generic;
using System.Data;

namespace HR.Leave
{
    public interface IWorkingWeek
    {
        bool AddWorkingWeek(WorkingWeek obj);
        bool UpdateWorkingWeek(WorkingWeek obj);
        bool Delete(int leaveId, string updatedBy);

        WorkingWeek GetById(int workingWeekId);
        WorkingWeek GetRecord(DataRow rw);
        List<WorkingWeek> GetByStatus(int companyId, bool active);
        List<WorkingWeek> GetByCompanyId(int companyId);
    }
}
