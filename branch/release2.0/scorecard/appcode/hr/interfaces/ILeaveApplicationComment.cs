﻿using System;
using System.Collections.Generic;
using System.Data;

namespace HR.Leave.Documents
{
    public interface ILeaveApplicationComment
    {
        bool AddComment(LeaveApplicationComment obj);
        bool DeleteComments(int commentId, string updatedBy);

        DataTable GetSchema();
        LeaveApplicationComment GetComment(int commentId);
        List<LeaveApplicationComment> GetComments(int applicationId);
    }
}
