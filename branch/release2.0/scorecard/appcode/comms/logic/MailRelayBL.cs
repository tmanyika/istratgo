﻿using System;
using System.Collections.Generic;
using scorecard.implementations;
using System.Data;
using System.Web;
using System.Net.Mail;
using HR.Human.Resources;
using System.Net;
using System.Text;

namespace System.Email.Communication
{
    public class MailRelayBL : IMailRelay
    {
        #region Properties

        public string ConnectionString
        {
            get { return Util.GetconnectionString(); }
        }

        public MailRelayDAL Dal
        {
            get { return new MailRelayDAL(ConnectionString); }
        }

        public IMailServer IMail
        {
            get { return new MailServerBL(); }
        }

        public IMailBatch IBatch
        {
            get { return new MailBatchBL(); }
        }

        #endregion

        #region Default Constructors

        public MailRelayBL() { }

        #endregion

        #region Public Methods & Functions

        public MailRelay GetBatchMail(int batchId)
        {
            var Batch = IBatch.GetById(batchId);
            SmtpClient Smtp = new SmtpClient
            {
                Host = Batch.Server.SmtpServer,
                Port = Batch.Server.Port,
                Credentials = new NetworkCredential(Batch.Server.UserName, Batch.Server.PIN)
            };

            return new MailRelay
            {
                AddressList = IBatch.GetRecipients(batchId),
                MailContent = Batch.MainTemplate.Replace("{1}", Batch.MailContent),
                MailClient = Smtp,
                MailSubject = Batch.MailSubject,
                Sender = new MailAddress(Batch.Server.UserName, Batch.Server.AccountName)
            };
        }

        public bool SendMail(MailRelay obj, out string result)
        {
            result = Messages.GetEmailFailedMessage();
            try
            {
                DataTable dT = Dal.Get(obj.MailId);

                if (dT.Rows.Count <= 0) return false;

                MailRelay info = GetRow(dT.Rows[0]);

                string subject = info.MailSubject;
                string body = info.MailContent;

                if (obj.Data != null)
                    for (int i = 0; i < obj.Data.Length; i++)
                    {
                        string tag = string.Format("{0}{1}{2}", "{", i + 1, "}");
                        body = body.Replace(tag, obj.Data[i].ToString());
                    }

                if (obj.SubjectData != null)
                {
                    for (int i = 0; i < obj.Data.Length; i++)
                    {
                        string tag = string.Format("{0}{1}{2}", "{", i, "}");
                        subject = subject.Replace(tag, obj.SubjectData[i].ToString());
                    }
                    info.MailSubject = subject;
                }

                MailMessage eml = new MailMessage
                {
                    From = info.Sender,
                    IsBodyHtml = true,
                    Subject = info.MailSubject
                };

                foreach (MailAddress recip in obj.Recipient)
                {
                    string emlBody = body.Replace("{0}", recip.DisplayName);
                    eml.To.Add(recip);
                    eml.Body = emlBody;
                    eml.IsBodyHtml = true;
                    info.MailClient.Send(eml);
                    eml.To.Clear();
                }
                result = Messages.GetEmailSentMessage();
            }
            catch (Exception ex)
            {
                result = Messages.GetEmailFailedMessage();
                Util.LogErrors(ex);
                return false;
            }
            return true;
        }

        public bool SendMail(int batchId, bool recordTrail, out string result)
        {
            result = Messages.GetEmailFailedMessage();
            try
            {
                MailRelay info = GetBatchMail(batchId);
                if (info.AddressList.Count <= 0)
                {
                    result = Messages.GetNoRecipients();
                    return false;
                }

                string body = info.MailContent;
                MailMessage eml = new MailMessage
                {
                    From = info.Sender,
                    IsBodyHtml = true,
                    Subject = info.MailSubject
                };

                SmtpClient client = new SmtpClient();
                client.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis;
                StringBuilder strB = new StringBuilder();
                foreach (Employee recip in info.AddressList)
                {
                    string emlBody = body.Replace("{0}", recip.FullName);
                    eml.To.Add(new MailAddress(recip.EmailAddress, recip.FullName));
                    eml.Body = emlBody;
                    eml.IsBodyHtml = true;
                    client.Send(eml);//info.MailClient.Send(eml);
                    strB.Append(string.Format("<Data><BatchId>{0}</BatchId><Email>{1}</Email><EmployeeId>{2}</EmployeeId></Data>", batchId, recip.EmailAddress, recip.EmployeeId));
                    eml.To.Clear();
                }
                string xmlData = (strB.ToString() == string.Empty) ? string.Empty : string.Format("<List>{0}</List>", strB.ToString());
                if (xmlData != string.Empty) IBatch.AddMailingList(batchId, xmlData);
                result = Messages.GetEmailSentMessage();
            }
            catch (Exception ex)
            {
                result = string.Format("{0}. {1}", Messages.GetEmailFailedMessage(), Messages.GetErrorMessage());
                Util.LogErrors(ex);
                return false;
            }
            return true;
        }

        MailRelay GetRow(DataRow rw)
        {
            return new MailRelay
            {
                MailId = int.Parse(rw["mailId"].ToString()),
                MailContent = rw["mailContent"].ToString(),
                MailSubject = rw["mailSubject"].ToString(),
                Sender = new MailAddress(Util.getFromEmailAddress(), Util.getEmailDisplayName()),
                MailClient = new SmtpClient()
            };
        }

        #endregion
    }
}
