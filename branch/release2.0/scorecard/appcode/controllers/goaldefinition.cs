﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using scorecard.interfaces;
using scorecard.entities;
using scorecard.implementations;


/* @Copy Albertoncaffeine.net */

namespace scorecard.controllers
{
    public class goaldefinition
    {
        Igoals goalsDef;
        public settingsmanager appsettings;
        public jobtitlesdefinitions jobtitles;
        public structurecontroller orgstructure;

        public goaldefinition(Igoals _goalsDef)
        {
            goalsDef = _goalsDef;
            appsettings = new settingsmanager(new applicationsettingsImpl());
            jobtitles = new jobtitlesdefinitions(new jobtitleImpl());
            orgstructure = new structurecontroller(new structureImpl());
        }
        public int defineStrategicGoal(strategic _goalDef)
        {
            return goalsDef.defineStrategicGoal(_goalDef);
        }
        public int updateStrategicGoal(strategic _goalDef)
        {
            return goalsDef.updateStrategicGoal(_goalDef);
        }
        public int deleteStrategicGoal(strategic _goalDef)
        {
            return goalsDef.deleteStrategicGoal(_goalDef);
        }
        public List<strategic> listStrategicGoals(company _company)
        {
            return goalsDef.listStrategicGoals(_company);
        }
        public int defineGeneralGoal(goals _goalDef)
        {
            return goalsDef.defineGeneralGoal(_goalDef);
        }
        public int updateGeneralGoal(goals _goalDef)
        {
            return goalsDef.updateGeneralGoal(_goalDef);
        }
        public int deleteGeneralGoal(goals _goalDef)
        {
            return goalsDef.deleteGeneralGoal(_goalDef);
        }
        public List<goals> listGeneralGoals(company _company)
        {
            return goalsDef.listGeneralGoals(_company);
        }

        public goals getGoalById(int id)
        {
            return goalsDef.getGoal(id);
        }
    }
}