﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using scorecard.interfaces;
using scorecard.entities;

/* @Copy Albertoncaffeine.net */

namespace scorecard.controllers
{
    public class scoresmanager
    {
        Iscores _scoresManager;

        public scoresmanager(Iscores _scoreInstance)
        {
            _scoresManager = _scoreInstance;
        }
        public List<scores> getScoresByGoalID(goals _goal)
        {
            return _scoresManager.getScoresByGoalID(_goal);
        }
        public int createNewScore(scores _scores)
        {
            return _scoresManager.createNewScore(_scores);
        }
        public int deleteScoresByGoalID(goals _goal)
        {
            return _scoresManager.deleteScoresByGoalID(_goal);
        }
        public int createWorkFlow(submissionWorkflow _workflow)
        {
            return _scoresManager.createSubmissionWorkflow(_workflow);
        }
        public List<submissionWorkflow> getApprovalWorkflowItems(submissionWorkflow _approver)
        {
            return _scoresManager.getApprovalWorkflowItems(_approver);
        }
        public List<submissionWorkflow> getApprovalWorkflowItems()
        {
            return _scoresManager.getApprovalWorkflowItems();
        }
        public int createAttachment(attachmentDocuments _attachment)
        {
            return _scoresManager.createAttachment(_attachment);
        }
        public List<attachmentDocuments> getAllAttachments(submissionWorkflow _wrkflow)
        {
            return _scoresManager.getAllAttachments(_wrkflow);
        }
        public List<scores> getSubmittedScores(submissionWorkflow _flow)
        {
            return _scoresManager.getSubmittedScores(_flow);
        }
        public int updateWorkflowStatus(submissionWorkflow _flow)
        {
            return _scoresManager.updateWorkflowStatus(_flow);
        }
        public totals getSubmittedScoreTotals(submissionWorkflow _flow)
        {
            return _scoresManager.getSubmittedScoreTotals(_flow);
        }

        public List<scores> getScoreDateValues(int criteriaId, int itemValueId)
        {
            return _scoresManager.getScoreDates(criteriaId, itemValueId);
        }

        public List<scores> getReportDateValues(int criteriaId, string xmlData)
        {
            return _scoresManager.getReportDates(criteriaId, xmlData);
        }

        public List<scores> getUsedItemDates(int criteriaId, int itemValueId)
        {
            return _scoresManager.getUsedScoreDates(criteriaId, itemValueId);
        }

        public List<scores> getScoresReport(int criteriaId, int itemValueId, string sdate, string edate)
        {
            return _scoresManager.getScoreReport(criteriaId, itemValueId, sdate, edate);
        }

        public List<scores> getScoresReport(int criteriaId, int itemValueId, string date)
        {
            return _scoresManager.getScoreReport(criteriaId, itemValueId, date);
        }

        public List<PerformanceReport> getDepartmentUsersScorePerformanceReport(int criteriaId, int itemValueId, string sdate, string edate)
        {
            return _scoresManager.getDepartmentUsersScorePerformanceReport(criteriaId, itemValueId, sdate, edate);
        }

        public List<PerformanceReport> getDepartmentScorePerformanceReport(int criteriaId, int itemValueId, string sdate, string edate, bool isCompany)
        {
            return _scoresManager.getDepartmentScorePerformanceReport(criteriaId, itemValueId, sdate, edate, isCompany);
        }

        public List<PerformanceReport> getEmployeeScorePerformanceReport(int criteriaId, int itemValueId, string sdate, string edate)
        {
            return _scoresManager.getEmployeeScorePerformanceReport(criteriaId, itemValueId, sdate, edate);
        }

        public List<PerformanceReport> getJobTitleScorePerformanceReport(int criteriaId, int itemValueId, string sdate, string edate)
        {
            return _scoresManager.getJobTitleScorePerformanceReport(criteriaId, itemValueId, sdate, edate);
        }

        public List<scores> getComparisonReport(int criteriaId, string areaValueId, string date)
        {
            return _scoresManager.getAggregateReport(criteriaId, areaValueId, date);
        }
    }
}