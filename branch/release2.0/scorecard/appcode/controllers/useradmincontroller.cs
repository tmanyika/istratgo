﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using scorecard.interfaces;
using scorecard.entities;
using scorecard.implementations;

/* @Copy Albertoncaffeine.net */

namespace scorecard.controllers
{
    [Serializable]
    public class useradmincontroller
    {
        Iuser user;
        Iorgunit _orgunit;
        public settingsmanager appsettings;

        public useradmincontroller(Iuser _user)
        {
            user = _user;
            _orgunit = new structureImpl();
            appsettings = new settingsmanager(new applicationsettingsImpl());
        }

        public int addUserAccount(useradmin _user, out string errorMessage)
        {
            return user.addUserAccount(_user,out errorMessage);
        }

        public int updateUserAccount(useradmin _user)
        {
            return user.updateUserAccount(_user);
        }
        public List<useradmin> getAllCompanyUsers(company _company)
        {
            return user.getAllCompanyUsers(_company);
        }

        public List<useradmin> getAllUsers()
        {
            return user.getAllUsers();
        }

        public useradmin getUserAccountInformation(useradmin _user)
        {
            return user.getUserAccountInformation(_user);
        }

        public useradmin getUserAccountInformationById(useradmin _user)
        {
            return user.getUserAccountById(_user);
        }

        public int deactivateUserAccountI(useradmin _user)
        {
            return user.deactivateUserAccount(_user);
        }

        public int updatePassword(useradmin _user)
        {
            return user.updatePassword(_user);
        }

        public useradmin loginUser(string userName, string password)
        {
            try
            {
                var user = getAllUsers().Where(u => u.LOGIN_ID.Trim() == userName && u.PASSWD.Trim() == password).ToList();
                if (user.Count == 1)
                {
                    var user_info = user.Single();
                    user_info.COMPANY_INFO = _orgunit.getOrgunitDetails(new Orgunit { ID = user_info.ORG_ID });
                    return user_info;
                }
                return null;
            }
            catch (Exception e)
            {
                scorecard.implementations.Util.LogErrors(e);
                return null;
            }
        }

        public useradmin getLineManagerUserAccount(int areaId, int areaValueId)
        {
            return user.getLineManagerUserAccount(areaId, areaValueId);
        }
    }
}