﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ui/inside.Master" AutoEventWireup="true" CodeBehind="graphicalreporting.aspx.cs" Inherits="scorecard.graphicalreporting" %>
<%@ Register src="controls/orgstructure.ascx" tagname="orgstructure" tagprefix="uc1" %>
<%@ Register src="controls/graphReportForm.ascx" tagname="graphReportForm" tagprefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <uc2:graphReportForm ID="graphReportForm1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="OrgStructure" runat="server">
    <uc1:orgstructure ID="orgstructure1" runat="server" />
</asp:Content>
