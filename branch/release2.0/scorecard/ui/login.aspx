<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="scorecard.ui.login" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="controls/login.ascx" TagName="login" TagPrefix="uc1" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!-- Page title -->
    <title>iStratgo </title>
    <!-- End of Page title -->
    <!-- Libraries -->
    <link type="text/css" href="css/login.css" rel="stylesheet" />
    <link type="text/css" href="css/layout.css" rel="stylesheet" />
    <link type="text/css" href="css/smoothness/jquery-ui-1.7.2.custom.html" rel="stylesheet" />
    <link href="css/modalPopup.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
    <script type="text/javascript" src="js/easyTooltip.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.7.2.custom.min.js"></script>
    <style type="text/css">
        .modalPopup
        {
            width: 600px !important;
            height: 300px !important;
            overflow: hidden !important;
        }
    </style>
    <!-- End of Libraries -->
</head>
<body>
    <form id="form1" runat="server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" CombineScripts="false"
        LoadScriptsBeforeUI="true">
    </asp:ToolkitScriptManager>
    <div id="container">
        <div class="logo">
            <a href="#">
                <img src="assets/logo.png" alt="" /></a>
        </div>
        <uc1:login ID="loginUser" runat="server" />
    </div>
    </form>
</body>
</html>
