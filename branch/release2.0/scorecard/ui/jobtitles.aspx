<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="jobtitles.aspx.cs" MasterPageFile="~/ui/inside.Master" Inherits="scorecard.ui.jobtitles" %>

<%@ Register src="controls/orgstructure.ascx" tagname="orgstructure" tagprefix="uc1" %>
<%@ Register src="controls/jobtitles.ascx" tagname="jobtitles" tagprefix="uc2" %>


<asp:Content ID="Content1" runat="server" contentplaceholderid="OrgStructure">
    <uc1:orgstructure ID="orgstructure1" runat="server" />
</asp:Content>
<asp:Content ID="Content2" runat="server" contentplaceholderid="MainContent">
    <uc2:jobtitles ID="jobtitlesDef" runat="server" />
</asp:Content>


