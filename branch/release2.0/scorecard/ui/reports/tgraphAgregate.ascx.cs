﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using scorecard.implementations;
using scorecard.controllers;
using scorecard.entities;
using vb = Microsoft.VisualBasic;
using System.Collections;


namespace scorecard.ui.reports
{
    public partial class tgraphAgregate : System.Web.UI.UserControl
    {
        #region Variables

        useradmincontroller usersDef;
        goaldefinition goalDef;
        scoresmanager scoresManager;
        structurecontroller orgunits;

        #endregion

        #region Properties

        string Date
        {
            get
            {
                return Request["dVal"].ToString();
            }
        }

        int AreaId
        {
            get
            {
                return int.Parse(Request["aid"].ToString());
            }
        }

        string AreaValueId
        {
            get
            {
                string[] dataVal = Request["idv"].ToString().Split(',');
                return Common.GetXML(dataVal, "Record", "Score", "Id");
            }
        }

        int CompanyId
        {
            get
            {
                return int.Parse(Request["cId"].ToString());
            }
        }
        #endregion

        #region Default Class Constructors

        public tgraphAgregate()
        {
            goalDef = new goaldefinition(new goalsImpl());
            orgunits = new structurecontroller(new structureImpl());
            usersDef = new useradmincontroller(new useradminImpl());
            scoresManager = new scoresmanager(new scorecard.implementations.scoresImpl());
        }

        #endregion

        #region Page Event Handling Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                fillInfo();
                loadData();
            }
        }

        #endregion

        #region Databinding Methods

        private void fillInfo()
        {
            try
            {
                string areaName = usersDef.appsettings.getApplicationSetting(new entities.applicationsettings { TYPE_ID = AreaId }).DESCRIPTION;
                lblCriteria.Text = areaName;
                lblName.Text = string.Format("{0} Comparison", areaName);
                lblDate.Text = Common.GetDescriptiveDate(Date, false);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        DataTable getData()
        {
            var objectives = scoresManager.getComparisonReport(AreaId, AreaValueId, Date);

            DataTable dT = new DataTable();
            dT.Columns.Add("Name", typeof(string));
            dT.Columns.Add("TargetValue", typeof(double));
            dT.Columns.Add("ActualValue", typeof(double));
            int prevId = 0;
            DataRow nRow;
            foreach (scores item in objectives)
            {
                double totalTarget = 0;
                double totalActual = 0;
                int nxtId = (AreaId == Util.getTypeDefinitionsRolesTypeID()) ? item.USER_ID :
                    (AreaId == Util.getTypeDefinitionsProjectID()) ? item.PROJECT_ID : item.ORG_ID;
                if (nxtId != prevId)
                {
                    int id = (AreaId == Util.getTypeDefinitionsRolesTypeID()) ? item.USER_ID :
                        (AreaId == Util.getTypeDefinitionsProjectID()) ? item.PROJECT_ID : item.ORG_ID;
                    string name = (AreaId == Util.getTypeDefinitionsRolesTypeID()) ? item.FULL_NAME :
                        (AreaId == Util.getTypeDefinitionsProjectID()) ? item.PROJECT_NAME : item.ORGUNIT_NAME;

                    totalTarget = (AreaId == Util.getTypeDefinitionsRolesTypeID()) ? objectives.Where(c => c.USER_ID == id).Sum(u => u.WEIGHT) :
                        (AreaId == Util.getTypeDefinitionsProjectID()) ? objectives.Where(c => c.PROJECT_ID == id).Sum(u => u.WEIGHT) : objectives.Where(c => c.ORG_ID == id).Sum(u => u.WEIGHT);
                    totalActual = (AreaId == Util.getTypeDefinitionsRolesTypeID()) ? Math.Round(objectives.Where(c => c.USER_ID == id).Sum(u => u.FINAL_SCORE), 2) :
                             (AreaId == Util.getTypeDefinitionsProjectID()) ? Math.Round(objectives.Where(c => c.PROJECT_ID == id).Sum(u => u.FINAL_SCORE), 2) : Math.Round(objectives.Where(c => c.ORG_ID == id).Sum(u => u.FINAL_SCORE), 2);

                    nRow = dT.NewRow();
                    nRow["Name"] = name;
                    nRow["TargetValue"] = totalTarget;
                    nRow["ActualValue"] = totalActual;
                    dT.Rows.Add(nRow);
                    nRow = null;
                }
                prevId = nxtId;
            }
            dT.AcceptChanges();
            return dT;
        }

        void loadData()
        {
            try
            {
                DataTable dT = getData();

                if (dT.Rows.Count >= Util.getGraphMinItems())
                {
                    RadChartAgg.Height = new Unit(Util.getGraphHeight());
                    RadChartAgg.Width = new Unit(Util.getGraphWidth());
                }
                settingsmanager obj = new settingsmanager(new applicationsettingsImpl());
                RadChartAgg.ChartTitle.TextBlock.Text = obj.getApplicationSetting(new applicationsettings { TYPE_ID = AreaId }).DESCRIPTION;
                RadChartAgg.DataSource = dT;
                RadChartAgg.DataBind();

            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        #endregion
    }
}