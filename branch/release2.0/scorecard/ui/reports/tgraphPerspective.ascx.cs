﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using scorecard.implementations;
using scorecard.entities;
using scorecard.controllers;
using System.Web.UI.DataVisualization.Charting;
using HR.Human.Resources;
using System.Collections;

namespace scorecard.ui.reports
{
    public partial class tgraphPerspective : System.Web.UI.UserControl
    {
        #region Variables

        useradmincontroller usersDef;
        goaldefinition goalDef;
        scoresmanager scoresManager;
        structurecontroller orgunits;

        #endregion

        #region Properties

        string Date
        {
            get
            {
                return Request["dVal"].ToString();
            }
        }

        int AreaId
        {
            get
            {
                return int.Parse(Request["aid"].ToString());
            }
        }

        int AreaValueId
        {
            get
            {
                return int.Parse(Request["idv"].ToString());
            }
        }

        #endregion

        #region Default Class Constructors

        public tgraphPerspective()
        {
            goalDef = new goaldefinition(new goalsImpl());
            orgunits = new structurecontroller(new structureImpl());
            usersDef = new useradmincontroller(new useradminImpl());
            scoresManager = new scoresmanager(new scorecard.implementations.scoresImpl());
        }

        #endregion

        #region Page Event Handling Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                fillInfo();
                loadData();
            }
        }

        #endregion

        #region Databinding Methods

        private void fillInfo()
        {
            try
            {
                lblCriteria.Text = usersDef.appsettings.getApplicationSetting(new entities.applicationsettings { TYPE_ID = AreaId }).NAME;
                lblName.Text = getName(AreaValueId, AreaId);
                lblDate.Text = Common.GetDescriptiveDate(Date, false);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        string getName(int id, int criteria)
        {
            try
            {
                if (id > 0)
                {
                    if (criteria == Util.getTypeDefinitionsRolesTypeID())
                    {
                        IEmployee emp = new EmployeeBL();
                        return emp.GetById(id).FullName;
                    }
                    else if (criteria == Util.getTypeDefinitionsProjectID()) return new projectmanagement(new projectImpl()).get(id).PROJECT_NAME;
                    else return orgunits.getOrgunitDetails(new Orgunit { ID = id }).ORGUNIT_NAME;
                }
            }
            catch (Exception e) { Util.LogErrors(e); }
            return "";
        }

        DataTable getData()
        {
            DataRow nRow;
            ArrayList obj = new ArrayList();
            DataTable dT = new DataTable();

            dT.Columns.Add("Perspective", typeof(string));
            dT.Columns.Add("TargetValue", typeof(double));
            dT.Columns.Add("ActualValue", typeof(double));

            var perspectives = scoresManager.getScoresReport(AreaId, AreaValueId, Date);
            foreach (scores item in perspectives)
            {
                double totalTarget = 0;
                double totalActual = 0;
                int perspectiveId = item.PERSPECTIVE_ID;
                string perspectiveName = item.PERSPECTIVE_NAME;
                if (!obj.Contains(perspectiveId))
                {
                    nRow = dT.NewRow();
                    totalTarget = perspectives.Where(c => c.PERSPECTIVE_ID == perspectiveId).Sum(u => u.WEIGHT);
                    totalActual = Math.Round(perspectives.Where(c => c.PERSPECTIVE_ID == perspectiveId).Sum(u => u.FINAL_SCORE), 2);
                    nRow["Perspective"] = perspectiveName;
                    nRow["TargetValue"] = totalTarget;
                    nRow["ActualValue"] = totalActual;
                    dT.Rows.Add(nRow);
                    nRow = null;
                    obj.Add(perspectiveId);
                }
            }

            nRow = dT.NewRow();
            nRow["Perspective"] = "Total";
            nRow["TargetValue"] = perspectives.Sum(u => u.WEIGHT);
            nRow["ActualValue"] = Math.Round(perspectives.Sum(u => u.FINAL_SCORE), 2);
            dT.Rows.Add(nRow);
            nRow = null;

            dT.AcceptChanges();
            return dT;
        }

        void loadData()
        {
            try
            {
                DataTable dT = getData();

                if (dT.Rows.Count >= Util.getGraphMinItems())
                {
                    RadChartPes.Height = new Unit(Util.getGraphHeight());
                    RadChartPes.Width = new Unit(Util.getGraphWidth());
                }
                RadChartPes.DataSource = dT;
                RadChartPes.DataBind();
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        #endregion
    }
}