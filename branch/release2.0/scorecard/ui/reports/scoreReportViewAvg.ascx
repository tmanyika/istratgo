﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="scoreReportViewAvg.ascx.cs"
    Inherits="scorecard.ui.reports.scoreReportViewAvg" %>
<%@ Import Namespace="scorecard.implementations" %>
<div style="background-color: White;">
    <table cellpadding="0" cellspacing="10" border="0" width="90%">
        <tr>
            <td style="width: 250px;">
                <h2>
                    Area</h2>
            </td>
            <td style="width: 250px;">
                <h2>
                    <span style="color: Black">
                        <asp:Literal ID="lblCriteria" runat="server"></asp:Literal></span></h2>
            </td>
            <td align="right">
                <span style="color: Red">
                    <asp:Literal ID="lblError" runat="server"></asp:Literal></span>
                <asp:LinkButton ID="lnkPrint" runat="server" CausesValidation="False" OnClientClick="window.print();">
                    <img runat="server" src="~/ui/assets/icons/print.png" id="imgPrint" alt="Click to Print" /></asp:LinkButton>&nbsp;&nbsp;<asp:LinkButton
                        ID="lnkExcel" runat="server" CausesValidation="False" OnClick="lnkExcel_Click">
                        <img src="~/ui/assets/icons/Excel.png" runat="server" id="imgExcel" alt="Export to Excel" /></asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td>
                <h2>
                    Name</h2>
            </td>
            <td colspan="2">
                <h2>
                    <span style="color: Black">
                        <asp:Literal ID="lblName" runat="server"></asp:Literal>
                    </span>
                </h2>
            </td>
        </tr>
        <tr>
            <td>
                <h2>
                    Start Date</h2>
            </td>
            <td colspan="2">
                <h2>
                    <span style="color: Black">
                        <asp:Literal ID="lblStartDate" runat="server"></asp:Literal>
                    </span>
                </h2>
            </td>
        </tr>
        <tr>
            <td>
                <h2>
                    End Date</h2>
            </td>
            <td colspan="2">
                <h2>
                    <span style="color: Black">
                        <asp:Literal ID="lblEndDate" runat="server"></asp:Literal>
                    </span>
                </h2>
            </td>
        </tr>
    </table>
</div>
<div>
    <asp:ListView ID="lstData" runat="server">
        <LayoutTemplate>
            <table class="fullwidth" cellpadding="0" cellspacing="0" border="0" width="90%">
                <thead>
                    <tr>
                        <th align="left">
                            Perspective
                        </th>
                        <th align="left">
                            Strategic Objective
                        </th>
                        <th align="left">
                            Area Goal
                        </th>
                        <th align="center">
                            Unit of Measure
                        </th>
                        <th align="right">
                            Goal Weight
                        </th>
                        <th align="right">
                            Average Perfomance Score (%)
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr id="ItemPlaceHolder" runat="server">
                    </tr>
                </tbody>
            </table>
        </LayoutTemplate>
        <ItemTemplate>
            <tr class="odd">
                <td align="left">
                    <asp:Literal ID="lblPerspective" runat="server" Text='<%#  Eval("PERSPECTIVE_NAME") %>'></asp:Literal>
                    <asp:HiddenField ID="hdnPid" runat="server" Value='<%#  Eval("PERSPECTIVE_ID") %>' />
                </td>
                <td align="left">
                    <asp:Literal ID="lblObjective" runat="server" Text='<%#  Eval("OBJECTIVE") %>' />
                </td>
                <td align="left">
                    <asp:Literal ID="lblGoalDesc" runat="server" Text='<%#  Eval("GOAL_DESCRIPTION") %>' />
                </td>
                <td align="center">
                    <asp:Literal ID="lblUnitMeasure" runat="server" Text='<%# Eval("UNIT_MEASURE_NAME")%>' />
                </td>
                <td align="right">
                    <asp:Literal ID="lblWeight" runat="server" Text='<%# Eval("WEIGHT") %>' />%
                </td>
                <td align="right">
                    <asp:Literal ID="lblFinalScore" runat="server" Text='<%# Common.FormatNumber(Eval("FINAL_SCORE")) %>' />%
                </td>
            </tr>
        </ItemTemplate>
        <EmptyDataTemplate>
            There are no records to display for the criteria you specified.
        </EmptyDataTemplate>
    </asp:ListView>
</div>
