﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.implementations;
using HR.Leave;
using System.Data;
using HR.Leave.Documents;
using System.Net.Mail;
using System.Email.Communication;

namespace scorecard.ui.controls.hr
{
    public partial class leaveApproval : System.Web.UI.UserControl
    {
        #region Page Event Handling Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) FillApplictions();
        }

        #endregion

        #region Databinding Methods

        void FillApplictions()
        {
            ILeaveApplication obj = new LeaveApplicationBL();
            List<LeaveApplication> data = obj.GetSubmittedLeave(Util.user.UserId, Util.getLeaveStatusSubmittedId());
            BindControl.BindListView(lstData, data);
            pager.Visible = data.Count > pager.PageSize ? true : false;
            mView.SetActiveView(vwData);
        }

        bool IsValid(int applicationId)
        {
            ILeaveApplication obj = new LeaveApplicationBL();
            LeaveApplication lv = obj.GetById(applicationId);

            if (!obj.ApplicationValid(lv))
            {
                ShowMessage(lblErr, Messages.GetLeaveApplicationOverlappingMessaging(lv.EndDate, lv.StartDate, " for the employee."));
                return false;
            }
            return true;
        }

        void ApproveLeave(int applicationId)
        {
            try
            {
                int statusId = int.Parse(ddStatus.SelectedValue);
                string comment = txtComment.Text;
                if (comment == string.Empty &&
                    (statusId == Util.getLeaveStatusDeclinedId() ||
                    statusId == Util.getLeaveStatusCancelledId()))
                {
                    ShowMessage(lblErr, "Reason for rejection / cancellation is required.");
                    return;
                }

                ILeaveApplication obj = new LeaveApplicationBL();
                LeaveApplication lv = new LeaveApplication
                {
                    ApprovedByEmployeeId = Util.user.UserId,
                    DateUpdated = DateTime.Now,
                    EmployeeId = Util.user.UserId,
                    LeaveApplicationId = applicationId,
                    RejectReason = comment,
                    StatusId = statusId,
                };

                bool saved = obj.UpdateStatus(lv);

                if (saved)
                {
                    leaveApplication send = new leaveApplication();

                    FillApplictions();
                    ShowMessage(lblMsg, Messages.GetSaveMessage());
                    send.SendEmail(applicationId, false, lblMsg);
                }
                else ShowMessage(lblErr, Messages.GetSaveFailedMessage());
            }
            catch (Exception ex)
            {
                string err = ex.ToString();
                Util.LogErrors(ex);
                ShowMessage(lblErr, ex.ToString());
            }
        }

        #endregion

        #region Utility Methods

        public string GetBoolean(object valid)
        {
            if (valid == DBNull.Value) return "No";
            return (bool.Parse(valid.ToString())) ? "Yes" : "No";
        }

        public bool GetVisibilityStatus(object statusId)
        {
            if (statusId == DBNull.Value) return false;
            return (int.Parse(statusId.ToString()) == Util.getLeaveStatusSubmittedId()) ? true : false;
        }

        void ClearTextBoxes()
        {
            lblErr.Text = "";
            lblEndDate.Text = "";
            lblLeaveType.Text = "";
            lblMsg.Text = "";
            lblNoOfDays.Text = "";
            lblNotes.Text = "";
            lblStartDate.Text = "";
        }

        void ShowMessage(Literal lbl, string msg)
        {
            lbl.Text = msg;
        }

        void FillDocuments(int applicationId)
        {
            ILeaveApplicationComment obj = new LeaveApplicationCommentBL();
            var data = obj.GetComments(applicationId);
            BindControl.BindRepeater(rptDocs, data);
        }

        void GetItem(int applicationId, string fullName)
        {
            try
            {
                ILeaveStatus lStatus = new LeaveStatusBL();
                ILeaveApplication leave = new LeaveApplicationBL();
                LeaveApplication obj = leave.GetById(applicationId);

                ClearTextBoxes();

                hdnId.Value = applicationId.ToString();
                lblEndDate.Text = Common.GetDescriptiveDate(obj.EndDate, false);
                lblStartDate.Text = Common.GetDescriptiveDate(obj.StartDate, false);
                lblNotes.Text = obj.LeaveNotes;
                lblNoOfDays.Text = obj.NoOfLeaveDays.ToString();
                lblLeaveType.Text = obj.Leave.LeaveName;
                lblName.Text = fullName;
                txtComment.Text = obj.RejectReason;
                FillDocuments(applicationId);
                BindControl.BindDropdown(ddStatus, "StatusName", "StatusId", lStatus.GetByStatus(true).Where(i => i.StatusId != Util.getLeaveStatusSubmittedId()));
                mView.SetActiveView(vwForm);
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        #endregion

        #region Button Event Handling Methods

        protected void btnUpdateLeave_Click(object sender, EventArgs e)
        {
            int applicationId = int.Parse(hdnId.Value);
            if (!IsValid(applicationId)) return;
            else ApproveLeave(applicationId);
        }

        protected void btnCancelLeave_Click(object sender, EventArgs e)
        {
            mView.SetActiveView(vwData);
        }

        #endregion

        #region ListView Event Handling Methods

        protected void lstData_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            pager.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            FillApplictions();
        }

        protected void lstData_ItemCommand(object sender, ListViewCommandEventArgs e)
        {

        }

        protected void lstData_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            try
            {
                int applicationId = int.Parse((lstData.Items[e.NewEditIndex].FindControl("hdnId") as HiddenField).Value);
                string fullName = (lstData.Items[e.NewEditIndex].FindControl("hdnName") as HiddenField).Value;
                GetItem(applicationId, fullName);
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        #endregion
    }
}