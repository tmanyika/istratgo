﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="leaveForm.ascx.cs" Inherits="scorecard.ui.controls.hr.leaveForm" %>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<style type="text/css">
    label
    {
        width: 220px !important;
    }
</style>
<asp:UpdatePanel ID="UpdatePanelScoreReport" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div>
            <h1>
                &nbsp;Leave Reporting
            </h1>
        </div>
        <div>
            <fieldset runat="server" id="pnlContactInfo">
                <legend>Comparison Report Criteria </legend>
                <div class="clr">
                    <div class="divFloatLeft">
                        <label for="sf">
                            Report Name:
                        </label>
                    </div>
                    <div class="divFloatLeft">
                        <span class="field_desc">
                            <asp:DropDownList ID="ddRepName" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddRepName_SelectedIndexChanged">
                                <asp:ListItem Value="leaveBalances.aspx">Leave Balances</asp:ListItem>
                                <asp:ListItem Value="staffOnLeave.aspx">All Staff On Leave</asp:ListItem>
                                <asp:ListItem Value="overthreshold.aspx">All Staff with Leave Over the Threshold</asp:ListItem>
                                <asp:ListItem Value="negativeBalances.aspx">Staff with Negative Balances</asp:ListItem>
                            </asp:DropDownList>
                        </span>
                    </div>
                </div>
                <div class="clr">
                    <div class="divFloatLeft">
                        <label for="sf">
                            Select Leave Types(s):
                        </label>
                    </div>
                    <div class="divFloatLeft">
                        <span class="field_desc">
                            <cc1:DropDownCheckBoxes ID="ddlValueType" runat="server" UseSelectAllNode="true"
                                UseButtons="true" Width="250px" AutoPostBack="True" DataTextField="LeaveName"
                                DataValueField="LeaveId">
                                <Texts OkButton="Yes" CancelButton="No" SelectAllNode="select all" SelectBoxCaption=" - select an option -" />
                            </cc1:DropDownCheckBoxes>
                        </span>
                    </div>
                </div>
                <div class="clr">
                    <div class="divFloatLeft">
                        <label for="sf">
                            Select Org Unit:
                        </label>
                    </div>
                    <div class="divFloatLeft">
                        <span class="field_desc">
                            <asp:DropDownList ID="ddOrg" runat="server" DataTextField="ORGUNIT_NAME" DataValueField="ID">
                            </asp:DropDownList>
                        </span>
                    </div>
                </div>
                <div class="clr" runat="server" id="divDates">
                    <div class="divFloatLeft">
                        <label for="sf">
                            From Date:
                        </label>
                    </div>
                    <div class="divFloatLeft">
                        <asp:TextBox ID="txtStartDate" runat="server"></asp:TextBox>
                        <asp:CalendarExtender ID="txtStartDate_CalendarExtender" runat="server" Enabled="True"
                            Format="dd/MM/yyyy" TargetControlID="txtStartDate">
                        </asp:CalendarExtender>
                    </div>
                    <div class="divFloatLeft">
                        To Date:
                    </div>
                    <div class="divFloatLeft">
                        &nbsp;&nbsp;
                        <asp:TextBox ID="txtEndDate" runat="server"></asp:TextBox>
                        <asp:CalendarExtender ID="txtEndDate_CalendarExtender" runat="server" Enabled="True"
                            Format="dd/MM/yyyy" TargetControlID="txtEndDate">
                        </asp:CalendarExtender>
                    </div>
                </div>
                <div class="errorMsg">
                    <label for="lblMsg">
                    </label>
                    <asp:Literal ID="lblMsg" runat="server"></asp:Literal>
                </div>
                <div class="clr">
                    <br />
                </div>
                <div class="clr">
                    <label for="btnViewReport">
                        &nbsp;
                    </label>
                    <asp:Button ID="btnViewReport" runat="server" CssClass="button tooltip" Text="View Report"
                        ValidationGroup="Form" CausesValidation="true" OnClick="btnViewReport_Click" />
                </div>
            </fieldset>
        </div>
        <asp:UpdateProgress ID="UpdateProgressScoreReport" runat="server" AssociatedUpdatePanelID="UpdatePanelScoreReport">
            <ProgressTemplate>
                <img runat="server" src="~/ui/images/icons/activity.gif" alt="Please wait..." id="imgPrg" />
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
