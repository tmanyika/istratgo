﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.implementations;
using HR.Human.Resources;

namespace scorecard.ui.controls.hr
{
    public partial class employeeStatuses : System.Web.UI.UserControl
    {
        #region Properties

        int CompId
        {
            get { return Util.user.CompanyId; }
        }

        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) InitialisePage();
        }

        #endregion

        #region Utility Methods

        void InitialisePage()
        {
            LoadEmployeeStatus();
        }

        void LoadEmployeeStatus()
        {
            IEmployeeStatus obj = new EmployeeStatusBL();
            BindControl.BindListView(lstData, obj.GetByStatus(true));
            pager.Visible = (lstData.Items.Count <= pager.PageSize) ? false : true;
        }

        #endregion

        #region Button Event Handling

        protected void lnkAdd_Click(object sender, EventArgs e)
        {
            BindControl.BindLiteral(lblMsg, "");
            lstData.InsertItemPosition = InsertItemPosition.LastItem;
            LoadEmployeeStatus();
        }

        protected void lnkCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("index.aspx", false);
        }

        #endregion

        #region Utility

        public string GetBoolean(object boolVal)
        {
            if (boolVal == DBNull.Value) return "Yes";
            return bool.Parse(boolVal.ToString()) ? "Yes" : "No";
        }

        private void CancelUpdate()
        {
            lstData.EditIndex = -1;
            lstData.InsertItemPosition = InsertItemPosition.None;
            LoadEmployeeStatus();
        }

        void DeleteItem(ListViewCommandEventArgs e)
        {
            int leaveId = int.Parse((e.Item.FindControl("hdnStatusId") as HiddenField).Value);
            string updatedBy = Util.user.LoginId;

            IEmployeeStatus obj = new EmployeeStatusBL();
            bool deleted = obj.Delete(leaveId, updatedBy);
            if (deleted) LoadEmployeeStatus();
            string msg = deleted ? Messages.GetDeleteMessage() : Messages.GetDeleteFailedMessage();
            BindControl.BindLiteral(lblMsg, msg);
        }

        void UpdateItem(ListViewCommandEventArgs e)
        {
            try
            {
                int leaveId = int.Parse((e.Item.FindControl("hdnStatusIdE") as HiddenField).Value);

                bool active = (e.Item.FindControl("chkActive") as CheckBox).Checked;

                string name = (e.Item.FindControl("txtName") as TextBox).Text;
                string updatedBy = Util.user.LoginId;

                EmployeeStatus obj = new EmployeeStatus
                {
                    StatusName = name,
                    StatusId = leaveId,
                    CreatedBy = updatedBy,
                    UpdatedBy = updatedBy,
                    Active = active
                };
                IEmployeeStatus objl = new EmployeeStatusBL();

                bool saved = objl.UpdateEmployeeStatus(obj);
                string msg = saved ? Messages.GetUpdateMessage() : Messages.GetUpdateFailedMessage();

                BindControl.BindLiteral(lblMsg, msg);
                if (saved)
                {
                    lstData.EditIndex = -1;
                    LoadEmployeeStatus();
                }

            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        void AddItem(ListViewCommandEventArgs e)
        {
            try
            {
                bool active = (e.Item.FindControl("chkActivei") as CheckBox).Checked;
                string name = (e.Item.FindControl("txtNamei") as TextBox).Text;
                string updatedBy = Util.user.LoginId;

                EmployeeStatus obj = new EmployeeStatus
                {
                    StatusName = name,
                    StatusId = 0,
                    CreatedBy = updatedBy,
                    UpdatedBy = updatedBy,
                    Active = active
                };

                IEmployeeStatus objl = new EmployeeStatusBL();
                bool saved = objl.AddEmployeeStatus(obj);
                string msg = saved ? Messages.GetUpdateMessage() : Messages.GetUpdateFailedMessage();
                BindControl.BindLiteral(lblMsg, msg);
                if (saved)
                {
                    lstData.InsertItemPosition = InsertItemPosition.None;
                    LoadEmployeeStatus();
                }
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        #endregion

        #region ListView Event Handling Methods including Slide Paging

        protected void lstData_PagePropertiesChanged(object sender, EventArgs e)
        {
            LoadEmployeeStatus();
        }

        protected void lstData_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            try
            {
                lstData.EditIndex = e.NewEditIndex;
                lstData.InsertItemPosition = InsertItemPosition.None;
                LoadEmployeeStatus();
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        protected void lstData_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                BindControl.BindLiteral(lblMsg, "");
                if (e.Item.ItemType == ListViewItemType.InsertItem ||
               e.Item.ItemType == ListViewItemType.DataItem)
                {
                    switch (e.CommandName)
                    {
                        case "AddItem":
                            AddItem(e);
                            break;
                        case "UpdateItem":
                            UpdateItem(e);
                            break;
                        case "DeleteItem":
                            DeleteItem(e);
                            break;
                        case "CancelUpdate":
                            CancelUpdate();
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        #endregion
    }
}