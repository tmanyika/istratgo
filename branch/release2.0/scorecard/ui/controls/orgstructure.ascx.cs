﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.controllers;
using scorecard.implementations;
using scorecard.entities;
using HierarchicalData;

namespace scorecard.ui.controls
{
    public partial class orgstructure : System.Web.UI.UserControl
    {
        structurecontroller orgunits;

        public orgstructure()
        {
            orgunits = new structurecontroller(new structureImpl());
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            loadCompanyStructure();
        }

        void loadCompanyStructure()
        {
            try
            {
                var units = orgunits.getActiveCompanyOrgStructure(Util.user.CompanyId);
                var dataSource = new HierarchicalDataSet(units, "ID", "PARENT_ORG");
                BindControl.BindTree(trvOrg, dataSource);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }
    }
}