﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="capture.ascx.cs" Inherits="scorecard.ui.controls.capture" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:UpdatePanel ID="UpdatePanelCapture" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div>
            <h1>
                &nbsp; Capture Scores
            </h1>
        </div>
        <div>
            <fieldset runat="server" id="pnlContactInfo">
                <legend>Setup Your Goals </legend>
                <p>
                    <label for="sf">
                        Select Area:
                    </label>
                    &nbsp;<span class="field_desc"><asp:DropDownList runat="server" ID="ddlArea" AutoPostBack="True"
                        OnSelectedIndexChanged="ddlArea_SelectedIndexChanged" />
                    </span>
                </p>
                <p>
                    <label for="sf">
                        Select Value:
                    </label>
                    &nbsp;<span class="field_desc"><asp:DropDownList runat="server" ID="ddlValueTypes"
                        AutoPostBack="True" OnSelectedIndexChanged="ddlValueTypes_SelectedIndexChanged" />
                    </span>
                </p>
                <p>
                    <label for="sf">
                        Date of Score:</label>&nbsp; <span class="field_desc">
                            <asp:TextBox ID="txtDate" runat="server" Width="120px"></asp:TextBox></span>
                    <asp:CalendarExtender ID="txtDate_CalendarExtender" runat="server" Enabled="True"
                        Format="d/MM/yyyy" TargetControlID="txtDate">
                    </asp:CalendarExtender>
                    <asp:RequiredFieldValidator ID="rqDate" runat="server" ErrorMessage="Date of Score"
                        Display="None" ControlToValidate="txtDate" ForeColor="Red" ValidationGroup="Form"></asp:RequiredFieldValidator>
                    <span style="color: Red">
                        <asp:Literal ID="lblDateUsed" runat="server"></asp:Literal></span>
                </p>
            </fieldset>
            <asp:Panel runat="server" ID="pnlScores" Width="100%">
                <asp:ListView ID="lvGoals" runat="server">
                    <LayoutTemplate>
                        <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                            <thead>
                                <tr id="Tr1" runat="server">
                                    <td>
                                    </td>
                                    <td>
                                        Goal Description
                                    </td>
                                    <td>
                                        Score
                                    </td>
                                    <td>
                                        Target
                                    </td>
                                    <td>
                                        Unit Of Measure
                                    </td>
                                    <td>
                                        Weight
                                    </td>
                                    <td>
                                        Final Score
                                    </td>
                                </tr>
                                <tr id="ItemPlaceHolder" runat="server">
                                </tr>
                            </thead>
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tbody>
                            <tr class="odd">
                                <td>
                                    <asp:Label ID="lblID" runat="server" Visible="false" Text='<%# Eval("ID") %>' />
                                    <asp:Label ID="lblUnitMeasureID" runat="server" Text='<%# Eval("UNIT_MEASURE") %>'
                                        Visible="false" />
                                </td>
                                <td>
                                    <%#  Eval("GOAL_DESCRIPTION")%>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtScore" Text='<%# getCurrentStore(int.Parse(Eval("ID").ToString())) %>' />
                                    <span style="color: Red">
                                        <asp:Literal ID="lblError" runat="server" Text=""></asp:Literal></span>
                                    <asp:HiddenField ID="hdnUnitMeasureId" runat="server" Value='<%# Eval("UNIT_MEASURE") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblTarget" runat="server" Text='<%# Eval("TARGET")%>' />
                                </td>
                                <td>
                                    <%# getTypeName(int.Parse(Eval("UNIT_MEASURE").ToString()))%>
                                </td>
                                <td>
                                    <asp:Label ID="lblWeight" runat="server" Text='<%# Eval("WEIGHT")%>'></asp:Label>
                                    %
                                </td>
                                <td>
                                    <asp:Label ID="lblTotal" runat="server" Text='<%# getFinalStore(int.Parse(Eval("ID").ToString())) %>' />
                                    %
                                </td>
                            </tr>
                        </tbody>
                    </ItemTemplate>
                </asp:ListView>
                <br />
                <p>
                    <br />
                    <span style="color: Red">
                        <asp:Literal ID="lblMsg" runat="server"></asp:Literal></span><br />
                </p>
                <asp:Panel ID="pnlNoData" runat="server" Visible="false">
                    <div class="message information close">
                        <h2>
                            Information
                        </h2>
                        <p>
                            You do not have any goals defined yet.
                        </p>
                    </div>
                </asp:Panel>
                <p>
                    <fieldset runat="server" id="Fieldset1">
                        <legend>Supporting Documents </legend>
                        <label for="sf">
                            Locate Document
                        </label>
                        <asp:FileUpload ID="ftpAttachDocuments_0" runat="server" />
                        <br />
                        <label for="sf">
                            Locate Document
                        </label>
                        <asp:FileUpload ID="ftpAttachDocuments_1" runat="server" />
                    </fieldset>
                    <br />
                </p>
                <p>
                    <asp:Button ID="btnNew" runat="server" CssClass="button tooltip" Text="Submit to Manager"
                        OnClick="btnNew_Click" ValidationGroup="Form" />
                    <asp:Button ID="lnkCancelToManager" runat="server" CssClass="button tooltip" CausesValidation="false"
                        Text="Done" PostBackUrl="~/ui/index.aspx" />
                </p>
            </asp:Panel>
            <asp:Panel ID="pnlSubmitted" runat="server" Visible="false" Width="100%">
                <fieldset runat="server" id="Fieldset2">
                    <legend>Confirmation </legend>
                    <p style="color: #FF3300">
                        Scores submitted succesfully, email has been sent to your manager..
                    </p>
                    <p>
                        <asp:Button ID="lnkSubmitNew" runat="server" CssClass="button" OnClick="lnkSubmitNew_Click"
                            Text="Submit New" />
                        <asp:Button ID="lnkCancel" runat="server" CssClass="button tooltip" Text="Done" CausesValidation="false"
                            PostBackUrl="~/ui/index.aspx" />
                    </p>
                </fieldset>
            </asp:Panel>
        </div>
        <asp:ValidationSummary ID="vdS" runat="server" HeaderText="The following fields are required:"
            ShowMessageBox="True" ShowSummary="False" ValidationGroup="Form" />
        <asp:UpdateProgress ID="UpdateProgressCapture" runat="server" AssociatedUpdatePanelID="UpdatePanelCapture">
            <ProgressTemplate>
                <img runat="server" src="~/ui/images/icons/activity.gif" alt="Please wait..." id="imgPrg" />
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnNew" />
    </Triggers>
</asp:UpdatePanel>
