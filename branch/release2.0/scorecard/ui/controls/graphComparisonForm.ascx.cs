﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.implementations;
using scorecard.entities;
using scorecard.controllers;
using System.Text;
using HR.Human.Resources;

namespace scorecard.ui.controls
{
    public partial class graphComparisonForm : System.Web.UI.UserControl
    {
        #region  Variables

        useradmincontroller usersDef;
        goaldefinition goalDef;
        scoresmanager scoresManager;

        #endregion

        #region Default Class Constructors

        public graphComparisonForm()
        {
            usersDef = new useradmincontroller(new useradminImpl());
            goalDef = new goaldefinition(new goalsImpl());
            scoresManager = new scoresmanager(new scorecard.implementations.scoresImpl());
        }

        #endregion

        #region Page Events Handling Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                getGoalsCriteriaTypes();
            }
        }

        #endregion

        #region Databinding Methods

        string getValueList()
        {
            StringBuilder strB = new StringBuilder("");
            foreach (ListItem item in ddlValueType.Items)
                if (item.Selected) strB.Append(string.Format("{0},", item.Value));
            string strVals = strB.ToString();
            int lstIdx = strVals.LastIndexOf(',');
            return lstIdx > 0 ? strVals.Substring(0, lstIdx) : strVals;
        }

        bool multipleSelectionAllowed(int areaId)
        {
            return (areaId == Util.getTypeDefinitionsRolesTypeID()) ? true : false;
        }

        void selectOnlyOneItem(bool oneItem)
        {
            ddDate.Items.Clear();
        }

        void getGoalsCriteriaTypes()
        {
            var areaList = goalDef.appsettings.getChildSettings(new applicationsettings { PARENT_ID = Util.getTypeDefinitionsGoalAreaTypes() }).ToList();
            BindControl.BindDropdown(ddlArea, "DESCRIPTION", "TYPE_ID", "-- select area --", "0", areaList.ToList());
        }

        void loadOrgStruct()
        {
            try
            {
                var userRole = Util.user.UserTypeId;
                var userID = Util.user.UserId;
                var structure = goalDef.orgstructure.getCompanyStructureListView(new company { COMPANY_ID = Util.user.CompanyId });
                if (userRole != Util.getTypeDefinitionsUserTypeAdministrator()) structure = structure.Where(t => t.OWNER_ID == userID).ToList();

                BindControl.BindDropdown(ddlValueType, "ORGUNIT_NAME", "ID", "", "", structure.ToList());// "- select organisation unit -", "0", structure.ToList());
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void loadProjects(int companyId)
        {
            projectmanagement proj = new projectmanagement(new projectImpl());

            int adminRoleId = Util.getTypeDefinitionsUserTypeAdministrator();
            int roleId = (int)Util.user.UserTypeId;
            IEnumerable<project> projL = proj.get(companyId, true);
            if (adminRoleId != Util.user.UserTypeId) projL = projL.Where(c => c.RESPONSIBLE_PERSON_ID == Util.user.UserId);

            BindControl.BindDropdown(ddlValueType, "PROJECT_NAME", "PROJECT_ID", "", "", projL);// "-- select project --", "0", projL);
        }

        void loadJobTitles()
        {
            try
            {
                IEmployee obj = new EmployeeBL();
                List<Employee> titles = obj.GetByOrgStructure(Util.user.OrgId, true);
                if (Util.user.UserTypeId != Util.getTypeDefinitionsUserTypeAdministrator())
                    titles = titles.Where(t => t.LineManagerEmployeeId == Util.user.UserId || t.EmployeeId == Util.user.UserId).ToList();
                BindControl.BindDropdown(ddlValueType, "FullName", "EmployeeId", "", "", titles);
                loadDates(int.Parse(ddlArea.SelectedValue), 0);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void loadDates(int areaId, int idx)
        {
            string dataVal = getValueList();
            if (dataVal.Split(',').Length <= 0)
            {
                lblMsg.Text = "Please select at least one or more values.";
                return;
            }
            string xmlData = Common.GetXML(dataVal.Split(','), "Record", "Score", "Id");
            List<scores> dates = scoresManager.getReportDateValues(areaId, xmlData);
            BindControl.BindDropdown(ddDate, "PERIOD_DATE_DESC", "PERIOD_DATE_VAL", "- select date -", "0", dates);
        }

        void manageValueDropDowns(bool userOption)
        {
            ddlValueType.UseButtons = userOption ? true : false;
            ddlValueType.UseSelectAllNode = userOption ? true : false;
        }

        void setCriteriaSelected()
        {
            try
            {
                bool oneOption = true;
                int areaId = int.Parse(ddlArea.SelectedValue);
                if (areaId == Util.getTypeDefinitionsRolesTypeID())
                {
                    oneOption = false;
                    loadJobTitles();
                }
                else if (areaId == Util.getTypeDefinitionsProjectID()) loadProjects(Util.user.CompanyId);
                else loadOrgStruct();

                selectOnlyOneItem(oneOption);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void downloadReport(bool addEventToButton)
        {
            string valIds = getValueList();
            if (valIds.Split(',').Length <= 0)
            {
                lblMsg.Text = "Please select at least one or more values.";
                return;
            }

            string areaValue = valIds;
            string areaId = ddlArea.SelectedValue;
            string dateVal = ddDate.SelectedValue;
            string url = "graphComparison.aspx";

            string script = addEventToButton ? string.Format("return openNewWindow('{3}?aid={0}&idv={1}&dVal={2}&isOrgId={4}&cId={5}');", areaId, areaValue, dateVal, url, false.ToString().ToLower(), Util.user.CompanyId) :
                                               string.Format("openNewWindow('{3}?aid={0}&idv={1}&dVal={2}&isOrgId={4}&cId={5}');", areaId, areaValue, dateVal, url, false.ToString().ToLower(), Util.user.CompanyId); ;

            if (addEventToButton) btnViewReport.OnClientClick = script;
            else AjaxScriptManager.RegisterStartupScript(Page, typeof(Page), "mygcReport", script, true);
        }

        #endregion

        #region Dropdown Event Handling Methods

        protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddDate.Items.Clear();
            ddlValueType.Items.Clear();
            setCriteriaSelected();
        }

        protected void ddlValueTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblMsg.Text = "";
            if (getValueList() == "")
            {
                lblMsg.Text = "Please select a value.";
                return;
            }

            int areaId = int.Parse(ddlArea.SelectedValue);
            int idx = ddlValueType.SelectedIndex;
            loadDates(areaId, idx);
        }

        //protected void ddDate_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    downloadReport(true);
        //}

        //protected void ddRepName_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    downloadReport(true);
        //}

        #endregion

        #region Button Event Handling Methods

        protected void btnViewReport_Click(object sender, EventArgs e)
        {
            downloadReport(false);
        }

        #endregion
    }
}