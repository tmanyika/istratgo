﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="approval.ascx.cs" Inherits="scorecard.ui.controls.approval" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:UpdatePanel ID="UpdatePanelApproval" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <h1>
            &nbsp; Workflow Approval
        </h1>
        <asp:ListView ID="lvSubmittedWorkflows" runat="server" OnItemCommand="lvSubmittedWorkflows_ItemCommand">
            <LayoutTemplate>
                <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                    <thead>
                        <tr id="Tr1" runat="server">
                            <td>
                                <input type="checkbox" class="checkall" />
                            </td>
                            <td>
                                Name
                            </td>
                            <td>
                                Criteria
                            </td>
                            <td>
                                Approver
                            </td>
                            <td>
                                Status
                            </td>
                            <td>
                                Date Submitted
                            </td>
                            <td>
                                Details
                            </td>
                        </tr>
                        <tr id="ItemPlaceHolder" runat="server">
                        </tr>
                    </thead>
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tbody>
                    <tr class="odd">
                        <td>
                            <input type="checkbox" />
                            <asp:Label ID="lblID" runat="server" Visible="false" Text='<%# Eval("ID") %>' />
                            <asp:Label ID="lblCriteria" runat="server" Visible="false" Text='<%# Eval("CRITERIA_TYPE_ID") %>' />
                            <asp:Label ID="lblItemSelected" runat="server" Visible="false" Text='<%# Eval("SELECTED_ITEM_VALUE") %>' />
                        </td>
                        <td>
                            <%# getObjectName(int.Parse(Eval("SELECTED_ITEM_VALUE").ToString()), int.Parse(Eval("CRITERIA_TYPE_ID").ToString()))%>
                        </td>
                        <td>
                            <%# getTypeName(int.Parse(Eval("CRITERIA_TYPE_ID").ToString()))%>
                        </td>
                        <td>
                            <%# getApproverName(int.Parse(Eval("APPROVER_ID").ToString()))%>
                        </td>
                        <td>
                            <%# getTypeName(int.Parse(Eval("STATUS").ToString()))%>
                        </td>
                        <td>
                            <%# String.Format("{0:dd/MM/yyyy}", Eval("DATE_SUBMITTED"))%>
                        </td>
                        <td>
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkView" CommandName="View" class="ui-state-default ui-corner-all"
                                    title="View Details " CausesValidation="false"><span class="ui-icon ui-icon-pencil">
                        </span></asp:LinkButton>
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </ItemTemplate>
            <EmptyItemTemplate>
                There are no record to display.
            </EmptyItemTemplate>
        </asp:ListView>
        <asp:Panel ID="pnlNoData" runat="server" Visible="false">
            <div class="message information close">
                <h2>
                    Information
                </h2>
                <p>
                    You do not have any pending workflows for your review
                </p>
            </div>
        </asp:Panel>
        <br />
        <asp:DataPager ID="dtPagerSubmittedWorkflows" runat="server" PagedControlID="lvSubmittedWorkflows"
            PageSize="1000">
            <Fields>
                <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True" ShowNextPageButton="False"
                    ShowPreviousPageButton="False" />
                <asp:NumericPagerField />
                <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True" ShowNextPageButton="False"
                    ShowPreviousPageButton="False" />
            </Fields>
        </asp:DataPager>
        <br />
        <br />
        <asp:Button ID="lnkDone" runat="server" class="button tooltip" title="Save Scores"
            Text="Done" OnClick="lnkDone_Click" CausesValidation="false"></asp:Button>
        <br />
        <br />
        <asp:Panel ID="mdlPanelShowScores" runat="server" CssClass="modalPopup" Height="768px"
            Width="1024px" ScrollBars="Both">
            <br />
            <h1>
                Scores
            </h1>
            <!-- Fieldset -->
            <fieldset runat="server" id="pnlScoresInfo">
                <legend>Captured Scores</legend>
                <p>
                    <label for="sf">
                        Criteria :
                    </label>
                    &nbsp;<span class="field_desc" style="font-style: normal;"><b><asp:Literal ID="lblArea"
                        runat="server"></asp:Literal></b>
                        <asp:HiddenField ID="hdnAreaId" runat="server" />
                        <asp:HiddenField ID="hdnValueId" runat="server" />
                    </span>
                    <asp:Label ID="lblID" runat="server" Visible="False"></asp:Label>
                </p>
                <p>
                    <label for="sf">
                        Name :
                    </label>
                    &nbsp;<span class="field_desc" style="font-style: normal;"><b><asp:Literal ID="lblName"
                        runat="server"></asp:Literal></b> </span>
                </p>
            </fieldset>
            <asp:ListView ID="lvGoals" runat="server">
                <LayoutTemplate>
                    <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                        <thead>
                            <tr id="Tr1" runat="server">
                                <td>
                                </td>
                                <td>
                                    Goal Description
                                </td>
                                <td>
                                    Score
                                </td>
                                <td>
                                    Target
                                </td>
                                <td>
                                    Unit Of Measure
                                </td>
                                <td>
                                    Weight
                                </td>
                                <td>
                                    Final Score 
                                </td>
                            </tr>
                            <tr id="ItemPlaceHolder" runat="server">
                            </tr>
                        </thead>
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <tbody>
                        <tr class="odd">
                            <td>
                                <asp:Label ID="lblID" runat="server" Visible="false" Text='<%# Eval("ID") %>' />
                            </td>
                            <td>
                                <%# Eval("GOAL_DESCRIPTION")%>
                            </td>
                            <td>
                                <%# Eval("SCORES") %>
                            </td>
                            <td>
                                <asp:Label ID="lblTarget" runat="server" Text='<%# Eval("TARGET")%>' />
                            </td>
                            <td>
                                <%# getTypeName(int.Parse(Eval("UNIT_MEASURE").ToString()))%>
                            </td>
                            <td>
                                <b>
                                    <asp:Label ID="lblWeight" runat="server" Text='<%# Eval("WEIGHT") %>'></asp:Label>
                                    %</b>
                            </td>
                            <td>
                                <b>
                                    <asp:Label ID="lblTotal" runat="server" Text='<%# Eval("FINAL_SCORE") %>' />
                                    %</b>
                            </td>
                        </tr>
                    </tbody>
                </ItemTemplate>
            </asp:ListView>
            <fieldset runat="server" id="Fieldset1">
                <legend>Supporting Documents </legend>
                <asp:DataList ID="dtlstAttachments" runat="server" Width="100%">
                    <ItemTemplate>
                        <a href='<%# getfilePath(Eval("ATTACHMENT_NAME").ToString())%>' id="lnk" runat="server"
                            target="_blank">
                            <asp:Image ID="IcnAttachment" runat="server" ImageUrl="~/ui/assets/1334961966_attachment.png" />
                            <%# Eval("ATTACHMENT_NAME")%></a>
                    </ItemTemplate>
                </asp:DataList>
                <br />
            </fieldset>
            <fieldset runat="server" id="Fieldset2">
                <legend>Approve / Decline Request </legend>
                <label for="sf">
                    Status:
                </label>
                &nbsp;<span class="field_desc"><asp:DropDownList ID="ddlStatus" runat="server" Width="130px" />
                </span>
                <asp:Button ID="lnkCloseView" runat="server" class="button" OnClick="btnNew_Click"
                    Text="Submit" />
                <asp:Button ID="lnkCancel" runat="server" class="button" OnClick="lnkCancel_Click"
                    Text="Cancel" />
            </fieldset>
        </asp:Panel>
        <asp:Button ID="btnScoreDetails" runat="server" Style="display: none" />
        <ajaxToolkit:ModalPopupExtender ID="mdlShowScoreDetails" runat="server" TargetControlID="btnScoreDetails"
            PopupControlID="mdlPanelShowScores" BackgroundCssClass="modalBackground" />
        <asp:UpdateProgress ID="UpdateProgressApproval" runat="server" AssociatedUpdatePanelID="UpdatePanelApproval">
            <ProgressTemplate>
                <img runat="server" src="~/ui/images/icons/activity.gif" alt="Please wait..." id="imgPrg" />
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
