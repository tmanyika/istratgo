﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="registeredCompanies.ascx.cs"
    Inherits="scorecard.ui.controls.registeredCompanies" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<h1>
    Registered Companies
</h1>
<asp:UpdatePanel ID="UpdatePanelCompanies" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:ListView ID="lstData" runat="server" 
            OnPagePropertiesChanged="lstData_PagePropertiesChanged" 
            onitemcommand="lstData_ItemCommand">
            <LayoutTemplate>
                <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                    <thead>
                        <tr>
                            <%--  <td>
                                Company ID
                            </td>
                            <td>
                                Account No.
                            </td>--%>
                            <td>
                                Company Name
                            </td>
                            <td>
                                Registration No.
                            </td>
                            <td>
                                VAT No.
                            </td>
                            <td>
                                Industry
                            </td>
                            <%-- <td>
                                Country
                            </td>--%>
                            <td>
                                Contact No.
                            </td>
                            <td>
                                Date Registered
                            </td>
                            <%--    <td>
                                Address
                            </td>--%>
                            <td>
                                Paid
                            </td>
                            <td>
                                Active
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr id="itemPlaceholder" runat="server" />
                    </tbody>
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tr class="odd">
                    <%--  <td>
                        <%# Eval("COMPANY_ID")%>
                    </td>
                   <td>
                        <%# Eval("ACCOUNT_NO")%>
                    </td>--%>
                    <td>
                        <%# Eval("COMPANY_NAME")%>
                    </td>
                    <td>
                        <%# Eval("REGISTRATION_NO")%>
                    </td>
                    <td>
                        <%# Eval("VAT_NO")%>
                    </td>
                    <td>
                        <%# Eval("INDUSTRY_NAME")%>
                    </td>
                    <%--   <td>
                        <%# Eval("COUNTRY_NAME")%>
                    </td>--%>
                    <td>
                        <%# Eval("CONTACT_NO")%>
                    </td>
                    <td>
                        <%# Eval("DATE_REGISTERED")%>
                    </td>
                    <%--    <td>
                        <%# Eval("ADDRESS")%>
                    </td>--%>
                    <td>
                        <asp:CheckBox ID="ChkPaid" runat="server" Checked='<%# Bind("PAID")%>' AutoPostBack="true"
                            ValidationGroup=' <%# Eval("COMPANY_ID")%>' OnCheckedChanged="ChkPaid_CheckedChanged" />
                    </td>
                    <td>
                        <asp:CheckBox ID="ChkActive" Enabled="false" runat="server" Checked='<%# Bind("ACTIVE")%>' />
                    </td>
                </tr>
            </ItemTemplate>
            <EmptyDataTemplate>
                <p class="errorMsg">
                    There are no records to display.
                </p>
            </EmptyDataTemplate>
        </asp:ListView>
        <br />
        <asp:DataPager ID="pager" runat="server" PagedControlID="lstData">
            <Fields>
                <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True" ShowNextPageButton="False"
                    ShowPreviousPageButton="False" />
                <asp:NumericPagerField />
                <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True" ShowNextPageButton="False"
                    ShowPreviousPageButton="False" />
            </Fields>
        </asp:DataPager>
        <br />
        <br />
        <p class="errorMsg">
            <asp:Literal ID="lblMsg" runat="server"></asp:Literal></p>
        <div id="tabs-3">
            <p>
                &nbsp;</p>
        </div>
        <asp:UpdateProgress ID="UpdateProgressComp" runat="server" AssociatedUpdatePanelID="UpdatePanelCompanies">
            <ProgressTemplate>
                <div>
                    <asp:Image ID="imgProj" runat="server" ImageUrl="~/ui/images/icons/activity.gif"
                        AlternateText="Please wait.."></asp:Image></div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
