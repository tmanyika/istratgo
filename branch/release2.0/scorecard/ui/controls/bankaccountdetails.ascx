﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="bankaccountdetails.ascx.cs"
    Inherits="scorecard.ui.controls.bankaccountdetails" %>
<asp:UpdatePanel ID="UpdatePanelBank" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div>
            <h1>
                Bank Account Details</h1>
            <p>
                <label for="lf">
                    Bank Name :
                </label>
                <span class="field_desc">
                    <input class="mf" name="mf1" type="text" runat="server" id="txtBankName" /></span>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="txtBankName"
                    Display="None" ErrorMessage="Bank Name "></asp:RequiredFieldValidator>
            </p>
            <p>
                <label for="lf">
                    Account No :
                </label>
                <span class="field_desc">
                    <input class="mf" name="mf1" type="text" runat="server" id="txtAccountNumber" /></span>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="txtAccountNumber"
                    Display="None" ErrorMessage="Bank Account Name"></asp:RequiredFieldValidator>
            </p>
            <p>
                <label for="lf">
                    Account Type :
                </label>
                <span class="field_desc">
                    <input class="mf" name="mf1" type="text" runat="server" id="txtAccType" /></span>
                <asp:RequiredFieldValidator ID="rqdValidator" runat="server" ControlToValidate="txtAccType"
                    Display="None" ErrorMessage="Account Type"></asp:RequiredFieldValidator>
            </p>
            <p>
                <label for="lf">
                    Branch :
                </label>
                <span class="field_desc">
                    <input class="mf" name="mf1" type="text" runat="server" id="txtBranch" /></span>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="txtBranch"
                    Display="None" ErrorMessage="Branch Name "></asp:RequiredFieldValidator>
            </p>
            <p>
                <label for="lf">
                    Bank Branch Code :
                </label>
                <span class="field_desc">
                    <input class="mf" name="mf1" type="text" runat="server" id="txtBranchCode" /></span>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="txtBranchCode"
                    Display="None" ErrorMessage="Branch Code "></asp:RequiredFieldValidator>
            </p>
            <p class="errorMsg">
                <label for="lf">
                    &nbsp;
                </label>
                <asp:Literal ID="lblError" runat="server"></asp:Literal>
            </p>
            <p>
                <label>
                    &nbsp;</label>
                <asp:Button ID="btnComplete" runat="server" class="button" Text="Update" OnClick="btnComplete_Click"
                    CommandArgument="false" />
                <asp:Button ID="btnCancel" runat="server" class="button" Text="Cancel" CausesValidation="False"
                    PostBackUrl="~/ui/index.aspx" />
            </p>
            <p>
                &nbsp;<asp:ValidationSummary ID="vdSummary" runat="server" HeaderText="Please provide the following fields:"
                    ShowMessageBox="True" ShowSummary="False" />
        </div>
        <asp:UpdateProgress ID="UpdateProgressBank" runat="server" AssociatedUpdatePanelID="UpdatePanelBank">
            <ProgressTemplate>
                <img runat="server" src="~/ui/images/icons/activity.gif" alt="Please wait..." id="imgPrg" />
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
