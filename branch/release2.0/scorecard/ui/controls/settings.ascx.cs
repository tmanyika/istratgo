﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.controllers;
using scorecard.implementations;
using scorecard.entities;

namespace scorecard.ui.controls
{
    public partial class settings : System.Web.UI.UserControl
    {
        settingsmanager settingsController = null;

        public settings() {
            settingsController = new settingsmanager(new applicationsettingsImpl());
        }
         
        protected void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                loadSettings();
            }
        }

        void loadSettings() {

            try {
                var settings = settingsController.getParentSettings();
                if (settings.Count <= 0) { lvSettings.InsertItemPosition = InsertItemPosition.LastItem;  }               
                if (settings.Count() < pgerLvSettings.PageSize) { pgerLvSettings.Visible = false; }               
                lvSettings.DataSource = settings;
                lvSettings.DataBind();             
            }
            catch (Exception e) {
                Util.LogErrors(e);
            }
        }

        protected void lvSettings_ItemCommand(object sender, ListViewCommandEventArgs e) {

            try {
                switch (e.CommandName) {                 
                  
                    case "addRecord":
                        addNewRecord(e);
                        break;                  
                    case "editRecord":
                        editRecord(e);
                        break;
                    case "viewRecord":
                        setChildSettings(e);
                        break;
                    case "deleteRecord":
                        deleteRecord(e);
                        break;
                    case "updateRecord":
                        updateRecord(e);
                        break;
                    case "cancelUpdate":
                        cancelUpdate(e);
                        break;
                    case "cancelAdd":
                        cancelAdd(e);
                        break;
                }
            }
            catch (Exception ex) {
                Util.LogErrors(ex);
            }
        }

        private void cancelAdd(ListViewCommandEventArgs e) {
            lvSettings.InsertItemPosition = InsertItemPosition.None;
            lvSettings.EditIndex = -1;
            loadSettings();  
        }

        void setChildSettings(ListViewCommandEventArgs e) {
            try{
                var id = e.Item.FindControl("lblId") as Label;
                var lblName = e.Item.FindControl("lblName") as Label;
                childSettingHeaderText.InnerText = lblName.Text;
                lblParentID.Text = id.Text;
                loadChildSettings();
                mdlShowChildSettings.Show();
            }
            catch (Exception ex) {
                Util.LogErrors(ex);
            }
        }

        void loadChildSettings() {
            try
            {
                var settings = settingsController.getChildSettings(new applicationsettings { PARENT_ID = int.Parse(lblParentID.Text) });
                pgrLvChildSettings.Visible = true;                
                if (settings.Count <= 0) { lvChildSettings.InsertItemPosition = InsertItemPosition.LastItem; }
                if (settings.Count() < pgrLvChildSettings.PageSize) { pgrLvChildSettings.Visible = false; } 
                lvChildSettings.DataSource = settings.OrderBy(c => c.NAME).ToList();
                lvChildSettings.DataBind();
            }
            catch (Exception e) {
                Util.LogErrors(e);
            }

        }
        void addNewRecord(ListViewCommandEventArgs e) {           
            
            try {
                var txtName = e.Item.FindControl("txtName") as TextBox;
                var txtDescription = e.Item.FindControl("txtDescription") as TextBox;
                var applicationSetting = new applicationsettings {
                     NAME = txtName.Text.Trim(),
                     DESCRIPTION = txtDescription.Text.Trim()
                };
                if( settingsController.AddNewSettings(applicationSetting) > 0){
                    loadSettings();
                 }
            }
            catch (Exception ex) {
                Util.LogErrors(ex);
            }
        }

        void editRecord(ListViewCommandEventArgs e) {
            
            try{                
                lvSettings.EditIndex = e.Item.DataItemIndex;
                lvSettings.InsertItemPosition = InsertItemPosition.None;
                loadSettings();
            }
            catch (Exception ex) {
                Util.LogErrors(ex);
            }
        }
      
        void cancelUpdate(ListViewCommandEventArgs e) {
            try{
                lvSettings.InsertItemPosition = InsertItemPosition.None;
                lvSettings.EditIndex = -1;
                loadSettings();
            }
            catch (Exception ex) {
                Util.LogErrors(ex);
            }        
        }

        void updateRecord(ListViewCommandEventArgs e) {
            try{
                var txtName = lvSettings.Items[e.Item.DataItemIndex].FindControl("txtEditName") as TextBox;
                var txtDescription = lvSettings.Items[e.Item.DataItemIndex].FindControl("txtEditDescription") as TextBox;
                var lblId = lvSettings.Items[e.Item.DataItemIndex].FindControl("lblId") as Label;
                var applicationSetting = new applicationsettings
                {
                    NAME = txtName.Text.Trim(),
                    DESCRIPTION = txtDescription.Text.Trim(),
                    TYPE_ID = int.Parse(lblId.Text)
                };
                if (settingsController.UpdateSetting(applicationSetting) > 0) {
                    lvSettings.EditIndex = -1;
                    lvSettings.InsertItemPosition = InsertItemPosition.None;
                    loadSettings();
                }
            }
            catch (Exception ex) {
                Util.LogErrors(ex);
            }
        }

        void deleteRecord(ListViewCommandEventArgs e){
            try{
                var lblId = e.Item.FindControl("lblId") as Label;
                var applicationSetting = new applicationsettings{           
                    TYPE_ID = int.Parse(lblId.Text)
                };
               // if(settingsController.DeleteSettings(applicationSetting) > 0) {
                    loadSettings();
               // }
            }
            catch (Exception ex) {
                Util.LogErrors(ex);
            }
        }
        protected void btnAddRecord_Click(object sender, EventArgs e){
            lvSettings.InsertItemPosition = InsertItemPosition.LastItem;
            loadSettings();
        }

        protected void lvChildSettings_ItemCommand(object sender, ListViewCommandEventArgs e) {
            try{
                    switch (e.CommandName)
                    {
                        case "addRecord":
                            addNewChildRecord(e);
                            break;
                        case "editRecord":
                            editChildRecord(e);
                            break;                    
                        case "deleteRecord":
                            deleteChildRecord(e);
                            break;
                        case "updateRecord":
                            updateChildRecord(e);
                            break;
                        case "cancelUpdate":
                            cancelChildUpdate(e);
                            break;
                     }
            }
            catch (Exception ex){
                Util.LogErrors(ex);
            }
        }

        protected void btnAddChildRecord_Click(object sender, EventArgs e) {
            lvChildSettings.InsertItemPosition = InsertItemPosition.LastItem;
            loadChildSettings();
            mdlShowChildSettings.Show();
        }

        protected void btnDeleteChildRecords_Click(object sender, EventArgs e){
            mdlShowChildSettings.Show();
        }

        protected void btnClose_Click(object sender, EventArgs e){
            pgrLvChildSettings.SetPageProperties(0, pgrLvChildSettings.PageSize, true);
            mdlShowChildSettings.Hide();
        }

        protected void btnSelectAllChildRecords_Click(object sender, EventArgs e){
            mdlShowChildSettings.Show();
        }

        void addNewChildRecord(ListViewCommandEventArgs e) {

            try {
                var txtName = e.Item.FindControl("txtName") as TextBox;
                var txtDescription = e.Item.FindControl("txtDescription") as TextBox;
                var applicationSetting = new applicationsettings{
                    NAME = txtName.Text.Trim(),
                    DESCRIPTION = txtDescription.Text.Trim(),
                    PARENT_ID = int.Parse(lblParentID.Text)
                };
                if (settingsController.AddNewSettings(applicationSetting) > 0){
                    loadChildSettings();
                    mdlShowChildSettings.Show();
                }
            }
            catch (Exception ex){
                Util.LogErrors(ex);
            }
        }

        void editChildRecord(ListViewCommandEventArgs e){
            try {
                lvChildSettings.EditIndex = e.Item.DataItemIndex;
                lvChildSettings.InsertItemPosition = InsertItemPosition.None;
                loadChildSettings();
                mdlShowChildSettings.Show();
            }
            catch (Exception ex){
                Util.LogErrors(ex);
            }
        }

        void cancelChildUpdate(ListViewCommandEventArgs e) {
            try {
                lvChildSettings.InsertItemPosition = InsertItemPosition.None;
                lvChildSettings.EditIndex = -1;
                loadChildSettings();
                mdlShowChildSettings.Show();
            }
            catch (Exception ex){
                Util.LogErrors(ex);
            }
        }

        void updateChildRecord(ListViewCommandEventArgs e){
            try
            {
                var txtName = lvChildSettings.Items[e.Item.DataItemIndex].FindControl("txtEditName") as TextBox;
                var txtDescription = lvChildSettings.Items[e.Item.DataItemIndex].FindControl("txtEditDescription") as TextBox;
                var lblId = lvChildSettings.Items[e.Item.DataItemIndex].FindControl("lblId") as Label;
                var applicationSetting = new applicationsettings{
                    NAME = txtName.Text.Trim(),
                    DESCRIPTION = txtDescription.Text.Trim(),
                    TYPE_ID = int.Parse(lblId.Text)                    
                };
                if (settingsController.UpdateSetting(applicationSetting) > 0){
                    lvChildSettings.EditIndex = -1;
                    lvChildSettings.InsertItemPosition = InsertItemPosition.None;
                    loadChildSettings();
                    mdlShowChildSettings.Show();
                }
            }
            catch (Exception ex){
                Util.LogErrors(ex);
            }
        }

        void deleteChildRecord(ListViewCommandEventArgs e) {
            try {
                var lblId = e.Item.FindControl("lblId") as Label;
                var applicationSetting = new applicationsettings{
                    TYPE_ID = int.Parse(lblId.Text)
                };
                if (settingsController.DeleteSettings(applicationSetting) > 0){
                    loadChildSettings();
                    mdlShowChildSettings.Show();
                }
            }
            catch (Exception ex) {
                Util.LogErrors(ex);
            }
        }
        protected void lvChildSettings_PagePropertiesChanged(object sender, EventArgs e) {
            loadChildSettings();
            mdlShowChildSettings.Show();
        }

        protected void lvSettings_PagePropertiesChanged(object sender, EventArgs e) {
            loadSettings();
        }

        protected void lnkCancel_Click(object sender, EventArgs e){
            Response.Redirect("index.aspx");
        }
    }
    }
