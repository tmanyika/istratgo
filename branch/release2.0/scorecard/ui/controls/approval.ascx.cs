﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using scorecard.controllers;
using scorecard.entities;
using scorecard.interfaces;
using scorecard.implementations;
using HR.Human.Resources;
using System.Net.Mail;
using System.Email.Communication;


namespace scorecard.ui.controls
{
    public partial class approval : System.Web.UI.UserControl
    {
        scoresmanager scoreManager;
        useradmincontroller usersDef;
        goaldefinition goalDef;
        structurecontroller orgunits;

        private UserInfo user
        {
            get { return Session["UserInfo"] as UserInfo; }
        }

        public approval()
        {
            scoreManager = new scoresmanager(new scorecard.implementations.scoresImpl());
            usersDef = new useradmincontroller(new useradminImpl());
            goalDef = new goaldefinition(new goalsImpl());
            orgunits = new structurecontroller(new structureImpl());
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadAllWorkflows();
                getScoreCardStatus();
            }
        }

        void loadAllWorkflows()
        {
            try
            {
                var company_id = user.CompanyId;
                var userRole = user.UserTypeId;
                var userID = user.UserId;

                pnlNoData.Visible = false;
                var workflows = scoreManager.getApprovalWorkflowItems().Where(s => s.STATUS == Util.getTypeDefinitionsWorkFlowNewRequestId() && s.COMPANY_ID == company_id);
                if (userRole != Util.getTypeDefinitionsUserTypeAdministrator())
                    workflows = workflows.Where(t => t.APPROVER_ID == userID).ToList();
                if (workflows.Count() <= 0 || workflows.Count() < dtPagerSubmittedWorkflows.PageSize) { dtPagerSubmittedWorkflows.Visible = false; }
                if (workflows.Count() <= 0) { pnlNoData.Visible = true; }
                lvSubmittedWorkflows.DataSource = workflows.ToList();
                lvSubmittedWorkflows.DataBind();
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        public string getTypeName(int id)
        {
            return usersDef.appsettings.getApplicationSetting(new entities.applicationsettings { TYPE_ID = id }).NAME;
        }

        public string getApproverName(int id)
        {
            try
            {
                IEmployee emp = new EmployeeBL();
                return emp.GetById(id).FullName; ;
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return "";
            }
        }

        public string getObjectName(int id, int criteria)
        {
            try
            {
                if (id > 0)
                {
                    if (criteria == Util.getTypeDefinitionsRolesTypeID())
                    {
                        IEmployee obj = new EmployeeBL();
                        return obj.GetById(id).FullName;
                    }
                    else if (criteria == Util.getTypeDefinitionsProjectID()) return new projectmanagement(new projectImpl()).get(id).PROJECT_NAME;
                    else return orgunits.getOrgunitDetails(new Orgunit { ID = id }).ORGUNIT_NAME;
                }
            }
            catch (Exception e) { Util.LogErrors(e); }
            return "";
        }

        void loadGoals(int selectedId, int criteria, int submittedId)
        {
            try
            {
                var objectives = scoreManager.getSubmittedScores(new submissionWorkflow
                {
                    ID = submittedId,
                    CRITERIA_TYPE_ID = criteria,
                    SELECTED_ITEM_VALUE = selectedId
                });
                var finalScore = objectives.Sum(u => Math.Round(u.FINAL_SCORE, 2));
                var finalWeightTotal = objectives.Sum(u => u.WEIGHT);
                var finalPercentage = (finalScore / double.Parse(finalWeightTotal.ToString()) * 100);
                scores totalDetails = new scores { GOAL_DESCRIPTION = "<b>Totals</b> ", WEIGHT = finalWeightTotal, FINAL_SCORE = Math.Round(finalPercentage, 2) };
                objectives.Add(totalDetails);
                lvGoals.DataSource = objectives;
                lvGoals.DataBind();
                getAttachments(submittedId);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void getScoreCardStatus()
        {
            var areaList = goalDef.appsettings.getChildSettings(new applicationsettings { PARENT_ID = Util.getTypeDefinitionsParentWorkFlowApprovalId() }).Where(t => t.TYPE_ID != Util.getTypeDefinitionsWorkFlowNewRequestId()).ToList();
            BindControl.BindDropdown(ddlStatus, "NAME", "TYPE_ID", "- select option -", Util.getTypeDefinitionsWorkFlowNewRequestId().ToString(), areaList);
        }

        void loadSetupTypes(DropDownList ddlPerspective)
        {
            try
            {
                var setuptypes = goalDef.appsettings.getChildSettings(new entities.applicationsettings { PARENT_ID = Util.getTypeDefinitionsPerspectivesParentID() });
                BindControl.BindDropdown(ddlPerspective, "NAME", "TYPE_ID", setuptypes);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void loadJobTitles(DropDownList ddlJobTitles)
        {
            try
            {
                IEmployee obj = new EmployeeBL();
                var titles = obj.GetAll(Util.user.OrgId);
                BindControl.BindDropdown(ddlJobTitles, "FullName", "EmployeeId", titles);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void loadStrategicGoals(DropDownList ddlStrategic)
        {
            try
            {
                var strategic = goalDef.listStrategicGoals(new company { COMPANY_ID = Util.user.CompanyId });
                BindControl.BindDropdown(ddlStrategic, "OBJECTIVE", "ID", strategic);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        public string getCurrentStore(int ID)
        {
            try
            {
                var score = scoreManager.getScoresByGoalID(new entities.goals { ID = ID, PERSPECTIVE = int.Parse(hdnValueId.Value) });
                return score.Single().SCORES.ToString();
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return "";
            }
        }

        public string getFinalStore(int ID)
        {
            try
            {
                var score = scoreManager.getScoresByGoalID(new entities.goals { ID = ID, PERSPECTIVE = int.Parse(hdnValueId.Value) });
                return score.Single().FINAL_SCORE.ToString();
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return "0";
            }
        }

        void loadAreas(DropDownList ddlAreas)
        {
            try
            {

                var userRole = Util.user.UserTypeId;
                var userID = int.Parse(Session["userId"].ToString());
                var structure = goalDef.orgstructure.getActiveCompanyOrgStructure(new company { COMPANY_ID = Util.user.CompanyId });

                if (userRole != Util.getTypeDefinitionsUserTypeAdministrator())
                    structure.RowFilter = string.Format("OWNER_ID = {0}", userID);
                BindControl.BindDropdown(ddlAreas, "ORGUNIT_NAME", "ID", structure);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        protected void lvSubmittedWorkflows_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                switch (e.CommandName)
                {
                    case "View":
                        var lblItemSelected = e.Item.FindControl("lblItemSelected") as Label;
                        var lblCriteria = e.Item.FindControl("lblCriteria") as Label;
                        var lblSubmittedId = e.Item.FindControl("lblID") as Label;

                        hdnAreaId.Value = lblCriteria.Text.Trim();
                        hdnValueId.Value = lblItemSelected.Text.Trim();

                        lblID.Text = lblSubmittedId.Text;
                        lblArea.Text = getTypeName(int.Parse(hdnAreaId.Value));
                        lblName.Text = getObjectName(int.Parse(hdnValueId.Value), int.Parse(hdnAreaId.Value));

                        loadGoals(int.Parse(hdnValueId.Value), int.Parse(hdnAreaId.Value), int.Parse(lblSubmittedId.Text));
                        mdlShowScoreDetails.Show();
                        break;
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void getAttachments(int submittedId)
        {
            try
            {
                var attachments = scoreManager.getAllAttachments(new submissionWorkflow
                {
                    ID = submittedId
                });
                dtlstAttachments.DataSource = attachments;
                dtlstAttachments.DataBind();
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        public string getfilePath(string fileName)
        {
            return Util.getConfigurationSettingServerAttachmentsUrl() + fileName;
        }

        void changeStatus()
        {
            try
            {
                if (int.Parse(ddlStatus.SelectedValue) != Util.getTypeDefinitionsWorkFlowNewRequestId())
                {
                    var result = scoreManager.updateWorkflowStatus(new submissionWorkflow
                    {
                        STATUS = int.Parse(ddlStatus.SelectedValue),
                        ID = int.Parse(lblID.Text)
                    });
                    if (result > 0)
                    {
                        loadAllWorkflows();
                        int areaId = int.Parse(hdnAreaId.Value);
                        int valId = int.Parse(hdnValueId.Value);
                        int userId = 0;
                        if (areaId == Util.getTypeDefinitionsOrgUnitsTypeID())
                            userId = new structurecontroller(new structureImpl()).getOrgunitDetails(new Orgunit { ID = valId }).OWNER_ID;
                        else if (areaId == Util.getTypeDefinitionsProjectID())
                            userId = (int)new projectmanagement(new projectImpl()).get(valId).RESPONSIBLE_PERSON_ID;
                        else if (areaId == Util.getTypeDefinitionsRolesTypeID())
                            userId = valId;
                        IEmployee obj = new EmployeeBL();
                        string statusDesc = ddlStatus.SelectedItem.Text;
                        var user = obj.GetById(userId);
                        sendMail(user, statusDesc);
                    }
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void sendMail(Employee user, string statusDesc)
        {
            try
            {
                string result = string.Empty;
                int emailId = Util.getWorkflowApprovalEmailId();

                MailAddressCollection addy = new MailAddressCollection();
                addy.Add(new MailAddress(user.EmailAddress, user.FullName));

                object[] subjectData = { statusDesc.ToLower() };
                object[] mydata = { statusDesc };

                IMailRelay objm = new MailRelayBL();
                MailRelay obj = new MailRelay
                {
                    MailId = emailId,
                    Recipient = addy,
                    Data = mydata,
                    SubjectData = subjectData
                };
                objm.SendMail(obj, out result);
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            changeStatus();
        }

        protected void lnkCancel_Click(object sender, EventArgs e)
        {
            mdlShowScoreDetails.Hide();
        }

        protected void lnkDone_Click(object sender, EventArgs e)
        {
            Response.Redirect("index.aspx");
        }
    }
}