﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="jobtitles.ascx.cs" Inherits="scorecard.ui.controls.jobtitles" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<h1>
    &nbsp; Job Title Administration</h1>
<asp:UpdatePanel ID="UpdatePanelJob" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:ListView ID="lvJobTitles" runat="server" OnItemCommand="lvJobTitles_ItemCommand"
            OnItemEditing="lvJobTitle_ItemEditing" OnPagePropertiesChanged="lvJobTitles_PagePropertiesChanged">
            <LayoutTemplate>
                <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                    <thead>
                        <tr runat="server">
                            <td>
                                <input type="checkbox" class="checkall" />
                            </td>
                            <td>
                                Name
                            </td>
                            <td>
                                Active
                            </td>
                            <td>
                                Edit
                            </td>
                            <td>
                                Delete
                            </td>
                        </tr>
                        <tr id="ItemPlaceHolder" runat="server">
                        </tr>
                    </thead>
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tbody>
                    <tr class="odd">
                        <td>
                            <input type="checkbox" />
                            <asp:Label ID="lblID" runat="server" Visible="false" Text='<%# Eval("ID") %>' />
                        </td>
                        <td>
                            <%# Eval("NAME") %>
                        </td>
                        <td>
                            <%# Eval("ACTIVE") %>
                        </td>
                        <td>
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkEdit" CommandName="Edit" class="ui-state-default ui-corner-all"
                                    title="Edit Record"><span class="ui-icon ui-icon-pencil">
                        </span></asp:LinkButton>
                            </ul>
                        </td>
                        <td>
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkDelete" OnClientClick="return confirmDelete();"
                                    CommandName="deleteJobTitle" class="ui-state-default ui-corner-all" title="Delete Record"><span class="ui-icon ui-icon-trash">
                        </span></asp:LinkButton>
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </ItemTemplate>
            <EditItemTemplate>
                <tbody>
                    <tr class="odd">
                        <td>
                            <input type="checkbox" />
                            <asp:Label ID="lblID" runat="server" Visible="false" Text='<%# Eval("ID") %>' />
                        </td>
                        <td>
                            <asp:TextBox runat="server" Text='<%# Eval("NAME") %>' ID="txtName" />
                        </td>
                        <td>
                            <asp:CheckBox ID="ckActive" runat="server" Checked='<%# Eval("ACTIVE") %>' />
                        </td>
                        <td>
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkSave" CommandName="updateJobTitle" class="ui-state-default ui-corner-all"
                                    title="Update Record"><span class="ui-icon ui-icon-disk">
                        </span></asp:LinkButton>
                            </ul>
                        </td>
                        <td>
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkCancelUpdate" CommandName="cancelUpdate" class="ui-state-default ui-corner-all"
                                    title="Cancel Update"><span class="ui-icon ui-icon-cancel">
                        </span></asp:LinkButton>
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </EditItemTemplate>
            <InsertItemTemplate>
                <tbody>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <asp:TextBox ID="txtName" runat="server" />
                        </td>
                        <td>
                            <asp:CheckBox ID="ckActive" runat="server" />
                        </td>
                        <td>
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkSave" class="ui-state-default ui-corner-all"
                                    title="Save this Record" CommandName="addNewJobTitle"><span class="ui-icon ui-icon-disk">
                        </span></asp:LinkButton>
                            </ul>
                        </td>
                        <td>
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkCancel" class="ui-state-default ui-corner-all"
                                    title="Cancel Save " CommandName="cancelUpdate"><span class="ui-icon ui-icon-cancel">
                        </span></asp:LinkButton>
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </InsertItemTemplate>
        </asp:ListView>
        <br />
        <asp:DataPager ID="dtPagerJobTitles" runat="server" PagedControlID="lvJobTitles" PageSize="15">
            <Fields>
                <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True" ShowNextPageButton="False"
                    ShowPreviousPageButton="False" />
                <asp:NumericPagerField />
                <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True" ShowNextPageButton="False"
                    ShowPreviousPageButton="False" />
            </Fields>
        </asp:DataPager>
        <br />
        <br />
        <div id="tabs-3">
            <p>
                <asp:Button ID="btnNew" runat="server" CssClass="button tooltip" Text="Add New Job Title"
                    OnClick="btnNew_Click" />
                &nbsp;<asp:Button ID="lnkCancel0" runat="server" class="button tooltip" Text="Done" />
        </div>
        <asp:UpdateProgress ID="UpdateProgressJob" runat="server" AssociatedUpdatePanelID="UpdatePanelJob">
            <ProgressTemplate>
                <div>
                    <asp:Image ID="imgOrg" runat="server" ImageUrl="~/ui/images/icons/activity.gif" AlternateText="Please wait..">
                    </asp:Image></div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
