﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ui/inside.Master" AutoEventWireup="true"
    CodeBehind="approvalflow.aspx.cs" Inherits="scorecard.ui.approvalflow" %>

<%@ Register Src="controls/orgstructure.ascx" TagName="orgstructure" TagPrefix="uc1" %>
<%@ Register Src="controls/approval.ascx" TagName="approval" TagPrefix="uc2" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="OrgStructure">
    <uc1:orgstructure ID="orgstructure1" runat="server" />
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MainContent">
    <uc2:approval ID="approval1" runat="server" />
</asp:Content>
