﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.controllers;
using scorecard.implementations;
using scorecard.entities;
using Scorecard.Business.Rules;
using System.Text;
using vb = Microsoft.VisualBasic;

namespace scorecard.ui.controls
{
    public partial class goal : System.Web.UI.UserControl
    {
        goaldefinition goalDef;
        scoresmanager scoreManager;
        IBusinessRule bR;
        string envContextHolder;

        public goal()
        {
            goalDef = new goaldefinition(new goalsImpl());
            scoreManager = new scoresmanager(new scorecard.implementations.scoresImpl());
            bR = new BusinessRuleBL();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            ResetLabels();
            if (!IsPostBack)
            {
                getGoalsCriteriaTypes();
                setCriteriaSelected();
                loadGoals();
            }
        }

        private void ResetLabels()
        {
            lblMsg.Text = "";
            LblError.Text = "";
        }

        public string getTypeName(int id)
        {
            return goalDef.appsettings.getApplicationSetting(new entities.applicationsettings { TYPE_ID = id }).NAME;
        }

        void getGoalsCriteriaTypes()
        {
            var areaList = goalDef.appsettings.getChildSettings(new applicationsettings { PARENT_ID = Util.getTypeDefinitionsGoalAreaTypes() }).ToList();
            ddlArea.DataSource = areaList;
            ddlArea.DataTextField = "NAME";
            ddlArea.DataValueField = "TYPE_ID";
            ddlArea.DataBind();
        }

        void loadJobTitles(DropDownList ddlJobTitles)
        {
            try
            {
                UserInfo user = Session["UserInfo"] as UserInfo;
                var titles = goalDef.jobtitles.getAllCompanyJobTitles(new company { COMPANY_ID = user.CompanyId }).Where(j => j.ACTIVE == true);
                ddlJobTitles.DataSource = titles;
                ddlJobTitles.DataTextField = "NAME";
                ddlJobTitles.DataValueField = "ID";
                ddlJobTitles.DataBind();
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void loadWeights(DropDownList ddlWeights)
        {
            if (ddlWeights != null)
                for (int i = 1; i <= 100; i++)
                    ddlWeights.Items.Add(new ListItem { Text = i.ToString() + "%", Value = i.ToString() });
        }

        void loadStrategicGoals(DropDownList ddlStrategic)
        {
            try
            {
                var strategic = goalDef.listStrategicGoals(new company { COMPANY_ID = Util.user.CompanyId });
                ddlStrategic.DataSource = strategic;
                ddlStrategic.DataTextField = "OBJECTIVE";
                ddlStrategic.DataValueField = "ID";
                ddlStrategic.DataBind();
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void loadUnitsOfMeasure(DropDownList ddlMeasure)
        {
            try
            {
                var unitsOfMeasure = goalDef.appsettings.getChildSettings(new applicationsettings { PARENT_ID = Util.getTypeDefinitionsUnitOfMeasureTypesID() }).ToList();
                ddlMeasure.DataSource = unitsOfMeasure;
                ddlMeasure.DataTextField = "NAME";
                ddlMeasure.DataValueField = "TYPE_ID";
                ddlMeasure.DataBind();
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void loadProjects(int companyId)
        {
            projectmanagement proj = new projectmanagement(new projectImpl());
            UserInfo user = Session["UserInfo"] as UserInfo;
            int adminRoleId = Util.getTypeDefinitionsUserTypeAdministrator();

            ddlValueTypes.DataTextField = "PROJECT_NAME";
            ddlValueTypes.DataValueField = "PROJECT_ID";
            int roleId = (int)Util.user.UserTypeId;
            IEnumerable<project> projL = proj.get(companyId, true);
            if (adminRoleId != user.UserTypeId) projL = projL.Where(c => c.RESPONSIBLE_PERSON_ID == user.UserId);
            BindControl.BindDropdown(ddlValueTypes, projL);
        }

        void loadAreas(DropDownList ddlAreas)
        {
            try
            {
                var userRole = Util.user.UserTypeId;
                var userID = Util.user.UserId;
                var structure = goalDef.orgstructure.getActiveCompanyOrgStructure(Util.user.CompanyId);
                if (userRole != Util.getTypeDefinitionsUserTypeAdministrator())
                    structure.RowFilter = string.Format("OWNER_ID = {0}", userID);

                ddlAreas.DataSource = structure;
                ddlAreas.DataTextField = "ORGUNIT_NAME";
                ddlAreas.DataValueField = "ID";
                ddlAreas.DataBind();
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void loadInitialPerspectives()
        {

            var ddlStrategic = lvGoals.InsertItem.FindControl("ddlStrategic") as DropDownList;
            var ddlAreas = lvGoals.InsertItem.FindControl("ddlArea") as DropDownList;
            var ddlRoles = lvGoals.InsertItem.FindControl("ddlRole") as DropDownList;
            var ddlWeights = lvGoals.InsertItem.FindControl("ddlWeight") as DropDownList;
            var ddlUnitOfMeasure = lvGoals.InsertItem.FindControl("ddlUnitOfMeasure") as DropDownList;

            loadAreas(ddlAreas);
            loadStrategicGoals(ddlStrategic);
            loadJobTitles(ddlRoles);
            loadWeights(ddlWeights);
            loadUnitsOfMeasure(ddlUnitOfMeasure);
        }

        List<entities.goals> getGoals()
        {
            if (!string.IsNullOrEmpty(ddlArea.SelectedValue) && !string.IsNullOrEmpty(ddlValueTypes.SelectedValue))
            {
                var objectives = goalDef.listGeneralGoals(new company { COMPANY_ID = Util.user.CompanyId }).Where(t => t.AREA_TYPE_ID == int.Parse(ddlArea.SelectedValue) && t.AREA_ID == int.Parse(ddlValueTypes.SelectedValue)).ToList(); ;
                return objectives;
            }
            return new List<entities.goals>();

        }

        void loadGoals()
        {
            try
            {
                var objectives = goalDef.listGeneralGoals(new company { COMPANY_ID = Util.user.CompanyId }).Where(t => t.AREA_TYPE_ID == int.Parse(ddlArea.SelectedValue) && t.AREA_ID == int.Parse(ddlValueTypes.SelectedValue)).ToList(); ;
                if (objectives.Count <= 0)
                {
                    lvGoals.InsertItemPosition = InsertItemPosition.LastItem;
                }
                else SetContextId(objectives[0]);

                dtPagerlvGoals.Visible = (objectives.Count > dtPagerlvGoals.PageSize) ? true : false;

                lvGoals.DataSource = objectives;
                lvGoals.DataBind();
                loadInitialPerspectives();
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        public string getRoleName(int roleID)
        {
            return goalDef.jobtitles.getJobTitleInfo(new JobTitle { ID = roleID }).NAME;
        }
        public string getAreaName(int areaID)
        {
            return goalDef.orgstructure.getOrgunitDetails(new Orgunit { ID = areaID }).ORGUNIT_NAME;
        }
        public string getStrategicObjective(int id)
        {
            return goalDef.listStrategicGoals(new company { COMPANY_ID = Util.user.CompanyId }).Where(s => s.ID == id).Single().OBJECTIVE;
        }
        protected void btnNew_Click(object sender, EventArgs e)
        {
            if (CanAdd(0, 0))
            {
                lvGoals.InsertItemPosition = InsertItemPosition.LastItem;
                loadGoals();
            }
        }

        bool infoValid(string description, string target, int unitofmeasure)
        {
            bool valid = true;
            StringBuilder strB = new StringBuilder("");
            if (string.IsNullOrEmpty(description))
            {
                valid = false;
                strB.Append("- Goal is required<br/>");
            }

            if (bR.UnitOfMeasureRequireNumericValue(unitofmeasure))
            {
                if (!vb.Information.IsNumeric(target))
                {
                    valid = false;
                    strB.Append("- Target should be numeric<br/>");
                }
            }
            else if (bR.UnitOfMeasureRequireYesNoValue(unitofmeasure))
            {
                if (!bR.UnitOfMeasureRequireYesNoValueValid(target))
                {
                    valid = false;
                    strB.Append("- Invalid Target value. Only Yes or No is allowed<br/>");
                }
            }

            LblError.Text = valid ? string.Empty : string.Format("<p style='color:Red;'>Please correct the following:<br/></p>{0}", strB.ToString());
            return valid;
        }

        void createNewGoal(ListViewCommandEventArgs e)
        {
            try
            {
                int? defInt = null;

                var txtDescription = e.Item.FindControl("txtDescription") as TextBox;
                var txtRational = e.Item.FindControl("txtRational") as TextBox;
                var txtTarget = e.Item.FindControl("txtTarget") as TextBox;
                var txtUnitOfMeasure = e.Item.FindControl("ddlUnitOfMeasure") as DropDownList;
                var ddlWeight = e.Item.FindControl("ddlWeight") as DropDownList;
                var ddlStrategic = e.Item.FindControl("ddlStrategic") as DropDownList;
                var envContext = string.IsNullOrEmpty(txtEnvContext.Text) ? string.Empty : txtEnvContext.Text.Trim();
                var contextId = string.IsNullOrEmpty(hdnContextId.Value) ? defInt : int.Parse(hdnContextId.Value);
                var rational = txtRational.Text;

                int areaId = int.Parse(ddlValueTypes.SelectedValue);

                bool valid = infoValid(txtDescription.Text, txtTarget.Text, int.Parse(txtUnitOfMeasure.SelectedValue));
                if (CanAdd(int.Parse(ddlWeight.SelectedValue), 0) && valid)
                {
                    int envContextId = goalDef.defineGeneralGoal(new scorecard.entities.goals
                     {
                         AREA_TYPE_ID = int.Parse(ddlArea.SelectedValue),
                         AREA_ID = int.Parse(ddlValueTypes.SelectedValue),
                         COMPANY_ID = Util.user.CompanyId,
                         GOAL_DESCRIPTION = txtDescription.Text.Trim(),
                         TARGET = txtTarget.Text.Trim(),
                         UNIT_MEASURE = int.Parse(txtUnitOfMeasure.SelectedValue),
                         WEIGHT = int.Parse(ddlWeight.SelectedValue),
                         STRATEGIC_ID = int.Parse(ddlStrategic.SelectedValue),
                         ORG_ID = int.Parse(ddlArea.SelectedValue),
                         ENV_CONTEXT = envContext,
                         ENV_CONTEXT_ID = contextId,
                         RATIONAL = rational
                     });
                    lvGoals.InsertItemPosition = InsertItemPosition.None;
                    loadGoals();
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void updateGoal(ListViewCommandEventArgs e)
        {
            try
            {
                int? defInt = null;
                var id = e.Item.FindControl("lblIDE") as Label;
                var txtDescription = e.Item.FindControl("txtEditDescription") as TextBox;
                var txtEditRational = e.Item.FindControl("txtEditRational") as TextBox;
                var txtTarget = e.Item.FindControl("txtEditTarget") as TextBox;
                var txtUnitOfMeasure = e.Item.FindControl("ddlUnitOfMeasure") as DropDownList;
                var ddlWeight = e.Item.FindControl("ddlEditWeight") as DropDownList;
                var ddlStrategic = e.Item.FindControl("ddlEditStrategic") as DropDownList;
                var rational = txtEditRational.Text;
                var envContext = txtEnvContext.Text;
                var contextId = string.IsNullOrEmpty(hdnContextId.Value) ? defInt : int.Parse(hdnContextId.Value);

                bool valid = infoValid(txtDescription.Text, txtTarget.Text, int.Parse(txtUnitOfMeasure.SelectedValue));
                if (CanAdd(int.Parse(ddlWeight.SelectedValue), int.Parse(id.Text)) && valid)
                {
                    goalDef.updateGeneralGoal(new scorecard.entities.goals
                    {
                        ID = int.Parse(id.Text),
                        AREA_TYPE_ID = int.Parse(ddlArea.SelectedValue),
                        AREA_ID = int.Parse(ddlValueTypes.SelectedValue),
                        COMPANY_ID = Util.user.CompanyId,
                        GOAL_DESCRIPTION = txtDescription.Text.Trim(),
                        TARGET = txtTarget.Text.Trim(),
                        UNIT_MEASURE = int.Parse(txtUnitOfMeasure.SelectedValue),
                        WEIGHT = int.Parse(ddlWeight.SelectedValue),
                        STRATEGIC_ID = int.Parse(ddlStrategic.SelectedValue),
                        RATIONAL = rational,
                        ENV_CONTEXT = envContext,
                        ENV_CONTEXT_ID = contextId
                    });
                    lvGoals.InsertItemPosition = InsertItemPosition.None;
                    lvGoals.EditIndex = -1;
                    loadGoals();
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void deleteGoalObjective(ListViewCommandEventArgs e)
        {
            try
            {

                var lblID = e.Item.FindControl("lblID") as Label;
                var attachedScores = scoreManager.getScoresByGoalID(new entities.goals { ID = int.Parse(lblID.Text) }).ToList();
                if (attachedScores.Count == 0)
                {
                    goalDef.deleteGeneralGoal(new scorecard.entities.goals
                    {
                        ID = int.Parse(lblID.Text)
                    });
                    lblMsg.Text = "";
                }
                else lblMsg.Text = "This record cannot be deleted because it has records associated with it";
                loadGoals();
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void cancelUpdate()
        {
            lvGoals.EditIndex = -1;
            lvGoals.InsertItemPosition = InsertItemPosition.None;
            loadGoals();
        }

        bool CanAdd(int currentlyAddedWeight, int goalId)
        {
            lblMsg.Text = "";
            const int maximum = 100;
            var goals = getGoals();
            int total = goals.Where(i => i.ID != goalId).Sum(g => g.WEIGHT);
            int totalWeighting = currentlyAddedWeight + total;
            if (totalWeighting > maximum)
            {
                lblMsg.Text = string.Format("Your total weight should not exceed {0}%. You have a total of {1}%", maximum, totalWeighting);
                return false;
            }
            return true;
        }

        protected void lvGoal_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            LblError.Text = "";
            envContextHolder = txtEnvContext.Text;
            if (e.Item.ItemType == ListViewItemType.InsertItem ||
                e.Item.ItemType == ListViewItemType.DataItem)
            {
                switch (e.CommandName)
                {
                    case "addNewGoal":
                        createNewGoal(e);
                        break;
                    case "updateGoals":
                        updateGoal(e);
                        break;
                    case "deleteGoal":
                        deleteGoalObjective(e);
                        break;
                    case "cancelUpdate":
                        cancelUpdate();
                        break;
                }
            }
        }

        protected void lvGoal_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            lvGoals.EditIndex = e.NewEditIndex;
            lvGoals.InsertItemPosition = InsertItemPosition.None;
            loadGoals();
            txtEnvContext.Text = envContextHolder;

            var ddlStrategic = lvGoals.Items[e.NewEditIndex].FindControl("ddlEditStrategic") as DropDownList;
            var ddlWeights = lvGoals.Items[e.NewEditIndex].FindControl("ddlEditWeight") as DropDownList;
            var ddlUnitOfMeasure = lvGoals.Items[e.NewEditIndex].FindControl("ddlUnitOfMeasure") as DropDownList;

            loadStrategicGoals(ddlStrategic);
            loadWeights(ddlWeights);
            loadUnitsOfMeasure(ddlUnitOfMeasure);

            if (lvGoals.EditItem.FindControl("lblID") != null)
            {
                string id = (lvGoals.EditItem.FindControl("lblID") as Label).Text;
                entities.goals goal = new goaldefinition(new goalsImpl()).getGoalById(int.Parse(id));
                if (ddlStrategic.Items.FindByValue(goal.STRATEGIC_ID.ToString()) != null)
                    ddlStrategic.SelectedValue = goal.STRATEGIC_ID.ToString();
                if (ddlWeights.Items.FindByValue(goal.WEIGHT.ToString()) != null)
                    ddlWeights.SelectedValue = goal.WEIGHT.ToString();
                if (ddlUnitOfMeasure.Items.FindByValue(goal.UNIT_MEASURE.ToString()) != null)
                    ddlUnitOfMeasure.SelectedValue = goal.UNIT_MEASURE.ToString();
            }
        }

        protected void lvGoal_PagePropertiesChanged(object sender, EventArgs e)
        {
            loadGoals();
        }

        protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlValueTypes.Items.Clear();
            clearValueHolders();
            setCriteriaSelected();
        }

        void clearValueHolders()
        {
            hdnContextId.Value = string.Empty;
            txtEnvContext.Text = string.Empty;
        }

        void setCriteriaSelected()
        {
            try
            {
                int areaId = int.Parse(ddlArea.SelectedValue);
                if (areaId == Util.getTypeDefinitionsRolesTypeID())
                    loadJobTitles(ddlValueTypes);
                else if (areaId == Util.getTypeDefinitionsProjectID())
                    loadProjects(Util.user.CompanyId);
                else loadAreas(ddlValueTypes);

                lvGoals.InsertItemPosition = InsertItemPosition.None;
                loadGoals();
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        protected void ddlValueTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            lvGoals.InsertItemPosition = InsertItemPosition.None;
            clearValueHolders();
            loadGoals();
        }

        private void SetContextId(scorecard.entities.goals obj)
        {
            if (obj.ENV_CONTEXT_ID == null)
            {
                var envData = goalDef.getEnvironmentContext(new environmentcontext
                {
                    COMPANY_ID = Util.user.CompanyId,
                    AREA_ID = int.Parse(ddlArea.SelectedValue),
                    AREA_TYPE_ID = int.Parse(ddlValueTypes.SelectedValue)
                });
                if (envData.ENV_CONTEXT_ID > 0)
                {
                    hdnContextId.Value = envData.ENV_CONTEXT_ID.ToString();
                    txtEnvContext.Text = envData.ENV_CONTEXT;
                }
            }
            else
            {
                hdnContextId.Value = obj.ENV_CONTEXT_ID.ToString();
                txtEnvContext.Text = obj.ENV_CONTEXT;
            }
        }

        protected void lnkCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("index.aspx");
        }

        protected void dtPagerlvGoals_PreRender(object sender, EventArgs e)
        {
            if (lvGoals.EditIndex == -1)
                loadGoals();
        }

        protected void btnContextSave_Click(object sender, EventArgs e)
        {
            addContextOfEnvironment();
        }

        private void addContextOfEnvironment()
        {
            string envContext = txtEnvContext.Text;
            int areadId = int.Parse(ddlArea.SelectedValue);
            int areaTypeId = int.Parse(ddlValueTypes.SelectedValue);
            int compId = Util.user.CompanyId;
            int contextId = string.IsNullOrEmpty(hdnContextId.Value) ? 0 : int.Parse(hdnContextId.Value);

            int contxtId = goalDef.addEnvironmentContext(new environmentcontext
            {
                AREA_ID = areadId,
                AREA_TYPE_ID = areaTypeId,
                COMPANY_ID = compId,
                DATE_CREATED = DateTime.Now,
                DATE_UPDATED = DateTime.Now,
                ENV_CONTEXT = envContext,
                ENV_CONTEXT_ID = contextId
            });

            hdnContextId.Value = contxtId.ToString();
            lblMsg.Text = Messages.GetSaveMessage();
        }
    }
}