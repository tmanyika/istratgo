﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using scorecard.controllers;
using scorecard.implementations;
using scorecard.entities;
using System.Text;
using scorecard.welcome.interfaces;
using scorecard.welcome;
using scorecard.welcome.logic;

namespace scorecard.ui.controls
{
    public partial class menu : System.Web.UI.UserControl
    {
        menucontroller accessManager;
        IWelcome start;

        public menu()
        {
            accessManager = new menucontroller(new rolebasedImpl());
            start = new WelcomeBL();
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                setUser();
                loadMenus();
            }
        }

        void setUser()
        {
            if (Session["UserInfo"] != null)
            {
                lblWelcomeMessage.Text = Util.user.FullName;
            }
        }

        void loadMenus()
        {
            try
            {
                if (Util.user.ShowAgain)
                {
                    var data = start.GetShowAgainWelcome(Util.user.UserId);
                    BindControl.BindDataList(dlstGettingStarted, data);
                    MainView.SetActiveView(vwGetStarted);
                }
                else
                {
                    var userRole = Util.user.UserTypeId;
                    var companyID = Util.user.CompanyId;
                    var menus = accessManager.getAccessByCompany(new company { COMPANY_ID = companyID }).Where(u => u.USER_TYPE_ID == userRole && u.MENU_PATH != string.Empty);
                    if (menus.Count() > 0)
                    {
                        MainView.SetActiveView(vwWelcome);
                        BindControl.BindRepeater(rptMenu, menus);
                    }
                    else
                    {
                        MainView.SetActiveView(vwNotAllowed);
                        plcMenuAccess.Controls.Add(new LiteralControl("<div class='message error close'><h2>Error!</h2><p>You havent been granted access to the system, please contact the administrator </p></div>"));
                    }
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        public string ToUpper(object strVal)
        {
            return (strVal != DBNull.Value) ? strVal.ToString().ToUpper() : string.Empty;
        }

        public string GetUrl(object popuppage, object welcomeId)
        {
            return string.Format("{0}?welcomeId={1}", popuppage, welcomeId);
        }

        protected void ChkShowAgain_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (ChkShowAgain.Checked)
                {
                    bool status = ChkShowAgain.Checked ? false : true;
                    int employeeId = Util.user.UserId;

                    start.SetAllWelcomeStatus(employeeId, status);
                    UserInfo user = Session["UserInfo"] as UserInfo;
                    user.ShowAgain = status;
                    Session["UserInfo"] = user;
                    loadMenus();
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }
    }
}