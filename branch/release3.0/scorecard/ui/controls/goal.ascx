﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="goal.ascx.cs" Inherits="scorecard.ui.controls.goal" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:UpdatePanel ID="UpdatePanelGoal" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <h1>
            &nbsp; Goals
        </h1>
        <fieldset runat="server" id="pnlContactInfo">
            <legend>Setup Your Goals </legend>
            <p>
                <label for="sf">
                    Select Area:
                </label>
                &nbsp;<span class="field_desc"><asp:DropDownList runat="server" ID="ddlArea" AutoPostBack="True"
                    OnSelectedIndexChanged="ddlArea_SelectedIndexChanged" Width="130px" />
                </span>
            </p>
            <p>
                <label for="sf">
                    Select Value:
                </label>
                &nbsp;<span class="field_desc"><asp:DropDownList runat="server" ID="ddlValueTypes"
                    AutoPostBack="True" OnSelectedIndexChanged="ddlValueTypes_SelectedIndexChanged"
                    Width="130px" />
                </span>
            </p>
        </fieldset>
        <fieldset runat="server" id="Fieldset2">
            <legend>Context of Environment</legend>
            <p>
                &nbsp;<span class="field_desc">
                    <div style="float: left; width: 90%">
                        <asp:TextBox runat="server" ID="txtEnvContext" Width="95%" ValidationGroup="Environment"
                            TextMode="MultiLine" Height="20px" />
                    </div>
                    <div style="float: left; text-align: left">
                        <asp:Button ID="btnContextSave" runat="server" CssClass="button" Text="Save" CausesValidation="true"
                            ValidationGroup="Environment" OnClick="btnContextSave_Click" />
                        <asp:HiddenField ID="hdnContextId" runat="server" />
                    </div>
                    <div style="clear: both" />
                </span>
            </p>
        </fieldset>
        <asp:ListView ID="lvGoals" runat="server" OnItemCommand="lvGoal_ItemCommand" OnItemEditing="lvGoal_ItemEditing"
            OnPagePropertiesChanged="lvGoal_PagePropertiesChanged">
            <LayoutTemplate>
                <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                    <thead>
                        <tr id="Tr1" runat="server">
                            <td>
                                Goal
                            </td>
                            <td style="width: 170px">
                                Rational
                            </td>
                            <td>
                                Target
                            </td>
                            <td>
                                Unit Of Measurement
                            </td>
                            <td>
                                Weight
                            </td>
                            <td>
                                Strategic
                            </td>
                            <td>
                                Edit
                            </td>
                            <td>
                                Delete
                            </td>
                        </tr>
                        <tr id="ItemPlaceHolder" runat="server">
                        </tr>
                    </thead>
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tbody>
                    <tr class="odd">
                        <td>
                            <%# Eval("GOAL_DESCRIPTION")%><asp:Label ID="lblID" runat="server" Visible="false"
                                Text='<%# Eval("ID") %>' />
                        </td>
                        <td>
                            <%# Eval("RATIONAL")%>
                        </td>
                        <td>
                            <%# Eval("TARGET")%>
                        </td>
                        <td>
                            <%# getTypeName(int.Parse(Eval("UNIT_MEASURE").ToString()))%>
                        </td>
                        <td>
                            <%# Eval("WEIGHT")%>&nbsp;%
                        </td>
                        <td>
                            <%# getStrategicObjective(int.Parse(Eval("STRATEGIC_ID").ToString()))%>
                        </td>
                        <td>
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkEdit" CommandName="Edit" class="ui-state-default ui-corner-all"
                                    title="Edit Record"><span class="ui-icon ui-icon-pencil">
                        </span></asp:LinkButton>
                            </ul>
                        </td>
                        <td>
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkDelete" CommandName="deleteGoal" class="ui-state-default ui-corner-all"
                                    title="Delete Record" OnClientClick="return confirmDelete();"><span class="ui-icon ui-icon-trash">
                        </span></asp:LinkButton>
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </ItemTemplate>
            <EditItemTemplate>
                <tbody>
                    <tr class="odd">
                        <td>
                            <asp:Label ID="lblIDE" runat="server" Visible="false" Text='<%# Eval("ID") %>' />
                            <asp:TextBox runat="server" ID="txtEditDescription" Text='<%# Eval("GOAL_DESCRIPTION")%>' />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtEditRational" Text='<%# Eval("RATIONAL")%>' Width="170px" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtEditTarget" Text='<%# Eval("TARGET")%>' Width="40px" />
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlUnitOfMeasure" />
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlEditWeight" Width="70px" />
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlEditStrategic" Width="200px" />
                        </td>
                        <td>
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkSave" CommandName="updateGoals" class="ui-state-default ui-corner-all"
                                    title="Update Record"><span class="ui-icon ui-icon-disk">
                        </span></asp:LinkButton>
                            </ul>
                        </td>
                        <td>
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkCancelUpdate" CommandName="cancelUpdate" class="ui-state-default ui-corner-all"
                                    title="Cancel Update"><span class="ui-icon ui-icon-cancel">
                        </span></asp:LinkButton>
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </EditItemTemplate>
            <InsertItemTemplate>
                <tbody>
                    <tr>
                        <td>
                            <asp:TextBox runat="server" ID="txtDescription" Width="170px" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtRational" Width="170px" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtTarget" Width="40px" />
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlUnitOfMeasure" Width="160px" />
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlWeight" Width="70px" />
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlStrategic" Width="200px" />
                        </td>
                        <td>
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkSave" class="ui-state-default ui-corner-all"
                                    title="Save this Record" CommandName="addNewGoal"><span class="ui-icon ui-icon-disk">
                        </span></asp:LinkButton>
                            </ul>
                        </td>
                        <td>
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkCancel" class="ui-state-default ui-corner-all"
                                    title="Cancel Save " CommandName="cancelUpdate"><span class="ui-icon ui-icon-cancel">
                        </span></asp:LinkButton>
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </InsertItemTemplate>
        </asp:ListView>
        <br />
        <asp:DataPager ID="dtPagerlvGoals" runat="server" PagedControlID="lvGoals" PageSize="10"
            OnPreRender="dtPagerlvGoals_PreRender">
            <Fields>
                <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True" ShowNextPageButton="False"
                    ShowPreviousPageButton="False" />
                <asp:NumericPagerField />
                <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True" ShowNextPageButton="False"
                    ShowPreviousPageButton="False" />
            </Fields>
        </asp:DataPager>
        <div>
            <span style="color: Red">
                <asp:Literal ID="LblError" runat="server" /></span></div>
        <div>
            <p>
                <br />
                <br />
                <asp:Label ID="lblMsg" runat="server" Style="color: #FF3300" />
                <br />
                <br />
            </p>
        </div>
        <div id="tabs-3">
            <p>
                <asp:Button ID="btnNew" runat="server" CssClass="button" Text="Add New Goal" OnClick="btnNew_Click"
                    CausesValidation="false" />
                <asp:Button ID="lnkCancel0" runat="server" CssClass="button" Text="Done" CausesValidation="false"
                    PostBackUrl="~/ui/index.aspx" />
            </p>
        </div>
        <asp:UpdateProgress ID="UpdateProgressGoal" runat="server" AssociatedUpdatePanelID="UpdatePanelGoal">
            <ProgressTemplate>
                <div>
                    <asp:Image ID="imgOrg" runat="server" ImageUrl="~/ui/images/icons/activity.gif" AlternateText="Please wait..">
                    </asp:Image></div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
