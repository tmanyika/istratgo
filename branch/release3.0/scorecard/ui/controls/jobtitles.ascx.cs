﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.implementations;
using scorecard.controllers;
using scorecard.entities;

namespace scorecard.ui.controls
{
    public partial class jobtitles : System.Web.UI.UserControl
    {
        jobtitlesdefinitions impl;

        public jobtitles()
        {
            impl = new jobtitlesdefinitions(new jobtitleImpl());
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadJobTitlesByCompany();
            }
        }

        void loadJobTitlesByCompany()
        {
            try
            {
                var jobtitles = impl.getAllCompanyJobTitles(new company { COMPANY_ID = Util.user.CompanyId }).Where(a => a.ACTIVE == true).ToList();
                dtPagerJobTitles.Visible = false;
                if (jobtitles.Count <= 0)
                {
                    lvJobTitles.InsertItemPosition = InsertItemPosition.LastItem;
                }
                if (jobtitles.Count > dtPagerJobTitles.PageSize)
                {
                    dtPagerJobTitles.Visible = true;
                }
                lvJobTitles.DataSource = jobtitles;
                lvJobTitles.DataBind();
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            lvJobTitles.InsertItemPosition = InsertItemPosition.LastItem;
            loadJobTitlesByCompany();
        }

        void createNewJobTitle(ListViewCommandEventArgs e)
        {
            try
            {
                var txtName = e.Item.FindControl("txtName") as TextBox;
                var ckActive = e.Item.FindControl("ckActive") as CheckBox;
                impl.addJobTitle(new JobTitle
                {
                    COMPANY_ID = Util.user.CompanyId,
                    NAME = txtName.Text.Trim(),
                    ACTIVE = ckActive.Checked
                });
                lvJobTitles.InsertItemPosition = InsertItemPosition.None;
                loadJobTitlesByCompany();
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void updateJobtitle(ListViewCommandEventArgs e)
        {
            try
            {
                var txtName = e.Item.FindControl("txtName") as TextBox;
                var ckActive = e.Item.FindControl("ckActive") as CheckBox;
                var lblTitleID = e.Item.FindControl("lblID") as Label;
                impl.updateJobTitle(new JobTitle
                {
                    ID = int.Parse(lblTitleID.Text),
                    COMPANY_ID = Util.user.CompanyId, //TODO: remove company ID 
                    NAME = txtName.Text.Trim(),
                    ACTIVE = ckActive.Checked
                });
                lvJobTitles.InsertItemPosition = InsertItemPosition.None;
                lvJobTitles.EditIndex = -1;
                loadJobTitlesByCompany();
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void deleteJobtitle(ListViewCommandEventArgs e)
        {
            try
            {
                var lblTitleID = e.Item.FindControl("lblID") as Label;
                impl.deleteJobTitle(new JobTitle
                {
                    ID = int.Parse(lblTitleID.Text)
                });
                loadJobTitlesByCompany();
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void cancelUpdate()
        {

            lvJobTitles.EditIndex = -1;
            lvJobTitles.InsertItemPosition = InsertItemPosition.None;
            loadJobTitlesByCompany();
        }

        protected void lvJobTitles_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.InsertItem || e.Item.ItemType == ListViewItemType.DataItem)
            {

                switch (e.CommandName)
                {
                    case "addNewJobTitle":
                        createNewJobTitle(e);
                        break;
                    case "updateJobTitle":
                        updateJobtitle(e);
                        break;
                    case "deleteJobTitle":
                        deleteJobtitle(e);
                        break;
                    case "cancelUpdate":
                        cancelUpdate();
                        break;
                }
            }
        }

        protected void lvJobTitle_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            lvJobTitles.EditIndex = e.NewEditIndex;
            lvJobTitles.InsertItemPosition = InsertItemPosition.None;
            loadJobTitlesByCompany();
        }

        protected void lvJobTitles_PagePropertiesChanged(object sender, EventArgs e)
        {
            loadJobTitlesByCompany();
        }

        protected void lnkCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("index.aspx");
        }
    }
}