﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="customerBase.ascx.cs"
    Inherits="scorecard.ui.reports.customerReport" %>
<%@ Import Namespace="scorecard.implementations" %>
<div style="background-color: White;">
    <table cellpadding="0" cellspacing="10" border="0" width="90%">
        <tr>
            <td style="width: 250px;">
                <h2>
                    Customer Base</h2>
            </td>
            <td align="left">
                <span style="color: Red">
                    <asp:Literal ID="lblError" runat="server"></asp:Literal></span>
                <asp:LinkButton ID="lnkPrint" runat="server" CausesValidation="False" OnClientClick="window.print();">
                    <img runat="server" src="~/ui/assets/icons/print.png" id="imgPrint" alt="Click to Print" /></asp:LinkButton>&nbsp;&nbsp;<asp:LinkButton
                        ID="lnkExcel" runat="server" CausesValidation="False" OnClick="lnkExcel_Click">
                        <img src="~/ui/assets/icons/Excel.png" runat="server" id="imgExcel" alt="Export to Excel" /></asp:LinkButton>
            </td>
        </tr>
    </table>
</div>
<div style="background-color: White;">
    <asp:UpdatePanel ID="UpdatePanelBase" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:ListView ID="lvCustomer" runat="server">
                <LayoutTemplate>
                    <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                        <thead>
                            <tr>
                                <td align="left">
                                    Company Name
                                </td>
                                <td align="left">
                                    Industry
                                </td>
                                <td align="left">
                                    Registration No.
                                </td>
                                <td align="left">
                                    First Name
                                </td>
                                <td align="left">
                                    Surname
                                </td>
                                <td align="left">
                                    Email Address
                                </td>
                                <td align="left">
                                    Cellphone No.
                                </td>
                                <td align="left">
                                    Account No.
                                </td>
                                <td align="left">
                                    Date Registered
                                </td>
                                <td>
                                    Last Login Date
                                </td>
                            </tr>
                            <tr id="ItemPlaceHolder" runat="server">
                            </tr>
                        </thead>
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <tbody>
                        <tr class="odd">
                            <td align="left">
                                <%#  Eval("Company Name") %>
                            </td>
                            <td align="left">
                                <%#  Eval("Industry") %>
                            </td>
                            <td align="left">
                                <%#  Eval("Registration No") %>
                            </td>
                            <td align="left">
                                <%#  Eval("First Name") %>
                            </td>
                            <td align="left">
                                <%#  Eval("Surname") %>
                            </td>
                            <td align="left">
                                <%#  Eval("Email Address") %>
                            </td>
                            <td align="left">
                                <%#  Eval("Cellphone Number") %>
                            </td>
                            <td align="left">
                                <%#  Eval("Account No") %>
                            </td>
                            <td align="left">
                                <%# Common.GetDescriptiveDate(Eval("Date Registered"), false) %>
                            </td>
                            <td>
                            <%# Common.GetDescriptiveDate(Eval("Last Login Time"), false)%>
                            </td>
                        </tr>
                    </tbody>
                </ItemTemplate>
            </asp:ListView>
            <br />
            <div id="pager">
                <asp:DataPager ID="dtPagerUserAccounts" runat="server" PagedControlID="lvCustomer"
                    PageSize="20" OnPreRender="DataPagerCustomers_PreRender">
                    <Fields>
                        <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True" ShowNextPageButton="False"
                            ShowPreviousPageButton="False" />
                        <asp:NumericPagerField />
                        <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True" ShowNextPageButton="False"
                            ShowPreviousPageButton="False" />
                    </Fields>
                </asp:DataPager>
            </div>
            <asp:UpdateProgress ID="UpdateProgressBase" runat="server" AssociatedUpdatePanelID="UpdatePanelBase">
                <ProgressTemplate>
                    <img runat="server" src="~/ui/images/icons/activity.gif" alt="Please wait..." id="imgPrg" />
                </ProgressTemplate>
            </asp:UpdateProgress>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
