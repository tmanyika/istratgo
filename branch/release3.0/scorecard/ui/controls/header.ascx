﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="header.ascx.cs" Inherits="scorecard.ui.controls.header" %>
<%@ Register Src="mainmenu.ascx" TagName="mainmenu" TagPrefix="uc1" %>
<div id="header">
    <!-- Top -->
    <div id="top">
        <!-- Logo -->
        <div class="logo">
            <a href="#" title="Online Balance Score Card" class="tooltip">
                <img src="assets/logo.png" alt="Balance Score Card" /></a>
        </div>
        <!-- End of Logo -->
        <!-- Meta information -->
        <div class="meta">
            <p>
                <asp:Label ID="lblloggedInText" runat="server" />
            </p>
            <ul>
                <li>
                    <asp:LinkButton ID="lnkLogOut" runat="server" title="Go to the home page." class="tooltip"
                        OnClick="lnkLogOut_Click" CausesValidation="False"><span class="ui-icon ui-icon-power">
                    </span>Logout</asp:LinkButton>
                </li>
                <li>
                    <asp:LinkButton ID="lnkHome" runat="server" title="End administrator session" PostBackUrl="~/ui/index.aspx"
                        class="tooltip" CausesValidation="False"><span class="ui-icon ui-icon-person">
                    </span>Home</asp:LinkButton>
                </li>
            </ul>
        </div>
        <!-- End of Meta information -->
    </div>
    <!-- End of Top-->
    <!-- The navigation bar -->
    <div id="navbar">
        <uc1:mainmenu ID="mainmenu1" runat="server" />
    </div>
    <!-- End of navigation bar" -->
    <!-- Search bar -->
    <!-- End of Search bar -->
</div>
<!-- End of Header -->
