﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.implementations;
using System.Data;
using scorecard.interfaces;
using scorecard.controllers;

namespace scorecard.ui.controls
{
    public partial class registeredCompanies : System.Web.UI.UserControl
    {
        #region Page Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            BindControl.BindLiteral(lblMsg, string.Empty);
            if (!IsPostBack) LoadData();
        }

        #endregion

        #region Utility Methods

        private void LoadData()
        {
            try
            {
                companyregistration obj = new companyregistration(new companyImpl());
                DataTable dT = obj.getAllRegisteredCompaniesList();
                BindControl.BindListView(lstData, dT);
                if (dT.Rows.Count <= pager.PageSize) pager.Visible = false;
            }
            catch (Exception e)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(e);
            }
        }

        #endregion

        #region ListView Event Handling Methods

        protected void PagerCommand(object sender, DataPagerCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Next":
                    //guard against going off the end of the list
                    e.NewStartRowIndex = Math.Min(e.Item.Pager.StartRowIndex + e.Item.Pager.MaximumRows, e.Item.Pager.TotalRowCount - e.Item.Pager.MaximumRows);
                    e.NewMaximumRows = e.Item.Pager.MaximumRows;
                    break;
                case "Previous":
                    //guard against going off the begining of the list
                    e.NewStartRowIndex = Math.Max(0, e.Item.Pager.StartRowIndex - e.Item.Pager.MaximumRows);
                    e.NewMaximumRows = e.Item.Pager.MaximumRows;
                    break;
                case "Last":
                    //the
                    e.NewStartRowIndex = e.Item.Pager.TotalRowCount - e.Item.Pager.MaximumRows;
                    e.NewMaximumRows = e.Item.Pager.MaximumRows;
                    break;
                case "First":
                default:
                    e.NewStartRowIndex = 0;
                    e.NewMaximumRows = e.Item.Pager.MaximumRows;
                    break;
            }
        }

        protected void CurrentPageChanged(object sender, EventArgs e)
        {
            TextBox txtCurrentPage = sender as TextBox;
            DataPager pager = this.lstData.FindControl("pager") as DataPager;
            int startRowIndex = Math.Min((int.Parse(txtCurrentPage.Text) - 1) * pager.MaximumRows, pager.TotalRowCount - pager.MaximumRows);
            pager.SetPageProperties(startRowIndex, pager.MaximumRows, true);
        }

        protected void lstData_PagePropertiesChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        protected void ChkPaid_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                CheckBox chk = sender as CheckBox;
                companyregistration obj = new companyregistration(new companyImpl());

                int compId = int.Parse(chk.ValidationGroup);
                bool updated = obj.updateCompanyPaidStatus(compId, chk.Checked);
                if (updated) LoadData();
                BindControl.BindLiteral(lblMsg, updated ? Messages.GetUpdateMessage() : Messages.GetUpdateFailedMessage());
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        protected void lstData_ItemCommand(object sender, ListViewCommandEventArgs e)
        {

        }

        #endregion
    }
}