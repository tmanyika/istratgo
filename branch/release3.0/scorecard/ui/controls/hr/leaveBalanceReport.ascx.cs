﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RKLib.ExportData;
using System.Data;
using HR.Leave;
using scorecard.implementations;

namespace scorecard.ui.controls.hr
{
    public partial class leaveBalances : System.Web.UI.UserControl
    {
        #region Properties

        int OrgUnitId
        {
            get { return int.Parse(Request.QueryString["orgid"].ToString()); }
        }

        string LeaveId
        {
            get { return Request.QueryString["lid"].ToString(); }
        }

        string OrgUnitName
        {
            get { return Request.QueryString["orgname"].ToString(); }
        }

        #endregion

        #region Page Load Event Handling Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lblHeader.Text = string.Format("Leave Balances for <b>{0}</b> Business Unit", OrgUnitName);
                BindData();
            }
        }

        #endregion

        #region Utility Methods

        void BindData()
        {
            ILeaveReport obj = new LeaveReportBL();
            DataTable dT = obj.GetLeaveBalances(OrgUnitId, Common.GetXML(LeaveId.Split(','), "Data", "Leave", "Id"));
            Cache["Ds"] = dT;
            BindControl.BindListView(lstData, dT);
        }

        #endregion

        #region Button Event Handling

        protected void lnkExcel_Click(object sender, EventArgs e)
        {
            DownLoad();
        }

        void DownLoad()
        {
            DateTime today = DateTime.Now;
            DataTable dT = Cache["Ds"] as DataTable;

            if (dT.Rows.Count <= 0)
            {
                BindControl.BindLiteral(lblError, "There are no records to display.");
                return;
            }

            int[] cols = { 0, 6, 7, 1, 2, 3, 4, 5 };
            string[] headers = { "Full Name", "Email", "Employee No.", "Leave Type", "Accumulated Leave Days", "No. of Leave Days Taken", "Leave Balance", "Maximum Threshold" };
            string fName = string.Format("{0}_Bal_{1}_{2}_{3}", OrgUnitName.Replace(" ", ""), today.Year, today.Month.ToString("D2"), today.Day.ToString("D2"));

            Export exp = new Export("Web");
            exp.ExportDetails(dT, cols, headers, Export.ExportFormat.Excel, Export.ExportAction.DownloadOnly, string.Format("{0}.xls", fName));
        }

        #endregion
    }
}