﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="leaveCalendar.ascx.cs"
    Inherits="scorecard.ui.controls.hr.leaveCalendar" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Import Namespace=" scorecard.implementations" %>
<h1>
    Leave Calendar Management</h1>
<asp:UpdatePanel ID="UpdatePanelLeave" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div>
            <asp:ListView ID="lstData" runat="server" OnItemEditing="lstData_ItemEditing" OnItemCommand="lstData_ItemCommand"
                OnPagePropertiesChanging="lstData_PagePropertiesChanging">
                <LayoutTemplate>
                    <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                        <thead>
                            <tr>
                                <td>
                                    Calendar Year
                                </td>
                                <td>
                                    Holiday
                                </td>
                                <td>
                                    Date
                                </td>
                                <td>
                                    Applied
                                </td>
                                <td>
                                    Edit
                                </td>
                                <td>
                                    Delete
                                </td>
                            </tr>
                            <tr id="itemPlaceholder" runat="server">
                            </tr>
                        </thead>
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <tbody>
                        <tr class="odd">
                            <td>
                                <%# Eval("CalendarYear")%>
                                <asp:HiddenField ID="hdnId" runat="server" Value='<%# Eval("HolidayDateId") %>' />
                            </td>
                            <td>
                                <%# Eval("Detail")%>
                            </td>
                            <td>
                                <%# String.Format("{0:dd/MM/yyyy}", Eval("HolidayDate"))%>
                            </td>
                            <td>
                                <asp:CheckBox ID="ChkApplied" runat="server" Checked='<%# Visibility(Eval("HolidayDateId"))%>'
                                    Enabled='<%# ApplyVisibility(Eval("HolidayDateId")) %>' AutoPostBack="True" ValidationGroup='<%# Eval("HolidayDateId") %>'
                                    OnCheckedChanged="ChkApplied_CheckedChanged" ToolTip="Tick the check box to apply the holiday date to your org unit employees." />
                            </td>
                            <td>
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="lnkEdit" CommandName="Edit" class="ui-state-default ui-corner-all"
                                        title="Edit Record" CausesValidation="false" Visible='<%# ApplyVisibility(Eval("HolidayDateId")) %>'><span class="ui-icon ui-icon-pencil">
                                        </span>
                                    </asp:LinkButton>
                                </ul>
                            </td>
                            <td>
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="lnkDelete" CommandName="DeleteItem" class="ui-state-default ui-corner-all"
                                        title="Delete Record" OnClientClick="return confirmDelete();" CausesValidation="false"><span class="ui-icon ui-icon-trash">
                        </span>
                                    </asp:LinkButton>
                                </ul>
                            </td>
                        </tr>
                    </tbody>
                </ItemTemplate>
                <EditItemTemplate>
                    <tbody>
                        <tr class="odd">
                            <td>
                                <%# Eval("CalendarYear")%>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtDetail" Text='<%# Eval("Detail")%>' ValidationGroup="Update" />
                                <asp:RequiredFieldValidator ID="rqdName" runat="server" ErrorMessage="*" ValidationGroup="Update"
                                    ControlToValidate="txtDetail" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                <asp:HiddenField ID="hdnIdE" runat="server" Value='<%# Eval("HolidayDateId") %>' />
                            </td>
                            <td>
                                <div class="calendarContainer">
                                    <asp:TextBox runat="server" ID="txtDate" Text='<%# Common.GetDescriptiveDate(Eval("HolidayDate")) %>'
                                        ValidationGroup="AddLeave" /></div>
                                <asp:CalendarExtender ID="txtDate_CalendarExtender" runat="server" Enabled="True"
                                    CssClass="calendar" Format="d/MM/yyyy" TargetControlID="txtDate">
                                </asp:CalendarExtender>
                                <asp:RequiredFieldValidator ID="rqdDate" runat="server" ErrorMessage="*" ValidationGroup="Update"
                                    ControlToValidate="txtDate" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="lnkUpdate" CommandName="UpdateItem" class="ui-state-default ui-corner-all"
                                        title="Update Record" ValidationGroup="Update"><span class="ui-icon ui-icon-disk"></span></asp:LinkButton>
                                </ul>
                            </td>
                            <td>
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="lnkCancelUpdate" CommandName="CancelUpdate" class="ui-state-default ui-corner-all"
                                        title="Cancel Update" CausesValidation="false"><span class="ui-icon ui-icon-cancel"></span></asp:LinkButton>
                                </ul>
                            </td>
                        </tr>
                    </tbody>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <tbody>
                        <tr class="odd">
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtDetaili" ValidationGroup="Add" />
                                <asp:RequiredFieldValidator ID="rqdDetaili" runat="server" ErrorMessage="*" ValidationGroup="Add"
                                    ControlToValidate="txtDetaili" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <div class="calendarContainer">
                                    <asp:TextBox runat="server" ID="txtDatei" ValidationGroup="Add" /></div>
                                <asp:CalendarExtender ID="txtDate_CalendarExtender" runat="server" Enabled="True"
                                    CssClass="calendar" Format="d/MM/yyyy" TargetControlID="txtDatei">
                                </asp:CalendarExtender>
                                <asp:RequiredFieldValidator ID="rqdDatei" runat="server" ErrorMessage="*" ValidationGroup="Add"
                                    ControlToValidate="txtDatei" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="lnkSave" CommandName="AddItem" class="ui-state-default ui-corner-all"
                                        title="Update Record" ValidationGroup="Add"><span class="ui-icon ui-icon-disk"></span></asp:LinkButton>
                                </ul>
                            </td>
                            <td>
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="lnkCancelUpdate" CommandName="CancelUpdate" class="ui-state-default ui-corner-all"
                                        title="Cancel Update" CausesValidation="false"><span class="ui-icon ui-icon-cancel"></span></asp:LinkButton>
                                </ul>
                            </td>
                        </tr>
                    </tbody>
                </InsertItemTemplate>
                <EmptyDataTemplate>
                    <p class="errorMsg">
                        There are no records to display for the selected calendar year.
                    </p>
                </EmptyDataTemplate>
            </asp:ListView>
            <br />
            <asp:DataPager ID="pager" runat="server" PagedControlID="lstData" PageSize="15">
                <Fields>
                    <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True" ShowNextPageButton="False"
                        ShowPreviousPageButton="False" />
                    <asp:NumericPagerField />
                    <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True" ShowNextPageButton="False"
                        ShowPreviousPageButton="False" />
                </Fields>
            </asp:DataPager>
            <br />
            <br />
        </div>
        <div>
            <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td align="right">
                        Select Calendar Year
                    </td>
                    <td align="right" style="width: 100px;">
                        <asp:DropDownList ID="ddCalYr" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddCalYr_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
        </div>
        <div>
            <br />
            <p class="errorMsg">
                <asp:Literal ID="lblMsg" runat="server"></asp:Literal></p>
            <br />
        </div>
        <div id="tabs-3">
            <p>
                <asp:Button ID="btnNew" runat="server" class="button" Text="Add New Holiday" OnClick="lnkAdd_Click"
                    CausesValidation="False" />
                <asp:Button ID="lnkCancel0" runat="server" class="button" Text="Done" CausesValidation="false"
                    PostBackUrl="~/ui/index.aspx" />
            </p>
        </div>
        <asp:UpdateProgress ID="UpdateProgressLeave" runat="server" AssociatedUpdatePanelID="UpdatePanelLeave">
            <ProgressTemplate>
                <div>
                    <asp:Image ID="imgProj" runat="server" ImageUrl="~/ui/images/icons/activity.gif"
                        AlternateText="Please wait.."></asp:Image></div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
