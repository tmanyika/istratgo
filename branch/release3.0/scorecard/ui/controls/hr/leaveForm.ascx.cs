﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HR.Leave;
using scorecard.implementations;
using scorecard.controllers;
using scorecard.entities;
using System.Text;

namespace scorecard.ui.controls.hr
{
    public partial class leaveForm : System.Web.UI.UserControl
    {
        #region Page Event Handling Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            BindControl.BindLiteral(lblMsg, "");
            if (!IsPostBack)
            {
                BindOrgUnits();
                BindLeaveDropDown();
                ManageDateVisibility();
            }
        }

        #endregion

        #region Utility Methods

        bool DatesAvailable() { return (ddRepName.SelectedIndex == 5) ? true : false; }

        void ManageDateVisibility()
        {
            divDates.Visible = DatesAvailable();
        }

        void BindLeaveDropDown()
        {
            ILeaveType obj = new LeaveTypeBL();
            List<LeaveType> data = obj.GetByStatus(Util.user.CompanyId, true);
            BindControl.BindDropdown(ddlValueType, data);
        }

        void BindOrgUnits()
        {
            var userRole = Util.user.UserTypeId;
            var userID = Util.user.UserId;
            var goalDef = new goaldefinition(new goalsImpl());
            var structure = goalDef.orgstructure.getCompanyStructureListView(new company { COMPANY_ID = Util.user.CompanyId });
            if (userRole != Util.getTypeDefinitionsUserTypeAdministrator()) structure = structure.Where(t => t.OWNER_ID == userID).ToList();
            BindControl.BindDropdown(ddOrg, "ORGUNIT_NAME", "ID", "- select organisation unit -", "0", structure);
        }

        string GetSelectedLeaveTypes()
        {
            StringBuilder strB = new StringBuilder("");
            foreach (ListItem item in ddlValueType.Items)
                if (item.Selected) strB.Append(string.Format("{0},", item.Value));
            string strVals = strB.ToString();
            int lstIdx = strVals.LastIndexOf(',');
            return lstIdx > 0 ? strVals.Substring(0, lstIdx) : strVals;
        }

        bool DatesSupplied()
        {
            return (txtEndDate.Text != string.Empty && txtStartDate.Text != string.Empty) ? true : false;
        }

        void BuildClientScriptFunction(bool addEventToButton)
        {
            //validate leave type
            string lvId = GetSelectedLeaveTypes();
            if (lvId == string.Empty)
            {
                BindControl.BindLiteral(lblMsg, "Please select leave type(s).");
                return;
            }

            //validate org unit
            string orgId = ddOrg.SelectedValue;
            if (lvId == string.Empty)
            {
                BindControl.BindLiteral(lblMsg, "Please select organisation unit.");
                return;
            }

            //validate dates
            string sDate = string.Empty;
            string eDate = string.Empty;

            if (DatesAvailable())
            {
                if (!DatesSupplied())
                {
                    BindControl.BindLiteral(lblMsg, "Start Date and End Date are mandatory.");
                    return;
                }

                string tmpsDate = (txtStartDate.Text != string.Empty) ? Common.GetInternationalDateFormat(txtStartDate.Text, DateFormat.dayMonthYear) : string.Empty;
                string tmpeDate = (txtEndDate.Text != string.Empty) ? Common.GetInternationalDateFormat(txtEndDate.Text, DateFormat.dayMonthYear) : string.Empty;

                if (tmpsDate != string.Empty && tmpeDate != string.Empty)
                {
                    if (Common.FromDateIsGreater(DateTime.Parse(tmpsDate), DateTime.Parse(tmpeDate)))
                    {
                        BindControl.BindLiteral(lblMsg, "Start Date cannot be greater than End Date.");
                        return;
                    }

                    sDate = tmpsDate.Replace('/', '-');
                    eDate = tmpeDate.Replace('/', '-');
                }
            }

            string orgName = ddOrg.SelectedItem.Text;
            string pageUrl = ddRepName.SelectedValue;
            string url = string.Format("{0}?lid={1}&orgid={2}&sdate={3}&edate={4}&orgname={5}", pageUrl, lvId, orgId, sDate, eDate, orgName);
            string script = addEventToButton ? string.Format("return openNewWindow('{0}');", url) :
                                               string.Format("openNewWindow('{0}');", url);

            if (addEventToButton) btnViewReport.OnClientClick = script;
            else AjaxScriptManager.RegisterStartupScript(Page, typeof(Page), "mylivReport", script, true);
        }

        #endregion

        #region Dropdown Event Handling Methods

        protected void ddRepName_SelectedIndexChanged(object sender, EventArgs e)
        {
            ManageDateVisibility();
            //BuildClientScriptFunction(true);
        }

        //protected void ddOrg_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    BuildClientScriptFunction(true);
        //}

        #endregion

        #region TextBox Event Handling Methods

        //protected void txtStartDate_TextChanged(object sender, EventArgs e)
        //{
        //    if (!DatesSupplied())
        //        BuildClientScriptFunction();
        //}

        //protected void txtEndDate_TextChanged(object sender, EventArgs e)
        //{
        //    if (!DatesSupplied())
        //        BuildClientScriptFunction();
        //}

        #endregion

        #region Button Event Handling Methods

        protected void btnViewReport_Click(object sender, EventArgs e)
        {
            BuildClientScriptFunction(false);
        }

        #endregion
    }
}