﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="projects.ascx.cs" Inherits="scorecard.ui.controls.projects" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<h1>
    Project Management
</h1>
<asp:UpdatePanel ID="UpdatePanelProject" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:ListView ID="lstProject" runat="server" OnPagePropertiesChanged="lstProject_PagePropertiesChanged"
            OnItemEditing="lstProject_ItemEditing" OnItemCommand="lstProject_ItemCommand">
            <LayoutTemplate>
                <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                    <thead>
                        <tr>
                            <td>
                                Name
                            </td>
                            <td>
                                Description
                            </td>
                            <td>
                                Responsible Person
                            </td>
                            <td>
                                Active
                            </td>
                            <td>
                                Edit
                            </td>
                            <td>
                                Delete
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr id="itemPlaceholder" runat="server" />
                    </tbody>
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tr class="odd">
                    <td align="left">
                        <asp:Literal ID="lblName" runat="server" Text='<%# Eval("project_name")%>'></asp:Literal>
                        <asp:HiddenField ID="hdnprojId" runat="server" Value='<%# Eval("project_id") %>' />
                    </td>
                    <td align="left">
                        <%# Eval("project_desc")%>
                    </td>
                    <td>
                        <%# Eval("RESPONSIBLE_NAME")%>
                    </td>
                    <td align="left">
                        <asp:Literal ID="lblActive" runat="server" Text='<%# GetBoolean(Eval("active")) %>' />
                    </td>
                    <td>
                        <ul id="icons">
                            <asp:LinkButton runat="server" ID="lnkEdit" CommandName="Edit" class="ui-state-default ui-corner-all"
                                title="Edit Record" CausesValidation="false"><span class="ui-icon ui-icon-pencil">
                            
                            </span></asp:LinkButton></ul>
                    </td>
                    <td>
                        <ul id="icons">
                            <asp:LinkButton runat="server" ID="lnkDelete" CommandName="deleteItem" class="ui-state-default ui-corner-all"
                                title="Delete Record" OnClientClick="return confirmDelete();" CausesValidation="false"><span class="ui-icon ui-icon-trash">
                        </span></asp:LinkButton>
                        </ul>
                    </td>
                </tr>
            </ItemTemplate>
            <EditItemTemplate>
                <tbody>
                    <tr class="odd">
                        <td>
                            <asp:TextBox runat="server" ID="txtNameE" Text='<%# Eval("project_name")%>' ValidationGroup="Edit" />
                             <asp:RequiredFieldValidator ID="RqdInfoE" runat="server" ErrorMessage="* - required"
                                    ValidationGroup="Edit" ControlToValidate="txtNameE" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:HiddenField ID="hdnprojId" runat="server" Value='<%# Eval("project_id") %>' />
                            <asp:HiddenField ID="hdnUID" runat="server" Value='<%# Eval("RESPONSIBLE_PERSON_ID") %>' />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtDescE" Text='<%# Eval("project_desc")%>' />
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddPerson" />
                        </td>
                        <td>
                            <asp:CheckBox ID="chkActiveE" runat="server" Checked='<%# Eval("active") %>' />
                        </td>
                        <td>
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkUpdate" CommandName="updateProject" ValidationGroup="Edit" class="ui-state-default ui-corner-all"
                                    title="Update Record"><span class="ui-icon ui-icon-disk"></span></asp:LinkButton>
                            </ul>
                        </td>
                        <td>
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkCancelUpdate" CommandName="cancelUpdate" class="ui-state-default ui-corner-all"
                                    title="Cancel Update"><span class="ui-icon ui-icon-cancel"></span></asp:LinkButton>
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </EditItemTemplate>
            <InsertItemTemplate>
                <tbody>
                    <tr class="odd">
                        <td>
                            <asp:TextBox runat="server" ID="txtNameI" ValidationGroup="Insert" />
                             <asp:RequiredFieldValidator ID="RqdInfoI" runat="server" ErrorMessage="* - required"
                                    ValidationGroup="Insert" ControlToValidate="txtNameI" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtDescI" />
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddPersonI" />
                        </td>
                        <td>
                            <asp:CheckBox ID="chkActiveI" runat="server" Checked="true" />
                        </td>
                        <td>
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkSave" CommandName="addProject" ValidationGroup="Insert" class="ui-state-default ui-corner-all"
                                    title="Add New Project"><span class="ui-icon ui-icon-disk"></span></asp:LinkButton>
                            </ul>
                        </td>
                        <td>
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkCancelUpdate" CommandName="cancelUpdate" class="ui-state-default ui-corner-all"
                                    title="Cancel Update"><span class="ui-icon ui-icon-cancel"></span></asp:LinkButton>
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </InsertItemTemplate>
            <EmptyDataTemplate>
                <p class="errorMsg">
                    There are no records to display.
                </p>
            </EmptyDataTemplate>
        </asp:ListView>
        <br />
        <asp:DataPager ID="pager" runat="server" PagedControlID="lstProject">
            <Fields>
                <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True" ShowNextPageButton="False"
                    ShowPreviousPageButton="False" />
                <asp:NumericPagerField />
                <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True" ShowNextPageButton="False"
                    ShowPreviousPageButton="False" />
            </Fields>
        </asp:DataPager>
        <br />
        <br />
        <p class="errorMsg">
            <asp:Literal ID="lblMsg" runat="server"></asp:Literal></p>
        <div id="tabs-3">
            <p>
                <asp:Button ID="btnNew" runat="server" class="button" Text="Add New Project" OnClick="lnkAdd_Click"
                    CausesValidation="False" />
                &nbsp;<asp:Button ID="lnkCancel0" runat="server" class="button" Text="Cancel" OnClick="lnkCancel_Click"
                    CausesValidation="false" />
            </p>
        </div>
        <asp:UpdateProgress ID="UpdateProgressProj" runat="server" AssociatedUpdatePanelID="UpdatePanelProject">
            <ProgressTemplate>
                <div>
                    <asp:Image ID="imgProj" runat="server" ImageUrl="~/ui/images/icons/activity.gif"
                        AlternateText="Please wait.."></asp:Image></div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
