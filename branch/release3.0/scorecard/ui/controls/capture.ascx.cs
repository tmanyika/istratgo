﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.controllers;
using scorecard.entities;
using scorecard.interfaces;
using scorecard.implementations;
using HR.Human.Resources;
using HierarchicalData;
using System.Data;
using System.Collections;
using System.Text;
using Scorecard.Business.Rules;
using vb = Microsoft.VisualBasic;
using System.Net.Mail;
using System.Email.Communication;

namespace scorecard.ui.controls
{
    public partial class capture : System.Web.UI.UserControl
    {
        goaldefinition goalDef;
        useradmincontroller usersDef;
        scoresmanager scoresManager;
        structurecontroller orgunits;
        IBusinessRule bR;
        IScoreRating iR;

        public capture()
        {
            orgunits = new structurecontroller(new structureImpl());
            goalDef = new goaldefinition(new goalsImpl());
            usersDef = new useradmincontroller(new useradminImpl());
            scoresManager = new scoresmanager(new scoresImpl());
            bR = new BusinessRuleBL();
            iR = new ScoreRatingBL();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                getGoalsCriteriaTypes();
                setCriteriaSelected();
                loadGoals();
            }
        }

        public string getTypeName(int id)
        {
            return goalDef.appsettings.getApplicationSetting(new entities.applicationsettings { TYPE_ID = id }).NAME;
        }

        int getUserAccountInfo(int ID)
        {
            try
            {
                if (ID == Util.getTypeDefinitionsRolesTypeID())
                {
                    IEmployee obj = new EmployeeBL();
                    var user = obj.GetById(int.Parse(ddlValueTypes.SelectedValue));
                    return user.JobTitleId == null ? 0 : (int)user.JobTitleId;
                }
                else return int.Parse(ddlValueTypes.SelectedValue);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return 0;
            }
        }

        void loadGoals()
        {
            try
            {
                if (ddlValueTypes.SelectedIndex == 0)
                {
                    pnlNoData.Visible = true;
                    BindControl.BindListView(lvGoals, new List<goals>());
                    return;
                }

                int areaTypeId = int.Parse(ddlArea.SelectedValue);
                int areaValueId = int.Parse(ddlValueTypes.SelectedValue);
                if (areaTypeId == Util.getTypeDefinitionsRolesTypeID())
                {
                    IEmployee obj = new EmployeeBL();
                    areaValueId = (int)obj.GetById(areaValueId).JobTitleId;
                }

                var objectives = goalDef.listGeneralGoals(new
                company
                {
                    COMPANY_ID = Util.user.CompanyId
                }).Where(t => t.AREA_TYPE_ID == areaTypeId && t.AREA_ID == areaValueId).ToList();

                pnlNoData.Visible = (objectives.Count() <= 0) ? true : false;
                BindControl.BindListView(lvGoals, objectives);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void getGoalsCriteriaTypes()
        {
            var areaList = goalDef.appsettings.getChildSettings(new applicationsettings { PARENT_ID = Util.getTypeDefinitionsGoalAreaTypes() }).ToList();
            BindControl.BindDropdown(ddlArea, "DESCRIPTION", "TYPE_ID", areaList);
        }

        void loadSetupTypes(DropDownList ddlPerspective)
        {
            try
            {
                var setuptypes = goalDef.appsettings.getChildSettings(new entities.applicationsettings { PARENT_ID = Util.getTypeDefinitionsPerspectivesParentID() });
                BindControl.BindDropdown(ddlValueTypes, "NAME", "TYPE_ID", setuptypes);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void loadJobTitles()
        {
            try
            {
                IEmployee obj = new EmployeeBL();
                List<Employee> titles = obj.GetByOrgStructure(Util.user.OrgId, true);

                if (Util.user.UserTypeId != Util.getTypeDefinitionsUserTypeAdministrator())
                    titles = titles.Where(t => t.LineManagerEmployeeId == Util.user.UserId || t.EmployeeId == Util.user.UserId).ToList();
                BindControl.BindDropdown(ddlValueTypes, "FullName", "EmployeeId", "- select user -", "-1", titles);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void loadStrategicGoals()
        {
            try
            {
                var strategic = goalDef.listStrategicGoals(new company { COMPANY_ID = Util.user.CompanyId });
                BindControl.BindDropdown(ddlValueTypes, "OBJECTIVE", "ID", "- select project -", "-1", strategic);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        public string getCurrentStore(int ID)
        {
            try
            {
                //  var score = scoresManager.getScoresByGoalID(new entities.goals { ID = ID , PERSPECTIVE = int.Parse(ddlValueTypes.SelectedValue)});
                //  return score.Single().SCORES.ToString();
                return "";
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return "";
            }
        }

        public string getFinalScore(int ID)
        {
            try
            {
                var score = scoresManager.getScoresByGoalID(new entities.goals { ID = ID, PERSPECTIVE = int.Parse(ddlValueTypes.SelectedValue) });
                //return (score != null) ? score[0].FINAL_SCORE.ToString() : "0";
                return (score != null) ? score.Single().FINAL_SCORE.ToString() : "0";
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return "0";
            }
        }


        void loadAreas()
        {
            try
            {
                var userRole = Util.user.UserTypeId;
                var userID = Util.user.UserId;
                var dV = orgunits.getActiveCompanyOrgStructure(Util.user.CompanyId);

                if (userRole != Util.getTypeDefinitionsUserTypeAdministrator())
                    dV.RowFilter = string.Format("OWNER_ID = {0}", userID);
                BindControl.BindDropdown(ddlValueTypes, "ORGUNIT_NAME", "ID", "- select org unit -", "-1", dV);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void loadProjects(int companyId)
        {
            projectmanagement proj = new projectmanagement(new projectImpl());
            int adminRoleId = Util.getTypeDefinitionsUserTypeAdministrator();

            ddlValueTypes.DataTextField = "PROJECT_NAME";
            ddlValueTypes.DataValueField = "PROJECT_ID";
            int roleId = (int)Util.user.UserTypeId;
            IEnumerable<project> projL = proj.get(companyId, true);
            if (adminRoleId != Util.user.UserTypeId) projL = projL.Where(c => c.RESPONSIBLE_PERSON_ID == Util.user.UserId);
            BindControl.BindDropdown(ddlValueTypes, "PROJECT_NAME", "PROJECT_ID", "- select project -", "-1", projL);
        }

        void setCriteriaSelected()
        {
            try
            {
                int areaId = int.Parse(ddlArea.SelectedValue);
                if (areaId == Util.getTypeDefinitionsRolesTypeID()) loadJobTitles();
                else if (areaId == Util.getTypeDefinitionsProjectID()) loadProjects(Util.user.CompanyId);
                else loadAreas();
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlValueTypes.Items.Clear();
            setCriteriaSelected();
            loadGoals();
        }

        protected void ddlValueTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadGoals();
        }

        protected void ddlPerspective_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadGoals();
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            calculateValues();
        }

        private entities.useradmin getApproverID(int id, int criteria)
        {
            try
            {
                if (criteria == Util.getTypeDefinitionsRolesTypeID())
                {
                    var approver_id = usersDef.getUserAccountInformation(new entities.useradmin { ID = (int)Util.user.ReportToUserId });
                    return approver_id;
                }
                else if (criteria == Util.getTypeDefinitionsProjectID())
                {
                    var proj = new projectmanagement(new projectImpl()).get(id);
                    var approver_id = new scorecard.entities.useradmin
                    {
                        ID = proj.RESPONSIBLE_PERSON_ID == null ? (int)Util.user.ReportToUserId : (int)proj.RESPONSIBLE_PERSON_ID,
                        NAME = proj.RESPONSIBLE_NAME,
                        EMAIL_ADDRESS = proj.RESPONSIBLE_EMAIL
                    };
                    return approver_id;
                }
                else
                {
                    var userId = goalDef.orgstructure.getOrgunitDetails(new Orgunit { ID = id }).OWNER_ID;
                    var approver_id = usersDef.getUserAccountInformation(new entities.useradmin { ID = userId });
                    return approver_id;
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return new entities.useradmin();
            }
        }


        void clearLabels()
        {
            lblDateUsed.Text = "";
            lblMsg.Text = "";
        }

        bool dateAlreadyCaptured(string periodDate, int criteriaId, int itemValId)
        {
            List<scores> scoreLst = scoresManager.getUsedItemDates(criteriaId, itemValId);
            string tmpDate = periodDate.Replace('/', '-');
            foreach (scores obj in scoreLst)
                if (string.Compare(obj.PERIOD_DATE_VAL, tmpDate) == 0) return true;
            return false;
        }

        bool informationValid()
        {
            int suppliedItems = 0;
            foreach (ListViewItem item in lvGoals.Items)
            {
                HiddenField hdnUnitMeasureId = item.FindControl("hdnUnitMeasureId") as HiddenField;
                Literal lblError = item.FindControl("lblError") as Literal;
                TextBox txtScore = item.FindControl("txtScore") as TextBox;

                lblError.Text = "";
                int unitOfMeasure = int.Parse(hdnUnitMeasureId.Value);
                string val = txtScore.Text;

                if (!string.IsNullOrEmpty(val))
                {
                    if (bR.UnitOfMeasureRequireNumericValue(unitOfMeasure))
                    {
                        bool valNumeric = vb.Information.IsNumeric(val);
                        if (valNumeric) suppliedItems += 1;
                        else lblError.Text = Messages.NumericValueRequired;
                    }
                    else if (bR.UnitOfMeasureRequireYesNoValue(unitOfMeasure))
                    {
                        bool yesNoValid = bR.UnitOfMeasureRequireYesNoValueValid(val);
                        if (yesNoValid) suppliedItems += 1;
                        else lblError.Text = Messages.YesNoValueRequired;
                    }
                }
                else lblError.Text = "* required.";
            }
            return (suppliedItems == lvGoals.Items.Count) ? true : false;
        }

        void calculateValues()
        {
            try
            {
                var selectedItem = int.Parse(ddlValueTypes.SelectedValue);
                var selectedArea = int.Parse(ddlArea.SelectedValue);
                var periodDate = Common.GetInternationalDateFormat(txtDate.Text, DateFormat.dayMonthYear);

                clearLabels();

                bool valid = informationValid();
                bool dateCaptured = dateAlreadyCaptured(periodDate, selectedArea, selectedItem);

                if (dateCaptured || !valid)
                {
                    lblDateUsed.Text = dateCaptured ? "You cannot capture information twice for the same date." : "Correct errors below.";
                    return;
                }

                var approver = new useradmincontroller(new useradminImpl()).getLineManagerUserAccount(selectedArea, selectedItem);
                var newRequest = new submissionWorkflow
                {
                    CRITERIA_TYPE_ID = int.Parse(ddlArea.SelectedValue),
                    SELECTED_ITEM_VALUE = int.Parse(ddlValueTypes.SelectedValue),
                    STATUS = Util.getTypeDefinitionsWorkFlowNewRequestId(),
                    APPROVER_ID = approver.ID
                };

                var requestID = scoresManager.createWorkFlow(newRequest);
                if (requestID > 0)
                {
                    decimal ratingVal = iR.GetCompanyRating(Util.user.CompanyId, true);
                    for (int i = 0; i < lvGoals.Items.Count; i++)
                    {
                        var lblTotal = lvGoals.Items[i].FindControl("lblTotal") as Label;
                        var lblCalc = lvGoals.Items[i].FindControl("lblUnitMeasureID") as Label;
                        var lblWeight = lvGoals.Items[i].FindControl("lblWeight") as Label;
                        var lblTarget = lvGoals.Items[i].FindControl("lblTarget") as Label;
                        var txtScore = lvGoals.Items[i].FindControl("txtScore") as TextBox;
                        var lblID = lvGoals.Items[i].FindControl("lblID") as Label;
                        var txtComment = lvGoals.Items[i].FindControl("txtComment") as TextBox;

                        if (lvGoals.Items[i].ItemType == ListViewItemType.DataItem)
                        {
                            var comment = txtComment.Text;
                            var unitMeasure = int.Parse(lblCalc.Text);
                            var weight = decimal.Parse(lblWeight.Text);
                            var item_id = int.Parse(lblID.Text);

                            decimal answer = 0;

                            if (bR.UnitOfMeasureRequireNumericValue(unitMeasure))
                            {
                                var target = decimal.Parse(lblTarget.Text);
                                var score = decimal.Parse(txtScore.Text);
                                answer = bR.FinalScore(unitMeasure, weight, score, target);
                                lblTotal.Text = answer.ToString();
                            }
                            else
                            {
                                if (txtScore.Text.Trim().ToLower() != "yes")
                                    lblTotal.Text = "0";
                                else
                                {
                                    lblTotal.Text = weight.ToString();
                                    answer = weight;
                                }
                            }

                            decimal myscore = decimal.Parse(answer.ToString());
                            decimal finalAgreedScore = bR.GetDefinedWeightedScore(myscore, ratingVal);
                            scores _scoreDef = new scores
                            {
                                JUSTIFICATION_COMMENT = comment,
                                CRITERIA_TYPE_ID = int.Parse(ddlArea.SelectedValue),
                                SELECTED_ITEM_VALUE = int.Parse(ddlValueTypes.SelectedValue),
                                SCORES = txtScore.Text.Trim(),
                                GOAL_ID = int.Parse(lblID.Text),
                                FINAL_SCORE = double.Parse(answer.ToString()),
                                SUBMISSION_ID = requestID,
                                PERIOD_DATE = DateTime.Parse(periodDate),
                                RATINGVALUE = ratingVal,
                                FINAL_SCORE_AGREED = finalAgreedScore
                            };
                            scoresManager.createNewScore(_scoreDef);
                        }
                    }
                    createAttachements(requestID);
                    pnlSubmitted.Visible = true;
                    pnlScores.Visible = false;
                    loadGoals();
                    sendMail(approver);
                }
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }


        void createAttachements(int submissionId)
        {
            try
            {
                for (int i = 0; i < 2; i++)
                {
                    var attachmentsUpload = this.FindControl("ftpAttachDocuments_" + i) as FileUpload;
                    if (attachmentsUpload != null && attachmentsUpload.PostedFile.ContentLength > 0)
                    {
                        attachmentDocuments docs = new attachmentDocuments
                        {
                            SUBMISSION_ID = submissionId,
                            ATTACHMENT_NAME = attachmentsUpload.PostedFile.FileName
                        };
                        attachmentsUpload.SaveAs(Server.MapPath(Util.getConfigurationSettingUploadFolder()) + attachmentsUpload.PostedFile.FileName);
                        scoresManager.createAttachment(docs);
                    }
                }
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void sendMail(entities.useradmin user)
        {
            try
            {
                string result = string.Empty;
                int emailId = Util.getWorkflowApprovalRequestEmailId();

                MailAddressCollection addy = new MailAddressCollection();
                addy.Add(new MailAddress(user.EMAIL_ADDRESS, user.NAME));

                IMailRelay objm = new MailRelayBL();
                MailRelay obj = new MailRelay
                {
                    MailId = emailId,
                    Recipient = addy,
                    Data = { }
                };
                objm.SendMail(obj, out result);
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        protected void lnkSubmitNew_Click(object sender, EventArgs e)
        {
            pnlSubmitted.Visible = false;
            pnlScores.Visible = true;
        }

    }
}