﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.implementations;
using System.Email.Communication;

namespace scorecard.ui.controls
{
    public partial class mailTemplates : System.Web.UI.UserControl
    {
        #region Properties

        int CompId
        {
            get { return Util.user.CompanyId; }
        }

        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) InitialisePage();
        }

        #endregion

        #region Utility Methods

        void InitialisePage()
        {
            LoadMailTemplates();
        }

        void ClearTextBoxes()
        {
            rdContent.Content = "";
            txtName.Text = "";
            txtSubject.Text = "";
            chkActive.Checked = true;
            chkMaster.Checked = false;
        }

        void BindMailTemplates()
        {
            IMailTemplate obj = new MailTemplateBL();
            List<MailTemplate> data = obj.GetMasterTemplates(true, true);
            BindControl.BindDropdown(ddMail, "MailName", "MailId", "-- select template --", "0", data);
        }

        void LoadMailTemplates()
        {
            mainView.SetActiveView(vwData);
            IMailTemplate obj = new MailTemplateBL();
            BindControl.BindListView(lstData, obj.GetByStatus(true));
            pager.Visible = (lstData.Items.Count <= pager.PageSize) ? false : true;
        }

        void ShowMessage(Literal lbl, string msg)
        {
            lbl.Text = msg;
        }

        #endregion

        #region Button Event Handling

        protected void lnkAdd_Click(object sender, EventArgs e)
        {
            lnkSaveMail.Visible = true;
            lnkUpdateMail.Visible = false;
            BindControl.BindLiteral(lblMsg, "");
            ClearTextBoxes();
            BindMailTemplates();
            mainView.SetActiveView(vwForm);
        }

        protected void lnkCancel_Click(object sender, EventArgs e)
        {
            mainView.SetActiveView(vwData);
        }

        #endregion

        #region Utility

        public string GetBoolean(object boolVal)
        {
            if (boolVal == DBNull.Value) return "Yes";
            return bool.Parse(boolVal.ToString()) ? "Yes" : "No";
        }

        private void CancelUpdate()
        {
            lstData.EditIndex = -1;
            lstData.InsertItemPosition = InsertItemPosition.None;
            LoadMailTemplates();
        }

        void DeleteItem(ListViewCommandEventArgs e)
        {
            int leaveId = int.Parse((e.Item.FindControl("hdnMailId") as HiddenField).Value);
            string updatedBy = Util.user.LoginId;

            IMailTemplate obj = new MailTemplateBL();
            bool deleted = obj.Delete(leaveId, updatedBy);
            if (deleted) LoadMailTemplates();
            string msg = deleted ? Messages.GetDeleteMessage() : Messages.GetDeleteFailedMessage();
            BindControl.BindLiteral(lblMsg, msg);
        }

        void EditItem(int mailId)
        {
            try
            {
                hdnMailIdE.Value = mailId.ToString();

                IMailTemplate objl = new MailTemplateBL();
                MailTemplate obj = objl.GetById(mailId);
                if (obj.MailName == null) BindControl.BindLiteral(lblMsg, Messages.GetRecordNotFoundMessage());
                else
                {
                    ClearTextBoxes();
                    BindMailTemplates();
                    mainView.SetActiveView(vwForm);

                    rdContent.Content = obj.MailContent;
                    txtName.Text = obj.MailName;
                    txtSubject.Text = obj.MailSubject;
                    chkActive.Checked = obj.Active;
                    chkMaster.Checked = obj.IsMasterTemplate;

                    if (ddMail.Items.FindByValue(obj.TemplateId.ToString()) != null)
                        ddMail.SelectedValue = obj.TemplateId.ToString();

                    lnkSaveMail.Visible = false;
                    lnkUpdateMail.Visible = true;
                }
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        void AddItem(int mailId)
        {
            try
            {
                bool active = chkActive.Checked;
                bool isMaster = chkMaster.Checked;

                string mailName = txtName.Text;
                string mailContent = rdContent.Content;
                string mailSubject = txtSubject.Text;
                string userName = Util.user.LoginId;

                int? templateId = null;
                if (ddMail.SelectedIndex != 0)
                    templateId = int.Parse(ddMail.SelectedValue);

                if (mailContent == string.Empty)
                {
                    ShowMessage(lblError, "Please supply mail content.");
                    return;
                }

                MailTemplate obj = new MailTemplate
                {
                    TemplateId = templateId,
                    MailId = mailId,
                    MailName = mailName,
                    MailSubject = mailSubject,
                    MailContent = mailContent,
                    CreatedBy = userName,
                    UpdatedBy = userName,
                    Active = active,
                    IsMasterTemplate = isMaster
                };

                IMailTemplate objl = new MailTemplateBL();
                bool saved = (mailId > 0) ? objl.UpdateMailTemplate(obj) : objl.AddMailTemplate(obj);
                string msg = saved ? Messages.GetUpdateMessage() : Messages.GetUpdateFailedMessage();
                BindControl.BindLiteral(lblMsg, msg);
                if (saved)
                {
                    LoadMailTemplates();
                    BindControl.BindLiteral(lblMsg, mailId > 0 ? msg : Messages.GetSaveMessage());
                }
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        #endregion

        #region ListView Event Handling Methods including Slide Paging

        protected void lstData_PagePropertiesChanged(object sender, EventArgs e)
        {
            LoadMailTemplates();
        }

        protected void lstData_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            try
            {
                HiddenField hdnMailId = lstData.Items[e.NewEditIndex].FindControl("hdnMailId") as HiddenField;
                if (hdnMailId != null && hdnMailId.Value != "")
                {
                    int mailId = int.Parse(hdnMailId.Value);
                    EditItem(mailId);
                }
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        protected void lstData_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                BindControl.BindLiteral(lblMsg, "");
                if (e.Item.ItemType == ListViewItemType.InsertItem ||
               e.Item.ItemType == ListViewItemType.DataItem)
                {
                    switch (e.CommandName)
                    {
                        case "DeleteItem":
                            DeleteItem(e);
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        #endregion

        #region Buton Event Handling

        protected void lnkSaveMail_Click(object sender, EventArgs e)
        {
            AddItem(0);
        }

        protected void lnkUpdateMail_Click(object sender, EventArgs e)
        {
            AddItem(int.Parse(hdnMailIdE.Value));
        }

        #endregion
    }
}