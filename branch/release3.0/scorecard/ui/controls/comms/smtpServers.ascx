﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="smtpServers.ascx.cs"
    Inherits="scorecard.ui.controls.comms.smtpServers" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<h1>
    Mail Server Management</h1>
<asp:UpdatePanel ID="UpdatePanelData" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div>
            <asp:ListView ID="lstData" runat="server" OnPagePropertiesChanged="lstData_PagePropertiesChanged"
                OnItemEditing="lstData_ItemEditing" OnItemCommand="lstData_ItemCommand">
                <LayoutTemplate>
                    <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                        <thead>
                            <tr>
                                <td>
                                    Account Name
                                </td>
                                <td>
                                    Outgoing SMTP Server
                                </td>
                                <td>
                                    MSSQL Profile Name
                                </td>
                                <td>
                                    Port
                                </td>
                                <td>
                                    UserName
                                </td>
                                <td>
                                    Password
                                </td>
                                <td>
                                    Active
                                </td>
                                <td>
                                    Edit
                                </td>
                                <td>
                                    Delete
                                </td>
                            </tr>
                            <tr id="itemPlaceholder" runat="server">
                            </tr>
                        </thead>
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <tbody>
                        <tr class="odd">
                            <td>
                                <asp:HiddenField ID="hdnAccountId" runat="server" Value='<%# Eval("AccountMailId") %>' />
                                <asp:Literal ID="lblName" runat="server" Text='<%# Eval("AccountName")%>'></asp:Literal>
                            </td>
                            <td>
                                <%# Eval("SmtpServer") %>
                            </td>
                            <td>
                                <%# Eval("SqlProfileName")%>
                            </td>
                            <td>
                                <%# Eval("Port") %>
                            </td>
                            <td>
                                <%# Eval("UserName") %>
                            </td>
                            <td>
                                <%# Eval("PIN") %>
                            </td>
                            <td>
                                <asp:Literal ID="lblActive" runat="server" Text='<%# GetBoolean(Eval("Active")) %>' />
                            </td>
                            <td>
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="lnkEdit" CommandName="Edit" class="ui-state-default ui-corner-all"
                                        title="Edit Record" CausesValidation="false"><span class="ui-icon ui-icon-pencil">
                                        </span>
                                    </asp:LinkButton>
                                </ul>
                            </td>
                            <td>
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="lnkDelete" CommandName="DeleteItem" class="ui-state-default ui-corner-all"
                                        title="Delete Record" OnClientClick="return confirmDelete();" CausesValidation="false"><span class="ui-icon ui-icon-trash">
                        </span></asp:LinkButton></ul>
                            </td>
                        </tr>
                    </tbody>
                </ItemTemplate>
                <EditItemTemplate>
                    <tbody>
                        <tr class="odd">
                            <td>
                                <asp:TextBox runat="server" ID="txtAccountName" Text='<%# Eval("AccountName")%>'
                                    ValidationGroup="Update" />
                                <asp:RequiredFieldValidator ID="rqdName" runat="server" ErrorMessage="*" ValidationGroup="Update"
                                    ControlToValidate="txtAccountName" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                <asp:HiddenField ID="hdnAccountIdE" runat="server" Value='<%# Eval("AccountMailId") %>' />
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtSmtpServer" Text='<%# Eval("SmtpServer")%>' />
                                <asp:RequiredFieldValidator ID="rqdServer" runat="server" ErrorMessage="*" ValidationGroup="Update"
                                    ControlToValidate="txtSmtpServer" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtSqlProfile" Text='<%# Eval("SqlProfileName")%>' />
                                <asp:RequiredFieldValidator ID="RqdProfile" runat="server" ErrorMessage="*" ValidationGroup="Update"
                                    ControlToValidate="txtSqlProfile" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtPort" Text='<%# Eval("Port")%>' ValidationGroup="Update"
                                    MaxLength="4" Width="20px" />
                                <asp:RequiredFieldValidator ID="RqdNum" runat="server" ErrorMessage="*" ValidationGroup="Update"
                                    ControlToValidate="txtPort" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="CvdNum" runat="server" ControlToValidate="txtPort" Display="Dynamic"
                                    ErrorMessage="numeric value required" Operator="DataTypeCheck" Type="Integer"
                                    ValidationGroup="Update" ForeColor="Red"></asp:CompareValidator>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtUserName" Text='<%# Eval("UserName")%>' />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                    ValidationGroup="Update" ControlToValidate="txtSmtpServer" Display="Dynamic"
                                    ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtPin" Text='<%# Eval("Pin")%>' />
                                <asp:RequiredFieldValidator ID="rqdPin" runat="server" ErrorMessage="*" ValidationGroup="Update"
                                    ControlToValidate="txtPin" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <asp:CheckBox ID="chkActive" runat="server" Checked='<%# Eval("Active") %>' />
                            </td>
                            <td>
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="lnkUpdate" CommandName="UpdateItem" class="ui-state-default ui-corner-all"
                                        title="Update Record" ValidationGroup="Update"><span class="ui-icon ui-icon-disk"></span></asp:LinkButton>
                                </ul>
                            </td>
                            <td>
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="lnkCancelUpdate" CommandName="CancelUpdate" class="ui-state-default ui-corner-all"
                                        title="Cancel Update" CausesValidation="false"><span class="ui-icon ui-icon-cancel"></span></asp:LinkButton>
                                </ul>
                            </td>
                        </tr>
                    </tbody>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <tbody>
                        <tr class="odd">
                            <td>
                                <asp:TextBox runat="server" ID="txtAccountNamei" ValidationGroup="Insert" />
                                <asp:RequiredFieldValidator ID="rqdName" runat="server" ErrorMessage="*" ValidationGroup="Insert"
                                    ControlToValidate="txtAccountNamei" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtSmtpServeri" ValidationGroup="Insert" />
                                <asp:RequiredFieldValidator ID="rqdServer" runat="server" ErrorMessage="*" ValidationGroup="Insert"
                                    ControlToValidate="txtSmtpServeri" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtSqlProfilei" ValidationGroup="Insert" />
                                <asp:RequiredFieldValidator ID="RqdProfilei" runat="server" ErrorMessage="*" ValidationGroup="Insert"
                                    ControlToValidate="txtSqlProfilei" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtPorti" ValidationGroup="Insert" Text="25" Width="20px" />
                                <asp:RequiredFieldValidator ID="RqdNumi" runat="server" ErrorMessage="*" ValidationGroup="Insert"
                                    ControlToValidate="txtPorti" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="CvdNumi" runat="server" ControlToValidate="txtPorti" Display="Dynamic"
                                    ErrorMessage="numeric value required" Operator="DataTypeCheck" Type="Integer"
                                    ValidationGroup="Insert" ForeColor="Red"></asp:CompareValidator>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtUserNamei" ValidationGroup="Insert" />
                                <asp:RequiredFieldValidator ID="rqdVNamei" runat="server" ErrorMessage="*" ValidationGroup="Insert"
                                    ControlToValidate="txtSmtpServeri" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtPini" ValidationGroup="Insert" />
                                <asp:RequiredFieldValidator ID="rqdPini" runat="server" ErrorMessage="*" ValidationGroup="Insert"
                                    ControlToValidate="txtPini" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <asp:CheckBox ID="chkActivei" runat="server" Checked="true" />
                            </td>
                            <td>
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="lnkSave" CommandName="AddItem" class="ui-state-default ui-corner-all"
                                        title="Update Record" ValidationGroup="Insert"><span class="ui-icon ui-icon-disk"></span></asp:LinkButton>
                                </ul>
                            </td>
                            <td>
                                <ul id="icons">
                                    <asp:LinkButton runat="server" ID="lnkCancelUpdate" CommandName="CancelUpdate" class="ui-state-default ui-corner-all"
                                        title="Cancel Update" CausesValidation="false"><span class="ui-icon ui-icon-cancel"></span></asp:LinkButton>
                                </ul>
                            </td>
                        </tr>
                    </tbody>
                </InsertItemTemplate>
                <EmptyDataTemplate>
                    <p class="errorMsg">
                        There are no records to display.
                    </p>
                </EmptyDataTemplate>
            </asp:ListView>
            <br />
            <asp:DataPager ID="pager" runat="server" PagedControlID="lstData">
                <Fields>
                    <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True" ShowNextPageButton="False"
                        ShowPreviousPageButton="False" />
                    <asp:NumericPagerField />
                    <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True" ShowNextPageButton="False"
                        ShowPreviousPageButton="False" />
                </Fields>
            </asp:DataPager>
            <br />
            <br />
            <p class="errorMsg">
                <asp:Literal ID="lblMsg" runat="server"></asp:Literal></p>
        </div>
        <div id="tabs-3">
            <p>
                <asp:Button ID="btnNew" runat="server" class="button" Text="Add New SMTP Server"
                    OnClick="lnkAdd_Click" CausesValidation="False" />
                <asp:Button ID="lnkCancel0" runat="server" class="button" Text="Done" OnClick="lnkCancel_Click"
                    CausesValidation="false" />
            </p>
        </div>
        <asp:UpdateProgress ID="UpdateProgressData" runat="server" AssociatedUpdatePanelID="UpdatePanelData">
            <ProgressTemplate>
                <div>
                    <asp:Image ID="imgProj" runat="server" ImageUrl="~/ui/images/icons/activity.gif"
                        AlternateText="Please wait.."></asp:Image></div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
