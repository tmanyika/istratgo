﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.implementations;
using scorecard.entities;
using scorecard.controllers;
using HR.Human.Resources;

namespace scorecard.ui.controls
{
    public partial class graphReportForm : System.Web.UI.UserControl
    {
        #region  Variables

        useradmincontroller usersDef;
        goaldefinition goalDef;
        scoresmanager scoresManager;

        #endregion

        #region Default Class Constructors

        public graphReportForm()
        {
            usersDef = new useradmincontroller(new useradminImpl());
            goalDef = new goaldefinition(new goalsImpl());
            scoresManager = new scoresmanager(new scorecard.implementations.scoresImpl());
        }

        #endregion

        #region Page Events Handling Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                getGoalsCriteriaTypes();
                ddlValueTypes.Items.Insert(0, new ListItem { Text = "-- select value --", Value = "0" });
            }
        }

        #endregion

        #region Databinding Methods

        void getGoalsCriteriaTypes()
        {
            var areaList = goalDef.appsettings.getChildSettings(new applicationsettings { PARENT_ID = Util.getTypeDefinitionsGoalAreaTypes() }).ToList();
            BindControl.BindDropdown(ddlArea, "DESCRIPTION", "TYPE_ID", "-- select area --", "0", areaList.ToList());
        }

        void loadOrgStruct()
        {
            try
            {
                var userRole = Util.user.UserTypeId;
                var userID = Util.user.UserId;
                var structure = goalDef.orgstructure.getCompanyStructureListView(new company { COMPANY_ID = Util.user.CompanyId });
                if (userRole != Util.getTypeDefinitionsUserTypeAdministrator()) structure = structure.Where(t => t.OWNER_ID == userID).ToList();
                rqdVal.ErrorMessage = "select organisation unit";
                BindControl.BindDropdown(ddlValueTypes, "ORGUNIT_NAME", "ID", "- select organisation unit -", "0", structure.ToList());
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void loadProjects(int companyId)
        {
            projectmanagement proj = new projectmanagement(new projectImpl());

            int adminRoleId = Util.getTypeDefinitionsUserTypeAdministrator();
            int roleId = (int)Util.user.UserTypeId;
            IEnumerable<project> projL = proj.get(companyId, true);
            if (adminRoleId != Util.user.UserTypeId) projL = projL.Where(c => c.RESPONSIBLE_PERSON_ID == Util.user.UserId);
            rqdVal.ErrorMessage = "select a project";
            BindControl.BindDropdown(ddlValueTypes, "PROJECT_NAME", "PROJECT_ID", "-- select project --", "0", projL);
        }

        void loadJobTitles()
        {
            try
            {
                IEmployee obj = new EmployeeBL();
                List<Employee> titles = obj.GetByOrgStructure(Util.user.OrgId, true);
                if (Util.user.UserTypeId != Util.getTypeDefinitionsUserTypeAdministrator())
                    titles = titles.Where(t => t.LineManagerEmployeeId == Util.user.UserId || t.EmployeeId == Util.user.UserId).ToList();
                BindControl.BindDropdown(ddlValueTypes, "FullName", "EmployeeId", "-- select a user --", "0", titles);
                rqdVal.ErrorMessage = "select a user";
                loadDates(int.Parse(ddlArea.SelectedValue), 0);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void loadDates(int areaId, int idx)
        {
            int areaValueId = 0;
            if (areaId == Util.getTypeDefinitionsRolesTypeID() && idx == 0)
            {
                ddRepName.SelectedIndex = 0;
                areaValueId = Util.user.CompanyId;
            }
            else areaValueId = int.Parse(ddlValueTypes.SelectedValue);

            List<scores> dates = scoresManager.getScoreDateValues(int.Parse(ddlArea.SelectedValue), int.Parse(ddlValueTypes.SelectedValue));
            BindControl.BindDropdown(ddDate, "PERIOD_DATE_DESC", "PERIOD_DATE_VAL", "- select date -", "0", dates);
        }

        void setCriteriaSelected()
        {
            try
            {
                int areaId = int.Parse(ddlArea.SelectedValue);
                if (areaId == Util.getTypeDefinitionsRolesTypeID()) loadJobTitles();
                else if (areaId == Util.getTypeDefinitionsProjectID()) loadProjects(Util.user.CompanyId);
                else loadOrgStruct();
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void downloadReport(bool addEventToButton)
        {
            string areaId = ddlArea.SelectedValue;
            string areaValue = ddlValueTypes.SelectedValue;
            string dateVal = ddDate.SelectedValue;
            string url = ddRepName.SelectedValue;
            if (int.Parse(ddlArea.SelectedValue) == Util.getTypeDefinitionsRolesTypeID() &&
                ddlValueTypes.SelectedIndex == 0)
            {
                url = "actualvstargetusers.aspx";
                areaValue = Util.user.CompanyId.ToString();
            }

            string script = addEventToButton ? string.Format("return openNewWindow('{3}?aid={0}&idv={1}&dVal={2}&isOrgId={4}');", areaId, areaValue, dateVal, url, false.ToString().ToLower()) :
                                              string.Format("openNewWindow('{3}?aid={0}&idv={1}&dVal={2}&isOrgId={4}');", areaId, areaValue, dateVal, url, false.ToString().ToLower());
            if (addEventToButton) btnViewReport.OnClientClick = script;
            else AjaxScriptManager.RegisterStartupScript(Page, typeof(Page), "mygReport", script, true);
        }

        #endregion

        #region Dropdown Event Handling Methods

        protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddDate.Items.Clear();
            ddlValueTypes.Items.Clear();
            setCriteriaSelected();
        }

        protected void ddlValueTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            int areaId = int.Parse(ddlArea.SelectedValue);
            int idx = ddlValueTypes.SelectedIndex;
            loadDates(areaId, idx);
        }

        //protected void ddDate_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    downloadReport();
        //}

        //protected void ddRepName_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    downloadReport();
        //}

        #endregion

        #region Button Event Handling Methods

        protected void btnViewReport_Click(object sender, EventArgs e)
        {
            downloadReport(false);
        }

        #endregion
    }
}