﻿function openNewWindow(url) {
    window.open(url, 'open_window', 'menubar=1, toolbar=0, status=0, scrollbars=1, resizable=1, width=800, height=600, left=0, top=0');
    return false;
}

function openStandardWindow(url) {
    window.open(url, 'open_window', 'menubar=1, toolbar=0, status=0, scrollbars=1, resizable=1, width=1024, height=768, left=0, top=0');
    return false;
}