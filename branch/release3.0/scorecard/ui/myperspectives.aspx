﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ui/inside.Master" AutoEventWireup="true" CodeBehind="myperspectives.aspx.cs" Inherits="scorecard.ui.myperspectives" %>
<%@ Register src="controls/perspectives.ascx" tagname="perspectives" tagprefix="uc1" %>
<%@ Register src="controls/orgstructure.ascx" tagname="orgstructure" tagprefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <uc1:perspectives ID="perspectives1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="OrgStructure" runat="server">
    <uc2:orgstructure ID="orgstructure1" runat="server" />
</asp:Content>
