﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ui/inside.Master" AutoEventWireup="true" CodeBehind="noaccess.aspx.cs" Inherits="scorecard.ui.noaccess" %>

<%@ Register src="controls/orgstructure.ascx" tagname="orgstructure" tagprefix="uc1" %>
<%@ Register src="controls/goal.ascx" tagname="goal" tagprefix="uc2" %>


<asp:Content ID="Content1" runat="server" contentplaceholderid="MainContent">
    <div class="message error close"><h2>Error!</h2><p>You haven't been granted access to this section of the system, please contact the administrator </p></div>
</asp:Content>

<asp:Content ID="Content2" runat="server" contentplaceholderid="OrgStructure">
    <uc1:orgstructure ID="orgstructure1" runat="server" />
</asp:Content>
