﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="perfomanceReport.ascx.cs"
    Inherits="scorecard.ui.reports.perfomanceReport" %>
<div style="background-color: White;">
    <table cellpadding="0" cellspacing="10" border="0" width="90%">
        <tr>
            <td style="width: 250px;">
                <h2>
                    &nbsp;</h2>
            </td>
            <td style="width: 250px;">
                &nbsp;
            </td>
            <td align="right">
                <span style="color: Red">
                    <asp:Literal ID="lblError" runat="server"></asp:Literal></span>
                <asp:LinkButton ID="lnkPdf" runat="server" CausesValidation="False" OnClick="lnkPdf_Click">
                    <img runat="server" src="~/ui/assets/icons/pdf.png" id="imgPrint" alt="Click to Print" /></asp:LinkButton>&nbsp;&nbsp;<asp:LinkButton
                        ID="lnkExcel" runat="server" CausesValidation="False" OnClick="lnkExcel_Click">
                        <img src="~/ui/assets/icons/Excel.png" runat="server" id="imgExcel" alt="Export to Excel" /></asp:LinkButton>
            </td>
        </tr>
    </table>
</div>
<div>
    <asp:Repeater ID="rptData" runat="server">
        <HeaderTemplate>
            <table cellpadding="10" cellspacing="10" border="0" width="100%">
                <thead>
                    <tr>
                        <td style="width: 250px;">
                            <h2>
                                Report Type</h2>
                        </td>
                        <td colspan="2">
                            <h2>
                                <span style="color: Black">
                                    <asp:Literal ID="lblCriteria" runat="server"></asp:Literal></span></h2>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h2>
                                Name</h2>
                        </td>
                        <td colspan="2">
                            <h2>
                                <span style="color: Black">
                                    <asp:Literal ID="lblName" runat="server"></asp:Literal>
                                </span>
                            </h2>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h2>
                                Start Date</h2>
                        </td>
                        <td colspan="2">
                            <h2>
                                <span style="color: Black">
                                    <asp:Literal ID="lblStartDate" runat="server"></asp:Literal>
                                </span>
                            </h2>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h2>
                                End Date</h2>
                        </td>
                        <td colspan="2">
                            <h2>
                                <span style="color: Black">
                                    <asp:Literal ID="lblEndDate" runat="server"></asp:Literal>
                                </span>
                            </h2>
                        </td>
                    </tr>
                </thead>
            </table>
            <table class="fullwidth" cellpadding="0" cellspacing="0" border="0" width="100%">
                <thead>
                    <tr>
                        <th align="left">
                            <span style="font-size: 12px">Name</span>
                        </th>
                        <th align="right">
                            <span style="font-size: 12px">Average Perfomance Score</span>
                        </th>
                        <th align="right">
                            <span style="font-size: 12px">Target</span>
                        </th>
                        <th align="right">
                            <span style="font-size: 12px">Average Weighted Defined Score</span>
                        </th>
                    </tr>
                </thead>
                <tbody>
        </HeaderTemplate>
        <ItemTemplate>
            <tr class="odd">
                <td align="left">
                    <asp:Literal ID="lblPerspective" runat="server" Text='<%#  Eval("ItemName") %>'></asp:Literal>
                </td>
                <td align="right">
                    <asp:Literal ID="lblFinalScore" runat="server" Text='<%# Eval("ActualValue") %>' />%
                </td>
                <td align="right">
                    <asp:Literal ID="lblTarget" runat="server" Text='<%# Eval("TargetValue") %>' />
                </td>
                <td align="right">
                    <asp:Literal ID="lblRatingValue" runat="server" Text='<%# Eval("DefinedActualValue") %>' />
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </tbody> </table>
        </FooterTemplate>
    </asp:Repeater>
</div>
