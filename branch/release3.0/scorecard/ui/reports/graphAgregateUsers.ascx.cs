﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using scorecard.implementations;
using scorecard.controllers;
using scorecard.entities;
using vb = Microsoft.VisualBasic;
using System.Collections;

namespace scorecard.ui.reports
{
    public partial class graphAgregateUsers : System.Web.UI.UserControl
    {
        #region Variables

        useradmincontroller usersDef;
        goaldefinition goalDef;
        scoresmanager scoresManager;
        structurecontroller orgunits;

        #endregion

        #region Properties

        int? JobTitleId
        {
            get
            {
                int? jib = null;
                if (Request["jIb"] != null)
                    jib = int.Parse(Request["jIb"].ToString());
                return jib;
            }
        }

        bool IsOrgId
        {
            get
            {
                return (Request["isOrgId"] != null) ? bool.Parse(Request["isOrgId"].ToString()) : false;
            }
        }
        string Date
        {
            get
            {
                return Request["dVal"].ToString();
            }
        }

        int AreaId
        {
            get
            {
                return int.Parse(Request["aid"].ToString());
            }
        }

        string AreaValueId
        {
            get
            {
                string[] dataVal = Request["idv"].ToString().Split(',');
                return Common.GetXML(dataVal, "Record", "Score", "Id");
            }
        }

        int CompanyId
        {
            get
            {
                return int.Parse(Request["cId"].ToString());
            }
        }
        #endregion

        #region Default Class Constructors

        public graphAgregateUsers()
        {
            goalDef = new goaldefinition(new goalsImpl());
            orgunits = new structurecontroller(new structureImpl());
            usersDef = new useradmincontroller(new useradminImpl());
            scoresManager = new scoresmanager(new scorecard.implementations.scoresImpl());
        }

        #endregion

        #region Page Event Handling Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                fillInfo();
                loadData();
            }
        }

        #endregion

        #region Databinding Methods

        private void fillInfo()
        {
            try
            {
                lblCriteria.Text = usersDef.appsettings.getApplicationSetting(new entities.applicationsettings { TYPE_ID = AreaId }).NAME;
                lblName.Text = "User Comparison";
                lblDate.Text = Common.GetDescriptiveDate(Date, false);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        DataTable getData()
        {
            var objectives = scoresManager.getComparisonReport(AreaId, AreaValueId, Date);

            DataTable dT = new DataTable();
            dT.Columns.Add("FullName", typeof(string));
            dT.Columns.Add("TargetValue", typeof(double));
            dT.Columns.Add("ActualValue", typeof(double));
            int prevUserId = 0;
            DataRow nRow;
            foreach (scores item in objectives)
            {
                if (AreaValueId.Contains(item.USER_ID.ToString()))
                {
                    double totalTarget = 0;
                    double totalActual = 0;
                    int nxtUserId = item.USER_ID;
                    if (nxtUserId != prevUserId)
                    {
                        int uid = item.USER_ID;
                        string fullName = item.FULL_NAME;
                        nRow = dT.NewRow();
                        totalTarget = objectives.Where(c => c.USER_ID == uid).Sum(u => u.WEIGHT);
                        totalActual = Math.Round(objectives.Where(c => c.USER_ID == uid).Sum(u => u.FINAL_SCORE), 2);
                        nRow["FullName"] = fullName;
                        nRow["TargetValue"] = totalTarget;
                        nRow["ActualValue"] = totalActual;
                        dT.Rows.Add(nRow);
                        nRow = null;
                    }
                    prevUserId = nxtUserId;
                }
            }
            dT.AcceptChanges();
            return dT;
        }

        void loadData()
        {
            try
            {
                DataTable dT = getData();
                int idx = 0;
                foreach (DataRow rw in dT.Rows)
                {
                    string seriesName = Util.getGraphLabelName(rw["FullName"].ToString());
                    double target = double.Parse(rw["TargetValue"].ToString());
                    double actual = double.Parse(rw["ActualValue"].ToString());

                    orgChart.Series[0].Points.AddY(actual);
                    orgChart.Series[1].Points.AddY(target);
                    orgChart.Series[0].Points[idx].AxisLabel = seriesName;
                    idx++;
                }

                if (dT.Rows.Count >= Util.getGraphMinItems())
                {
                    orgChart.Height = new Unit(Util.getGraphHeight());
                    orgChart.Width = new Unit(Util.getGraphWidth());
                }
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        #endregion
    }
}