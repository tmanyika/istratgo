﻿using System;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

namespace HR.Human.Resources
{
    public class EmployeeStatusDAL
    {
        #region Variables

        private string _conString;

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return _conString; }
            set { _conString = value; }
        }

        #endregion

        #region Default Constructors

        public EmployeeStatusDAL(string conString)
        {
            _conString = conString;
        }

        #endregion

        #region Public Methods and Functions

        public DataTable GetAll()
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_EmployeeStatus_GetAll").Tables[0];
        }

        public DataTable Get(bool active)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_EmployeeStatus_GetActive", active).Tables[0];
        }

        public DataTable GetById(int statusId)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_EmployeeStatus_GetById", statusId).Tables[0];
        }

        public int Save(EmployeeStatus obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_EmployeeStatus_Insert", obj.StatusName,
                                                       obj.Active, obj.CreatedBy);
            return result;
        }

        public int Update(EmployeeStatus obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_EmployeeStatus_Update", obj.StatusId, obj.StatusName,
                                                        obj.Active, obj.UpdatedBy);
            return result;
        }

        public int Deactivate(int statusId, string updatedBy)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_EmployeeStatus_Deactivate", statusId, updatedBy);
            return result;
        }

        #endregion
    }
}