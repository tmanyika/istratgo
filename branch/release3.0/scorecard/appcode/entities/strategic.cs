﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/* @Copy Albertoncaffeine.net */

namespace scorecard.entities
{
    public class strategic : perspective
    {
        public int ID { get; set; }
        public int ORG_ID { get; set; }
        public int PERSPECTIVE { get; set; }
        public string OBJECTIVE { get; set; }
        public int AREA_TYPE_ID { get; set; }
        public int AREA_ID { get; set; }
    }

    public class environmentcontext
    {
        public int ENV_CONTEXT_ID { get; set; }
        public int COMPANY_ID { get; set; }
        public int AREA_TYPE_ID { get; set; }
        public int AREA_ID { get; set; }
        public string ENV_CONTEXT { get; set; }
        public DateTime DATE_CREATED { get; set; }
        public DateTime? DATE_UPDATED { get; set; }

    }
}