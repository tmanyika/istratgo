﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace scorecard.entities
{
    public class project
    {

        public int PROJECT_ID { get; set; }
        public int COMPANY_ID { get; set; }
        public int? RESPONSIBLE_PERSON_ID { get; set; }
        public string RESPONSIBLE_NAME { get; set; }
        public string RESPONSIBLE_EMAIL { get; set; }

        public string PROJECT_NAME { get; set; }
        public string PROJECT_DESC { get; set; }
        public string CREATED_BY { get; set; }
        public string UPDATE_BY { get; set; }

        public bool ACTIVE { get; set; }

        public DateTime DATE_CREATED { get; set; }
        public DateTime? DATE_UPDATED { get; set; }

    }
}