﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/* @Copy Albertoncaffeine.net */

namespace scorecard.entities
{
    public class token
    {
        public int ID { get; set; }
        public string TOKEN { get; set; }
        public DateTime EXPIRES { get; set; }
        public int ACCOUNT_NO { get; set; }
        public DateTime DATE_CREATED { get; set; }
    }
}