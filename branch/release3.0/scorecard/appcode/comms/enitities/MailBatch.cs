﻿using System;

namespace System.Email.Communication
{
    public class MailBatch
    {
        public int BatchId { get; set; }
        public int AccountMailId { get; set; }
        public int MailId { get; set; }
        public int NoOfUsers { get; set; }

        public bool IsSent { get; set; }
        public bool SendNow { get; set; }

        public string BatchName { get; set; }
        public string MailSubject { get; set; }
        public string MailContent { get; set; }
        public string MainTemplate { get; set; }
        public string UserRoles { get; set; }
        public string CreatedBy { get; set; }
       
        public DateTime DateToBeSent { get; set; }
        public DateTime DateCreated { get; set; }

        public MailServer Server { get; set; }
    }
}