﻿using System;
using System.Collections.Generic;
using System.Data;

namespace System.Email.Communication
{
    public interface IMailServer
    {
        bool AddMailServer(MailServer obj);
        bool Delete(int accountId, string updatedBy);

        MailServer GetById(int mailId);
        MailServer GetRecord(DataRow rw);
        List<MailServer> GetByStatus(bool active);
        List<MailServer> GetAll();
    }
}
