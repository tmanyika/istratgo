﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using scorecard.interfaces;
using System.Data;
using scorecard.implementations;
using scorecard.entities;

namespace scorecard.controllers
{
    public class reportcontroller
    {
        Ireports rep;

        public reportcontroller(Ireports _rep)
        {
            rep = _rep;
        }

        public DataTable getCustomerReport()
        {
            return rep.getCustomerReport();
        }

        public DataTable getDepartmentReport(string  startdate, string enddate,string company)
        {
            return rep.getDepartmentReport( startdate, enddate,company);
        }

        public DataTable getEmpJobReport(string startdate, string enddate, string jobtitle, string company)
        {
            return rep.getEmpJobReport( startdate,  enddate,  jobtitle,company );
        }

        public DataTable getEmpDeptReport(string   startdate, string enddate , string company , string deptName)
        {
            return rep.getEmpDeptReport(startdate, enddate, company, deptName);
        }


        public List<roleentityreport> getReportData(useradmin _user)
        {
            return rep.getReportData(_user);
        }

        public List<roleentityreport> getReportData(Orgunit _org)
        {
            return rep.getReportData(_org);
        }
    }
}