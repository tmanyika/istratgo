﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using scorecard.interfaces;
using scorecard.entities;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

/* @Copy Albertoncaffeine.net */


namespace scorecard.implementations
{
    public class customerImpl : Icustomer
    {
        #region Icustomer Members

        public int registerCustomer(customer _customerInfo, company _company, out string errorMessage)
        {
            int companyId = 0;
            errorMessage = "";
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try
            {
                SqlDataReader reader = SqlHelper.ExecuteReader(con, "PROC_ADD_REGISTEREDCUSTOMER",
                    _customerInfo.USER_NAME,
                    _customerInfo.SETUP_TYPE,
                    _customerInfo.PASSWORD,
                    _customerInfo.EMAIL_ADDRESS,
                    _customerInfo.FIRST_NAME,
                    _customerInfo.LAST_NAME,
                    _customerInfo.CELL_NUMBER,
                    _customerInfo.DATE_OF_BIRTH,
                    _company.COMPANY_NAME,
                    _customerInfo.JOB_TITLE_NAME,
                    _customerInfo.USER_TYPE,
                    _customerInfo.CountryId
                    );

                if (reader.HasRows)
                {
                    reader.Read();
                    companyId = int.Parse(reader["companyId"].ToString());
                    bool unameExist = bool.Parse(reader["uname"].ToString());
                    bool emailExist = bool.Parse(reader["email"].ToString());
                    errorMessage = unameExist ? "Username" : string.Empty;
                    errorMessage = (emailExist && unameExist) ? string.Format("{0} and {1}", errorMessage, "Email") : emailExist ? "Email" : string.Empty;
                    errorMessage = string.Format("{0} already exist.", errorMessage);
                }
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                companyId = 0;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
            return companyId;
        }
        public int updateCustomer(customer _customerInfo)
        {

            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try
            {
                return SqlHelper.ExecuteNonQuery(con, "PROC_UPDATE_CUSTOMER",
                    _customerInfo.ACCOUNT_NO,
                    _customerInfo.EMAIL_ADDRESS,
                    _customerInfo.FIRST_NAME,
                    _customerInfo.LAST_NAME,
                    _customerInfo.CELL_NUMBER,
                    _customerInfo.DATE_OF_BIRTH
                    );
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return 0;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        public int deleteCustomer(customer _customerInfo)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try
            {
                return SqlHelper.ExecuteNonQuery(con, "PROC_UPDATE_CUSTOMER", _customerInfo.ACCOUNT_NO);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return 0;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        public customer getCustomerInfo(customer _customer)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            SqlDataReader read = null;
            try
            {
                List<customer> customers = new List<customer>();
                read = SqlHelper.ExecuteReader(con, "PROC_GET_CUSTOMER", _customer.ACCOUNT_NO);
                while (read.Read())
                {
                    customers.Add(new customer
                    {
                        ACCOUNT_NO = read.GetInt32(0),
                        PASSWORD = read.GetString(1),
                        EMAIL_ADDRESS = read.GetString(2),
                        FIRST_NAME = read.GetString(3),
                        LAST_NAME = read.GetString(4),
                        CELL_NUMBER = read.GetString(5),
                        DATE_OF_BIRTH = read.GetDateTime(6),
                        DATE_CREATED = read.GetDateTime(7)
                    });
                }
                return customers[0];
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
            finally
            {
                read.Dispose();
                read.Close();
                con.Dispose();
                con.Close();
            }
        }
        public List<customer> getCustomersList()
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            SqlDataReader read = null;
            try
            {
                List<customer> customers = new List<customer>();
                read = SqlHelper.ExecuteReader(con, "PROC_GET_CUSTOMER_LIST");
                while (read.Read())
                {
                    customers.Add(new customer
                    {
                        ACCOUNT_NO = read.GetInt32(0),
                        SETUP_TYPE = read.GetInt32(1),
                        PASSWORD = read.GetString(2),
                        EMAIL_ADDRESS = read.GetString(3),
                        FIRST_NAME = read.GetString(4),
                        LAST_NAME = read.GetString(5),
                        CELL_NUMBER = read.GetString(6),
                        USER_NAME = read.GetString(7),
                        DATE_OF_BIRTH = read.GetDateTime(8),
                        DATE_CREATED = read.GetDateTime(9)
                    });
                }
                return customers;
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
            finally
            {
                read.Dispose();
                read.Close();
                con.Dispose();
                con.Close();
            }
        }
        #endregion

        #region Icustomer Members

        public int registerCustomer(customer _customerInfo, company _company, bankaccount _account)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try
            {
                return (int)SqlHelper.ExecuteScalar(con, "PROC_ADD_CUSTOMER",
                _customerInfo.USER_NAME,
                 _customerInfo.SETUP_TYPE,
                 _customerInfo.PASSWORD,
                 _customerInfo.EMAIL_ADDRESS,
                 _customerInfo.FIRST_NAME,
                 _customerInfo.LAST_NAME,
                 _customerInfo.CELL_NUMBER,
                 _customerInfo.DATE_OF_BIRTH,
                 _company.COMPANY_NAME,
                 _company.REGISTRATION_NO,
                 _company.INDUSTRY,
                 _company.COUNTRY,
                 _company.VAT_NO,
                 _company.ADDRESS,
                 _company.CONTACT_NO,
                 _account.BANK_NAME,
                 _account.BRANCH_NAME,
                 _account.BRANCH_CODE,
                 _customerInfo.JOB_TITLE_NAME,
                 _customerInfo.USER_TYPE
                 );
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return 0;
            }
        }

        #endregion
    }
}