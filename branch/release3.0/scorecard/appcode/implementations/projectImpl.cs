﻿using System;
using System.Collections.Generic;
using scorecard.interfaces;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using scorecard.entities;

namespace scorecard.implementations
{
    public class projectImpl : Iproject
    {
        public bool addProject(project obj)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try
            {
                int result = (int)SqlHelper.ExecuteScalar(con, "PROC_ADD_PROJECT",
                 obj.COMPANY_ID,
                 obj.PROJECT_NAME,
                 obj.PROJECT_DESC,
                 obj.CREATED_BY,
                 obj.RESPONSIBLE_PERSON_ID
                 );
                return result > 0 ? true : false;
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return false;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public bool updateProject(project obj)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try
            {
                int result = (int)SqlHelper.ExecuteScalar(con, "PROC_UPDATE_PROJECT",
                  obj.PROJECT_ID,
                 obj.PROJECT_NAME,
                 obj.PROJECT_DESC,
                 obj.UPDATE_BY,
                 obj.RESPONSIBLE_PERSON_ID
                 );
                return result > 0 ? true : false;
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return false;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public bool setProjectStatus(project obj)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try
            {
                int result = (int)SqlHelper.ExecuteScalar(con, "PROC_DEACTIVATE_PROJECT",
                    obj.PROJECT_ID, obj.UPDATE_BY, obj.ACTIVE);
                return result > 0 ? true : false;
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return false;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public project getProject(int projectId)
        {

            SqlConnection con = new SqlConnection(Util.GetconnectionString());

            try
            {
                DateTime? defDate = null;
                using (SqlDataReader rw = SqlHelper.ExecuteReader(con, "PROC_GET_PROJECT", projectId))
                {
                    if (rw.HasRows)
                    {
                        rw.Read();
                        return new project
                             {
                                 ACTIVE = bool.Parse(rw["ACTIVE"].ToString()),
                                 COMPANY_ID = int.Parse(rw["COMPANY_ID"].ToString()),
                                 CREATED_BY = rw["CREATED_BY"].ToString(),
                                 DATE_CREATED = DateTime.Parse(rw["DATE_CREATED"].ToString()),
                                 DATE_UPDATED = (rw["DATE_UPDATED"] != DBNull.Value) ? DateTime.Parse(rw["DATE_UPDATED"].ToString()) : defDate,
                                 PROJECT_DESC = (rw["PROJECT_DESC"] != DBNull.Value) ? rw["PROJECT_DESC"].ToString() : string.Empty,
                                 PROJECT_ID = projectId,
                                 PROJECT_NAME = rw["PROJECT_NAME"].ToString(),
                                 UPDATE_BY = (rw["UPDATE_BY"] != DBNull.Value) ? rw["UPDATE_BY"].ToString() : string.Empty,
                                 RESPONSIBLE_PERSON_ID = (rw["RESPONSIBLE_PERSON_ID"] == DBNull.Value) ? 0 : int.Parse(rw["RESPONSIBLE_PERSON_ID"].ToString()),
                                 RESPONSIBLE_NAME = (rw["NAME"] != DBNull.Value) ? rw["NAME"].ToString() : string.Empty,
                                 RESPONSIBLE_EMAIL=(rw["EMAIL_ADDRESS"] != DBNull.Value) ? rw["EMAIL_ADDRESS"].ToString() : string.Empty,
                             };
                    }
                    else return new project();
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return new project();
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public List<project> getProjects(int companyId, bool active)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try
            {
                List<project> proj = new List<project>();
                DateTime? defDate = null;
                int? defInt = null;
                using (SqlDataReader rw = SqlHelper.ExecuteReader(con, "PROC_GET_COMPANYPROJECTS", companyId, active))
                {
                    while (rw.Read())
                        proj.Add(new project
                        {
                            ACTIVE = bool.Parse(rw["ACTIVE"].ToString()),
                            COMPANY_ID = int.Parse(rw["COMPANY_ID"].ToString()),
                            CREATED_BY = rw["CREATED_BY"].ToString(),
                            DATE_CREATED = DateTime.Parse(rw["DATE_CREATED"].ToString()),
                            DATE_UPDATED = (rw["DATE_UPDATED"] != DBNull.Value) ? DateTime.Parse(rw["DATE_UPDATED"].ToString()) : defDate,
                            PROJECT_DESC = (rw["PROJECT_DESC"] != DBNull.Value) ? rw["PROJECT_DESC"].ToString() : string.Empty,
                            PROJECT_ID = int.Parse(rw["PROJECT_ID"].ToString()),
                            PROJECT_NAME = rw["PROJECT_NAME"].ToString(),
                            UPDATE_BY = (rw["UPDATE_BY"] != DBNull.Value) ? rw["UPDATE_BY"].ToString() : string.Empty,
                            RESPONSIBLE_NAME = (rw["NAME"] != DBNull.Value) ? rw["NAME"].ToString() : string.Empty,
                            RESPONSIBLE_EMAIL = (rw["EMAIL_ADDRESS"] != DBNull.Value) ? rw["EMAIL_ADDRESS"].ToString() : string.Empty,
                            RESPONSIBLE_PERSON_ID = (rw["RESPONSIBLE_PERSON_ID"] != DBNull.Value) ? int.Parse(rw["RESPONSIBLE_PERSON_ID"].ToString()) : defInt,
                        });
                }
                return proj;
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return new List<project>();
            }
            finally
            {
                con.Dispose();
                con.Close();
            }
        }
    }
}