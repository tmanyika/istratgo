﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;

using scorecard.entities;
using scorecard.interfaces;
using System.Data;
using HierarchicalData;

/* @Copy Albertoncaffeine.net */

namespace scorecard.implementations
{
    public class rolebasedImpl : Irolebased
    {

        public int createAccess(menuaccess _menu)
        {

            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try
            {
                return SqlHelper.ExecuteNonQuery(con, "PROC_ADD_MENU_ACCESS",
                    _menu.MENU_ID,
                    _menu.USER_TYPE_ID,
                    _menu.COMPANY_ID
                    );
            }
            catch (SqlException sqle)
            {
                Util.LogErrors(sqle);
                return 0;
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return 0;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public int deleteAccessByCompany(menuaccess _menu)
        {

            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try
            {
                return SqlHelper.ExecuteNonQuery(con, "PROC_DELETE_MENU_ACCESS",
                    _menu.USER_TYPE_ID,
                    _menu.COMPANY_ID
                   );
            }
            catch (SqlException sqle)
            {
                Util.LogErrors(sqle);
                return 0;
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return 0;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public List<menu> getAllMenus()
        {

            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            SqlDataReader read = null;
            try
            {
                List<menu> menus = new List<menu>();
                read = SqlHelper.ExecuteReader(con, "PROC_GET_MENUS");
                while (read.Read())
                {
                    menus.Add(new menu
                    {
                        PARENT_MENU_ID = (read["PARENT_MENU_ID"] != DBNull.Value) ? int.Parse(read["PARENT_MENU_ID"].ToString()) : 0,
                        MENU_ID = int.Parse(read["MENU_ID"].ToString()),
                        MENU_NAME = read["MENU_NAME"].ToString(),
                        IMAGE_ICON = read["IMAGE_ICON"] != DBNull.Value ? read["IMAGE_ICON"].ToString() : string.Empty,
                        MENU_PATH = read["MENU_PATH"] != DBNull.Value ? read["MENU_PATH"].ToString() : string.Empty,
                        ORDER_ID = int.Parse(read["ORDER_ID"].ToString()),
                        ACTIVE = bool.Parse(read["ACTIVE"].ToString())
                    });
                }
                return menus.Where(c => c.ACTIVE == true).OrderBy(o => o.ORDER_ID).ToList();
            }
            catch (SqlException sqle)
            {
                Util.LogErrors(sqle);
                return null;
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
            finally
            {
                read.Dispose();
                read.Close();
                con.Close();
                con.Dispose();
            }
        }

        public HierarchicalDataSet getAccess(int companyId, int userRole)
        {

            using (SqlConnection con = new SqlConnection(Util.GetconnectionString()))
            {
                try
                {
                    DataSet Ds = SqlHelper.ExecuteDataset(con, "PROC_GET_ROLE_ACCESS", companyId, userRole);
                    return new HierarchicalDataSet(Ds, "Menu_Id", "Parent_Menu_Id");
                }
                catch (Exception ex)
                {
                    Util.LogErrors(ex);
                    return null;
                }
            }
        }

        public List<menuaccess> getAccessByCompany(company _company)
        {

            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            SqlDataReader read = null;
            try
            {
                List<menuaccess> access = new List<menuaccess>();
                read = SqlHelper.ExecuteReader(con, "PROC_GET_ACCESS_BY_COMPANY_ID", _company.COMPANY_ID);
                while (read.Read())
                {
                    access.Add(new menuaccess
                    {
                        ID = int.Parse(read["ID"].ToString()),
                        MENU_ID = int.Parse(read["MENU_ID"].ToString()),
                        USER_TYPE_ID = int.Parse(read["USER_TYPE_ID"].ToString()),
                        COMPANY_ID = int.Parse(read["COMPANY_ID"].ToString()),
                        DATE_CREATED = DateTime.Parse(read["DATE_CREATED"].ToString()),
                        MENU_NAME = read["MENU_NAME"].ToString(),
                        IMAGE_ICON = read["IMAGE_ICON"] != DBNull.Value ? read["IMAGE_ICON"].ToString() : string.Empty,
                        MENU_PATH = read["MENU_PATH"] != DBNull.Value ? read["MENU_PATH"].ToString() : string.Empty,
                        ORDER_ID = read["ORDER_ID"] != DBNull.Value ? int.Parse(read["ORDER_ID"].ToString()) : 0,
                        ACTIVE = read["Active"] != DBNull.Value ? bool.Parse(read["Active"].ToString()) : false
                    });
                }
                return access;
            }
            catch (SqlException sqle)
            {
                Util.LogErrors(sqle);
                return null;
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
            finally
            {
                read.Dispose();
                read.Close();
                con.Close();
                con.Dispose();
            }
        }
    }
}