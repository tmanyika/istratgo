﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace scorecard.welcome
{
    public class WelcomeState
    {
        public int EmployeeId { get; set; }
        public bool ShowAgain { get; set; }
        public DateTime? DateLastRequested { get; set; }
    }
}