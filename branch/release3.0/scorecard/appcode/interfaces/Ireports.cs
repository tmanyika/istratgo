﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using scorecard.entities;
using System.Data;


/* @Copy Albertoncaffeine.net */

namespace scorecard.interfaces
{
    public interface Ireports
    {
        List<roleentityreport> getReportData(useradmin _user);
        List<roleentityreport> getReportData(Orgunit _org);
        DataTable getCustomerReport();
        DataTable getDepartmentReport(string  startdate, string enddate, string company);
        DataTable getEmpJobReport(string startdate, string enddate, string jobtitle, string company);
        DataTable getEmpDeptReport(string startdate, string enddate, string company, string deptName);
    }
    
}
