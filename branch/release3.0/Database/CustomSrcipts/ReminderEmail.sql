set nocount on;
set transaction isolation level read uncommitted;

--declare @Subject varchar(1800), @MainTemplate nvarchar(max);

--select @Subject=MailSubject, @MainTemplate=MailContent from imd_user_db.MailTemplate where MailId = 1

----insert into imd_user_db.imd_user_db.MailOutbox(EmployeeId, FullName, Email, BatchId, MailSubject, MailContent, MainTemplate, IsSent, DateToBeSent)
select     e.EmployeeId, e.FullName, e.Email, 0 AS BatchId, 
			replace((select mt.MailSubject from imd_user_db.MailTemplate as mt where mt.MailId = 1),'{0}',t.MailSubject) as MailSubject, replace(t.MailContent collate database_default,'{2}',e.Months collate database_default) as MailContent, (select MailContent from imd_user_db.MailTemplate where MailId = 1) as MainTemplate, cast(0 as bit) as IsSent, getdate() as DateToBeSent
from         imd_user_db.vw_30To60DaysReminder AS e CROSS JOIN
            imd_user_db.MailTemplate AS t
where     (t.MailId = 5) and (not (e.EmployeeId in (select EmployeeId from imd_user_db.MailBatchUserList where BatchId = 0)))
union
select     e.EmployeeId, e.FullName, e.Email, -1 as BatchId, 
		replace((select tm.MailSubject from imd_user_db.MailTemplate as tm where tm.MailId = 1),'{0}',t.MailSubject) as MailSubject, t.MailContent, (select MailContent from imd_user_db.MailTemplate where MailId = 1) as MainTemplate, cast(0 as bit) as IsSent, getdate() as DateToBeSent
from       imd_user_db.vw_90DaysReminder as e CROSS JOIN
            imd_user_db.MailTemplate AS t
where     (t.MailId = 6) and (not (e.EmployeeId in (select EmployeeId from imd_user_db.MailBatchUserList where BatchId = -1)))




