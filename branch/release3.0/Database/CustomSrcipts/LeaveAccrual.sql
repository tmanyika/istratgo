USE [ScoreCard]
GO

/****** Object:  StoredProcedure [imd_user_db].[Proc_LeaveAccrual_GetById]    Script Date: 11/08/2012 20:29:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_LeaveAccrual_GetById]
	@leaveAccrualId int
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	select     l.AccrualTimeId, l.Active, l.CreatedBy, l.DateCreated, l.DateUpdated, l.UpdatedBy, l.LeaveAccrualId, l.LeaveId, l.WorkingWeekId, l.AccrualRate, l.MaximumThreshold, 
                      lat.TimeName, lat.TimeValue, w.CompanyId, w.Name, w.NoOfDays, lt.LeaveName, lt.LeaveDescription
	from         imd_user_db.LeaveAccrual AS l INNER JOIN
                      imd_user_db.LeaveAccrualTime AS lat ON l.AccrualTimeId = lat.AccrualTimeId INNER JOIN
                      imd_user_db.LeaveType AS lt ON l.LeaveId = lt.LeaveId INNER JOIN
                      imd_user_db.WorkingWeek AS w ON l.WorkingWeekId = w.WorkingWeekId
	where     (l.LeaveAccrualId = @leaveAccrualId)
END



GO

/****** Object:  StoredProcedure [imd_user_db].[Proc_LeaveAccrual_Deactivate]    Script Date: 11/08/2012 20:29:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_LeaveAccrual_Deactivate]
	@leaveAccrualId int,
	@updatedBy varchar(20)
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	update  imd_user_db.LeaveAccrual
	set     Active = 0, 
			UpdatedBy = @updatedBy, 
			DateUpdated = GETDATE()
	where   (LeaveAccrualId = @leaveAccrualId)
		
	select @@rowcount
END



GO

/****** Object:  StoredProcedure [imd_user_db].[Proc_LeaveAccrual_Update]    Script Date: 11/08/2012 20:29:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_LeaveAccrual_Update]
	@leaveAccrualId int,
	@leaveId int, 
	@workingWeekId int, 
	@accrualTimeId int,
    @accrualRate decimal(18,4), 
    @maximumThreshold decimal(18,4), 
    @active bit, 
    @updatedBy varchar(20)
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	update imd_user_db.LeaveAccrual
	set   LeaveId = @leaveId, 
		  WorkingWeekId = @workingWeekId, 
		  AccrualTimeId = @accrualTimeId, 
		  AccrualRate = @accrualRate, 
          MaximumThreshold = @maximumThreshold, 
          Active = @active, 
          UpdatedBy = @updatedBy, 
          DateUpdated = GETDATE()
	where LeaveAccrualId = @leaveAccrualId
	select @@rowcount
END



GO

/****** Object:  StoredProcedure [imd_user_db].[Proc_LeaveAccrual_Insert]    Script Date: 11/08/2012 20:29:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_LeaveAccrual_Insert]
	@leaveId int, 
	@workingWeekId int, 
	@accrualTimeId int,
    @accrualRate decimal(18,4), 
    @maximumThreshold decimal(18,4), 
    @active bit, 
    @createdBy varchar(20)
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	insert into imd_user_db.LeaveAccrual
	(
		LeaveId, 
		WorkingWeekId, 
		AccrualTimeId, 
		AccrualRate, 
		MaximumThreshold, 
		Active, 
		CreatedBy, 
		DateCreated
	)
	values
	(
		@leaveId, 
		@workingWeekId, 
		@accrualTimeId, 
		@accrualRate, 
		@maximumThreshold, 
		@active, 
		@createdBy, 
		getdate()
	)
		
	select @@rowcount
END



GO

/****** Object:  StoredProcedure [imd_user_db].[Proc_LeaveAccrual_GetActive]    Script Date: 11/08/2012 20:29:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_LeaveAccrual_GetActive]
	@companyId int,
	@active bit
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	select     l.AccrualTimeId, l.Active, l.CreatedBy, l.DateCreated, l.DateUpdated, l.UpdatedBy, l.LeaveAccrualId, l.LeaveId, l.WorkingWeekId, l.AccrualRate, l.MaximumThreshold, 
                      lat.TimeName, lat.TimeValue, w.CompanyId, w.Name, w.NoOfDays, lt.LeaveName, lt.LeaveDescription
	from      imd_user_db.LeaveAccrual AS l INNER JOIN
              imd_user_db.LeaveAccrualTime AS lat ON l.AccrualTimeId = lat.AccrualTimeId INNER JOIN
              imd_user_db.LeaveType AS lt ON l.LeaveId = lt.LeaveId INNER JOIN
              imd_user_db.WorkingWeek AS w ON l.WorkingWeekId = w.WorkingWeekId
	where     (l.Active = @active) AND (lt.CompanyId = @companyId)
	order by lt.LeaveName
END



GO

/****** Object:  StoredProcedure [imd_user_db].[Proc_LeaveAccrual_GetAll]    Script Date: 11/08/2012 20:29:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_LeaveAccrual_GetAll]
	@companyId int
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	select     l.AccrualTimeId, l.Active, l.CreatedBy, l.DateCreated, l.DateUpdated, l.UpdatedBy, l.LeaveAccrualId, l.LeaveId, l.WorkingWeekId, l.AccrualRate, l.MaximumThreshold, 
               lat.TimeName, lat.TimeValue, w.CompanyId, w.Name, w.NoOfDays, lt.LeaveName, lt.LeaveDescription
	from      imd_user_db.LeaveAccrual AS l INNER JOIN
              imd_user_db.LeaveAccrualTime AS lat ON l.AccrualTimeId = lat.AccrualTimeId INNER JOIN
              imd_user_db.LeaveType AS lt ON l.LeaveId = lt.LeaveId INNER JOIN
              imd_user_db.WorkingWeek AS w ON l.WorkingWeekId = w.WorkingWeekId
	where     (lt.CompanyId = @companyId)
	order by lt.LeaveName
END



GO


