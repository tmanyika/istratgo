--- import emails to be sent and push them into MailOutBox

select     e.EmployeeId, e.FirstName + ' ' + e.MiddleName + ' ' + e.LastName AS Fullname, e.EmailAddress as Email, mb.BatchId, mb.MailSubject, mb.MailContent, mb.MainTemplate, 
          mb.DateToBeSent, mb.IsSent
from  imd_user_db.EmployeeLogin INNER JOIN
      imd_user_db.Employee AS e ON imd_user_db.EmployeeLogin.EmployeeId = e.EmployeeId INNER JOIN
      imd_user_db.MailBatchUserRole AS ur INNER JOIN
      imd_user_db.MailBatch AS mb ON ur.BatchId = mb.BatchId ON imd_user_db.EmployeeLogin.RoleId = ur.RoleId
where     (mb.IsSent = 0) AND datediff(dd,mb.DateToBeSent,getdate()) >= 0

-- script to send emails
set nocount on;
set transaction isolation level read uncommitted;

declare @i int, @rec int, @batchId int, @empId int,
		@outboxId int, @todayDate datetime;
declare @subject varchar(2000), @email varchar(100),
		@sqlProfile varchar(100), @body varchar(max);

set @i = 1;
set @rec = 0;
set @sqlProfile = 'iStratGoAdmin';

select OutBoxId, EmployeeId, FullName, Email, BatchId, MailSubject, MailContent, MainTemplate, row_number() over(order by EmployeeId asc) as rId
into #outbox
from imd_user_db.MailOutbox
where IsSent = 0

select @rec=count(EmployeeId) from #outbox
begin try
	begin transaction
		while (@i <= @rec)
			begin
				select @outboxId=OutBoxId, @batchId=BatchId, @empId=EmployeeId, @email=Email, @body=replace(replace(MainTemplate,'{0}',FullName),'{1}', MailContent) , @subject=MailSubject from #outbox where rId = @i
				exec msdb.dbo.sp_send_dbmail 
								@profile_name=@sqlProfile,
								@importance = 'NORMAL',
								@recipients = @email,
								@subject = @subject,
								@body = @body,
								@body_format = 'HTML'
				
				if not (exists(select employeeid from imd_user_db.MailBatchUserList where batchId = @batchId and employeeId = @empId))
					insert into imd_user_db.MailBatchUserList(BatchId,EmployeeId,Email,Exported)
					values(@batchId,@empId,@email,0)
				
				update imd_user_db.MailOutbox
				set IsSent = 1
				where OutBoxId = @outboxId
				
				set @i = @i + 1;
			end
	commit transaction	
end try
begin catch
	if(@@trancount > 0)
		rollback transaction
end catch
drop table #outbox

------ Update export field ----
update imd_user_db.MailBatchUserList
set Exported = 1
where Exported = 0

------------ Update Live Table Statistics ---------------

set nocount on;
set transaction isolation level read uncommitted;

declare @i int, @rec int, @batchId int, @total int

set @i = 1;
set @rec = 0;
set @total = 0;

select batchId, count(employeeId) as total, row_number() over(order by batchId asc) as rId
into #stats
from imd_user_db.MailBatchUserList
group by batchId

select @rec=count(rId) from #stats
begin try
begin transaction
while(@i <= @rec)
	begin
		select @batchId=batchId, @total=total from #stats where rId = @i
		update imd_user_db.MailBatch
		set NoOfUsers = isnull(@total,0),
			IsSent = 1
		where batchId = @batchId
		set @i = @i + 1
	end
commit transaction
end try
begin catch
	if(@@trancount > 0 )
		rollback transaction
end catch

drop table #stats
------------ End of Update Live Table Statistics ---------------