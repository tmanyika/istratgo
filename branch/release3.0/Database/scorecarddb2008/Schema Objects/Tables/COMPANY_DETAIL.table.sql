﻿CREATE TABLE [imd_user_db].[COMPANY_DETAIL] (
    [COMPANY_ID]      INT           IDENTITY (1, 1) NOT NULL,
    [ACCOUNT_NO]      INT           NULL,
    [COMPANY_NAME]    VARCHAR (150) COLLATE Latin1_General_CI_AS NULL,
    [REGISTRATION_NO] NCHAR (20)    COLLATE Latin1_General_CI_AS NULL,
    [INDUSTRY]        INT           NULL,
    [COUNTRY]         INT           NULL,
    [VAT_NO]          VARCHAR (50)  COLLATE Latin1_General_CI_AS NULL,
    [ADDRESS]         VARCHAR (150) COLLATE Latin1_General_CI_AS NULL,
    [CONTACT_NO]      VARCHAR (50)  COLLATE Latin1_General_CI_AS NULL,
    [ACTIVE]          BIT           NULL,
    [DATE_REGISTERED] DATETIME      NOT NULL,
    [PAID]            BIT           NOT NULL
);

