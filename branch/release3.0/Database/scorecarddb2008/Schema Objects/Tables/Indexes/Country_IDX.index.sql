﻿CREATE NONCLUSTERED INDEX [Country_IDX]
    ON [imd_user_db].[LeaveCalendar]([CountryId] ASC)
    INCLUDE([HolidayDateId], [HolidayDate], [Detail], [CalendarYear], [Active], [CreatedBy], [DateCreated], [DateUpdated], [UpdatedBy]) WITH (FILLFACTOR = 90, ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
    ON [PRIMARY];

