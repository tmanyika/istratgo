﻿CREATE TABLE [imd_user_db].[INDIVIDUAL_SCORES] (
    [ID]                  INT           IDENTITY (1, 1) NOT NULL,
    [CRITERIA_TYPE_ID]    INT           NULL,
    [SELECTED_ITEM_VALUE] INT           NULL,
    [RESPONSIBLE_USER_ID] INT           NULL,
    [GOAL_ID]             INT           NOT NULL,
    [SCORES]              VARCHAR (250) COLLATE Latin1_General_CI_AS NOT NULL,
    [FINAL_SCORE]         FLOAT         NOT NULL,
    [PERIOD_DATE]         DATETIME      NOT NULL,
    [DATE_CREATED]        DATETIME      NULL,
    [SUBMISSION_ID]       INT           NULL
);

