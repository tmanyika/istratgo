﻿CREATE TABLE [imd_user_db].[MailSentItem] (
    [SentItemId]   BIGINT         IDENTITY (1, 1) NOT NULL,
    [EmployeeId]   INT            NOT NULL,
    [FullName]     VARCHAR (100)  NULL,
    [Email]        VARCHAR (80)   NOT NULL,
    [BatchId]      INT            NOT NULL,
    [MailSubject]  VARCHAR (8000) NOT NULL,
    [MailContent]  NVARCHAR (MAX) NOT NULL,
    [MainTemplate] NVARCHAR (MAX) NOT NULL,
    [IsSent]       BIT            NOT NULL,
    [DateToBeSent] DATETIME       NULL
);

