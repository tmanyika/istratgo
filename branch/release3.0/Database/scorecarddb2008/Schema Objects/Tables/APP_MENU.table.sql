﻿CREATE TABLE [imd_user_db].[APP_MENU] (
    [MENU_ID]        INT           IDENTITY (1, 1) NOT NULL,
    [PARENT_MENU_ID] INT           NULL,
    [MENU_NAME]      VARCHAR (50)  COLLATE Latin1_General_CI_AS NULL,
    [IMAGE_ICON]     VARCHAR (150) COLLATE Latin1_General_CI_AS NULL,
    [MENU_PATH]      VARCHAR (50)  COLLATE Latin1_General_CI_AS NULL,
    [ORDER_ID]       INT           NULL,
    [POSITION]       SMALLINT      NULL,
    [ACTIVE]         BIT           NULL
);

