﻿CREATE TABLE [imd_user_db].[LeaveCalendarCompany] (
    [LinkedId]      BIGINT       IDENTITY (1, 1) NOT NULL,
    [HolidayDateId] BIGINT       NOT NULL,
    [OrgUnitId]     INT          NOT NULL,
    [Applied]       BIT          NOT NULL,
    [CreatedBy]     VARCHAR (20) NOT NULL,
    [DateCreated]   DATETIME     NOT NULL,
    [UpdatedBy]     VARCHAR (20) NULL,
    [DateUpdated]   DATETIME     NULL
);

