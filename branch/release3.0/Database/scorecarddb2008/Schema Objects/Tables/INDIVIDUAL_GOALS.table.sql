﻿CREATE TABLE [imd_user_db].[INDIVIDUAL_GOALS] (
    [ID]               INT           IDENTITY (1, 1) NOT NULL,
    [STRATEGIC_ID]     INT           NOT NULL,
    [AREA_TYPE_ID]     INT           NOT NULL,
    [AREA_ID]          INT           NOT NULL,
    [UNIT_MEASURE]     INT           NOT NULL,
    [TARGET]           VARCHAR (150) COLLATE Latin1_General_CI_AS NULL,
    [WEIGHT]           INT           NULL,
    [PERIOD_DATE]      DATETIME      NOT NULL,
    [DATE_CREATED]     DATETIME      NULL,
    [GOAL_DESCRIPTION] VARCHAR (250) COLLATE Latin1_General_CI_AS NULL
);

