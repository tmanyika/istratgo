﻿CREATE TABLE [imd_user_db].[ORG_JOB_TITLES] (
    [ID]           INT      IDENTITY (1, 1) NOT NULL,
    [ORG_UNIT_ID]  INT      NOT NULL,
    [JOB_TITLE_ID] INT      NOT NULL,
    [DATE_CREATED] DATETIME NOT NULL,
    [ACTIVE]       BIT      NULL
);

