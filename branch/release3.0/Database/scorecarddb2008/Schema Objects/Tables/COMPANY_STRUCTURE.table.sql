﻿CREATE TABLE [imd_user_db].[COMPANY_STRUCTURE] (
    [ID]              INT          IDENTITY (1, 1) NOT NULL,
    [COMPANY_ID]      INT          NOT NULL,
    [ORGUNIT_NAME]    VARCHAR (50) COLLATE Latin1_General_CI_AS NULL,
    [PARENT_ORG]      INT          NULL,
    [ORGUNT_TYPE]     INT          NULL,
    [DATE_CREATED]    DATETIME     NULL,
    [OWNER_ID]        INT          NULL,
    [DATE_REGISTERED] DATETIME     NOT NULL,
    [DATE_UPDATED]    DATETIME     NULL,
    [ACTIVE]          BIT          NOT NULL,
    [CREATED_BY]      VARCHAR (20) NULL,
    [UPDATED_BY]      VARCHAR (20) NULL
);

