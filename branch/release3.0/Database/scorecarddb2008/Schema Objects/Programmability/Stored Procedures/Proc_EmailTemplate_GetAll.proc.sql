﻿


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_EmailTemplate_GetAll]
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	select   MailId, TemplateId, MailName, MailSubject, MailContent, IsMasterTemplate, Active, CreatedBy, DateCreated, DateUpdated, UpdatedBy
	from     imd_user_db.MailTemplate
	order by MailName
END



