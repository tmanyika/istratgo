﻿CREATE PROCEDURE [imd_user_db].[PROC_GET_SCORES_REPORT_AGGREGATE] 
	@CRITERIA_TYPE_ID INT,
	@SubXml xml,
	@SCORE_DATE DATETIME
as
begin
	set transaction isolation level READ uncommitted;
	set nocount on;
	
	select	nref.value('Id[1]', 'int') as areaValueId
	into #vid
	from   @SubXml.nodes('//Record/Score') AS R(nref)
	if(@CRITERIA_TYPE_ID = 32) -- users
		select     isc.ID, isc.CRITERIA_TYPE_ID, isc.SELECTED_ITEM_VALUE, isc.GOAL_ID, isc.SCORES, isc.FINAL_SCORE, isc.DATE_CREATED, isc.SUBMISSION_ID, ig.WEIGHT, 
                      ig.TARGET, ig.GOAL_DESCRIPTION, isc.PERIOD_DATE, cp.PERSPECTIVE_NAME, cs.OBJECTIVE, u.TYPE_NAME AS UNIT_MEASURE, cp.PERSPECTIVE_ID, 
                      cp.COMPANY_ID, l.RoleId as USER_TYPE,ua.FirstName + ' ' + isnull(ua.MiddleName,'') + ' ' + ua.LastName as NAME, ua.EmployeeId AS USERID
		from    INDIVIDUAL_SCORES AS isc INNER JOIN
				  INDIVIDUAL_GOALS AS ig ON isc.GOAL_ID = ig.ID INNER JOIN
				  COMPANY_STRATEGIC AS cs ON ig.STRATEGIC_ID = cs.ID INNER JOIN
				  COMPANY_PERSPECTIVE AS cp ON cs.PERSPECTIVE = cp.PERSPECTIVE_ID INNER JOIN
				  Employee AS ua ON isc.SELECTED_ITEM_VALUE = ua.EmployeeId INNER JOIN
				  EmployeeLogin as l on ua.EmployeeId = l.EmployeeId  LEFT OUTER JOIN
				  UTILITIES AS u ON ig.UNIT_MEASURE = u.ID
		where     (isc.CRITERIA_TYPE_ID = @CRITERIA_TYPE_ID) AND (DATEDIFF(dd, @SCORE_DATE, isc.PERIOD_DATE) = 0) AND (ua.EmployeeId in (select areaValueId from #vid))
		order by ua.LastName, ua.FirstName 
	else if(@CRITERIA_TYPE_ID = 33) -- org unit
		select     isc.ID, isc.CRITERIA_TYPE_ID, isc.SELECTED_ITEM_VALUE, isc.GOAL_ID, isc.SCORES, isc.FINAL_SCORE, isc.DATE_CREATED, isc.SUBMISSION_ID, ig.WEIGHT, 
                      ig.TARGET, ig.GOAL_DESCRIPTION, isc.PERIOD_DATE, cp.PERSPECTIVE_NAME, cs.OBJECTIVE, u.TYPE_NAME AS UNIT_MEASURE, cp.PERSPECTIVE_ID, 
                      cp.COMPANY_ID, cst.ORGUNIT_NAME, cst.ID AS ORGUNIT_ID
		from         INDIVIDUAL_SCORES AS isc INNER JOIN
                      INDIVIDUAL_GOALS AS ig ON isc.GOAL_ID = ig.ID INNER JOIN
                      COMPANY_STRATEGIC AS cs ON ig.STRATEGIC_ID = cs.ID INNER JOIN
                      COMPANY_PERSPECTIVE AS cp ON cs.PERSPECTIVE = cp.PERSPECTIVE_ID INNER JOIN
                      COMPANY_STRUCTURE AS cst ON isc.SELECTED_ITEM_VALUE = cst.ID LEFT OUTER JOIN
                      UTILITIES AS u ON ig.UNIT_MEASURE = u.ID
		where     (isc.CRITERIA_TYPE_ID = @CRITERIA_TYPE_ID) AND (DATEDIFF(dd, @SCORE_DATE, isc.PERIOD_DATE) = 0) AND (cst.ID in (select areaValueId from #vid))
		order by cst.ORGUNIT_NAME
	else if(@CRITERIA_TYPE_ID = 49) -- Project
		select     isc.ID, isc.CRITERIA_TYPE_ID, isc.SELECTED_ITEM_VALUE, isc.GOAL_ID, isc.SCORES, isc.FINAL_SCORE, isc.DATE_CREATED, isc.SUBMISSION_ID, ig.WEIGHT, 
						  ig.TARGET, ig.GOAL_DESCRIPTION, isc.PERIOD_DATE, cp.PERSPECTIVE_NAME, cs.OBJECTIVE, u.TYPE_NAME AS UNIT_MEASURE, cp.PERSPECTIVE_ID, 
						  cp.COMPANY_ID, cmp.PROJECT_ID, cmp.PROJECT_NAME
		from         INDIVIDUAL_SCORES AS isc INNER JOIN
						  INDIVIDUAL_GOALS AS ig ON isc.GOAL_ID = ig.ID INNER JOIN
						  COMPANY_STRATEGIC AS cs ON ig.STRATEGIC_ID = cs.ID INNER JOIN
						  COMPANY_PERSPECTIVE AS cp ON cs.PERSPECTIVE = cp.PERSPECTIVE_ID INNER JOIN
						  COMPANY_PROJECT AS cmp ON isc.SELECTED_ITEM_VALUE = cmp.PROJECT_ID LEFT OUTER JOIN
						  UTILITIES AS u ON ig.UNIT_MEASURE = u.ID
		where     (isc.CRITERIA_TYPE_ID = @CRITERIA_TYPE_ID) AND (DATEDIFF(dd, @SCORE_DATE, isc.PERIOD_DATE) = 0) AND (cmp.PROJECT_ID in (select areaValueId from #vid))
		order by cmp.PROJECT_NAME
	--if(@CRITERIA_TYPE_ID = 32) -- users
	--	select     isc.ID, isc.CRITERIA_TYPE_ID, isc.SELECTED_ITEM_VALUE, isc.GOAL_ID, isc.SCORES, isc.FINAL_SCORE, isc.DATE_CREATED, isc.SUBMISSION_ID, ig.WEIGHT, 
 --                     ig.TARGET, ig.GOAL_DESCRIPTION, isc.PERIOD_DATE, cp.PERSPECTIVE_NAME, cs.OBJECTIVE, u.TYPE_NAME AS UNIT_MEASURE, cp.PERSPECTIVE_ID, 
 --                     cp.COMPANY_ID, ua.USER_TYPE, ua.NAME, ua.ID AS USERID
	--	from         INDIVIDUAL_SCORES AS isc INNER JOIN
	--						  INDIVIDUAL_GOALS AS ig ON isc.GOAL_ID = ig.ID INNER JOIN
	--						  COMPANY_STRATEGIC AS cs ON ig.STRATEGIC_ID = cs.ID INNER JOIN
	--						  COMPANY_PERSPECTIVE AS cp ON cs.PERSPECTIVE = cp.PERSPECTIVE_ID INNER JOIN
	--						  USER_ADMIN AS ua ON isc.SELECTED_ITEM_VALUE = ua.ID LEFT OUTER JOIN
	--						  UTILITIES AS u ON ig.UNIT_MEASURE = u.ID
	--	where     (isc.CRITERIA_TYPE_ID = @CRITERIA_TYPE_ID) AND (DATEDIFF(dd, @SCORE_DATE, isc.PERIOD_DATE) = 0) AND (ua.ID in (select areaValueId from #vid))
	--	order by ua.NAME 
		
	drop table #vid
end