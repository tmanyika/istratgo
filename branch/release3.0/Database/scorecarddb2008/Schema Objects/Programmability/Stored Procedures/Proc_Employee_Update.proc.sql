﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_Employee_Update] 
	 @EmployeeId int,
	 @LineManagerEmployeeId int = NULL,
     @OrgUnitId int, 
     @FirstName varchar(25), 
     @MiddleName varchar(25), 
     @LastName varchar(25), 
     @EmailAddress varchar(80),
     @JobTitleId int = NULL, 
     @StatusId int = NULL, 
     @EmployeeNo varchar(15), 
     @WorkingWeekId int = NULL, 
     @Cellphone varchar(15),
     @BusTelephone varchar(15),    
     @UpdatedBy varchar(20),
     @Active bit = 1,
     @RoleId int
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	declare @FullName varchar(100), @updated int,
			@EmailExist bit;
	
	set @updated = 0;
	set @EmailExist = 0;
	set @FullName = @FirstName + ' ' + @MiddleName + ' ' + @LastName;
	
	if ((exists(select emailaddress from imd_user_db.Employee where EmailAddress = @EmailAddress and EmployeeId <> @EmployeeId)) or (exists(select EMAIL_ADDRESS from imd_user_db.USER_ADMIN where EMAIL_ADDRESS = @EmailAddress and ID <> @EmployeeId)))	
		set @EmailExist = 1
	
	if(@EmailExist = 0)	
		begin	
			begin try
				begin transaction
					update imd_user_db.Employee
					set LineManagerEmployeeId = @LineManagerEmployeeId, OrgUnitId = @OrgUnitId, FirstName = @FirstName, MiddleName = @MiddleName, LastName = @LastName, 
						EmailAddress = @EmailAddress, JobTitleId = @JobTitleId, StatusId = @StatusId, EmployeeNo = @EmployeeNo, WorkingWeekId = @WorkingWeekId, 
						Cellphone = @Cellphone, BusTelephone = @BusTelephone, UpdatedBy = @UpdatedBy, DateUpdated = GETDATE()
					where EmployeeId = @EmployeeId
					select @updated=@@ROWCOUNT
			        
			        update imd_user_db.EmployeeLogin
			        set Active = @Active, RoleId = @RoleId
			        where EmployeeId = @EmployeeId
			        
					--update imd_user_db.USER_ADMIN
					--set NAME = @FullName, JOB_TITLE_ID = @JobTitleId, EMAIL_ADDRESS = @EmailAddress, 
					--	ORG_ID = @OrgUnitId, REPORTS_TO = @LineManagerEmployeeId, ACTIVE = @Active,
					--	USER_TYPE = @RoleId			
					--where ID = @EmployeeId
				commit transaction		
			end try
			begin catch
				if(@@TRANCOUNT > 0)
					begin
						set @updated = 0
						rollback transaction
					end
			end catch
		end 
	select @updated
END
