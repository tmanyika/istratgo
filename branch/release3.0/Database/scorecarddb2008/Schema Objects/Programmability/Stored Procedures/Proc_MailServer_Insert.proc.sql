﻿




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_MailServer_Insert] 
	@mailaccountId int,
	@accountName varchar(200),
	@sqlProfileName varchar(100),
	@user_Name varchar(100),
	@pin varchar(128),
	@smtpServer varchar(100),
	@portN int,
	@active bit, 
	@createdBy varchar(20)
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	declare @recCount int, @encPin varbinary(128);
	--declare @profileId int, @accountId int;
	
	open master key decryption by password = '3891@Macd'
	open symmetric key FieldSymmetricKey
	decryption by certificate FieldCertificate;
	
	set @recCount = 0;
	set @encPin = imd_user_db.fxEncrypt(@pin);
	
	---- get account id
	--select @accountId=account_id from msdb.dbo.sysmail_account where @accountName = name
	--select @profileId=profile_id from msdb.dbo.sysmail_profile where name = @sqlProfileName
	--select @accountId, @profileId
	--begin try
		--begin transaction
		  -- --Create database mail account.
		  -- if not (exists(select * from msdb.dbo.sysmail_account where @accountName = name))
			 --  exec msdb.dbo.sysmail_add_account_sp
				-- @account_name = @accountName
				--,@description = @accountName
				--,@email_address = @user_Name
				--,@replyto_address = @user_Name
				--,@display_name = @accountName
				--,@mailserver_name = @smtpServer
				--,@port = @portN
				--,@username = @user_Name
				--,@password = @pin
		  -- else
			 --   exec msdb.dbo.sysmail_update_account_sp
				--	 @account_id = @accountId
				--	,@account_name = @accountName
				--	,@description = @accountName
				--	,@email_address = @user_Name
				--	,@replyto_address = @user_Name
				--	,@display_name = @accountName
				--	,@mailserver_name = @smtpServer
				--	,@port = @portN
				--	,@username = @user_Name
				--	,@password = @pin
					
		  -- --Create global mail profile.
		  -- if not (exists(select * from msdb.dbo.sysmail_profile where @sqlProfileName = name))
				--exec msdb.dbo.sysmail_add_profile_sp
				--	@profile_name = @sqlProfileName
				--	,@description = @accountName					
		  -- else
				-- exec msdb.dbo.sysmail_update_profile_sp
				--  @profile_id = @profileId
				-- ,@profile_name = @sqlProfileName
				-- ,@description = @accountName
		  -- --Add the account to the profile.
		  -- if not (exists(select * from msdb.dbo.sysmail_profileaccount where profile_id = @profileId and account_id = @accountId))
				--exec msdb.dbo.sysmail_add_profileaccount_sp
				-- @profile_name = @sqlProfileName
				--,@account_name = @accountName
				--,@sequence_number=1		  
				
		  -- --grant access to the profile to all users in the msdb database
		  -- if not(exists(select * from msdb.dbo.sysmail_principalprofile where profile_id = @profileId)) 
			 --  exec msdb.dbo.sysmail_add_principalprofile_sp
				-- @profile_name = @sqlProfileName
				--,@principal_name = 'imd_user_db'
				--,@is_default = 0
			
	if (@mailaccountId <= 0)	
		insert into imd_user_db.MailServer(AccountName, SqlProfileName, UserName, PIN, SmtpServer, Port, Active, CreatedBy, DateCreated)
		values(@accountName, @sqlProfileName, @user_Name, @encPin, @smtpServer, @portN, @active, @createdBy, getdate())
	else
		update imd_user_db.MailServer
		set AccountName = @accountName, 
			SqlProfileName = @sqlProfileName,
			UserName = @user_Name,
			Port = @portN,
			PIN = @encPin,
			SmtpServer = @smtpServer,
			Active = @active, 
			UpdatedBy = @createdBy, 
			DateUpdated = GETDATE()
		where  (AccountMailId = @mailaccountId)
	select @recCount=@@rowcount	
		--commit transaction
	 -- end try
	 -- begin catch
		--if(@@TRANCOUNT > 0)
		--	begin
		--		 --insert into imd_user_db.ProcError(ErrorNo, ErrorSeverity, ErrorState, ErrorProcedure, ErrorLine, ErrorMessage)
		--		 --select ERROR_NUMBER() AS ErrorNumber
		--			--,ERROR_SEVERITY() AS ErrorSeverity
		--			--,ERROR_STATE() AS ErrorState
		--			--,ERROR_PROCEDURE() AS ErrorProcedure
		--			--,ERROR_LINE() AS ErrorLine
		--			--,ERROR_MESSAGE() AS ErrorMessage;
		--		set @recCount = -1
		--		rollback transaction				
		--	end
	 -- end catch
	select @recCount
END





