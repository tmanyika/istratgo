﻿
CREATE PROCEDURE  [imd_user_db].[PROC_ADDNEWSETTING]
@PARENT_ID INT,
@NAME VARCHAR(50),
@DESCRIPTION VARCHAR(50)
AS
INSERT INTO UTILITIES
(
  PARENT_ID,TYPE_NAME, DESCRIPTION
)
VALUES
(
@PARENT_ID,
@NAME,
@DESCRIPTION
)

