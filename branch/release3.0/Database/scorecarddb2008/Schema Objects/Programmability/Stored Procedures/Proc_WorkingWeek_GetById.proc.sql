﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_WorkingWeek_GetById]
	@workingWeekId int
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	select   WorkingWeekId, CompanyId, Name, NoOfDays, Active, CreatedBy, DateCreated, DateUpdated, UpdatedBy
	from     imd_user_db.WorkingWeek
	where     (@workingWeekId = WorkingWeekId)
END

