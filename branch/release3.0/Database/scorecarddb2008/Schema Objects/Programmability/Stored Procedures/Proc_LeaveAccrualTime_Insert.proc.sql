﻿

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_LeaveAccrualTime_Insert]
	@timeName varchar(150), 
	@timeValue smallint,
	@active bit, 
	@createdBy varchar(20)
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	insert into imd_user_db.LeaveAccrualTime( TimeName, TimeValue,  Active, CreatedBy, DateCreated)
	values(@timeName, @timeValue, @active, @createdBy, getdate())
		
	select @@rowcount
END


