﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_LeaveType_GetActive]
	@companyId int,
	@active bit
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	select   LeaveId, CompanyId, LeaveName, LeaveDescription, Active, CreatedBy, DateCreated, DateUpdated, UpdatedBy
	from     imd_user_db.LeaveType
	where     (CompanyId = @companyId and Active = @active)
	order by LeaveName
END
