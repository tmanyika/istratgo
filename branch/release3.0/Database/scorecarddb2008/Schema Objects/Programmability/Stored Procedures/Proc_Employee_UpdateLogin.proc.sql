﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [imd_user_db].[Proc_Employee_UpdateLogin] 
	 @Username varchar(20), 
	 @Active bit, 
	 @Pass varchar(250),
     @RoleId int, 
     @UpdatedBy varchar(20)
AS
BEGIN
	set nocount on;
	set transaction isolation level read uncommitted;
	
	declare @updated int;
	set @updated = 0;
	begin try
		begin transaction
			update imd_user_db.EmployeeLogin
			set Pass = @Pass, RoleId = @RoleId, Active = @Active,
				DateUpdated = getdate()
			where UserName = @Username
			select @updated=@@ROWCOUNT
	        
			--update imd_user_db.USER_ADMIN
			--set [PASSWORD] = @Pass, USER_TYPE = @RoleId, DATE_CREATED = getdate()	
			--where LOGIN_ID = @EmployeeId
		commit transaction		
	end try
	begin catch
		if(@@TRANCOUNT > 0)
			begin
				set @updated = 0
				rollback transaction
			end
	end catch 
	select @updated
END
