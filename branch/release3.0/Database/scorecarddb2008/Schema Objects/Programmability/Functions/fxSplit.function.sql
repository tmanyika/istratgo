﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE function [imd_user_db].[fxSplit]
(     
	  @Id varchar(12),     
      @string varchar(200),  -- Variable for string
      @delimiter varchar(50) -- Delimiter in the string
)
returns @table table(        --Return type of the function
		dataVal varchar(25),
		Id varchar(12),
		orderId int identity(1,1)
)
begin
     declare @Xml as xml 
-- Replace the delimiter to the opeing and closing tag
--to make it an xml document
     set @Xml = cast(('<A>'+replace(@String,@delimiter,'</A><A>')+'</A>') as xml) 
--Query this xml document via xquery to split rows
--and insert it into table to return.
     insert into @table(dataVal,Id)
     select A.value('.', 'varchar(25)') as [Column], @Id as Id from @Xml.nodes('A') AS FN(A) 
return
end
