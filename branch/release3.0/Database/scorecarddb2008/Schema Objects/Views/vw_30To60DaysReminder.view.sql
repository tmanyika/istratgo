﻿CREATE VIEW imd_user_db.vw_30To60DaysReminder
AS
SELECT     e.EmployeeId, e.FirstName + ' ' + ISNULL(e.MiddleName, '') + ' ' + e.LastName AS FullName, e.EmailAddress AS Email, CASE WHEN datediff(dd, 
                      isnull(cd.DATE_REGISTERED, c.DATE_CREATED), getdate()) >= 30 AND datediff(dd, isnull(cd.DATE_REGISTERED, c.DATE_CREATED), getdate()) 
                      < 60 THEN '2' WHEN datediff(dd, isnull(cd.DATE_REGISTERED, c.DATE_CREATED), getdate()) >= 60 THEN '1' END AS Months
FROM         imd_user_db.COMPANY_STRUCTURE AS c WITH (nolock) INNER JOIN
                      imd_user_db.Employee AS e WITH (nolock) ON c.ID = e.OrgUnitId INNER JOIN
                      imd_user_db.EmployeeLogin AS l WITH (nolock) ON e.EmployeeId = l.EmployeeId INNER JOIN
                      imd_user_db.COMPANY_DETAIL AS cd WITH (nolock) ON c.COMPANY_ID = cd.COMPANY_ID
WHERE     (DATEDIFF(dd, ISNULL(c.DATE_REGISTERED, c.DATE_CREATED), GETDATE()) BETWEEN 30 AND 60) AND (l.RoleId = 44) AND (cd.PAID = 0)

GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[15] 4[39] 2[29] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -96
         Left = 0
      End
      Begin Tables = 
         Begin Table = "c"
            Begin Extent = 
               Top = 0
               Left = 170
               Bottom = 240
               Right = 353
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "e"
            Begin Extent = 
               Top = 2
               Left = 379
               Bottom = 121
               Right = 585
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "l"
            Begin Extent = 
               Top = 6
               Left = 647
               Bottom = 125
               Right = 807
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cd"
            Begin Extent = 
               Top = 137
               Left = 460
               Bottom = 256
               Right = 645
            End
            DisplayFlags = 280
            TopColumn = 8
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End', @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'VIEW', @level1name = N'vw_30To60DaysReminder';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'imd_user_db', @level1type = N'VIEW', @level1name = N'vw_30To60DaysReminder';

