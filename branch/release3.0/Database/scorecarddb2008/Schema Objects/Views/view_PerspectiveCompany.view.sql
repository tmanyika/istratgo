﻿CREATE VIEW imd_user_db.view_PerspectiveCompany
AS
SELECT     COMPANY_ID
FROM         imd_user_db.COMPANY_DETAIL WITH (nolock)
WHERE     (NOT (COMPANY_ID IN
                          (SELECT     COMPANY_ID
                            FROM          imd_user_db.COMPANY_PERSPECTIVE WITH (nolock))))
