 <asp:Menu ID="NavigationMenu" runat="server" CssClass="menu" EnableViewState="false"
                    IncludeStyleBlock="false" Orientation="Horizontal">
                    <Items>
                        <asp:MenuItem NavigateUrl="~/Register.aspx" Text="Register" Value="1,2" />
                        <asp:MenuItem NavigateUrl="~/Buy.aspx" Text="Buy" Value="1,2" />
                        <asp:MenuItem NavigateUrl="~/Pay.aspx" Text="Pay" Value="1" />
                        <asp:MenuItem NavigateUrl="~/Claim.aspx" Text="Claim" Value="1" />
                        <asp:MenuItem NavigateUrl="~/ResetPin.aspx" Text="Reset PIN" Value="1,2" />
                        <asp:MenuItem NavigateUrl="~/Documentation.aspx" Text="Technical Documentation" Value="1,2"/>
                    </Items>
                </asp:Menu>