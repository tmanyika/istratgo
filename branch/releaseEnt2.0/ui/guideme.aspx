﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="guideme.aspx.cs" Inherits="scorecard.ui.guideme" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <title>iStratgo :: Guide Me</title>
    <style type="text/css">
        label
        {
            white-space: nowrap !important;
        }
        
        #GuideDiv
        {
            float: left;
            width: 1360px;
            margin: 0px auto;
            text-align: center;
            font-weight: normal;
            font-size: 36px;
            line-height: 36px;
            color: #003366;
        }
    </style>
    <link href="css/welcomesite.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManagerMain" runat="server" CombineScripts="True"
        EnablePartialRendering="true" EnablePageMethods="true" />
    <div id="DoNotShow">
        <asp:UpdatePanel ID="UpdatePanelWelcome" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:UpdateProgress ID="UpdateProgressWelcome" runat="server" AssociatedUpdatePanelID="UpdatePanelWelcome">
                    <ProgressTemplate>
                        <div>
                            <asp:Image ID="imgProj" runat="server" ImageUrl="~/ui/images/icons/activity.gif"
                                AlternateText="Please wait.."></asp:Image></div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
                <asp:CheckBox ID="ChkShowAgain" runat="server" AutoPostBack="true" Checked="false"
                    OnCheckedChanged="ChkShowAgain_CheckedChanged" Text="Do not show this again"
                    TextAlign="Left" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
     <div id="GuideDiv">
         <iframe frameborder="0" height="700px" width="1360px" marginheight="0" scrolling="no"
        src="startguide.htm"></iframe>
    </div>
    </form>
</body>
</html>
