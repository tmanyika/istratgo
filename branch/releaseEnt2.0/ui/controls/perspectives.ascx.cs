﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.implementations;
using scorecard.controllers;
using scorecard.entities;

namespace scorecard.ui.controls
{
    public partial class perspectives : System.Web.UI.UserControl
    {
        #region Properties

        int compId
        {
            get { return Util.user.CompanyId; }
        }

        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) initialisePage();
        }


        #endregion

        #region Utility Methods

        void initialisePage()
        {
            loadPerspectives();
        }

        void loadPerspectives()
        {
            var dataSource = new perspectivemanagement(new perspectiveImpl()).get(compId, true);
            BindControl.BindListView(lstPerspective, dataSource);
            if (dataSource.Count <= pager.PageSize) pager.Visible = false;
        }

        #endregion

        #region Button Event Handling


        protected void lnkAdd_Click(object sender, EventArgs e)
        {
            lstPerspective.InsertItemPosition = InsertItemPosition.LastItem;
            loadPerspectives();
        }

        protected void lnkCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("index.aspx");
        }

        #endregion

        #region Utility

        public string GetBoolean(object boolVal)
        {
            if (boolVal == DBNull.Value) return "Unknown";
            return bool.Parse(boolVal.ToString()) ? "Yes" : "No";
        }

        private void cancelUpdate()
        {
            lstPerspective.EditIndex = -1;
            lstPerspective.InsertItemPosition = InsertItemPosition.None;
            loadPerspectives();
        }

        void deleteItem(ListViewCommandEventArgs e)
        {
            int pid = int.Parse((e.Item.FindControl("hdnperspectiveId") as HiddenField).Value);
            string updatedBy = Util.user.FullName;
            perspective obj = new perspective
            {
                PERSPECTIVE_ID = pid,
                ACTIVE = false,
                UPDATE_BY = updatedBy
            };

            bool deleted = new perspectivemanagement(new perspectiveImpl()).setStatus(obj);
            if (deleted) loadPerspectives();
            string msg = deleted ? Messages.GetDeleteMessage() : Messages.GetDeleteFailedMessage();
            BindControl.BindLiteral(lblMsg, msg);
        }

        void updateItem(ListViewCommandEventArgs e)
        {
            try
            {
                int pid = int.Parse((e.Item.FindControl("hdnperspectiveIdE") as HiddenField).Value);
                bool active = (e.Item.FindControl("chkActive") as CheckBox).Checked;

                string desc = (e.Item.FindControl("txtDesc") as TextBox).Text;
                string name = (e.Item.FindControl("txtName") as TextBox).Text;
                string updatedBy = Util.user.LoginId;
                string userName = Util.user.FullName;

                if (!isDataValid(name))
                {
                    lblMsg.Text = "Key Focus Area is required.";
                    return;
                }

                perspective objp = new perspective
                {
                    COMPANY_ID = compId,
                    PERSPECTIVE_NAME = name,
                    PERSPECTIVE_DESC = desc,
                    PERSPECTIVE_ID = pid,
                    CREATED_BY = userName,
                    UPDATE_BY = userName,
                    ACTIVE = active
                };

                if (compId > 0)
                {
                    perspectivemanagement obj = new perspectivemanagement(new perspectiveImpl());
                    bool saved = obj.update(objp);
                    string msg = saved ? Messages.GetUpdateMessage() : Messages.GetUpdateFailedMessage();
                    BindControl.BindLiteral(lblMsg, msg);
                    if (saved)
                    {
                        lstPerspective.EditIndex = -1;
                        loadPerspectives();
                    }
                }
                else BindControl.BindLiteral(lblMsg, Messages.GetSaveFailedMessage());
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        bool isDataValid(string name)
        {
            return string.IsNullOrEmpty(name) ? false : true;
        }

        void addItem(ListViewCommandEventArgs e)
        {
            try
            {
                bool active = (e.Item.FindControl("chkActivei") as CheckBox).Checked;
                string desc = (e.Item.FindControl("txtDesci") as TextBox).Text;
                string name = (e.Item.FindControl("txtNamei") as TextBox).Text;
                string updatedBy = Util.user.LoginId;
                string userName = Util.user.FullName;

                if (!isDataValid(name))
                {
                    lblMsg.Text = "Key focus area is required.";
                    return;
                }

                perspective objp = new perspective
                {
                    COMPANY_ID = compId,
                    PERSPECTIVE_NAME = name,
                    PERSPECTIVE_DESC = desc,
                    PERSPECTIVE_ID = 0,
                    CREATED_BY = userName,
                    UPDATE_BY = userName,
                    ACTIVE = active
                };

                if (compId > 0)
                {
                    perspectivemanagement obj = new perspectivemanagement(new perspectiveImpl());
                    bool saved = obj.add(objp);
                    string msg = saved ? Messages.GetSaveMessage() : Messages.GetSaveFailedMessage();
                    BindControl.BindLiteral(lblMsg, msg);
                    if (saved)
                    {
                        lstPerspective.InsertItemPosition = InsertItemPosition.None;
                        loadPerspectives();
                    }
                }
                else BindControl.BindLiteral(lblMsg, Messages.GetSaveFailedMessage());
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        #endregion

        #region ListView Event Handling Methods including Slide Paging

        protected void lstPerspective_PagePropertiesChanged(object sender, EventArgs e)
        {
            loadPerspectives();
        }

        protected void lstPerspective_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            try
            {
                lstPerspective.EditIndex = e.NewEditIndex;
                lstPerspective.InsertItemPosition = InsertItemPosition.None;
                loadPerspectives();
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        protected void lstPerspective_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListViewItemType.InsertItem ||
               e.Item.ItemType == ListViewItemType.DataItem)
                {
                    switch (e.CommandName)
                    {
                        case "addPerspective":
                            addItem(e);
                            break;
                        case "updatePerspective":
                            updateItem(e);
                            break;
                        case "deleteItem":
                            deleteItem(e);
                            break;
                        case "cancelUpdate":
                            cancelUpdate();
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        protected void pager_PreRender(object sender, EventArgs e)
        {
            if (lstPerspective.EditIndex == -1)
                loadPerspectives();
        }

        #endregion

    }
}