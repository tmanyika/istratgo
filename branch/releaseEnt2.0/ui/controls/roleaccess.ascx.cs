﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using scorecard.controllers;
using scorecard.implementations;
using scorecard.entities;


namespace scorecard.ui.controls
{
    public partial class roleaccess : System.Web.UI.UserControl
    {
        menucontroller accessManager;

        public roleaccess()
        {
            accessManager = new menucontroller(new rolebasedImpl());
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadMenus();
                loadRoles();
                selectSelectedAccess();
            }
        }

        void loadMenus()
        {
            try
            {
                var menus = new object(); ;
                bool canaccess = Util.canAccessSecurity(Util.user.CompanyId.ToString());
                if (!canaccess) menus = accessManager.getAllMenus().Where(a => a.ACTIVE == true && a.MENU_ID != Util.getSystemSettingsMenuId() && a.PARENT_MENU_ID != Util.getSystemSettingsMenuId());
                //accessManager.getAllMenus().Where(a => a.ACTIVE = true && a.MENU_ID != Util.getSystemSettingsMenuId()).Where(a => a.PARENT_MENU_ID != Util.getSystemSettingsMenuId());
                else menus = accessManager.getAllMenus().Where(a => a.ACTIVE == true);
                lstMenus.DataSource = menus;
                lstMenus.DataValueField = "MENU_ID";
                lstMenus.DataTextField = "MENU_NAME";
                lstMenus.DataBind();
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void loadRoles()
        {
            try
            {
                var roles = accessManager.appsettings.getChildSettings(new applicationsettings { PARENT_ID = Util.getTypeDefinitionsSystemUserTypesParentId() });
                ddlUserRoles.DataSource = roles;
                ddlUserRoles.DataValueField = "TYPE_ID";
                ddlUserRoles.DataTextField = "NAME";
                ddlUserRoles.DataBind();
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void selectSelectedAccess()
        {
            try
            {
                var menuAccess = accessManager.getAccessByCompany(new company { COMPANY_ID = Util.user.CompanyId }).Where(a => a.USER_TYPE_ID == int.Parse(ddlUserRoles.SelectedValue));
                for (int y = 0; y < lstMenus.Items.Count; y++)
                {
                    lstMenus.Items[y].Selected = false;
                    for (int i = 0; i < menuAccess.Count(); i++)
                    {
                        if (lstMenus.Items[y].Value == menuAccess.ElementAt(i).MENU_ID.ToString())
                        {
                            lstMenus.Items[y].Selected = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            createAccess();
        }

        void createAccess()
        {
            try
            {
                var selectedRole = int.Parse(ddlUserRoles.SelectedValue);
                var result = accessManager.deleteAccessByCompany(new menuaccess { COMPANY_ID = Util.user.CompanyId, USER_TYPE_ID = selectedRole });
                for (int i = 0; i < lstMenus.Items.Count; i++)
                {
                    if (lstMenus.Items[i].Selected)
                    {
                        accessManager.createAccess(new menuaccess
                        {
                            USER_TYPE_ID = selectedRole,
                            COMPANY_ID = Util.user.CompanyId,
                            MENU_ID = int.Parse(lstMenus.Items[i].Value)
                        });
                    }
                }
                pnlNoData.Visible = true;
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        protected void ddlUserRoles_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectSelectedAccess();
        }
    }
}