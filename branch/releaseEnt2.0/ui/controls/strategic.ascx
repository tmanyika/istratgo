﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="strategic.ascx.cs" Inherits="scorecard.ui.controls.strategic" %>
<%@ Import Namespace="scorecard.implementations" %>
<asp:UpdatePanel ID="UpdatePanelStrat" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <h1>
            &nbsp;Key Perfomance Deliverable Management
        </h1>
        <asp:ListView ID="lvStategicObjectives" runat="server" OnItemCommand="lvStategicObjectives_ItemCommand"
            OnItemEditing="lvStategicObjectives_ItemEditing">
            <LayoutTemplate>
                <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                    <thead>
                        <tr>
                            <td align="left" width="250px">
                                Strategic Objective
                            </td>
                            <td align="left" width="350px">
                                Key Perfomance Deliverable
                            </td>
                            <td align="left" width="500px">
                                Rationale
                            </td>
                            <td width="70px">
                                Edit
                            </td>
                            <td width="70px">
                                Delete
                            </td>
                        </tr>
                        <tr id="ItemPlaceHolder" runat="server">
                        </tr>
                    </thead>
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tbody>
                    <tr class="odd">
                        <td valign="top" align="left">
                            <asp:Label ID="lblID" runat="server" Visible="false" Text='<%# Eval("ID") %>' />
                            <%# getTypeName(int.Parse(Eval("PERSPECTIVE").ToString()))%>
                        </td>
                        <td valign="top" align="left">
                            <%# Eval("OBJECTIVE")%>
                        </td>
                        <td valign="top" align="left">
                            <%# Eval("RATIONALE")%>
                        </td>
                        <td valign="middle">
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkEdit" CommandName="Edit" class="ui-state-default ui-corner-all"
                                    title="Edit Record"><span class="ui-icon ui-icon-pencil">
                        </span></asp:LinkButton>
                            </ul>
                        </td>
                        <td valign="middle">
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkDelete" CommandName="deleteStategicObjective"
                                    class="ui-state-default ui-corner-all" title="Delete Record" OnClientClick="return confirmDelete();"><span class="ui-icon ui-icon-trash">
                        </span></asp:LinkButton>
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </ItemTemplate>
            <EditItemTemplate>
                <tbody>
                    <tr class="odd">
                        <td align="left" valign="top" align="left">
                            <asp:DropDownList ID="ddlEditPerspective" runat="server" />
                            <asp:Label ID="lblID" runat="server" Visible="false" Text='<%# Eval("ID") %>' />
                            <asp:HiddenField ID="hdnPerspectiveId" runat="server" Value='<%# Bind("PERSPECTIVE") %>' />
                        </td>
                        <td align="left" valign="top">
                            <asp:TextBox runat="server" ID="txtEditObjectiveName" Text='<%# Eval("OBJECTIVE") %>'
                                Width="240px" ValidationGroup="Edit" />
                            <asp:RequiredFieldValidator ID="RqdInfo" runat="server" ErrorMessage="* - required"
                                ValidationGroup="Edit" ControlToValidate="txtEditObjectiveName" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" valign="top">
                            <asp:TextBox runat="server" ID="txtEditRationale" Text='<%# Eval("RATIONALE") %>'
                                Width="240px" ValidationGroup="Edit" />
                        </td>
                        <td valign="middle">
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkSave" CommandName="updateStategicObjective"
                                    ValidationGroup="Edit" class="ui-state-default ui-corner-all" title="Update Record"><span class="ui-icon ui-icon-disk">
                        </span></asp:LinkButton>
                            </ul>
                        </td>
                        <td valign="middle">
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkCancelUpdate" CommandName="cancelUpdate" class="ui-state-default ui-corner-all"
                                    title="Cancel Update"><span class="ui-icon ui-icon-cancel">
                        </span></asp:LinkButton>
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </EditItemTemplate>
            <InsertItemTemplate>
                <tbody>
                    <tr>
                        <td align="left" valign="top">
                            <asp:DropDownList ID="ddlPerspective" runat="server" Width="200px" />
                        </td>
                        <td align="left" valign="top">
                            <asp:TextBox runat="server" ID="txtObjectiveName" Width="240px" ValidationGroup="Insert" />
                            <asp:RequiredFieldValidator ID="RqdInfo1" runat="server" ErrorMessage="* - required"
                                ValidationGroup="Insert" ControlToValidate="txtObjectiveName" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" valign="top">
                            <asp:TextBox runat="server" ID="txtRationale" Width="240px" ValidationGroup="Insert" />
                        </td>
                        <td valign="middle">
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkSave" ValidationGroup="Insert" class="ui-state-default ui-corner-all"
                                    title="Save this Record" CommandName="addNewStategicObjective"><span class="ui-icon ui-icon-disk">
                        </span></asp:LinkButton>
                            </ul>
                        </td>
                        <td valign="middle">
                            <ul id="icons">
                                <asp:LinkButton runat="server" ID="lnkCancel" class="ui-state-default ui-corner-all"
                                    title="Cancel Save " CommandName="cancelUpdate"><span class="ui-icon ui-icon-cancel">
                        </span></asp:LinkButton>
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </InsertItemTemplate>
        </asp:ListView>
        <br />
        <asp:DataPager ID="dtPagerStategicObjectives" runat="server" PagedControlID="lvStategicObjectives"
            PageSize="10" OnPreRender="dtPagerStategicObjectives_PreRender">
            <Fields>
                <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True" ShowNextPageButton="False"
                    ShowPreviousPageButton="False" />
                <asp:NumericPagerField />
                <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True" ShowNextPageButton="False"
                    ShowPreviousPageButton="False" />
            </Fields>
        </asp:DataPager>
        <br />
        <br />
        <asp:Label ID="lblMsg" runat="server" Style="color: #FF3300" />
        <br />
        <div id="tabs-3">
            <p>
                <asp:Button ID="btnNew" runat="server" CssClass="button" Text="Add New Key Perfomance Deliverable"
                    OnClick="btnNew_Click" />
                <asp:Button ID="lnkCancel0" runat="server" CssClass="button" Text="Done" OnClick="lnkCancel_Click"
                    CausesValidation="false" />
            </p>
        </div>
        <p>
            &nbsp;</p>
        <asp:UpdateProgress ID="UpdateProgressStrategy" runat="server" AssociatedUpdatePanelID="UpdatePanelStrat">
            <ProgressTemplate>
                <div>
                    <asp:Image ID="imgOrg" runat="server" ImageUrl="~/ui/images/icons/activity.gif" AlternateText="Please wait..">
                    </asp:Image></div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
