﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="mailTemplates.ascx.cs"
    Inherits="scorecard.ui.controls.mailTemplates" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<h1>
    Mail Templates Administration</h1>
<asp:UpdatePanel ID="UpdatePanelData" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:MultiView ID="mainView" runat="server" ActiveViewIndex="0">
            <asp:View ID="vwData" runat="server">
                <div>
                    <asp:ListView ID="lstData" runat="server" OnPagePropertiesChanged="lstData_PagePropertiesChanged"
                        OnItemEditing="lstData_ItemEditing" OnItemCommand="lstData_ItemCommand">
                        <LayoutTemplate>
                            <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                                <thead>
                                    <tr>
                                        <td align="right">
                                            Mail ID
                                        </td>
                                        <td>
                                            Mail Name
                                        </td>
                                        <td>
                                            Mail Subject
                                        </td>
                                        <td>
                                            Master Template
                                        </td>
                                        <td>
                                            Active
                                        </td>
                                        <td>
                                            Edit
                                        </td>
                                        <td>
                                            Deactivate
                                        </td>
                                    </tr>
                                    <tr id="itemPlaceholder" runat="server">
                                    </tr>
                                </thead>
                            </table>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tbody>
                                <tr class="odd">
                                    <td align="right">
                                        <%# Eval("MailId") %>
                                        <asp:HiddenField ID="hdnMailId" runat="server" Value='<%# Eval("MailId") %>' />
                                        <asp:HiddenField ID="hdnTid" runat="server" Value='<%# Eval("TemplateId") %>' />
                                    </td>
                                    <td>
                                        <%# Eval("MailName")%>
                                    </td>
                                    <td>
                                        <%# Eval("MailSubject")%>
                                    </td>
                                    <td>
                                        <asp:Literal ID="lblMaster" runat="server" Text='<%# GetBoolean(Eval("IsMasterTemplate")) %>' />
                                    </td>
                                    <td>
                                        <asp:Literal ID="lblActive" runat="server" Text='<%# GetBoolean(Eval("Active")) %>' />
                                    </td>
                                    <td>
                                        <ul id="icons">
                                            <asp:LinkButton runat="server" ID="lnkEdit" CommandName="Edit" class="ui-state-default ui-corner-all"
                                                title="Edit Record" CausesValidation="false" CommandArgument='<%# Eval("MailId") %>'><span class="ui-icon ui-icon-pencil">
                                        </span>
                                            </asp:LinkButton>
                                        </ul>
                                    </td>
                                    <td>
                                        <ul id="icons">
                                            <asp:LinkButton runat="server" ID="lnkDelete" CommandName="DeleteItem" class="ui-state-default ui-corner-all"
                                                title="Delete Record" OnClientClick="return confirmDelete();" CausesValidation="false"><span class="ui-icon ui-icon-trash">
                                                </span>
                                            </asp:LinkButton>
                                        </ul>
                                    </td>
                                </tr>
                            </tbody>
                        </ItemTemplate>
                        <EmptyDataTemplate>
                            <p class="errorMsg">
                                There are no records to display.
                            </p>
                        </EmptyDataTemplate>
                    </asp:ListView>
                    <br />
                    <asp:DataPager ID="pager" runat="server" PagedControlID="lstData" PageSize="15">
                        <Fields>
                            <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True" ShowNextPageButton="False"
                                ShowPreviousPageButton="False" />
                            <asp:NumericPagerField />
                            <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True" ShowNextPageButton="False"
                                ShowPreviousPageButton="False" />
                        </Fields>
                    </asp:DataPager>
                    <br />
                    <br />
                </div>
                <div>
                    <p class="errorMsg">
                        <br />
                        <asp:Literal ID="lblMsg" runat="server"></asp:Literal>
                        <br />
                    </p>
                </div>
                <div id="tabs-3">
                    <p>
                        <asp:Button ID="btnNew" runat="server" class="button" Text="Add New Template" OnClick="lnkAdd_Click"
                            CausesValidation="False" />
                        <asp:Button ID="lnkCancel0" runat="server" class="button" Text="Done" CausesValidation="false"
                            PostBackUrl="~/ui/index.aspx" />
                    </p>
                </div>
            </asp:View>
            <asp:View ID="vwForm" runat="server">
                <div>
                    <asp:Panel ID="pnlNewMail" runat="server" GroupingText="Manage Mail Template">
                        <div>
                            <p>
                                <asp:HiddenField ID="hdnMailIdE" runat="server" />
                                <br />
                            </p>
                        </div>
                        <div>
                            <div class="emailIndent">
                                <label for="sf">
                                    Mail Name :
                                </label>
                            </div>
                            <div>
                                <br />
                            </div>
                            <div class="emailIndent">
                                <span class="field_desc">
                                    <asp:TextBox ID="txtName" runat="server" CssClass="mf" ValidationGroup="Add" Width="600px"></asp:TextBox></span>
                                <asp:RequiredFieldValidator ID="rqdVd" runat="server" ControlToValidate="txtName"
                                    ValidationGroup="Add" Display="None" ErrorMessage="Mail Name"></asp:RequiredFieldValidator>
                            </div>
                            <div>
                                <br />
                            </div>
                            <div class="emailIndent">
                                <label class="lf">
                                    Select Master Template :
                                </label>
                            </div>
                            <div>
                                <br />
                            </div>
                            <div class="emailIndent">
                                <asp:DropDownList ID="ddMail" runat="server">
                                </asp:DropDownList>
                            </div>
                            <div>
                                <br />
                            </div>
                            <div class="emailIndent">
                                <label for="sf">
                                    Mail Subject :
                                </label>
                            </div>
                            <div>
                                <br />
                            </div>
                            <div class="emailIndent">
                                <span class="field_desc">
                                    <asp:TextBox ID="txtSubject" runat="server" CssClass="mf" ValidationGroup="Add" Width="600px"></asp:TextBox></span>
                                </span><asp:RequiredFieldValidator ID="rdqSubj" runat="server" ControlToValidate="txtSubject"
                                    ValidationGroup="Add" Display="None" ErrorMessage="Mail Name"></asp:RequiredFieldValidator>
                            </div>
                            <div>
                                <br />
                            </div>
                            <div class="emailIndent">
                                <label for="sf">
                                    Mail Content :
                                </label>
                            </div>
                            <div>
                                <br />
                            </div>
                            <div class="emailIndent">
                                <span class="field_desc">
                                    <telerik:RadEditor ID="rdContent" runat="server">
                                        <CssFiles>
                                            <telerik:EditorCssFile Value="" />
                                        </CssFiles>
                                    </telerik:RadEditor>
                                </span>
                            </div>
                        </div>
                        <div>
                            <br />
                        </div>
                        <div class="emailIndent">
                            <p>
                                <label for="sf">
                                    Is Master Template :
                                </label>
                                <span class="field_desc">
                                    <asp:CheckBox ID="chkMaster" runat="server" />
                                </span>
                            </p>
                        </div>
                        <div class="emailIndent">
                            <p>
                                <label for="sf">
                                    Active :
                                </label>
                                <span class="field_desc">
                                    <asp:CheckBox ID="chkActive" runat="server" />
                                </span>
                            </p>
                        </div>
                    </asp:Panel>
                </div>
                <div>
                    <p class="errorMsg">
                        <br />
                        <asp:Literal ID="lblError" runat="server"></asp:Literal>
                        <br />
                        <br />
                    </p>
                    <p>
                        <asp:Button ID="lnkSaveMail" runat="server" CssClass="button" OnClick="lnkSaveMail_Click"
                            Text="Save" ValidationGroup="Add" />
                        <asp:Button ID="lnkUpdateMail" runat="server" CssClass="button" Text="Update" Visible="False"
                            ValidationGroup="Add" OnClick="lnkUpdateMail_Click" />
                        <asp:Button ID="lnkCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="false"
                            OnClick="lnkCancel_Click" />
                        <asp:ValidationSummary ID="vdSummary" runat="server" HeaderText="Provide The Following Fields"
                            ShowMessageBox="True" ShowSummary="False" ValidationGroup="Add" ForeColor="Red" />
                    </p>
                </div>
                </div>
            </asp:View>
        </asp:MultiView>
        <asp:UpdateProgress ID="UpdateProgressData" runat="server" AssociatedUpdatePanelID="UpdatePanelData">
            <ProgressTemplate>
                <div>
                    <asp:Image ID="imgProj" runat="server" ImageUrl="~/ui/images/icons/activity.gif"
                        AlternateText="Please wait.."></asp:Image></div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
