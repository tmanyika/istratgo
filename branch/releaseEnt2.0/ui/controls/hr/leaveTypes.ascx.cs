﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.implementations;
using HR.Leave;

namespace scorecard.ui.controls
{
    public partial class leaveTypes : System.Web.UI.UserControl
    {
        #region Properties

        int CompId
        {
            get { return Util.user.CompanyId; }
        }

        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) InitialisePage();
        }

        #endregion

        #region Utility Methods

        void InitialisePage()
        {
            LoadLeaveTypes();
        }

        void LoadLeaveTypes()
        {
            ILeaveType obj = new LeaveTypeBL();
            BindControl.BindListView(lstLeaveType, obj.GetByStatus(CompId, true));
            pager.Visible = (lstLeaveType.Items.Count <= pager.PageSize) ? false : true;
        }

        #endregion

        #region Button Event Handling

        protected void lnkAdd_Click(object sender, EventArgs e)
        {
            BindControl.BindLiteral(lblMsg, "");
            lstLeaveType.InsertItemPosition = InsertItemPosition.LastItem;
            LoadLeaveTypes();
        }

        protected void lnkCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("index.aspx", false);
        }

        #endregion

        #region Utility

        public string GetBoolean(object boolVal)
        {
            if (boolVal == DBNull.Value) return "Yes";
            return bool.Parse(boolVal.ToString()) ? "Yes" : "No";
        }

        private void CancelUpdate()
        {
            lstLeaveType.EditIndex = -1;
            lstLeaveType.InsertItemPosition = InsertItemPosition.None;
            LoadLeaveTypes();
        }

        void DeleteItem(ListViewCommandEventArgs e)
        {
            int leaveId = int.Parse((e.Item.FindControl("hdnLeaveId") as HiddenField).Value);
            string updatedBy = Util.user.LoginId;

            ILeaveType obj = new LeaveTypeBL();
            bool deleted = obj.Delete(leaveId, updatedBy);
            if (deleted) LoadLeaveTypes();
            string msg = deleted ? Messages.GetDeleteMessage() : Messages.GetDeleteFailedMessage();
            BindControl.BindLiteral(lblMsg, msg);
        }

        void UpdateItem(ListViewCommandEventArgs e)
        {
            try
            {
                int leaveId = int.Parse((e.Item.FindControl("hdnLeaveIdE") as HiddenField).Value);
                bool active = (e.Item.FindControl("chkActive") as CheckBox).Checked;

                string desc = (e.Item.FindControl("txtDesc") as TextBox).Text;
                string name = (e.Item.FindControl("txtName") as TextBox).Text;
                string updatedBy = Util.user.LoginId;

                LeaveType obj = new LeaveType
                {
                    CompanyId = CompId,
                    LeaveName = name,
                    Description = desc,
                    LeaveId = leaveId,
                    CreatedBy = updatedBy,
                    UpdatedBy = updatedBy,
                    Active = active
                };

                if (CompId > 0)
                {
                    ILeaveType objl = new LeaveTypeBL();
                    bool saved = objl.UpdateLeaveType(obj);
                    string msg = saved ? Messages.GetUpdateMessage() : Messages.GetUpdateFailedMessage();
                    BindControl.BindLiteral(lblMsg, msg);
                    if (saved)
                    {
                        lstLeaveType.EditIndex = -1;
                        LoadLeaveTypes();
                    }
                }
                else BindControl.BindLiteral(lblMsg, Messages.GetSaveFailedMessage());
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        void AddItem(ListViewCommandEventArgs e)
        {
            try
            {
                bool active = (e.Item.FindControl("chkActivei") as CheckBox).Checked;
                string desc = (e.Item.FindControl("txtDesci") as TextBox).Text;
                string name = (e.Item.FindControl("txtNamei") as TextBox).Text;
                string updatedBy = Util.user.LoginId;

                LeaveType obj = new LeaveType
                {
                    CompanyId = CompId,
                    LeaveName = name,
                    Description = desc,
                    LeaveId = 0,
                    CreatedBy = updatedBy,
                    UpdatedBy = updatedBy,
                    Active = active
                };

                if (CompId > 0)
                {
                    ILeaveType objl = new LeaveTypeBL();
                    bool saved = objl.AddLeaveType(obj);
                    string msg = saved ? Messages.GetUpdateMessage() : Messages.GetUpdateFailedMessage();
                    BindControl.BindLiteral(lblMsg, msg);
                    if (saved)
                    {
                        lstLeaveType.InsertItemPosition = InsertItemPosition.None;
                        LoadLeaveTypes();
                    }
                }
                else BindControl.BindLiteral(lblMsg, Messages.GetSaveFailedMessage());
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        #endregion

        #region ListView Event Handling Methods including Slide Paging

        protected void lstLeaveType_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            pager.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            LoadLeaveTypes();
        }

        protected void lstLeaveType_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            try
            {
                lstLeaveType.EditIndex = e.NewEditIndex;
                lstLeaveType.InsertItemPosition = InsertItemPosition.None;
                LoadLeaveTypes();
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        protected void lstLeaveType_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                BindControl.BindLiteral(lblMsg, "");
                if (e.Item.ItemType == ListViewItemType.InsertItem ||
               e.Item.ItemType == ListViewItemType.DataItem)
                {
                    switch (e.CommandName)
                    {
                        case "AddItem":
                            AddItem(e);
                            break;
                        case "UpdateItem":
                            UpdateItem(e);
                            break;
                        case "DeleteItem":
                            DeleteItem(e);
                            break;
                        case "CancelUpdate":
                            CancelUpdate();
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        #endregion
    }
}