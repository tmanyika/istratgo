﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.implementations;
using HR.Leave;

namespace scorecard.ui.controls.hr
{
    public partial class workingWeeks : System.Web.UI.UserControl
    {
        #region Properties

        int CompId
        {
            get { return Util.user.CompanyId; }
        }

        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) InitialisePage();
        }

        #endregion

        #region Utility Methods

        void InitialisePage()
        {
            LoadWorkingWeek();
        }

        void LoadWorkingWeek()
        {
            IWorkingWeek obj = new WorkingWeekBL();
            BindControl.BindListView(lstData, obj.GetByStatus(CompId, true));
            pager.Visible = (lstData.Items.Count <= pager.PageSize) ? false : true;
        }

        #endregion

        #region Button Event Handling

        protected void lnkAdd_Click(object sender, EventArgs e)
        {
            BindControl.BindLiteral(lblMsg, "");
            lstData.InsertItemPosition = InsertItemPosition.LastItem;
            LoadWorkingWeek();
        }

        protected void lnkCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("index.aspx", false);
        }

        #endregion

        #region Utility

        public string GetBoolean(object boolVal)
        {
            if (boolVal == DBNull.Value) return "Yes";
            return bool.Parse(boolVal.ToString()) ? "Yes" : "No";
        }

        private void CancelUpdate()
        {
            lstData.EditIndex = -1;
            lstData.InsertItemPosition = InsertItemPosition.None;
            LoadWorkingWeek();
        }

        void DeleteItem(ListViewCommandEventArgs e)
        {
            int workingWeekId = int.Parse((e.Item.FindControl("hdnWorkingWeekId") as HiddenField).Value);
            string updatedBy = Util.user.LoginId;

            IWorkingWeek obj = new WorkingWeekBL();
            bool deleted = obj.Delete(workingWeekId, updatedBy);
            if (deleted) LoadWorkingWeek();
            BindControl.BindLiteral(lblMsg, deleted ? Messages.GetDeleteMessage() : Messages.GetDeleteFailedMessage());
        }

        void UpdateItem(ListViewCommandEventArgs e)
        {
            try
            {
                bool active = (e.Item.FindControl("chkActive") as CheckBox).Checked;

                int workingWeekId = int.Parse((e.Item.FindControl("hdnLeaveIdE") as HiddenField).Value);
                int noOfDays = int.Parse((e.Item.FindControl("txtNoOfDays") as TextBox).Text.Trim());

                string name = (e.Item.FindControl("txtName") as TextBox).Text;
                string updatedBy = Util.user.LoginId;

                WorkingWeek obj = new WorkingWeek
                {
                    CompanyId = CompId,
                    Name = name,
                    NoOfDays = noOfDays,
                    WorkingWeekId = workingWeekId,
                    CreatedBy = updatedBy,
                    UpdatedBy = updatedBy,
                    Active = active
                };

                if (CompId > 0)
                {
                    IWorkingWeek objl = new WorkingWeekBL();
                    bool saved = objl.UpdateWorkingWeek(obj);
                    BindControl.BindLiteral(lblMsg, saved ? Messages.GetUpdateMessage() : Messages.GetUpdateFailedMessage());
                    if (saved)
                    {
                        lstData.EditIndex = -1;
                        LoadWorkingWeek();
                    }
                }
                else BindControl.BindLiteral(lblMsg, Messages.GetSaveFailedMessage());
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        void AddItem(ListViewCommandEventArgs e)
        {
            try
            {
                bool active = (e.Item.FindControl("chkActivei") as CheckBox).Checked;

                int noOfDays = int.Parse((e.Item.FindControl("txtNoOfDaysi") as TextBox).Text.Trim());

                string name = (e.Item.FindControl("txtNamei") as TextBox).Text;
                string updatedBy = Util.user.LoginId;

                WorkingWeek obj = new WorkingWeek
                {
                    CompanyId = CompId,
                    Name = name,
                    NoOfDays = noOfDays,
                    WorkingWeekId = 0,
                    CreatedBy = updatedBy,
                    UpdatedBy = updatedBy,
                    Active = active
                };

                if (CompId > 0)
                {
                    IWorkingWeek objl = new WorkingWeekBL();
                    bool saved = objl.AddWorkingWeek(obj);
                    BindControl.BindLiteral(lblMsg, saved ? Messages.GetUpdateMessage() : Messages.GetUpdateFailedMessage());
                    if (saved)
                    {
                        lstData.InsertItemPosition = InsertItemPosition.None;
                        LoadWorkingWeek();
                    }
                }
                else BindControl.BindLiteral(lblMsg, Messages.GetSaveFailedMessage());
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        #endregion

        #region ListView Event Handling Methods including Slide Paging

        protected void lstData_PagePropertiesChanged(object sender, EventArgs e)
        {
            LoadWorkingWeek();
        }

        protected void lstData_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            try
            {
                lstData.EditIndex = e.NewEditIndex;
                lstData.InsertItemPosition = InsertItemPosition.None;
                LoadWorkingWeek();
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        protected void lstData_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                BindControl.BindLiteral(lblMsg, "");
                if (e.Item.ItemType == ListViewItemType.InsertItem ||
                    e.Item.ItemType == ListViewItemType.DataItem)
                {
                    switch (e.CommandName)
                    {
                        case "AddItem":
                            AddItem(e);
                            break;
                        case "UpdateItem":
                            UpdateItem(e);
                            break;
                        case "DeleteItem":
                            DeleteItem(e);
                            break;
                        case "CancelUpdate":
                            CancelUpdate();
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }
        #endregion
    }
}