﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.controllers;
using scorecard.implementations;
using scorecard.entities;

namespace scorecard.ui.controls
{
    public partial class report : System.Web.UI.UserControl
    {

      goaldefinition goalDef;
         useradmincontroller usersDef;
         scoresmanager scoresManager;

         public report()
         {
            goalDef = new goaldefinition(new goalsImpl());
            usersDef = new useradmincontroller(new useradminImpl());
            scoresManager = new scoresmanager(new scoresImpl());
        }

        protected void Page_Load(object sender, EventArgs e){
            if (!IsPostBack) {
                getGoalsCriteriaTypes();               
                setCriteriaSelected();              
            }
        }
        
        public string getTypeName(int id) {
            return goalDef.appsettings.getApplicationSetting(new entities.applicationsettings { TYPE_ID = id }).NAME;
        }       

        void getGoalsCriteriaTypes() {
            var areaList = goalDef.appsettings.getChildSettings(new applicationsettings { PARENT_ID = Util.getTypeDefinitionsGoalAreaTypes() }).ToList();
            ddlArea.DataSource = areaList;
            ddlArea.DataTextField = "NAME";
            ddlArea.DataValueField = "TYPE_ID";
            ddlArea.DataBind();
        }

        void loadSetupTypes(DropDownList ddlPerspective)
        {
            try
            {
                var setuptypes = goalDef.appsettings.getChildSettings(new entities.applicationsettings { PARENT_ID = Util.getTypeDefinitionsPerspectivesParentID() });
                ddlPerspective.DataSource = setuptypes;
                ddlPerspective.DataTextField = "NAME";
                ddlPerspective.DataValueField = "TYPE_ID";
                ddlPerspective.DataBind();
            }
            catch (Exception e){
                Util.LogErrors(e);
            }
        }
        void loadJobTitles(DropDownList ddlJobTitles)   {
            try
            {
                var titles = usersDef.getAllCompanyUsers(new company { COMPANY_ID = Util.user.CompanyId });
                ddlJobTitles.DataSource = titles;
                ddlJobTitles.DataTextField = "NAME";
                ddlJobTitles.DataValueField = "ID";
                ddlJobTitles.DataBind();
            }
            catch (Exception e){
                Util.LogErrors(e);
            }
        }

        void loadStrategicGoals(DropDownList ddlStrategic){
            try
            {
                var strategic = goalDef.listStrategicGoals(new company { COMPANY_ID = Util.user.CompanyId });
                ddlStrategic.DataSource = strategic;
                ddlStrategic.DataTextField = "OBJECTIVE";
                ddlStrategic.DataValueField = "ID";
                ddlStrategic.DataBind();
            }
            catch (Exception e){
                Util.LogErrors(e);
            }
        }

        void loadAreas(DropDownList ddlAreas){
            try{
                var structure = goalDef.orgstructure.getCompanyStructure(new company { COMPANY_ID = Util.user.CompanyId });
                ddlAreas.DataSource = structure;
                ddlAreas.DataTextField = "ORGUNIT_NAME";
                ddlAreas.DataValueField = "ID";
                ddlAreas.DataBind();
            }
            catch (Exception e){
                Util.LogErrors(e);
            }
        }

        void setCriteriaSelected(){
            try
            {
                if (int.Parse(ddlArea.SelectedValue) == Util.getTypeDefinitionsRolesTypeID()){
                    loadJobTitles(ddlValueTypes);
                }
                else{
                    loadAreas(ddlValueTypes);
                }                     
            }
            catch (Exception e){
                Util.LogErrors(e);
            }
        }

        protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e){
            setCriteriaSelected();
            
        }          
        protected void ddlValueTypes_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}