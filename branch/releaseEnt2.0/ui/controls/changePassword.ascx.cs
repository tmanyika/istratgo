﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.implementations;
using scorecard.controllers;
using HR.Human.Resources;

namespace scorecard.ui.controls
{
    public partial class changePassword : System.Web.UI.UserControl
    {
        #region Page Event Handling Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                mView.SetActiveView(vwForm);
        }

        #endregion

        #region Utility Methods

        private void updateUserPassword()
        {
            try
            {
                string oldPassword = Util.HashPassword(txtOldPassword.Value.Trim());
                string newPass = Util.HashPassword(txtPassword.Value.Trim());
                string userName = Util.user.LoginId;

                IEmployee obj = new EmployeeBL();
                Employee user = obj.GetByUserName(userName);

                bool oldPinRight = (user.UserName != null) ? string.CompareOrdinal(user.Pass, oldPassword) == 0 ? true : false : false;
                if (oldPinRight)
                {
                    user.Pass = newPass;
                    user.UpdatedBy = user.UserName;
                    bool updated = obj.UpdateLogin(user);
                    if (updated)
                    {
                        lblMsg.Text = "Password was updated successfully.";
                        mView.SetActiveView(vwMsg);
                    }
                    else
                    {
                        mView.SetActiveView(vwForm);
                        lblError.Text = "The system failed to update the password. Please try again.";
                    }
                }
                else lblError.Text = "The old password you supplied is incorrect.";
            }
            catch (Exception e)
            {
                lblError.Text = Messages.GetErrorMessage();
                Util.LogErrors(e);
            }
        }

        #endregion

        #region Button Event Handling Methods

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            updateUserPassword();
        }

        #endregion
    }
}