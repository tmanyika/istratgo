﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.implementations;
using scorecard.controllers;
using scorecard.entities;
using HR.Human.Resources;

namespace scorecard.ui.controls
{
    public partial class scoreExceptionForm : System.Web.UI.UserControl
    {
        #region Properties

        bool IsAdmin
        {
            get { return (Util.user.UserTypeId != Util.getTypeDefinitionsUserTypeAdministrator()) ? false : true; }
        }

        #endregion

        #region  Variables

        useradmincontroller usersDef;
        goaldefinition goalDef;
        scoresmanager scoresManager;

        #endregion

        #region Default Class Constructors

        public scoreExceptionForm()
        {
            usersDef = new useradmincontroller(new useradminImpl());
            goalDef = new goaldefinition(new goalsImpl());
            scoresManager = new scoresmanager(new scorecard.implementations.scoresImpl());
        }

        #endregion

        #region Page Events Handling Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                loadOrgStruct();
        }

        #endregion

        #region Databinding Methods

        void downloadReport(bool addEventToButton)
        {
            string script = addEventToButton ? string.Format("return openNewWindow('previewexception.aspx?orgunitId={0}&dateVal={1}&deptName={2}');", ddOrgUnit.SelectedValue, ddDate.SelectedValue, HttpUtility.HtmlEncode(ddOrgUnit.SelectedItem.Text)) :
                                               string.Format("openNewWindow('previewexception.aspx?orgunitId={0}&dateVal={1}&deptName={2}');", ddOrgUnit.SelectedValue, ddDate.SelectedValue, HttpUtility.HtmlEncode(ddOrgUnit.SelectedItem.Text));
            if (addEventToButton) btnViewReport.OnClientClick = script;
            else AjaxScriptManager.RegisterStartupScript(Page, typeof(Page), "myReport", script, true);
        }

        void loadOrgStruct()
        {
            try
            {
                var userRole = Util.user.UserTypeId;
                var userID = Util.user.UserId;
                var structure = goalDef.orgstructure.getCompanyStructureListView(new company { COMPANY_ID = Util.user.CompanyId });
                if (userRole != Util.getTypeDefinitionsUserTypeAdministrator()) structure = structure.Where(t => t.OWNER_ID == userID).ToList();
                rqdVal.ErrorMessage = "select organisation unit";
                BindControl.BindDropdown(ddOrgUnit, "ORGUNIT_NAME", "ID", "- select organisation unit -", "0", structure.ToList());
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void BindReportDates(int compId, bool isOrgUnit)
        {
            List<scores> dates = scoresManager.getScoreDateValues(compId, isOrgUnit);
            BindControl.BindDropdown(ddDate, "PERIOD_DATE_VAL", "PERIOD_DATE_VAL", "- select date -", "0", dates);
        }

        #endregion

        #region Dropdown Event Handling Methods

        protected void ddDate_SelectedIndexChanged(object sender, EventArgs e)
        {
            downloadReport(true);
        }

        protected void ddOrgUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddOrgUnit.SelectedIndex > 0)
            {
                int compId = int.Parse(ddOrgUnit.SelectedValue);
                BindReportDates(compId, false);
            }
        }

        #endregion

        #region Button Event Handling Methods

        protected void btnViewReport_Click(object sender, EventArgs e)
        {
            downloadReport(false);
        }

        #endregion
    }
}