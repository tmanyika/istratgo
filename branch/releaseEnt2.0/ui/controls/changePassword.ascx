﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="changePassword.ascx.cs"
    Inherits="scorecard.ui.controls.changePassword" %>
<asp:UpdatePanel ID="UpdatePanelPin" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <h1>
            Change Password
        </h1>
        <asp:MultiView ID="mView" runat="server" ActiveViewIndex="0">
            <asp:View ID="vwForm" runat="server">
                <fieldset runat="server" id="pnlContactInfo">
                    <p>
                        <label for="sf">
                            <br />
                            &nbsp;
                        </label>
                    </p>
                    <p>
                        <label for="sf">
                            Old Password:
                        </label>
                        <span class="field_desc">
                            <input class="mf" name="mf0" type="password" runat="server" id="txtOldPassword" /></span>
                        <asp:RequiredFieldValidator ID="rqdOldPass" runat="server" ControlToValidate="txtOldPassword"
                            ValidationGroup="ChangePas" Display="None" ErrorMessage="Old Password"></asp:RequiredFieldValidator>
                    </p>
                    <p>
                        <label for="sf">
                            Password:
                        </label>
                        <span class="field_desc">
                            <input class="mf" name="mf0" type="password" runat="server" id="txtPassword" /></span>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtPassword"
                            ValidationGroup="ChangePas" Display="None" ErrorMessage="Password"></asp:RequiredFieldValidator>
                    </p>
                    <p>
                        <label for="sf">
                            Confirm Password:
                        </label>
                        <span class="field_desc">
                            <input class="mf" name="mf0" type="password" runat="server" id="txtConfirmPassword" /></span>
                        <asp:RequiredFieldValidator ID="rqdConfirm" runat="server" ControlToValidate="txtConfirmPassword"
                            Display="None" ErrorMessage="Confirm Password "></asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtConfirmPassword"
                            ValidationGroup="ChangePas" ControlToValidate="txtPassword" Display="None" ErrorMessage="Passwords do not match"></asp:CompareValidator>
                    </p>
                    <p>
                        <br />
                    </p>
                    <p class="errorMsg">
                        <label for="lblError">
                        </label>
                        <asp:Literal ID="lblError" runat="server"></asp:Literal><br />
                    </p>
                    <p>
                        <br />
                    </p>
                    <p>
                        <label>
                            &nbsp;
                        </label>
                        <asp:Button ID="btnSubmit" runat="server" class="button" Text="Submit" ValidationGroup="ChangePas"
                            OnClick="btnSubmit_Click" />
                        <asp:Button ID="btnCancel" runat="server" class="button" Text="Cancel" CausesValidation="False"
                            PostBackUrl="~/ui/index.aspx" />
                    </p>
                    <p>
                        &nbsp;<asp:ValidationSummary ID="vdSumarry" runat="server" HeaderText="Please provide the following fields:"
                            ShowMessageBox="True" ShowSummary="False" ValidationGroup="ChangePas" />
                    </p>
                </fieldset>
            </asp:View>
            <asp:View ID="vwMsg" runat="server">
                <p class="errorMsg">
                    <br />
                    &nbsp;
                    <br />
                    <asp:Literal ID="lblMsg" runat="server"></asp:Literal>
                </p>
            </asp:View>
        </asp:MultiView>
        <asp:UpdateProgress ID="UpdateProgressPin" runat="server" AssociatedUpdatePanelID="UpdatePanelPin">
            <ProgressTemplate>
                <div>
                    <asp:Image ID="imgPin" runat="server" ImageUrl="~/ui/images/icons/activity.gif" AlternateText="Please wait..">
                    </asp:Image></div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
