﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="dashboard.ascx.cs" Inherits="scorecard.ui.reports.dashboard" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../reports/dashStrategicObjectives.ascx" TagName="dashStrategicObjectives"
    TagPrefix="uc1" %>
<%@ Register Src="../reports/dashPerspectivePerformance.ascx" TagName="dashPerspectivePerformance"
    TagPrefix="uc2" %>
<%@ Register Src="../reports/dashFocusAreaWeighting.ascx" TagName="dashFocusAreaWeighting"
    TagPrefix="uc3" %>
<%@ Register Src="../reports/dashPerformanceRatingVsNumStaff.ascx" TagName="dashPerformanceRatingVsNumStaff"
    TagPrefix="uc4" %>
<%@ Register Src="../reports/dashTalentMatrix.ascx" TagName="dashTalentMatrix" TagPrefix="uc5" %>
<div>
    <h1>
        &nbsp;Dashboard
    </h1>
</div>
<div>
    <fieldset runat="server" id="pnlContactInfo">
        <legend>Dashboard Criteria </legend>
        <table border="0" cellpadding="4" cellspacing="2">
            <tr>
                <td>
                    <label for="sf">
                        Select Org Unit:
                    </label>
                </td>
                <td>
                    &nbsp;<span class="field_desc"><asp:DropDownList runat="server" ID="ddOrgUnit" ValidationGroup="Form"
                        Width="200px" />
                    </span>
                </td>
                <td>
                    <label for="sf">
                        Start Date:
                    </label>
                </td>
                <td>
                    <span class="field_desc">&nbsp;<asp:TextBox runat="server" ID="txtStartDate" Text=""
                        Width="80px" ValidationGroup="Form" />
                        <asp:CalendarExtender ID="txtStartDate_CalendarExtender" runat="server" Enabled="True"
                            CssClass="calendar" Format="MM/dd/yyyy" TargetControlID="txtStartDate">
                        </asp:CalendarExtender>
                    </span>
                </td>
                <td>
                    <label for="sf">
                        End Date:
                    </label>
                </td>
                <td>
                    <span class="field_desc">&nbsp;<asp:TextBox runat="server" ID="txtEndDate" Text=""
                        Width="80px" ValidationGroup="Form" />
                        <asp:CalendarExtender ID="txtEndDate_CalendarExtender" runat="server" Enabled="True"
                            CssClass="calendar" Format="MM/dd/yyyy" TargetControlID="txtEndDate">
                        </asp:CalendarExtender>
                    </span>
                </td>
                <td>
                    <asp:Button ID="btnViewReport" runat="server" CssClass="button tooltip" Text="Submit"
                        ValidationGroup="Form" OnClick="btnViewReport_Click" CausesValidation="true" />
                </td>
            </tr>
            <tr>
                <td colspan="7">
                    <asp:Literal ID="lblMsg" runat="server"></asp:Literal>
                </td>
            </tr>
        </table>
    </fieldset>
</div>
<div>
    <table cellpadding="6" cellspacing="4" border="0" width="100%">
        <tr runat="server" id="row1" visible="false">
            <td style="padding-left: 150px;">
                <h2>
                    <b>Performance Rating vs Number of Staff</b></h2>
            </td>
            <td>
                <h2>
                    Strategic Objectives<b> Weighting</b></h2>
            </td>
        </tr>
        <tr>
            <td>
                <uc4:dashPerformanceRatingVsNumStaff ID="dashPerformanceRatingVsNumStaff1" runat="server" />
            </td>
            <td style="vertical-align: top">
                <uc3:dashFocusAreaWeighting ID="dashFocusAreaWeighting1" runat="server" />
            </td>
        </tr>
        <tr runat="server" id="row2" visible="false">
            <td style="padding-left: 150px;">
                <h2>
                    <b>Strategic Objectives</b></h2>
            </td>
            <td>
                <h2>
                    <b>Key Performance Deliverables</b></h2>
            </td>
        </tr>
        <tr>
            <td>
                <uc2:dashPerspectivePerformance ID="dashPerspectivePerformance1" runat="server" />
            </td>
            <td>
                <uc1:dashStrategicObjectives ID="dashStrategicObjectives1" runat="server" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;<br />
            </td>
        </tr>
        <tr>
            <td colspan="2" style="padding-left: 480px;">
                <h2>
                    <b>Talent Matrix</b></h2>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="padding-left: 150px;">
                <uc5:dashTalentMatrix ID="dashTalentMatrix1" runat="server" />
            </td>
        </tr>
    </table>
</div>
