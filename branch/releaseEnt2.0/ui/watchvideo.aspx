﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="watchvideo.aspx.cs" Inherits="scorecard.ui.watchvideo" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>iStratgo :: Watch Video</title>
    <script src="js/standard.js" type="text/javascript"></script>
    <link href="css/welcomesite.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManagerMain" runat="server" CombineScripts="True"
        EnablePartialRendering="true" EnablePageMethods="true" LoadScriptsBeforeUI="true">
    </asp:ToolkitScriptManager>
    <div id="DoNotShow">
        <asp:UpdatePanel ID="UpdatePanelWelcome" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:UpdateProgress ID="UpdateProgressWelcome" runat="server" AssociatedUpdatePanelID="UpdatePanelWelcome">
                    <ProgressTemplate>
                        <div>
                            <asp:Image ID="imgProj" runat="server" ImageUrl="~/ui/images/icons/activity.gif"
                                AlternateText="Please wait.."></asp:Image></div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
                <asp:CheckBox ID="ChkShowAgain" runat="server" AutoPostBack="true" Checked="false"
                    OnCheckedChanged="ChkShowAgain_CheckedChanged" Text="Do not show this again"
                    TextAlign="Left" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="VideoDiv">
        <div id="CaptivateContent">
            &nbsp;
        </div>
        <script type="text/javascript">
            var so = new SWFObject("welcome/Overview.swf", "Captivate", "1297", "952", "7", "#CCCCCC");
            so.addParam("quality", "high");
            so.addParam("name", "Captivate");
            so.addParam("id", "Captivate");
            so.addParam("wmode", "window");
            so.addParam("bgcolor", "#F5F4F1");
            so.addParam("menu", "false");
            so.addVariable("variable1", "value1");
            so.setAttribute("redirectUrl", "http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash");
            so.write("CaptivateContent");
        </script>
        <script type="text/javascript">
            document.getElementById('Captivate').focus();
            document.Captivate.focus();
        </script>
    </div>
    </form>
</body>
</html>
