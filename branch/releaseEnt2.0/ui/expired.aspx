<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="expired.aspx.cs" Inherits="scorecard.ui.expired" %>

<%@ Register Src="controls/footer.ascx" TagName="footer" TagPrefix="uc1" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head id="Head1" runat="server">
    <!-- Meta -->
    <meta http-equiv="Refresh" content="4; URL=login.aspx" />
    <!-- End of Meta -->
    <!-- Page title -->
    <title>iStratgo </title>
    <!-- End of Page title -->
    <!-- Libraries -->
    <link type="text/css" href="css/layout.css" rel="stylesheet" />
    <script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
    <script type="text/javascript" src="js/easyTooltip.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.7.2.custom.min.js"></script>
    <script type="text/javascript" src="js/jquery.wysiwyg.js"></script>
    <script type="text/javascript" src="js/hoverIntent.js"></script>
    <script type="text/javascript" src="js/superfish.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <!-- End of Libraries -->
    <link href="css/avi.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <!-- Container -->
    <div id="container">
        <!-- Header -->
        <div id="header">
            <!-- Top -->
            <div id="top">
                <!-- Logo -->
                <div class="logo">
                    <a href="#" title="Administration Home" class="tooltip">
                        <img src="assets/logo.png" alt="Wide Admin" id="mylogo" /></a>
                </div>
                <!-- End of Logo -->
                <!-- Meta information -->
                <!-- End of Meta information -->
            </div>
            <!-- End of Top-->
            <!-- The navigation bar -->
            <div id="navbar">
            </div>
            <!-- End of navigation bar" -->
            <!-- Search bar -->
            <!-- End of Search bar -->
        </div>
        <!-- End of Header -->
        <!-- Background wrapper -->
        <div id="bgwrap">
            <!-- Main Content -->
            <div id="content">
                <div id="main">
                    <hr />
                    <p>
                        Your session has ended, you are being redirected to the login page ...</p>
                </div>
            </div>
            <!-- End of Main Content -->
            <!-- Sidebar -->
            <!-- End of Sidebar -->
        </div>
        <!-- End of bgwrap -->
    </div>
    <!-- End of Container -->
    <!-- Footer -->
    <div id="footer">
        <p class="mid">
            <!-- Change this to your own once purchased -->
            <uc1:footer ID="footer1" runat="server" />
            <!-- -->
        </p>
    </div>
    <!-- End of Footer -->
    </form>
</body>
</html>
