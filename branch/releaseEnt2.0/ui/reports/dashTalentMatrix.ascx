﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="dashTalentMatrix.ascx.cs"
    Inherits="scorecard.ui.reports.dashTalentMatrix" %>
<div>
    <table border="0" cellpadding="2" cellspacing="2" style="height: 400px">
        <tr>
            <td rowspan="2" class="box_rotate">
                <b>Years In Same Role</b>
            </td>
            <td>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="height: 100%"
                    class="matrixaxislabels">
                    <tr>
                        <td valign="top" align="center">
                            4
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" align="center">
                            3
                        </td>
                    </tr>
                </table>
            </td>
            <td class="matrixtdbackcolor">
                <div class="matrixcaption">
                    <asp:Literal ID="LblData1" runat="server"></asp:Literal>
                </div>
                <br />
                <div class="matrixcaptioninfo">
                    <asp:Literal ID="LblData1_1" runat="server"></asp:Literal></div>
            </td>
            <td class="matrixtdbackcolor">
                <div class="matrixcaption">
                    <asp:Literal ID="LblData2" runat="server"></asp:Literal></div>
                <br />
                <div class="matrixcaptioninfo">
                    <asp:Literal ID="LblData2_2" runat="server"></asp:Literal></div>
            </td>
        </tr>
        <tr>
            <td align="center">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="height: 100%"
                    class="matrixaxislabels">
                    <tr>
                        <td valign="top" align="center">
                            2
                        </td>
                    </tr>
                    <tr>
                        <td valign="middle" align="center">
                            1
                        </td>
                    </tr>
                </table>
            </td>
            <td class="matrix2tdbackcolor">
                <div class="matrixcaption">
                    <asp:Literal ID="LblData3" runat="server"></asp:Literal></div>
                <br />
                <div class="matrixcaptioninfo">
                    <asp:Literal ID="LblData3_3" runat="server"></asp:Literal></div>
            </td>
            <td class="matrix2tdbackcolor">
                <div class="matrixcaption">
                    <asp:Literal ID="LblData4" runat="server"></asp:Literal></div>
                <br />
                <div class="matrixcaptioninfo">
                    <asp:Literal ID="LblData4_4" runat="server"></asp:Literal></div>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td colspan="2">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="matrixaxislabels">
                    <tr>
                        <td align="center" valign="top">
                            1
                        </td>
                        <td align="center" valign="top">
                            2
                        </td>
                        <td align="center" valign="top">
                            3
                        </td>
                        <td align="center" valign="top">
                            4
                        </td>
                        <td align="center" valign="top">
                            5
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" align="center">
                            <br />
                            <b>Company Defined Rating</b>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
