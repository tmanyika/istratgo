﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.implementations;
using scorecard.entities;
using scorecard.controllers;
using System.Data;
using System.Web.UI.DataVisualization.Charting;
using HR.Human.Resources;

namespace scorecard.ui.reports
{
    public partial class graphReporting : System.Web.UI.UserControl
    {
        #region Variables

        useradmincontroller usersDef;
        goaldefinition goalDef;
        scoresmanager scoresManager;
        structurecontroller orgunits;

        #endregion

        #region Properties

        string Date
        {
            get
            {
                return Request["dVal"].ToString();
            }
        }

        int AreaId
        {
            get
            {
                return int.Parse(Request["aid"].ToString());
            }
        }

        int AreaValueId
        {
            get
            {
                return int.Parse(Request["idv"].ToString());
            }
        }

        #endregion

        #region Default Class Constructors

        public graphReporting()
        {
            goalDef = new goaldefinition(new goalsImpl());
            orgunits = new structurecontroller(new structureImpl());
            usersDef = new useradmincontroller(new useradminImpl());
            scoresManager = new scoresmanager(new scorecard.implementations.scoresImpl());
        }

        #endregion

        #region Page Event Handling Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                fillInfo();
                loadData();
            }
        }

        #endregion

        #region Databinding Methods

        private void fillInfo()
        {
            try
            {
                lblCriteria.Text = usersDef.appsettings.getApplicationSetting(new entities.applicationsettings { TYPE_ID = AreaId }).NAME;
                lblName.Text = getName(AreaValueId, AreaId);
                lblDate.Text = Common.GetDescriptiveDate(Date, false);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        string getName(int id, int criteria)
        {
            try
            {
                if (id > 0)
                {
                    if (criteria == Util.getTypeDefinitionsRolesTypeID())
                    {
                        IEmployee emp = new EmployeeBL();
                        return emp.GetById(id).FullName;
                    }
                    else if (criteria == Util.getTypeDefinitionsProjectID()) return new projectmanagement(new projectImpl()).get(id).PROJECT_NAME;
                    else return orgunits.getOrgunitDetails(new Orgunit { ID = id }).ORGUNIT_NAME;
                }
            }
            catch (Exception e) { Util.LogErrors(e); }
            return "";
        }

        DataTable getData()
        {
            string prevPerspective = "";
            var objectives = scoresManager.getScoresReport(AreaId, AreaValueId, Date);

            DataTable dT = new DataTable();
            dT.Columns.Add("Perspective", typeof(string));
            dT.Columns.Add("PerspectiveValue", typeof(int));

            foreach (scores item in objectives)
            {
                string nxtPerspective = item.PERSPECTIVE_NAME;
                if (string.Compare(nxtPerspective, prevPerspective) != 0)
                {
                    int pid = item.PERSPECTIVE_ID;
                    int total = objectives.Where(c => c.PERSPECTIVE_ID == pid).Sum(u => u.WEIGHT);

                    DataRow nRow = dT.NewRow();
                    nRow["Perspective"] = string.Format("{0} {1}%", Util.getGraphLabelName(nxtPerspective), total);
                    nRow["PerspectiveValue"] = total;
                    dT.Rows.Add(nRow);
                    nRow = null;
                }
                prevPerspective = nxtPerspective;
            }
            dT.AcceptChanges();
            return dT;
        }

        void loadData()
        {
            try
            {
                DataTable dT = getData();

                orgChart.Series[0].ToolTip = "#VALX - #VALY%";
                orgChart.Series[0].XValueMember = "Perspective";
                orgChart.Series[0].YValueMembers = "PerspectiveValue";
                orgChart.DataSource = dT;
                orgChart.DataBind();

                if (dT.Rows.Count >= Util.getGraphMinItems())
                {
                    orgChart.Height = new Unit(Util.getGraphHeight());
                    orgChart.Width = new Unit(Util.getGraphWidth());
                }
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        #endregion
    }
}