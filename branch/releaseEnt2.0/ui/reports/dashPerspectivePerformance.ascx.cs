﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using scorecard.implementations;
using scorecard.controllers;
using scorecard.entities;
using vb = Microsoft.VisualBasic;
using HR.Human.Resources;
using System.Collections;
using Dashboard.Reporting;

namespace scorecard.ui.reports
{
    public partial class dashPerspectivePerformance : System.Web.UI.UserControl
    {
        #region Variables

        IDashboard dash;

        #endregion

        #region Default Class Constructors

        public dashPerspectivePerformance()
        {
            dash = new DashboardBL();
        }

        #endregion

        #region Delegates Methods

        public void FillPerspectives(int areaId, int orgUnitId, bool isCompany, string startDate, string endDate)
        {
            try
            {
                double actual = 0;
                double target = 0;

                DataTable dT = dash.GetPerspectives(areaId, orgUnitId, isCompany, startDate, endDate);

                LblNoDataMsg.Visible = dT.Rows.Count > 0 ? false : true;
                RadChartPes.Visible = dT.Rows.Count > 0 ? true : false;
                if (dT.Rows.Count <= 0) return;

                DataRow nRow = dT.NewRow();
                foreach (DataRow rw in dT.Rows)
                {
                    actual += double.Parse(rw["ActualValue"].ToString());
                    target += double.Parse(rw["TargetValue"].ToString());
                }

                nRow["Perspective"] = "Total";
                nRow["TargetValue"] = Math.Round(target, 0, MidpointRounding.AwayFromZero);
                nRow["ActualValue"] = Math.Round(actual, 0, MidpointRounding.AwayFromZero);
                dT.Rows.Add(nRow);
                nRow = null;

                if (dT.Rows.Count >= Util.getDashBoardGraphMinItems())
                {
                    RadChartPes.Height = new Unit(Util.getDashBoardGraphHeight());
                    RadChartPes.Width = new Unit(Util.getDashBoardGraphWidth());
                }

                RadChartPes.Visible = dT.Rows.Count > 0 ? true : false;
                RadChartPes.DataSource = dT;
                RadChartPes.DataBind();
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        #endregion
    }
}