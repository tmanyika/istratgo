﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using scorecard.implementations;
using scorecard.controllers;
using scorecard.entities;
using vb = Microsoft.VisualBasic;
using HR.Human.Resources;
using System.Collections;
using Dashboard.Reporting;

namespace scorecard.ui.reports
{
    public partial class dashTalentMatrix : System.Web.UI.UserControl
    {
        #region Variables

        IDashboard dash;
        ITalentMatrix mtx;

        #endregion

        #region Default Class Constructors

        public dashTalentMatrix()
        {
            dash = new DashboardBL();
            mtx = new TalentMatrixBL();
        }

        #endregion

        #region Delegates Methods

        int GetTotalPeople(DataTable dT, decimal yrsinrole, string yrsinroleoperator, decimal ratingval, string ratingvaloperator)
        {
            string filter = string.Format("YearsOnSameRole {0} {1} and RatingValue {2} {3}", yrsinroleoperator, yrsinrole, ratingvaloperator, ratingval);
            DataRow[] row = dT.Select(filter);
            return row.Length;
        }

        public void FillTalentMatrix(int areaId, int orgUnitId, bool isCompany, string startDate, string endDate)
        {
            try
            {
                var data = mtx.GetActiveTalentMatrix(Util.user.CompanyId, true);
                DataTable dT = dash.GetTalentMatrix(areaId, orgUnitId, isCompany, startDate, endDate);

                List<TalentMatrix> mtxData = new List<TalentMatrix>();

                foreach (TalentMatrix item in data)
                {
                    decimal yrsinsamerole = item.LowerBound;
                    decimal ratingval = item.UpperBound;
                    int position = item.PositionInMatrix;

                    string yrsinsameroleoperator = item.LowerBoundSign;
                    string ratingvaloperator = item.UpperBoundSign;

                    int numofppl = GetTotalPeople(dT, yrsinsamerole, yrsinsameroleoperator, ratingval, ratingvaloperator);
                    string talentmatrixname = item.TalentMatrixName;
                    string talentmatrixdesc = string.Format("{0} people", numofppl);
                    
                    if (position == 1)
                    {
                        BindControl.BindLiteral(LblData1, talentmatrixname);
                        BindControl.BindLiteral(LblData1_1, talentmatrixdesc);
                    }
                    else if (position == 2)
                    {
                        BindControl.BindLiteral(LblData2, talentmatrixname);
                        BindControl.BindLiteral(LblData2_2, talentmatrixdesc);
                    }
                    else if (position == 3)
                    {
                        BindControl.BindLiteral(LblData3, talentmatrixname);
                        BindControl.BindLiteral(LblData3_3, talentmatrixdesc);
                    }
                    else if (position == 4)
                    {
                        BindControl.BindLiteral(LblData4, talentmatrixname);
                        BindControl.BindLiteral(LblData4_4, talentmatrixdesc);
                    }
                }
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        #endregion
    }
}