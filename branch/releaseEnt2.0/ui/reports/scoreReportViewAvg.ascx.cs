﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.controllers;
using scorecard.implementations;
using scorecard.entities;
using System.Data;
using RKLib.ExportData;
using HR.Human.Resources;
using System.Collections;

namespace scorecard.ui.reports
{
    public partial class scoreReportViewAvg : System.Web.UI.UserControl
    {
        #region Variables

        useradmincontroller usersDef;
        goaldefinition goalDef;
        scoresmanager scoresManager;
        structurecontroller orgunits;

        #endregion

        #region Properties

        string StartDate
        {
            get
            {
                return Request["dsVal"].ToString();
            }
        }

        string EndDate
        {
            get
            {
                return Request["deVal"].ToString();
            }
        }

        int AreaId
        {
            get
            {
                return int.Parse(Request["aid"].ToString());
            }
        }

        int AreaValueId
        {
            get
            {
                return int.Parse(Request["idv"].ToString());
            }
        }

        #endregion

        #region Default Class Constructors

        public scoreReportViewAvg()
        {
            goalDef = new goaldefinition(new goalsImpl());
            orgunits = new structurecontroller(new structureImpl());
            usersDef = new useradmincontroller(new useradminImpl());
            scoresManager = new scoresmanager(new scorecard.implementations.scoresImpl());
        }

        #endregion

        #region Page Event Handling Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                fillInfo();
                loadData();
            }
        }

        #endregion

        #region Databinding Methods

        private void fillInfo()
        {
            try
            {
                lblCriteria.Text = usersDef.appsettings.getApplicationSetting(new entities.applicationsettings { TYPE_ID = AreaId }).DESCRIPTION;
                lblName.Text = getName(AreaValueId, AreaId);
                lblStartDate.Text = Common.GetDescriptiveDate(StartDate, false);
                lblEndDate.Text = Common.GetDescriptiveDate(EndDate, false);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        string getName(int id, int criteria)
        {
            try
            {
                if (id > 0)
                    if (criteria == Util.getTypeDefinitionsRolesTypeID())
                    {
                        IEmployee emp = new EmployeeBL();
                        return emp.GetById(id).FullName;
                    }
                    else if (criteria == Util.getTypeDefinitionsProjectID()) return new projectmanagement(new projectImpl()).get(id).PROJECT_NAME;
                    else return orgunits.getOrgunitDetails(new Orgunit { ID = id }).ORGUNIT_NAME;
            }
            catch (Exception e) { Util.LogErrors(e); }
            return string.Empty;
        }

        void trimData(List<scores> objectives)
        {
            try
            {
                string prevPerspective = "";
                decimal totalPerspective = 0;

                foreach (ListViewDataItem item in lstData.Items)
                {
                    HiddenField hdnPid = item.FindControl("hdnPid") as HiddenField;
                    Literal lblPerspective = item.FindControl("lblPerspective") as Literal;
                    string nxtPerspective = (item.FindControl("lblPerspective") as Literal).Text.Trim();
                    if (string.Compare(nxtPerspective, prevPerspective) == 0) lblPerspective.Text = "";
                    else
                    {
                        int pid = int.Parse(hdnPid.Value);
                        if (item.DataItemIndex != lstData.Items.Count - 1)
                        {
                            decimal total = objectives.Where(c => c.PERSPECTIVE_ID == pid).Sum(u => u.WEIGHT);
                            totalPerspective += total;
                        }
                    }
                    prevPerspective = nxtPerspective;
                }
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        public object GetVal(object agreedScore, object finalScore)
        {
            return (agreedScore != DBNull.Value) ? agreedScore : finalScore;
        }

        void loadData()
        {
            try
            {
                var objectives = scoresManager.getScoresReport(AreaId, AreaValueId, StartDate, EndDate);
                var finalScore = objectives.Sum(u => u.FINAL_SCORE);
                var finalAgreedScore = objectives.Sum(u => u.FINAL_SCORE_AGREED);
                var totalWeight = objectives.Sum(u => u.WEIGHT);
                scores totalDetails = new scores
                {
                    PERSPECTIVE_NAME = "<b>Average Performance Score</b> ",
                    WEIGHT = totalWeight,
                    FINAL_SCORE = Math.Round(finalScore, 2),
                    FINAL_SCORE_AGREED = Math.Round((decimal)finalAgreedScore, 1),
                };
                objectives.Add(totalDetails);
                BindControl.BindListView(lstData, objectives);
                trimData(objectives);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        #endregion

        #region Button Event Handling Methods

        protected void lnkExcel_Click(object sender, EventArgs e)
        {
            Download();
        }

        DataTable GetExportSchema()
        {
            DataTable dT = new DataTable();
            dT.Columns.Add("Perspective", typeof(string));
            dT.Columns.Add("Objective", typeof(string));
            dT.Columns.Add("Goal", typeof(string));
            dT.Columns.Add("Unit", typeof(string));
            dT.Columns.Add("Weight", typeof(string));
            dT.Columns.Add("FinalScore", typeof(string));
            dT.Columns.Add("DefinedScore", typeof(string));
            dT.AcceptChanges();
            return dT;
        }

        private void Download()
        {
            if (lstData.Items.Count <= 0)
            {
                lblError.Text = "There are no records to export to excel.";
                return;
            }

            DataTable dT = GetExportSchema();
            int[] cols = { 0, 1, 2, 3, 4, 5, 6 };
            string[] headers = { "", "", "", "", "", "", "" };

            DataRow nRow = dT.NewRow();
            nRow["Perspective"] = "Area".ToUpper();
            nRow["Objective"] = lblCriteria.Text;
            dT.Rows.Add(nRow);
            dT.Rows.Add(dT.NewRow());//add blank row
            nRow = null;

            nRow = dT.NewRow();
            nRow["Perspective"] = "Name".ToUpper();
            nRow["Objective"] = lblName.Text;
            dT.Rows.Add(nRow);
            dT.Rows.Add(dT.NewRow());//add blank row
            nRow = null;

            nRow = dT.NewRow();
            nRow["Perspective"] = "Start Date".ToUpper();
            nRow["Objective"] = lblStartDate.Text;
            dT.Rows.Add(nRow);
            dT.Rows.Add(dT.NewRow());//add blank row
            nRow = null;

            nRow = dT.NewRow();
            nRow["Perspective"] = "End Date".ToUpper();
            nRow["Objective"] = lblEndDate.Text;
            dT.Rows.Add(nRow);
            dT.Rows.Add(dT.NewRow());//add blank row
            nRow = null;

            nRow = dT.NewRow();
            nRow["Perspective"] = "Strategic Objective".ToUpper();
            nRow["Objective"] = "Key Performance Deliverable".ToUpper();
            nRow["Goal"] = "Performance Measure".ToUpper();
            nRow["Unit"] = "Unit of Measure".ToUpper();
            nRow["Weight"] = "Performance Measure Weight".ToUpper();
            nRow["FinalScore"] = "Average Perfomance Score (%)".ToUpper();
            nRow["DefinedScore"] = "Average Company Defined Score".ToUpper();
            dT.Rows.Add(nRow);
            nRow = null;

            foreach (ListViewItem item in lstData.Items)
            {
                string strPerspective = (item.FindControl("lblPerspective") as Literal).Text;
                strPerspective = strPerspective.Replace("</b>", "").Replace("<b>", "");

                nRow = dT.NewRow();
                nRow["Perspective"] = strPerspective;
                nRow["Objective"] = (item.FindControl("lblObjective") as Literal).Text;
                nRow["Goal"] = (item.FindControl("lblGoalDesc") as Literal).Text;
                nRow["Unit"] = (item.FindControl("lblUnitMeasure") as Literal).Text;
                nRow["Weight"] = string.Format("{0}%", (item.FindControl("lblWeight") as Literal).Text);
                nRow["FinalScore"] = string.Format("{0}%", (item.FindControl("lblFinalScore") as Literal).Text);
                nRow["DefinedScore"] = (item.FindControl("lblRatingValue") as Literal).Text;
                dT.Rows.Add(nRow);
                nRow = null;
            }

            dT.AcceptChanges();
            Export exp = new Export("Web");
            exp.ExportDetails(dT, cols, headers, Export.ExportFormat.Excel, Export.ExportAction.DownloadOnly, string.Format("{0}.xls", lblName.Text));
        }

        #endregion
    }
}