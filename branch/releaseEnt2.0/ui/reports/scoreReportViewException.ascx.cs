﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.controllers;
using scorecard.implementations;
using scorecard.entities;
using System.Data;
using RKLib.ExportData;
using HR.Human.Resources;

namespace scorecard.ui.reports
{
    public partial class scoreReportViewException : System.Web.UI.UserControl
    {
        #region Variables

        useradmincontroller usersDef;
        goaldefinition goalDef;
        scoresmanager scoresManager;
        structurecontroller orgunits;

        #endregion

        #region Properties

        string DeptName
        {
            get
            {
                return HttpUtility.HtmlDecode(Request["deptName"].ToString());
            }
        }

        string Date
        {
            get
            {
                return Request["dateVal"].ToString();
            }
        }

        int OrgunitId
        {
            get
            {
                return int.Parse(Request["orgunitId"].ToString());
            }
        }

        #endregion

        #region Default Class Constructors

        public scoreReportViewException()
        {
            goalDef = new goaldefinition(new goalsImpl());
            orgunits = new structurecontroller(new structureImpl());
            usersDef = new useradmincontroller(new useradminImpl());
            scoresManager = new scoresmanager(new scorecard.implementations.scoresImpl());
        }

        #endregion

        #region Page Event Handling Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                fillInfo();
                loadData();
            }
        }

        #endregion

        #region Databinding Methods

        private void fillInfo()
        {
            try
            {
                lblCriteria.Text = DeptName;
                lblDate.Text = Common.GetDescriptiveDate(Date, false);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void loadData()
        {
            try
            {
                var objectives = scoresManager.getScoreExceptionReport(OrgunitId, Date);
                BindControl.BindListView(lstData, objectives);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        #endregion

        #region Button Event Handling Methods

        protected void lnkExcel_Click(object sender, EventArgs e)
        {
            Download();
        }

        DataTable GetExportSchema()
        {
            DataTable dT = new DataTable();
            dT.Columns.Add("FullName", typeof(string));
            dT.Columns.Add("FinalScore", typeof(string));
            dT.Columns.Add("FinalRating", typeof(string));
            dT.Columns.Add("Status", typeof(string));
            dT.AcceptChanges();
            return dT;
        }

        private void Download()
        {
            if (lstData.Items.Count <= 0)
            {
                lblError.Text = "There are no records to export to excel.";
                return;
            }

            DataTable dT = GetExportSchema();
            int[] cols = { 0, 1, 2, 3 };
            string[] headers = { "", "", "", "" };

            DataRow nRow = dT.NewRow();
            nRow["FullName"] = "Department".ToUpper();
            nRow["FinalScore"] = lblCriteria.Text;
            dT.Rows.Add(nRow);
            dT.Rows.Add(dT.NewRow());//add blank row
            nRow = null;

            nRow = dT.NewRow();
            nRow["FullName"] = "Report Date".ToUpper();
            nRow["FinalScore"] = lblDate.Text;
            dT.Rows.Add(nRow);
            dT.Rows.Add(dT.NewRow());//add blank row
            nRow = null;

            nRow = dT.NewRow();
            nRow["FullName"] = "Full Name".ToUpper();
            nRow["FinalScore"] = " Final Score (%)".ToUpper();
            nRow["FinalRating"] = "Defined Weighted Score".ToUpper();
            nRow["Status"] = "Status".ToUpper();
            dT.Rows.Add(nRow);
            nRow = null;

            foreach (ListViewItem item in lstData.Items)
            {
                string finalScore = (item.FindControl("lblFinalScore") as Literal).Text;
                nRow = dT.NewRow();
                nRow["FullName"] = (item.FindControl("lblFullName") as Literal).Text;
                if (!string.IsNullOrEmpty(finalScore))
                    nRow["FinalScore"] = string.Format("{0}%", finalScore);
                nRow["FinalRating"] = (item.FindControl("lblFinalRating") as Literal).Text;
                nRow["Status"] = (item.FindControl("lblStatus") as Literal).Text;
                dT.Rows.Add(nRow);
                nRow = null;
            }

            dT.AcceptChanges();
            Export exp = new Export("Web");
            exp.ExportDetails(dT, cols, headers, Export.ExportFormat.Excel, Export.ExportAction.DownloadOnly, string.Format("{0}.xls", lblCriteria.Text));
        }

        #endregion
    }
}