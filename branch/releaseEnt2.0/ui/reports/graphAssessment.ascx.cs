﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using scorecard.implementations;
using scorecard.entities;
using scorecard.controllers;
using System.Web.UI.DataVisualization.Charting;
using HR.Human.Resources;
using System.Collections;
using Dashboard.Reporting;
using scorecard.interfaces;
using System.Text;

namespace scorecard.ui.reports
{
    public partial class graphAssessment : System.Web.UI.UserControl
    {
        #region Variables

        IDashboard dash;
        IScoreRating iR;
        useradmincontroller usersDef;
        structurecontroller orgunits;

        string startYrPrev;
        string endYrPrev;
        string startYrCur;
        string endYrCur;

        #endregion

        #region Properties

        string StartDate
        {
            get { return Request.QueryString["dsVal"].ToString(); }
        }

        string EndDate
        {
            get { return Request.QueryString["deVal"].ToString(); }
        }

        int AreaId
        {
            get { return int.Parse(Request.QueryString["aid"].ToString()); }
        }

        int AreaValueId
        {
            get { return int.Parse(Request.QueryString["idv"].ToString()); }
        }

        string StartDateYearPrev
        {
            get { return startYrPrev; }
            set { startYrPrev = value; }
        }

        string EndDateYearPrev
        {
            get { return endYrPrev; }
            set { endYrPrev = value; }
        }

        string StartDateYearCur
        {
            get { return startYrCur; }
            set { startYrCur = value; }
        }

        string EndDateYearCur
        {
            get { return endYrCur; }
            set { endYrCur = value; }
        }

        #endregion

        #region Default Class Constructors

        public graphAssessment()
        {
            dash = new DashboardBL();
            iR = new ScoreRatingBL();
            usersDef = new useradmincontroller(new useradminImpl());
            orgunits = new structurecontroller(new structureImpl());
        }

        #endregion

        #region Page Load Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillInfo();
                FillAssessmentRatings(AreaId, AreaValueId, StartDate, EndDate);
            }
        }

        #endregion

        #region Databing Methods

        void FillInfo()
        {
            try
            {
                lblCriteria.Text = usersDef.appsettings.getApplicationSetting(new entities.applicationsettings { TYPE_ID = AreaId }).NAME;
                lblName.Text = GetName(AreaValueId, AreaId); ;
                lblDate.Text = Common.GetDescriptiveDate(StartDate, false);
                lblEndDate.Text = Common.GetDescriptiveDate(EndDate, false);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        string GetName(int id, int criteria)
        {
            try
            {
                if (id > 0)
                {
                    if (criteria == Util.getTypeDefinitionsRolesTypeID())
                    {
                        IEmployee emp = new EmployeeBL();
                        return emp.GetById(id).FullName;
                    }
                    else if (criteria == Util.getTypeDefinitionsProjectID()) return new projectmanagement(new projectImpl()).get(id).PROJECT_NAME;
                    else return orgunits.getOrgunitDetails(new Orgunit { ID = id }).ORGUNIT_NAME;
                }
            }
            catch (Exception e) { Util.LogErrors(e); }
            return "";
        }

        public void FillAssessmentRatings(int areaId, int valueId, string startdate, string enddate)
        {
            try
            {
                DateTime prevDate = DateTime.Parse(startdate);
                DateTime currDate = DateTime.Parse(enddate);

                string sdate1 = string.Format("{0:MM/dd/yyyy}", new DateTime(prevDate.Year - 1, prevDate.Month, prevDate.Day));
                string edate1 = string.Format("{0:MM/dd/yyyy}", new DateTime(currDate.Year - 1, currDate.Month, currDate.Day));

                string startDate1 = Common.GetInternationalDateFormat(sdate1, DateFormat.monthDayYear);
                string endDate1 = Common.GetInternationalDateFormat(edate1, DateFormat.monthDayYear);

                StartDateYearPrev = (prevDate.Year - 1).ToString();
                EndDateYearPrev = (currDate.Year - 1).ToString();
                StartDateYearCur = prevDate.Year.ToString();
                EndDateYearCur = currDate.Year.ToString();

                DataTable dTY = dash.GetAssessmentRatings(areaId, valueId, startdate, enddate);
                DataTable dTP = dash.GetAssessmentRatings(areaId, valueId, startDate1, endDate1);
                DataTable dT = GetData(dTY, dTP);

                if (dT.Rows.Count >= Util.getGraphMinItems())
                {
                    MyRadChart.Height = new Unit(Util.getGraphHeight());
                    MyRadChart.Width = new Unit(Util.getGraphWidth());
                }

                MyRadChart.Visible = dT.Rows.Count > 0 ? true : false;
                MyRadChart.DataSource = dT;
                MyRadChart.DataBind();

                WriteData(dT);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        #endregion

        #region Telerik Event Handling Methods

        //protected void RadChart_BeforeLayout(object sender, EventArgs e)
        //{
        //    for (int i = 0; i < MyRadChart.PlotArea.YAxis.Items.Count; i++)
        //    {
        //        int integer = (int)MyRadChart.PlotArea.YAxis.Items[i].Value;
        //        if (MyRadChart.PlotArea.YAxis.Items[i].Value - integer > 0)
        //        {
        //            MyRadChart.PlotArea.YAxis.Items[i].TextBlock.Text = " ";
        //            MyRadChart.PlotArea.YAxis.Appearance.MajorTick.Visible = false;
        //        }
        //    }
        //}

        #endregion

        #region Databinding Methods

        double GetScore(int month, int year, DataTable dE)
        {
            DataRow[] rw = dE.Select(string.Format("PeriodMonth = {0} and PeriodYear = {1}", month, year));
            return rw.Length > 0 ? double.Parse(rw[0]["DefinedActualValue"].ToString()) : 0;
        }

        double GetScore(int month, DataTable dE)
        {
            DataRow[] rw = dE.Select(string.Format("PeriodMonth = {0}", month));
            return rw.Length > 0 ? Math.Round(double.Parse(rw[0]["DefinedActualValue"].ToString()), 1) : 0;
        }

        DataTable GetData(DataTable dTY, DataTable dTP)
        {
            ArrayList rating = new ArrayList();
            DataTable dT = new DataTable();

            dT.Columns.Add("Month", typeof(string));
            dT.Columns.Add("CurrentYear", typeof(double));
            dT.Columns.Add("LastYear", typeof(double));

            for (int i = 1; i <= 12; i++)
            {
                int month = i;

                double ratingPVal = GetScore(month, dTP);
                double ratingYVal = GetScore(month, dTY);

                string monthName = Common.GetMonthName(i, false);
                if (ratingYVal != 0 || ratingPVal != 0)
                {
                    DataRow nRow = dT.NewRow();

                    nRow["Month"] = monthName;
                    nRow["CurrentYear"] = ratingYVal;
                    nRow["LastYear"] = ratingPVal;
                    dT.Rows.Add(nRow);
                    nRow = null;
                }
            }

            dT.AcceptChanges();
            return dT;
        }

        void WriteData(DataTable dV)
        {
            try
            {
                LblData.Text = string.Empty;

                StringBuilder str1 = new StringBuilder("");
                StringBuilder str2 = new StringBuilder("");
                StringBuilder str3 = new StringBuilder("");

                foreach (DataRow rw in dV.Rows)
                {
                    str1.Append(string.Format("<td>{0}</td>", rw["LastYear"]));
                    str2.Append(string.Format("<td>{0}</td>", rw["CurrentYear"]));
                    str3.Append(string.Format("<td>{0}</td>", rw["Month"]));
                }

                string roottable = hdnTable.Value;
                string prevYear = StartDateYearPrev.Equals(EndDateYearPrev) ?
                                  StartDateYearPrev : string.Format("{0}-{1}", StartDateYearPrev, EndDateYearPrev);
                string curYear = StartDateYearCur.Equals(EndDateYearCur) ?
                                StartDateYearCur : string.Format("{0}-{1}", StartDateYearCur, EndDateYearCur);

                StringBuilder strB = new StringBuilder(roottable);
                string mytable = string.Format("<tr><td>Previous Year: {3}</td>{0}</tr><tr><td>Current Year: {4}</td>{1}</tr><tr><td>Month</td>{2}</tr>",
                                                str1.ToString(), str2.ToString(), str3.ToString(), prevYear, curYear);
                strB.Append(string.Format("{0}</table>", mytable));
                LblData.Text = strB.ToString();
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        #endregion
    }
}