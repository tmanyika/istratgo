﻿using System;
using scorecard.entities;
using System.Collections.Generic;

namespace Scorecard.Business.Rules
{
    public interface IBusinessRule
    {
        bool SubscriptionValid(int companyId);
        bool UnitOfMeasureRequireYesNoValue(int unitOfMeasure);
        bool UnitOfMeasureRequireNumericValue(int unitOfMeasure);
        bool UnitOfMeasureRequireYesNoValueValid(string score);
        decimal FinalScore(int unitOfMeasure, decimal weight, decimal score, decimal target);

        decimal GetDefinedWeightedScore(decimal finalScore, decimal ratingVal);
        decimal? GetScoringRate(List<ScoreRating> data, decimal finalscore, decimal? maxValue, decimal? maxRating, bool applyproportion);
        decimal? GetScoringRate(List<ScoreRating> data, decimal weight, decimal finalscore, decimal? maxValue, decimal? maxRating);
        void GetMaxValues(List<ScoreRating> data, out decimal? maxRate, out decimal? maxValue);

    }
}
