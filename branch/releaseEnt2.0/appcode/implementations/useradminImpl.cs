﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using scorecard.interfaces;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using scorecard.entities;


/* @Copy Albertoncaffeine.net */

namespace scorecard.implementations
{
    [Serializable]
    public class useradminImpl : Iuser
    {
        #region Iuser Members

        public int addUserAccount(useradmin _user, out string errorMessage)
        {
            errorMessage = "";
            int userId = 0;
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try
            {
                SqlDataReader reader = SqlHelper.ExecuteReader(con, "PROC_ADD_USER",
                     _user.LOGIN_ID,
                     _user.PASSWD,
                     _user.ORG_ID,
                     _user.JOB_TITLE_ID,
                     _user.NAME,
                     _user.EMAIL_ADDRESS,
                     _user.REPORTS_TO,
                     _user.USER_TYPE
                 );

                if (reader.HasRows)
                {
                    reader.Read();
                    userId = int.Parse(reader["userId"].ToString());
                    bool unameExist = bool.Parse(reader["uname"].ToString());
                    bool emailExist = bool.Parse(reader["email"].ToString());
                    errorMessage = unameExist ? "Username" : string.Empty;
                    errorMessage = (emailExist && unameExist) ? string.Format("{0} and {1}", errorMessage, "Email") : emailExist ? "Email" : string.Empty;
                    errorMessage = string.Format("{0} already exist.", errorMessage);
                }
            }
            catch (Exception e)
            {
                Util.LogErrors(e);

            }
            finally
            {
                con.Close();
                con.Dispose();
            }
            return userId;
        }

        public int updateUserAccount(useradmin _user)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try
            {
                return SqlHelper.ExecuteNonQuery(con, "PROC_UPDATE_USER",
                     _user.ID,
                     _user.ORG_ID,
                     _user.JOB_TITLE_ID,
                     _user.NAME,
                     _user.EMAIL_ADDRESS,
                     _user.REPORTS_TO,
                     _user.USER_TYPE
                 );
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return 0;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }


        public List<useradmin> getAllCompanyUsers(company _company)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            SqlDataReader read = null;
            try
            {
                List<useradmin> users = new List<useradmin>();
                read = SqlHelper.ExecuteReader(con, "PROC_GET_USERS_BY_COMPANY_ID", _company.COMPANY_ID);
                while (read.Read())
                {
                    users.Add(new useradmin
                    {
                        ID = read.GetInt32(0),
                        LOGIN_ID = read.GetString(1),
                        PASSWD = read.GetString(2),
                        ORG_ID = read.GetInt32(3),
                        JOB_TITLE_ID = read.GetInt32(4),
                        NAME = read.GetString(5),
                        EMAIL_ADDRESS = read.GetString(6),
                        ACTIVE = read.GetBoolean(7),
                        REPORTS_TO = read.GetInt32(8),
                        USER_TYPE = read.GetInt32(9)
                    });
                }
                return users;
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
            finally
            {
                read.Dispose();
                read.Close();
                con.Dispose();
                con.Close();
            }
        }

        public List<useradmin> getAllUsers()
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            SqlDataReader read = null;
            try
            {
                List<useradmin> users = new List<useradmin>();
                read = SqlHelper.ExecuteReader(con, "PROC_GET_USERS");
                while (read.Read())
                {
                    users.Add(new useradmin
                    {
                        ID = read.GetInt32(0),
                        LOGIN_ID = read.GetString(1),
                        PASSWD = read.GetString(2),
                        ORG_ID = read.GetInt32(3),
                        JOB_TITLE_ID = read.GetInt32(4),
                        NAME = read.GetString(5),
                        EMAIL_ADDRESS = read.GetString(6),
                        ACTIVE = read.GetBoolean(7),
                        REPORTS_TO = read.GetInt32(8),
                        USER_TYPE = read.GetInt32(9),
                        CountryId = int.Parse(read["countryId"].ToString())
                    });
                }
                return users;
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
            finally
            {
                read.Dispose();
                read.Close();
                con.Dispose();
                con.Close();
            }
        }

        public useradmin getUserAccountInformation(useradmin _user)
        {

            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            SqlDataReader read = null;
            try
            {
                List<useradmin> users = new List<useradmin>();
                read = SqlHelper.ExecuteReader(con, "PROC_GET_USERBYLOGIN", _user.LOGIN_ID);
                while (read.Read())
                {
                    users.Add(new useradmin
                    {
                        ID = int.Parse(read["id"].ToString()),
                        LOGIN_ID = read["LOGIN_ID"].ToString(),
                        PASSWD = read["PASSWORD"].ToString(),
                        ORG_ID = int.Parse(read["ORG_ID"].ToString()),
                        JOB_TITLE_ID = int.Parse(read["JOB_TITLE_ID"].ToString()),
                        NAME = read["NAME"].ToString(),
                        EMAIL_ADDRESS = read["EMAIL_ADDRESS"].ToString(),
                        ACTIVE = bool.Parse(read["ACTIVE"].ToString()),
                        REPORTS_TO = int.Parse(read["REPORTS_TO"].ToString()),
                        USER_TYPE = int.Parse(read["USER_TYPE"].ToString())
                    });
                }
                return (users.Count > 0) ? users.Single() : new useradmin();
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
            finally
            {
                read.Dispose();
                read.Close();
                con.Dispose();
                con.Close();
            }
        }

        public useradmin getLineManagerUserAccount(int areaId, int areaValueId)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            SqlDataReader read = null;
            try
            {
                List<useradmin> users = new List<useradmin>();
                read = SqlHelper.ExecuteReader(con, "PROC_GET_LINEMANAGERACCOUNT", areaId, areaValueId);
                while (read.Read())
                {
                    users.Add(new useradmin
                    {
                        ID = int.Parse(read["ID"].ToString()),
                        LOGIN_ID = read["LOGIN_ID"].ToString(),
                        PASSWD = (read["PASSWORD"] != DBNull.Value) ? read["PASSWORD"].ToString() : string.Empty,
                        ORG_ID = (read["ORG_ID"] != DBNull.Value) ?
                                        int.Parse(read["ORG_ID"].ToString()) : 0,
                        JOB_TITLE_ID = (read["JOB_TITLE_ID"] != DBNull.Value) ?
                                        int.Parse(read["JOB_TITLE_ID"].ToString()) : 0,
                        NAME = (read["NAME"] != DBNull.Value) ? read["NAME"].ToString() : string.Empty,
                        EMAIL_ADDRESS = (read["EMAIL_ADDRESS"] != DBNull.Value) ? read["EMAIL_ADDRESS"].ToString() : string.Empty,
                        ACTIVE = (read["ACTIVE"] != DBNull.Value) ? bool.Parse(read["ACTIVE"].ToString()) : false,
                        REPORTS_TO = (read["REPORTS_TO"] != DBNull.Value) ?
                                        int.Parse(read["REPORTS_TO"].ToString()) : 0,
                        USER_TYPE = (read["USER_TYPE"] != DBNull.Value) ?
                                        int.Parse(read["USER_TYPE"].ToString()) : 0
                    });
                }
                return (users.Count > 0) ? users.Single() : new useradmin();
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
            finally
            {
                read.Dispose();
                read.Close();
                con.Dispose();
                con.Close();
            }
        }

        public useradmin getUserAccountById(useradmin _user)
        {

            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            SqlDataReader read = null;
            try
            {
                List<useradmin> users = new List<useradmin>();
                read = SqlHelper.ExecuteReader(con, "PROC_GET_USERBYID", _user.ID);
                while (read.Read())
                {
                    users.Add(new useradmin
                    {
                        ID = int.Parse(read["id"].ToString()),
                        LOGIN_ID = read["LOGIN_ID"].ToString(),
                        PASSWD = read["PASSWORD"].ToString(),
                        ORG_ID = int.Parse(read["ORG_ID"].ToString()),
                        JOB_TITLE_ID = int.Parse(read["JOB_TITLE_ID"].ToString()),
                        NAME = read["NAME"].ToString(),
                        EMAIL_ADDRESS = read["EMAIL_ADDRESS"].ToString(),
                        ACTIVE = bool.Parse(read["ACTIVE"].ToString()),
                        REPORTS_TO = int.Parse(read["REPORTS_TO"].ToString()),
                        USER_TYPE = int.Parse(read["USER_TYPE"].ToString())
                    });
                }
                return (users.Count > 0) ? users.Single() : new useradmin();
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
            finally
            {
                read.Dispose();
                read.Close();
                con.Dispose();
                con.Close();
            }
        }

        public int deactivateUserAccount(useradmin _user)
        {

            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try
            {
                return SqlHelper.ExecuteNonQuery(con, "PROC_DEACTIVATE_USER",
                     _user.ID
                 );
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return 0;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public int updatePassword(useradmin user)
        {

            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try
            {
                return (int)SqlHelper.ExecuteScalar(con, "PROC_UPDATE_USER_PASSWORD",
                     user.ID, user.PASSWD
                 );
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return 0;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        #endregion
    }
}