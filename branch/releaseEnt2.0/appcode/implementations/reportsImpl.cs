﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using scorecard.interfaces;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using scorecard.entities;
using System.Data;

/* @Copy Albertoncaffeine.net */

namespace scorecard.implementations
{
    public class reportsImpl : Ireports
    {

        #region Ireports Members

        public List<roleentityreport> getReportData(useradmin _user)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            SqlDataReader read = null;
            try
            {
                List<roleentityreport> reports = new List<roleentityreport>();
                read = SqlHelper.ExecuteReader(con, "PROC_GET_USER_REPORTS", _user.ID);
                while (read.Read())
                {
                    reports.Add(new roleentityreport
                    {
                       CRITERIA_TYPE_ID = read.GetInt32(0),
                       SCORES = read.GetString(1),
                       FINAL_SCORE = read.GetDouble(2),
                       AREA_TYPE_ID = read.GetInt32(3),
                       AREA_ID = read.GetInt32(4),
                       GOAL_DESCRIPTION = read.GetString(5),
                       TARGET = read.GetString(6),
                       WEIGHT = read.GetInt32(7),
                       OBJECTIVE = read.GetString(8),
                       NAME = read.GetString(9),
                       EMAIL_ADDRESS = read.GetString(10),
                       JOB_TITLE = read.GetString(11),
                       USER_ID = read.GetInt32(12),
                       GOAL_ID = read.GetInt32(13),
                       STRATEGIC = read.GetInt32(14)
                    });
                }
                return reports;
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
            finally
            {
                read.Dispose();
                read.Close();
                con.Dispose();
                con.Close();
            }
        }

        public List<entities.roleentityreport> getReportData(entities.Orgunit _org)
        {
            throw new NotImplementedException();
        }

        public DataTable getCustomerReport()
        {
            return SqlHelper.ExecuteDataset(Util.GetconnectionString(), "PROC_GET_CUSTOMER_REPORT").Tables[0];
        }


        public DataTable getDepartmentReport(string startdate, string  enddate,string company)
        {
            return SqlHelper.ExecuteDataset(Util.GetconnectionString(), "PROC_GET_DEPARTMENT_REPORT",startdate,enddate ,company ).Tables[0];
        }

        public DataTable getEmpJobReport(string  startdate, string enddate, string company, string  jobtitle)
        {
            return SqlHelper.ExecuteDataset(Util.GetconnectionString(), "PROC_GET_USERJOB_REPORT", startdate , enddate , company , jobtitle).Tables[0];
        }

        public DataTable getEmpDeptReport(string startdate, string enddate, string company, string deptName)
        {
            return SqlHelper.ExecuteDataset(Util.GetconnectionString(), "PROC_GET_USERDEPT_REPORT", startdate, enddate, company, deptName ).Tables[0];
        }


        #endregion


    }
}