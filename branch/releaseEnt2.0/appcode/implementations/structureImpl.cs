﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using scorecard.interfaces;
using scorecard.entities;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;
using HR.Human.Resources;
using System.Collections;
using System.Text;


/* @Copy Albertoncaffeine.net */

namespace scorecard.implementations
{
    public class structureImpl : Iorgunit
    {
        #region Iorgunit Members

        public int addOrgUnit(Orgunit _orgunit)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try
            {
                return (int)SqlHelper.ExecuteScalar(con, "PROC_ADD_ORG_UNIT",
                 _orgunit.COMPANY_ID,
                 _orgunit.ORGUNIT_NAME,
                 _orgunit.PARENT_ORG,
                 _orgunit.ORGUNT_TYPE,
                 _orgunit.OWNER_ID,
                 _orgunit.CREATED_BY
                 );
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return 0;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public int updateOrgUnit(Orgunit _orgunit)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try
            {
                return (int)SqlHelper.ExecuteScalar(con, "PROC_UPDATE_ORG_UNIT",
                  _orgunit.ID,
                 _orgunit.ORGUNIT_NAME,
                 _orgunit.PARENT_ORG,
                 _orgunit.ORGUNT_TYPE,
                 _orgunit.OWNER_ID,
                 _orgunit.UPDATED_BY
                 );
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return 0;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public int deleteOrgUnit(Orgunit _orgunit)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try
            {
                return (int)SqlHelper.ExecuteScalar(con, "PROC_DELETE_COMPANY_ORG_UNIT",
                  _orgunit.ID,
                  _orgunit.UPDATED_BY,
                  _orgunit.ACTIVE
                 );
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return 0;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public DataSet getCompanyStructure(company _company)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try
            {
                return SqlHelper.ExecuteDataset(con, "PROC_GET_ALL_ORGUNITS_BY_COMPANY", _company.COMPANY_ID);
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
            finally
            {
                con.Dispose();
                con.Close();
            }
        }

        string GetActiveOrgId(DataTable dS)
        {
            ArrayList parent = new ArrayList();
            ArrayList idS = new ArrayList();
            StringBuilder strB = new StringBuilder("");

            foreach (DataRow rw in dS.Rows)
            {
                idS.Add(rw["ID"].ToString());
                parent.Add(rw["PARENT_ORG"].ToString());
            }

            foreach (object item in parent)
                if (!idS.Contains(item.ToString()) &&
                    !item.ToString().Equals("0"))
                    strB.Append(string.Format("{0},", item));

            string strVal = string.Empty;
            string activeIds = strB.ToString();
            int lIdx = activeIds.LastIndexOf(',');

            if (activeIds.Length > 0)
                strVal = activeIds.Substring(0, lIdx);
            return strVal;
        }

        public DataView getActiveCompanyStructure(int companyId)
        {
            return SqlHelper.ExecuteDataset(Util.GetconnectionString(), "PROC_GET_ALL_ACTIVE_ORGUNITS_BY_COMPANY", companyId).Tables[0].DefaultView;
        }

        public DataView getActiveCompanyStructure(company _company)
        {
            DataTable dT = getCompanyStructure(_company).Tables[0];
            string inactiveIds = GetActiveOrgId(dT);

            DataView dV = dT.DefaultView;
            if (!string.IsNullOrEmpty(inactiveIds))
                dV.RowFilter = string.Format("NOT (PARENT_ORG IN ({0}))", inactiveIds);
            return dV;
        }

        public List<Orgunit> getCompanyStructureListView(company _company)
        {

            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            SqlDataReader read = null;
            try
            {
                List<Orgunit> orgunits = new List<Orgunit>();
                read = SqlHelper.ExecuteReader(con, "PROC_GET_ALL_ORGUNITS_BY_COMPANY", _company.COMPANY_ID);
                while (read.Read())
                {
                    orgunits.Add(new Orgunit
                    {
                        ID = read.GetInt32(0),
                        COMPANY_ID = read.GetInt32(1),
                        ORGUNIT_NAME = read.GetString(2),
                        PARENT_ORG = read.GetInt32(3),
                        ORGUNT_TYPE = read.GetInt32(4),
                        DATE_CREATED = read.GetDateTime(5),
                        OWNER_ID = read.GetInt32(6)
                    });
                }
                return orgunits;
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
            finally
            {
                read.Dispose();
                read.Close();
                con.Dispose();
                con.Close();
            }
        }

        public List<Orgunit> getOrgstructureByParentID(Orgunit _unit)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            SqlDataReader read = null;
            try
            {
                List<Orgunit> orgunits = new List<Orgunit>();
                read = SqlHelper.ExecuteReader(con, "PROC_GET_ALL_ORGUNITS_BY_PARENT_ID", _unit.PARENT_ORG);
                while (read.Read())
                {
                    orgunits.Add(new Orgunit
                    {
                        ID = read.GetInt32(0),
                        COMPANY_ID = read.GetInt32(1),
                        ORGUNIT_NAME = read.GetString(2),
                        PARENT_ORG = read.GetInt32(3),
                        ORGUNT_TYPE = read.GetInt32(4),
                        DATE_CREATED = read.GetDateTime(5),
                        OWNER_ID = read.GetInt32(6),
                        Owner = new Employee
                        {
                            FullName = string.Format("{0} {1} {2}", read["FirstName"], read["MiddleName"], read["LastName"])
                        }
                    });
                }
                return orgunits;
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
            finally
            {
                read.Dispose();
                read.Close();
                con.Dispose();
                con.Close();
            }
        }

        public Orgunit getOrgunitDetails(Orgunit _unit)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            SqlDataReader read = null;
            try
            {
                List<Orgunit> orgunits = new List<Orgunit>();
                read = SqlHelper.ExecuteReader(con, "PROC_GET_ALL_ORGUNIT_BY_ID", _unit.ID);
                while (read.Read())
                {
                    orgunits.Add(new Orgunit
                    {
                        ID = read.GetInt32(0),
                        COMPANY_ID = read.GetInt32(1),
                        ORGUNIT_NAME = read.GetString(2),
                        PARENT_ORG = read.GetInt32(3),
                        ORGUNT_TYPE = read.GetInt32(4),
                        DATE_CREATED = read.GetDateTime(5),
                        OWNER_ID = read.GetInt32(6)
                    });
                }
                return orgunits.ElementAt(0);
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
            finally
            {
                read.Dispose();
                read.Close();
                con.Dispose();
                con.Close();
            }
        }

        public int asignJobTitle(Orgunit _unit, JobTitle _title)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try
            {
                return SqlHelper.ExecuteNonQuery(con, "PROC_ADD_ORGUNIT_JOB_TITLES",
                  _unit.ID, _title.ID
                 );
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return 0;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public int deleteAllJobTitleAsignments(Orgunit _unit)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try
            {
                return SqlHelper.ExecuteNonQuery(con, "PROC_DELETE_ALL_JOB_TITLE_ASIGNMENTS",
                  _unit.ID
                 );
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return 0;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public DataSet getAllJobTitleAsignments(Orgunit _unit)
        {
            SqlConnection con = new SqlConnection(Util.GetconnectionString());
            try
            {
                return SqlHelper.ExecuteDataset(con, "PROC_GET_ALL_ORG_UNIT_JOBTILES", _unit.ID);
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                return null;
            }
            finally
            {
                con.Dispose();
                con.Close();
            }
        }

        #endregion
    }
}