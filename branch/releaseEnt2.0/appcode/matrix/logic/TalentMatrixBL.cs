﻿using System;
using System.Data;
using System.Collections.Generic;
using scorecard.implementations;

namespace HR.Human.Resources
{
    public class TalentMatrixBL : ITalentMatrix
    {
        #region Properties

        public string ConnectionString
        {
            get { return Util.GetconnectionString(); }
        }

        public TalentMatrixDAL Dal
        {
            get { return new TalentMatrixDAL(ConnectionString); }
        }

        #endregion

        #region Default Constructors

        public TalentMatrixBL() { }

        #endregion

        #region Public Methods & Functions

        public bool AddTalentMatrix(TalentMatrix obj)
        {
            int result = Dal.Save(obj);
            return result > 0 ? true : false;
        }

        public bool UpdateTalentMatrix(TalentMatrix obj)
        {
            int result = Dal.Update(obj);
            return result > 0 ? true : false;
        }

        public bool DeactivateMatrix(TalentMatrix obj)
        {
            int result = Dal.Deactivate(obj);
            return result > 0 ? true : false;
        }

        public TalentMatrix GetTalentMatrixRow(int matrixId)
        {
            DataTable dT = Dal.GetById(matrixId);
            return (dT.Rows.Count > 0) ? GetRow(dT.Rows[0]) : new TalentMatrix();
        }

        public List<TalentMatrix> GetAllTalentMatrix(int companyId)
        {
            List<TalentMatrix> obj = new List<TalentMatrix>();
            DataTable dT = Dal.GetAll(companyId);
            if (dT.Rows.Count <= 0) return obj;
            foreach (DataRow rw in dT.Rows) obj.Add(GetRow(rw));
            return obj;
        }

        public List<TalentMatrix> GetActiveTalentMatrix(int companyId, bool active)
        {
            List<TalentMatrix> obj = new List<TalentMatrix>();
            DataTable dT = Dal.Get(companyId, active);
            if (dT.Rows.Count <= 0) return obj;
            foreach (DataRow rw in dT.Rows) obj.Add(GetRow(rw));
            return obj;
        }

        public TalentMatrix GetRow(DataRow rw)
        {
            DateTime? defDate = null;
            return new TalentMatrix
            {
                Active = bool.Parse(rw["Active"].ToString()),
                CompanyId = int.Parse(rw["CompanyId"].ToString()),
                CreatedBy = rw["CreatedBy"].ToString(),
                DateCreated = DateTime.Parse(rw["DateCreated"].ToString()),
                DateUpdated = (rw["DateUpdated"] != DBNull.Value) ? DateTime.Parse(rw["DateUpdated"].ToString()) : defDate,
                LowerBound = decimal.Parse(rw["LowerBound"].ToString()),
                LowerBoundSign = rw["LowerBoundSign"].ToString(),
                MatrixId = int.Parse(rw["MatrixId"].ToString()),
                PositionInMatrix = int.Parse(rw["PositionInMatrix"].ToString()),
                TalentMatrixDescription = (rw["TalentMatrixDescription"] != DBNull.Value) ?
                                            rw["TalentMatrixDescription"].ToString() : string.Empty,
                TalentMatrixName = rw["TalentMatrixName"].ToString(),
                UpdatedBy = (rw["UpdatedBy"] != DBNull.Value) ?
                rw["UpdatedBy"].ToString() : string.Empty,
                UpperBound = decimal.Parse(rw["UpperBound"].ToString()),
                UpperBoundSign = rw["UpperBoundSign"].ToString()
            };
        }

        #endregion
    }
}