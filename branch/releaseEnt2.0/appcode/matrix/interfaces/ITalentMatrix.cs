﻿using System;
using System.Data;
using System.Collections.Generic;

namespace HR.Human.Resources
{
    public interface ITalentMatrix
    {
        bool AddTalentMatrix(TalentMatrix obj);
        bool UpdateTalentMatrix(TalentMatrix obj);
        bool DeactivateMatrix(TalentMatrix obj);

        TalentMatrix GetRow(DataRow rw);
        TalentMatrix GetTalentMatrixRow(int matrixId);

        List<TalentMatrix> GetAllTalentMatrix(int companyId);
        List<TalentMatrix> GetActiveTalentMatrix(int companyId, bool active);
    }
}
