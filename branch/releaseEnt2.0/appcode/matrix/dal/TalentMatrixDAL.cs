﻿using System;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using HR.Human.Resources.Security;

namespace HR.Human.Resources
{
    public class TalentMatrixDAL
    {
        #region Variables

        private string _conString;

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return _conString; }
            set { _conString = value; }
        }

        #endregion

        #region Default Constructors

        public TalentMatrixDAL(string conString)
        {
            _conString = conString;
        }

        #endregion

        #region Public Methods and Functions

        public DataTable Get(int companyId, bool active)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_TalentMatrix_GetByCompanyId", companyId, active).Tables[0];
        }

        public DataTable GetAll(int companyId)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_TalentMatrix_GetAll", companyId).Tables[0];
        }

        public DataTable GetById(int matrixId)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_TalentMatrix_GetById", matrixId).Tables[0];
        }

        public int Save(TalentMatrix obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_TalentMatrix_Insert", obj.CompanyId, obj.TalentMatrixName,
                                                            obj.TalentMatrixDescription, obj.LowerBoundSign, obj.LowerBound, obj.UpperBoundSign,
                                                            obj.UpperBound, obj.Active, obj.CreatedBy, obj.PositionInMatrix);
            return result;
        }

        public int Update(TalentMatrix obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_TalentMatrix_Update", obj.MatrixId, obj.CompanyId, obj.TalentMatrixName,
                                                            obj.TalentMatrixDescription, obj.LowerBoundSign, obj.LowerBound, obj.UpperBoundSign,
                                                            obj.UpperBound, obj.Active, obj.UpdatedBy, obj.PositionInMatrix);
            return result;
        }


        public int Deactivate(TalentMatrix obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_TalentMatrix_SetStatus", obj.MatrixId, obj.Active, 
                                                        obj.UpdatedBy, obj.PositionInMatrix);
            return result;
        }
        #endregion
    }
}