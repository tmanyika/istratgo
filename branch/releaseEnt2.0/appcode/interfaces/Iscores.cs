﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using scorecard.entities;
using System.Data;

/* @Copy Albertoncaffeine.net */

namespace scorecard.interfaces
{
    public interface Iscores
    {
        List<scores> getScoresByGoalID(goals _goal);
        List<submissionWorkflow> getApprovalWorkflowItems(submissionWorkflow _approver);
        List<submissionWorkflow> getApprovalWorkflowItems();
        DataTable getApprovalWorkflowItems(int companyId, int userId, bool isAdmin);
        List<attachmentDocuments> getAllAttachments(submissionWorkflow _wrkflow);

        int createNewScore(scores _scores);
        int updateScore(scores _scores);
        int deleteScoresByGoalID(goals _goal);
        int updateWorkflowStatus(submissionWorkflow _flow);
        int createAttachment(attachmentDocuments _attachment);
        int createSubmissionWorkflow(submissionWorkflow _workflow);
        int updateSubmissionWorkflow(submissionWorkflow _workflow);
        int updateWorkflowItem(scores _flow);
        int updateWorkflowComment(submissionWorkflow _flow);

        List<scores> getUsedScoreDates(int criteriaId, int itemValueId);
        List<scores> getSubmittedScores(submissionWorkflow _flow);
        List<reportexception> getScoreExceptionReport(int orgUnitId, string date);
        List<scores> getScoreReport(int criteriaId, int itemValueId, string date);
        List<scores> getScoreReport(int criteriaId, int itemValueId, string sdate, string edate);

        List<PerformanceReport> getEmployeeScorePerformanceReport(int criteriaId, int itemValueId, string sdate, string edate);
        List<PerformanceReport> getJobTitleScorePerformanceReport(int criteriaId, int itemValueId, string sdate, string edate);
        List<PerformanceReport> getDepartmentUsersScorePerformanceReport(int criteriaId, int itemValueId, string sdate, string edate);
        List<PerformanceReport> getDepartmentScorePerformanceReport(int criteriaId, int itemValueId, string sdate, string edate, bool isCompany);

        List<scores> getAggregateReport(int criteriaId, string areaValueId, string date);
        List<scores> getScoreDates(int criteriaId, int itemValueId);
        List<scores> getScoreDates(int compId, bool isCompId);
        List<scores> getReportDates(int criteriaId, string xmlData);
        totals getSubmittedScoreTotals(submissionWorkflow _flow);

        DataTable getSavedAndDeclinedSubmissions(int capturerId);
        int deleteAttachment(int attachmentId);

        int deleteSubmittedScore(int submitId);
    }
}