﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using scorecard.entities;
using System.Data;

/* @Copy Albertoncaffeine.net */

namespace scorecard.interfaces
{
    public interface Iorgunit
    {
         int addOrgUnit(Orgunit _orgunit);
         int updateOrgUnit(Orgunit _orgunit);
         int deleteOrgUnit(Orgunit _orgunit);

         DataSet getCompanyStructure(company _company);
         DataView getActiveCompanyStructure(company _company);
         DataView getActiveCompanyStructure(int companyId);

         List<Orgunit> getCompanyStructureListView(company _company);
         List<Orgunit> getOrgstructureByParentID(Orgunit _unit);
         Orgunit getOrgunitDetails(Orgunit _unit);
         int asignJobTitle(Orgunit _unit, JobTitle _title);
         int deleteAllJobTitleAsignments(Orgunit _unit);
         DataSet getAllJobTitleAsignments(Orgunit _unit);             
    }
}