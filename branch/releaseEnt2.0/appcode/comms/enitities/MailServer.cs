﻿using System;

namespace System.Email.Communication
{
    public class MailServer
    {
        public int AccountMailId { get; set; }
        public int Port { get; set; }

        public string SqlProfileName { get; set; }
        public string AccountName { get; set; }
        public string UserName { get; set; }
        public string PIN { get; set; }
        public string SmtpServer { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public bool Active { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }

    }
}