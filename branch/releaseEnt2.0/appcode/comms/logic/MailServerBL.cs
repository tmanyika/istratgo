﻿using System;
using System.Collections.Generic;
using scorecard.implementations;
using System.Data;

namespace System.Email.Communication
{
    public class MailServerBL : IMailServer
    {
        #region Properties

        public string ConnectionString
        {
            get { return Util.GetconnectionString(); }
        }

        public MailServerDAL Dal
        {
            get { return new MailServerDAL(ConnectionString); }
        }

        #endregion

        #region Default Constructors

        public MailServerBL() { }

        #endregion

        #region Public Finctions And Methods

        public bool AddMailServer(MailServer obj)
        {
            int result = Dal.Save(obj);
            return result > 0 ? true : false;
        }

        public bool Delete(int mailId, string updatedBy)
        {
            int result = Dal.Deactivate(mailId, updatedBy);
            return result > 0 ? true : false;
        }

        public List<MailServer> GetByStatus(bool active)
        {
            List<MailServer> obj = new List<MailServer>();
            DataTable dT = Dal.Get(active);

            if (dT.Rows.Count <= 0) return obj;
            foreach (DataRow rw in dT.Rows)
                obj.Add(GetRecord(rw));
            return obj;
        }

        public List<MailServer> GetAll()
        {
            List<MailServer> obj = new List<MailServer>();
            DataTable dT = Dal.GetAll();

            if (dT.Rows.Count <= 0) return obj;
            foreach (DataRow rw in dT.Rows)
                obj.Add(GetRecord(rw));
            return obj;
        }

        public MailServer GetById(int mailId)
        {
            DataTable dT = Dal.GetById(mailId);
            if (dT.Rows.Count <= 0) return new MailServer();
            DataRow rw = dT.Rows[0];
            return GetRecord(rw);
        }

        public MailServer GetRecord(DataRow rw)
        {
            DateTime? defDate = null;
            return new MailServer
            {
                AccountMailId = int.Parse(rw["AccountMailId"].ToString()),
                AccountName = rw["AccountName"].ToString(),
                SqlProfileName = rw["SqlProfileName"].ToString(),
                SmtpServer = rw["SmtpServer"].ToString(),
                UserName = rw["UserName"].ToString(),
                Active = bool.Parse(rw["Active"].ToString()),
                Port = int.Parse(rw["Port"].ToString()),
                PIN = (rw["PIN"].ToString() != "") ? rw["PIN"].ToString().Trim() : "",
                CreatedBy = rw["Createdby"].ToString(),
                DateCreated = DateTime.Parse(rw["Datecreated"].ToString()),
                DateUpdated = (rw["Dateupdated"] != DBNull.Value) ? DateTime.Parse(rw["Dateupdated"].ToString()) : defDate,
                UpdatedBy = (rw["Updatedby"] != DBNull.Value) ? rw["Updatedby"].ToString() : string.Empty
            };
        }

        #endregion
    }
}