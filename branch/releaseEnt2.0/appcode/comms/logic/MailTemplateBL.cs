﻿using System;
using System.Collections.Generic;
using scorecard.implementations;
using System.Data;

namespace System.Email.Communication
{
    public class MailTemplateBL : IMailTemplate
    {
        #region Properties

        public string ConnectionString
        {
            get { return Util.GetconnectionString(); }
        }

        public MailTemplateDAL Dal
        {
            get { return new MailTemplateDAL(ConnectionString); }
        }

        #endregion

        public MailTemplateBL() { }

        public bool AddMailTemplate(MailTemplate obj)
        {
            int result = Dal.Save(obj);
            return result > 0 ? true : false;
        }

        public bool UpdateMailTemplate(MailTemplate obj)
        {
            int result = Dal.Update(obj);
            return result > 0 ? true : false;
        }

        public bool Delete(int mailId, string updatedBy)
        {
            int result = Dal.Deactivate(mailId, updatedBy);
            return result > 0 ? true : false;
        }

        public List<MailTemplate> GetMasterTemplates(bool active, bool ismaster)
        {
            List<MailTemplate> obj = new List<MailTemplate>();
            DataTable dT = Dal.Get(active, ismaster);

            if (dT.Rows.Count <= 0) return obj;
            foreach (DataRow rw in dT.Rows)
                obj.Add(GetRecord(rw));

            return obj;
        }

        public List<MailTemplate> GetByStatus(bool active)
        {
            List<MailTemplate> obj = new List<MailTemplate>();
            DataTable dT = Dal.Get(active);

            if (dT.Rows.Count <= 0) return obj;
            foreach (DataRow rw in dT.Rows)
                obj.Add(GetRecord(rw));

            return obj;
        }

        public List<MailTemplate> GetAll()
        {
            List<MailTemplate> obj = new List<MailTemplate>();
            DataTable dT = Dal.GetAll();

            if (dT.Rows.Count <= 0) return obj;
            foreach (DataRow rw in dT.Rows)
                obj.Add(GetRecord(rw));

            return obj;
        }

        public MailTemplate GetById(int mailId)
        {
            DataTable dT = Dal.GetById(mailId);
            if (dT.Rows.Count <= 0) return new MailTemplate();
            DataRow rw = dT.Rows[0];
            return GetRecord(rw);
        }

        MailTemplate GetRecord(DataRow rw)
        {
            DateTime? defDate = null;
            int? defInt = null;
            return new MailTemplate
            {
                MailId = int.Parse(rw["mailId"].ToString()),
                TemplateId = (rw["templateId"] == DBNull.Value) ? defInt : int.Parse(rw["templateId"].ToString()),
                MailName = rw["mailName"].ToString(),
                MailSubject = rw["mailSubject"].ToString(),
                MailContent = rw["mailContent"].ToString(),
                Active = bool.Parse(rw["active"].ToString()),
                IsMasterTemplate = bool.Parse(rw["IsMasterTemplate"].ToString()),
                CreatedBy = rw["createdby"].ToString(),
                DateCreated = DateTime.Parse(rw["datecreated"].ToString()),
                DateUpdated = (rw["dateupdated"] != DBNull.Value) ? DateTime.Parse(rw["dateupdated"].ToString()) : defDate,
                UpdatedBy = (rw["updatedby"] != DBNull.Value) ? rw["updatedby"].ToString() : string.Empty
            };
        }
    }
}