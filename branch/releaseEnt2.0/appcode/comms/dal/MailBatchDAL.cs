﻿using System;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

namespace System.Email.Communication
{
    public class MailBatchDAL
    {
        #region Variables

        private string _conString;

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return _conString; }
            set { _conString = value; }
        }

        #endregion

        #region Default Constructors

        public MailBatchDAL(string conString)
        {
            _conString = conString;
        }

        #endregion

        #region Public Methods & Functions

        public DataTable GetAll()
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_MailBatch_GetAll").Tables[0];
        }

        public DataTable GetById(int batchId)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_MailBatch_GetById", batchId).Tables[0];
        }

        public int Save(MailBatch obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_MailBatch_Insert", obj.AccountMailId, obj.BatchName, obj.MailSubject,
                                                        obj.MailContent, obj.MailId, obj.NoOfUsers, obj.DateToBeSent, obj.SendNow,
                                                        obj.UserRoles, obj.CreatedBy);
            return result;
        }

        public int SaveMailingList(int batchId, string xmlData)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_MailBatch_InsertMailingList", batchId, xmlData);
            return result;
        }

        public string GetRoleList(int batchId)
        {
            string roleList = (string)SqlHelper.ExecuteScalar(ConnectionString, "Proc_MailBatch_GetRoleList", batchId);
            return roleList;
        }

        public DataTable GetAddresses(int batchId)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_MailBatch_GetAddressByBatchId", batchId).Tables[0];
        }

        #endregion
    }
}