﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using scorecard.implementations;
using scorecard.welcome.interfaces;
using scorecard.welcome.dal;
using System.Data;

namespace scorecard.welcome.logic
{
    public class WelcomeBL : IWelcome
    {
        #region Properties

        public string ConnectionString
        {
            get { return Util.GetconnectionString(); }
        }

        public WelcomeDAL Dal
        {
            get { return new WelcomeDAL(ConnectionString); }
        }

        #endregion

        #region Default Constructors

        public WelcomeBL() { }

        #endregion

        #region Public Methods & Functions

        public Welcome GetById(int welcomeId)
        {
            DataTable dT = Dal.Get(welcomeId);
            return (dT.Rows.Count > 0) ? GetRow(dT.Rows[0]) : new Welcome();
        }

        public List<Welcome> GetShowAgainWelcome(int employeeId)
        {
            List<Welcome> data = new List<Welcome>();
            DataTable dT = Dal.GetByEmployeeId(employeeId);

            foreach (DataRow rw in dT.Rows)
                data.Add(GetUserRow(rw));
            return data;
        }

        public Welcome GetRow(DataRow rw)
        {
            DateTime? defDate = null;
            return new Welcome
            {
                WelcomeId = rw["WelcomeId"] != DBNull.Value ? int.Parse(rw["WelcomeId"].ToString()) : 0,
                WelcomeSectionName = rw["WelcomeSectionName"] != DBNull.Value ? rw["WelcomeSectionName"].ToString() : string.Empty,
                WelcomeDisplayCaption = rw["WelcomeDisplayCaption"] != DBNull.Value ? rw["WelcomeDisplayCaption"].ToString() : string.Empty,
                VirtualPath = rw["VirtualPath"] != DBNull.Value ? rw["VirtualPath"].ToString() : string.Empty,
                PopupPageName = rw["PopupPageName"] != DBNull.Value ? rw["PopupPageName"].ToString() : string.Empty,
                Position = rw["Position"] != DBNull.Value ? int.Parse(rw["Position"].ToString()) : 0,
                Active = rw["Active"] != DBNull.Value ? bool.Parse(rw["Active"].ToString()) : false,
                DateCreated = rw["DateCreated"] != DBNull.Value ? DateTime.Parse(rw["DateCreated"].ToString()) : DateTime.Now,
                CreatedBy = rw["CreatedBy"] != DBNull.Value ? rw["CreatedBy"].ToString() : string.Empty,
                DateUpdate = rw["DateUpdate"] != DBNull.Value ? DateTime.Parse(rw["DateUpdate"].ToString()) : defDate,
                UpdatedBy = rw["UpdatedBy"] != DBNull.Value ? rw["UpdatedBy"].ToString() : string.Empty
            };
        }

        public Welcome GetUserRow(DataRow rw)
        {
            return new Welcome
            {
                WelcomeId = rw["WelcomeId"] != DBNull.Value ? int.Parse(rw["WelcomeId"].ToString()) : 0,
                WelcomeSectionName = rw["WelcomeSectionName"] != DBNull.Value ? rw["WelcomeSectionName"].ToString() : string.Empty,
                VirtualPath = rw["VirtualPath"] != DBNull.Value ? rw["VirtualPath"].ToString() : string.Empty,
                Active = rw["Active"] != DBNull.Value ? bool.Parse(rw["Active"].ToString()) : false,
                EmployeeId = rw["EmployeeId"] != DBNull.Value ? int.Parse(rw["EmployeeId"].ToString()) : 0,
                ShowAgain = rw["ShowAgain"] != DBNull.Value ? bool.Parse(rw["ShowAgain"].ToString()) : false,
                Position = rw["Position"] != DBNull.Value ? Int16.Parse(rw["Position"].ToString()) : 0,
                WelcomeDisplayCaption = rw["WelcomeDisplayCaption"] != DBNull.Value ? rw["WelcomeDisplayCaption"].ToString() : string.Empty,
                PopupPageName = rw["PopupPageName"] != DBNull.Value ? rw["PopupPageName"].ToString() : string.Empty
            };
        }

        public bool SetEmployeeWelcomeStatus(Welcome obj)
        {
            int result = Dal.Update(obj);
            return result > 0 ? true : false;
        }

        public bool SetAllWelcomeStatus(int employeeId, bool status)
        {
            int result = Dal.Update(employeeId, status);
            return result > 0 ? true : false;
        }

        #endregion
    }
}