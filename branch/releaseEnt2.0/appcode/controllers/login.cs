﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using scorecard.interfaces;
using scorecard.entities;
using scorecard.implementations;

/* @Copy Albertoncaffeine.net */

namespace scorecard.controllers
{
    public class login 
    {
        Iauth authenticator;
        settingsmanager appsettings; 
        public login(Iauth _authenticator) {
            authenticator = _authenticator;
            appsettings = new settingsmanager(new applicationsettingsImpl());
        }
        public useradmin authenticateUser(useradmin user, token token){
           return authenticator.loginUser(user,token);
        }
    }
}