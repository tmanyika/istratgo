﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

namespace HR.Leave
{
    public class LeaveCalendarDAL
    {
        #region Variables

        private string _conString;

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return _conString; }
            set { _conString = value; }
        }

        #endregion

        #region Default Constructors

        public LeaveCalendarDAL(string conString)
        {
            _conString = conString;
        }

        #endregion

        #region Public Methods and Functions

        public DataTable Get(int holidayDateId)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_LeaveCalendar_GetById", holidayDateId).Tables[0];
        }

        public DataTable Get(int countryId, int calendarYear, bool active)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_LeaveCalendar_GetActive", countryId, calendarYear, active).Tables[0];
        }

        public DataTable Get(int orgUnitId, int calendarYear)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_LeaveCalendar_GetCompany", orgUnitId, calendarYear).Tables[0];
        }

        public int Save(LeaveCalendar obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_LeaveCalendar_Insert", obj.CountryId, obj.HolidayDate, obj.Detail,
                                                        obj.CalendarYear, obj.Active, obj.CreatedBy);
            return result;
        }

        public int Update(LeaveCalendar obj)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_LeaveCalendar_Update", obj.HolidayDateId, obj.CountryId, obj.HolidayDate, obj.Detail,
                                                        obj.CalendarYear, obj.Active, obj.UpdatedBy);
            return result;
        }

        public int UpdateLeave(LeaveCalendar obj, int orgUnitId)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_LeaveCalendar_UpdateAccrual", obj.HolidayDateId, orgUnitId, obj.Applied, obj.CreatedBy);
            return result;
        }

        public int Deactivate(int holidayDateId, string updatedBy)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_LeaveCalendar_Deactivate", holidayDateId, updatedBy);
            return result;
        }

        public int GetHolidayDays(DateTime sDate, DateTime eDate, int countryId)
        {
            int result = (int)SqlHelper.ExecuteScalar(ConnectionString, "Proc_LeaveCalendar_GetHolidayDays", sDate, eDate, countryId);
            return result;
        }

        #endregion
    }
}