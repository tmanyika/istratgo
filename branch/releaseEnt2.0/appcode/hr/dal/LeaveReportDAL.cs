﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

namespace HR.Leave
{
    public class LeaveReportDAL
    {
        #region Variables

        private string _conString;

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return _conString; }
            set { _conString = value; }
        }

        #endregion

        #region Default Constructors

        public LeaveReportDAL(string conString)
        {
            _conString = conString;
        }

        #endregion

        #region Public Functions & Methods

        public DataTable Get(int orgUnitId, string leaveTypeXML)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_Leave_GetLeaveBalances", orgUnitId, leaveTypeXML).Tables[0];
        }

        public DataTable GetOnLeave(int orgUnitId, string leaveTypeXML)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_Leave_GetEmployeesOnLeave", orgUnitId, leaveTypeXML).Tables[0];
        }

        public DataTable GetOverThreshold(int orgUnitId, string leaveTypeXML)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_Leave_GetEmployeesOverThreshold", orgUnitId, leaveTypeXML).Tables[0];
        }

        public DataTable GetNegativeLeave(int orgUnitId, string leaveTypeXML)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "Proc_Leave_GetNegativeBalances", orgUnitId, leaveTypeXML).Tables[0];
        }
        
        #endregion
    }
}