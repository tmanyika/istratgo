﻿using System;
using System.Collections.Generic;
using System.Data;

namespace HR.Human.Resources
{
    public interface IEmployeeStatus
    {
        bool AddEmployeeStatus(EmployeeStatus obj);
        bool UpdateEmployeeStatus(EmployeeStatus obj);
        bool Delete(int statusId, string updatedBy);

        EmployeeStatus GetById(int statusId);
        EmployeeStatus GetRecord(DataRow rw);
        List<EmployeeStatus> GetByStatus(bool active);
        List<EmployeeStatus> GetAll();
    }
}
