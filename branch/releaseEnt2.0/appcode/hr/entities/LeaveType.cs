﻿using System;

namespace HR.Leave
{
    public class LeaveType
    {
        public int LeaveId { get; set; }
        public int CompanyId { get; set; }

        public string LeaveName { get; set; }
        public string Description { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public bool Active { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }

    }
}