﻿using System;

namespace HR.Leave
{
    [Serializable]
    public class LeaveCalendarCompany
    {
        public Int64 LinkedId { get; set; }
        public int OrgUnitId { get; set; }
        public bool Applied { get; set; }

    }
}