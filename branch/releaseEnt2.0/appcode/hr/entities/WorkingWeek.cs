﻿using System;

namespace HR.Leave
{
    public class WorkingWeek
    {
        public int WorkingWeekId { get; set; }
        public int CompanyId { get; set; }
        public int NoOfDays { get; set; }

        public string Name { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public bool Active { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
    }
}