﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Universe.Nations
{
    [Serializable]
    public class Country
    {
        public int CountryId { get; set; }
        public string CountryName { get; set; }
    }
}