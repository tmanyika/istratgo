﻿using System;
using System.Data;
using scorecard.entities;
using System.Collections.Generic;

namespace Dashboard.Reporting
{
    public interface IDashboard
    {
        DataTable GetPerfomanceRatings(int areaId, int orgUnitId, bool isCompany, string startDate, string endDate);
        DataTable GetObjectives(int areaId, int orgUnitId, bool isCompany, string startDate, string endDate);
        DataTable GetPerspectives(int areaId, int orgUnitId, bool isCompany, string startDate, string endDate);
        DataTable GetFocusAreas(int areaId, int orgUnitId, bool isCompany, string startDate, string endDate);
        DataTable GetFocusAreas(int areaId, int orgUnitId, bool isCompany);
        DataTable GetTalentMatrix(int areaId, int orgUnitId, bool isCompany, string startDate, string endDate);
        DataTable GetAssessmentRatings(int areaId, int valueId, string startDate, string endDate);
    }
}
