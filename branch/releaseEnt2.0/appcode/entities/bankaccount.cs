﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
/* @Copy Albertoncaffeine.net */
namespace scorecard.entities
{
    public class bankaccount
    {
        public int COMPANY_ID { get; set; }
        public string ACCOUNT_NO { get; set; }
        public string ACCOUNT_TYPE { get; set; }
        public string BANK_NAME { get; set; }
        public string BRANCH_NAME { get; set; }
        public string BRANCH_CODE { get; set; }
    }
}