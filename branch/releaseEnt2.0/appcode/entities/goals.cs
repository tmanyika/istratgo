﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
/* @Copy Albertoncaffeine.net */
namespace scorecard.entities
{
    public class goals : strategic
    {
        public int? PARENT_ID { get; set; }
        public int PROJECT_ID { get; set; }
        public int STRATEGIC_ID { get; set; }
        public int UNIT_MEASURE { get; set; }
        public int WEIGHT { get; set; }
        public int? ENV_CONTEXT_ID { get; set; }

        public bool HAS_SUB_GOALS { get; set; }

        public string MEASURE { get; set; }
        public string GOAL_DESCRIPTION { get; set; }        
        public string UNIT_MEASURE_NAME { get; set; }
        public string TARGET { get; set; }
        public string RATIONAL { get; set; }
        public string ENV_CONTEXT { get; set; }
       
        public DateTime PERIOD_DATE { get; set; }
    }
}