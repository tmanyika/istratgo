﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace scorecard.welcome
{
    public class Welcome : WelcomeState
    {
        public int WelcomeId { get; set; }
        public int Position { get; set; }

        public bool Active { get; set; }

        public string WelcomeSectionName { get; set; }
        public string WelcomeDisplayCaption { get; set; }
        public string VirtualPath { get; set; }
        public string PopupPageName { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public DateTime? DateUpdate { get; set; }
        public DateTime DateCreated { get; set; }
    }
}