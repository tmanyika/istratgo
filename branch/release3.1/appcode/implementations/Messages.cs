﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

namespace scorecard.implementations
{
    public static class Messages
    {

        public static string GetErrorMessage()
        {
            string msg = "The system encountered an error. Please try again";
            return msg;
        }

        public static string GetSaveMessage()
        {
            string msg = "Your information was successfully saved.";
            return msg;
        }

        public static string GetSaveFailedMessage()
        {
            string msg = "The system failed to save your information.";
            return msg;
        }

        public static string GetSaveFailedMessage(string itemName)
        {
            string msg = string.Format("The system failed to save {0} information. Either the email address or user name you supplied already exists.", itemName);
            return msg;
        }

        public static string GetUpdateMessage()
        {
            string msg = "Your information was updated successfully.";
            return msg;
        }

        public static string GetUpdateFailedMessage()
        {
            string msg = "The system failed to update your information.";
            return msg;
        }

        public static string getInvalidMessage(string invalidItem)
        {
            string msg = string.Format("{0} is invalid.", invalidItem);
            return msg;
        }

        public static string GetDeleteMessage()
        {
            string msg = "The record was successfully deleted.";
            return msg;
        }

        public static string GetDeleteFailedMessage()
        {
            string msg = "The system failed to delete the record.";
            return msg;
        }

        public static string GetRecordNotFoundMessage()
        {
            string msg = "There are no records to display.";
            return msg;
        }

        public static string GetLeaveApplicationOverlappingMessaging(DateTime eDate, DateTime sDate, string lastPart)
        {
            string msg = string.Format("The <b>Start Date ({2})</b> or <b>End Date ({0})</b> is overlapping with one of the submitted / in progress / approved application(s){1}", Common.GetDescriptiveDate(eDate, true), lastPart, Common.GetDescriptiveDate(sDate, true));
            return msg;
        }

        public static string GetEmailFailedMessage()
        {
            string msg = "The system failed to send the message.";
            return msg;
        }

        public static string GetEmailSentMessage()
        {
            string msg = "The email was sent successfully.";
            return msg;
        }

        public static string GetNoRecipients()
        {
            string msg = "There are no recipients to send the email to.";
            return msg;
        }

        public static string GetRequiredFieldsMessage(bool useHtmlFormat, params string[] strData)
        {
            StringBuilder strB = new StringBuilder(useHtmlFormat ? "<p><ul>" : string.Empty);
            string msg = "Please supply the following fields:";
            foreach (string strVal in strData)
                strB.Append(useHtmlFormat ? string.Format("<{0} class='errorMsg'>{1}</{0}>", "li", strVal) : strVal);

            strB.Append(useHtmlFormat ? "</ul></p>" : string.Empty);
            return string.Format("{0}{1}{2}", msg, useHtmlFormat ? "<br/>" : string.Empty, strB.ToString());
        }

        public static string GetLeaveAccrualRateErrorMessage()
        {
            string msg = "The leave accrual rate you supplied already exist. You cannot have multiple Leave Type & Working Week combination.";
            return msg;
        }

        public static string GetMailedQueueMessage()
        {
            string msg = "The mail was saved succesfully and queued.";
            return msg;
        }

        public static string GetHolidayDateAppliedSsuccessMessage()
        {
            string msg = "The holiday date was applied successfuly to all applicable employees only.";
            return msg;
        }

        public static string GetHolidayDateApplyFailMessage()
        {
            string msg = "The holiday date could not be applied. Please try again.";
            return msg;
        }

        public static string NumericValueRequired
        {
            get
            {
                string msg = "<br/>only numeric value allowed.";
                return msg;
            }
        }

        public static string YesNoValueRequired
        {
            get
            {
                string msg = "<br/>only Yes or No is allowed.";
                return msg;
            }
        }

        public static string GetAlreadyExistMessage()
        {
            string msg = "The record already exist.";
            return msg;
        }

        public static string YouCannotExceed(string strVal)
        {
            return string.Format("You cannot exceed {0}", strVal);
        }
    }
}