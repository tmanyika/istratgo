﻿using System;
using System.Collections.Generic;
using System.Net.Mail;

namespace HR.Leave
{
    public interface ILeaveApplication
    {
        int SubmitApplication(LeaveApplication obj);
        bool UpdateApplication(LeaveApplication obj);
        bool UpdateStatus(LeaveApplication obj);
        bool ApplicationValid(LeaveApplication obj);

        string BuildEmailData(int applicationId, bool submitting, out string approverName, out string approverEmail, out string status, out MailAddress recip);

        DateTime AddBusinessDays(DateTime dt, int nDays);
        LeaveApplication GetById(int applicationId);

        List<LeaveApplication> GetSubmittedLeave(int approverId, int statusId);
        List<LeaveApplication> GetByApproverId(int approverId);
        List<LeaveApplication> GetByApplicantId(int applicantId);
        List<LeaveApplication> GetByCompanyId(int companyId);
    }
}
