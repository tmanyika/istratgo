﻿using System;
using System.Data;

namespace HR.Leave
{
    public interface ILeaveReport
    {
        DataTable GetLeaveBalances(int orgUnitId, string leaveTypeXML);
        DataTable GetEmployeesOnLeave(int orgUnitId, string leaveTypeXML);
        DataTable GetEmployeesOverThreshold(int orgUnitId, string leaveTypeXML);
        DataTable GetNegativeLeaveBalances(int orgUnitId, string leaveTypeXML);
    }
}
