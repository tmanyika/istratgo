﻿using System;
using System.Data;

using System.Collections.Generic;
using scorecard.entities;
using Microsoft.ApplicationBlocks.Data;

namespace scorecard.implementations
{
    public class ScoreRatingDAL
    {
        #region Variables

        private string _conString;

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return _conString; }
            set { _conString = value; }
        }

        #endregion

        #region Default Constructors

        public ScoreRatingDAL(string conString)
        {
            _conString = conString;
        }

        #endregion

        public int Save(ScoreRating obj)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_ScoreRating_Insert", obj.CompanyId, obj.RatingValue, obj.MinimumValue,
                                                  obj.MaximumValue, obj.RatingDescription, obj.Active, obj.CreatedBy);
            return (result == DBNull.Value) ? 0 : int.Parse(result.ToString());
        }

        public int Update(ScoreRating obj)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_ScoreRating_Update", obj.RatingId, obj.CompanyId, obj.RatingValue, obj.MinimumValue,
                                                  obj.MaximumValue, obj.RatingDescription, obj.Active, obj.UpdatedBy);
            return (result == DBNull.Value) ? 0 : int.Parse(result.ToString());
        }

        public int Deactivate(ScoreRating obj)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_ScoreRating_Deactivate", obj.RatingId, obj.Active, obj.UpdatedBy);
            return (result == DBNull.Value) ? 0 : int.Parse(result.ToString());
        }

        public IDataReader GetAll(int companyId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_ScoreRating_GetByCompanyId", companyId);
        }

        public IDataReader Get(int ratingId)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_ScoreRating_GetById", ratingId);
        }

        public decimal GetMax(int companyId, bool active)
        {
            object result = SqlHelper.ExecuteScalar(ConnectionString, "Proc_ScoreRating_GetMaximumRating", companyId, active);
            return (result == DBNull.Value) ? 0 : decimal.Parse(result.ToString());
        }

        public IDataReader Get(int companyId, bool active)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "Proc_ScoreRating_GetByStatus", companyId, active);
        }
    }
}