﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace scorecard.entities
{
    public class perspective
    {
        public int PERSPECTIVE_ID { get; set; }
        public int COMPANY_ID { get; set; }
        public string PERSPECTIVE_NAME { get; set; }
        public string PERSPECTIVE_DESC { get; set; }
        public string CREATED_BY { get; set; }
        public string UPDATE_BY { get; set; }
        public bool ACTIVE { get; set; }
        public DateTime DATE_CREATED { get; set; }
        public DateTime? DATE_UPDATED { get; set; }
    }
}