﻿using System;
using System.Collections.Generic;
using scorecard.implementations;
using System.Data;
using HR.Human.Resources;

namespace System.Email.Communication
{
    public class MailBatchBL : IMailBatch
    {
        #region Properties

        public string ConnectionString
        {
            get { return Util.GetconnectionString(); }
        }

        public MailBatchDAL Dal
        {
            get { return new MailBatchDAL(ConnectionString); }
        }

        public IMailServer IMail
        {
            get { return new MailServerBL(); }
        }

        #endregion

        #region Default Constructors

        public MailBatchBL() { }

        #endregion

        public int AddMailBatch(MailBatch obj)
        {
            int result = Dal.Save(obj);
            return result;
        }

        public string GetRoleList(int batchId)
        {
            return Dal.GetRoleList(batchId);
        }

        public MailBatch GetById(int batchId)
        {
            DataTable dT = Dal.GetById(batchId);
            if (dT.Rows.Count <= 0) return new MailBatch();
            return GetRow(dT.Rows[0]);
        }

        public List<Employee> GetRecipients(int batchId)
        {
            List<Employee> obj = new List<Employee>();
            DataTable dT = Dal.GetAddresses(batchId);
            if (dT.Rows.Count <= 0) return obj;

            foreach (DataRow rw in dT.Rows)
            {
                string mName = rw["middleName"] == DBNull.Value ? " " : string.Format(" {0} ", rw["middleName"]);
                obj.Add(new Employee
                {
                    FullName = string.Format("{0}{1}{2}", rw["firstName"], mName, rw["lastName"]),
                    EmailAddress = rw["emailAddress"].ToString(),
                    UserName = rw["UserName"].ToString(),
                    EmployeeId = int.Parse(rw["employeeId"].ToString())
                });
            }
            return obj;
        }

        public bool AddMailingList(int batchId, string mailingList)
        {
            try
            {
                int result = Dal.SaveMailingList(batchId, mailingList);
                return result > 0 ? true : false;
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return false;
            }
        }

        public List<MailBatch> GetAll()
        {
            DataTable dT = Dal.GetAll();
            List<MailBatch> obj = new List<MailBatch>();

            if (dT.Rows.Count <= 0) return obj;
            foreach (DataRow rw in dT.Rows)
                obj.Add(GetRow(rw));
            return obj;
        }

        MailBatch GetRow(DataRow rw)
        {
            return new MailBatch
            {
                BatchName = rw["batchName"].ToString(),
                BatchId = int.Parse(rw["batchId"].ToString()),
                AccountMailId = int.Parse(rw["accountMailId"].ToString()),
                MailSubject = rw["mailSubject"].ToString(),
                MailContent = rw["mailContent"].ToString(),
                MainTemplate = rw["mainTemplate"].ToString(),
                NoOfUsers = int.Parse(rw["NoOfUsers"].ToString()),
                IsSent = bool.Parse(rw["isSent"].ToString()),
                DateToBeSent = DateTime.Parse(rw["dateToBeSent"].ToString()),
                CreatedBy = rw["createdBy"].ToString(),
                DateCreated = DateTime.Parse(rw["dateCreated"].ToString()),
                Server = IMail.GetRecord(rw),
                UserRoles = GetRoleList(int.Parse(rw["batchId"].ToString()))
            };
        }
    }
}
