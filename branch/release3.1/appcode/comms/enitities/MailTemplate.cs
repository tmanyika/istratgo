﻿using System;

namespace System.Email.Communication
{
    public class MailTemplate
    {
        public int MailId { get; set; }
        public int? TemplateId { get; set; }

        public string MailName { get; set; }
        public string MailSubject { get; set; }
        public string MailContent { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public bool Active { get; set; }
        public bool IsMasterTemplate { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
    }
}