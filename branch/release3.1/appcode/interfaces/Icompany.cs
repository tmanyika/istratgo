﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using scorecard.entities;
using System.Data;

/* @Copy Albertoncaffeine.net */

namespace scorecard.interfaces
{
    public interface Icompany
    {
        int addCompany(company _companyInfo);
        int updateCompany(company _companyInfo);
        int deleteCompany(company _companyInfo);
        List<company> getAllRegisteredCompanies();
        DataTable getAllRegisteredCompaniesList();
        List<company> getCompanyByCustomer(customer _customer);
        company getCompanyInfo(company _companyInfo);

        int addCompanyAccountDetails(bankaccount account);
        int updateCompanyAccountDetails(bankaccount account);
        bankaccount getCompanyAccountDetails(int companyId);

        bool updateCompanyPaidStatus(int compId, bool paid);
    }
}
