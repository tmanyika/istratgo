﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using scorecard.entities;

namespace scorecard.interfaces
{
    public interface Iperspective
    {
        bool addPerspective(perspective obj);
        bool updatePerspective(perspective obj);
        bool setPerspectiveStatus(perspective obj);

        perspective getPerspective(int projectId);
        List<perspective> getPerspectives(int companyId, bool active);
    }
}
