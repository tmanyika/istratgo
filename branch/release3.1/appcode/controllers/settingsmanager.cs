﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using scorecard.interfaces;
using scorecard.entities;

/* @Copy Albertoncaffeine.net */

namespace scorecard.controllers
{
    public class settingsmanager
    {        
            Isettings settingsManager;
            public settingsmanager(Isettings _settingsManager){
                settingsManager = _settingsManager;
            }
            public List<applicationsettings> getParentSettings(){
                return settingsManager.getParentSettings();
            }
            public List<applicationsettings> getChildSettings(applicationsettings appsettings){
                return settingsManager.getChildSettings(appsettings);
            }
            public applicationsettings getApplicationSetting(applicationsettings appSetting) {
                return settingsManager.getApplicationSetting(appSetting);
            }
            public int AddNewSettings(applicationsettings appSetting){
                return settingsManager.AddNewSettings(appSetting);
            }
            public int DeleteSettings(applicationsettings appSetting){
                return settingsManager.DeleteSettings(appSetting);
            }
            public int UpdateSetting(applicationsettings appSetting){
                return settingsManager.UpdateSetting(appSetting);
            }
        }    
}