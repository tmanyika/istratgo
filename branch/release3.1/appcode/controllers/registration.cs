﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using scorecard.interfaces;
using scorecard.entities;
using scorecard.implementations;

/* @Copy Albertoncaffeine.net */

namespace scorecard.controllers
{
    public class registration
    {
        Icustomer onlineCustomerRegistration;
        companyregistration companyregistration;
        public settingsmanager appsettings;

        public registration(Icustomer _onlineCustomerRegistration)
        {
            onlineCustomerRegistration = _onlineCustomerRegistration;
            companyregistration = new companyregistration(new companyImpl());
            appsettings = new settingsmanager(new applicationsettingsImpl());
        }

        public int registerCustomer(customer _customer, company _company, bankaccount _account)
        {
            return onlineCustomerRegistration.registerCustomer(_customer, _company, _account);
        }

        public int registerCustomer(customer _customer, company _company, out string errMsg)
        {
            return onlineCustomerRegistration.registerCustomer(_customer, _company, out errMsg);
        }

        public int updateCustomerRegistration(customer _customer)
        {
            return onlineCustomerRegistration.updateCustomer(_customer);
        }

        public int deleteCustomerRegistration(customer _customer)
        {
            return onlineCustomerRegistration.deleteCustomer(_customer);
        }

        public customer getRegisteredCustomerInformation(customer _customer)
        {
            return onlineCustomerRegistration.getCustomerInfo(_customer);
        }

        public List<customer> getRegisteredCustomersList()
        {
            return onlineCustomerRegistration.getCustomersList();
        }

    }
}