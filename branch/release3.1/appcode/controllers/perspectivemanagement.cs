﻿using System;
using System.Collections.Generic;
using scorecard.interfaces;
using scorecard.entities;

namespace scorecard.controllers
{
    public class perspectivemanagement
    {
        Iperspective iproj;

        public perspectivemanagement(Iperspective proj)
        {
            this.iproj = proj;
        }

        public bool add(perspective obj)
        {
            return iproj.addPerspective(obj);
        }

        public bool update(perspective obj)
        {
            return iproj.updatePerspective(obj);
        }

        public bool setStatus(perspective obj)
        {
            return iproj.setPerspectiveStatus(obj);
        }

        public perspective get(int projectId)
        {
            return iproj.getPerspective(projectId);
        }

        public List<perspective> get(int companyId, bool active)
        {
            return iproj.getPerspectives(companyId, active);
        }
    }
}