﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using scorecard.interfaces;
using scorecard.entities;


/* @Copy Albertoncaffeine.net */

namespace scorecard.controllers
{
    public class jobtitlesdefinitions
    {
        Ijobtitles titles;

        public jobtitlesdefinitions(Ijobtitles _titles) {
            titles = _titles; 
        }
        public int addJobTitle(JobTitle _title){
            return titles.addJobTitle(_title);
        }
        public int updateJobTitle(JobTitle _title){
            return titles.updateJobTitle(_title);
        }
        public int deleteJobTitle(JobTitle _title){
            return titles.deleteJobTitle(_title);
        }
        public List<JobTitle> getAllCompanyJobTitles(company _company){
            return titles.getAllCompanyJobTitles(_company);
        }
        public JobTitle getJobTitleInfo(JobTitle _title){
            return titles.getJobTitleInfo(_title);
        }

    }
}