﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using scorecard.interfaces;
using scorecard.entities;

namespace scorecard.controllers
{
    public class projectmanagement
    {
        Iproject iproj;

        public projectmanagement(Iproject proj)
        {
            this.iproj = proj;
        }

        public bool add(project obj)
        {
            return iproj.addProject(obj);
        }

        public bool update(project obj)
        {
            return iproj.updateProject(obj);
        }

        public bool setStatus(project obj)
        {
            return iproj.setProjectStatus(obj);
        }

        public project get(int projectId)
        {
            return iproj.getProject(projectId);
        }

        public List<project> get(int companyId, bool active)
        {
            return iproj.getProjects(companyId, active);
        }
    }
}