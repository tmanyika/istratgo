﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using scorecard.implementations;
using scorecard.controllers;
using scorecard.entities;
using vb = Microsoft.VisualBasic;
using HR.Human.Resources;
using System.Collections;

namespace scorecard.ui.reports
{
    public partial class graphAreaGoal : System.Web.UI.UserControl
    {
        #region Variables

        useradmincontroller usersDef;
        goaldefinition goalDef;
        scoresmanager scoresManager;
        structurecontroller orgunits;

        #endregion

        #region Properties

        string Date
        {
            get
            {
                return Request["dVal"].ToString();
            }
        }

        int AreaId
        {
            get
            {
                return int.Parse(Request["aid"].ToString());
            }
        }

        int AreaValueId
        {
            get
            {
                return int.Parse(Request["idv"].ToString());
            }
        }

        #endregion

        #region Default Class Constructors

        public graphAreaGoal()
        {
            goalDef = new goaldefinition(new goalsImpl());
            orgunits = new structurecontroller(new structureImpl());
            usersDef = new useradmincontroller(new useradminImpl());
            scoresManager = new scoresmanager(new scorecard.implementations.scoresImpl());
        }

        #endregion

        #region Page Event Handling Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                fillInfo();
                loadData();
            }
        }

        #endregion

        #region Databinding Methods

        private void fillInfo()
        {
            try
            {
                lblCriteria.Text = usersDef.appsettings.getApplicationSetting(new entities.applicationsettings { TYPE_ID = AreaId }).NAME;
                lblName.Text = getName(AreaValueId, AreaId);
                lblDate.Text = Common.GetDescriptiveDate(Date, false);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        string getName(int id, int criteria)
        {
            try
            {
                if (id > 0)
                {
                    if (criteria == Util.getTypeDefinitionsRolesTypeID())
                    {
                        IEmployee emp = new EmployeeBL();
                        return emp.GetById(id).FullName;
                    }
                    else if (criteria == Util.getTypeDefinitionsProjectID()) return new projectmanagement(new projectImpl()).get(id).PROJECT_NAME;
                    else return orgunits.getOrgunitDetails(new Orgunit { ID = id }).ORGUNIT_NAME;
                }
            }
            catch (Exception e) { Util.LogErrors(e); }
            return "";
        }

        DataTable getData()
        {
            ArrayList obj = new ArrayList();
            DataTable dT = new DataTable();

            dT.Columns.Add("Goal", typeof(string));
            dT.Columns.Add("TargetValue", typeof(double));
            dT.Columns.Add("ActualValue", typeof(double));

            DataRow nRow;
            var data = scoresManager.getScoresReport(AreaId, AreaValueId, Date);
            var goals = data.Where(p => p.PARENT_ID == null).ToList();

            foreach (scores item in goals)
            {
                double target = 0;
                double actual = 0;
                int goalId = item.GOAL_ID;
                string goalName = item.GOAL_DESCRIPTION;
                if (!obj.Contains(goalId))
                {
                    nRow = dT.NewRow();
                    target = goals.Where(c => c.GOAL_ID == goalId).Sum(u => u.WEIGHT);
                    actual = Math.Round(goals.Where(c => c.GOAL_ID == goalId).Sum(u => u.FINAL_SCORE), 2);
                    nRow["Goal"] = vb.Strings.StrConv(item.GOAL_DESCRIPTION, vb.VbStrConv.ProperCase);
                    nRow["TargetValue"] = Math.Round(target, 2);
                    nRow["ActualValue"] = Math.Round(actual, 2);
                    dT.Rows.Add(nRow);
                    nRow = null;
                    obj.Add(goalId);
                }
            }

            nRow = dT.NewRow();
            nRow["Goal"] = "Total";
            nRow["TargetValue"] = goals.Sum(u => u.WEIGHT);
            nRow["ActualValue"] = Math.Round(goals.Sum(u => u.FINAL_SCORE), 2);
            dT.Rows.Add(nRow);
            dT.AcceptChanges();
            return dT;
        }

        void loadData()
        {
            try
            {
                DataTable dT = getData();
                int idx = 0;
                foreach (DataRow rw in dT.Rows)
                {
                    string seriesName = Util.getGraphLabelName(rw["Goal"].ToString());
                    double target = double.Parse(rw["TargetValue"].ToString());
                    double actual = double.Parse(rw["ActualValue"].ToString());

                    orgChart.Series[0].Points.AddY(actual);
                    orgChart.Series[1].Points.AddY(target);
                    orgChart.Series[0].Points[idx].AxisLabel = seriesName;
                    idx++;
                }

                if (dT.Rows.Count >= Util.getGraphMinItems())
                {
                    orgChart.Height = new Unit(Util.getGraphHeight());
                    orgChart.Width = new Unit(Util.getGraphWidth());
                }
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        #endregion
    }
}