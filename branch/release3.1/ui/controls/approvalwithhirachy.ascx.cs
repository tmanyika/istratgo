﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using scorecard.controllers;
using scorecard.entities;
using scorecard.interfaces;
using scorecard.implementations;
using HR.Human.Resources;
using System.Net.Mail;
using System.Email.Communication;
using Scorecard.Business.Rules;

using vb = Microsoft.VisualBasic;

namespace scorecard.ui.controls
{
    public partial class approvalwithhirachy : System.Web.UI.UserControl
    {
        #region Variables

        scoresmanager scoreManager;
        useradmincontroller usersDef;
        goaldefinition goalDef;
        structurecontroller orgunits;
        IBusinessRule bR;
        IScoreRating iR;

        #endregion

        #region Properties

        private UserInfo user
        {
            get { return Util.user; }
        }

        #endregion

        #region Default Constructor

        public approvalwithhirachy()
        {
            scoreManager = new scoresmanager(new scorecard.implementations.scoresImpl());
            usersDef = new useradmincontroller(new useradminImpl());
            goalDef = new goaldefinition(new goalsImpl());
            orgunits = new structurecontroller(new structureImpl());
            bR = new BusinessRuleBL();
            iR = new ScoreRatingBL();
        }

        #endregion

        #region Page Event Handling Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            SetLabels();
            if (!IsPostBack)
            {
                loadAllWorkflows();
                getScoreCardStatus();
            }
        }

        private void SetLabels()
        {
            lblResult.Text = string.Empty;
            lblMsg.Text = "You do not have any pending workflows for your review.";
            lblLstView.Text = string.Empty;
            lblPopupMsg.Text = string.Empty;
        }

        #endregion

        #region Databinding Methods

        void loadAllWorkflows()
        {
            try
            {
                pnlNoData.Visible = false;

                var company_id = user.CompanyId;
                var userRole = user.UserTypeId;
                var userID = user.UserId;
                var isAdmin = (userRole != Util.getTypeDefinitionsUserTypeAdministrator()) ? false : true;
                var workflows = scoreManager.getApprovalWorkflowItems(company_id, userID, isAdmin);
                var recCount = workflows.Rows.Count;

                if (recCount <= 0 || recCount < dtPagerSubmittedWorkflows.PageSize) { dtPagerSubmittedWorkflows.Visible = false; }
                if (recCount <= 0) { pnlNoData.Visible = true; }
                BindControl.BindListView(lvSubmittedWorkflows, workflows);
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        public object GetValue(object dataVal)
        {
            return (dataVal == null) ? string.Empty : dataVal.ToString();
        }

        public string getTypeName(int id)
        {
            return usersDef.appsettings.getApplicationSetting(new entities.applicationsettings { TYPE_ID = id }).NAME;
        }

        public string getApproverName(int id)
        {
            try
            {
                IEmployee emp = new EmployeeBL();
                return emp.GetById(id).FullName; ;
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return "";
            }
        }

        public string getObjectName(int id, int criteria)
        {
            try
            {
                if (id > 0)
                {
                    if (criteria == Util.getTypeDefinitionsRolesTypeID())
                    {
                        IEmployee obj = new EmployeeBL();
                        return obj.GetById(id).FullName;
                    }
                    else if (criteria == Util.getTypeDefinitionsProjectID()) return new projectmanagement(new projectImpl()).get(id).PROJECT_NAME;
                    else return orgunits.getOrgunitDetails(new Orgunit { ID = id }).ORGUNIT_NAME;
                }
            }
            catch (Exception e) { Util.LogErrors(e); }
            return "";
        }

        List<scores> GetSubMeasures(int parentId)
        {
            var data = Cache["Data"] as List<scores>;
            return data.Where(p => p.PARENT_ID == parentId).ToList();
        }

        decimal GetSubMeasureWeight(int parentId)
        {
            try
            {
                var data = Cache["Data"] as List<scores>;
                return data.Where(p => p.ID == parentId).Single().WEIGHT;
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
            return 0;
        }

        void loadGoals(int selectedId, int criteria, int submittedId)
        {
            try
            {
                decimal? defDec = null;
                var data = scoreManager.getSubmittedScores(new submissionWorkflow
                {
                    ID = submittedId,
                    CRITERIA_TYPE_ID = criteria,
                    SELECTED_ITEM_VALUE = selectedId
                });

                Cache["Data"] = data;

                var objectives = data.Where(p => p.PARENT_ID == null).ToList();
                var totalCount = objectives.Count;
                var finalScore = objectives.Sum(u => Math.Round(u.FINAL_SCORE, 2));
                var finalWeightTotal = objectives.Sum(u => u.WEIGHT);
                var finalPercentage = (finalScore / double.Parse(finalWeightTotal.ToString()) * 100);
                var definedWeightedScore = objectives.Sum(u => (u.FINAL_SCORE_AGREED != null) ? u.FINAL_SCORE_AGREED : 0);

                scores totalDetails = new scores
                {
                    GOAL_DESCRIPTION = "<b>Totals</b> ",
                    WEIGHT = finalWeightTotal,
                    FINAL_SCORE = Math.Round(finalPercentage, 2),
                    FINAL_SCORE_AGREED = (definedWeightedScore != null) ? Math.Round((decimal)definedWeightedScore, 2) : defDec,
                    VISIBLE = false,
                    HAS_SUB_GOALS = true
                };

                objectives.Add(totalDetails);
                BindControl.BindListView(lvGoals, objectives);
                getAttachments(submittedId);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void getScoreCardStatus()
        {
            var areaList = goalDef.appsettings.getChildSettings(new applicationsettings { PARENT_ID = Util.getTypeDefinitionsParentWorkFlowApprovalId() }).Where(t => t.TYPE_ID != Util.getTypeDefinitionsWorkFlowNewRequestId()
                && t.TYPE_ID != Util.getTypeDefinitionsWorkFlowSaveForLaterId()).ToList();
            BindControl.BindDropdown(ddlStatus, "NAME", "TYPE_ID", "- select option -", Util.getTypeDefinitionsWorkFlowNewRequestId().ToString(), areaList);
        }

        void loadSetupTypes(DropDownList ddlPerspective)
        {
            try
            {
                var setuptypes = goalDef.appsettings.getChildSettings(new entities.applicationsettings { PARENT_ID = Util.getTypeDefinitionsPerspectivesParentID() });
                BindControl.BindDropdown(ddlPerspective, "NAME", "TYPE_ID", setuptypes);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void loadJobTitles(DropDownList ddlJobTitles)
        {
            try
            {
                IEmployee obj = new EmployeeBL();
                var titles = obj.GetAll(Util.user.OrgId);
                BindControl.BindDropdown(ddlJobTitles, "FullName", "EmployeeId", titles);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        void loadStrategicGoals(DropDownList ddlStrategic)
        {
            try
            {
                var strategic = goalDef.listStrategicGoals(new company { COMPANY_ID = Util.user.CompanyId });
                BindControl.BindDropdown(ddlStrategic, "OBJECTIVE", "ID", strategic);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        public string getCurrentStore(int ID)
        {
            try
            {
                var score = scoreManager.getScoresByGoalID(new entities.goals { ID = ID, PERSPECTIVE = int.Parse(hdnValueId.Value) });
                return score.Single().SCORES.ToString();
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return "";
            }
        }

        public string getFinalStore(int ID)
        {
            try
            {
                var score = scoreManager.getScoresByGoalID(new entities.goals { ID = ID, PERSPECTIVE = int.Parse(hdnValueId.Value) });
                return score.Single().FINAL_SCORE.ToString();
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
                return "0";
            }
        }

        void loadAreas(DropDownList ddlAreas)
        {
            try
            {

                var userRole = Util.user.UserTypeId;
                var userID = int.Parse(Session["userId"].ToString());
                var structure = goalDef.orgstructure.getActiveCompanyOrgStructure(new company { COMPANY_ID = Util.user.CompanyId });

                if (userRole != Util.getTypeDefinitionsUserTypeAdministrator())
                    structure.RowFilter = string.Format("OWNER_ID = {0}", userID);
                BindControl.BindDropdown(ddlAreas, "ORGUNIT_NAME", "ID", structure);
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        #endregion

        #region ListView Event Handling Methods

        protected void lvGoals_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                if (e.CommandName.Equals("updateItem"))
                    UpdateWorkflowItem(e);
            }
        }

        protected void lvSubmittedWorkflows_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                switch (e.CommandName)
                {
                    case "View":
                        var lblItemSelected = e.Item.FindControl("lblItemSelected") as Label;
                        var lblCriteria = e.Item.FindControl("lblCriteria") as Label;
                        var lblSubmittedId = e.Item.FindControl("lblID") as Label;
                        var hdnOverallComment = e.Item.FindControl("hdnOverallComment") as HiddenField;

                        hdnAreaId.Value = lblCriteria.Text.Trim();
                        hdnValueId.Value = lblItemSelected.Text.Trim();
                        hdnSubmissionId.Value = lblSubmittedId.Text;

                        txtComment.Text = hdnOverallComment.Value;
                        lblID.Text = lblSubmittedId.Text;
                        lblArea.Text = getTypeName(int.Parse(hdnAreaId.Value));
                        lblName.Text = getObjectName(int.Parse(hdnValueId.Value), int.Parse(hdnAreaId.Value));

                        loadGoals(int.Parse(hdnValueId.Value), int.Parse(hdnAreaId.Value), int.Parse(lblSubmittedId.Text));
                        mdlShowScoreDetails.Show();
                        break;
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        protected void lvGoals_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListViewItemType.DataItem)
                {
                    int parentId = int.Parse((e.Item.FindControl("lblID") as Label).Text);
                    bool hdnHasSubGoals = bool.Parse((e.Item.FindControl("hdnHasSubGoals") as HiddenField).Value);
                    Repeater rptData = e.Item.FindControl("rptMeasureData") as Repeater;
                    if (rptData != null && hdnHasSubGoals)
                    {
                        var data = GetSubMeasures(parentId);
                        BindControl.BindRepeater(rptData, data);
                        rptData.Visible = (data.Count <= 0) ? false : true;
                    }
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        #endregion

        #region Service Methods

        void getAttachments(int submittedId)
        {
            try
            {
                var attachments = scoreManager.getAllAttachments(new submissionWorkflow
                {
                    ID = submittedId
                });
                dtlstAttachments.DataSource = attachments;
                dtlstAttachments.DataBind();
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        public string getfilePath(string fileName)
        {
            return Util.getConfigurationSettingServerAttachmentsUrl() + fileName;
        }

        bool informationValid(bool saveForLater)
        {
            bool valid = true;
            foreach (ListViewItem item in lvGoals.Items)
            {
                HiddenField hdnHasSubGoals = item.FindControl("hdnHasSubGoals") as HiddenField;
                bool hassubgoals = string.IsNullOrEmpty(hdnHasSubGoals.Value) ? false : bool.Parse(hdnHasSubGoals.Value);
                if (!hassubgoals)
                {
                    HiddenField hdnUnitMeasureId = item.FindControl("hdnUnitMeasureId") as HiddenField;
                    Literal lblError = item.FindControl("lblError") as Literal;
                    Literal lblError2 = item.FindControl("lblError2") as Literal;
                    TextBox txtManagerScore = item.FindControl("txtManagerScore") as TextBox;
                    TextBox txtAgreedScore = item.FindControl("txtAgreedScore") as TextBox;

                    lblError.Text = "";
                    lblError2.Text = "";

                    int unitOfMeasure = int.Parse(hdnUnitMeasureId.Value);
                    string mngval = txtManagerScore.Text;
                    string agreedval = txtAgreedScore.Text;

                    bool mngnotnull = string.IsNullOrEmpty(mngval);
                    bool agreednotnull = string.IsNullOrEmpty(agreedval);
                    bool isnumericval = bR.UnitOfMeasureRequireNumericValue(unitOfMeasure);
                    bool isyesnoval = bR.UnitOfMeasureRequireYesNoValue(unitOfMeasure);

                    //manager score validation
                    if (!mngnotnull)
                    {
                        if (isnumericval)
                        {
                            bool valNumeric = Util.IsNumeric(mngval);// vb.Information.IsNumeric(mngval);
                            if (!valNumeric)
                            {
                                valid = false;
                                lblError.Text = Messages.NumericValueRequired;
                            }
                        }
                        else if (isyesnoval)
                        {
                            bool yesNoValid = bR.UnitOfMeasureRequireYesNoValueValid(mngval);
                            if (!yesNoValid)
                            {
                                valid = false;
                                lblError.Text = Messages.YesNoValueRequired;
                            }
                        }
                    }
                    else if (!saveForLater)
                    {
                        valid = false;
                        lblError.Text = "*";
                    }

                    //agreed value validation
                    if (!agreednotnull)
                    {
                        if (isnumericval)
                        {
                            bool valNumeric = Util.IsNumeric(agreedval); // vb.Information.IsNumeric(agreedval);
                            if (!valNumeric)
                            {
                                valid = false;
                                lblError2.Text = Messages.NumericValueRequired;
                            }
                        }
                        else if (isyesnoval)
                        {
                            bool yesNoValid = bR.UnitOfMeasureRequireYesNoValueValid(agreedval);
                            if (!yesNoValid)
                            {
                                valid = false;
                                lblError2.Text = Messages.YesNoValueRequired;
                            }
                        }
                    }
                    else if (!saveForLater)
                    {
                        valid = false;
                        lblError2.Text = "*";
                    }
                }
                else
                {
                    Repeater rptMeasureData = item.FindControl("rptMeasureData") as Repeater;
                    foreach (RepeaterItem subitem in rptMeasureData.Items)
                    {
                        var mngval = (subitem.FindControl("txtManagerScore") as TextBox).Text;
                        var unitOfMeasure = int.Parse((subitem.FindControl("hdnUnitMeasure") as HiddenField).Value);
                        var agreedval = (subitem.FindControl("txtAgreedScore") as TextBox).Text;
                        var lblError2 = subitem.FindControl("lblError2") as Literal;
                        var lblError = subitem.FindControl("lblError") as Literal;

                        bool mngnotnull = string.IsNullOrEmpty(mngval);
                        bool agreednotnull = string.IsNullOrEmpty(agreedval);
                        bool isnumericval = bR.UnitOfMeasureRequireNumericValue(unitOfMeasure);
                        bool isyesnoval = bR.UnitOfMeasureRequireYesNoValue(unitOfMeasure);

                        lblError.Text = "";
                        lblError2.Text = "";

                        //manager score validation
                        if (!mngnotnull)
                        {
                            if (isnumericval)
                            {
                                bool valNumeric = Util.IsNumeric(mngval); // vb.Information.IsNumeric(mngval);
                                if (!valNumeric)
                                {
                                    valid = false;
                                    lblError.Text = Messages.NumericValueRequired;
                                }
                            }
                            else if (isyesnoval)
                            {
                                bool yesNoValid = bR.UnitOfMeasureRequireYesNoValueValid(mngval);
                                if (!yesNoValid)
                                {
                                    valid = false;
                                    lblError.Text = Messages.YesNoValueRequired;
                                }
                            }
                        }
                        else if (!saveForLater)
                        {
                            valid = false;
                            lblError.Text = "*";
                        }

                        //agreed value validation
                        if (!agreednotnull)
                        {
                            if (isnumericval)
                            {
                                bool valNumeric = Util.IsNumeric(agreedval);// vb.Information.IsNumeric(agreedval);
                                if (!valNumeric)
                                {
                                    valid = false;
                                    lblError2.Text = Messages.NumericValueRequired;
                                }
                            }
                            else if (isyesnoval)
                            {
                                bool yesNoValid = bR.UnitOfMeasureRequireYesNoValueValid(agreedval);
                                if (!yesNoValid)
                                {
                                    valid = false;
                                    lblError2.Text = Messages.YesNoValueRequired;
                                }
                            }
                        }
                        else if (!saveForLater)
                        {
                            valid = false;
                            lblError2.Text = "*";
                        }
                    }
                }
            }
            return valid;
        }

        bool UpdateWorkFlowItems()
        {
            try
            {
                int lastItemIdx = lvGoals.Items.Count - 1;
                foreach (ListViewItem item in lvGoals.Items)
                {
                    if (item.DataItemIndex == lastItemIdx) return true;
                    if (item.ItemType == ListViewItemType.DataItem)
                    {
                        var lblID = item.FindControl("lblID") as Label;
                        var lblWeight = item.FindControl("lblWeight") as Label;
                        var hdnHasSubGoals = item.FindControl("hdnHasSubGoals") as HiddenField;
                        var lblRatingValue = item.FindControl("lblRatingValue") as Label;

                        bool hasSubGoals = bool.Parse(hdnHasSubGoals.Value);
                        int parentId = int.Parse(lblID.Text);

                        if (!hasSubGoals)
                        {
                            var lblTarget = item.FindControl("lblTarget") as Label;
                            var lblTotal = item.FindControl("lblTotal") as Label;


                            var hdnOrgScoreValue = item.FindControl("hdnOrgScoreValue") as HiddenField;
                            var hdnUnitMeasure = item.FindControl("hdnUnitMeasureId") as HiddenField;

                            var txtManagerScore = item.FindControl("txtManagerScore") as TextBox;
                            var txtMngComment = item.FindControl("txtMngComment") as TextBox;
                            var txtAgreedScore = item.FindControl("txtAgreedScore") as TextBox;

                            string mngscore = txtManagerScore.Text;
                            string agreedScore = txtAgreedScore.Text;

                            var mngcomment = txtMngComment.Text;
                            var unitMeasure = int.Parse(hdnUnitMeasure.Value);
                            var weight = decimal.Parse(lblWeight.Text);
                            var item_id = int.Parse(lblID.Text);

                            decimal answer = 0;
                            decimal ratingVal = decimal.Parse(lblRatingValue.Text);

                            bool unitmeasureisnumeric = bR.UnitOfMeasureRequireNumericValue(unitMeasure);
                            if (unitmeasureisnumeric)
                            {
                                var target = decimal.Parse(lblTarget.Text);
                                if (string.IsNullOrEmpty(agreedScore)) answer = 0;
                                else
                                {
                                    var score = decimal.Parse(agreedScore);
                                    answer = bR.FinalScore(unitMeasure, weight, score, target);
                                }
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(mngscore))
                                {
                                    answer = 0;
                                }
                                else
                                {
                                    if (mngscore.Trim().ToLower() != "yes") answer = 0;
                                    else answer = weight;
                                }
                            }

                            decimal myscore = decimal.Parse(answer.ToString());
                            decimal finalAgreedScore = bR.GetDefinedWeightedScore(myscore, ratingVal);
                            scores _scoreDef = new scores
                            {
                                ID = item_id,
                                MANAGER_SCORE = mngscore,
                                AGREED_SCORE = agreedScore,
                                FINAL_SCORE_AGREED = finalAgreedScore,
                                FINAL_SCORE = (double)answer,
                                MANAGER_COMMENT = mngcomment,
                            };

                            scoreManager.updateWorkflowItem(_scoreDef);
                            _scoreDef = null;
                        }
                        else
                        {//dealing with items that have subgoals
                            Repeater rptMeasureData = item.FindControl("rptMeasureData") as Repeater;
                            foreach (RepeaterItem subitem in rptMeasureData.Items)
                            {
                                var mngcomment = (subitem.FindControl("txtMngComment") as TextBox).Text;
                                var mngscoreVal = (subitem.FindControl("txtManagerScore") as TextBox).Text;
                                var unitMeasure = int.Parse((subitem.FindControl("hdnUnitMeasure") as HiddenField).Value);
                                var weight = decimal.Parse(lblWeight.Text);//take weight from parent goal
                                var item_id = int.Parse((subitem.FindControl("hdnSubId") as HiddenField).Value);
                                var targetValue = (subitem.FindControl("hdnTarget") as HiddenField).Value;
                                var agreedScoreVal = (subitem.FindControl("txtAgreedScore") as TextBox).Text;
                                var lblError2 = subitem.FindControl("lblError2") as Literal;

                                decimal answer = 0;
                                decimal ratingVal = decimal.Parse(lblRatingValue.Text);

                                bool unitmeasureisnumeric = bR.UnitOfMeasureRequireNumericValue(unitMeasure);
                                if (unitmeasureisnumeric)
                                {
                                    var target = decimal.Parse(targetValue);
                                    if (string.IsNullOrEmpty(agreedScoreVal)) answer = 0;
                                    else
                                    {
                                        var score = decimal.Parse(agreedScoreVal);
                                        answer = bR.FinalScore(unitMeasure, weight, score, target);
                                    }
                                }
                                else
                                {
                                    if (string.IsNullOrEmpty(agreedScoreVal))//mngscoreVal
                                    {
                                        answer = 0;
                                    }
                                    else
                                    {
                                        if (agreedScoreVal.Trim().ToLower() != "yes") answer = 0;
                                        else answer = weight;
                                    }
                                }

                                decimal myscore = decimal.Parse(answer.ToString());
                                decimal finalAgreedScore = bR.GetDefinedWeightedScore(myscore, ratingVal);
                                scores _scoreDef = new scores
                                {
                                    ID = item_id,
                                    MANAGER_SCORE = mngscoreVal,
                                    AGREED_SCORE = agreedScoreVal,
                                    FINAL_SCORE_AGREED = finalAgreedScore,
                                    FINAL_SCORE = (double)answer,
                                    MANAGER_COMMENT = mngcomment,
                                };

                                scoreManager.updateWorkflowItem(_scoreDef);
                                _scoreDef = null;
                            }

                            if (parentId > 0)
                                scoreManager.updateParentScore((int)parentId);
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
            return false;
        }

        void changeStatus()
        {
            try
            {

                int submissionId = int.Parse(lblID.Text);
                int statusId = int.Parse(ddlStatus.SelectedValue);
                bool saveForLater = Util.isManagerSavingForlater(statusId);
                if (statusId != Util.getTypeDefinitionsWorkFlowNewRequestId())
                {
                    bool valid = informationValid(saveForLater);
                    if (!valid)
                    {
                        lblPopupMsg.Text = "Please correct fields marked with * before approving by capturing the correct information.";
                        mdlShowScoreDetails.Show();
                        return;
                    }

                    string comment = txtComment.Text;
                    var result = scoreManager.updateWorkflowStatus(new submissionWorkflow
                    {
                        STATUS = statusId,
                        ID = submissionId,
                        OVERALL_COMMENT = comment
                    });
                    if (result > 0)
                    {
                        bool saved = UpdateWorkFlowItems();
                        if (saved)
                        {
                            loadAllWorkflows();
                            int areaId = int.Parse(hdnAreaId.Value);
                            int valId = int.Parse(hdnValueId.Value);
                            int userId = 0;
                            if (areaId == Util.getTypeDefinitionsOrgUnitsTypeID())
                                userId = new structurecontroller(new structureImpl()).getOrgunitDetails(new Orgunit { ID = valId }).OWNER_ID;
                            else if (areaId == Util.getTypeDefinitionsProjectID())
                                userId = (int)new projectmanagement(new projectImpl()).get(valId).RESPONSIBLE_PERSON_ID;
                            else if (areaId == Util.getTypeDefinitionsRolesTypeID())
                                userId = valId;

                            IEmployee obj = new EmployeeBL();
                            string statusDesc = ddlStatus.SelectedItem.Text;
                            var user = obj.GetById(userId);
                            lblResult.Text = (statusId == Util.getTypeDefinitionsWorkFlowApprovalId()) ?
                                "The scores have been approved successfully." : Messages.GetSaveMessage();
                            sendMail(user, statusDesc);
                        }
                        else//reverse updated status if the selected update was approved
                        {
                            int newrequestId = Util.getTypeDefinitionsWorkFlowNewRequestId();
                            if (statusId == Util.getTypeDefinitionsWorkFlowApprovalId())
                                scoreManager.updateWorkflowStatus(new submissionWorkflow
                                    {
                                        STATUS = newrequestId,
                                        ID = submissionId,
                                        OVERALL_COMMENT = comment
                                    });
                            pnlNoData.Visible = true;
                            lblMsg.Text = Messages.GetSaveFailedMessage();
                            mdlShowScoreDetails.Show();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void sendMail(Employee user, string statusDesc)
        {
            try
            {
                string result = string.Empty;
                int emailId = Util.getWorkflowApprovalEmailId();

                MailAddressCollection addy = new MailAddressCollection();
                addy.Add(new MailAddress(user.EmailAddress, user.FullName));

                object[] subjectData = { statusDesc.ToLower() };
                object[] mydata = { statusDesc };

                IMailRelay objm = new MailRelayBL();
                MailRelay obj = new MailRelay
                {
                    MailId = emailId,
                    Recipient = addy,
                    Data = mydata,
                    SubjectData = subjectData
                };
                objm.SendMail(obj, out result);
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
            }
        }

        void UpdateWorkflowItem(ListViewCommandEventArgs e)
        {
            try
            {
                var lblID = e.Item.FindControl("lblID") as Label;
                var lblWeight = e.Item.FindControl("lblWeight") as Label;
                var lblTarget = e.Item.FindControl("lblTarget") as Label;
                var lblTotal = e.Item.FindControl("lblTotal") as Label;
                var lblFinalAgreedTotal = e.Item.FindControl("lblFinalAgreedTotal") as Label;
                var lblRatingValue = e.Item.FindControl("lblRatingValue") as Label;
                var lblError = e.Item.FindControl("lblError") as Literal;
                var lblError2 = e.Item.FindControl("lblError2") as Literal;

                var hdnOrgScoreValue = e.Item.FindControl("hdnOrgScoreValue") as HiddenField;
                var hdnParentId = e.Item.FindControl("hdnParentId") as HiddenField;
                var hdnUnitMeasure = e.Item.FindControl("hdnUnitMeasureId") as HiddenField;

                var txtManagerScore = e.Item.FindControl("txtManagerScore") as TextBox;
                var txtMngComment = e.Item.FindControl("txtMngComment") as TextBox;
                var txtAgreedScore = e.Item.FindControl("txtAgreedScore") as TextBox;

                lblError.Text = string.Empty;
                lblError2.Text = string.Empty;

                int? parentId = null;
                if (e.Item.ItemType == ListViewItemType.DataItem)
                {
                    decimal answer = 0;

                    var mngcomment = txtMngComment.Text;
                    var unitMeasure = int.Parse(hdnUnitMeasure.Value);
                    var weight = decimal.Parse(lblWeight.Text);
                    var item_id = int.Parse(lblID.Text);
                    var orgScore = hdnOrgScoreValue.Value;
                    var ratingValue = decimal.Parse(lblRatingValue.Text);

                    string mngscore = txtManagerScore.Text;
                    string agreedScore = txtAgreedScore.Text;

                    #region New Validation Code
                    bool valid = true;

                    bool mngnotnull = string.IsNullOrEmpty(mngscore);
                    bool agreednotnull = string.IsNullOrEmpty(agreedScore);
                    bool isnumericval = bR.UnitOfMeasureRequireNumericValue(unitMeasure);
                    bool isyesnoval = bR.UnitOfMeasureRequireYesNoValue(unitMeasure);

                    if (!mngnotnull)
                    {
                        if (isnumericval)
                        {
                            bool valNumeric = Util.IsNumeric(mngscore);// vb.Information.IsNumeric(mngscore);
                            if (!valNumeric)
                            {
                                valid = false;
                                lblError.Text = Messages.NumericValueRequired;
                            }
                        }
                        else if (isyesnoval)
                        {
                            bool yesNoValid = bR.UnitOfMeasureRequireYesNoValueValid(mngscore);
                            if (!yesNoValid)
                            {
                                valid = false;
                                lblError.Text = Messages.YesNoValueRequired;
                            }
                        }
                    }
                    else
                    {
                        valid = false;
                        lblError.Text = "*";
                    }

                    //agreed value validation
                    if (!agreednotnull)
                    {
                        if (isnumericval)
                        {
                            bool valNumeric = Util.IsNumeric(agreedScore);// vb.Information.IsNumeric(agreedScore);
                            if (!valNumeric)
                            {
                                valid = false;
                                lblError2.Text = Messages.NumericValueRequired;
                            }
                        }
                        else if (isyesnoval)
                        {
                            bool yesNoValid = bR.UnitOfMeasureRequireYesNoValueValid(agreedScore);
                            if (!yesNoValid)
                            {
                                valid = false;
                                lblError2.Text = Messages.YesNoValueRequired;
                            }
                        }
                    }
                    else
                    {
                        valid = false;
                        lblError2.Text = "*";
                    }

                    if (!valid)
                    {
                        if (lblError2.Text == "*" || lblError.Text == "*")
                            lblLstView.Text = "Please enter values in the field marked with a * before pressing the save icon.";
                        mdlShowScoreDetails.Show();
                        return;
                    }

                    #endregion

                    if (isnumericval)//unit of measure
                    {
                        bool valNumeric = Util.IsNumeric(agreedScore); //vb.Information.IsNumeric(agreedScore);
                        if (valNumeric)
                        {
                            var target = decimal.Parse(lblTarget.Text);
                            var score = decimal.Parse(agreedScore);
                            answer = bR.FinalScore(unitMeasure, weight, score, target);
                        }
                        else
                        {
                            lblError2.Text = Messages.NumericValueRequired;
                            valid = false;
                        }
                    }
                    else if (isyesnoval) //unit of measure
                    {
                        bool yesNoValid = bR.UnitOfMeasureRequireYesNoValueValid(agreedScore);
                        if (yesNoValid)
                        {
                            if (agreedScore.Trim().ToLower() != "yes") answer = 0;
                            else answer = weight;
                        }
                        else
                        {
                            valid = false;
                            lblError2.Text = Messages.YesNoValueRequired;
                        }
                    }

                    if (valid)
                    {
                        decimal myscore = decimal.Parse(answer.ToString());
                        decimal finalAgreedScore = bR.GetDefinedWeightedScore(myscore, ratingValue);
                        scores _scoreDef = new scores
                        {
                            ID = item_id,
                            MANAGER_SCORE = mngscore,
                            AGREED_SCORE = agreedScore,
                            FINAL_SCORE_AGREED = Math.Round(finalAgreedScore, 2),
                            FINAL_SCORE = Math.Round((double)answer, 2),
                            MANAGER_COMMENT = mngcomment
                        };

                        int result = scoreManager.updateWorkflowItem(_scoreDef);
                        _scoreDef = null;

                        //update parent record in case of hierarchical scoring
                        if (!string.IsNullOrEmpty(hdnParentId.Value))
                            parentId = int.Parse(hdnParentId.Value);
                        if (result > 0 && parentId > 0 && parentId != null)
                            scoreManager.updateParentScore((int)parentId);

                        if (result > 0)//reload the goals. it is much simple and easier
                        {
                            int valueId = int.Parse(hdnValueId.Value);
                            int areaId = int.Parse(hdnAreaId.Value);
                            int submitId = int.Parse(hdnSubmissionId.Value);

                            loadGoals(valueId, areaId, submitId);
                            mdlShowScoreDetails.Show();
                            lblLstView.Text = Messages.GetSaveMessage();
                        }
                    }
                    else
                    {
                        if (lblError2.Text == "*" || lblError.Text == "*")
                            lblLstView.Text = "Please enter values in the field marked with a * before pressing the save icon";
                        mdlShowScoreDetails.Show();
                    }
                }
            }
            catch (Exception ex)
            {
                lblLstView.Text = Messages.GetErrorMessage();
                Util.LogErrors(ex);
                mdlShowScoreDetails.Show();
            }
        }

        void UpdateWorkflowItem(RepeaterCommandEventArgs e)
        {
            try
            {
                int parentId = 0;
                decimal weight = 0;
                var hdnParentId = e.Item.FindControl("hdnParentId") as HiddenField;
                if (string.IsNullOrEmpty(hdnParentId.Value))
                {
                    ShowMessage(lblLstView, Messages.GetSaveFailedMessage());
                    return;
                }
                else
                {
                    parentId = int.Parse(hdnParentId.Value);
                    weight = GetSubMeasureWeight(parentId);
                    if (weight <= 0)
                    {
                        ShowMessage(lblLstView, Messages.GetSaveFailedMessage());
                        return;
                    }
                }

                var hdnSubId = e.Item.FindControl("hdnSubId") as HiddenField;
                var hdnTarget = e.Item.FindControl("hdnTarget") as HiddenField;
                var hdnUnitMeasure = e.Item.FindControl("hdnUnitMeasure") as HiddenField;

                var lblRatingValue = e.Item.FindControl("lblRatingValue") as Label;
                var lblError = e.Item.FindControl("lblError") as Literal;
                var lblError2 = e.Item.FindControl("lblError2") as Literal;

                var txtManagerScore = e.Item.FindControl("txtManagerScore") as TextBox;
                var txtMngComment = e.Item.FindControl("txtMngComment") as TextBox;
                var txtAgreedScore = e.Item.FindControl("txtAgreedScore") as TextBox;

                lblError.Text = string.Empty;
                lblError2.Text = string.Empty;

                if (e.Item.ItemType == ListItemType.Item ||
                    e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    decimal answer = 0;

                    var mngcomment = txtMngComment.Text;
                    var unitMeasure = int.Parse(hdnUnitMeasure.Value);

                    var item_id = int.Parse(hdnSubId.Value);
                    var ratingValue = decimal.Parse(lblRatingValue.Text);

                    string mngscore = txtManagerScore.Text;
                    string agreedScore = txtAgreedScore.Text;

                    bool isyesnoval = bR.UnitOfMeasureRequireYesNoValue(unitMeasure);
                    bool unitmeasureisnumeric = bR.UnitOfMeasureRequireNumericValue(unitMeasure);

                    #region New Validation Code
                    bool valid = true;

                    bool mngnotnull = string.IsNullOrEmpty(mngscore);
                    bool agreednotnull = string.IsNullOrEmpty(agreedScore);

                    if (!mngnotnull)
                    {
                        if (unitmeasureisnumeric)
                        {
                            bool valNumeric = Util.IsNumeric(mngscore); // vb.Information.IsNumeric(mngscore);
                            if (!valNumeric)
                            {
                                valid = false;
                                lblError.Text = Messages.NumericValueRequired;
                            }
                        }
                        else if (isyesnoval)
                        {
                            bool yesNoValid = bR.UnitOfMeasureRequireYesNoValueValid(mngscore);
                            if (!yesNoValid)
                            {
                                valid = false;
                                lblError.Text = Messages.YesNoValueRequired;
                            }
                        }
                    }
                    else
                    {
                        valid = false;
                        lblError.Text = "*";
                    }

                    //agreed value validation
                    if (!agreednotnull)
                    {
                        if (unitmeasureisnumeric)
                        {
                            bool valNumeric = Util.IsNumeric(agreedScore); // vb.Information.IsNumeric(agreedScore);
                            if (!valNumeric)
                            {
                                valid = false;
                                lblError2.Text = Messages.NumericValueRequired;
                            }
                        }
                        else if (isyesnoval)
                        {
                            bool yesNoValid = bR.UnitOfMeasureRequireYesNoValueValid(agreedScore);
                            if (!yesNoValid)
                            {
                                valid = false;
                                lblError2.Text = Messages.YesNoValueRequired;
                            }
                        }
                    }
                    else
                    {
                        valid = false;
                        lblError2.Text = "*";
                    }

                    if (!valid)
                    {
                        if (lblError2.Text == "*" || lblError.Text == "*")
                            lblLstView.Text = "Please enter values in the field marked with a * before pressing the save icon";
                        mdlShowScoreDetails.Show();
                        return;
                    }

                    #endregion

                    if (unitmeasureisnumeric)
                    {
                        bool valNumeric = Util.IsNumeric(agreedScore);// vb.Information.IsNumeric(agreedScore);
                        if (valNumeric)
                        {
                            var target = decimal.Parse(hdnTarget.Value);
                            var score = decimal.Parse(agreedScore);
                            answer = bR.FinalScore(unitMeasure, weight, score, target);
                        }
                        else
                        {
                            valid = false;
                            lblError2.Text = Messages.NumericValueRequired;
                        }
                    }
                    else if (isyesnoval)
                    {
                        bool yesNoValid = bR.UnitOfMeasureRequireYesNoValueValid(agreedScore);
                        if (yesNoValid)
                        {
                            if (agreedScore.Trim().ToLower() != "yes") answer = 0;
                            else answer = weight;
                        }
                        else
                        {
                            valid = false;
                            lblError2.Text = Messages.YesNoValueRequired;
                        }
                    }

                    if (valid)
                    {
                        decimal myscore = decimal.Parse(answer.ToString());
                        decimal finalAgreedScore = bR.GetDefinedWeightedScore(myscore, ratingValue);
                        scores _scoreDef = new scores
                        {
                            ID = item_id,
                            MANAGER_SCORE = mngscore,
                            AGREED_SCORE = agreedScore,
                            FINAL_SCORE_AGREED = Math.Round(finalAgreedScore, 2),
                            FINAL_SCORE = Math.Round((double)answer, 2),
                            MANAGER_COMMENT = mngcomment
                        };

                        int result = scoreManager.updateWorkflowItem(_scoreDef);
                        _scoreDef = null;

                        //update parent record in case of hierarchical scoring                   
                        if (result > 0 && parentId > 0)
                            scoreManager.updateParentScore(parentId);

                        if (result > 0)//reload the goals. it is much simple and easier
                        {
                            int valueId = int.Parse(hdnValueId.Value);
                            int areaId = int.Parse(hdnAreaId.Value);
                            int submissionId = int.Parse(hdnSubmissionId.Value);

                            loadGoals(valueId, areaId, submissionId);
                            mdlShowScoreDetails.Show();
                            ShowMessage(lblLstView, Messages.GetSaveMessage());
                        }
                    }
                    else
                    {
                        if (lblError2.Text == "*" || lblError.Text == "*")
                            lblLstView.Text = "Please enter values in the field marked with a * before pressing the save icon";
                        mdlShowScoreDetails.Show();
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                lblLstView.Text = Messages.GetErrorMessage();
                Util.LogErrors(ex);
                mdlShowScoreDetails.Show();
            }
        }

        void UpdateOveralComment()
        {
            try
            {
                int submissionId = int.Parse(hdnSubmissionId.Value);
                string comment = txtComment.Text;

                var result = scoreManager.updateWorkflowComment(new submissionWorkflow
                {
                    ID = submissionId,
                    OVERALL_COMMENT = comment
                });

                lblPopupMsg.Text = (result > 0) ? Messages.GetSaveMessage() : Messages.GetSaveFailedMessage();
                //txtComment.Text = comment;
                if (result > 0)
                {
                    foreach (ListViewDataItem item in lvSubmittedWorkflows.Items)
                    {
                        Label lblID = item.FindControl("lblID") as Label;
                        HiddenField hdnOverallComment = item.FindControl("hdnOverallComment") as HiddenField;
                        if (lblID != null)
                            if (lblID.Text.Equals(submissionId.ToString()))
                            {
                                hdnOverallComment.Value = comment;
                                break;
                            }
                    }
                }
            }
            catch (Exception ex)
            {
                lblPopupMsg.Text = Messages.GetErrorMessage();
                Util.LogErrors(ex);
            }
            finally
            {
                mdlShowScoreDetails.Show();
            }
        }

        void ShowMessage(Literal lbl, string msg)
        {
            lbl.Text = msg;
        }

        #endregion

        #region Button Event Handling Methods

        protected void btnSaveAll_Click(object sender, EventArgs e)
        {
            bool saved = UpdateWorkFlowItems();
            ShowMessage(lblLstView, saved ? Messages.GetSaveMessage() : Messages.GetSaveFailedMessage());
            mdlShowScoreDetails.Show();
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            changeStatus();
        }

        protected void btnUpdateComment_Click(object sender, EventArgs e)
        {
            UpdateOveralComment();
        }

        protected void lnkCancel_Click(object sender, EventArgs e)
        {
            pnlNoData.Visible = false;
            mdlShowScoreDetails.Hide();
        }

        protected void lnkDone_Click(object sender, EventArgs e)
        {
            Response.Redirect("index.aspx");
        }

        #endregion

        #region Repeater Event Handling Methods

        protected void rptMeasureData_OnItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.ToLower().Equals("update".ToLower()))
                    UpdateWorkflowItem(e);
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                ShowMessage(lblLstView, Messages.GetErrorMessage());
            }
        }

        #endregion
    }
}