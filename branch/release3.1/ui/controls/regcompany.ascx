﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="regcompany.ascx.cs"
    Inherits="scorecard.ui.controls.regcompany" %>
<div class="form">
    <asp:UpdatePanel ID="UpdatePanelOrg" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <h1>
                Company Information</h1>
            <p>
                <label for="sf">
                    Registration No :
                </label>
                <span class="field_desc">
                    <input class="mf" name="mf0" type="text" runat="server" id="txtRegistrationNo" /></span>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtRegistrationNo"
                    Display="None" ErrorMessage="Registration Number"></asp:RequiredFieldValidator>
            </p>
            <p>
                <label for="sf">
                    Company Name:
                </label>
                <span class="field_desc">
                    <input class="mf" name="mf0" type="text" runat="server" id="txtCompanyName" /></span>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtCompanyName"
                    Display="None" ErrorMessage="Company Name "></asp:RequiredFieldValidator>
            </p>
            <p>
                <label for="sf">
                    Vat No:
                </label>
                <span class="field_desc">
                    <input class="mf" name="mf0" type="text" runat="server" id="txtVatNo" /></span>
            </p>
            <p>
                <label for="sf">
                    Industry :
                </label>
                <asp:DropDownList name="dropdown" class="dropdown" runat="server" ID="ddlIndustry"
                    DataValueField="TYPE_ID" DataTextField="NAME" />
            </p>
            <p>
                <div class="floatLeft">
                    <label for="lf">
                        Address :
                    </label>
                </div>
                <div class="floatLeft">
                    &nbsp;&nbsp;&nbsp;<textarea rows="5" cols="35" runat="server" id="txtPostalAddress"
                        class="mf" name="mf0" style="font-size: medium; width: 300px; height: 80px; border-color: #555555"></textarea>
                </div>
                <div class="clr">
                </div>
            </p>
            <p>
                <label for="lf">
                    Contact Telephone :
                </label>
                <input class="mf" name="lf0" type="text" runat="server" id="txtContactTelephone" /></p>
            <p>
                <label for="sf">
                    Country :
                </label>
                <asp:DropDownList name="dropdown" class="dropdown" runat="server" ID="ddlCountry"
                    DataValueField="TYPE_ID" DataTextField="NAME" />
            </p>
            <p class="errorMsg">
                <label for="lf">
                    &nbsp;
                </label>
                <asp:Literal ID="lblError" runat="server"></asp:Literal>
            </p>
            <p>
                <label>
                    &nbsp;</label>
                <asp:Button ID="btnComplete" runat="server" class="button" Text="Update" OnClick="btnComplete_Click" />
                <asp:Button ID="btnCancelComplete" runat="server" class="button" Text="Cancel" CausesValidation="False"
                    PostBackUrl="~/ui/index.aspx" />
            </p>
            <p>
                &nbsp;<asp:ValidationSummary ID="ValidationSummary1" runat="server" HeaderText="Please provide the following fields:"
                    ShowMessageBox="True" ShowSummary="False" />
            </p>
            <asp:UpdateProgress ID="UpdateProgressOrg" runat="server" AssociatedUpdatePanelID="UpdatePanelOrg">
                <ProgressTemplate>
                    <div>
                        <asp:Image ID="imgOrg" runat="server" ImageUrl="~/ui/images/icons/activity.gif" AlternateText="Please wait..">
                        </asp:Image></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
