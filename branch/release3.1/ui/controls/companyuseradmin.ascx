﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="companyuseradmin.ascx.cs"
    Inherits="scorecard.ui.controls.companyuseradmin" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<h1>
    &nbsp; User Administration</h1>
<asp:UpdatePanel ID="UpdatePanelOrgUsers" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:ListView ID="lstVUsers" runat="server" OnItemEditing="lstVUsers_ItemEditing">
            <LayoutTemplate>
                <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                    <thead>
                        <tr>
                            <td>
                                <input type="checkbox" class="checkall" />
                            </td>
                            <td>
                                Name
                            </td>
                            <td>
                                Email Address
                            </td>
                            <td>
                                Username
                            </td>
                            <td>
                                Job Title
                            </td>
                            <td>
                                Org Unit
                            </td>
                            <td>
                                Edit
                            </td>
                            <td>
                                Delete
                            </td>
                        </tr>
                        <tr id="ItemPlaceHolder" runat="server">
                        </tr>
                    </thead>
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tbody>
                    <tr class="odd">
                        <td>
                            <input type="checkbox" />
                            <asp:Label ID="lblID" runat="server" Visible="false" Text='<%# Eval("ID") %>' />
                        </td>
                        <td>
                            <%# Eval("NAME") %>
                        </td>
                        <td>
                            <%# Eval("EMAIL_ADDRESS")%>
                        </td>
                        <td>
                            <%# Eval("LOGIN_ID")%>
                        </td>
                        <td>
                            <%# getRoleName(int.Parse(Eval("JOB_TITLE_ID").ToString()))%>
                        </td>
                        <td>
                            <%# getAreaName(int.Parse(Eval("ORG_ID").ToString()))%>
                        </td>
                        <td>
                            <asp:LinkButton runat="server" ID="lnkEditing" CommandName="Edit" class="ui-state-default ui-corner-all"
                                title="Edit Record" CausesValidation="false"><span class="ui-icon ui-icon-pencil">
                        </span></asp:LinkButton>
                        </td>
                        <td>
                            <asp:LinkButton runat="server" ID="lnkDeleting" CommandName="deleteItem" class="ui-state-default ui-corner-all"
                                title="Delete Record" OnClientClick="return confirmDelete();" CausesValidation="false"><span class="ui-icon ui-icon-trash">
                        </span></asp:LinkButton>
                        </td>
                    </tr>
                </tbody>
            </ItemTemplate>
        </asp:ListView>
        <br />
        <div id="pager">
            <asp:DataPager ID="dtPagerUsers" runat="server" PagedControlID="lstVUsers" PageSize="20"
                OnPreRender="DataPagerUser_PreRender">
                <Fields>
                    <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True" ShowNextPageButton="False"
                        ShowPreviousPageButton="False" />
                    <asp:NumericPagerField />
                    <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True" ShowNextPageButton="False"
                        ShowPreviousPageButton="False" />
                </Fields>
            </asp:DataPager>
        </div>
        <asp:UpdateProgress ID="UpdateProgressUsers" runat="server" AssociatedUpdatePanelID="UpdatePanelOrgUsers">
            <ProgressTemplate>
                <div>
                    <asp:Image ID="imgOrg" runat="server" ImageUrl="~/ui/images/icons/activity.gif" AlternateText="Please wait..">
                    </asp:Image>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <br />
        <div id="tabs-3">
            <p>
                <asp:Button ID="btnNew" runat="server" CssClass="button" Text="Add New User" OnClick="btnNew_Click"
                    CausesValidation="false" />
                &nbsp;<asp:Button ID="lnkDone" runat="server" CssClass="button tooltip" Text="Done"
                    CausesValidation="false" PostBackUrl="~/ui/index.aspx" />
            </p>
        </div>
        <asp:Panel ID="mdlPnlAddNewUser" runat="server" CssClass="modalPopup">
            <p>
                <br />
            </p>
            <h1>
                User Account Information
            </h1>
            <%-- <fieldset>
                <legend>Create User Account</legend>--%>
            <p>
                <label for="sf">
                    Username :
                </label>
                &nbsp;<span class="field_desc">
                    <input class="mf" name="mf0" type="text" runat="server" id="txtLoginName" /></span>
                <asp:Label ID="lblUserID" runat="server" Visible="False"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtLoginName"
                    ValidationGroup="AddUser" Display="None" ErrorMessage="User Login Name"></asp:RequiredFieldValidator>
            </p>
            <p>
                &nbsp;<div runat="server" id="pnlPassword">
                    <p>
                        <label for="sf">
                            Password :
                        </label>
                        &nbsp;<span class="field_desc">&nbsp;<input class="mf" name="mf0" type="password"
                            runat="server" id="txtPassword" /></span>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtPassword"
                            Display="None" ErrorMessage="Password" ValidationGroup="AddUser"></asp:RequiredFieldValidator>
                    </p>
                    <p>
                        <label for="sf">
                            Confirm Password :
                        </label>
                        &nbsp;<span class="field_desc">
                            <input class="mf" name="mf0" type="password" runat="server" id="txtConfirmPassword" /></span>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtConfirmPassword"
                            Display="None" ErrorMessage="Confirm Password" ValidationGroup="AddUser"></asp:RequiredFieldValidator>
                    </p>
                </div>
                <p>
                </p>
                <p>
                    <label for="sf">
                        Name :
                    </label>
                    &nbsp;<span class="field_desc">
                        <input class="mf" name="mf0" type="text" runat="server" id="txtContactName" />
                    </span>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtLoginName"
                        Display="None" ErrorMessage="Contact Name " ValidationGroup="AddUser"></asp:RequiredFieldValidator>
                </p>
                <p>
                    <label for="sf">
                        Email Address :
                    </label>
                    &nbsp;<span class="field_desc">
                        <input class="mf" name="mf0" type="text" runat="server" id="txtEmailAddress" />
                    </span>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtEmailAddress"
                        Display="None" ErrorMessage="Email Address " ValidationGroup="AddUser"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmailAddress"
                        Display="None" ErrorMessage="Valid Email Addres" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                        ValidationGroup="AddUser"></asp:RegularExpressionValidator>
                </p>
                <p>
                    <label for="sf">
                        Organisational Unit:
                    </label>
                    &nbsp;
                    <asp:DropDownList ID="ddlOrgUnit" runat="server" AutoPostBack="false" OnSelectedIndexChanged="ddlOrgUnit_SelectedIndexChanged"
                        Width="140px">
                    </asp:DropDownList>
                </p>
                <p>
                    <label for="sf">
                        Job Title :
                    </label>
                    &nbsp;
                    <asp:DropDownList ID="ddlJobTitle" runat="server" OnSelectedIndexChanged="ddlJobTitle_SelectedIndexChanged"
                        Width="140px">
                    </asp:DropDownList>
                </p>
                <p>
                    <label for="sf">
                        User Type :
                    </label>
                    &nbsp;
                    <asp:DropDownList ID="ddlUserType" runat="server" Width="140px">
                    </asp:DropDownList>
                </p>
                <p>
                    <label for="sf">
                        Reports To:
                    </label>
                    &nbsp;
                    <asp:DropDownList ID="ddlUsersReportsTo" runat="server" OnSelectedIndexChanged="ddlUsersReportsTo_SelectedIndexChanged"
                        Width="140px">
                    </asp:DropDownList>
                </p>
            </p>
         <%--   </fieldset>
            <fieldset>--%>
                <p class="errorMsg">
                    <br />
                    <asp:Literal ID="lblMsg" runat="server"></asp:Literal>
                    <br />
                    <br />
                </p>
                <p>
                    <asp:Button ID="lnkSaveAddUser" runat="server" CssClass="button" Text="Save" OnClick="lnkSaveAddUser_Click"
                        ValidationGroup="AddUser" />
                    <asp:Button ID="lnkUpdateUser" runat="server" CssClass="button" OnClick="lnkUpdateUser_Click"
                        Text="Save" Visible="False" ValidationGroup="AddUser" />
                    <asp:Button ID="lnkCancelAddUser" runat="server" CssClass="button" Text="Cancel"
                        OnClick="lnkCancelAddUser_Click" CausesValidation="false" />
                    <asp:ValidationSummary ID="vdS" runat="server" HeaderText="Provide the following fields:"
                        ShowMessageBox="True" ShowSummary="False" ValidationGroup="AddUser" />
                </p>
          <%--  </fieldset>--%>
        </asp:Panel>
        <asp:Button ID="btnNewCompUser" runat="server" Style="display: none" />
        <ajaxToolkit:ModalPopupExtender ID="mdlUserForm" runat="server" TargetControlID="btnNewCompUser"
            PopupControlID="mdlPnlAddNewUser" BackgroundCssClass="modalBackground" />
        <asp:Panel ID="Panel1" runat="server">
            <p>
                Torrence Testing</p>
        </asp:Panel>
        <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnNewCompUser"
            PopupControlID="Panel1" BackgroundCssClass="modalBackground">
        </ajaxToolkit:ModalPopupExtender>
    </ContentTemplate>
</asp:UpdatePanel>
