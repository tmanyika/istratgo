﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="emailCommunication.ascx.cs"
    Inherits="scorecard.ui.controls.emailCommunication" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="cc1" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Import Namespace="scorecard.implementations" %>
<h1>
    Send Email Communication</h1>
<div>
    <asp:UpdatePanel ID="UpdatePanelData" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:MultiView ID="vMain" runat="server" ActiveViewIndex="0">
                <asp:View ID="vwData" runat="server">
                    <div style="text-align: right">
                        <asp:Button ID="lnkAddNew" runat="server" CssClass="button" Text="Send Email" CausesValidation="false"
                            OnClick="lnkAddNew_Click" />
                    </div>
                    <div>
                        <br />
                    </div>
                    <div id="divMailList">
                        <asp:ListView ID="lstData" runat="server" OnPagePropertiesChanging="lstData_PagePropertiesChanging">
                            <LayoutTemplate>
                                <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                                    <thead>
                                        <tr>
                                            <td>
                                                Batch Name
                                            </td>
                                            <td>
                                                Mail Subject
                                            </td>
                                            <td align="right">
                                                No. of Users Sent To
                                            </td>
                                            <td>
                                                Recipient Role(s)
                                            </td>
                                            <td>
                                                Date To Be Sent
                                            </td>
                                            <td>
                                                Sent
                                            </td>
                                            <td>
                                                Sent By
                                            </td>
                                            <td>
                                                Sender Account Name
                                            </td>
                                        </tr>
                                        <tr id="itemPlaceholder" runat="server">
                                        </tr>
                                    </thead>
                                </table>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tbody>
                                    <tr class="odd">
                                        <td>
                                            <%# Eval("BatchName")%>
                                        </td>
                                        <td>
                                            <%# Eval("MailSubject")%>
                                        </td>
                                        <td align="right">
                                            <%# Eval("NoOfUsers")%>
                                        </td>
                                        <td>
                                            <%# Eval("UserRoles")%>
                                        </td>
                                        <td>
                                            <%# Common.GetDescriptiveDate(Eval("DateToBeSent"),true) %>
                                        </td>
                                        <td>
                                            <%# GetBoolean(Eval("IsSent"))%>
                                        </td>
                                        <td>
                                            <%# Eval("CreatedBy")%>
                                        </td>
                                        <td>
                                            <%# Eval("Server.AccountName")%>
                                        </td>
                                    </tr>
                                </tbody>
                            </ItemTemplate>
                            <EmptyDataTemplate>
                                <p class="errorMsg">
                                    There are no records to display.
                                </p>
                            </EmptyDataTemplate>
                        </asp:ListView>
                        <br />
                        <asp:DataPager ID="DtPager" runat="server" PagedControlID="lstData" PageSize="15">
                            <Fields>
                                <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True" ShowNextPageButton="False"
                                    ShowPreviousPageButton="False" />
                                <asp:NumericPagerField />
                                <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True" ShowNextPageButton="False"
                                    ShowPreviousPageButton="False" />
                            </Fields>
                        </asp:DataPager>
                        <br />
                        <br />
                    </div>
                </asp:View>
                <asp:View ID="vForm" runat="server">
                    <div id="divForm">
                        <div>
                            <asp:Panel ID="pnlNewMail" runat="server" GroupingText="Send Mass Email">
                                <div class="emailIndent">
                                    <label class="lf">
                                        Select Recipient Role(s) :
                                    </label>
                                </div>
                                <div>
                                    <br />
                                </div>
                                <div class="emailIndent">
                                    <span class="field_desc">
                                        <asp:CheckBoxList ID="chkRoles" runat="server" RepeatColumns="3" RepeatLayout="Flow">
                                        </asp:CheckBoxList>
                                    </span>
                                </div>
                                <div>
                                    <br />
                                </div>
                                <div class="emailIndent">
                                    <label class="lf">
                                        Select Master Template :
                                    </label>
                                </div>
                                <div>
                                    <br />
                                </div>
                                <div class="emailIndent">
                                    <asp:DropDownList ID="ddMail" runat="server">
                                    </asp:DropDownList>
                                </div>
                                <div>
                                    <br />
                                </div>
                                <div class="emailIndent">
                                    <label class="lf">
                                        Mail Account To Send From :
                                    </label>
                                </div>
                                <div>
                                    <br />
                                </div>
                                <div class="emailIndent">
                                    <asp:DropDownList ID="ddAccount" runat="server">
                                    </asp:DropDownList>
                                </div>
                                <div>
                                    <br />
                                </div>
                                <div class="emailIndent">
                                    <label class="lf">
                                        Batch Name :
                                    </label>
                                </div>
                                <div>
                                    <br />
                                </div>
                                <div class="emailIndent">
                                    <span class="field_desc">
                                        <asp:TextBox ID="txtBatchName" runat="server" CssClass="mf" ValidationGroup="Add"
                                            Width="600px"></asp:TextBox></span> </span><asp:RequiredFieldValidator ID="RqdName"
                                                runat="server" ControlToValidate="txtBatchName" ValidationGroup="Add" Display="None"
                                                ErrorMessage="Batch Name" ForeColor="Red"></asp:RequiredFieldValidator>
                                </div>
                                <div>
                                    <br />
                                </div>
                                <div class="emailIndent">
                                    <label class="lf">
                                        Mail Subject :
                                    </label>
                                </div>
                                <div>
                                    <br />
                                </div>
                                <div class="emailIndent">
                                    <span class="field_desc">
                                        <asp:TextBox ID="txtSubject" runat="server" CssClass="mf" ValidationGroup="Add" Width="600px"></asp:TextBox></span>
                                    </span><asp:RequiredFieldValidator ID="rdqSubj" runat="server" ControlToValidate="txtSubject"
                                        ValidationGroup="Add" Display="None" ErrorMessage="Mail Name" ForeColor="Red"></asp:RequiredFieldValidator>
                                </div>
                                <div>
                                    <br />
                                </div>
                                <div class="emailIndent">
                                    <label class="lf">
                                        Mail Content (<span style="font-size: small; color: Red;"><i><b>Note</b>: do not specify
                                            font type</i></span>):
                                    </label>
                                </div>
                                <div>
                                    <br />
                                </div>
                                <div class="emailIndent">
                                    <span class="field_desc">
                                        <telerik:RadEditor ID="rdContent" runat="server">
                                            <CssFiles>
                                                <telerik:EditorCssFile Value="" />
                                            </CssFiles>
                                        </telerik:RadEditor>
                                    </span>
                                </div>
                                <div>
                                    <br />
                                </div>
                                <div>
                                    <p>
                                        <label class="lf">
                                            Date to Send The Email:</label>&nbsp; <span class="field_desc">
                                                <asp:TextBox ID="txtDate" runat="server" Width="120px"></asp:TextBox></span>
                                        <asp:CalendarExtender ID="txtDate_CalendarExtender" runat="server" Enabled="True"
                                            Format="d/MM/yyyy" TargetControlID="txtDate">
                                        </asp:CalendarExtender>
                                        <asp:RequiredFieldValidator ID="rqDate" runat="server" ErrorMessage="Date of Score"
                                            Display="None" ControlToValidate="txtDate" ForeColor="Red" ValidationGroup="Add"></asp:RequiredFieldValidator>
                                    </p>
                                </div>
                                <div id="DivUpload" runat="server" visible="false">
                                    <p>
                                        <label class="lf">
                                            Mail Attachments:</label>&nbsp;<span class="field_desc"><a href="#">Upload</a>
                                        </span>
                                    </p>
                                </div>
                            </asp:Panel>
                        </div>
                        <div>
                            <p class="errorMsg">
                                <br />
                                <asp:Literal ID="lblError" runat="server"></asp:Literal>
                                <br />
                                <br />
                            </p>
                            <p>
                                <asp:Button ID="lnkSaveMail" runat="server" CssClass="button" Text="Save" ValidationGroup="Add"
                                    OnClick="lnkSaveMail_Click" />
                                <asp:Button ID="lnkCancel" runat="server" CssClass="button" Text="Cancel" 
                                    CausesValidation="false" onclick="lnkCancel_Click" />
                                <asp:ValidationSummary ID="vdSummary" runat="server" HeaderText="Provide The Following Fields"
                                    ShowMessageBox="True" ShowSummary="False" ValidationGroup="Add" ForeColor="Red" />
                            </p>
                        </div>
                    </div>
                </asp:View>
            </asp:MultiView>
            <asp:UpdateProgress ID="UpdateProgressData" runat="server" AssociatedUpdatePanelID="UpdatePanelData">
                <ProgressTemplate>
                    <div>
                        <asp:Image ID="imgProj" runat="server" ImageUrl="~/ui/images/icons/activity.gif"
                            AlternateText="Please wait.."></asp:Image></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
