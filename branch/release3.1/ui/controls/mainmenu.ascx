﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="mainmenu.ascx.cs" Inherits="scorecard.ui.controls.mainmenu" %>
<asp:Menu ID="AppMenu" runat="server">
    <DataBindings>
        <asp:MenuItemBinding NavigateUrlField="menu_path" TextField="menu_name" />
    </DataBindings>
</asp:Menu>
