﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HR.Leave;
using System.Data;
using scorecard.implementations;
using RKLib.ExportData;

namespace scorecard.ui.reports
{
    public partial class leaveReport : System.Web.UI.UserControl
    {
        #region Properties

        int OrgUnitId
        {
            get { return int.Parse(Request.QueryString["orgid"].ToString()); }
        }

        string LeaveId
        {
            get { return Request.QueryString["lid"].ToString(); }
        }

        string OrgUnitName
        {
            get { return Request.QueryString["orgname"].ToString(); }
        }

        string StartDate
        {
            get { return Request.QueryString["sdate"].ToString(); }
        }

        string EndDate
        {
            get { return Request.QueryString["edate"].ToString(); }
        }

        #endregion

        #region Page Load Event Handling Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lblHeader.Text = string.Format("Staff On Leave Report for <b>{0}</b> Business Unit", OrgUnitName);
                BindData();
            }
        }

        #endregion

        #region Utility Methods

        void BindData()
        {
            ILeaveReport obj = new LeaveReportBL();
            DataTable dT = obj.GetEmployeesOnLeave(OrgUnitId, Common.GetXML(LeaveId.Split(','), "Data", "Leave", "Id"));
            Cache["Ds"] = dT;
            BindControl.BindListView(lstData, dT);
        }

        #endregion

        #region Button Event Handling

        protected void lnkExcel_Click(object sender, EventArgs e)
        {
            DownLoad();
        }

        void DownLoad()
        {
            DateTime today = DateTime.Now;
            DataTable dT = Cache["Ds"] as DataTable;

            if (dT.Rows.Count <= 0)
            {
                BindControl.BindLiteral(lblError, "There are no records to display.");
                return;
            }

            int[] cols = { 0, 1, 2, 3, 4, 5, 6, 7 };
            string[] headers = { "Full Name", "Email", "Employee No.", "Leave Type", "No. of Leave Days Taken", "Start Date", "End Date", "Status" };
            string fName = string.Format("{0}_OnLeave_{1}_{2}_{3}", OrgUnitName.Replace(" ", ""), today.Year, today.Month.ToString("D2"), today.Day.ToString("D2"));

            Export exp = new Export("Web");
            exp.ExportDetails(dT, cols, headers, Export.ExportFormat.Excel, Export.ExportAction.DownloadOnly, string.Format("{0}.xls", fName));
        }

        #endregion
    }
}