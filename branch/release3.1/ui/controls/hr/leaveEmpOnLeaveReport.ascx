﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="leaveEmpOnLeaveReport.ascx.cs"
    Inherits="scorecard.ui.reports.leaveReport" %>
<%@ Import Namespace="scorecard.implementations" %>
<div style="background-color: White;">
    <table cellpadding="0" cellspacing="10" border="0" width="90%">
        <tr>
            <td style="width: 600px;">
                <h2>
                    <asp:Literal ID="lblHeader" runat="server"></asp:Literal></h2>
            </td>
            <td align="right">
                <span style="color: Red">
                    <asp:Literal ID="lblError" runat="server"></asp:Literal></span>
                <asp:LinkButton ID="lnkPrint" runat="server" CausesValidation="False" OnClientClick="window.print();">
                    <img runat="server" src="~/ui/assets/icons/print.png" id="imgPrint" alt="Click to Print" /></asp:LinkButton>&nbsp;&nbsp;<asp:LinkButton
                        ID="lnkExcel" runat="server" CausesValidation="False" OnClick="lnkExcel_Click">
                        <img src="~/ui/assets/icons/Excel.png" runat="server" id="imgExcel" alt="Export to Excel" /></asp:LinkButton>
            </td>
        </tr>
    </table>
</div>
<div>
    <asp:ListView ID="lstData" runat="server">
        <LayoutTemplate>
            <table class="fullwidth" cellpadding="0" cellspacing="0" border="0" width="90%">
                <thead>
                    <tr>
                        <th align="left">
                            Full Name
                        </th>
                        <th align="left">
                            Email
                        </th>
                        <th align="left">
                            Employee No.
                        </th>
                        <th align="left">
                            Leave Type
                        </th>
                        <th align="right">
                            No. Of Days
                        </th>
                        <th align="right">
                            Start Date
                        </th>
                        <th align="right">
                            End Date
                        </th>
                        <th align="right">
                            Status
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr id="ItemPlaceHolder" runat="server">
                    </tr>
                </tbody>
            </table>
        </LayoutTemplate>
        <ItemTemplate>
            <tr class="odd">
                <td align="left">
                    <%# Eval("FullName")%>
                </td>
                <td align="left">
                    <%# Eval("EmailAddress")%>
                </td>
                <td align="left">
                    <%# Eval("EmployeeNo")%>
                </td>
                <td align="left">
                    <%# Eval("LeaveName")%>
                </td>
                <td align="right">
                    <%# Eval("NoOfLeaveDays")%>
                </td>
                <td align="right">
                    <%#Eval("StartDate") %>
                </td>
                <td align="right">
                    <%# Eval("EndDate")%>
                </td>
                <td align="right">
                    <%# Eval("StatusName")%>
                </td>
            </tr>
        </ItemTemplate>
        <EmptyDataTemplate>
            There are no records to display for the criteria you specified.
        </EmptyDataTemplate>
    </asp:ListView>
</div>
