﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="employees.ascx.cs" Inherits="scorecard.ui.controls.employees" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<h1>
    &nbsp; Employee Administration</h1>
<asp:UpdatePanel ID="UpdatePanelUser" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:MultiView ID="mainView" runat="server" ActiveViewIndex="0">
            <asp:View ID="vwData" runat="server">
                <div>
                    <asp:ListView ID="lvUsers" runat="server" OnItemEditing="lvUsers_ItemEditing" OnPagePropertiesChanged="lvUsers_PagePropertiesChanged">
                        <LayoutTemplate>
                            <table class="fullwidth" cellpadding="0" cellspacing="0" border="0">
                                <thead>
                                    <tr>
                                        <td>
                                            Full Name
                                        </td>
                                        <td>
                                            Employee No.
                                        </td>
                                        <td>
                                            Email Address
                                        </td>
                                        <td>
                                            User Name
                                        </td>
                                        <td>
                                            Job Title
                                        </td>
                                        <td>
                                            Role
                                        </td>
                                        <td>
                                            Company
                                        </td>
                                        <td>
                                            Cell No.
                                        </td>
                                        <td>
                                            Status
                                        </td>
                                        <td>
                                            Working Week
                                        </td>
                                        <td>
                                            Edit
                                        </td>
                                    </tr>
                                    <tr id="ItemPlaceHolder" runat="server">
                                    </tr>
                                </thead>
                            </table>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tbody>
                                <tr class="odd">
                                    <td>
                                        <%# Eval("FirstName") %>&nbsp;<%# Eval("MiddleName") %>&nbsp;<%# Eval("LastName") %><asp:HiddenField
                                            ID="hdnId" runat="server" Value='<%# Eval("EmployeeId")%>' />
                                    </td>
                                    <td>
                                        <%# Eval("EmployeeNo")%>
                                    </td>
                                    <td>
                                        <%# Eval("EmailAddress")%>
                                    </td>
                                    <td>
                                        <%# Eval("UserName")%>
                                    </td>
                                    <td>
                                        <%# Eval("Position.Name")%>
                                    </td>
                                    <td>
                                        <%# Eval("UserRole.RoleName")%>
                                    </td>
                                    <td>
                                        <%# Eval("Organisation.ORGUNIT_NAME")%>
                                    </td>
                                    <td>
                                        <%# Eval("Cellphone") %>
                                    </td>
                                    <td>
                                        <%# Eval("Status.StatusName")%>
                                    </td>
                                    <td>
                                        <%# Eval("WekinWeek.Name")%>
                                    </td>
                                    <td>
                                        <asp:LinkButton runat="server" ID="lnkEdit" CommandName="Edit" class="ui-state-default ui-corner-all"
                                            title="Edit Record" CausesValidation="false"> <span class="ui-icon ui-icon-pencil"></span></asp:LinkButton>
                                    </td>
                                </tr>
                            </tbody>
                        </ItemTemplate>
                        <EmptyItemTemplate>
                            <p class="errorMsg">
                                There are no records to display.</p>
                        </EmptyItemTemplate>
                    </asp:ListView>
                    <br />
                    <asp:DataPager ID="PagerData" runat="server" PagedControlID="lvUsers" PageSize="15">
                        <Fields>
                            <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True" ShowNextPageButton="False"
                                ShowPreviousPageButton="False" />
                            <asp:NumericPagerField />
                            <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True" ShowNextPageButton="False"
                                ShowPreviousPageButton="False" />
                        </Fields>
                    </asp:DataPager>
                    <br />
                    <asp:UpdateProgress ID="UpdateProgressUser" runat="server" AssociatedUpdatePanelID="UpdatePanelUser">
                        <ProgressTemplate>
                            <div>
                                <asp:Image ID="imgOrg" runat="server" ImageUrl="~/ui/images/icons/activity.gif" AlternateText="Please wait..">
                                </asp:Image>
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <br />
                    <div>
                        <p class="errorMsg">
                            <br />
                            <asp:Literal ID="lblMsg" runat="server"></asp:Literal>
                            <br />
                        </p>
                    </div>
                    <div id="tabs-3">
                        <p>
                            <asp:Button ID="btnNew" runat="server" CssClass="button" Text="Add New User" OnClick="btnNew_Click"
                                CausesValidation="false" />
                            &nbsp;<asp:Button ID="lnkDone" runat="server" CssClass="button tooltip" Text="Done"
                                CausesValidation="false" PostBackUrl="~/ui/index.aspx" />
                        </p>
                    </div>
                </div>
            </asp:View>
            <asp:View ID="vwForm" runat="server">
                <div>
                    <div>
                        <p>
                            <br />
                        </p>
                    </div>
                    <div>
                        <asp:Panel ID="pnlNewUser" runat="server" GroupingText="User Account Information">
                            <p>
                                <label for="sf">
                                    First Name :
                                </label>
                                &nbsp;<span class="field_desc">
                                    <input class="mf" name="mf0" type="text" runat="server" id="txtFirstName" />
                                </span>
                                <asp:RequiredFieldValidator ID="rqdFName" runat="server" ControlToValidate="txtFirstName"
                                    Display="None" ErrorMessage="Contact Name " ValidationGroup="AddUser"></asp:RequiredFieldValidator>
                            </p>
                            <p>
                                <label for="sf">
                                    Middle Name :
                                </label>
                                &nbsp;<span class="field_desc">
                                    <input class="mf" name="mf0" type="text" runat="server" id="txtMiddleName" />
                                </span>
                            </p>
                            <p>
                                <label for="sf">
                                    Surname :
                                </label>
                                &nbsp;<span class="field_desc">
                                    <input class="mf" name="mf0" type="text" runat="server" id="txtSurname" />
                                </span>
                                <asp:RequiredFieldValidator ID="rqdSurname" runat="server" ControlToValidate="txtSurname"
                                    Display="None" ErrorMessage="Last Name " ValidationGroup="AddUser"></asp:RequiredFieldValidator>
                            </p>
                            <p>
                                <label for="sf">
                                    Employee No. :
                                </label>
                                &nbsp;<span class="field_desc">
                                    <input class="mf" name="mf0" type="text" runat="server" id="txtEmployeeNo" />
                                </span>
                            </p>
                            <p>
                                <label for="sf">
                                    Email Address :
                                </label>
                                &nbsp;<span class="field_desc">
                                    <input class="mf" name="mf0" type="text" runat="server" id="txtEmailAddress" />
                                </span>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtEmailAddress"
                                    Display="None" ErrorMessage="Email Address " ValidationGroup="AddUser"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmailAddress"
                                    Display="None" ErrorMessage="Valid Email Addres" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                    ValidationGroup="AddUser"></asp:RegularExpressionValidator>
                            </p>
                            <p>
                                <label for="sf">
                                    Cellphone No. :
                                </label>
                                &nbsp;<span class="field_desc">
                                    <input class="mf" name="mf0" type="text" runat="server" id="txtCellNo" />
                                </span>
                            </p>
                            <p>
                                <label for="sf">
                                    Telephone No. :
                                </label>
                                &nbsp;<span class="field_desc">
                                    <input class="mf" name="mf0" type="text" runat="server" id="txtTel" />
                                </span>
                            </p>
                            <p>
                                <label for="sf">
                                    Company :
                                </label>
                                &nbsp;
                                <asp:DropDownList ID="ddlOrgUnit" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlOrgUnit_SelectedIndexChanged">
                                </asp:DropDownList>
                            </p>
                            <p>
                                <label for="sf">
                                    Status :
                                </label>
                                &nbsp;
                                <asp:DropDownList ID="ddStatus" runat="server">
                                </asp:DropDownList>
                            </p>
                            <p>
                                <label for="sf">
                                    Working Week :
                                </label>
                                &nbsp;
                                <asp:DropDownList ID="ddWeek" runat="server">
                                </asp:DropDownList>
                            </p>
                            <p>
                                <label for="sf">
                                    Job Title :
                                </label>
                                &nbsp;
                                <asp:DropDownList ID="ddlJobTitle" runat="server">
                                </asp:DropDownList>
                            </p>
                            <p>
                                <label for="sf">
                                    Date of Apointment :
                                </label>
                                &nbsp;
                                <asp:TextBox ID="txtStartDate" runat="server" Text="" ValidationGroup="Form" />
                                <asp:CalendarExtender ID="txtStartDate_CalendarExtender" runat="server" CssClass="calendar"
                                    Enabled="True" Format="MM/dd/yyyy" TargetControlID="txtStartDate">
                                </asp:CalendarExtender>
                            </p>
                            <p>
                                <label for="sf">
                                    User Type / Role :
                                </label>
                                &nbsp;
                                <asp:DropDownList ID="ddlUserType" runat="server">
                                </asp:DropDownList>
                            </p>
                            <p>
                                &nbsp;<asp:UpdatePanel ID="UpdatePanelMng" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <label for="sf">
                                            Line Manager :
                                        </label>
                                        &nbsp;
                                        <asp:DropDownList ID="ddlUsersReportsTo" runat="server" Width="150px">
                                        </asp:DropDownList>
                                        <asp:UpdateProgress ID="UpdateProgressMng" runat="server" AssociatedUpdatePanelID="UpdatePanelMng">
                                            <ProgressTemplate>
                                                <asp:Image ID="imgOrg1" runat="server" AlternateText="Please wait.." ImageUrl="~/ui/images/icons/activity.gif" />
                                            </ProgressTemplate>
                                        </asp:UpdateProgress>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddlOrgUnit" EventName="SelectedIndexChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="lvUsers" EventName="ItemEditing" />
                                        <asp:AsyncPostBackTrigger ControlID="btnNew" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                                <p>
                                </p>
                                <p>
                                    <label for="sf">
                                    Username :
                                    </label>
                                    &nbsp;<span class="field_desc">
                                    <input class="mf" name="mf0" type="text" runat="server" id="txtLoginName" />
                                    </span>
                                    <asp:RequiredFieldValidator ID="rqdUName" runat="server" 
                                        ControlToValidate="txtLoginName" Display="None" ErrorMessage="User Login Name" 
                                        ValidationGroup="AddUser"></asp:RequiredFieldValidator>
                                </p>
                                <p>
                                    &nbsp;<asp:Panel ID="pnlPassword" runat="server">
                                        <p>
                                            <label for="sf">
                                            Password :
                                            </label>
                                            &nbsp;<span class="field_desc">&nbsp;<input class="mf" name="mf0" type="password"
                                            runat="server" id="txtPassword" /></span>
                                            <asp:RequiredFieldValidator ID="rqdPin" runat="server" 
                                                ControlToValidate="txtPassword" Display="None" ErrorMessage="Password" 
                                                ValidationGroup="AddUser"></asp:RequiredFieldValidator>
                                        </p>
                                        <p>
                                            <label for="sf">
                                            Confirm Password :
                                            </label>
                                            &nbsp;<span class="field_desc">
                                            <input class="mf" name="mf0" type="password" runat="server" id="txtConfirmPassword" />
                                            </span>
                                            <asp:RequiredFieldValidator ID="rqdPass" runat="server" 
                                                ControlToValidate="txtConfirmPassword" Display="None" 
                                                ErrorMessage="Confirm Password" ValidationGroup="AddUser"></asp:RequiredFieldValidator>
                                        </p>
                                    </asp:Panel>
                                    <p>
                                    </p>
                                    <p>
                                        <label for="sf">
                                        Active :
                                        </label>
                                        &nbsp;
                                        <asp:CheckBox ID="chkActive" runat="server" />
                                    </p>
                                </p>
                                </p>
                             
                        </asp:Panel>
                    </div>
                    <div>
                        <p class="errorMsg">
                            <br />
                            <asp:HiddenField ID="hdnEmpId" runat="server" />
                            <asp:Literal ID="lblError" runat="server"></asp:Literal>
                            <br />
                            <br />
                        </p>
                        <p>
                            <asp:Button ID="lnkSaveAddUser" runat="server" CssClass="button" Text="Save" OnClick="lnkSaveAddUser_Click"
                                ValidationGroup="AddUser" />
                            <asp:Button ID="lnkUpdateUser" runat="server" CssClass="button" OnClick="lnkUpdateUser_Click"
                                Text="Save" Visible="False" ValidationGroup="AddUser" />
                            <asp:Button ID="lnkCancelAddUser" runat="server" CssClass="button" Text="Cancel"
                                OnClick="lnkCancelAddUser_Click" CausesValidation="false" />
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" HeaderText="Provide The Following Fields"
                                ShowMessageBox="True" ShowSummary="False" ValidationGroup="AddUser" />
                        </p>
                    </div>
                </div>
            </asp:View>
        </asp:MultiView>
    </ContentTemplate>
</asp:UpdatePanel>
