﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HR.Leave;
using scorecard.implementations;

namespace scorecard.ui.controls.hr
{
    public partial class leaveCalendar : System.Web.UI.UserControl
    {
        #region Property Method

        List<LeaveCalendar> OrgLeave
        {
            get { return ViewState["OrgLeave"] as List<LeaveCalendar>; }
        }

        #endregion

        #region Page Event Handling Methods

        int startNumber = DateTime.Now.Year - 2;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindControl.BindDropdown(ddCalYr, startNumber, DateTime.Now.Year, 2);
                SetOrgLeave();
                FillData();
            }
        }

        #endregion

        #region Utility Methods

        public bool Visibility(object hDateId)
        {
            var data = OrgLeave;
            Int64 hId = Int64.Parse(hDateId.ToString());
            foreach (LeaveCalendar o in data)
                if (o.HolidayDateId == hId) return o.Applied;
            return false;
        }

        public bool ApplyVisibility(object hId)
        {
            return (hId == DBNull.Value) ? true : !Visibility(hId);
        }

        void SetOrgLeave()
        {
            ILeaveCalendar oL = new LeaveCalendarBL();
            int year = int.Parse(ddCalYr.SelectedValue);
            var oLeave = oL.GetAppliedHolidays(Util.user.OrgId, int.Parse(ddCalYr.SelectedValue));
            ViewState["OrgLeave"] = oLeave;
        }

        void FillData()
        {
            int year = int.Parse(ddCalYr.SelectedValue);
            ILeaveCalendar obj = new LeaveCalendarBL();
            List<LeaveCalendar> data = obj.GetByStatus(Util.user.CountryId, year, true);
            BindControl.BindListView(lstData, data);
            pager.Visible = (data.Count <= pager.PageSize) ? false : true;
        }

        private void CancelUpdate()
        {
            lstData.EditIndex = -1;
            lstData.InsertItemPosition = InsertItemPosition.None;
            FillData();
        }

        void DeleteItem(ListViewCommandEventArgs e)
        {
            int holidayId = int.Parse((e.Item.FindControl("hdnId") as HiddenField).Value);
            string updatedBy = Util.user.LoginId;

            ILeaveCalendar obj = new LeaveCalendarBL();
            bool deleted = obj.Delete(holidayId, updatedBy);
            if (deleted) FillData();
            string msg = deleted ? Messages.GetDeleteMessage() : Messages.GetDeleteFailedMessage();
            BindControl.BindLiteral(lblMsg, msg);
        }

        void UpdateItem(ListViewCommandEventArgs e)
        {
            try
            {
                bool active = (e.Item.FindControl("chkActive") as CheckBox).Checked;
                string detail = (e.Item.FindControl("txtDetail") as TextBox).Text;
                string date = Common.GetInternationalDateFormat((e.Item.FindControl("txtDate") as TextBox).Text, DateFormat.dayMonthYear);
                string updatedBy = Util.user.LoginId;

                DateTime holidaydate = DateTime.Parse(date);

                int year = holidaydate.Year;
                int countryid = Util.user.CountryId;
                int holidayDateId = int.Parse((e.Item.FindControl("hdnIdE") as HiddenField).Value);

                LeaveCalendar obj = new LeaveCalendar
                {
                    CountryId = countryid,
                    Detail = detail,
                    HolidayDateId = holidayDateId,
                    HolidayDate = holidaydate,
                    CalendarYear = year,
                    CreatedBy = updatedBy,
                    UpdatedBy = updatedBy,
                    Active = active
                };

                ILeaveCalendar objl = new LeaveCalendarBL();
                bool saved = objl.UpdateLeaveCalendar(obj);
                string msg = saved ? Messages.GetUpdateMessage() : Messages.GetUpdateFailedMessage();
                BindControl.BindLiteral(lblMsg, msg);
                if (saved)
                {
                    lstData.EditIndex = -1;
                    FillData();
                }
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        void AddItem(ListViewCommandEventArgs e)
        {
            try
            {
                bool active = true;
                string detail = (e.Item.FindControl("txtDetaili") as TextBox).Text;
                string date = Common.GetInternationalDateFormat((e.Item.FindControl("txtDatei") as TextBox).Text, DateFormat.dayMonthYear);
                string updatedBy = Util.user.LoginId;

                DateTime holidaydate = DateTime.Parse(date);

                int year = holidaydate.Year;
                int countryid = Util.user.CountryId;

                LeaveCalendar obj = new LeaveCalendar
                {
                    CountryId = countryid,
                    Detail = detail,
                    HolidayDateId = 0,
                    HolidayDate = holidaydate,
                    CalendarYear = year,
                    CreatedBy = updatedBy,
                    UpdatedBy = updatedBy,
                    Active = active
                };

                ILeaveCalendar objl = new LeaveCalendarBL();
                bool saved = objl.AddLeaveCalendar(obj);
                string msg = saved ? Messages.GetUpdateMessage() : Messages.GetUpdateFailedMessage();
                BindControl.BindLiteral(lblMsg, msg);
                if (saved)
                {
                    lstData.InsertItemPosition = InsertItemPosition.None;
                    FillData();
                }
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }
        #endregion

        #region ListView Event Handling Methods

        protected void lstData_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            try
            {
                lstData.EditIndex = e.NewEditIndex;
                lstData.InsertItemPosition = InsertItemPosition.None;
                FillData();
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        protected void lstData_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                BindControl.BindLiteral(lblMsg, "");
                if (e.Item.ItemType == ListViewItemType.InsertItem ||
               e.Item.ItemType == ListViewItemType.DataItem)
                {
                    switch (e.CommandName)
                    {
                        case "AddItem":
                            AddItem(e);
                            break;
                        case "UpdateItem":
                            UpdateItem(e);
                            break;
                        case "DeleteItem":
                            DeleteItem(e);
                            break;
                        case "CancelUpdate":
                            CancelUpdate();
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        protected void lstData_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            pager.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            FillData();
        }

        #endregion

        #region Button Event Handling Methods

        protected void lnkAdd_Click(object sender, EventArgs e)
        {
            lstData.InsertItemPosition = InsertItemPosition.LastItem;
            FillData();
        }

        #endregion

        #region Dropdown Event Handling Methods

        protected void ddCalYr_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetOrgLeave();
            FillData();
        }

        #endregion

        #region CheckBox Event Handling Methods

        protected void ChkApplied_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                CheckBox chk = sender as CheckBox;
                if (chk.Checked)
                {
                    int hId = int.Parse(chk.ValidationGroup);

                    ILeaveCalendar obj = new LeaveCalendarBL();
                    LeaveCalendar lc = new LeaveCalendar
                    {
                        HolidayDateId = hId,
                        Applied = chk.Checked,
                        CreatedBy = Util.user.LoginId,
                        UpdatedBy = Util.user.LoginId
                    };

                    bool applied = obj.ApplyLeaveCalendar(lc, Util.user.OrgId);
                    BindControl.BindLiteral(lblMsg, applied ? Messages.GetHolidayDateAppliedSsuccessMessage() : Messages.GetHolidayDateApplyFailMessage());
                    if (applied)
                    {
                        SetOrgLeave();
                        FillData();
                    }
                }
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        #endregion
    }
}