﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using scorecard.implementations;
using HR.Human.Resources;
using ms = Microsoft.VisualBasic;

namespace scorecard.ui.controls.matrix
{
    public partial class matrix : System.Web.UI.UserControl
    {
        #region Properties

        int CompId
        {
            get { return Util.user.CompanyId; }
        }

        #endregion

        #region Private Variables

        ITalentMatrix objm;

        #endregion

        #region Default Constructor

        public matrix()
        {
            objm = new TalentMatrixBL();
        }

        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMsg.Text = "";
            if (!IsPostBack) InitialisePage();
        }

        #endregion

        #region Utility Methods

        void InitialisePage()
        {
            LoadTalentMatrix();
        }

        void LoadTalentMatrix()
        {
            try
            {
                var data = objm.GetActiveTalentMatrix(CompId, true);
                BindControl.BindListView(lstMatrix, data);
                if (data.Count <= pager.PageSize) pager.Visible = false;
                if (data.Count <= 0) lstMatrix.InsertItemPosition = InsertItemPosition.LastItem;
            }
            catch (Exception e)
            {
                Util.LogErrors(e);
            }
        }

        public string GetBoolean(object boolVal)
        {
            if (boolVal == DBNull.Value) return "Unknown";
            return bool.Parse(boolVal.ToString()) ? "Yes" : "No";
        }

        void CancelUpdate()
        {
            lstMatrix.EditIndex = -1;
            lstMatrix.InsertItemPosition = InsertItemPosition.None;
            LoadTalentMatrix();
        }

        void DeleteMatrix(int matrixId)
        {
            try
            {
                string updatedBy = Util.user.LoginId;
                TalentMatrix obj = new TalentMatrix
                {
                    Active = false,
                    MatrixId = matrixId,
                    UpdatedBy = updatedBy
                };

                bool deleted = objm.DeactivateMatrix(obj);
                if (deleted) LoadTalentMatrix();
                BindControl.BindLiteral(lblMsg, deleted ? Messages.GetDeleteMessage() : Messages.GetDeleteFailedMessage());
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        void AddNewMatrix(ListViewCommandEventArgs e)
        {
            try
            {
                if (lstMatrix.Items.Count > Util.MaximumMatrixSize)
                {
                    BindControl.BindLiteral(lblMsg, string.Format("You cannot have more than {0} items in a matrix.", Util.MaximumMatrixSize));
                    return;
                }

                TextBox txtTalentMatrixDescription = e.Item.FindControl("txtTalentMatrixDescription") as TextBox;
                TextBox txtLowerBound = e.Item.FindControl("txtLowerBound") as TextBox;
                TextBox txtUpperBound = e.Item.FindControl("txtUpperBound") as TextBox;
                TextBox txtTalentMatrixName = e.Item.FindControl("txtTalentMatrixName") as TextBox;
                TextBox txtLowerBoundSign = e.Item.FindControl("txtLowerBoundSign") as TextBox;
                TextBox txtUpperBoundSign = e.Item.FindControl("txtUpperBoundSign") as TextBox;
                TextBox txtPositionInMatrix = e.Item.FindControl("txtPositionInMatrix") as TextBox;

                DropDownList ddL = e.Item.FindControl("ddLowerBoundSign") as DropDownList;
                DropDownList ddU = e.Item.FindControl("ddUpperBoundSign") as DropDownList;

                //CheckBox chkActive = e.Item.FindControl("chkActive") as CheckBox;
                if (!ms.Information.IsNumeric(txtLowerBound.Text) ||
                   !ms.Information.IsNumeric(txtUpperBound.Text) ||
                    !ms.Information.IsNumeric(txtPositionInMatrix.Text)
                    )
                {
                    BindControl.BindLiteral(lblMsg, "Upper Bound, Lower Bound & Position should be a numeric");
                    return;
                }

                decimal lowerBound = decimal.Parse(txtLowerBound.Text);
                decimal upperBound = decimal.Parse(txtUpperBound.Text);
                int position = int.Parse(txtPositionInMatrix.Text);

                string userName = Util.user.FullName;
                string desc = txtTalentMatrixDescription.Text;
                string matrixName = txtTalentMatrixName.Text;
                string lowerBoundSign = ddL.SelectedValue;
                string upperBoundSign = ddU.SelectedValue;

                TalentMatrix obj = new TalentMatrix
                {
                    Active = true,
                    CompanyId = Util.user.CompanyId,
                    CreatedBy = Util.user.LoginId,
                    DateCreated = DateTime.Now,
                    DateUpdated = DateTime.Now,
                    LowerBound = lowerBound,
                    LowerBoundSign = lowerBoundSign,
                    MatrixId = 0,
                    PositionInMatrix = position,
                    TalentMatrixDescription = desc,
                    TalentMatrixName = matrixName,
                    UpdatedBy = Util.user.LoginId,
                    UpperBound = upperBound,
                    UpperBoundSign = upperBoundSign
                };

                bool saved = objm.AddTalentMatrix(obj);
                if (saved)
                {
                    lstMatrix.InsertItemPosition = InsertItemPosition.None;
                    LoadTalentMatrix();
                    BindControl.BindLiteral(lblMsg, Messages.GetSaveMessage());
                }
                else BindControl.BindLiteral(lblMsg, Messages.GetSaveFailedMessage());
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
            }
        }

        void UpdateMatrix(ListViewCommandEventArgs e)
        {
            try
            {
                TextBox txtTalentMatrixDescription = e.Item.FindControl("txtTalentMatrixDescriptionE") as TextBox;
                TextBox txtLowerBound = e.Item.FindControl("txtLowerBoundE") as TextBox;
                TextBox txtUpperBound = e.Item.FindControl("txtUpperBoundE") as TextBox;
                TextBox txtTalentMatrixName = e.Item.FindControl("txtTalentMatrixNameE") as TextBox;
                TextBox txtLowerBoundSign = e.Item.FindControl("txtLowerBoundSignE") as TextBox;
                TextBox txtUpperBoundSign = e.Item.FindControl("txtUpperBoundSignE") as TextBox;
                TextBox txtPositionInMatrix = e.Item.FindControl("txtPositionInMatrixE") as TextBox;

                HiddenField hdnMatrixId = e.Item.FindControl("hdnMatrixIdE") as HiddenField;

                DropDownList ddL = e.Item.FindControl("ddLowerBoundSignE") as DropDownList;
                DropDownList ddU = e.Item.FindControl("ddUpperBoundSignE") as DropDownList;

                CheckBox chkActiveE = e.Item.FindControl("chkActiveE") as CheckBox;

                if (!ms.Information.IsNumeric(txtLowerBound.Text) ||
                  !ms.Information.IsNumeric(txtUpperBound.Text) ||
                   !ms.Information.IsNumeric(txtPositionInMatrix.Text)
                   )
                {
                    BindControl.BindLiteral(lblMsg, "Upper Bound, Lower Bound & Position should be a numeric");
                    return;
                }

                decimal lowerBound = decimal.Parse(txtLowerBound.Text);
                decimal upperBound = decimal.Parse(txtUpperBound.Text);
                
                int matrixId = int.Parse(hdnMatrixId.Value);
                int position = int.Parse(txtPositionInMatrix.Text);

                string userName = Util.user.FullName;
                string desc = txtTalentMatrixDescription.Text;
                string matrixName = txtTalentMatrixName.Text;
                string lowerBoundSign = ddL.SelectedValue;
                string upperBoundSign = ddU.SelectedValue;

                TalentMatrix obj = new TalentMatrix
                {
                    Active = chkActiveE.Checked,
                    CompanyId = Util.user.CompanyId,
                    CreatedBy = Util.user.LoginId,
                    DateCreated = DateTime.Now,
                    DateUpdated = DateTime.Now,
                    LowerBound = lowerBound,
                    LowerBoundSign = lowerBoundSign,
                    MatrixId = matrixId,
                    PositionInMatrix = position,
                    TalentMatrixDescription = desc,
                    TalentMatrixName = matrixName,
                    UpdatedBy = Util.user.LoginId,
                    UpperBound = upperBound,
                    UpperBoundSign = upperBoundSign
                };

                bool saved = objm.UpdateTalentMatrix(obj);
                lstMatrix.EditIndex = -1;
                lstMatrix.InsertItemPosition = InsertItemPosition.None;
                if (saved)
                {
                    LoadTalentMatrix();
                    BindControl.BindLiteral(lblMsg, Messages.GetSaveMessage());
                }
                else BindControl.BindLiteral(lblMsg, Messages.GetSaveFailedMessage());
            }
            catch (Exception ex)
            {
                Util.LogErrors(ex);
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
            }
        }

        void BindDrodDowns(DropDownList ddL, DropDownList ddU, string lowerValue, string upperValue)
        {
            BindControl.BindDropdown(ddL, "Text", "Value", lowerValue, Util.GetOperatorList());
            BindControl.BindDropdown(ddU, "Text", "Value", upperValue, Util.GetOperatorList());
        }

        #endregion

        #region ListView Event Handling Methods including Slide Paging

        protected void PagerCommand(object sender, DataPagerCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Next":
                    //guard against going off the end of the list
                    e.NewStartRowIndex = Math.Min(e.Item.Pager.StartRowIndex + e.Item.Pager.MaximumRows, e.Item.Pager.TotalRowCount - e.Item.Pager.MaximumRows);
                    e.NewMaximumRows = e.Item.Pager.MaximumRows;
                    break;
                case "Previous":
                    //guard against going off the begining of the list
                    e.NewStartRowIndex = Math.Max(0, e.Item.Pager.StartRowIndex - e.Item.Pager.MaximumRows);
                    e.NewMaximumRows = e.Item.Pager.MaximumRows;
                    break;
                case "Last":
                    //the
                    e.NewStartRowIndex = e.Item.Pager.TotalRowCount - e.Item.Pager.MaximumRows;
                    e.NewMaximumRows = e.Item.Pager.MaximumRows;
                    break;
                case "First":
                default:
                    e.NewStartRowIndex = 0;
                    e.NewMaximumRows = e.Item.Pager.MaximumRows;
                    break;
            }
        }

        protected void CurrentPageChanged(object sender, EventArgs e)
        {
            TextBox txtCurrentPage = sender as TextBox;
            DataPager pager = this.lstMatrix.FindControl("pager") as DataPager;
            int startRowIndex = Math.Min((int.Parse(txtCurrentPage.Text) - 1) * pager.MaximumRows, pager.TotalRowCount - pager.MaximumRows);
            pager.SetPageProperties(startRowIndex, pager.MaximumRows, true);
        }

        protected void lstMatrix_PagePropertiesChanged(object sender, EventArgs e)
        {
            LoadTalentMatrix();
        }

        protected void lstMatrix_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            try
            {
                lstMatrix.EditIndex = e.NewEditIndex;
                lstMatrix.InsertItemPosition = InsertItemPosition.None;
                LoadTalentMatrix();
                ListViewItem lItem = lstMatrix.Items[e.NewEditIndex];

                DropDownList ddL = lItem.FindControl("ddLowerBoundSignE") as DropDownList;
                DropDownList ddU = lItem.FindControl("ddUpperBoundSignE") as DropDownList;

                string lowerValue = (lItem.FindControl("hdnLowerBoundSignE") as HiddenField).Value;
                string upperValue = (lItem.FindControl("hdnUpperBoundSignE") as HiddenField).Value;

                BindDrodDowns(ddL, ddU, lowerValue, upperValue);
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        protected void lstMatrix_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                switch (e.CommandName)
                {
                    case "insertItem":
                        AddNewMatrix(e);
                        break;
                    case "updateItem":
                        UpdateMatrix(e);
                        break;
                    case "deleteItem":
                        int rId = int.Parse((e.Item.FindControl("hdnMatrixId") as HiddenField).Value);
                        DeleteMatrix(rId);
                        break;
                    case "cancelUpdate":
                        CancelUpdate();
                        break;
                }
            }
            catch (Exception ex)
            {
                BindControl.BindLiteral(lblMsg, Messages.GetErrorMessage());
                Util.LogErrors(ex);
            }
        }

        #endregion

        #region Button Event Handling Methods

        protected void lnkCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("index.aspx");
        }

        protected void lnkAdd_Click(object sender, EventArgs e)
        {
            lstMatrix.InsertItemPosition = InsertItemPosition.LastItem;
            LoadTalentMatrix();
            var ddL = lstMatrix.InsertItem.FindControl("ddLowerBoundSign") as DropDownList;
            var ddU = lstMatrix.InsertItem.FindControl("ddUpperBoundSign") as DropDownList;
            BindDrodDowns(ddL, ddU, string.Empty, string.Empty);
        }

        #endregion
    }
}