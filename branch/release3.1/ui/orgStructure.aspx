﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ui/inside.Master" AutoEventWireup="true"
    CodeBehind="orgStructure.aspx.cs" Inherits="scorecard.ui.orgStructure" %>

<%@ Register Src="controls/mgtorgstructure.ascx" TagName="mgtorgstructure" TagPrefix="uc1" %>
<%@ Register Src="controls/orgstructure.ascx" TagName="orgstructure" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <uc1:mgtorgstructure ID="mgtorgstructure1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="OrgStructure" runat="server">
    <uc2:orgstructure ID="orgstructure1" runat="server" />
</asp:Content>
