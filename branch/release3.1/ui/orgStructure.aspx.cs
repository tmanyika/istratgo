﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Geekees.Common.Controls;

namespace scorecard.ui
{
    public partial class orgStructure : System.Web.UI.Page
    {
        #region Properties

        protected string ASTreeViewVersion
        {
            get
            {
                return typeof(ASTreeView).Assembly.GetName().Version.ToString();
            }
        }

        #endregion

        #region Page Event Handling Methods

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #endregion
    }
}